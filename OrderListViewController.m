//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "OrderListViewController.h"
#import "CustomCellGeneral.h"
#import "CurrentOrderViewControllerDetail.h"
bool firstTime = YES;
bool isSearchMode;
NSArray *aryDisFilterData;
@interface OrderListViewController ()
{

    
}

@property (weak, nonatomic) IBOutlet UITableView *tblOrderLis;



@end

@implementation OrderListViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
     self.filteredListContent = [[NSMutableArray alloc] init];
     self.arrQuoteList = [[NSMutableArray alloc] init];
     isSearchMode = NO;
     firstTime = YES;
     isSearchMode = NO;
    if (self.arrQuoteList.count > 0) {
        [self.arrQuoteList removeAllObjects];
    }
    [self CallWebserviceToGetData];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    }

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [aryDisFilterData count];
    }
	else
	{
        return [self.arrQuoteList count];
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    @try {
        static NSString *CellIdentifier = CELL_IDENTIFIER11;
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell = [nib objectAtIndex:10];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (isSearchMode)
        {
            cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[aryDisFilterData objectAtIndex:indexPath.row] objectForKey:@"Delname"]];
            cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[aryDisFilterData objectAtIndex:indexPath.row] objectForKey:@"OrderNo"]];
        }
        else
        {
            cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[[self.arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"Delname"] objectForKey:@"text"]];
            cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[[self.arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"OrderNo"] objectForKey:@"text"]];
        }
        
        [cell.btnQuoteStatus setImage:Nil forState:UIControlStateNormal];
        [cell.btnReadUnread setImage:Nil forState:UIControlStateNormal];
        cell.lblCustomerName.textAlignment = NSTextAlignmentLeft;
        cell.lblCustomerName.frame = CGRectMake(12, cell.lblCustomerName.frame.origin.y, cell.lblCustomerName.frame.size.width,  cell.lblCustomerName.frame.size.height);
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"Exeprtion::%@",exception.description);
    }
    
}

// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrentOrderViewControllerDetail *objData = [[CurrentOrderViewControllerDetail alloc]initWithNibName:@"CurrentOrderViewControllerDetail" bundle:[NSBundle mainBundle]];
    
    if (isSearchMode)
	{
        objData.strOrderNum = [[aryDisFilterData objectAtIndex:indexPath.row] objectForKey:@"OrderNo"];
        
    }
	else
	{
         objData.strOrderNum = [[[self.arrQuoteList objectAtIndex:indexPath.row] objectForKey:@"OrderNo"] objectForKey:@"text"];
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:objData invokeByController:self isStackStartView:FALSE];
}

-(void)CallWebserviceToGetData
{
    
    NSLog(@"%@",[AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected?@"true":@"false");
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        spinner.mode = MBProgressHUDModeIndeterminate;
            [spinner removeFromSuperview];

           // if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                [self callWSGetAvailable];
           // }
            
        
    }
    
    else
    {
        UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:INTERNET_OFFLINE_MESSAGE delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [aler show];
        aler = nil;
        
    }
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
    {
        CGRect frm =     self.searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
    else{
        CGRect frm =     self.searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
    [self.searchingBar setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];

}

-(void)callWSGetAvailable{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs objectForKey:@"members"];
    NSString *salesuserName = [prefs objectForKey:@"userName"];
    NSString *branch = [prefs objectForKey:@"BRANCH"];
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.strWebserviceType = @"WS_PRODUCT_GET_AVIALABLE_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"branch=%@&salestype=%@",branch,salesType];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CurrentOrderStatus_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}
#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    
    @try {
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        if([dictResponse isKindOfClass:[NSDictionary class]])
        {
            if([[[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"]objectForKey:@"Flag"] objectForKey:@"text"])
            {
                if([[[[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"]objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"])
                    
                {
                    if([[[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"] objectForKey:@"Data"]isKindOfClass:[NSArray class]])
                    {
                        if(!self.arrQuoteList)
                        {
                            self.arrQuoteList = [[NSMutableArray alloc]initWithArray:[[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"] objectForKey:@"Data"]];
                        }
                        else{
                          
                           
                            self.arrQuoteList = [[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"] objectForKey:@"Data"];
                        }
                    }
                    
                    else if([[[dictResponse objectForKey:@"Response"]objectForKey:@"Data"]isKindOfClass:[NSDictionary class]])
                    {
                        if(!self.arrQuoteList)
                        {
                            self.arrQuoteList = [[NSMutableArray alloc]init];
                        }
                        
                        
                        [self.arrQuoteList addObject:[[[dictResponse objectForKey:@"Response"]objectForKey:@"OrderList"] objectForKey:@"Data"]] ;
                        
                    }
                }
            }
            
        }
        NSLog(@"dictResponse %@",dictResponse);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileInDirectory1 = [documentsDirectory stringByAppendingPathComponent:@"TEST1212.plist"];
        [dictResponse writeToFile:fileInDirectory1 atomically:YES];
        
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    @finally {
        [self changeData];
        [self.tblOrderLis reloadData];
        [spinner removeFromSuperview];
    }
    
}

-(void)changeData
{

    for (NSDictionary *dict in self.arrQuoteList) {
        
        NSMutableDictionary *dcit1 = [[NSMutableDictionary alloc]init];
        [dcit1 setValue:[[dict objectForKey:@"Delname"] objectForKey:@"text"] forKey:@"Delname"];
        [dcit1 setValue:[[dict objectForKey:@"OrderNo"] objectForKey:@"text"] forKey:@"OrderNo"];
        [self.filteredListContent addObject:dcit1];
        dcit1 = nil;
        
    }
}
-(void)ASIHTTPRequest_Error:(id)error
{
    [spinner removeFromSuperview];
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        isSearchMode = YES;
        
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    
    isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
     [self.tblOrderLis reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
  
    [self.tblOrderLis reloadData];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    @try {
        if([scope isEqualToString:@"Order No"])
        {
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"OrderNo", searchText];
            
            aryDisFilterData = [self.filteredListContent filteredArrayUsingPredicate:predicate];
            
        }
        else
        {
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"Delname", searchText];
            
            aryDisFilterData = [self.filteredListContent filteredArrayUsingPredicate:predicate];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"Erro::%@",exception.description);
    }
    @finally {
         [self.tblOrderLis reloadData];
    }
    
}
@end
