//
//  Reachability_CentralizedHelper.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 23/02/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability_Centralized.h"

@interface Reachability_CentralizedHelper : Reachability_Centralized

+ (Reachability_CentralizedHelper *) sharedHelper;
@end
