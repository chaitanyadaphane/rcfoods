//
//  NSObject+UITableViewMyCustomTable.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 23/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITableView (MyCustomTable)

//Apply rounded corners at bottom of tableview
-(void)applyCornersAtBottom;
@end
