//
//  DownloadUrlOperation.h
//  OperationsDemo
//
//  Created by Ankit Gupta on 6/6/11.
//  Copyright 2011 Pulse News. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZipArchive.h"


@interface DownloadUrlOperation : NSOperation <CHCSVParserDelegate>{
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    // The actual NSURLConnection management
    NSURL*    connectionURL_;
    NSURLConnection*  connection_;
    NSMutableData*    data_;
    
    int tag;
    
    BOOL isUploading;
    
    NSString *soapMessage;
    NSString *soapAction;
    
    
    NSMutableArray *_lines;
    NSMutableArray *_currentLine;
    NSString *_insertSQL;
    FMDatabase *_db;
    NSString *_tableName;
}

@property (nonatomic,readonly) NSError* error;
@property (nonatomic,readonly) NSMutableData *data;
@property (nonatomic,readonly) NSURL *connectionURL;
@property (nonatomic,assign) int tag;
@property (nonatomic,assign) BOOL isUploading;
@property (nonatomic,assign) BOOL isOrders;
@property (nonatomic,assign) BOOL isQuotes;
@property (nonatomic,assign) BOOL isOfflineQuotes;

@property (nonatomic,retain) NSString *soapMessage;
@property (nonatomic,retain) NSString *soapAction;
@property (nonatomic,retain) ZipArchive *unZip;

@property (nonatomic,retain) NSMutableArray *_lines;
@property (nonatomic,retain) NSMutableArray *_currentLine;
@property (nonatomic,retain) NSString *_insertSQL;

@property (nonatomic,retain) NSString *strFlush;
- (id)initWithURL:(NSURL*)url;

- (void)prepareOrdersHeaderForWritingToCsv;
- (void)prepareOrdersDetailsForWritingToCsv;
- (void)prepareQuotesHeaderForWritingToCsv;
- (void)prepareQuotesDetailsForWritingToCsv;

@end
