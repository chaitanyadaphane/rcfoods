//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "MBProgressHUD.h"

@interface QuoteHistoryCustomerListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate>{

    NSString *strWebserviceType;
    
    int currPage;
    int totalCount;
    int recordNumber;
    
    NSMutableArray *arrCustomer; // The master content.
    NSMutableArray *arrCustomerForWebservices;
    
    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
    
    BOOL isFromLoadProfile;
    
    dispatch_queue_t backgroundQueueForCustomerList;
    FMDatabaseQueue *dbQueueCustomerList;
    
    BOOL isNetworkConnected;
    
    NSIndexPath *lastIndexPath;
    NSOperationQueue *operationQueue;
    
    NSMutableDictionary *mainDict;
}
// Usercall List value for select user
@property (nonatomic, retain) NSString *selectedUserStr;
@property (nonatomic, assign) int callDay;

@property (nonatomic, assign) BOOL isUserCallSchedule;



@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrCustomer;
@property (nonatomic, retain) NSMutableArray *arrCustomerForWebservices;
@property (nonatomic, retain) NSIndexPath *lastIndexPath;
@property (nonatomic, retain)  NSMutableDictionary *mainDict;
@property (nonatomic, assign) BOOL isFromLoadProfile;
@property (nonatomic, retain) MBProgressHUD *spinner;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblQuoteList;
@property (nonatomic,retain) IBOutlet UISearchBar *searchingBar;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwResults;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;

@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

#pragma mark - Custom Methods
-(void)callWSGetCustomerList:(int)pageIndex;
-(void)callGetCustomerListFromDB:(FMDatabase *)db;
@end
