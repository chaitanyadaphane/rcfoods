//
//  TermsAndConditionVC.m
//  Blayney
//
//  Created by Pooja on 10/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "TermsAndConditionVC.h"
#import "AppDelegate.h"
#import "StartStartViewController.h"
#import "LoginViewController.h"

@interface TermsAndConditionVC (){
    StartStartViewController *startStartViewController;
}

@end

@implementation TermsAndConditionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //hides the background shadow
    for (UIView *view in [[[_webviewTC subviews] objectAtIndex:0] subviews])
    {
        if ([view isKindOfClass:[UIImageView class]])
            view.hidden = YES;
    }
    
    //calls htmls file to display in webview
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"aboutus" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_webviewTC loadHTMLString:htmlString baseURL:nil];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)AcceptAction:(id)sender {
    
    NSString *strFirstTime = @"FirstTime";
    
    [[NSUserDefaults standardUserDefaults] setValue:strFirstTime forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[AppDelegate getAppDelegateObj] loadLocalDB];
    HJObjManager *obj_Manager = [[HJObjManager alloc] initWithLoadingBufferSize:6 memCacheSize:20];
    NSString* cacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/imgcache/"] ;
    HJMOFileCache* fileCache = [[HJMOFileCache alloc] initWithRootPath:cacheDirectory];
    obj_Manager.fileCache = fileCache;
    fileCache.fileCountLimit = 100;
    fileCache.fileAgeLimit=60*60*24*7;
    [fileCache trimCacheUsingBackgroundThread];
    
    //Create Sync Folder
    [[AppDelegate getAppDelegateObj] createSyncFolder];
    
    [self dismissVerticalPressed];
    
}


#pragma mark - Curtain view Method
- (void)dismissVerticalPressed
{
    
    StartStartViewController *startStartViewControllerObj  = [[StartStartViewController alloc] initWithNibName:@"StartStartViewController" bundle:[NSBundle mainBundle]];
    [AppDelegate getAppDelegateObj].startViewController =startStartViewControllerObj;
    [AppDelegate getAppDelegateObj].window.rootViewController = startStartViewControllerObj;
    [[AppDelegate getAppDelegateObj].window makeKeyAndVisible];


}
@end
