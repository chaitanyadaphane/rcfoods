//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "ASINetworkQueue.h"

@interface OfflineQuoteListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ReachabilityRequest_delegate,ASIHTTPRequest_delegate>{
    
    NSString *strWebserviceType;

    int currPage;
    int totalCount;
    int recordNumber;
    
    NSMutableArray *arrQuotes; // The master content.
    NSMutableArray *arrSelectedQuotes;
    
    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.

    NSString *strCustCode;
    
    IBOutlet UIView *vwCustomLoadProfileTip;
    
    dispatch_queue_t backgroundQueueForUploadQuote;;
    FMDatabaseQueue *dbQueueQuoteList;
    
    BOOL isQuoteEntryModule;    
    BOOL isNetworkConnected;
    
    
    NSString *strQuoteNum;
    NSMutableArray *arrProducts;
    NSMutableDictionary *dictHeaderDetails;
    
    NSIndexPath  *deleteIndexPath;
    
    ASINetworkQueue *operationQueueUpload;
    NSTimer *timerForUpload; 
    
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrQuotes;
@property (nonatomic, retain) NSMutableArray *arrSelectedQuotes;
@property (nonatomic, retain) NSString *strCustCode;

@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) NSMutableDictionary *dictHeaderDetails;

@property (nonatomic,retain)  NSIndexPath  *deleteIndexPath;

@property (nonatomic, retain) NSTimer *timerForUpload;

@property (nonatomic, unsafe_unretained)IBOutlet UILabel *lblStatusText;
@property (nonatomic, unsafe_unretained) IBOutlet UIProgressView *progessView;
@property (nonatomic, unsafe_unretained)IBOutlet UIButton *btnUpload;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *btnLoad;
@property (nonatomic, unsafe_unretained)IBOutlet UIButton *btnDelete;
@property (nonatomic, unsafe_unretained) IBOutlet UITableView *tblQuoteList;
@property (nonatomic, retain)IBOutlet UIView *vwContentSuperview;
@property (nonatomic, retain)IBOutlet UIView *vwNoQuotes;
@property (nonatomic, retain)IBOutlet UIView *vwTblQuotes;
@property (nonatomic, assign)BOOL isUploadInProgress;


#pragma mark - Custom Methods

//- (void)callWSGetQuoteList:(int)pageIndex;
//- (void)callWSViewQuoteDetailsDetail:(int)index;

-(void)callGetQuoteListFromDB:(FMDatabase *)db;
- (IBAction)actionLoadProfile:(UIButton *)sender;

-(void)callGetQuoteListByQuoteFromDB:(FMDatabase *)db Quote:(NSString *)quote;

@end
