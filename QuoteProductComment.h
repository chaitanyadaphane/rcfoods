//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol SetCommentDelegate
- (void)setCommentPosition:(BOOL)status;
- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict;
@end

@interface QuoteProductComment : UIViewController<UITextViewDelegate>{
    
}

@property (nonatomic,retain) NSString *commentStr;
@property (assign) int commntTAg;
@property (nonatomic,unsafe_unretained)IBOutlet UITextView *commentTextView;
@property (nonatomic,unsafe_unretained)IBOutlet UIButton *btnComment;
@property (unsafe_unretained) id<SetCommentDelegate> delegate;
@property (assign)int productIndex;
@property (assign)BOOL isCommentAddedOnLastLine;

@property (nonatomic,unsafe_unretained)IBOutlet UISwitch *swtchComment;

-(IBAction)sendComment:(id)sender;

@end
