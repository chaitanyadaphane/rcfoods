//
//  CashPickUpPopOverController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 08/05/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissPopOverDelegateForCashPickUpDelegate
- (void)actiondismissPopOverForCashPickUp:(NSString*)strPickUpAmt IsChequeAvaliable:(BOOL)isChequeAvaliable;
@end

@interface CashPickUpPopOverController : UIViewController{
    BOOL isChequeAvailable;
    
    id<DismissPopOverDelegateForCashPickUpDelegate> __unsafe_unretained delegate;   
}

@property (nonatomic,retain) NSString *strCashAmount;

@property (assign)IBOutlet UITextField *txtCashPickAmount;
@property (assign)IBOutlet UISwitch *swtchChequeNotAvailable;
@property (unsafe_unretained)id<DismissPopOverDelegateForCashPickUpDelegate>delegate;

-(IBAction)actionSetChequeAvailability:(id)sender;
-(IBAction)actionDissmissPopOver:(id)sender;
@end
