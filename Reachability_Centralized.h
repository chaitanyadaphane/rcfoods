//
//  Reachability_Centralized.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 23/02/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReachabilityRequest_delegate <NSObject>
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus;
@end


@interface Reachability_Centralized : NSObject<ReachabilityRequest_delegate>{
    id <ReachabilityRequest_delegate> delegate;
    Reachability* internetReachable;
}

@property(nonatomic,retain) id <ReachabilityRequest_delegate> delegate;
@property(nonatomic,retain) Reachability* internetReachable;

-(void)setReachabilityObserver;
-(void)notifyNetworkReachability:(NSNotification *)notice;

@end
