//
//  NSString+MD5.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 31/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5;
- (NSString *)getFileNameFromURL;
- (NSString *)trimSpaces;
@end
