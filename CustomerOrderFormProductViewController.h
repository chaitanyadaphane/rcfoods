//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerOrderFormProductViewController : UIViewController<UITextFieldDelegate>{
    
    NSString *strWebserviceType;
    
    NSMutableDictionary *dictCustomerHistoryDetails;
    NSArray *arrHeaderLabels;
    float priceCode;
    NSString *strTranCode;
    float discount;
    
    BOOL isFromCustomerHistory;
}

@property (nonatomic,retain) NSMutableDictionary *dictCustomerHistoryDetails;
@property (nonatomic,retain) NSString *strTranCode;
@property (nonatomic,assign) BOOL isFromCustomerHistory;
@property (nonatomic,retain) NSString *strPrice;
@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,retain) NSString *strPriceType;
@property NSString *custCatogry;
@property NSString *strMargin;
@property NSString *isProductFromSacontrct;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblTansactionNumber;

@end
