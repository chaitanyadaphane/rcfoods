//
//  UploadStatusViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 06/08/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadStatusViewController : UIViewController{
    
}

@property (nonatomic,retain)NSString *strMessage;
@property (nonatomic,retain)NSString *strStatusNumber;
@property (nonatomic,retain)NSString *strOldStatusNumber;
@property (nonatomic,retain)NSString *strUploadDate;
@property (nonatomic,retain)NSArray *arrHeaderLabels;
@property (nonatomic,assign)BOOL isOrder;
@property (nonatomic,assign)BOOL isFinalisedOrder;
@property (nonatomic,unsafe_unretained)IBOutlet UILabel *lblStatus;
@property (nonatomic,assign)BOOL isError;

@end
