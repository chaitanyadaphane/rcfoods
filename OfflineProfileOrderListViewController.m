//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "OfflineProfileOrderListViewController.h"
#import "CustomCellGeneral.h"
#import "ViewQuoteDetailsViewController.h"
#import "OfflineAddQuoteViewController.h"
#import "DownloadUrlOperation.h"
#import "UploadStatusViewController.h"

#import "ProfileOrderEntryViewController.h"

#import "OfflineProfileDetailViewController.h"

#define QUOTE_HISTORY_QUOTE_LIST_WS @"quote/quotehistory.php?"sent

#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface OfflineProfileOrderListViewController ()
{
    BOOL _loadingInProgress;
    BOOL isSelectAll;
    NSMutableDictionary *dictStockValue;
    NSMutableArray *imageAttachedArr;

}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;

@property (nonatomic) BOOL viewIsIn;

@end

NSString *strHTLM ;
@implementation OfflineProfileOrderListViewController
@synthesize listContent,filteredListContent,arrQuotes,strCustCode,btnLoad,arrQuoteDetails,arrQuotesrForWebservices,strQuoteNum,dictHeaderDetails,arrProducts,btnDelete,deleteIndexPath,isProfileOrder,lblNoOrders,btnUpload,progessView,timerForUpload,arrSelectedOrders,lblStatusText,strPriceChange,strPickupInfo;

@synthesize vwTblQuotes,vwContentSuperview,vwNoQuotes,tblQuoteList;
@synthesize strDeliveryDate,strContact,strAddress1,strAddress2,strAddress3,strSubUrb,strAllowPartial,strBranch,strCarrier,strChargeRate,strChargetype,strCountry,strDirect,strDropSequence,strExchangeCode,strExportDebtor,strHeld,strNumboxes,strOrderDate,strPeriodRaised,strPostCode,strPriceCode,strSalesBranch,strSalesman,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strTradingTerms,strYearRaised,strStockPickup,strWarehouse,strCashRepPickUp,strCashAmount,strCashCollect,strDateRaised,strDelName,strOperator,strStatus,strOrderNum,strDelInst1,strDelInst2,strDeliveryRun,strDebtor,strCustOrder,strFuelLevy,strOrderValue,strTotalLines,stExchangeRate,strTotalCartonQuantity,strActive,strEditStatus,strTotalWeight,strTotalVolume,strTotalTax;

@synthesize strExchangeRate,strRefreshPrice,strBOQty,strConvFactor,strDecimalPlaces,strOrigCost,strUploadDate,strSuccessMessage,strOldOrderNum,isUploadInProgress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            btnUpload.enabled = NO;
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            btnUpload.enabled = YES;
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES)
    {
        btnUpload.enabled = YES;
    }
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == NO)
    {
        btnUpload.enabled = NO;
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    isSelectAll = NO;
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.strOperator = [prefs stringForKey:@"userName"];
    self.strOperatorName = [prefs objectForKey:@"members"];
    
    self.strPriceChange = @"N";
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrQuotes = [[NSMutableArray alloc] init];
    strQuoteNum = [[NSString alloc] init];
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    arrProducts = [[NSMutableArray alloc] init];
    arrSelectedOrders = [[NSMutableArray alloc] init];
    operationQueueUpload = [ASINetworkQueue new];
    
    backgroundQueueForUploadQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForUploadProfileOrders", NULL);
    
    if (isProfileOrder) {
        lblNoOrders.text = @"No Offline Profile Orders";
    }
    else{
        lblNoOrders.text = @"No Offline Sales Orders";
    }
    
    [self setDefaults];
    
    //First page
    currPage = 1;
    
    totalCount = 0;
    self.isUploadInProgress = NO;
    
    operationQueue = [NSOperationQueue new];
    NSLog(@"strOperator %@",self.strOperator);
    
    NSLog(@"SELECT COUNT(*) as totalCount FROM soheader_offline where SALESMAN = %@",self.strOperator);
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM soheader_offline where SALESMAN = ?",self.strOperator] ;
            
            
            if (!results)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                //return;
            }
            
            if ([results next]) {
                totalCount = [[[results resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [results close];
            
            [self callGetOrderListFromDB:db];

        }];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
    
    //Add fade in fade out animation to attract users attention
    [self addFadeInFadeOutAnimations];


    
}


- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.vwNoQuotes = nil;
    self.vwContentSuperview = nil;
    self.vwTblQuotes = nil;
    self.tblQuoteList = nil;
    self.lblStatusText = nil;
    self.btnDelete = nil;
    self.progessView = nil;
    self.btnUpload = nil;
    self.btnLoad = nil;
    self.lblNoOrders = nil;
}

#pragma mark - Cancel WS calls
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self removeFadeInFadeOutAnimations];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    [operationQueueUpload cancelAllOperations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.filteredListContent count];
    }
    else
    {
         NSLog(@"%lu",(unsigned long)[arrQuotes count]);
       // return [self.arrQuotes count];
        return [arrQuotes count];
       
    }
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
//    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//    
//    [databaseQueue   inDatabase:^(FMDatabase *db) {
//        [self callGetOrderListFromDB:db];
//    }];
   
    
   // NSMutableDictionary *billInfo=[[NSMutableDictionary alloc]init];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:7];
        
    }
    
    //cell.accessoryType = UITableViewCellAccessoryNone;
    
    UILabel *statusLbl = [[UILabel alloc]init];
    statusLbl.textColor = [UIColor whiteColor];
    statusLbl.backgroundColor = [UIColor clearColor];
    statusLbl.font = [UIFont boldSystemFontOfSize:14.0];
    statusLbl.text = @"";
    statusLbl.frame = CGRectMake(cell.lblCustomerName.frame.size.width + 50, cell.lblCustomerName.frame.origin.y, 70, 30);
    [cell.contentView addSubview:statusLbl];
    
    UIButton *button = nil;
    HJManagedImageV *cachedImgView = nil;
                cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
    BOOL isRunning = FALSE;
    if (cell.accessoryView!=nil) {
        button = (UIButton *)cell.accessoryView;
        NSLog(@"%d",[[button subviews] count]);
        
        // NSLog(@"%@",arrQuotes);
        NSLog(@"%@", cell.lblQuoteNo.text);
        
        if([[button subviews]count] > 0)
        {
            HJManagedImageV *cachedImgView = nil;
             cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
            
            
            if ([[[button subviews] objectAtIndex:0] isKindOfClass:[UIActivityIndicatorView class]]) {
                UIActivityIndicatorView *indicator = [[button subviews] objectAtIndex:0];
                if (!indicator.isAnimating) {
                    [indicator startAnimating];
                }
                else
                {
                    [indicator stopAnimating];
                }
                isRunning = TRUE;
            }
            else{
                
                isRunning = FALSE;
            }
            
        }
        else{
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(btnClciked:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = indexPath.row;
            
            cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
            isRunning = FALSE;
        }
        
    }
    else{
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(btnClciked:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = indexPath.row;
        
        cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
        isRunning = FALSE;
    }
    
    //    if (!isRunning)
    {
        [cachedImgView clear];
        [button removeAllSubviews];
    }
    
    // }

   
    
    if (![arrQuotes count])
    {
        
    }
    else
    {
    NSMutableDictionary *item =   [arrQuotes objectAtIndex:[indexPath row]];
    
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"DelName"]];
        cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"OrderNo"]];
        NSString *sentStatus= [NSString stringWithFormat:@"%@",[item objectForKey:@"SentStatus"]];
        if([sentStatus isEqualToString:@"1"]){
            statusLbl.text =@"Sent";
        }
        else{
            statusLbl.text =@"Not Sent";
            
        }
       //************ketaki code modified***************//
        [item setObject:cell forKey:@"cell"];
       [item setObject:indexPath forKey:@"cellIndexPath"];
      
        
        
        
        
        NSLog(@"item : %@",item);
//            if (cell.accessoryView!=nil) {
//                button = (UIButton *)cell.accessoryView;
//                NSLog(@"%d",[[button subviews] count]);
//
//               // NSLog(@"%@",arrQuotes);
//                NSLog(@"%@", cell.lblQuoteNo.text);
//                
//                if([[button subviews]count] > 0)
//                {
//                   
//               
//                
//                if ([[[button subviews] objectAtIndex:0] isKindOfClass:[UIActivityIndicatorView class]]) {
//                    UIActivityIndicatorView *indicator = [[button subviews] objectAtIndex:0];
//                    if (!indicator.isAnimating) {
//                        [indicator startAnimating];
//                    }
//                    else
//                    {
//                        [indicator stopAnimating];
//                    }
//                    isRunning = TRUE;
//                }
//                else{
//                
//                    isRunning = FALSE;
//                }
//                    
//                    }
//                else{
//                    button = [UIButton buttonWithType:UIButtonTypeCustom];
//                    [button addTarget:self action:@selector(btnClciked:) forControlEvents:UIControlEventTouchUpInside];
//                    button.tag = indexPath.row;
//                    
//                    cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
//                    isRunning = FALSE;
//                }
//                
//            }
//            else{
//                button = [UIButton buttonWithType:UIButtonTypeCustom];
//                [button addTarget:self action:@selector(btnClciked:) forControlEvents:UIControlEventTouchUpInside];
//                button.tag = indexPath.row;
//                
//                cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
//                isRunning = FALSE;
//            }
//            
//            //    if (!isRunning)
//            {
//                [cachedImgView clear];
//                [button removeAllSubviews];
//            }
//
//       // }
        
        
        //Error
        if ([[item objectForKey:@"isErrorWhileFinalise"] intValue] == 1) {
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"error" ofType:@"png"];
            cachedImgView.image=[UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
        }
        //Success
        else if ([[item objectForKey:@"isErrorWhileFinalise"] intValue] == 2) {
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
            cachedImgView.image=[UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
        }
        else{
            NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
            NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
            
            BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
            cachedImgView.image = (checked) ? [UIImage imageWithContentsOfFile:strImage1] : [UIImage imageWithContentsOfFile:strImage2];
            
            strImage1=nil;
            strImage2=nil;
        }
      
    }
    
        [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
        [button addSubview:cachedImgView];
        CGRect frame = CGRectMake(0.0, 0.0, 22, 22);
        button.frame = frame;	// match the button's size with the image size
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;
    
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.deleteIndexPath = indexPath;
    
    isSelectAll = NO;
    [_selectAllBtn setTitle:@"Select All" forState:UIControlStateNormal];
    [_selectAllBtn setTitle:@"Select All" forState:UIControlStateSelected];
    
    
    NSMutableDictionary *item = [arrQuotes objectAtIndex:[indexPath row]];
    
    //    NSString *strOrderNum = [item objectForKey:@"OrderNo"];
    //
    //
    //    if ([item objectForKey:@"errorMessage"]) {
    //
    //        NSString *strErrorMessage = [item objectForKey:@"errorMessage"];
    //        NSString *strUploadDateTime = [item objectForKey:@"UploadDate"];
    //
    //        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
    //
    //        dataViewController.strStatusNumber = @"-" ;
    //        dataViewController.strMessage = strErrorMessage;
    //        dataViewController.strUploadDate = strUploadDateTime;
    //        dataViewController.isOrder = YES;
    //        dataViewController.isError = YES;
    //
    //        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    //
    //    }
    //    else if ([item objectForKey:@"successMessage"]) {
    //
    //        NSString *strSuccMessage = [item objectForKey:@"successMessage"];
    //        NSString *strUploadDateTime = [item objectForKey:@"UploadDate"];
    //        NSString *strOrderNumber = [item objectForKey:@"orderNum"];
    //
    //        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
    //
    //        dataViewController.strStatusNumber = strOrderNumber;
    //        dataViewController.strMessage = strSuccMessage;
    //        dataViewController.strUploadDate = strUploadDateTime;
    //        dataViewController.isOrder = YES;
    //        dataViewController.isError = NO;
    //
    //        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    //    }
    //    else{
    //        if (!isUploadInProgress) {
    //            [self tableView: tblQuoteList accessoryButtonTappedForRowWithIndexPath: indexPath];
    //        }
    //
    //    }
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    NSLog(@"SELECTED ARRAY %@",arrSelectedOrders);
    //
    
    NSString *strOrderNumsel = [item objectForKey:@"OrderNo"];
    
    OfflineProfileDetailViewController *dataViewController = [[OfflineProfileDetailViewController alloc] initWithNibName:@"OfflineProfileDetailViewController" bundle:[NSBundle mainBundle]];
    dataViewController.strOrderNumSel = strOrderNumsel;
    NSLog(@"%@",strOrderNumsel);
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        self.deleteIndexPath = indexPath;
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            [self callDeleteQuoteFromDB:db RowToDelete:[indexPath row]];
        }];
        
        
    }
    
}

-(void)btnClciked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    //int tagVal = (integer_t )[sender tag];
    
    //int tagval = (int)[sender tag];
   
    
    NSMutableDictionary *item = [arrQuotes objectAtIndex:btn.tag];
    
    BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
   // [item setObject:[NSNumber numberWithBool:CHECKFLAG_NO] forKey:@"CheckFlag"];
    if (checked) {
        
        [arrSelectedOrders removeObject:item];
        
       
    }
    else{
        [arrSelectedOrders addObject:item];
        //[item setObject:[NSNumber numberWithBool:CHECKFLAG_YES] forKey:@"CheckFlag"];
    }
     [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
  
    
    UITableViewCell *cell = [item objectForKey:@"cell"];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    HJManagedImageV *cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
    [cachedImgView clear];
    [button removeAllSubviews];
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    if (checked) {
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage2];
    }
    else{
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage1];
    }
    
    strImage1 = nil;
    strImage2 = nil;
    
    [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
    [button addSubview:cachedImgView];
    
    if ([arrSelectedOrders count]) {
        btnUpload.enabled = TRUE;
    }
    else{
        btnUpload.enabled = FALSE;
    }
    
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES)
    {
        btnUpload.enabled = YES;
    }
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == NO)
    {
        btnUpload.enabled = NO;
    }
    
    
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    self.deleteIndexPath = indexPath;
    
    NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];;
    
    BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    if (checked) {
        [arrSelectedOrders removeObject:item];
    }
    else{
        [arrSelectedOrders addObject:item];
    }
    
    [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
    
    UITableViewCell *cell = [item objectForKey:@"cell"];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    HJManagedImageV *cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
    [cachedImgView clear];
    [button removeAllSubviews];
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    if (checked) {
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage2];
    }
    else{
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage1];
    }
    
    strImage1 = nil;
    strImage2 = nil;
    
    [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
    [button addSubview:cachedImgView];
    
    if ([arrSelectedOrders count]) {
        btnUpload.enabled = TRUE;
    }
    else{
        btnUpload.enabled = FALSE;
    }
    
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES)
    {
        btnUpload.enabled = YES;
    }
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == NO)
    {
        btnUpload.enabled = NO;
    }
    
    
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
            [_loadMoreFooterView setState:StateNormal];
            
        } else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
            [_loadMoreFooterView setState:StatePulling];
        }
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrQuotes count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 600.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    //[spinner showLoadingView:self.view size:2];
    
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetOrderListFromDB:db];
            
        }];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
}

- (void) didFinishLoadingMoreSampleData
{
    _loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            btnUpload.enabled = NO;
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            btnUpload.enabled = YES;
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            btnUpload.enabled = YES;
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
}
#pragma mark - Custom Methods
-(IBAction)actionUpload:(id)sender
{
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
        
        if ([arrSelectedOrders count] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Select atleast one order to send!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            return;
        }
        
        self.isUploadInProgress = YES;
        
        [self doBeforeUpload];
        
        [operationQueueUpload cancelAllOperations];
        [operationQueueUpload setUploadProgressDelegate:progessView];
        [operationQueueUpload setDelegate:self];
        [operationQueueUpload setQueueDidFinishSelector:@selector(queueComplete:)];
        [operationQueueUpload setMaxConcurrentOperationCount:1];
        
        for (int i=0; i<[arrSelectedOrders count]; i++) {
            
            //@autoreleasepool {
            self.strOldOrderNum = [[arrSelectedOrders objectAtIndex:i] objectForKey:@"OrderNo"];
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callViewOrderDetailsHeaderFromDB:db];
                [self callViewOrderDetailsDetailsFromDB:db];
                [self callGetOrderListFromDB:db];
            }];
            
            [self getStockPickupDetailInfoFromDB:self.strOldOrderNum];
            [self CallServiceForStockPickupEmail];
            
            NSString *soapXMLStr = (isProfileOrder)?[self callCreateSoapStringProfileOrder:0]:[self callCreateSoapStringSalesOrder:0];
            
            /*
             NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wws=\"%@\">"
             "<soapenv:Header/>"
             "<soapenv:Body>"
             "%@"
             "</soapenv:Body>"
             "</soapenv:Envelope>",WSURL_WSDL,soapXMLStr];
             
             NSLog(@"soapMessage %@ " , soapMessage);
             NSURL *url = [NSURL URLWithString:WSURL_WSDL];
             
             ASIHTTPRequest *request = [ASIFormDataRequest requestWithURL:url];
             
             [request setDelegate:self];
             [request addRequestHeader:@"Content-Type" value:@"text/xml; charset=utf-8"];
             [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%d",[soapMessage length]]];
             
             if (isProfileOrder) {
             [request addRequestHeader:@"SOAPAction" value:@"urn:wwservice#InsertProfOrder"];
             }
             else{
             [request addRequestHeader:@"SOAPAction" value:@"urn:wwservice#insertDebtorDetails"];
             }
             
             [request setPostLength:[soapMessage length]];
             
             [request setRequestMethod:@"POST"];
             [request setPostBody:[[soapMessage dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
             
             
             NSIndexPath *selectedIndexPath = [[arrSelectedOrders objectAtIndex:i] objectForKey:@"cellIndexPath"];
             
             */
            
            NSIndexPath *selectedIndexPath = [[arrSelectedOrders objectAtIndex:i] objectForKey:@"cellIndexPath"];
            NSString *strRequest=[AppDelegate getServiceURL:@"webservices_rcf/insert_modules.php"];
            
            NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
            ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request setTimeOutSeconds:300];
            [request setPostValue:soapXMLStr forKey:@"xmlData"];
            [request setPostValue:@"1" forKey:@"transactionType"];
            NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
            [dictInfo setObject:[[arrSelectedOrders objectAtIndex:i] objectForKey:@"OrderNo"] forKey:@"OrderNo"];
            [dictInfo setObject:[NSString stringWithFormat:@"%d",i ] forKey:@"RowNumber"];
            request.userInfo = dictInfo;
            request.tag = selectedIndexPath.row;
            dictInfo = nil;
            // [request startAsynchronous];
            NSLog(@"selectedIndexPath.row %d",selectedIndexPath.row);
            strWebserviceType = @"NewChangedWebservice";
            //Write data in plist
            NSString *strHeaderFileName = [NSString stringWithFormat:@"soheader%d.plist",selectedIndexPath.row];
            NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:strHeaderFileName];
            [dictHeaderDetails writeToFile:filePath atomically: YES];
            strHeaderFileName = nil;
            
            [operationQueueUpload addOperation:request];
            
            CustomCellGeneral *cell = (CustomCellGeneral *)[tblQuoteList cellForRowAtIndexPath:selectedIndexPath];
        
            
            soapXMLStr = nil;
            request = nil;
             NSLog(@"%@",self.arrQuotes);
        
             NSLog(@"%@",self.arrQuotes);
           UIButton *button = (UIButton *)cell.accessoryView;
            [button removeAllSubviews];
            
            localSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [localSpinner setCenter:CGPointMake(button.frame.size.width / 2,button.frame.size.height /2)];
            [button addSubview:localSpinner];
            [button bringSubviewToFront:localSpinner];
            NSLog(@"%d",[[button subviews] count]);
            [localSpinner startAnimating];
           
            [arrQuotes removeObjectAtIndex:selectedIndexPath.row];
//            // [tblQuoteList deleteRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:nil];
//            [tblQuoteList reloadData];
           
        }
        
        
    }
    
    [operationQueueUpload go];
    
    self.timerForUpload = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(loadProgress) userInfo:nil repeats:YES];
    
    
    // }
    //  else{
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You are offline!\nUploading requires internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //    [alert show];
    //  }
}

-(void)loadProgress{
    // NSLog(@"Max: %f", [self.progessView progress]);
}
- (void)queueComplete:(ASINetworkQueue *)queue
{
    NSLog(@"Queue complete");
    //NSLog(@"Max: %f, Value: %f", [self.progessView maxValue],[self.progessView doubleValue]);
    
    [timerForUpload invalidate];
    timerForUpload=nil;
    lblStatusText.text = UPLOAD_AFTER_STATUS_MESSAGE;
    
    [self performSelector:@selector(setProgressBarHidden) withObject:nil afterDelay:1.0];
    
}

- (void)setProgressBarHidden{
    
    self.isUploadInProgress = NO;
    progessView.hidden = YES;
    btnDelete.enabled = TRUE;
    _selectAllBtn.enabled = TRUE;
    btnUpload.enabled = FALSE;
    [arrSelectedOrders removeAllObjects];
    lblStatusText.text = UPLOAD_BEFORE_STATUS_MESSAGE_WITHOUT_LONG_PRESS;
    
    [self addFadeInFadeOutAnimations];
}

- (void)doBeforeUpload{
    [self removeFadeInFadeOutAnimations];
    
    [progessView setProgress:0.0];
    progessView.hidden = FALSE;
    btnDelete.enabled = FALSE;
    btnUpload.enabled = FALSE;
    _selectAllBtn.enabled = FALSE;
    
    lblStatusText.text = UPLOAD_RUNNING_STATUS_MESSAGE;
    
}

- (void)doAfterDataFetched{
    if ([arrQuotes count]) {
        
        lblStatusText.text = UPLOAD_BEFORE_STATUS_MESSAGE_WITHOUT_LONG_PRESS;
        
        btnUpload.hidden = false;
        btnDelete.hidden = NO;
        _selectAllBtn.hidden = NO;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwTblQuotes];
        
        if([arrQuotes count] < totalCount){
            [self repositionLoadMoreFooterView];
            
            // Dismiss loading footer
            [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
            recordNumber += ROWS_PER_PAGE;
            
//            [tblQuoteList reloadData];
            
        }
        else{
            
            [_loadMoreFooterView removeFromSuperview];
            _loadMoreFooterView=nil;
        }
        [tblQuoteList reloadData];

    }
    else{
        lblStatusText.text = @"";
        btnUpload.hidden = true;
        btnDelete.hidden = YES;
        _selectAllBtn.hidden = YES;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoQuotes];
    }
    
}



-(void)doAfterdelete{
    
    if ([arrQuotes count] == 0) {
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoQuotes];
        btnDelete.hidden = YES;
        btnUpload.hidden = YES;
        lblStatusText.text = @"";
    }
    else{
        btnUpload.enabled = YES;
    }
    
}

-(void)showErrorMessage:(id)sender event:(id)event{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
    NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];
        NSString *strOrderNum = [item objectForKey:@"OrderNo"];
        NSString *strErrorMessage = [item objectForKey:@"errorMessage"];
        NSString *strUploadDate = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum ;
        dataViewController.strMessage = strErrorMessage;
        dataViewController.strUploadDate = strUploadDate;
        dataViewController.isError = YES;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
    
}

-(void)showSuccessMessage:(id)sender event:(id)event{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
    NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];
        NSString *strSuccessMessage1 = [item objectForKey:@"successMessage"];
        NSString *strOrderNum1 = [item objectForKey:@"OrderNo"];
        NSString *strUploadDate1 = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum1 ;
        dataViewController.strMessage = strSuccessMessage1;
        dataViewController.strUploadDate = strUploadDate1;
        dataViewController.isError = NO;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
    
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
    NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView: tblQuoteList accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (void)removeFadeInFadeOutAnimations{
    [lblStatusText.layer removeAllAnimations];
}

- (void)addFadeInFadeOutAnimations{
    [lblStatusText.layer addAnimation:[[AppDelegate getAppDelegateObj] fadeInFadeOutAnimation]
                               forKey:@"animateOpacity"];
}

- (void)setDefaults{
    self.strExchangeRate = @"1.000000";
    self.strActive = @"N";
    self.strEditStatus = @"11";
    self.strStatus = @"1";
    self.strRefreshPrice = @"N";
    self.strConvFactor = @"1.0000";
    self.strDecimalPlaces = @"2";
    self.strTotalCartonQuantity = self.strBOQty = self.strOrigCost = @"0.0000";
    self.strTotalLines = self.strTotalTax = self.strTotalVolume = self.strTotalWeight = @"0";
}


#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    /*
     Update the filtered array based on the search text and scope.
     */
    
    [self.filteredListContent removeAllObjects]; // First clear the filtered array.
    
    /*
     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    if ([scope isEqualToString:@"OrderNum"])
    {
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetOrderListByOrderNumFromDB:db OrderNum:searchText];
                
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [tblQuoteList reloadData];
            }];
        }];
        
    }
    if ([scope isEqualToString:@"DebtorName"])
    {
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetOrderListByOrderNumFromDB:db OrderNum:searchText];
                
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [tblQuoteList reloadData];
            }];
        }];
        
    }
    
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller{
    /*
     Bob: Because the searchResultsTableView will be released and allocated automatically, so each time we start to begin search, we set its delegate here.
     */
    [self.searchDisplayController.searchResultsTableView setDelegate:self];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    /*
     Hide the search bar
     */
    //[tblQuoteList setContentOffset:CGPointMake(0, 44.f) animated:YES];
}

#pragma mark - IBActions
- (IBAction)actionSendAll:(id)sender
{
    UIButton *selectAllBtn = (UIButton *)sender;
    // **
    if (isSelectAll == NO) {
        isSelectAll = YES;
        NSLog(@"send all");
        [selectAllBtn setTitle:@"Deselect All" forState:UIControlStateNormal];
        [selectAllBtn setTitle:@"Deselect All" forState:UIControlStateSelected];
        
        if(arrSelectedOrders.count > 0){
            [arrSelectedOrders removeAllObjects];
        }
        for (int i = 0; i< arrQuotes.count ;i++ ) {
            NSMutableDictionary *item = [arrQuotes objectAtIndex:i];
            
            BOOL checked = NO;
            
            if (checked) {
                [arrSelectedOrders removeObject:item];
            }
            else{
                [arrSelectedOrders addObject:item];
            }
            [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
            
            UITableViewCell *cell = [item objectForKey:@"cell"];
            UIButton *button = (UIButton *)cell.accessoryView;
            
            HJManagedImageV *cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
            [cachedImgView clear];
            [button removeAllSubviews];
            
            NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
            NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
            
            if (checked) {
                cachedImgView.image = [UIImage imageWithContentsOfFile:strImage2];
            }
            else{
                cachedImgView.image = [UIImage imageWithContentsOfFile:strImage1];
            }
            
            strImage1 = nil;
            strImage2 = nil;
            
            [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
            [button addSubview:cachedImgView];
            
            if ([arrSelectedOrders count]) {
                btnUpload.enabled = TRUE;
            }
            else{
                btnUpload.enabled = FALSE;
            }
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES)
            {
                btnUpload.enabled = YES;
            }
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == NO)
            {
                btnUpload.enabled = NO;
            }
            
            
        }
    }
    else{
        isSelectAll = NO;
        
        NSLog(@"Deselect all");
        [selectAllBtn setTitle:@"Select All" forState:UIControlStateNormal];
        [selectAllBtn setTitle:@"Select All" forState:UIControlStateSelected];
        if(arrSelectedOrders.count > 0){
            [arrSelectedOrders removeAllObjects];
        }
        for (int i = 0; i< arrQuotes.count ;i++ ) {
            NSMutableDictionary *item = [arrQuotes objectAtIndex:i];
            
            //            BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
            BOOL checked = YES;
            
            if (checked) {
                [arrSelectedOrders removeObject:item];
            }
            else{
                [arrSelectedOrders addObject:item];
            }
            [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
            
            UITableViewCell *cell = [item objectForKey:@"cell"];
            UIButton *button = (UIButton *)cell.accessoryView;
            
            HJManagedImageV *cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
            [cachedImgView clear];
            [button removeAllSubviews];
            
            NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
            NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
            
            if (checked) {
                cachedImgView.image = [UIImage imageWithContentsOfFile:strImage2];
            }
            else{
                cachedImgView.image = [UIImage imageWithContentsOfFile:strImage1];
            }
            
            strImage1 = nil;
            strImage2 = nil;
            
            [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
            [button addSubview:cachedImgView];
            
            if ([arrSelectedOrders count]) {
                btnUpload.enabled = TRUE;
            }
            else{
                btnUpload.enabled = FALSE;
            }
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES)
            {
                btnUpload.enabled = YES;
            }
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == NO)
            {
                btnUpload.enabled = NO;
            }
        }
    }
    NSLog(@"Finalllll SELECTED ARRAY %@",arrSelectedOrders);
    
    [tblQuoteList reloadData];
}

//No
- (IBAction)actionDismissTipView:(UIButton *)sender{
    // [popTipView dismissAnimated:YES];
}

- (void)actionShowQuoteDetails:(UIButton *)sender{
    NSString *strQuoteNum1 = [[[arrQuotes objectAtIndex:sender.tag] objectForKey:@"OrderNo"] objectForKey:@"text"];
    
    ViewQuoteDetailsViewController *dataViewController = [[ViewQuoteDetailsViewController alloc] initWithNibName:@"ViewQuoteDetailsViewController" bundle:[NSBundle mainBundle]];
    
    dataViewController.strQuoteNum = strQuoteNum1;
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}


//Delegate Method
- (void)actionSaveQuote:(int)finalizeTag{
    
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts) {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        if ([dict objectForKey:@"Item"]){
            
            if (![dict objectForKey:@"ExtnPrice"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = FALSE;
                break;
            }
            else if (![dict objectForKey:@"ExtnPrice"]&& ![[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = TRUE;
                //  break;
            }
            else if ([dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = FALSE;
                break;
            }
            else{
                isCalculationPending = FALSE;
            }
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
}


//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([tblQuoteList isEditing]) {
        // [self showOrEnableButtons];
        
        btnUpload.enabled = TRUE;
        
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblQuoteList setEditing:NO animated:YES];
    }
    else{
        
        btnUpload.enabled = FALSE;
        
        // [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblQuoteList setEditing:YES animated:YES];
    }
    
    [tblQuoteList reloadData];
}

#pragma mark - Database calls
-(void)callGetOrderListFromDB:(FMDatabase *)db
{
    [arrQuotes removeAllObjects ];
    FMResultSet *rs;
    @try {
        
        //Profile order
        if (isProfileOrder) {
            rs = [db executeQuery:@"SELECT ORDER_NO as OrderNo, DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun,isEdited,SentStatus FROM soheader_offline WHERE isProfileOrder =1 LIMIT ?,?",[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        }
        //Sales Order
        else{
            rs =  [db executeQuery:@"SELECT ORDER_NO as OrderNo, DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun,isEdited,SentStatus FROM soheader_offline WHERE isProfileOrder =0 LIMIT ?,?",[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        }
        
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrQuotes addObject:dictData];
            
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];    }
}

-(void)callGetOrderListByOrderNumFromDB:(FMDatabase *)db OrderNum:(NSString *)order{
    
    FMResultSet *rs;
    
    @try {
        
        
        rs = [db executeQuery:@"SELECT ORDER_NO as OrderNo,DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun FROM soheader_offline WHERE ORDER_NO LIKE ?",[NSString stringWithFormat:@"%%%@%%", order]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)callGetOrderListByDebtorNameFromDB:(FMDatabase *)db Debtor:(NSString *)debtor{
    
    FMResultSet *rs;
    
    @try {
        
        rs = [db executeQuery:@"SELECT ORDER_NO as OrderNo,DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun FROM soheader_offline WHERE DEBTOR LIKE ?",[NSString stringWithFormat:@"%%%@%%", debtor]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)callViewOrderDetailsHeaderFromDB:(FMDatabase *)db{
    FMResultSet *rs1;
    @try {
        
        rs1 = [db executeQuery:@"SELECT ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,STOCK_RETURNS,PERIOD_RAISED,YEAR_RAISED,ORDER_VALUE,DETAIL_LINES,EXCHANGE_RATE,TOTL_CARTON_QTY,ACTIVE,EDIT_STATUS,DETAIL_LINES,WEIGHT,VOLUME,TAX,TAX_AMOUNT1,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,STOCK_RETURNS,PICKUP_INFO,PriceChnaged FROM soheader_offline WHERE ORDER_NO=?",strOldOrderNum];
        
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next])
        {
            self.dictHeaderDetails = (NSMutableDictionary *) [rs1 resultDictionary];
            [self prepareHeaderData];
        }
        
        [rs1 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        NSLog(@"%@",strErrorMessage);
        
    }
}

-(void)callViewOrderDetailsDetailsFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_QUOTE_DETAILS_DETAILS";
    [arrProducts removeAllObjects];

    
//    [arrProducts removeAllObjects];
    
    FMResultSet *rs2;
    
    @try {
        
        //Get Details details
        rs2 = [db executeQuery:@"SELECT ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO,PriceChnaged FROM sodetail_offline WHERE ORDER_NO = ?",strOldOrderNum];
        
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        

        while ([rs2 next]) {
            NSDictionary *dictData = [rs2 resultDictionary];
            [arrProducts addObject:dictData];
        }
        
        NSLog(@"arrProducts %@",arrProducts);
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        NSLog(@"%@",strErrorMessage);
        
    }

  //[self makeHTML];

}


-(void)callDeleteQuoteFromDB:(FMDatabase*)db RowToDelete:(int)rowToDelete
{
    NSString *strQuote = [[arrQuotes objectAtIndex:rowToDelete] objectForKey:@"OrderNo"];
    
    
    @try {
        
        [db executeUpdate:@"DELETE FROM `soheader_offline` WHERE ORDER_NO = ?",strQuote];
        [db executeUpdate:@"DELETE FROM `sodetail_offline` WHERE ORDER_NO = ?",strQuote];
        
        [arrQuotes removeObjectAtIndex:rowToDelete];
        
        [tblQuoteList beginUpdates];
        [tblQuoteList deleteRowsAtIndexPaths:[NSArray arrayWithObject:deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tblQuoteList endUpdates];
        
        [self doAfterdelete];
        
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)callInsertProfileOrderToDB:(int)tag{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *strFileName = [NSString stringWithFormat:@"soheader%d.plist",tag];
        NSString *strFilePath = [AppDelegate getFileFromDocumentsDirectory:strFileName];
        
        NSDictionary *plistDict = [NSDictionary dictionaryWithContentsOfFile:strFilePath];
        
        NSString *strOldOrderNo = [plistDict objectForKey:@"ORDER_NO"];
        NSString *strNewOrderNo = [plistDict objectForKey:@"NEW_ORDER_NO"];
        NSString *strNewUploadDate = [plistDict objectForKey:@"UPLOAD_DATE"];
        NSString *strNewSuccessMessage = [plistDict objectForKey:@"SUCCESS_MESSAGE"];
        NSString *EmailStatus = 0;
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            BOOL y2 = [db executeUpdate:@"INSERT INTO `soheader` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,STOCK_RETURNS, PERIOD_RAISED,YEAR_RAISED,EXCHANGE_RATE,TOTL_CARTON_QTY,ACTIVE,EDIT_STATUS,ORDER_VALUE,DETAIL_LINES) SELECT ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,STOCK_RETURNS, PERIOD_RAISED,YEAR_RAISED,EXCHANGE_RATE,TOTL_CARTON_QTY,ACTIVE,EDIT_STATUS,ORDER_VALUE,DETAIL_LINES,PriceChnaged FROM soheader_offline WHERE ORDER_NO = ?",strOldOrderNo];
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            BOOL y3 = [db executeUpdate:@"UPDATE `soheader` SET ORDER_NO = ?,created_date = ?,modified_date = ? WHERE ORDER_NO = ?",strNewOrderNo,strNewUploadDate,strNewUploadDate,strOldOrderNo];
            
            if (!y3)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            for (NSMutableDictionary *dictProductDetails in arrProducts) {
                
                NSString *strStockCode = nil;
                NSString *strQuantityOrdered = @"0";
                
                //Comments
                if ([dictProductDetails objectForKey:@"ITEM"] && [[dictProductDetails objectForKey:@"ITEM"] isEqualToString:@"/C"]){
                    
                    strStockCode = @"/C";
                    //                    strDescription = [dictProductDetails objectForKey:@"DESCRIPTION"];
                    
                }
                //Freight
                else if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"]){
                    strStockCode = @"/F";
                }
                
                //Product
                else{
                    strStockCode = [dictProductDetails objectForKey:@"ITEM"];
                    strQuantityOrdered = [dictProductDetails objectForKey:@"QUANTITY"];
                }
                
                BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO) SELECT ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO FROM sodetail_offline WHERE ORDER_NO = ?",strOldOrderNo];
                
                
                if (!y1)
                {
                    isSomethingWrongHappened = TRUE;
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                BOOL y3 = [db executeUpdate:@"UPDATE `sodetail` SET ORDER_NO = ?,created_date = ?,modified_date = ? WHERE ORDER_NO = ?",strNewOrderNo,strNewUploadDate,strNewUploadDate,strOldOrderNo];
                
                if (!y3)
                {
                    isSomethingWrongHappened = TRUE;
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                /*
                 if (![strStockCode isEqualToString:@"/C"] && ![strStockCode isEqualToString:@"/F"]) {
                 BOOL y2 = [db executeUpdate:@"UPDATE `sastkloc` SET  QUANTITY= ?, ALLOCATED =(SELECT ALLOCATED + QUANTITY FROM sastkloc WHERE STOCK_CODE = ? AND WAREHOUSE = ?) WHERE STOCK_CODE = ? AND WAREHOUSE = ?",strQuantityOrdered,strStockCode,strWarehouse,strStockCode,strWarehouse];
                 
                 if (!y2) {
                 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                 }
                 
                 BOOL y3 = [db executeUpdate:@"UPDATE `samaster` SET ALLOCATED =(SELECT ALLOCATED + ? FROM samaster WHERE CODE = ? AND WAREHOUSE = ?) WHERE CODE = ? AND WAREHOUSE = ?",strQuantityOrdered,strStockCode,strWarehouse,strStockCode,strWarehouse];
                 
                 if (!y3) {
                 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                 }
                 
                 BOOL y4 = [db executeUpdate:@"UPDATE `samaster` SET AVAILABLE =(SELECT ON_HAND - ALLOCATED FROM samaster WHERE CODE = ? AND WAREHOUSE = ?) WHERE CODE = ? AND WAREHOUSE = ?",strStockCode,strWarehouse,strStockCode,strWarehouse];
                 
                 if (!y4) {
                 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                 }
                 
                 }
                 */
                
            }
            
            *rollback = NO;
            
            [db executeUpdate:@"DELETE FROM `soheader_offline` WHERE ORDER_NO = ?",strOldOrderNo];
            [db executeUpdate:@"DELETE FROM `sodetail_offline` WHERE ORDER_NO = ?",strOldOrderNo];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath: strFilePath error:NULL];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSMutableDictionary *item = [arrQuotes objectAtIndex:tag];
                [item setValue:[NSNumber numberWithInt:2] forKey:@"isErrorWhileFinalise"];
                [item setValue:strNewSuccessMessage forKey:@"successMessage"];
                [item setValue:strNewOrderNo forKey:@"orderNum"];
                
                [item setValue:strNewUploadDate forKey:@"UploadDate"];
                //Remove Stuffs
                [item removeObjectForKey:@"CheckFlag"];
                
                UITableViewCell *cell = [item objectForKey:@"cell"];
                UIButton *button = (UIButton *)cell.accessoryView;
                
                [button removeAllSubviews];
                
                HJManagedImageV *cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
                cachedImgView.image = [UIImage imageWithContentsOfFile:strImage];
                strImage = nil;
                
                [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
                [button addSubview:cachedImgView];
                [arrSelectedOrders removeObject:item];
                
                [self doAfterdelete];
                
            });
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            NSLog(@"%@",strErrorMessage);
            
        }
        
    }];
}

#pragma mark - Server Calls

-(void)prepareHeaderData{
    
    //Debtor's Info
    if ([dictHeaderDetails objectForKey:@"ORDER_VALUE"]) {
        self.strOrderValue = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ORDER_VALUE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DETAIL_LINES"]) {
        self.strTotalLines = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DETAIL_LINES"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_NAME"]) {
        self.strDelName = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_NAME"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"OPERATOR"]) {
        self.strOperator = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"OPERATOR"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"WAREHOUSE"]&&![[dictHeaderDetails objectForKey:@"WAREHOUSE"] isEqual:[NSNull null]]) {
        self.strWarehouse = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"WAREHOUSE"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"STATUS"]) {
        self.strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"STATUS"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ORDER_NO"]) {
        self.strOrderNum = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ORDER_NO"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_INST1"]) {
        self.strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_INST1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_INST2"]) {
        self.strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_INST2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DELIVERY_RUN"]) {
        self.strDeliveryRun = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DELIVERY_RUN"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEBTOR"]) {
        self.strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEBTOR"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DATE_RAISED"]) {
        self.strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DATE_RAISED"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CUST_ORDER"]) {
        self.strCustOrder = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CUST_ORDER"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_AMOUNT"]) {
        self.strCashAmount = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_AMOUNT"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_COLLECT"]) {
        self.strCashCollect = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_COLLECT"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"]) {
        self.strCashRepPickUp = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"DEL_DATE"]) {
        self.strDeliveryDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_DATE"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CONTACT"]) {
        self.strContact = [[dictHeaderDetails objectForKey:@"CONTACT"] trimSpaces];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS1"]) {
        self.strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS2"]) {
        self.strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS2"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS3"]) {
        self.strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_SUBURB"]) {
        self.strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_SUBURB"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ALLOW_PARTIAL"]) {
        self.strAllowPartial = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ALLOW_PARTIAL"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"BRANCH"]) {
        self.strBranch = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"BRANCH"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CARRIER_CODE"]) {
        self.strCarrier = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CARRIER_CODE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CHARGE_RATE"]) {
        self.strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_RATE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CHARGE_TYPE"]) {
        self.strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_TYPE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_COUNTRY"]) {
        self.strCountry = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_COUNTRY"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DIRECT"]) {
        self.strDirect = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DIRECT"]];
        
    }
    if ([dictHeaderDetails objectForKey:@"DROP_SEQ"]) {
        self.strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DROP_SEQ"]];
    }
    if ([dictHeaderDetails objectForKey:@"EXCHANGE_CODE"]) {
        self.strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EXCHANGE_CODE"]];
    }
    if ([dictHeaderDetails objectForKey:@"EXPORT_DEBTOR"]) {
        self.strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EXPORT_DEBTOR"]];
    }
    if ([dictHeaderDetails objectForKey:@"HELD"]) {
        self.strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"HELD"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"NUM_BOXES"]) {
        self.strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"NUM_BOXES"]];
    }
    if ([dictHeaderDetails objectForKey:@"PERIOD_RAISED"]) {
        self.strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PERIOD_RAISED"]];
    }
    if ([dictHeaderDetails objectForKey:@"DEL_POST_CODE"]) {
        self.strPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_POST_CODE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"PRICE_CODE"]) {
        self.strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PRICE_CODE"]];
    }
    if ([dictHeaderDetails objectForKey:@"SALES_BRANCH"]) {
        self.strSalesBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"SALES_BRANCH"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"SALESMAN"]) {
        self.strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SALESMAN"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION1"]) {
        self.strTaxExemption1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION1"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION2"]) {
        self.strTaxExemption2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION2"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION3"]) {
        self.strTaxExemption3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION3"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION4"]) {
        self.strTaxExemption4 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION4"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION5"]) {
        self.strTaxExemption5 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION5"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION6"]) {
        self.strTaxExemption6 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION6"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TRADING_TERMS"]) {
        self.strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TRADING_TERMS"]];
    }
    if ([dictHeaderDetails objectForKey:@"YEAR_RAISED"]) {
        self.strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YEAR_RAISED"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"STOCK_RETURNS"]){
        self.strStockPickup = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"STOCK_RETURNS"]];
    }
    //    strPickupInfo
    //    PICKUP_INFO
    if ([dictHeaderDetails objectForKey:@"PICKUP_INFO"]){
        self.strPickupInfo = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PICKUP_INFO"]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"EXCHANGE_RATE"]){
        self.stExchangeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EXCHANGE_RATE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"TOTL_CARTON_QTY"]){
        self.strTotalCartonQuantity = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TOTL_CARTON_QTY"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ACTIVE"]){
        self.strActive = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ACTIVE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"EDIT_STATUS"]){
        self.strEditStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EDIT_STATUS"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"WEIGHT"]){
        self.strTotalWeight = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"WEIGHT"]];
    }
    if ([dictHeaderDetails objectForKey:@"VOLUME"]){
        self.strTotalVolume = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"VOLUME"]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX"]){
        self.strTotalTax = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TAX"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"PriceChnaged"]){
        self.strPriceChange = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceChnaged"]];
    }
    
}

/*
 -(NSString *)callCreateSoapStringProfileOrder:(int)finalizeTag
 {
 strWebserviceType = @"WS_PROFILE_ORDER_SAVE";
 
 NSString *strItem = [[NSString alloc] init];
 
 for (NSDictionary *dict in arrProducts) {
 
 NSString *strStockCode = nil;
 NSString *strPrice = @"0";
 NSString *strDescription = nil;
 NSString *strExtension = @"0";
 NSString *strDissection = @"0";
 NSString *strDissection_Cos = @"0";
 NSString *strWeight = @"0";
 NSString *strVolume = @"0";
 NSString *strCost = @"0";
 NSString *strQuantityOrdered = @"0";
 NSString *strGross = @"0";
 NSString *strTax = @"0";
 NSString *strLineNumber = @"0";
 
 
 if ([dict objectForKey:@"LINE_NO"]) {
 strLineNumber = [dict objectForKey:@"LINE_NO"];
 }
 
 //Comments
 if ([dict objectForKey:@"ITEM"] && [[dict objectForKey:@"ITEM"] isEqualToString:@"/C"]){
 
 strStockCode = @"/C";
 strDescription = [dict objectForKey:@"DESCRIPTION"];
 
 }
 //Freight
 else if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
 strStockCode = @"/F";
 strDescription = [dict objectForKey:@"Description"];
 strPrice = [dict objectForKey:@"Price"];
 strExtension = [dict objectForKey:@"ExtnPrice"];
 strTax = [dict objectForKey:@"Tax"];
 
 }
 
 //Product
 else{
 strStockCode = [dict objectForKey:@"ITEM"];
 strPrice = [dict objectForKey:@"PRICE"];
 strDissection = [dict objectForKey:@"DISSECTION"];
 strDissection_Cos = [dict objectForKey:@"DISSECTION_COS"];
 strWeight = [dict objectForKey:@"WEIGHT"];
 strVolume = [dict objectForKey:@"VOLUME"];
 
 strDescription = [dict objectForKey:@"DESCRIPTION"];
 strCost = [dict objectForKey:@"COST"];
 
 if ([dict objectForKey:@"EXTENSION"]) {
 strExtension = [dict objectForKey:@"EXTENSION"];
 }
 
 strQuantityOrdered = [dict objectForKey:@"QUANTITY"];
 strVolume = [dict objectForKey:@"VOLUME"];
 strGross = [dict objectForKey:@"GROSS"];
 strTax = [dict objectForKey:@"TAX"];
 
 }
 
 
 strItem = [strItem stringByAppendingFormat:@"<product><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><ORIG_ORD_QTY>%@</ORIG_ORD_QTY><ORD_QTY>%@</ORD_QTY><BO_QTY>%@</BO_QTY><CONV_FACTOR>%@</CONV_FACTOR><REFRESH_PRICE>%@</REFRESH_PRICE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><COST>%@</COST><ORIG_COST>0.0000</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><CREATE_OPERATOR>%@</CREATE_OPERATOR><EDIT_STATUS>%@</EDIT_STATUS><DECIMAL_PLACES>%@</DECIMAL_PLACES><LINE_NO>%@</LINE_NO><RELEASE_New>Y</RELEASE_New></product>",strStockCode,strDescription,strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,strConvFactor,strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strGross,strTax,strOperator,strEditStatus,strDecimalPlaces,strLineNumber];
 }
 
 
 NSString *soapXMLStr = [NSString stringWithFormat:
 @"<wws:InsertProfOrder soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
 "<profOrdHeader xsi:type=\"wws:ProfOrdHeaderRequest\">"
 "<DEBTOR>%@</DEBTOR>"
 "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"//editable
 "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"//editable
 "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"//editable
 "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"//editable
 "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"//editable
 "<DEL_POST_CODE>%@</DEL_POST_CODE>"//editable
 "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"//editable
 "<CONTACT>%@</CONTACT>"//editable
 "<DEL_DATE><![CDATA[%@]]></DEL_DATE>"
 "<DATE_RAISED><![CDATA[%@]]></DATE_RAISED>"//todays date
 "<CUST_ORDER>%@</CUST_ORDER>"//editable
 "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"//editable
 "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"//editable
 "<STATUS><![CDATA[%@]]></STATUS>"//always 1
 "<DIRECT><![CDATA[%@]]></DIRECT>"
 "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
 "<BRANCH><![CDATA[%@]]></BRANCH>"
 "<PRICE_CODE><![CDATA[%@]]></PRICE_CODE>"
 "<SALESMAN><![CDATA[%@]]></SALESMAN>"
 "<SALES_BRANCH><![CDATA[%@]]></SALES_BRANCH>"
 "<TRADING_TERMS><![CDATA[%@]]></TRADING_TERMS>"
 "<DELIVERY_RUN><![CDATA[%@]]></DELIVERY_RUN>"
 "<TAX_EXEMPTION1><![CDATA[%@]]></TAX_EXEMPTION1>"
 "<TAX_EXEMPTION2><![CDATA[%@]]></TAX_EXEMPTION2>"
 "<TAX_EXEMPTION3><![CDATA[%@]]></TAX_EXEMPTION3>"
 "<TAX_EXEMPTION4><![CDATA[%@]]></TAX_EXEMPTION4>"
 "<TAX_EXEMPTION5><![CDATA[%@]]></TAX_EXEMPTION5>"
 "<TAX_EXEMPTION6><![CDATA[%@]]></TAX_EXEMPTION6>"
 "<EXCHANGE_CODE><![CDATA[%@]]></EXCHANGE_CODE>"
 "<ALLOW_PARTIAL><![CDATA[%@]]></ALLOW_PARTIAL>"
 "<CHARGE_TYPE><![CDATA[%@]]></CHARGE_TYPE>"
 "<CARRIER_CODE><![CDATA[%@]]></CARRIER_CODE>"
 "<EXPORT_DEBTOR><![CDATA[%@]]></EXPORT_DEBTOR>"
 "<DROP_SEQ><![CDATA[%@]]></DROP_SEQ>"
 "<HELD><![CDATA[%@]]></HELD>"
 "<CHARGE_RATE><![CDATA[%@]]></CHARGE_RATE>"
 "<NUM_BOXES><![CDATA[%@]]></NUM_BOXES>"
 "<CASH_COLLECT><![CDATA[%@]]></CASH_COLLECT>"
 "<CASH_AMOUNT><![CDATA[%@]]></CASH_AMOUNT>"
 "<CASH_REP_PICKUP><![CDATA[%@]]></CASH_REP_PICKUP>"
 "<OPERATOR><![CDATA[%@]]></OPERATOR>"
 "<CREATED_BY><![CDATA[%@]]></CREATED_BY>"
 "<STOCK_RETURNS><![CDATA[%@]]></STOCK_RETURNS>"
 "<PERIOD_RAISED><![CDATA[%@]]></PERIOD_RAISED>"
 "<YEAR_RAISED><![CDATA[%@]]></YEAR_RAISED>"
 "<DETAIL_LINES><![CDATA[%@]]></DETAIL_LINES>"
 "<ORDER_VALUE><![CDATA[%@]]></ORDER_VALUE>"
 "<EXCHANGE_RATE>%@</EXCHANGE_RATE>" // Static value
 "<TOTL_CARTON_QTY>%@</TOTL_CARTON_QTY>" // Static value
 "<ACTIVE>%@</ACTIVE>" // Static value
 "<EDIT_STATUS>%@</EDIT_STATUS>" // Static value
 "<WEIGHT>%@</WEIGHT>"
 "<VOLUME>%@</VOLUME>"
 "<TAX>%@</TAX>"
 "<TAX_AMOUNT1>%@</TAX_AMOUNT1>"
 "</profOrdHeader>"
 "<profOrdDetails xsi:type=\"wws:ProfOrdDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
 "%@"
 "</profOrdDetails>"
 "</wws:InsertProfOrder>",strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strPostCode,strCountry,strContact,strDeliveryDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,strCashCollect,strCashAmount,strCashRepPickUp,strOperator,self.strOperatorName,strStockPickup,strPeriodRaised,strYearRaised,strTotalLines,strOrderValue,stExchangeRate,strTotalCartonQuantity,strActive,strEditStatus,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strItem];
 
 NSLog(@"soapXMLStr %@",soapXMLStr);
 
 return soapXMLStr;
 }
 
 */

#pragma mark - chaitanya
-(void)makeHTML
{

  NSString *strProducts = @"";
  NSString *strProductshtml = @"";

  //Read from HTML file
  NSError* error = nil;

  NSString *path = [[NSBundle mainBundle] pathForResource: @"QuoteStatus" ofType: @"html"];
  strHTLM = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];


  if(arrProducts.count > 0)
  {
    for (int j = 0; j < arrProducts.count; j++) {
      strProducts = [NSString stringWithFormat:@"<tr><td valign=\"middle\" class=\"td7\">"
                     "<p class=\"p2\">%@</p>"
                     "</td>"
                     "<td valign=\"middle\" class=\"td7\">"
                     "<p class=\"p2\">%@</p>"
                     "</td>"
                     "<td valign=\"middle\" class=\"td8\">"
                     "<p class=\"p2\">%@</p>"
                     "</td>"
                     "<td valign=\"middle\" class=\"td9\">"
                     "<p class=\"p2\">%@</p>"
                     "</tr>",[[arrProducts objectAtIndex:j] objectForKey:@"ITEM"],[[arrProducts objectAtIndex:j] objectForKey:@"DESCRIPTION"],[[arrProducts objectAtIndex:j] objectForKey:@"PRICE"],[[arrProducts objectAtIndex:j] objectForKey:@"TAX"]];

      // s = [NSString stringWithFormat:@"<br>%@</br>",strProducts];

      strProductshtml = [strProductshtml stringByAppendingString:strProducts];
      
    }
  }

//  strHTLM =   [strHTLM stringByReplacingOccurrencesOfString:@"trim($code)" withString:[[[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"Debtor"] objectForKey:@"text"]];
//  strHTLM =  [strHTLM stringByReplacingOccurrencesOfString:@"trim($name)" withString:[[[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"Delname"] objectForKey:@"text"]];

  strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"CustoMerOrderToReplace" withString:strProductshtml];


  /*
   NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   NSString *documentsDirectory = [paths objectAtIndex:0];
   NSString *fileInDirectory1 = [documentsDirectory stringByAppendingPathComponent:@"TestNEW.html"];
   [strHTLM writeToFile:fileInDirectory1 atomically:YES];
   */

  [self callMailComposer];

}
-(void)callMailComposer{

  Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
  if (mailClass != nil)
  {
    // We must always check whether the current device is configured for sending emails
    if ([mailClass canSendMail])
      [self displayComposerSheet];
    else
      [self launchMailAppOnDevice];
  }

  else
  {
    [self launchMailAppOnDevice];
  }
}
-(void)displayComposerSheet{

  MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
  picker.mailComposeDelegate = self;
  NSString *tosubject =@"";
  [picker setSubject:tosubject];

  // Set up recipients
  [picker setBccRecipients:@[@"orders@rumcityfoods"]];
  [picker setMessageBody:strHTLM isHTML:YES];
  [self presentViewController:picker animated:YES completion:Nil];

  if(picker) picker=nil;

}
-(void)launchMailAppOnDevice{
  // Displays an email composition interface inside the application. Populates all the Mail fields.

  NSString *recipients = @"mailto:?cc=&subject=";
  NSString *body = @"&body=";

  NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
  email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];

}



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
  NSString* alertMessage;
  // message.hidden = NO;
  // Notifies users about errors associated with the interface
  switch (result)
  {
    case MFMailComposeResultCancelled:
      alertMessage = @"Email composition cancelled";
      break;
    case MFMailComposeResultSaved:
      alertMessage = @"Your e-mail has been saved successfully";

      break;
    case MFMailComposeResultSent:
      alertMessage = @"Your email has been sent successfully";

      break;
    case MFMailComposeResultFailed:
      alertMessage = @"Failed to send email";

      break;
    default:
      alertMessage = @"Email Not Sent";

      break;
  }

  UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                      message:alertMessage
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
  [alertView show];
  [self dismissViewControllerAnimated:YES completion:Nil];
}


-(NSString *)callCreateSoapStringProfileOrder:(int)finalizeTag
{
    strWebserviceType = @"WS_PROFILE_ORDER_SAVE";

    NSString *strItem = [[NSString alloc] init];
    int line_num = 0;
    
    
    for (NSDictionary *dict in arrProducts)
    {
        
        NSString *strStockCode = nil;
        NSString *strPrice = @"0";
        NSString *strDescription = nil;
        NSString *strExtension = @"0";
        NSString *strDissection = @"0";
        NSString *strDissection_Cos = @"0";
        NSString *strWeight = @"0";
        NSString *strVolume = @"0";
        NSString *strCost = @"0";
        NSString *strQuantityOrdered = @"0";
        NSString *strGross = @"0";
        NSString *strTax = @"0";
        NSString *strLineNumber = @"0";
        
        NSLog(@"line No %d",line_num);
        
        //        int NumberOflinesForComment = 0;
        
        //Comments
        if ([dict objectForKey:@"ITEM"] && [[dict objectForKey:@"ITEM"] isEqualToString:@"/C"]){
            
            int detail_lines = 0;
            
            //            line_num =(int)[strLineNumber integerValue];
            
            strDescription = [[dict objectForKey:@"DESCRIPTION"] trimSpaces];
            
            int NumberOflines = strDescription.length/30;
            
            if(NumberOflines%30 > 0)
            {
                NumberOflines= NumberOflines +1;
                
            }
            
            if(NumberOflines == 0)
            {
                int z = 0;
                NSRange rang = NSMakeRange(z, 30);
                
                detail_lines++;
                line_num++;
                
                strStockCode = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
                strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                //strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                
           
                
               // strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
            for (int k = 0; k < NumberOflines; k++)
            {
                int z = k*30;
                NSRange rang = NSMakeRange(z, 30);
                
                detail_lines++;
                line_num++;
                
                strStockCode = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
                strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                //strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                
                //                strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
                //<CUST_ORDER>%@<CUST_ORDER/>
            }
        }
        else{
            // LineNum
            if ([dict objectForKey:@"LINE_NO"]) {
                if([[dict objectForKey:@"LINE_NO"] isKindOfClass:[NSNumber class]])
                {
                    strLineNumber = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"LINE_NO"] stringValue]];
                }
                else{
                    strLineNumber = [dict objectForKey:@"LINE_NO"];
                }
            }
            //Freight
            if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
                strStockCode = @"/F";
                strDescription = [dict objectForKey:@"Description"];
                strPrice = [dict objectForKey:@"Price"];
                strExtension = [dict objectForKey:@"ExtnPrice"];
                strTax = [dict objectForKey:@"Tax"];
                
            }
            
            //Product
            else{
               
                strStockCode = [dict objectForKey:@"ITEM"];
                strPrice = [dict objectForKey:@"PRICE"];
                strDissection = [dict objectForKey:@"DISSECTION"];
                strDissection_Cos = [dict objectForKey:@"DISSECTION_COS"];
                strWeight = [dict objectForKey:@"WEIGHT"];
                strVolume = [dict objectForKey:@"VOLUME"];
                
                strDescription = [dict objectForKey:@"DESCRIPTION"];
                strCost = [dict objectForKey:@"COST"];
                
                if ([dict objectForKey:@"EXTENSION"]) {
                    strExtension = [dict objectForKey:@"EXTENSION"];
                }
                if([dict objectForKey:@"QUANTITY"] )
                {
                    if([[dict objectForKey:@"QUANTITY"] isKindOfClass:[NSNumber class]])
                    {
                        strQuantityOrdered = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"QUANTITY"] stringValue]];
                    }
                    else{
                        strQuantityOrdered = [dict objectForKey:@"QUANTITY"];
                    }
                }
                strVolume = [dict objectForKey:@"VOLUME"];
                strGross = [dict objectForKey:@"GROSS"];
                strTax = [dict objectForKey:@"TAX"];
            }
            
            if([strPrice isKindOfClass:[NSNumber class]])
            {
                id price = strPrice;
                strPrice = [NSString stringWithFormat:@"%@",[price stringValue]];
            }
            
            if([strTax isKindOfClass:[NSNumber class]])
            {
                id tax = strTax;
                strTax = [NSString stringWithFormat:@"%@",[tax stringValue]];
            }
            if([strQuantityOrdered isKindOfClass:[NSNumber class]])
            {
                id tax = strQuantityOrdered;
                strQuantityOrdered = [NSString stringWithFormat:@"%@",[tax stringValue]];
            }
            
            
            //==============Check fro HTML TAGS //==============
            strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
           // strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
            
           if( [strQuantityOrdered integerValue] > 0)
           {
               line_num++;
               strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,strLineNumber,[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,[strDescription trimSpaces],[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
           }
            
        }
    }
    


  NSString *iso8601String = @"";
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  //  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
  NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
  [dateFormatter setLocale:enUSPOSIXLocale];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
  NSDate *now = [NSDate date];
  iso8601String = [dateFormatter stringFromDate:now];

  //[dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:00"];
   [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateRaised = [dateFormatter dateFromString:self.strDateRaised];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
  strDateRaised = [dateFormatter stringFromDate:dateRaised];

  //Change by subhu
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
  if (dateDel==nil)
  {
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    dateDel = [dateFormatter dateFromString:strDeliveryDate];
  }
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
  strDeliveryDate = [dateFormatter stringFromDate:dateDel];




/*
    //--DATE_RAISED
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *dateRaised = [dateFormatter dateFromString:self.strDateRaised];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    strDateRaised = [dateFormatter stringFromDate:dateRaised];

    //--Delivery
    //Change by subhu
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
//    if (dateDel==nil)
//    {
//        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//        dateDel = [dateFormatter dateFromString:strDeliveryDate];
//    }
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    strDeliveryDate = [dateFormatter stringFromDate:dateDel];


  //Change : Chaitanya
//  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//  NSDate *dateRaised = [dateFormatter dateFromString:self.strDateRaised];
//  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
//  strDateRaised = [dateFormatter stringFromDate:dateRaised];
//
//  //Delivery Date
//  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//  NSDate *dateDelivery = [dateFormatter dateFromString:self.strDeliveryDate];
//  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
//  strDeliveryDate = [dateFormatter stringFromDate:dateDelivery];
//  NSLog(@"%@",strDeliveryDate);*/

    if(strDeliveryDate == nil)
    {
        strDeliveryDate = iso8601String;
    }
    
    if(self.strDateRaised == nil)
    {
        self.strDateRaised = iso8601String;
    }
    
    NSString *strcashcollect = @"";
    NSString *strcashamount= @"";
    NSString *strcashreppickup= @"";
    NSString *strstockreturns= @"";
    NSString *strPickupInfoVal = @"";
    
    if([dictHeaderDetails objectForKey:@"CASH_COLLECT"])
    {
        strcashcollect = [dictHeaderDetails objectForKey:@"CASH_COLLECT"];
    }
    
    if([dictHeaderDetails objectForKey:@"CASH_AMOUNT"])
    {
        strcashamount = [dictHeaderDetails objectForKey:@"CASH_AMOUNT"];
    }
    
    if([dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"])
    {
        strcashreppickup = [dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"];
    }
    
    if([dictHeaderDetails objectForKey:@"STOCK_RETURNS"])
    {
        strstockreturns = [dictHeaderDetails objectForKey:@"STOCK_RETURNS"];
    }
    
    //    PICKUP_INFO
    if([dictHeaderDetails objectForKey:@"PICKUP_INFO"])
    {
        strPickupInfoVal = [dictHeaderDetails objectForKey:@"PICKUP_INFO"];
    }
    
    if ([self.strPriceChange isEqualToString:@"<null>"]) {
        self.strPriceChange = @"N";
    }
    //    strCustOrder = @"";  //  ***** Need to send customer order
    if(strSubUrb.length > 15)
    {
        strSubUrb = [strSubUrb substringToIndex:14];
    }
    
    strAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress1];
    strAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress2];
    strAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress3];
    strDelInst2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    strDelInst1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<SalesOrder>"
                            "<HEADER>"
                            "<OVERWRITE_SO>N</OVERWRITE_SO>"
                            "<CANCEL_SO>N</CANCEL_SO>"
                            "<INC_WAREHOUSE>Y</INC_WAREHOUSE>"
                            "<DELIVERY_ADDRESS_TYPE>S</DELIVERY_ADDRESS_TYPE>"
                            "<DELIV_NUM>0</DELIV_NUM>"
                            "<SUB_SET_NUM/>"
                            "<SUPPLIED_LINE_NUMBERS>N</SUPPLIED_LINE_NUMBERS>"
                            "<ORDER_NO />"
                            "<WEBORDERID>0</WEBORDERID>"
                            "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                            "<CASH_COLLECT>%@</CASH_COLLECT>"
                            "<CASH_AMOUNT>%@</CASH_AMOUNT>"
                            "<CASH_REP_PICKUP>%@</CASH_REP_PICKUP>"
                            "<STOCK_RETURNS>%@</STOCK_RETURNS>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<BRANCH>%@</BRANCH>"
                            "<SALES_BRANCH>%@</SALES_BRANCH>"
                            "<DEL_NAME>%@</DEL_NAME>"
                            "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                            "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                            "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                            "<DEL_SUBURB>%@</DEL_SUBURB>"
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                            "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                            "<DEL_INST1>%@</DEL_INST1>"
                            "<DEL_INST2>%@</DEL_INST2>"
                            "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                            "<CHARGE_RATE>%@</CHARGE_RATE>"
                            "<DATE_RAISED>%@</DATE_RAISED>"
                            "<DEL_DATE>%@</DEL_DATE>"
                            "<CUST_ORDER>%@</CUST_ORDER>" // Changed <CUST_ORDER />
                            "<SALESMAN>%@</SALESMAN>"
                            "<WAREHOUSE>%@</WAREHOUSE>"
                            "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                            "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                            "<DISC_PERCENT>0.00</DISC_PERCENT>"
                            "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                            "<INC_PRICE>%@</INC_PRICE>"
                            
                            "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                            "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                            "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                            "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                            "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                            "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                            
                            "</HEADER>"
                            "%@"
                            "</SalesOrder>",[iso8601String trimSpaces],strcashcollect,strcashamount,strcashreppickup,strPickupInfoVal,[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strAddress1 trimSpaces],[strAddress2 trimSpaces],[strAddress3 trimSpaces],[strSubUrb trimSpaces],[strPostCode trimSpaces],[strCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],strChargeRate ,[strDateRaised trimSpaces],[strDeliveryDate trimSpaces],[strCustOrder trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces ],self.strPriceChange,strItem ];// strstockreturns
    
    NSLog(@"soapXMLStr %@",soapXMLStr);

    return soapXMLStr;
}



-(NSString *)callCreateSoapStringSalesOrder:(int)finalizeTag
{
    strWebserviceType = @"WS_SALES_ORDER_SAVE";
    
    NSString *strItem = [[NSString alloc] init];
    int line_num = 0;
    
    for (NSDictionary *dict in arrProducts) {
        
        NSString *strStockCode = nil;
        NSString *strPrice = @"0";
        NSString *strDescription = nil;
        NSString *strExtension = @"0";
        NSString *strDissection = @"0";
        NSString *strDissection_Cos = @"0";
        NSString *strWeight = @"0";
        NSString *strVolume = @"0";
        NSString *strCost = @"0";
        NSString *strQuantityOrdered = @"0";
        NSString *strGross = @"0";
        NSString *strTax = @"0";
        NSString *strLineNumber = @"0";
        
        
        //Comments
        if ([dict objectForKey:@"ITEM"] && [[dict objectForKey:@"ITEM"] isEqualToString:@"/C"]){
            
            int detail_lines = 0;
            strDescription = [[dict objectForKey:@"DESCRIPTION"] trimSpaces];
            
            int NumberOflines = strDescription.length/30;
            
            if(NumberOflines%30 > 0)
            {
                NumberOflines= NumberOflines +1;
                
            }
            
            if(NumberOflines == 0)
            {
                int z = 0;
                NSRange rang = NSMakeRange(z, 30);
                
                detail_lines++;
                line_num++;
                
                strStockCode = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
                strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
               // strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                
                //              strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                

                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
            for (int k = 0; k < NumberOflines; k++)
            {
                int z = k*30;
                NSRange rang = NSMakeRange(z, 30);
                
                detail_lines++;
                line_num++;
                
                strStockCode = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
                strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                //strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                //              <CUST_ORDER>%@</CUST_ORDER>
                
                
                //              strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
        }
        else{
            if ([dict objectForKey:@"LINE_NO"]) {
                if([[dict objectForKey:@"LINE_NO"] isKindOfClass:[NSNumber class]])
                {
                    strLineNumber = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"LINE_NO"] stringValue]];
                    
                }
                else
                    strLineNumber = [dict objectForKey:@"LINE_NO"];
            }
            
            
            //Freight
            if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
                strStockCode = @"/F";
                strDescription = [dict objectForKey:@"Description"];
                strPrice = [dict objectForKey:@"Price"];
                strExtension = [dict objectForKey:@"ExtnPrice"];
                strTax = [dict objectForKey:@"Tax"];
                
            }
            //Product
            else{
                
                strStockCode = [dict objectForKey:@"ITEM"];
                
                if([[dict objectForKey:@"PRICE"] isKindOfClass:[NSNumber class]] || [[dict objectForKey:@"PRICE"] isKindOfClass:[NSNumber class]]) {
                    strPrice = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"PRICE"] stringValue]];
                }
                else
                    strPrice = [dict objectForKey:@"PRICE"];
                strDissection = [dict objectForKey:@"DISSECTION"];
                strDissection_Cos = [dict objectForKey:@"DISSECTION_COS"];
                strWeight = [dict objectForKey:@"WEIGHT"];
                strVolume = [dict objectForKey:@"VOLUME"];
                strDescription = [dict objectForKey:@"DESCRIPTION"];
                NSLog(@"%@",strDescription);
                strCost = [dict objectForKey:@"COST"];
                
                if ([dict objectForKey:@"EXTENSION"]) {
                    strExtension = [dict objectForKey:@"EXTENSION"];
                }
                
                if([[dict objectForKey:@"QUANTITY"] isKindOfClass:[NSNumber class]])
                {
                    strQuantityOrdered = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"QUANTITY"] stringValue]];
                }
                else
                    strQuantityOrdered = [dict objectForKey:@"QUANTITY"];
                strVolume = [dict objectForKey:@"VOLUME"];
                strGross = [dict objectForKey:@"GROSS"];
                if([[dict objectForKey:@"TAX"] isKindOfClass:[NSNumber class]])
                {
                    strTax = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"TAX"] stringValue]];
                }
                else
                    strTax = [dict objectForKey:@"TAX"];
                
            }
            //==============Check fro HTML TAGS //==============
            strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
            //strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
            
            if( [strQuantityOrdered integerValue] > 0)
            {
            line_num++;
            strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM /><ORDER_NO /><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",strCustOrder,[strLineNumber trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,[strDescription trimSpaces],[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
            
            }
        }
        
    }
    
    self.strFuelLevy = @"Y";
    
    /*
     NSString *soapXMLStr = [NSString stringWithFormat:
     @"<wws:InsertSalesOrder soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
     "<salesOrdHeader xsi:type=\"wws:SalesOrdHeaRequest\">"
     "<DEBTOR>%@</DEBTOR>"
     "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"//editable
     "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"//editable
     "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"//editable
     "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"//editable
     "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"//editable
     "<DEL_POST_CODE>%@</DEL_POST_CODE>"//editable
     "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"//editable
     "<CONTACT>%@</CONTACT>"//editable
     "<DEL_DATE><![CDATA[%@]]></DEL_DATE>"
     "<DATE_RAISED><![CDATA[%@]]></DATE_RAISED>"//todays date
     "<CUST_ORDER>%@</CUST_ORDER>"//editable
     "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"//editable
     "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"//editable
     "<STATUS><![CDATA[%@]]></STATUS>"//always 1
     "<DIRECT><![CDATA[%@]]></DIRECT>"
     "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
     "<BRANCH><![CDATA[%@]]></BRANCH>"
     "<PRICE_CODE><![CDATA[%@]]></PRICE_CODE>"
     "<SALESMAN><![CDATA[%@]]></SALESMAN>"
     "<SALES_BRANCH><![CDATA[%@]]></SALES_BRANCH>"
     "<TRADING_TERMS><![CDATA[%@]]></TRADING_TERMS>"
     "<DELIVERY_RUN><![CDATA[%@]]></DELIVERY_RUN>"
     "<TAX_EXEMPTION1><![CDATA[%@]]></TAX_EXEMPTION1>"
     "<TAX_EXEMPTION2><![CDATA[%@]]></TAX_EXEMPTION2>"
     "<TAX_EXEMPTION3><![CDATA[%@]]></TAX_EXEMPTION3>"
     "<TAX_EXEMPTION4><![CDATA[%@]]></TAX_EXEMPTION4>"
     "<TAX_EXEMPTION5><![CDATA[%@]]></TAX_EXEMPTION5>"
     "<TAX_EXEMPTION6><![CDATA[%@]]></TAX_EXEMPTION6>"
     "<EXCHANGE_CODE><![CDATA[%@]]></EXCHANGE_CODE>"
     "<ALLOW_PARTIAL><![CDATA[%@]]></ALLOW_PARTIAL>"
     "<CHARGE_TYPE><![CDATA[%@]]></CHARGE_TYPE>"
     "<CARRIER_CODE><![CDATA[%@]]></CARRIER_CODE>"
     "<EXPORT_DEBTOR><![CDATA[%@]]></EXPORT_DEBTOR>"
     "<DROP_SEQ><![CDATA[%@]]></DROP_SEQ>"
     "<HELD><![CDATA[%@]]></HELD>"
     "<CHARGE_RATE><![CDATA[%@]]></CHARGE_RATE>"
     "<NUM_BOXES><![CDATA[%@]]></NUM_BOXES>"
     "<CASH_COLLECT><![CDATA[%@]]></CASH_COLLECT>"
     "<CASH_AMOUNT><![CDATA[%@]]></CASH_AMOUNT>"
     "<CASH_REP_PICKUP><![CDATA[%@]]></CASH_REP_PICKUP>"
     "<OPERATOR><![CDATA[%@]]></OPERATOR>"
     "<CREATED_BY><![CDATA[%@]]></CREATED_BY>"
     "<STOCK_RETURNS><![CDATA[%@]]></STOCK_RETURNS>"
     "<PERIOD_RAISED><![CDATA[%@]]></PERIOD_RAISED>"
     "<YEAR_RAISED><![CDATA[%@]]></YEAR_RAISED>"
     "<DETAIL_LINES><![CDATA[%@]]></DETAIL_LINES>"
     "<ORDER_VALUE><![CDATA[%@]]></ORDER_VALUE>"
     "<EXCHANGE_RATE>%@</EXCHANGE_RATE>" // Static value
     "<TOTL_CARTON_QTY>%@</TOTL_CARTON_QTY>" // Static value
     "<ACTIVE>%@</ACTIVE>" // Static value
     "<EDIT_STATUS>%@</EDIT_STATUS>" // Static value
     "<WEIGHT>%@</WEIGHT>"
     "<VOLUME>%@</VOLUME>"
     "<TAX>%@</TAX>"
     "<TAX_AMOUNT1>%@</TAX_AMOUNT1>"
     "</salesOrdHeader>"
     "<salesOrdDetails xsi:type=\"wws:SalesOrdDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
     "%@"
     "</salesOrdDetails>"
     "</wws:InsertSalesOrder>",strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strPostCode,strCountry,strContact,strDeliveryDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,strCashCollect,strCashAmount,strCashRepPickUp,strOperator,self.strOperatorName,strStockPickup,strPeriodRaised,strYearRaised,strTotalLines,strOrderValue,stExchangeRate,strTotalCartonQuantity,strActive,strEditStatus,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strItem];
     */
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDate *now = [NSDate date];
    NSString *iso8601String = [dateFormatter stringFromDate:now];
    
    //--DATE_RAISED
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *dateRaised = [dateFormatter dateFromString:strDateRaised];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    strDateRaised = [dateFormatter stringFromDate:dateRaised];

    //  //Expirary Date
    //  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //  NSDate *dateExpirary = [dateFormatter dateFromString:self.strDateRaised];
    //  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    //  self.strDateRaised = [dateFormatter stringFromDate:dateExpirary];
    
    //--Delivery
    //Change by subhu
    //  [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    strDeliveryDate = [dateFormatter stringFromDate:dateDel];



  //Change : Chaitanya
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateRaised = [dateFormatter dateFromString:self.strDateRaised];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
  strDateRaised = [dateFormatter stringFromDate:dateRaised];

  //Delivery Date
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateDelivery = [dateFormatter dateFromString:self.strDeliveryDate];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00ZZZZZ"];
  strDeliveryDate = [dateFormatter stringFromDate:dateDelivery];
  NSLog(@"%@",strDeliveryDate);
    
    if(strDeliveryDate == nil)
    {
        strDeliveryDate = iso8601String;
    }
    
    if(self.strDateRaised == nil)
    {
        self.strDateRaised = iso8601String;
    }
    //
    NSString *CASH_COLLECT = @"";
    NSString *CASH_REP_PICKUP = @"";
    NSString *CASH_AMOUNT = @"";
    NSString *STOCK_RETURNS = @"";
    NSString *PICKUP_INFO = @"";
    
    
    if([dictHeaderDetails objectForKey:@"CASH_COLLECT"])
    {
        CASH_COLLECT = [dictHeaderDetails objectForKey:@"CASH_COLLECT"];
    }
    if([dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"])
    {
        CASH_REP_PICKUP = [dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"];
    }
    if([dictHeaderDetails objectForKey:@"CASH_AMOUNT"])
    {
        CASH_AMOUNT = [dictHeaderDetails objectForKey:@"CASH_AMOUNT"];
    }
    if([dictHeaderDetails objectForKey:@"STOCK_RETURNS"])
    {
        STOCK_RETURNS = [dictHeaderDetails objectForKey:@"STOCK_RETURNS"];
    }
    
    //    PICKUP_INFO
    if([dictHeaderDetails objectForKey:@"PICKUP_INFO"])
    {
        PICKUP_INFO = [dictHeaderDetails objectForKey:@"PICKUP_INFO"];
    }
    
    //    strCustOrder = @"";  //  ***** Need to send customer order
    if(strSubUrb.length > 15)
    {
        strSubUrb = [strSubUrb substringToIndex:14];
    }
    
    
    if ([self.strPriceChange isEqualToString:@"<null>"]) {
        self.strPriceChange = @"N";
    }
    
    //==============Check fro HTML TAGS //==============
    
    
    
    strDebtor = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDebtor];
    strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
    strSalesBranch =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strSalesBranch];
    strPostCode =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strPostCode];
    strCountry =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strCountry];
    
    strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    
    strAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress1];
    strAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress2];
    strAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress3];
    
    strDelInst1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    strDelInst2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<SalesOrder>"
                            "<HEADER>"
                            "<OVERWRITE_SO>N</OVERWRITE_SO>"
                            "<CANCEL_SO>N</CANCEL_SO>"
                            "<INC_WAREHOUSE>Y</INC_WAREHOUSE>"
                            "<DELIVERY_ADDRESS_TYPE>S</DELIVERY_ADDRESS_TYPE>"
                            "<DELIV_NUM>0</DELIV_NUM>"
                            "<SUB_SET_NUM/>"
                            "<SUPPLIED_LINE_NUMBERS>N</SUPPLIED_LINE_NUMBERS>"
                            "<ORDER_NO />"
                            "<WEBORDERID>0</WEBORDERID>"
                            "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                            "<CASH_COLLECT>%@</CASH_COLLECT>"
                            "<CASH_AMOUNT>%@</CASH_AMOUNT>"
                            "<CASH_REP_PICKUP>%@</CASH_REP_PICKUP>"
                            "<STOCK_RETURNS>%@</STOCK_RETURNS>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<BRANCH>%@</BRANCH>"
                            "<SALES_BRANCH>%@</SALES_BRANCH>"
                            "<DEL_NAME>%@</DEL_NAME>"
                            "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                            "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                            "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                            "<DEL_SUBURB>%@</DEL_SUBURB>"
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                            "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                            "<DEL_INST1>%@</DEL_INST1>"
                            "<DEL_INST2>%@</DEL_INST2>"
                            "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                            "<CHARGE_RATE>%@</CHARGE_RATE>"
                            "<DATE_RAISED>%@</DATE_RAISED>"
                            "<DEL_DATE>%@</DEL_DATE>"
                            "<CUST_ORDER>%@</CUST_ORDER>"//// Changed "<CUST_ORDER/>"
                            "<SALESMAN>%@</SALESMAN>"
                            "<WAREHOUSE>%@</WAREHOUSE>"
                            "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                            "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                            "<DISC_PERCENT>0.00</DISC_PERCENT>"
                            "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                            "<INC_PRICE>%@</INC_PRICE>"
                            "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                            "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                            "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                            "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                            "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                            "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                            "</HEADER>"
                            "%@"
                            "</SalesOrder>",[iso8601String trimSpaces],CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,PICKUP_INFO,[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strAddress1 trimSpaces],[strAddress2 trimSpaces],[strAddress3 trimSpaces],[strSubUrb trimSpaces],[strPostCode trimSpaces],[strCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],strChargeRate ,[strDateRaised trimSpaces],[strDeliveryDate trimSpaces],[strCustOrder trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces],self.strPriceChange,strItem];// STOCK_RETURNS
    
    NSLog(@"soapXMLStr %@",soapXMLStr);
    return soapXMLStr;
}

-(void)updateSentStatus:(NSString *)OrderNum{
    @try {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            NSString *strQuery = [NSString stringWithFormat:@"Update  soheader_offline set SentStatus = '1' where ORDER_NO = '%@'",OrderNum];
            BOOL result,result1;
            result = [db executeUpdate:strQuery];
            //rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strQuoteNumRecived];
            
            if (result == YES)
            {
                NSString *strQuery = [NSString stringWithFormat:@"Update soheader_offline set SentStatus = '1' where ORDER_NO = '%@'",OrderNum];
                result1 = [db executeUpdate:strQuery];
                
            }
            
            if(result == NO || result1 == NO)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            else if (result == YES || result1 == YES)
            {
                [self loadMoreData];
            }
            
            
        }];
    }
    @catch (NSException *exception) {
        @throw [NSException exceptionWithName:@"Local DB Error" reason:@"Local DB Error" userInfo:nil];
    }
    
    
}


-(void)DeleteDataFromTbl:(NSString *)OrderNum
{
    @try {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            NSString *strQuery = [NSString stringWithFormat:@"Delete from soheader_offline where ORDER_NO = '%@'",OrderNum];
            BOOL result,result1;
            result = [db executeUpdate:strQuery];
            //rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strQuoteNumRecived];
            
            if (result == YES)
            {
                NSString *strQuery = [NSString stringWithFormat:@"Delete from sodetail_offline where ORDER_NO = '%@'",OrderNum];
                result1 = [db executeUpdate:strQuery];
                
            }
            
            if(result == NO || result1 == NO)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            else if (result == YES || result1 == YES)
            {
                [self loadMoreData];
            }
            
            
        }];
    }
    @catch (NSException *exception) {
        @throw [NSException exceptionWithName:@"Local DB Error" reason:@"Local DB Error" userInfo:nil];
    }
    
    
}



-(void)CallServiceForStockPickupEmail{
    //
    //    NSLog(@"arrSelectedOrders %@",arrSelectedOrders);
    //    for (int i=0; i<[arrSelectedOrders count]; i++) {
    
    //@autoreleasepool {
    //       NSString *strOrderNo = [[arrSelectedOrders objectAtIndex:i] objectForKey:@"OrderNo"];
    //
    //        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    //        [databaseQueue   inDatabase:^(FMDatabase *db) {
    //
    //            [self getStockPickupDetailInfoFromDB:strOrderNo];
    //        }];
    //
    
    //        NSIndexPath *selectedIndexPath = [[arrSelectedOrders objectAtIndex:i] objectForKey:@"cellIndexPath"];
    NSString *strRequest=[AppDelegate getServiceURL:@"webservices_rcf/stockpickupform.php?status=1"];
    
    NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
    if (dictStockValue.count > 0) {
        
        NSLog(@"dictStockValue %@",dictStockValue);
        
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        
        [request setPostValue:[dictStockValue objectForKey:@"batchNo"] forKey:@"batchNo"];
        [request setPostValue:[dictStockValue objectForKey:@"BATCH_NAME"] forKey:@"batchName"];
        
        [request setPostValue:[dictStockValue objectForKey:@"bestBeforedate"] forKey:@"bestBeforedate"];
        [request setPostValue:[dictStockValue objectForKey:@"comment"]forKey:@"comment"];
        [request setPostValue:[dictStockValue objectForKey:@"contactAccountNo"] forKey:@"contactAccountNo"];
        [request setPostValue:[dictStockValue objectForKey:@"contactPhone"] forKey:@"contactPhone"];
        [request setPostValue:[dictStockValue objectForKey:@"customerId"] forKey:@"customerId"];
        
        [request setPostValue:[dictStockValue objectForKey:@"customerName"] forKey:@"customerName"];
        [request setPostValue:[dictStockValue objectForKey:@"dateRaised"] forKey:@"dateRaised"];
        [request setPostValue:[dictStockValue objectForKey:@"emailcc"] forKey:@"emailcc"];
        [request setPostValue:[dictStockValue objectForKey:@"emailto"] forKey:@"emailto"];
        [request setPostValue:[dictStockValue objectForKey:@"pickupNo"] forKey:@"pickupNo"];
        
        [request setPostValue:[dictStockValue objectForKey:@"productCode"] forKey:@"productCode"];
        
        [request setPostValue:[dictStockValue objectForKey:@"productDesc"] forKey:@"productDesc"];
        [request setPostValue:[dictStockValue objectForKey:@"reason"] forKey:@"reason"];
        [request setPostValue:[dictStockValue objectForKey:@"relatedInvoiceNo"] forKey:@"relatedInvoiceNo"];
        [request setPostValue:[dictStockValue objectForKey:@"requestedBy"] forKey:@"requestedBy"];
        [request setPostValue:[dictStockValue objectForKey:@"return"] forKey:@"return"];
        [request setPostValue:[dictStockValue objectForKey:@"quantity"] forKey:@"quantity"];
        
        
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached1"] forKey:@"imageAttached1"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached2"] forKey:@"imageAttached2"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached3"] forKey:@"imageAttached3"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached4"] forKey:@"imageAttached4"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached5"] forKey:@"imageAttached5"];
        
        
        
        strWebserviceType = @"StockPickUpForm";
        [operationQueueUpload addOperation:request];
        
    }
    
    request = nil;
    //       [dictStockValue removeAllObjects];
    
}

-(void)updateEmailStockPickupSend:(NSString *)pickupNum{
    @try {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            NSString *strQuery = [NSString stringWithFormat:@"Update  StockPickup set isEmailSent ='YES' where PICKUP_NO = '%@'",pickupNum];
            BOOL result;
            result = [db executeUpdate:strQuery];
            
            if(result == NO)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
        }];
    }
    @catch (NSException *exception) {
        @throw [NSException exceptionWithName:@"Local DB Error" reason:@"Local DB Error" userInfo:nil];
    }
    
    
}


#pragma mark - ASIHTTP Request delegates

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
    
    NSLog(@"dictResponse : %@",dictResponse);
    NSDictionary *userinfo = requestParameter.userInfo;
    
    NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"text : %@",text);
    
    //If successfully uploaded
    
    if([dictResponse isKindOfClass:[NSDictionary class]])
    {
        NSString *str = [dictResponse objectForKey:@"Flag"];
        if ([str isEqualToString:@"TRUE"]) {
            NSLog(@"%@",dictResponse);
            
            NSString *strPickup = [dictResponse objectForKey:@"PickupNo"];
            [self updateEmailStockPickupSend:strPickup];
            // Update database
        }
        if ([dictResponse count])
        {
        
        if([[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"])
        {
            
            if([[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"])
            {
                UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"Order Created Sucessfully" message:[NSString stringWithFormat:@" Order Number:%@",[[[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aler show];
                aler = nil;
                [localSpinner stopAnimating];
                
                if([userinfo objectForKey:@"OrderNo"])
                {
                    //31stOct
//                                 [arrQuotes removeObjectAtIndex:requestParameter.tag];
                                // [tblQuoteList deleteRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:nil];
                                [tblQuoteList reloadData];

                    [self DeleteDataFromTbl:[userinfo objectForKey:@"OrderNo"]];
                }
            }
            
        }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in uploading" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    if ([strWebserviceType isEqualToString:@"WS_PROFILE_ORDER_SAVE"]){
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            NSDictionary *dict = [[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"];
            
            //NSLog(@"strOrderNum :%@",strOrderNum);
            NSString *strFileName = [NSString stringWithFormat:@"soheader%d.plist",requestParameter.tag];
            NSString *strFilePath = [AppDelegate getFileFromDocumentsDirectory:strFileName];
            
            NSMutableDictionary *plistDict = [NSMutableDictionary dictionaryWithContentsOfFile:strFilePath];
            
            [plistDict setValue:[[dict objectForKey:@"orderno"] objectForKey:@"text"] forKey:@"NEW_ORDER_NO"];
            [plistDict setValue:[[dict objectForKey:@"UploadDate"] objectForKey:@"text"] forKey:@"UPLOAD_DATE"];
            [plistDict setValue:[[dict objectForKey:@"message"] objectForKey:@"text"] forKey:@"SUCCESS_MESSAGE"];
            
            //Write data in plist
            [plistDict writeToFile:strFilePath atomically: YES];
            
            strFileName = nil;
            strFilePath = nil;
            
            
            dispatch_async(backgroundQueueForUploadQuote, ^(void) {
                [self callInsertProfileOrderToDB:requestParameter.tag];
            });
            
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            NSString *strNewUploadDateMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"UploadDate"] objectForKey:@"text"];
            
            NSMutableDictionary *item = [arrQuotes objectAtIndex:requestParameter.tag];
            
            [item setValue:[NSNumber numberWithInt:1] forKey:@"isErrorWhileFinalise"];
            [item setValue:strErrorMessage forKey:@"errorMessage"];
            [item setValue:strNewUploadDateMessage forKey:@"UploadDate"];
            
            //Remove Stuffs
            [item removeObjectForKey:@"CheckFlag"];
            
            UITableViewCell *cell = [item objectForKey:@"cell"];
            UIButton *button = (UIButton *)cell.accessoryView;
            
            [button removeAllSubviews];
            
            HJManagedImageV *cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"error" ofType:@"png"];
            cachedImgView.image = [UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
            
            [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
            [button addSubview:cachedImgView];
            [arrSelectedOrders removeObject:item];
            [self doAfterdelete];
        }
        
        //Send email pf StockPickup
        
    }
    else if ([strWebserviceType isEqualToString:@"WS_SALES_ORDER_SAVE"]){
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            NSDictionary *dict = [[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"];
            
            //NSLog(@"strOrderNum :%@",strOrderNum);
            NSString *strFileName = [NSString stringWithFormat:@"soheader%d.plist",requestParameter.tag];
            NSString *strFilePath = [AppDelegate getFileFromDocumentsDirectory:strFileName];
            
            NSMutableDictionary *plistDict = [NSMutableDictionary dictionaryWithContentsOfFile:strFilePath];
            
            [plistDict setValue:[[dict objectForKey:@"orderno"] objectForKey:@"text"] forKey:@"NEW_ORDER_NO"];
            [plistDict setValue:[[dict objectForKey:@"UploadDate"] objectForKey:@"text"] forKey:@"UPLOAD_DATE"];
            [plistDict setValue:[[dict objectForKey:@"message"] objectForKey:@"text"] forKey:@"SUCCESS_MESSAGE"];
            
            //Write data in plist
            [plistDict writeToFile:strFilePath atomically: YES];
            
            strFileName = nil;
            strFilePath = nil;
            
            
            //NSLog(@"dict==%@",dict);
            
            dispatch_async(backgroundQueueForUploadQuote, ^(void) {
                [self callInsertProfileOrderToDB:requestParameter.tag];
            });
            
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            NSString *strNewUploadDateMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"UploadDate"] objectForKey:@"text"];
            
            NSMutableDictionary *item = [arrQuotes objectAtIndex:requestParameter.tag];
            
            [item setValue:[NSNumber numberWithInt:1] forKey:@"isErrorWhileFinalise"];
            [item setValue:strErrorMessage forKey:@"errorMessage"];
            [item setValue:strNewUploadDateMessage forKey:@"UploadDate"];
            
            //Remove Stuffs
            [item removeObjectForKey:@"CheckFlag"];
            
            UITableViewCell *cell = [item objectForKey:@"cell"];
            UIButton *button = (UIButton *)cell.accessoryView;
            
            [button removeAllSubviews];
            
            HJManagedImageV *cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"error" ofType:@"png"];
            cachedImgView.image = [UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
            
            [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
            [button addSubview:cachedImgView];
            
            [arrSelectedOrders removeObject:item];
            [self doAfterdelete];
        }
    }
    
    
    
    if (dictStockValue.count > 0) {
        NSString *strPickup = [dictStockValue objectForKey:@"pickupNo"];
        [self updateEmailStockPickupSend:strPickup];
        [dictStockValue removeAllObjects];
    }
    
}


- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    NSLog(@"Error %d : %@",requestParameter.tag,[requestParameter error]);
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:requestParameter.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
    NSError *error = [requestParameter error];
    if (error.code == 1 || error.code == 2) {
        progessView.hidden = YES;
        btnDelete.enabled = TRUE;
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:requestParameter.tag];
        
        UITableViewCell *cell = [item objectForKey:@"cell"];
        UIButton *button = (UIButton *)cell.accessoryView;
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
        UIImage *newImage=[[UIImage alloc]initWithContentsOfFile:strImage];
        [button setBackgroundImage:newImage forState:UIControlStateNormal];
        
    }
    requestParameter = nil;
}


#pragma mark - Email_for_StockPickup

-(void)getStockPickupDetailInfoFromDB:(NSString *)orderNumStr{
    
    
    NSLog(@"strOrderNum :%@",strOrderNum);
    dictStockValue = [[NSMutableDictionary alloc] init];
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        NSString *sqlquery = [NSString stringWithFormat:@"SELECT * FROM StockPickup as S, soheader_offline as H where H.PICKUP_NO = S.PICKUP_NO AND H.ORDER_NO = '%@'",strOrderNum];
        
        NSLog(@"%@",sqlquery);
        FMResultSet *res = [db executeQuery:sqlquery];
        
        if (!res)
        {
            NSLog(@"Error: %@", [db lastErrorMessage]);
            //[[AppDelegate getAppDelegateObj].database close];
            return;
        }
        
        NSLog(@"str Order Num %@",strOrderNum);
        
        while ([res next]) {
            NSDictionary *dictData = [res resultDictionary];
            
            
            NSString *pickupNo =[dictData objectForKey:@"PICKUP_NO"];
            NSString *dateRaised = [dictData objectForKey:@"TODAY_DATE"];
            NSString *requestedBy = [dictData objectForKey:@"REQUESTED_BY"];
            NSString *customerId = [dictData objectForKey:@"DEBTOR"];
            
            NSString *customerName = [dictData objectForKey:@"DEBTOR_NAME"];
            NSString *contactAccountNo = [dictData objectForKey:@"CONTACT_ACCOUNT_NO"];
            NSString *contactPhone = [dictData objectForKey:@"CONTACT_PHONE_NUMBER"];
            
            NSString *relatedInvoiceNo = [dictData objectForKey:@"RELATED_INVOICE_NO"];
            
            NSString *productCode = [dictData objectForKey:@"PRODUCT_CODE"];
            NSString *productDesc =[dictData objectForKey:@"PRODUCT_NAME"];
            NSString *batchNo = [dictData objectForKey:@"BATCH_NUMBER"];
            
            NSString *strBatchName = [dictData objectForKey:@"BATCH_NAME"];
            NSString *bestBeforedate = [dictData objectForKey:@"BEST_BEFORE_DATE"];
            NSString *emailto = [dictData objectForKey:@"EMAIL_TO"];
            
            NSString *emailcc = [dictData objectForKey:@"EMAIL_CC"];
            NSString *reason = [dictData objectForKey:@"REASON_OF_RETURN"];
            NSString *comment = [dictData objectForKey:@"DETAIL_REASON_RETURN"];
            
            NSString *quantity = [dictData objectForKey:@"QUANTITY"];
            NSString *returnVal =[dictData objectForKey:@"RETURN"];
            
            
            
            //    $pickupNo = $_REQUEST['pickupNo'];
            //    $pickupNo = 5;
            //    $dateRaised = $_REQUEST['dateRaised'];
            //    $requestedBy = $_REQUEST['requestedBy'];
            //    $customerId = $_REQUEST['customerId'];
            //    $customerName = $_REQUEST['customerName'];
            //    $contactPhone = $_REQUEST['contactPhone'];
            //    $contactAccountNo = $_REQUEST['contactAccountNo'];
            //    $relatedInvoiceNo = $_REQUEST['relatedInvoiceNo'];
            //    $productCode = $_REQUEST['productCode'];
            //    $productDesc = $_REQUEST['productDesc'];
            //    $batchNo = $_REQUEST['batchNo'];
            //    $bestBeforedate = $_REQUEST['bestBeforedate'];
            //
            //    $stockcode = explode(" --- ", $productCode);
            //    $stock_returns = $stockcode[0] . "|" . $_REQUEST['quantity'] . "|" . $REQUEST['reason'] . "|" . $REQUEST['return'] . "|" . $REQUEST['comment'] . "|";
            //
            //    $emailto = $_REQUEST['emailto'];
            //    $emailcc = $_REQUEST['emailcc'];
            //    $imageAttached1 = $_REQUEST['imageAttached1'];
            //    $imageAttached2 = $_REQUEST['imageAttached2'];
            //    $imageAttached3 = $_REQUEST['imageAttached3'];
            //    $imageAttached4 = $_REQUEST['imageAttached4'];
            //    $imageAttached5 = $_REQUEST['imageAttached5'];
            
            
            [dictStockValue setObject:(pickupNo)?pickupNo:@"" forKey:@"pickupNo"];
            [dictStockValue setObject:(dateRaised)?dateRaised:@"" forKey:@"dateRaised"];
            [dictStockValue setObject:(requestedBy)?requestedBy:@"" forKey:@"requestedBy"];
            
            [dictStockValue setObject:(customerId)?customerId:@"" forKey:@"customerId"];
            [dictStockValue setObject:(customerName)?customerName:@"" forKey:@"customerName"];
            [dictStockValue setObject:(contactPhone)?contactPhone:@"" forKey:@"contactPhone"];
            
            [dictStockValue setObject:(contactAccountNo)?contactAccountNo:@"" forKey:@"contactAccountNo"];
            [dictStockValue setObject:(relatedInvoiceNo)?relatedInvoiceNo:@"" forKey:@"relatedInvoiceNo"];
            [dictStockValue setObject:(productCode)?productCode:@"" forKey:@"productCode"];
            
            [dictStockValue setObject:(productDesc)?productDesc:@"" forKey:@"productDesc"];
            [dictStockValue setObject:(batchNo)?batchNo:@"" forKey:@"batchNo"];
            [dictStockValue setObject:(strBatchName)?strBatchName:@"" forKey:@"BATCH_NAME"];
            
            [dictStockValue setObject:(reason)?reason:@"" forKey:@"reason"];
            [dictStockValue setObject:(quantity)?quantity:@"" forKey:@"quantity"];
            [dictStockValue setObject:(returnVal)?returnVal:@"" forKey:@"return"];
            [dictStockValue setObject:(comment)?comment:@"" forKey:@"comment"];
            
            [dictStockValue setObject:(bestBeforedate)?bestBeforedate:@"" forKey:@"bestBeforedate"];
            
            [dictStockValue setObject:(emailto)?emailto:@"" forKey:@"emailto"];
            [dictStockValue setObject:(emailcc)?emailcc:@"" forKey:@"emailcc"];
            
            
            // Add image1 data
            NSString *image1 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED1"]];
            if ([image1 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached1"];
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED1"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached1"];
            }
            
            // Add image2 data
            NSString *image2 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED2"]];
            if ([image2 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached2"];
                
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED2"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached2"];
            }
            
            // Add image3 data
            NSString *image3 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED3"]];
            if ([image3 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached3"];
                
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED3"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached3"];
                
            }
            
            // Add image4 data
            NSString *image4 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED4"]];
            if ([image4 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached4"];
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED4"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached4"];
            }
            
            // Add image5 data
            NSString *image5 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED5"]];
            
            if ([image5 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached5"];
                
            }
            else{
                
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED5"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached5"];
                
            }
            
            dictData = nil;
        }
        
        [res close];
    }];
    
}

- (void)showEmail:(NSMutableDictionary*)stockPickupDict{
    
}


@end
