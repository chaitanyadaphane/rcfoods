//
//  StockWareHouseViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockWareHouseViewController : UIViewController{
    NSMutableArray *arrWareHouse;
}

@property(nonatomic,retain)NSMutableArray *arrWareHouse;

@end
