//
//  OfflineProfileDetailViewController.m
//  Blayney
//
//  Created by Pooja on 19/11/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "OfflineProfileDetailViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteProductListViewController.h"
#import "ViewQuoteProductDetailsViewController.h"
#import "QuoteHistoryCustomerListEditableMode.h"
#import "TopViewController.h"
#import "KGModal.h"
#import "MJDetailViewController.h"
#import "OfflineProductDetailVC.h"
#define TAG_50 50
#define TAG_100 100
#define TAG_200 200
#define TAG_300 300
#define TAG_400 400
#define TAG_999 999

//Tags for editable fields
#define TAG_500 500
#define TAG_600 600
#define TAG_700 700
#define TAG_800 800
#define TAG_900 900
#define TAG_1000 1000
#define TAG_1100 1100
#define TAG_1200 1200
#define TAG_1300 1300
#define TAG_1400 1400

#define PROFILE_PRODUCT_LIST_WS @"profordentry/prodetailslist.php?"
#define QUOTE_SAVE_HEADER_WS @"quote/adddebtordetails_quote.php?"
#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

#define ROWS_PER_PAGE_FOR_PROFILE 5
#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10
UILabel *lblBalance;
UIImageView *imgSpecialBg;

@interface OfflineProfileDetailViewController (){
    BOOL _loadingInProgress;
    int alertFreightValue;
    float freightValue;
    
    BOOL _longPress;
    NSMutableArray *arrProductPurchaseHistory;
    UITableView *tblProductHistory;
    NSString *strStockCode;
    NSString *strCopyDelDate;
    NSMutableDictionary *dictHeader;
    
    NSString *isStockPick;
    BOOL isEdit;
    
}
@property (nonatomic, strong) JFDepthView* depthView;
- (IBAction)actionEditOrder:(id)sender;
- (IBAction)actionDisplayQuoteMenu:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *editnewBtn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentSearch;
- (IBAction)filterSearchResult:(id)sender;

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;
- (IBAction)btnShowSpecials:(id)sender;
@end
static int line_num;
@implementation OfflineProfileDetailViewController

@synthesize arrHeaderLabels,arrProducts,arrSearchProducts,dictHeaderDetails,strQuoteNum,popoverController,strQuoteNumRecived,EditableMode,strOrderNum,strCashPickUp,strChequeAvailable,strDeliveryRun,strDebtor,lblTotalCost,isSearch;

@synthesize strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,isStorePickUpOn,strDelInst1,strDelInst2,strCustOrder,isCashPickUpOn,isRecordsExist,strPrevDebtor;

@synthesize btnDelete;

@synthesize btnLoadProfile,btnAddProducts,btnFinalise,btnSaveDetails,vwSegmentedControl,vwContentSuperview,vwDetails,vwHeader,vwNoProducts,imgArrowDownwards,imgArrowRightWards,keyboardToolBar,btnAddMenu,cntProfileOrders;

@synthesize strPhone,strEmail,strFax,strBalance,strAddress1,strAddress2,strAddress3,strSubUrb,strAllowPartial,strBranch,strCarrier,strChargeRate,strChargetype,strCountry,strDirect,strDropSequence,strExchangeCode,strExportDebtor,strHeld,strName,strNumboxes,strOrderDate,strPeriodRaised,strPostCode,strPriceCode,strSalesBranch,strSalesman,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strTradingTerms,strYearRaised,strStockPickup,strTotalCartonQty,strExchangeRate,strEditStatus,strActive,strStatus,strTotalWeight,strTotalVolume,strTotalTax,strTotalCost,strTotalLines,strRefreshPrice,strConvFactor,strBOQty,strOperator,strDecimalPlaces,strUploadDate,strSuccessMessage,strOrigCost,strDateRaised,strCommentDateCreated,strCommentFollowDate,strCommenttxt,strDelDate,strWarehouse,strContact,strSalesType,strCreditLimit,isCommentAddedOnLastLine;


@synthesize strOrderNumSel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!imgSpecialBg)
    {
        imgSpecialBg = [[UIImageView alloc]init];
    }
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [_tblDetails addGestureRecognizer:longPressRecognizer];
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    isEdit = NO;
    
    self.isCommentAddedOnLastLine = YES; // 14thNov
    
    strQuoteNum = [[NSString alloc] init];
    operationQueue = [NSOperationQueue  new];
    
    strPrevDebtor = nil;
    recordNumber = 0;
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    [vwContentSuperview addSubview:vwDetails ];
    vwDetails.hidden = YES;
    chkFinalize = NO;
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"OfflineOrderHeader" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    arrProducts = [[NSMutableArray alloc] init];
    arrSearchProducts = [[NSMutableArray alloc] init];
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    
    backgroundQueueForNewQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForNewQuote1", NULL);
    
    self.strDateRaised = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.strOperator = [prefs stringForKey:@"userName"];
    self.strSalesType = [prefs stringForKey:@"salesType"];
    
    self.strWarehouse = [prefs objectForKey:@"warehouse"];
    if (self.strWarehouse.length < 2) {
        self.strWarehouse = [NSString stringWithFormat:@"0%@",self.strWarehouse];
    }

    self.strOperatorName = [prefs objectForKey:@"members"];
    cntViewWillAppear = 1;
    
    //--Header Detail
    [self callGetSalesDetailsDetailsFromDB:strOrderNumSel];
    //Prodcut Array
    [self callGetProfileDetailsforProducts:strOrderNumSel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    isEdit = NO;
    
    
    if (cntViewWillAppear == 1) {
        
        //Notification to reload table when debtor is selected from Debtor list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHeader:) name:@"ReloadAddDebtorDetailsNotification" object:nil];
        
        //Notification to reload table when products added to details from product list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadAddProductDetailsNotification" object:nil];
    }
    //Counter is taken coz viewwillappear gets called twice.
    cntViewWillAppear++;
    

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    cntViewWillAppear = 0;
}


#pragma mark - Custom Methods

- (void)hideOrDisableButtons{
    btnLoadProfile.enabled = FALSE;
    btnSaveDetails.enabled = FALSE;
    btnAddProducts.enabled = FALSE;
    btnFinalise.enabled = FALSE;
    btnAddMenu.enabled = FALSE;
}
- (void)showOrEnableButtons{
    btnLoadProfile.enabled = TRUE;
    btnSaveDetails.enabled = TRUE;
    btnAddProducts.enabled = TRUE;
    btnFinalise.enabled = TRUE;
    btnAddMenu.enabled = TRUE;
}

- (void)showPointingAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.autoreverses = YES;
    animation.duration = 0.35;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    [layerPointingImage addAnimation:animation forKey:@"pulseAnimation"];
    [imgArrowDownwards.layer addSublayer:layerPointingImage];
}

- (void)setDefaults{
    self.isCashPickUpOn = @"N";
    self.strCashPickUp = @"0";
    self.strChequeAvailable = @"N";
    self.strStockPickup = @"N";
    self.strExchangeRate = @"1.000000";
    self.strActive = @"N";
    self.strEditStatus = @"11";
    self.strStatus = STATUS;
    self.strRefreshPrice = @"N";
    self.strConvFactor = @"1.0000";
    self.strDecimalPlaces = @"2";
    self.strTotalCartonQty = self.strBOQty = self.strOrigCost = @"0.0000";
    self.strTotalLines = self.strTotalTax = self.strTotalVolume = self.strTotalWeight = @"0";
    
    isDeliveryRunSlected = NO;
    segControl.enabled = NO;
    isRecordsExist = FALSE;
    totalCost = 0;
//    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
//    [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:lblTotalCost];

   lblTotalCost.text =[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[NSString stringWithFormat:@"%f",totalCost] floatValue]];
    self.cntProfileOrders = 0;
}

- (void)prepareForNewOrder{
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
    [arrProducts removeAllObjects];
    [dictHeaderDetails removeAllObjects];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwHeader];
    
//    [self setDefaults];
    lblBalance.text = @"";
    self.strFax = nil;
    self.strBalance = nil;
    self.strEmail = nil;
    self.strPhone = nil;
    self.strCreditLimit = nil;
    self.strDelName = nil;
    self.strDelAddress1 = nil;
    self.strDelAddress2 = nil;
    self.strDelAddress3 = nil;
    self.strDelSubUrb = nil;
    self.strDelPostCode = nil;
    self.strDelCountry = nil;
    self.strDeliveryRun = nil;
    self.strDebtor = nil;
    self.strDelInst1 = nil;
    self.strDelInst2 = nil;
    self.strCustOrder = nil;
    self.strContact = nil;
    self.strDelDate = nil;
    self.strAddress1 = nil;
    self.strAddress2 = nil;
    self.strAddress3 = nil;
    self.strSubUrb = nil;
    self.strAllowPartial = nil;
    self.strBranch = nil;
    self.strCarrier = nil;
    self.strChargeRate = nil;
    self.strChargetype = nil;
    self.strCountry = nil;
    self.strDirect = nil;
    self.strDropSequence = nil;
    self.strExchangeCode = nil;
    self.strExportDebtor = nil;
    self.strHeld = nil;
    self.strName = nil;
    self.strNumboxes = nil;
    self.strOrderDate = nil;
    self.strPeriodRaised = nil;
    self.strPostCode = nil;
    self.strPriceCode = nil;
    self.strSalesBranch = nil;
    self.strSalesman = nil;
    self.strTaxExemption1 = nil;
    self.strTaxExemption2 = nil;
    self.strTaxExemption3 = nil;
    self.strTaxExemption4 = nil;
    self.strTaxExemption5 = nil;
    self.strTaxExemption6 = nil;
    self.strTradingTerms = nil;
    self.strYearRaised = nil;
    self.strUploadDate = nil;
    self.strSuccessMessage = nil;
    self.strCommenttxt = nil;
    self.strCommentDateCreated = nil;
    self.strCommentFollowDate = nil;
    
    //Change the selection
    [segControl setSelectedIndex:0];
    
    [_tblHeader reloadData];
}


-(void)doAfterDataFetch{
    if (isRecordsExist) {
        
        [_tblDetails reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        recordNumber += ROWS_PER_PAGE_FOR_PROFILE;
    }
    else{
        
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
    
}

- (void)prepareView{
    
    if ([arrProducts count] > cntProfileOrders) {
        //Show delete button
        btnDelete.hidden = NO;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
    }
    else{
        btnDelete.hidden = YES;
    }
    
    if ([arrProducts count]) {
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [vwContentSuperview addSubview:vwDetails];
        
    }
    else{
        [vwContentSuperview addSubview:vwNoProducts];
        [self showPointingAnimation];
    }
    
    if ([[arrProducts valueForKey:@"ITEM"] isEqualToString:@"/C"]) {
        isCommentAddedOnLastLine = YES;
    }
    
}

-(void)calcFreight{
    
    // float freightValue = FREIGHT_VALUE + [self calcFreightTax];
    freightValue = alertFreightValue + [self calcFreightTax];
    
    totalCost += freightValue;
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    
    NSString *strFreightValue = [NSString stringWithFormat:@"%f",freightValue];
    NSString *strFreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:@"0" forKey:@"Available"];
    [dict setValue:@"Freight" forKey:@"Description"];
    [dict setValue:@"0" forKey:@"Location"];
    [dict setValue:[NSString stringWithFormat:@"%f",FREIGHT_VALUE] forKey:@"Price"];
    [dict setValue:strFreightTax forKey:@"Tax"];
    [dict setValue:strFreightValue forKey:@"ExtnPrice"];
    [dict setValue:@"0" forKey:@"ScmRecum"];
    [dict setValue:@"-" forKey:@"StockCode"];
    [dict setValue:@"0" forKey:@"Warehouse"];
    [dict setValue:@"0" forKey:@"CheckFlag"];
    [dict setValue:@"/F" forKey:@"Item"];
     [dict setValue:@"1" forKey:@"isEdit"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
}

-(float)calcFreightTax
{
    return (alertFreightValue*FREIGHT_TAX)/100;
}

-(void)prepareOfflineDataDetail:(NSMutableDictionary *)offlinedataDetail {
    
    //Debtor's Info
    self.strCreditLimit = [NSString stringWithFormat:@"%@",[offlinedataDetail objectForKey:@"CreditLimit"]];
    self.strDebtor = [offlinedataDetail objectForKey:@"Debtor"];
    self.strPhone = [offlinedataDetail objectForKey:@"Phone"];
    self.strFax = [offlinedataDetail objectForKey:@"Fax"];
    self.strBalance = [NSString stringWithFormat:@"%@",[offlinedataDetail objectForKey:@"Balance"]];
    self.strAddress1 = [offlinedataDetail objectForKey:@"Address1"];
    self.strAddress2 = [offlinedataDetail objectForKey:@"Address2"];
    self.strAddress3 = [offlinedataDetail objectForKey:@"Address3"];
    self.strSubUrb = [offlinedataDetail objectForKey:@"Suburb"];
    self.strAllowPartial = [offlinedataDetail objectForKey:@"AllowPartial"];
    self.strBranch = [offlinedataDetail objectForKey:@"Branch"];
    self.strCarrier = [offlinedataDetail objectForKey:@"Carrier"];
    self.strContact = [offlinedataDetail objectForKey:@"Contact"];
    self.strChargeRate = [offlinedataDetail objectForKey:@"ChargeRate"];
    self.strChargetype = [offlinedataDetail objectForKey:@"Chargetype"];
    self.strCountry = [offlinedataDetail objectForKey:@"Country"];
    self.strDirect = [offlinedataDetail objectForKey:@"Direct"];
    self.strDropSequence = [offlinedataDetail objectForKey:@"DropSequence"];
    self.strExchangeCode = [offlinedataDetail objectForKey:@"ExchangeCode"];
    self.strExportDebtor = [offlinedataDetail objectForKey:@"ExportDebtor"];
    self.strHeld = [offlinedataDetail objectForKey:@"Held"];
    self.strName = [offlinedataDetail objectForKey:@"Name"];
    self.strNumboxes = [offlinedataDetail objectForKey:@"Numboxes"];
    self.strOrderDate = [offlinedataDetail objectForKey:@"OrderDate"];
    self.strPeriodRaised = [offlinedataDetail objectForKey:@"PeriodRaised"];
    self.strPriceCode = [offlinedataDetail objectForKey:@"PriceCode"];
    self.strSalesBranch = [offlinedataDetail objectForKey:@"SalesBranch"];
    self.strSalesman = [offlinedataDetail objectForKey:@"Salesman"];
    self.strTaxExemption1 = [offlinedataDetail objectForKey:@"TaxExemption1"];
    self.strTaxExemption2 = [offlinedataDetail objectForKey:@"TaxExemption2"];
    self.strTaxExemption3 = [offlinedataDetail objectForKey:@"TaxExemption3"];
    self.strTaxExemption4 = [offlinedataDetail objectForKey:@"TaxExemption4"];
    self.strTaxExemption5 = [offlinedataDetail objectForKey:@"TaxExemption5"];
    self.strTaxExemption6 = [offlinedataDetail objectForKey:@"TaxExemption6"];
    self.strTradingTerms = [offlinedataDetail objectForKey:@"TradingTerms"];
    self.strYearRaised = [offlinedataDetail objectForKey:@"YearRaised"];
    self.strDelDate = [offlinedataDetail objectForKey:@"DeliveryDate"];
    self.strDelAddress1 = [offlinedataDetail objectForKey:@"DelAddress1"];
    self.strDelAddress2 = [offlinedataDetail objectForKey:@"DelAddress2"];
    self.strDelAddress3 = [offlinedataDetail objectForKey:@"DelAddress3"];
    self.strDelName = [offlinedataDetail objectForKey:@"Name"];
    
    self.strDelSubUrb = [offlinedataDetail objectForKey:@"DelSuburb"];
    self.strDelPostCode = [offlinedataDetail objectForKey:@"DelPostCode"];
    self.strDelCountry = [offlinedataDetail objectForKey:@"DelCountry"];
    
    self.strDelInst1 = [offlinedataDetail objectForKey:@"DelInst1"];
    
    self.strDelInst2 = [offlinedataDetail objectForKey:@"DelInst2"];
    
    self.strCustOrder =[offlinedataDetail objectForKey:@"CustOrderno"];
    
    self.strCommenttxt = [offlinedataDetail objectForKey:@"Commenttxt"];
    self.strCommentDateCreated = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"DateCreated"];
    self.strCommentFollowDate = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"FollowDate"];
    
    NSLog(@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]);
    
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"]) {
        self.strDeliveryRun = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"];
        isDeliveryRunSlected = YES;
    }
    else{
        isDeliveryRunSlected = NO;
    }
}

-(void)prepareHeaderData{
    
    //Debtor's Info
    self.strCreditLimit = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CreditLimit"]];
    
    //    PICKUP_INFO
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PICKUP_INFO"]) {
        isStockPick = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PICKUP_INFO"];
    }
    else{
        isStockPick =@"";
    }
    
    self.strDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"];
    self.strPhone = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Phone"];
    self.strFax = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Fax"];
    self.strBalance = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Balance"]];
    self.strAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address1"];
    self.strAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address2"];
    self.strAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address3"];
    self.strSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Suburb"];
    self.strAllowPartial = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"AllowPartial"];
    self.strBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Branch"];
    self.strCarrier = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Carrier"];
    self.strContact = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Contact"];
    self.strChargeRate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ChargeRate"];
    self.strChargetype = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Chargetype"];
    self.strCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Country"];
    self.strDirect = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Direct"];
    self.strDropSequence = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DropSequence"];
    self.strExchangeCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExchangeCode"];
    self.strExportDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExportDebtor"];
    self.strHeld = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Held"];
    self.strName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Name"];
    self.strNumboxes = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Numboxes"];
    self.strOrderDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderDate"];
    self.strPeriodRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PeriodRaised"];
    self.strPriceCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PriceCode"];
    self.strSalesBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"SalesBranch"];
    self.strSalesman = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Salesman"];
    self.strTaxExemption1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption1"];
    self.strTaxExemption2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption2"];
    self.strTaxExemption3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption3"];
    self.strTaxExemption4 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption4"];
    self.strTaxExemption5 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption5"];
    self.strTaxExemption6 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption6"];
    self.strTradingTerms = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TradingTerms"];
    self.strYearRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"YearRaised"];
    self.strDelDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelDate"];
    self.strDelAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address1"];
    self.strDelAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address2"];
    self.strDelAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address3"];
    //    self.strDelName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelName"];
    self.strDelName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Name"];
    self.strDelSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelSuburb"];
    self.strDelPostCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelPostCode"];
    self.strDelCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelCountry"];
    
//    self.strDelInst1 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"]) {
        self.strDelInst1 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"];
    }
    
//    self.strDelInst2 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"]) {
        self.strDelInst2 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"];
    }
    
//    self.strCustOrder = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"]) {
        self.strCustOrder = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"];
    }
    
    self.strCommenttxt = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Commenttxt"];
    self.strCommentDateCreated = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"DateCreated"];
    self.strCommentFollowDate = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"FollowDate"];
    
    NSLog(@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]);
    /*
     if (isEditable) {
     if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]){
     isStockPickUpOn = @"Y";
     self.strStockPickup = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]];
     }
     else{
     self.strStockPickup = @"N";
     isStockPickUpOn = @"N";
     }
     }
     */
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelRun"]) {
        self.strDeliveryRun = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelRun"];
        isDeliveryRunSlected = YES;
    }
    else{
        isDeliveryRunSlected = NO;
    }
    
    /*
     if (isEditable) {
     if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashCollect"] isEqualToString:@"1"]) {
     isCashPickUpOn = @"Y";
     
     self.strCashPickUp = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashAmount"];
     }
     else{
     isCashPickUpOn = @"N";
     self.strCashPickUp = @"0";
     }
     
     }
     */
    
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
    
}



- (IBAction)actionDisplayQuoteMenu:(id)sender{
    
    //tagFlsh = 1;
    //[self flashFinal:btnAddMenu];
  //DDD
    
    QuoteMenuPopOverViewController *popoverContent = [[QuoteMenuPopOverViewController alloc] initWithNibName:@"QuoteMenuPopOverViewController" bundle:[NSBundle mainBundle]];
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    
    //Finalise option not needed
    popoverContent.isFinalisedNeeded = NO;
    popoverContent.isCommentsNotNeeded = NO;
    popoverContent.isProfileOrderEntry = NO;
    popoverContent.isFromOfflineOrder = YES;
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    popoverController.delegate = self;
    popoverContent.delegate = self;
    
    
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_x_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect rectBtnMenu = CGRectMake(btnAddMenu.frame.origin.x, self.view.frame.size.height - 40,btnAddMenu.frame.size.width,btnAddMenu.frame.size.height);
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:rectBtnMenu
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}


//Delegate Method
- (void)actionSaveQuote:(int)finalizeTag{
    
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts) {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        if ([dict objectForKey:@"Item"] || [dict objectForKey:@"ITEM"] || [dict objectForKey:@"StockCode"]){
            
            if (![dict objectForKey:@"ExtnPrice"] && ([[dict objectForKey:@"Item"] isEqualToString:@"/C"] || [[dict objectForKey:@"ITEM"] isEqualToString:@"/C"])) {
                isCalculationPending = FALSE;
                break;
            }
            else if (![dict objectForKey:@"QUANTITY"]&& (![[dict objectForKey:@"Item"] isEqualToString:@"/C"] || ![[dict objectForKey:@"ITEM"] isEqualToString:@"/C"])) {
                isCalculationPending = TRUE;
                  break;
            }
            else if ([dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = FALSE;
                break;
            }
            else{
                isCalculationPending = FALSE;
            }
        }
    }
    
    
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Successfull" message:@"Data store successfully" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
       // alert.tag=TAG_50;

    }
  
    [self updateOfflineDetail_data];
   
}


-(NSString *)getOrderNumber:(NSMutableArray *)arrProd{
 NSString *str = @"";
    for (int i = 0; i < arrProd.count; i++) {
        if ([[arrProd objectAtIndex:i] isKindOfClass:[NSNull class ]]) {
        }
        else{
            str = [NSString stringWithFormat:@"%@",[arrProd objectAtIndex:i]];
            return str;
        }
    }
    return str?str:@"";
}



-(void)updateOfflineDetail_data{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        @try {
           
            NSString *strOrderNum1 = [self getOrderNumber:[arrProducts valueForKey:@"ORDER_NO"]];
            if(strOrderNum1.length > 0){
                FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) FROM sodetail_offline where ORDER_NO = ?",strOrderNum1];
                
                if (!rs1)
                {
                    [rs1 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                while ([rs1 next]) {
                    NSDictionary *dict = [rs1 resultDictionary];
                    line_num = [[dict objectForKey:@"MAX(LINE_NO)"]integerValue];
                    NSLog(@"%d",line_num);
                     }
                line_num=line_num+1;
            }

            
            for(int i = 0; [arrProducts count] > 0;i++){
                
                if (([[[arrProducts objectAtIndex:i] valueForKey:@"isEdit"]isEqualToString:@"1"]) && (![[[arrProducts objectAtIndex:i] valueForKey:@"isNewProduct"]isEqualToString:@"1"])) {
                    
                    FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM sodetail_offline where DEBTOR = ? AND ITEM = ? AND ORDER_NO = ?",strDebtor,[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"],strOrderNum1];
                    
                    
                    if (!rs1)
                    {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    NSMutableDictionary *dictData = [NSMutableDictionary new];
                    while ([rs1 next]) {
                        dictData = [[rs1 resultDictionary] mutableCopy];
                    }
                    
                    [rs1 close];
                    // Update Sodetail_offline

                    float qty= [[[arrProducts objectAtIndex:i]objectForKey:@"QUANTITY"] floatValue];
                    float tax =[[[arrProducts objectAtIndex:i]objectForKey:@"TAX"] floatValue];
                    float gross = [[[arrProducts objectAtIndex:i]objectForKey:@"GROSS"] floatValue];
                    float valPrice  = [[[arrProducts objectAtIndex:i]objectForKey:@"PRICE"] floatValue];
                    float extension = [[[arrProducts objectAtIndex:i]objectForKey:@"EXTENSION"]floatValue];


                    
                    BOOL y2 ;

                    
                    y2 = [db executeUpdate:@"UPDATE sodetail_offline SET ORD_QTY = ? , QUANTITY =?  , ALT_QTY = ?  , TAX = ?  , GROSS = ? , PRICE = ? , EXTENSION = ?  WHERE DEBTOR = ?  AND ITEM = ? AND ORDER_NO = ?",[NSNumber numberWithFloat:qty] ,[NSNumber numberWithFloat:qty],[NSNumber numberWithFloat:qty],[NSNumber numberWithFloat:tax],[NSNumber numberWithFloat:gross],[NSNumber numberWithFloat:valPrice],[NSNumber numberWithFloat:extension],strDebtor,[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"],strOrderNum1];
                    

                    if (!y2)
                    {
                        [db close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    else{
                        NSLog(@"Product updated!!");
                        [_tblDetails reloadData];
                    }
                    
                }
                //comment
                else if (([[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"] isEqualToString:@"/C"] || [[[arrProducts objectAtIndex:i]objectForKey:@"Item"] isEqualToString:@"/C"] )&& [[[arrProducts objectAtIndex:i]objectForKey:@"isEdit"] isEqualToString:@"1"]){
                    
                    
                    NSString *itemVal = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:i]objectForKey:@"Item"]?[[arrProducts objectAtIndex:i]objectForKey:@"Item"]:[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"]];
                    
                    FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM sodetail_offline where DEBTOR = ? AND ITEM = ? AND ORDER_NO = ? ",strDebtor,itemVal,strOrderNum1];
                    
                    if (!rs1)
                    {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    NSMutableDictionary *dictData = [NSMutableDictionary new];
                    while ([rs1 next]) {
                        dictData = [[rs1 resultDictionary] mutableCopy];
                    }
                    
                    [rs1 close];
                    // Update Sodetail_offline
                    
                    if (dictData.count > 0 ) {
                        NSString *strDescription =[NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:i]objectForKey:@"Description"]?[[arrProducts objectAtIndex:i]objectForKey:@"Description"]:[[arrProducts objectAtIndex:i]objectForKey:@"DESCRIPTION"]];
                        
                        
                        
                        BOOL y2 ;
                        
                        y2 = [db executeUpdate:@"UPDATE sodetail_offline SET  DESCRIPTION = ?  WHERE DEBTOR = ?  AND ITEM = ? AND ORDER_NO = ?",strDescription,strDebtor,itemVal,strOrderNum1];
                        
                        if (!y2)
                        {
                            [db close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        else{
                            NSLog(@"Comment updated!!");
                            [_tblDetails reloadData];
                        }
                    }
                    else{
                        
                        NSString *strStockCode1 = nil;
                        NSString *strPrice = @"0";
                        NSString *strDissection = @"0";
                        NSString *strDissection_Cos = @"0";
                        NSString *strWeight = @"0";
                        NSString *strVolume = @"0";
                        NSString *strCost = @"0";
                        NSString *strExtension = @"0";
                        NSString *strGross = @"0";
                        NSString *strTax = @"0";
                        NSString *strQuantityOrdered = @"0";
                        
                        NSString *strAltQuantity = @"0";
                        NSString *strCUST_PRICE = @"0";
                        NSString *strCUST_ORD_QTY  = @"0";
                        NSString *strCUST_SHIP_QTY = @"0";
                        NSString *strCUST_VOLUME = @"0";
                        NSString *strPRICING_UNIT = @"0";
                        NSString *strCharge_Type = @"";
                        NSString *strRelease_New = @"Y";
                        //int line_num = 0;
                        
                        
                        //Comments
                        if ([[[arrProducts objectAtIndex:i] objectForKey:@"ITEM"] isEqualToString:@"/C"] ||[[[arrProducts objectAtIndex:i] objectForKey:@"Item"] isEqualToString:@"/C"] ||[[[arrProducts objectAtIndex:i] objectForKey:@"StockCode"] isEqualToString:@"/C"] )
                        {
                          //  line_num++;
                            
                            strStockCode1 = @"/C";
                            
                            strQuantityOrdered = @"1.00";
                            self.strConvFactor = @"0.0000";
                            strRelease_New = @"Y";
                            
                            strDissection = @"/C";
                            strDissection_Cos = @"";
                            
                            self.strRefreshPrice = @"";
                            
                            
                            //Insert
                            NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                            BOOL y11 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum1,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[[arrProducts objectAtIndex:i] objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,[[arrProducts objectAtIndex:i] objectForKey:@"QUANTITY"],strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,@"",@"",strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                            
                            if (!y11)
                            {
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                break;
                            }
                        }
                    }
                }
                
                // Freight
                else if (([[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"] isEqualToString:@"/F"] || [[[arrProducts objectAtIndex:i]objectForKey:@"Item"] isEqualToString:@"/F"]) && ([[[arrProducts objectAtIndex:i]objectForKey:@"isEdit"] isEqualToString:@"1"])){
                    
                    NSString *itemVal = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:i]objectForKey:@"Item"]?[[arrProducts objectAtIndex:i]objectForKey:@"Item"]:[[arrProducts objectAtIndex:i]objectForKey:@"ITEM"]];
                    
                    FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM sodetail_offline where DEBTOR = ? AND ITEM = ? AND ORDER_NO = ? ",strDebtor,itemVal,strOrderNum1];
                    
                    if (!rs1)
                    {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    NSMutableDictionary *dictData = [NSMutableDictionary new];
                    while ([rs1 next]) {
                        dictData = [[rs1 resultDictionary] mutableCopy];
                    }
                    
                    [rs1 close];
                    // Update Sodetail_offline
                    if (dictData.count > 0) {
                        
                    }
                    else{
//                        NSString *strStockCode1 = nil;
                        NSString *strPrice = @"0";
                        NSString *strDissection = @"0";
                        NSString *strDissection_Cos = @"0";
                        NSString *strWeight = @"0";
                        NSString *strVolume = @"0";
                        NSString *strCost = @"0";
                        NSString *strExtension = @"0";
                        NSString *strGross = @"0";
//                        NSString *strTax = @"0";
                        NSString *strQuantityOrdered = @"0";
                        
                        NSString *strAltQuantity = @"0";
                        NSString *strCUST_PRICE = @"0";
                        NSString *strCUST_ORD_QTY  = @"0";
                        NSString *strCUST_SHIP_QTY = @"0";
                        NSString *strCUST_VOLUME = @"0";
                        NSString *strPRICING_UNIT = @"0";
                        NSString *strCharge_Type = @"";
                        NSString *strRelease_New = @"Y";
                       // int line_num = 0;
                        
                      /*  if(strOrderNum1.length > 0){
                            FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) as LINE_NO FROM sodetail_offline where ORDER_NO = ? LIMIT 1",strOrderNum1];
                            
                            if (!rs1)
                            {
                                [rs1 close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            while ([rs1 next]) {
                                NSDictionary *dict = [rs1 resultDictionary];
                                line_num = [[dict objectForKey:@"LINE_NO"]integerValue];
                            }
                        }*/
                        if ([[[arrProducts objectAtIndex:i] objectForKey:@"ITEM"] isEqualToString:@"/F"] ||[[[arrProducts objectAtIndex:i] objectForKey:@"Item"] isEqualToString:@"/F"] )
                        {
                        line_num++;
                        NSString *strStockCodenew = @"/F";
                        
                        strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
                        strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
                        strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
                        strExtension = [NSString stringWithFormat:@"%f",freightValue];
                        NSString* strfreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
                        
                        
                        strAltQuantity = @"0.00";
                        strQuantityOrdered = @"1.00";
                        self.strConvFactor = @"0.0000";
                        
                        strCUST_PRICE = strPrice;
                        strCUST_ORD_QTY  = @"1";
                        strCUST_SHIP_QTY = @"1";
                        strCUST_VOLUME = @"1";
                        strPRICING_UNIT = @"1";
                        strCharge_Type = @"N";
                        strRelease_New = @"Y";
                        
                        strDissection = @"/F";
                        strDissection_Cos = @"";
                            
                        self.strRefreshPrice = @"";
                        self.strStatus = STATUS;
                        self.strConvFactor = @"1.0000";
                        self.strDecimalPlaces = @"";
                        self.strTotalCartonQty = self.strBOQty = self.strOrigCost = @"0.0000";
                          strEditStatus = @"";
                            isDeliveryRunSlected = NO;
                            segControl.enabled = NO;
                            isRecordsExist = FALSE;
                            totalCost = 0;

                        
                        //Insert
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?, ?,?,?,?)", strOrderNum1,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[[arrProducts objectAtIndex:i] objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strSalesman,@"",@"",strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        else{
                            NSLog(@"Freight Inserted");
                        }
                        }

                    }
                    
                    
                }
             
                // Insert the new value
                else if([[[arrProducts objectAtIndex:i] valueForKey:@"isNewProduct"] isEqualToString:@"1"] && [[[arrProducts objectAtIndex:i] valueForKey:@"isEdit"]isEqualToString:@"1"] ){
                    
//                    NSString *strStockCode1 = nil;
//                    NSString *strPrice = @"0";
//                    NSString *strDissection = @"0";
//                    NSString *strDissection_Cos = @"0";
//                    NSString *strWeight = @"0";
//                    NSString *strVolume = @"0";
//                    NSString *strCost = @"0";
//                    NSString *strExtension = @"0";
//                    NSString *strGross = @"0";
//                    NSString *strTax = @"0";
//                    NSString *strQuantityOrdered = @"0";
//                    
//                    NSString *strAltQuantity = @"0";
//                    NSString *strCUST_PRICE = @"0";
//                    NSString *strCUST_ORD_QTY  = @"0";
//                    NSString *strCUST_SHIP_QTY = @"0";
//                    NSString *strCUST_VOLUME = @"0";
//                    NSString *strPRICING_UNIT = @"0";
//                    NSString *strCharge_Type = @"";
//                    NSString *strRelease_New = @"Y";
//                    int line_num = 0;
//                    
//                 
//                    if(strOrderNum1.length > 0){
//                        FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) FROM sodetail_offline where ORDER_NO = ?",strOrderNum1];
//                        
//                        if (!rs1)
//                        {
//                            [rs1 close];
//                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//                        }
//                        while ([rs1 next]) {
//                            line_num = [rs1 stringForColumn:@"LINE_NO"];
//                        }
//                    }
//                   
                    
//                    //Comments
//                    if ([[[arrProducts objectAtIndex:i] objectForKey:@"ITEM"] isEqualToString:@"/C"] ||[[[arrProducts objectAtIndex:i] objectForKey:@"Item"] isEqualToString:@"/C"] ||[[[arrProducts objectAtIndex:i] objectForKey:@"StockCode"] isEqualToString:@"/C"] )
//                    {
//                        line_num++;
//                        
//                        strStockCode1 = @"/C";
//                        
//                        strQuantityOrdered = @"1.00";
//                        self.strConvFactor = @"0.0000";
//                        strRelease_New = @"Y";
//                        
//                        strDissection = @"/C";
//                        strDissection_Cos = @"";
//                        
//                        self.strRefreshPrice = @"";
//                        
//                    
//                            //Insert
//                            NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
//                            BOOL y11 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum1,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[[arrProducts objectAtIndex:i] objectForKey:@"DESCRIPTION"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,[[arrProducts objectAtIndex:i] objectForKey:@"QUANTITY"],strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,@"",@"",strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
//                            
//                            if (!y11)
//                            {
//                                
//                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//                                break;
//                            }
//                            
//                            
// 
//                }
//                    //Freight
//                    else if ([[arrProducts objectAtIndex:i] objectForKey:@"Item"] && [[[arrProducts objectAtIndex:i] objectForKey:@"ITEM"] isEqualToString:@"/F"])
//                    {
//                        line_num++;
//                        
//                        NSString *strStockCodenew = @"/F";
//                        
//                        strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
//                        strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
//                        strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
//                        strExtension = [NSString stringWithFormat:@"%f",freightValue];
//                        NSString* strfreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
//                        
//                        
//                        strAltQuantity = @"0.00";
//                        strQuantityOrdered = @"1.00";
//                        self.strConvFactor = @"0.0000";
//                        
//                        strCUST_PRICE = strPrice;
//                        strCUST_ORD_QTY  = @"1";
//                        strCUST_SHIP_QTY = @"1";
//                        strCUST_VOLUME = @"1";
//                        strPRICING_UNIT = @"1";
//                        strCharge_Type = @"N";
//                        strRelease_New = @"Y";
//                        
//                        strDissection = @"/F";
//                        strDissection_Cos = @"";
//                        self.strRefreshPrice = @"";
//                  
//                            //Insert
//                            NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
//                            BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?, ?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[[arrProducts objectAtIndex:i] objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strSalesman,@"",@"",strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
//                            
//                            if (!y1)
//                            {
//                                
//                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//                                break;
//                            }
//                            
//                            
//                    }
//                    else{
//                        NSString *strStockCode1 = nil;
                    
                    
                        NSString *strPrice = @"0";
                        NSString *strDissection = @"0";
                        NSString *strDissection_Cos = @"0";
                        NSString *strWeight = @"0";
                        NSString *strVolume = @"0";
                        NSString *strCost = @"0";
                        NSString *strExtension = @"0";
                        NSString *strGross = @"0";
                        NSString *strTax = @"0";
                        NSString *strQuantityOrdered = @"0";
                        
//                        NSString *strAltQuantity = @"0";
                        NSString *strCUST_PRICE = @"0";
                        NSString *strCUST_ORD_QTY  = @"0";
                        NSString *strCUST_SHIP_QTY = @"0";
                        NSString *strCUST_VOLUME = @"0";
                        NSString *strPRICING_UNIT = @"0";
                        NSString *strCharge_Type = @"";
                        NSString *strRelease_New = @"Y";
                    self.strRefreshPrice = @"";
                    self.strStatus = STATUS;
                    self.strConvFactor = @"1.0000";
                    self.strDecimalPlaces = @"";
                    self.strTotalCartonQty = self.strBOQty = self.strOrigCost = @"0.0000";
                    strEditStatus = @"";
                        //int line_num = 0;
                        NSString *strStockCodenew = @"";
                    
                       /* if(strOrderNum1.length > 0){
                            FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) FROM sodetail_offline where ORDER_NO = ?",strOrderNum1];
                            
                            if (!rs1)
                            {
                                [rs1 close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            while ([rs1 next]) {
                                NSDictionary *dict = [rs1 resultDictionary];
                                line_num = [[dict objectForKey:@"MAX(LINE_NO)"]integerValue];
                                NSLog(@"%d",[[dict objectForKey:@"MAX(LINE_NO)"]integerValue]);
                            }
                            line_num=line_num+1;
                        }*/
                        
                            if ([[arrProducts objectAtIndex:i] objectForKey:@"Item"]) {
                                strStockCodenew = [[arrProducts objectAtIndex:i] objectForKey:@"Item"];
                            }
                            else if ([[arrProducts objectAtIndex:i] objectForKey:@"StockCode"]){
                                strStockCodenew = [[arrProducts objectAtIndex:i] objectForKey:@"StockCode"];
                            }
                            
                            if ([[arrProducts objectAtIndex:i] objectForKey:@"Price"]) {
                                strPrice = [[arrProducts objectAtIndex:i] objectForKey:@"Price"];
                            }
                            
                            if ([[arrProducts objectAtIndex:i] objectForKey:@"ExtnPrice"])
                            {
                                //--check for price is greater than zero
                                NSString *strExt = [[arrProducts objectAtIndex:i] objectForKey:@"ExtnPrice"];
                                if(strExt.integerValue > 0)
                                {
                                    //line_num++;
                                    strDissection = [[arrProducts objectAtIndex:i] objectForKey:@"Dissection"];
                                    strDissection_Cos = [[arrProducts objectAtIndex:i] objectForKey:@"Dissection_Cos"];
                                    strWeight = [[arrProducts objectAtIndex:i] objectForKey:@"Weight"];
                                    strVolume = [[arrProducts objectAtIndex:i] objectForKey:@"Volume"];
                                    strCost = [[arrProducts objectAtIndex:i] objectForKey:@"AverageCost"];
                                    strExtension = [[arrProducts objectAtIndex:i] objectForKey:@"ExtnPrice"];
                                    
                                    strQuantityOrdered = [[arrProducts objectAtIndex:i] objectForKey:@"QUANTITY"];
                                    
                                    strWeight = [[arrProducts objectAtIndex:i] objectForKey:@"Weight"];
                                    
                                    strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                                    
                                    
                                    strVolume = [[arrProducts objectAtIndex:i] objectForKey:@"Volume"];
                                    
                                    strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                                    
                                    
                                    strGross = [[arrProducts objectAtIndex:i] objectForKey:@"Gross"];
                                    strTax = [[arrProducts objectAtIndex:i] objectForKey:@"Tax"];
                                    
                                    self.strDecimalPlaces = @"2";
                                    
                                    strCUST_PRICE = strPrice;
                                    strCUST_ORD_QTY  = strQuantityOrdered;
                                    strCUST_SHIP_QTY = strQuantityOrdered;
                                    strCUST_VOLUME = @"1";
                                    strPRICING_UNIT = @"1";
                                    strCharge_Type = @"";
                                    strRelease_New = @"Y";
                                    self.strRefreshPrice = @"N";
                                    self.strConvFactor = @"1.0000";
                                }
                            }
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                    BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?,?,?,?,?)", strOrderNum1,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[[arrProducts objectAtIndex:i] objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax?strTax:@"",strSalesman,@"",@"",strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        else{
                            NSLog(@"New Prduct added");
                          /*  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ok" message:@"" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
                            alert.tag = TAG_50;*/
                        }

//                    }
                }
            }
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Successfull" message:@"Data store successfully" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
//            [alert show];
        }
        @catch(NSException *e){
            
        }
    }];
}

-(void)deleteProductFromOrder:(NSString *)strOrderNo stockCode:(NSString *)strItem
{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            
            //Delete the record from offline db
            BOOL y1 =  [db executeUpdate:@"DELETE FROM `sodetail_offline` WHERE `ORDER_NO` = ? AND `ITEM` = ?",strOrderNo,strItem];
            
            
            //[arrProducts removeObjectAtIndex:countval];
            
            if (!y1)
            {
                isSomethingWrongHappened = TRUE;
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:@"%@ deleted",strItem] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            });
            
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            });
            
            
        }
        
        
    }];
    
}


#pragma mark - PopOverDelegates
//Add Products
- (void)showProductList{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

-(void)showSpecials
{
    NSLog(@"Specials Clicked");
    [self btnShowSpecials:nil];
}

-(void)dismissPopOver{
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    [popoverController dismissPopoverAnimated:YES];
    
    [self viewWillAppear:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [btnAddMenu setImage:[UIImage imageNamed:@"red_plus_down.png"] forState:UIControlStateNormal];
    [[self.view findFirstResponder] resignFirstResponder];
}


#pragma mark - Handle Notification
- (void)reloadTableViewData:(NSNotification *)notification {
    
    isSearch = NO;
    
    NSMutableDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
    
    //NSLog(@"dictSelectedProduct : %@",dictSelectedProduct);
    
    
    //    __block BOOL isScrollToBottom = YES;
    __block BOOL isScrollToBottom = YES;
    
    //Comments
    if ([[dictSelectedProduct objectForKey:@"ITEM"] isEqualToString:@"/C"] || [[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
        //[dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
        //[arrProducts addObject:dictSelectedProduct];
    }
    //Freight
    else if ([[dictSelectedProduct objectForKey:@"ITEM"] isEqualToString:@"/F"] || [[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/F"]) {
        [dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
        //--Code alwas add the Frieght at end
        [arrProducts insertObject:dictSelectedProduct atIndex:[arrProducts count]];
//                [arrProducts addObject:dictSelectedProduct];
    }
    else{
        
        __block BOOL isProductExist = FALSE;
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            isProductExist = [self callGetProductFromCustProfileDB:db ProductCode:[dictSelectedProduct objectForKey:@"ITEM"]];
        }];
        
        if (isProductExist) {
            NSString *strMessage = [NSString stringWithFormat:@"\"%@\"\n is already present in the Profile!",[dictSelectedProduct objectForKey:@"Description"]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            strMessage = nil;
        }
        else{
            [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
            
            __block BOOL toRemove = NO;
            if ([arrProducts count]) {
                
                [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if ([[obj objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]] || [[obj objectForKey:@"Item"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]]) {
                        
                        if ([[obj objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
                            
                        }
                        else{
                            if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
                                [arrProducts removeObjectAtIndex:idx];
                            }
                            
                        }
                        isScrollToBottom = NO;
                        toRemove = YES;
                        *stop = YES;
                    }
                    else{
                        toRemove = NO;
                    }
                }];
                
            }
            else{
                toRemove = NO;
                
            }
            
            if (!toRemove)
            {
                [arrProducts insertObject:dictSelectedProduct atIndex:0];
                //                [arrProducts addObject:dictSelectedProduct];
            }
            
        }
    }
    
    if ([arrProducts count] > cntProfileOrders) {
        //Show delete button
        btnDelete.hidden = NO;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
    }
    else{
        btnDelete.hidden = YES;
    }
    
    int lastIndex = [arrProducts count];
    if (lastIndex) {
        
        [self showOrEnableButtons];
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"ITEM"] isEqualToString:@"/C"] || [[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                }
            }];
        }
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [_tblDetails setContentOffset:CGPointMake(0, 0)];
        [_tblDetails reloadData];
        
     
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
}
 
 /*
- (void)reloadTableViewData:(NSNotification *)notification {
 [segControl setSelectedIndex:1];
 
 NSMutableDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
 
 //NSLog(@"dictSelectedProduct : %@",dictSelectedProduct);
 
 __block BOOL isScrollToBottom = YES;
 //Comments
 if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
 //[dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
 //[arrProducts addObject:dictSelectedProduct];
 }
 //Freight
 else if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/F"]) {
 [dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
 [arrProducts addObject:dictSelectedProduct];
 }
 else{
 
 __block BOOL isProductExist = FALSE;
 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
 [databaseQueue   inDatabase:^(FMDatabase *db) {
 isProductExist = [self callGetProductFromCustProfileDB:db ProductCode:[dictSelectedProduct objectForKey:@"StockCode"]];
 }];
 
 if (isProductExist) {
 NSString *strMessage = [NSString stringWithFormat:@"\"%@\"\n is already present in the Profile!",[dictSelectedProduct objectForKey:@"Description"]];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
 [alert show];
 strMessage = nil;
 }
 else{
 [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
 
 __block BOOL toRemove = NO;
 if ([arrProducts count]) {
 
 [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
 if ([[obj objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]] || [[obj objectForKey:@"Item"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]]) {
 
 if ([[obj objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
 
 }
 else{
 if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
 [arrProducts removeObjectAtIndex:idx];
 }
 
 }
 isScrollToBottom = NO;
 toRemove = YES;
 *stop = YES;
 }
 else{
 toRemove = NO;
 }
 }];
 
 }
 else{
 toRemove = NO;
 
 }
 
 if (!toRemove) {
 [arrProducts addObject:dictSelectedProduct];
 }
 
 }
 
 }
 
 if ([arrProducts count] > cntProfileOrders) {
 //Show delete button
 btnDelete.hidden = NO;
 _tblDetails.editing = NO;
 [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
 [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
 }
 else{
 btnDelete.hidden = YES;
 }
 
 int lastIndex = [arrProducts count];
 if (lastIndex) {
 
 [self showOrEnableButtons];
 
 if (isCommentAddedOnLastLine) {
 [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
 if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
 id object = [arrProducts objectAtIndex:idx];
 [arrProducts removeObjectAtIndex:idx];
 [arrProducts insertObject:object atIndex:lastIndex-1];
 *stop = YES;
 }
 }];
 }
 
 [vwContentSuperview removeAllSubviews];
 [vwContentSuperview addSubview:vwDetails];
 
 [_tblDetails reloadData];
 
 if (_loadMoreFooterView == nil) {
 [self setupLoadMoreFooterView];
 }
 
 [self repositionLoadMoreFooterView];
 
 if (isScrollToBottom) {
 NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
 [_tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
 }
 
 }
 else{
 [vwContentSuperview removeAllSubviews];
 [vwContentSuperview addSubview:vwNoProducts];
 
 [_loadMoreFooterView removeFromSuperview];
 _loadMoreFooterView=nil;
 }
 }

 
 */


- (void)reloadHeader:(NSNotification *)notification {
    
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    //Show the header
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    [operationQueue cancelAllOperations];
    
    [segControl setSelectedIndex:0];
    
    self.dictHeaderDetails = [notification.userInfo valueForKey:@"source"];
    
    //    NSLog(@"dictHeaderDetails : %@",dictHeaderDetails);
    //    if ([[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"DelName"] ) {
    if ([[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"] ) {
        
        //        _lblCustName.text = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"DelName"];
        _lblCustName.text = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"];
        
        
    }
    //    dfsdfs
    
    [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"]) {
        
        [self prepareHeaderData];
        
        //Comments Exist
        if ([[[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Flag"] isEqualToString:@"True"]) {
            // Path to the plist (in the application bundle)
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"ProfileOrderEntryHeaderWithComments" Type:@"plist"];
            
            // Build the array from the plist
            arrHeaderLabels = [NSArray arrayWithContentsOfFile:path];
        }
        else{
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"OfflineOrderHeader" Type:@"plist"];
            
            // Build the array from the plist
            arrHeaderLabels = [NSArray arrayWithContentsOfFile:path];
        }
    }
    else{
        segControl.enabled = NO;
    }
    
    //    NSLog(@"dictHeaderDetails %@",dictHeaderDetails);
    [_tblHeader reloadData];
    isSearch = NO;
    [arrSearchProducts removeAllObjects];
    //  [_tblDetails reloadData];
}


#pragma mark - Long Press
-(void)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:_tblDetails];
    
    NSIndexPath *indexPath = [_tblDetails indexPathForRowAtPoint:p];
    
    //NSLog(@"%d",indexPath.row);
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    
    if(isSearch)
    {
        // NSLog(@"%@",[arrSearchProducts objectAtIndex:indexPath.row]);
        strStockCode = [[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    else
    {
        // NSLog(@"%@",[arrProducts objectAtIndex:indexPath.row]);
        strStockCode = [[arrProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSURL *url = [NSURL URLWithString:[AppDelegate getServiceURL:@"webservices_rcf/profordentry/arhisdetlist.php"]];
            
            
            ASIFormDataRequest *request1 = [ASIFormDataRequest requestWithURL:url];
            __weak ASIFormDataRequest *request = request1;
            [request addPostValue:strDebtor forKey:@"customer"];
            [request addPostValue:strStockCode forKey:@"stock_code"];
            //            [request addPostValue:@"PRIEST01" forKey:@"customer"];
            //            [request addPostValue:@"BAKEP2" forKey:@"stock_code"];
            [request startAsynchronous];
            
            [request setCompletionBlock:^{
                // Use when fetching text data
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                NSString *responseString = [request responseString];
                
                NSDictionary *dictResponse = [XMLReader dictionaryForXMLString:responseString error:nil];
                
                //NSLog(@"dictResponse %@",dictResponse);
                
                NSString *strFalg = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Flag"] objectForKey:@"text"];
                
                if([strFalg isEqualToString:@"True"])
                {
                    if(!arrProductPurchaseHistory)
                    {
                        arrProductPurchaseHistory = [NSMutableArray new];
                    }
                    else
                    {
                        [arrProductPurchaseHistory removeAllObjects];
                    }
                    
                    NSMutableArray *arrTempArr = [NSMutableArray new];
                    
                    if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]])
                    {
                        [arrTempArr addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"]];
                    }
                    else
                    {
                        arrTempArr = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"];
                    }
                    
                    if([arrTempArr count] > 0)
                    {
                        for (int i=0;i<[arrTempArr count];i++)
                        {
                            NSLog(@"%@",[arrTempArr objectAtIndex:i]);
                            NSMutableDictionary *arrTempDict = [NSMutableDictionary new];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Price"] objectForKey:@"text"] forKey:@"Price"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"DateRaised"] objectForKey:@"text"] forKey:@"DateRaised"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"InvNo"] objectForKey:@"text"] forKey:@"InvNo"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Quantity"] objectForKey:@"text"] forKey:@"Quantity"];
                            
                            [arrProductPurchaseHistory addObject:arrTempDict];
                            
                        }
                        //[arrTempArr removeAllObjects];
                    }
                    
                    if([arrProductPurchaseHistory count] > 0)
                    {
                        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                        
                        tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                        [tblProductHistory setDataSource:self];
                        [tblProductHistory setDelegate:self];
                        tblProductHistory.tag = TAG_999;
                        
                        [contentView addSubview:tblProductHistory];
                        
                        [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                        [tblProductHistory reloadData];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                _longPress = NO;
            }];
            [request setFailedBlock:^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                //NSError *error = [request error];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Error loading the details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                _longPress = NO;
            }];
            
        }
    }
    else
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            //Get the data from DB
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                
                FMResultSet *rs1 = [db executeQuery:@"SELECT TRIM(DEBTOR), TRIM(STOCK_CODE), TRIM(QUANTITY) as QUANTITY, TRIM(PRICE) as PRICE, TRIM(DATE_RAISED) as DATE_RAISED, TRIM(TRAN_NO) as TRAN_NO from arhisdet WHERE DEBTOR = ? AND STOCK_CODE = ? ORDER BY DATE_RAISED desc",strDebtor,strStockCode];
                
                if (!rs1)
                {
                    [rs1 close];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to fetch the records." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                if(!arrProductPurchaseHistory)
                {
                    arrProductPurchaseHistory = [NSMutableArray new];
                }
                else
                {
                    [arrProductPurchaseHistory removeAllObjects];
                }
                
                while ([rs1 next])
                {
                    NSString *strDate = [rs1 stringForColumn:@"DATE_RAISED"];
                    
                    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                    [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *theDate = [inputFormatter dateFromString:strDate];
                    
                    NSDateFormatter *inputFormatterNew = [[NSDateFormatter alloc] init];
                    [inputFormatterNew setDateFormat:@"dd/MM/yy"];
                    
                    NSString *strNewDate = [inputFormatterNew stringFromDate:theDate];
                    
                    NSMutableDictionary *tempDict = [NSMutableDictionary new];
                    [tempDict setValue:[rs1 stringForColumn:@"PRICE"] forKey:@"Price"];
                    [tempDict setValue:strNewDate forKey:@"DateRaised"];
                    [tempDict setValue:[rs1 stringForColumn:@"TRAN_NO"] forKey:@"InvNo"];
                    [tempDict setValue:[rs1 stringForColumn:@"QUANTITY"] forKey:@"Quantity"];
                    
                    [arrProductPurchaseHistory addObject:tempDict];
                }
                
                if([arrProductPurchaseHistory count] > 0)
                {
                    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                    
                    tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                    [tblProductHistory setDataSource:self];
                    [tblProductHistory setDelegate:self];
                    tblProductHistory.tag = TAG_999;
                    
                    [contentView addSubview:tblProductHistory];
                    
                    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                    [tblProductHistory reloadData];
                    
                    _longPress = NO;
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                    
                    _longPress = NO;
                }
                
                [rs1 close];
            }];
        }
    }
}

- (void)changeCloseButtonType:(id)sender{
    UIButton *button = (UIButton *)sender;
    KGModal *modal = [KGModal sharedInstance];
    KGModalCloseButtonType type = modal.closeButtonType;
    
    if(type == KGModalCloseButtonTypeLeft){
        modal.closeButtonType = KGModalCloseButtonTypeRight;
        [button setTitle:@"Close Button Right" forState:UIControlStateNormal];
    }else if(type == KGModalCloseButtonTypeRight){
        modal.closeButtonType = KGModalCloseButtonTypeNone;
        [button setTitle:@"Close Button None" forState:UIControlStateNormal];
    }else{
        modal.closeButtonType = KGModalCloseButtonTypeLeft;
        [button setTitle:@"Close Button Left" forState:UIControlStateNormal];
    }
}


#pragma mark -  Segmented Control Actions


-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
   // [vwContentSuperview removeAllSubviews];
    btnDelete.hidden = NO; // btnDelete.hidden = YES;//19thsept
    
    [operationQueue cancelAllOperations];
    
    switch (sender.SelectedIndex) {
        case 0:{
            vwHeader.hidden = NO;
            vwDetails.hidden = YES;
            [self.tblDetails reloadData];

            //     strPrevDebtor = strDebtor;
           // [vwContentSuperview addSubview:vwHeader];
        }
            break;
            
        case 1:{
            lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
            NSLog(@"%@",lblTotalCost.text);
            [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:totalCost];
            vwHeader.hidden = YES;
            vwDetails.hidden = NO;
            [self.tblDetails reloadData];

            return;
            if (isDeliveryRunSlected) {
                if (![strPrevDebtor isEqualToString:strDebtor]) {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    recordNumber = 0;
                    
                    
                    [arrProducts removeAllObjects];
                    [operationQueue addOperationWithBlock:^{
//                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//                        [databaseQueue   inDatabase:^(FMDatabase *db) {
////                            [self callViewQuoteDetailsDetailsFromDB:db];
//                        }];
                        
                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [self prepareView];
                            [_tblDetails reloadData];
                            
                            //[self doAfterDataFetch];
                            
                            // NSLog(@"arrProducts %@",arrProducts);
                        }];
                    }];
                    
                }
                else{
                    [self prepareView];
                }
            }
            else{
                
                [self.tblDetails reloadData];
                [vwContentSuperview addSubview:vwDetails];
                //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select delivery run!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //                alert.tag = TAG_1300;
                //                [alert show];
            }
            
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 0) {
                return nil;
            }
            else if (indexPath.section == 2 && [indexPath row]!=9) {
               return indexPath;

//                NSString *stringToCheck = nil;
//                switch ([indexPath row]) {
//                    case 0:
//                    {
//                        stringToCheck = strName;
//                    }
//                        break;
//                    case 2:
//                    {
//                        stringToCheck = strContact;
//                    }
//                        break;
//                    case 3:
//                    {
//                        stringToCheck = strEmail;
//                    }
//                        break;
//                }
//                
//                //We only don't want to allow selection on any cells which cannot be expanded
//                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
//                {
//                    return indexPath;
//                }
//                else {
//                    return nil;
//                }
            }
            else if (indexPath.section == 3 && [indexPath row]!=8) {
                return indexPath;

            }
            else if (indexPath.section == 4) {
                return nil;
            }
            else if (indexPath.section == 5) {
                return nil;
            }
            else if (indexPath.section == 6) {
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strCommenttxt;
                    }
                        break;
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                //                COMMENT_LABEL_MIN_HEIGHT
                if([self getLabelHeightForString:stringToCheck] > 2)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
                
            }
            
            else{
                return indexPath;
            }
            
            
            
        }break;
            
        case TAG_100:return indexPath;break;

        default:return nil;break;
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strName;
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = strContact;
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = strEmail;
                    }
                        break;
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                    //return 60;
                }
            }
            else{
                return 60;break;
            }
            
            
            
        }break;
            
        case TAG_100:return 50;break;
            
        case TAG_999:return 50;break;
            
        default:return 0;break;
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == TAG_999)
    {
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 560, 80)];
        [tempView setBackgroundColor:[UIColor lightGrayColor]];
        
        CGSize maximumSize = CGSizeMake(300, 9999);
        NSString *myString = [NSString stringWithFormat:@"Customer: %@",strDebtor];
        UIFont *myFont = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize = [myString sizeWithFont:myFont
                                   constrainedToSize:maximumSize
                                       lineBreakMode:NSLineBreakByWordWrapping];
        
        CGSize maximumSize2 = CGSizeMake(300, 9999);
        NSString *myString2 = [NSString stringWithFormat:@"Stock Code: %@",strStockCode];
        UIFont *myFont2 = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize2 = [myString2 sizeWithFont:myFont2
                                     constrainedToSize:maximumSize2
                                         lineBreakMode:NSLineBreakByWordWrapping];
        
        UILabel *lblDebtor = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, myStringSize.width, myStringSize.height)];
        [lblDebtor setBackgroundColor:[UIColor clearColor]];
        [lblDebtor setText:myString];
        [lblDebtor setTextAlignment:NSTextAlignmentCenter];
        [lblDebtor setTextColor:[UIColor blackColor]];
        [lblDebtor setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        int x = 552 - myStringSize2.width;
        UILabel *lblStockCode = [[UILabel alloc] initWithFrame:CGRectMake(x, 10, myStringSize2.width, myStringSize2.height)];
        [lblStockCode setBackgroundColor:[UIColor clearColor]];
        [lblStockCode setText:myString2];
        [lblStockCode setTextAlignment:NSTextAlignmentCenter];
        [lblStockCode setTextColor:[UIColor blackColor]];
        [lblStockCode setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(8, 45, 60, 30)];
        [lblDate setBackgroundColor:[UIColor clearColor]];
        [lblDate setText:@"Date"];
        [lblDate setTextAlignment:NSTextAlignmentCenter];
        [lblDate setTextColor:[UIColor blackColor]];
        [lblDate setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblQuanty = [[UILabel alloc] initWithFrame:CGRectMake(120, 45, 90, 30)];
        [lblQuanty setBackgroundColor:[UIColor clearColor]];
        [lblQuanty setText:@"Quantity"];
        [lblQuanty setTextAlignment:NSTextAlignmentCenter];
        [lblQuanty setTextColor:[UIColor blackColor]];
        [lblQuanty setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(240,45, 90, 30)];
        [lblPrice setBackgroundColor:[UIColor clearColor]];
        [lblPrice setText:@"Price"];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];
        [lblPrice setTextColor:[UIColor blackColor]];
        [lblPrice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        
        UILabel *lblInvoice = [[UILabel alloc] initWithFrame:CGRectMake(345,45, 200, 30)];
        [lblInvoice setBackgroundColor:[UIColor clearColor]];
        [lblInvoice setText:@"Invoice/CN No"];
        [lblInvoice setTextAlignment:NSTextAlignmentCenter];
        [lblInvoice setTextColor:[UIColor blackColor]];
        [lblInvoice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        [tempView addSubview:lblDebtor];
        [tempView addSubview:lblStockCode];
        [tempView addSubview:lblDate];
        [tempView addSubview:lblQuanty];
        [tempView addSubview:lblPrice];
        [tempView addSubview:lblInvoice];
        
        lblDate = nil;
        lblQuanty = nil;
        lblPrice = nil;
        lblInvoice = nil;
        
        return tempView;
    }
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    switch (tableView.tag)
    {
        case TAG_50:
            if([[AppDelegate getAppDelegateObj] CheckIfDemoApp])
            {
                return [arrHeaderLabels count] - 2;
            }
            else
                return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        case TAG_999:return 1;break;
            
        default:return 0;break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == TAG_999)
    {
        return 80.0;
    }
    else if(tableView.tag == TAG_100)
    {
        return 0.0;
    }
    else
    {
        return 40.0;
    }
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == TAG_50)
    {
       // return 1;
        return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];
    }
    else if (tableView.tag == TAG_100)
    {
        if (isSearch)
        {
            return [arrSearchProducts count];
        }
        else
        {
            return [arrProducts count];
        }
    }
    
    
    //    switch (tableView.tag)
    //    {
    //        case TAG_50: return [[arrHeaderLabels objectAtIndex:section] count];break;
    //
    //        case TAG_100:return [arrProducts count];break;
    //
    //        default:return 0;break;
    //    }
    return nil;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    CustomCellGeneral *cell = nil;
    
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    static NSString *CellIdentifier12 = CELL_IDENTIFIER12;
    //static NSString *CellIdentifier9 = CELL_IDENTIFIER9;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    static NSString *CellIdentifier14 = CELL_IDENTIFIER14;
    
    NSLog(@"%@",[dictHeader valueForKey:@"Data"]);
    
    switch (tableView.tag) {
        case TAG_50:{
            
            NSArray *nib;
            
            if ([indexPath section] == 0) {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                switch ([indexPath row]) {
                    case 0:{
                        
                        if (cell == nil) {
                            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:4];
                            
                        }
                        
                        NSLog(@"---Order_no====%@",[[dictHeader valueForKey:@"Data"] valueForKey:@"Order_no"]);
                        //                        cell.lblValue.text = @"---";//Pradeep
                        cell.lblValue.text = [[dictHeaderDetails valueForKey:@"Data"] valueForKey:@"Order_no"];
                        
                        
                    }
                        break;
                }
            }
            else if ([indexPath section] == 1) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                
                switch ([indexPath row]) {
                    case 0:{
                        
                        //Debtor
                        if (cell == nil) {
                            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:3];
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            cell.txtValue.userInteractionEnabled = FALSE;
                            cell.txtValue.placeholder = @"Please Select";
                            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                            
                        }
                        
                        cell.txtValue.text = nil;
                        if (strDebtor) {
                            cell.txtValue.text = strDebtor;
                            segControl.enabled = YES;
                        }
                        
                    }
                        
                        break;
                }
            }
            else if ([indexPath section] == 2) {
                
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                    
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:4];
                        lblBalance = [[UILabel alloc]initWithFrame:cell.lblValue.frame];
                    }
                    
//                }
                
                
                cell.lblValue.text = @"";
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        cell.lblValue.text = @"";
                        if (strName) {
                            cell.lblValue.text = strName;
                        }
                        
                    }
                        break;
                        
                    case 1:
                    {
                        lblBalance.text = @"";
                        cell.lblValue.text =@"";
                        if (strDeliveryRun) {
                            cell.lblValue.text =strDeliveryRun;
                        }
                        
                    }
                        
                    default: {
                    }
                        break;
                }
                
                if (selectedSectionIndex == 2 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
                
                
            }
            else if ([indexPath section] == 3) {
                
                if ([indexPath row] == 7) {
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                    
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:4];
                        
                    }
                    
                }
                else if([indexPath row] == 8){
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                    //Debtor
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:3];
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.txtValue.userInteractionEnabled = FALSE;
                        cell.txtValue.placeholder = @"Please Select";
                        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                        
                    }
                    
                }
                else{
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                    
                    if (cell == nil)
                    {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:9];
                    }
                    
                    cell.txtGlowingValue.keyboardType = UIKeyboardTypeNamePhonePad;
                    cell.txtGlowingValue.delegate = self;
                    
                }
                
                cell.lblValue.text = nil;
                cell.txtGlowingValue.text = nil;
//                cell.txtValue.text = nil;
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_500;
                        if (strDelName) {
                            cell.txtGlowingValue.text = strDelName;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        
                        cell.txtGlowingValue.tag = TAG_600;
                        if (strDelAddress1) {
                            cell.txtGlowingValue.text = strDelAddress1;
                        }
                        
                        
                    }
                        break;
                    case 2:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_700;
                        if (strDelAddress2) {
                            cell.txtGlowingValue.text = strDelAddress2;
                        }
                        
                    }
                        break;
                    case 3:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_800;
                        if (strDelAddress3) {
                            cell.txtGlowingValue.text = strDelAddress3;
                        }
                        
                        
                    }
                        break;
                    case 4:
                    {
                        
                        
                        cell.txtGlowingValue.tag = TAG_900;
                        if (strDelSubUrb) {
                            cell.txtGlowingValue.text = strDelSubUrb;
                        }
                        
                    }
                        break;
                    case 5:
                    {
                        cell.txtGlowingValue.tag = TAG_1000;
                        if (strDelPostCode) {
                            cell.txtGlowingValue.text = strDelPostCode;
                        }
                        
                    }
                        break;
                    case 6:
                    {
                        cell.txtGlowingValue.tag = TAG_1100;
                        if (strDelCountry) {
                            cell.txtGlowingValue.text = strDelCountry;
                        }
                        
                    }
                        break;
                        
                    case 7:
                    {
                        
                        if (strOrderDate && ![strOrderDate isEqualToString:STANDARD_APP_DATE] && ![strOrderDate isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strOrderDate]];
                        }
                        
                    }
                        break;
                    case 8:
                    {
                        
                        if (strDelDate && ![strDelDate isEqualToString:STANDARD_APP_DATE] && ![strDelDate isEqualToString:STANDARD_SERVER_DATE]) {
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSDate *delDate = [formatter dateFromString:strDelDate];
                            if (delDate == nil) {
                                cell.txtValue.text = strDelDate;
                            }
                            else{
                                cell.txtValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strDelDate];
                            }

                        }
                        
                        
                    }
                        
                        break;
                    case 9:
                    {
                        cell.txtGlowingValue.tag = TAG_1200;

                        if (strDelInst1) {
                          cell.txtGlowingValue.text = strDelInst1;
                        }
                    }
                        
                        break;
                        
                    case 10:
                    {
                        cell.txtGlowingValue.tag = TAG_1300;
//
                        if (strDelInst2) {
                            cell.txtGlowingValue.text = strDelInst2;
                        }
                    }
                        
                        break;
                        
                    case 11:
                    {
                        cell.txtGlowingValue.tag = TAG_1400;

                        if (strCustOrder) {
                            cell.txtGlowingValue.text = strCustOrder;
                        }
                    }
                        
                        break;
                        
                        
                    default:{
                        
                    }
                        break;
                }
            }
            //Cash pick up
            else if ([indexPath section] == 4) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier12];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:11];
                    
                }
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        if([isCashPickUpOn isEqualToString:@"Y"]){
                            [cell.swtch setOn:YES];
                        }
                        else{
                            [cell.swtch setOn:NO];
                        }
                        
                        cell.swtch.tag = TAG_200;
                        
                        // Switch is not editable
//                        [cell.swtch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventValueChanged];
                        
                    }
                        break;
                    default:{
                    }
                        break;
                }
            }
            //Stock pick up
            else if ([indexPath section] == 5) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier12];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:11];
                }
                
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        
                        if([isStockPick isEqualToString:@"N"]){
                            [cell.swtch setOn:NO];
                        }
                        else{
                            [cell.swtch setOn:YES];
                        }
                        
                        cell.swtch.tag = TAG_300;
//                        [cell.swtch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventValueChanged];
                        
                    }
                        break;
                    default:{
                        
                    }
                        
                        break;
                }
            }
            //Comments
            else if ([indexPath section] == 6) {
                
                // NSLog(@"In 6");
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblValue.text = nil;
                switch ([indexPath row]) {
                        
                    case 0:
                    {
                        if (strCommenttxt) {
                            cell.lblValue.text = strCommenttxt;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        if (strCommentDateCreated && ![strCommentDateCreated isEqualToString:STANDARD_APP_DATE]&& ![strCommentDateCreated isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentDateCreated];
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        
                        if (strCommentFollowDate && ![strCommentFollowDate isEqualToString:STANDARD_APP_DATE]&& ![strCommentFollowDate isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentFollowDate];
                        }
                    }
                        break;
                        
                        
                    default: {
                        
                    }
                        break;
                }
                
                if (selectedSectionIndex == 6 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
            }
            
            cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            NSLog(@"Plist  %@",[[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"]);
            NSLog(@" Cell %@",cell.lblTitle.text);
            
            return cell;
        }
            break;
            
        case TAG_100:{
            
            NSLog(@"%@",arrProducts);
            NSString *str = [NSString stringWithFormat:@""];
            NSString *str1 = [NSString stringWithFormat:@""];
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:@"cellIdentifier15"];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:14];
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                
            }
            
            if(isSearch)
            {
                
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                
                if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"Item"] || [[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"ITEM"] || [[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"]) {
                    
                    NSString *str2;
                    if ([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"ITEM"]) {
                      str2 = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ITEM"];
                    }
                    else{
                        str2 = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                    }
                    
                    if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound) {
                       
                        if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"DESCRIPTION"]) {
                            cell.lblTitle.text  = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"DESCRIPTION"]]   ;
                        }
                        else{
                            cell.lblTitle.text  = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]]   ;
                        }
                        
                        
                        if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        }
                        else{
                            cell.lblValue.text = @"";
                        }
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                    else
                    {
                        //Freight
                        if ([str2 isEqualToString:str1]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                            
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            
                        }
                        
                        //Comment
                        if ([str2 isEqualToString:str]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = @"";
                            
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        }
                        
                    }
                }
                else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"]){
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"] floatValue]];

                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }
            else //For Product Array
            {
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"] floatValue]];
                }
                else{
                    cell.lblValue.text = @"";
                }
                
                if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"DESCRIPTION"]) {
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"DESCRIPTION"]];
                }
                else if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]){
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    if ([[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"] isEqualToString:@"Freight"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                }
               
                /*
                 cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                 if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"FromSpecials"])
                 {
                 
                 cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];
                 //   [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
                 }
                 
                 if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                 
                 NSString *str2 = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                 if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound) {
                 
                 cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                 if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                 cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                 }
                 else{
                 cell.lblValue.text = @"";
                 }
                 
                 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                 }
                 else
                 {
                 //Freight
                 if ([str2 isEqualToString:str1]) {
                 cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                 cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                 
                 cell.accessoryType = UITableViewCellAccessoryNone;
                 
                 }
                 
                 //Comment
                 if ([str2 isEqualToString:str]) {
                 cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"DESCRIPTION"]];
                 cell.lblValue.text = @"";
                 
                 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                 }
                 
                 
                 }
                 }
                 else{
                 cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                 
                 if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"]) {
                 cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"EXTENSION"] floatValue]];
                 }
                 else{
                 cell.lblValue.text = @"";
                 }
                 
                 cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                 }
                 
                 */
            }
            
            return cell;
            
        }break;
            
        case TAG_999:
        {
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier14];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:13];
            }
            
            int row = indexPath.row;
            cell.lblProdDate.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"DateRaised"];
            cell.lblProdQuantity.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Quantity"];
            cell.lblProdPrice.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Price"];
            cell.lblProdInv.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"InvNo"];
            
            return cell;
        }break;
        default:return nil;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case TAG_50:{
            if ([indexPath section] == 1) {
                
                switch ([indexPath row]) {
                        //Debtor
                    case 0:
                    {
                        QuoteHistoryCustomerListViewController *dataViewController = [[QuoteHistoryCustomerListViewController alloc] initWithNibName:@"QuoteHistoryCustomerListViewController" bundle:[NSBundle mainBundle]];
                        dataViewController.isFromLoadProfile = NO;
                       // dataViewController.isUserCallSchedule = NO;
                        
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
             if ([indexPath section] == 2  && [indexPath row] == 1) {
                
                switch ([indexPath row]) {
                        
                        //Delivery Run
                    case 1:
                    {
                        CustomCellGeneral *cell = (CustomCellGeneral *)[_tblHeader cellForRowAtIndexPath:indexPath];
                        
                        DeliveryRunViewController *dataViewController = [[DeliveryRunViewController alloc] initWithNibName:@"DeliveryRunViewController" bundle:[NSBundle mainBundle]];
                        
                        dataViewController.contentSizeForViewInPopover =
                        CGSizeMake(300, 400);
                        
                        //create a popover controller
                        self.popoverController = [[UIPopoverController alloc]
                                                  initWithContentViewController:dataViewController];
                        
                        CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                        
                        dataViewController.delegate = self;
                        [self.popoverController presentPopoverFromRect:rect
                                                                inView:cell
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
                    }
                        break;

                    default:{
                        //The user is selecting the cell which is currently expanded
                        //we want to minimize it back
                        if(selectedIndex == indexPath.row)
                        {
                            selectedIndex = -1;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            
                            return;
                        }
                        
                        //First we check if a cell is already expanded.
                        //If it is we want to minimize make sure it is reloaded to minimize it back
                        if(selectedIndex >= 0)
                        {
                            NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                            selectedIndex = indexPath.row;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                        }
                        
                        //Finally set the selected index to the new selection and reload it to expand
                        selectedIndex = indexPath.row;
                        selectedSectionIndex = indexPath.section;
                        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    }
                        break;
                }
                
            }
            
            if ([indexPath section] == 3 && [indexPath row] == 8) {
                CustomCellGeneral *cell = (CustomCellGeneral *)[_tblHeader cellForRowAtIndexPath:indexPath];
                
                DatePopOverController *dataViewController = [[DatePopOverController alloc] initWithNibName:@"DatePopOverController" bundle:[NSBundle mainBundle]];
                
                dataViewController.contentSizeForViewInPopover =
                CGSizeMake(300, 300);
                dataViewController.isStockPickup = NO;
               // dataViewController.isCallDate = NO;
                
                //create a popover controller
                self.popoverController = [[UIPopoverController alloc]
                                          initWithContentViewController:dataViewController];
                
                CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                
                dataViewController.delegate = self;
                [self.popoverController presentPopoverFromRect:rect
                                                        inView:cell
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
            }
            
            
            
            if ([indexPath section] == 6 && [indexPath row] == 0) {
                
//                if (detailViewController1) {
//                    [self dismissPopupViewControllerWithanimationType:nil];
//                    detailViewController1= nil;
//                }
//                
//                detailViewController1 = [[MJDetailViewController alloc] initWithNibName:@"MJDetailViewController" bundle:nil];
//                
//                detailViewController1.strMessage = strCommenttxt;
//                [self presentPopupViewController:detailViewController1 animationType:3];
//                
//                return;
                
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    
                    return;
                }
                
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            
            
        }
            break;
            
        case TAG_100:{
            
            if(isSearch)
            {
                NSDictionary *dictProductDetails = [arrSearchProducts objectAtIndex:[indexPath row]];
                
                //Comments
                if ([[dictProductDetails objectForKey:@"Item"]isEqualToString:@"/C"] || [[dictProductDetails objectForKey:@"ITEM"] isEqualToString:@"/C"]){
                    QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                    
                    if ([dictProductDetails objectForKey:@"DESCRIPTION"]) {
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"DESCRIPTION"]];
                    }
                    else{
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Description"]];
                        
                    }
                    dataViewController.delegate = self;
                    dataViewController.productIndex = [indexPath row];
                    dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    
                }
                else if ([[dictProductDetails objectForKey:@"Item"]isEqualToString:@"/F"] || [[dictProductDetails objectForKey:@"ITEM"] isEqualToString:@"/F"]){
                }
                else
                {
                    
                        OfflineProductDetailVC *dataViewController = [[OfflineProductDetailVC alloc] initWithNibName:@"OfflineProductDetailVC" bundle:[NSBundle mainBundle]];
                        
                        dataViewController.strDebtor = strDebtor;
                        dataViewController.dictProductDetails = [arrSearchProducts objectAtIndex:[indexPath row]];
                        dataViewController.strWarehouse = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Warehouse"];
                        
                        
                        if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]) {
                            dataViewController.stock_code = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                        }
                        else if([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ITEM"]){
                            dataViewController.stock_code = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ITEM"];
                        }
                        else{
                            dataViewController.stock_code = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"];
                        }
                        
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                }
            }
            else{
                NSDictionary *dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                
                //Comments
                if ([[dictProductDetails objectForKey:@"Item"]isEqualToString:@"/C"] || [[dictProductDetails objectForKey:@"ITEM"] isEqualToString:@"/C"]){
                    QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                    
                    if ([dictProductDetails objectForKey:@"DESCRIPTION"]) {
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"DESCRIPTION"]];
                    }
                    else{
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Description"]];
                        
                    }
                    dataViewController.delegate = self;
                    dataViewController.productIndex = [indexPath row];
                    dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    
                }
                else if ([[dictProductDetails objectForKey:@"Item"]isEqualToString:@"/F"] || [[dictProductDetails objectForKey:@"ITEM"] isEqualToString:@"/F"]){
                }
                else
                {
                    OfflineProductDetailVC *dataViewController = [[OfflineProductDetailVC alloc] initWithNibName:@"OfflineProductDetailVC" bundle:[NSBundle mainBundle]];
                        
                        dataViewController.strDebtor = strDebtor;
                        dataViewController.dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                        dataViewController.strWarehouse = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Warehouse"];
                        
                        if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ITEM"]) {
                            dataViewController.stock_code = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ITEM"];
                        }
                        else if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]){
                            dataViewController.stock_code = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                        }
                        else{
                            dataViewController.stock_code = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Stock_code"];
                        }
                        
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                }

            }
            
        }
            break;
    }
    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    switch (tableView.tag) {
            
        case TAG_50:{
            return NO;
        }break;
            
        case TAG_100:{
            //New product,Freight,Comment //19thsept
//            if ([[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"] isEqualToString:@"1"]||[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"] isEqualToString:@"2"]) {
//                return YES;
//            }
//            else{
//                return NO;
//            }
          return YES;
        }break;
            
        default:return NO;break;
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
            
        case TAG_100:{
            if (editingStyle == UITableViewCellEditingStyleDelete)
            {
                NSDictionary *dict = [arrProducts objectAtIndex:[indexPath row]];
                
                [arrProducts removeObjectAtIndex:[indexPath row]];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                [tableView endUpdates];
                
                if ([dict objectForKey:@"ExtnPrice"]) {
                    NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
                    totalCost -= [strExtnPrice floatValue];
                }
                else if([dict objectForKey:@"COST"])
                {
                    [self deleteProductFromOrder:[dict objectForKey:@"ORDER_NO"] stockCode:[dict objectForKey:@"ITEM"]];
                    NSString *strCostPrice = [dict objectForKey:@"COST"];
                    totalCost -= [strCostPrice floatValue];
                }
               
                
                lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
                [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:lblTotalCost];

                [_tblDetails reloadData];
                
                [self repositionLoadMoreFooterView];
                
                if ([arrProducts count] > cntProfileOrders) {
                    //Show delete button
                    btnDelete.hidden = NO;
                }
                else{
                    btnDelete.hidden = YES;
                    btnAddMenu.enabled = TRUE;
                    [tableView setEditing:NO animated:YES];
                }
                
                if ([arrProducts count] == 0) {
                    [vwContentSuperview removeAllSubviews];
                    [vwContentSuperview addSubview:vwNoProducts];
                    [self showOrEnableButtons];
                }
            }
            
        }break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:
        {
            if (section == 1) {
                return nil;
            }
            if (section == 4) {
                return @"Comments";
            }
        }break;
    }
    // Return the displayed title for the specified section.
    return nil;
}


#pragma mark - Database call
-(void)callGetNewOrderNumberFromDB:(NSString *)ordernum{
    
    strWebserviceType = @"DB_GET_ORDER_NUMBER";
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            FMResultSet *rs = [db executeQuery:@"SELECT last_order_num FROM (SELECT ORDER_NO as last_order_num FROM soheader UNION SELECT ORDER_NO as last_order_num FROM soheader_offline)ORDER BY last_order_num DESC LIMIT 1"];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:[rs resultDictionary]];
                NSLog(@"temp %@",temp);
                [_tblHeader reloadData];
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}

//Load product codes from
-(BOOL)callGetProductFromCustProfileDB:(FMDatabase *)db ProductCode:(NSString *)productCode{
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT STOCK_CODE FROM smcuspro WHERE CUSTOMER_CODE = ? AND STOCK_CODE = ?",strDebtor,productCode];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            [rs1 close];
            return TRUE;
        }
        
        return FALSE;
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        return FALSE;
    }
    
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db withStockCode:(NSString *)strStock_code{
    
    strWebserviceType = @"DB_GET_PRODUCT_IN_DETAILS";
    ///[arrProducts removeAllObjects];
    
    @try {
        
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE trim(CODE) = ?",[strDebtor trimSpaces]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            self.strPriceCode = [dictData objectForKey:@"PRICE_CODE"];
        }
        
        [rs1 close];
        
        
        //Change by subhu
        strDebtor = [strDebtor trimSpaces];
        
        FMResultSet *rs2 = [db executeQuery:@"SELECT AVAILABLE,STOCK_CODE, PREF_SUPPLIER, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, max(AVERAGE_COST) AS AVERAGE_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT a.AVAILABLE as AVAILABLE, a.PREF_SUPPLIER AS PREF_SUPPLIER,a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.AVERAGE_COST AS AVERAGE_COST,a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1, a.PROD_GROUP as PROD_GROUP,                                                                                                                                                                                                                                                                                               (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ? AND a.`CODE` = ? AND ((a.`STATUS` = 'AC' OR a.`STATUS` = 'NO')AND a.`STATUS` = 'OB')) temp GROUP BY STOCK_CODE ORDER BY PROD_GROUP",strStock_code,strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces]];
        if (!rs2)
        {
            [rs2 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        isRecordsExist = FALSE;
        while ([rs2 next]) {
            
            isRecordsExist = TRUE;
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs2 resultDictionary];
            [dict setValue:[dictData objectForKey:@"AVAILABLE"] forKey:@"Available"];
            [dict setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
            [dict setValue:[dictData objectForKey:@"COST_DISS"] forKey:@"Dissection_Cos"];
            [dict setValue:[dictData objectForKey:@"SALES_DISS"] forKey:@"Dissection"];
            [dict setValue:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"LastSold"];
            [dict setValue:[dictData objectForKey:@"STOCK_CODE"] forKey:@"StockCode"];
            //            PREF_SUPPLIER
            [dict setValue:[dictData objectForKey:@"PREF_SUPPLIER"] forKey:@"PREF_SUPPLIER"];
            [dict setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
            [dict setValue:[dictData objectForKey:@"PRICE2"] forKey:@"Price2"];
            [dict setValue:[dictData objectForKey:@"PRICE3"] forKey:@"Price3"];
            [dict setValue:[dictData objectForKey:@"PRICE4"] forKey:@"Price4"];
            [dict setValue:[dictData objectForKey:@"PRICE5"] forKey:@"Price5"];
            [dict setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
            [dict setValue:[dictData objectForKey:@"q0"] forKey:@"q0"];
            [dict setValue:[dictData objectForKey:@"q1"] forKey:@"q1"];
            [dict setValue:[dictData objectForKey:@"q2"]forKey:@"q2"];
            [dict setValue:[dictData objectForKey:@"q3"] forKey:@"q3"];
            [dict setValue:[dictData objectForKey:@"q4"] forKey:@"q4"];
            [dict setValue:[dictData objectForKey:@"STANDARD_COST"] forKey:@"Cost"];
            [dict setValue:[dictData objectForKey:@"AVERAGE_COST"] forKey:@"AverageCost"];
            [dict setValue:[dictData objectForKey:@"TAX_CODE1"] forKey:@"TAX_CODE1"];
            [dict setValue:@"0" forKey:@"isNewProduct"];
            
            [arrProducts addObject:dict];
            
        }
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"ITEM"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:[arrProducts count]];
                    *stop = YES;
                }
            }];
        }
        
        self.cntProfileOrders = [arrProducts count];
        
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
    // NSLog(@"arrProducts for edit action==%@",arrProducts);
}


-(void)getDataForProductDetail:(NSString *)stock_code{
    
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        [self callViewQuoteDetailsDetailsFromDB:db withStockCode:stock_code];
    }];
}

-(void)callInsertProfileOrderToDB{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            BOOL y2;
            
            self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
            
            if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                
                self.strUploadDate = self.strDateRaised;

                self.strOrderNum = strOrderNumSel;
                
                
                if([dictHeaderDetails objectForKey:@"StockPickUp"])
                {
                    self.strStockPickup = [dictHeaderDetails objectForKey:@"StockPickUp"];
                }
                
                
                y2 = [db executeUpdate:@"INSERT OR REPLACE INTO `soheader_offline` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS, PERIOD_RAISED,YEAR_RAISED,ORDER_VALUE,DETAIL_LINES, isUploadedToServer,isProfileOrder,isEdited,TOTL_CARTON_QTY) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?, ?,?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strSalesman,strSalesman,strStockPickup,strPeriodRaised,strPeriodRaised,strTotalCost,strTotalLines,[NSNumber numberWithInt:0],[NSNumber numberWithInt:1],[NSNumber numberWithInt:0],self.strTotalCartonQty];
                
                
            }
            else{
                //Insert
                y2 = [db executeUpdate:@"INSERT OR REPLACE INTO `soheader` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS, PERIOD_RAISED,YEAR_RAISED,EXCHANGE_RATE,TOTL_CARTON_QTY,ACTIVE,EDIT_STATUS,ORDER_VALUE,DETAIL_LINES, `created_date`, `modified_date`,TOTL_CARTON_QTY) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strUploadDate,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strOperator,strOperator,strStockPickup,strPeriodRaised,strYearRaised,strExchangeRate,strTotalCartonQty,strActive,strEditStatus,strTotalCost,strTotalLines,strUploadDate,strUploadDate,self.strTotalCartonQty];
            }
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            NSArray *components = [strUploadDate componentsSeparatedByString:@" "];
            NSString *strCreatedTime ;
            if(components.count > 1)
            {
                strCreatedTime = (NSString *)[components objectAtIndex:1];
            }
            else
            {
                components = [strUploadDate componentsSeparatedByString:@"T"];
                if(components.count > 1)
                {
                    strCreatedTime = (NSString *)[components objectAtIndex:1];
                }
            }
            NSString *strCreatedDate = (NSString *)[components objectAtIndex:0];
            
            float totalWeight = 0;
            float totalVolume = 0 ;
            float totalTax = 0;
            int totalQuantity = 0;
            
            int detail_lines = 0;
           // int line_num = 0;
            
            
            
            for (NSMutableDictionary *dictProductDetails in arrProducts) {
                
                NSString *strStockCode1 = nil;
                NSString *strPrice = @"0";
                NSString *strDissection = @"0";
                NSString *strDissection_Cos = @"0";
                NSString *strWeight = @"0";
                NSString *strVolume = @"0";
                NSString *strCost = @"0";
                NSString *strExtension = @"0";
                NSString *strGross = @"0";
                NSString *strTax = @"0";
                NSString *strQuantityOrdered = @"0";
                
                NSString *strAltQuantity = @"0";
                NSString *strCUST_PRICE = @"0";
                NSString *strCUST_ORD_QTY  = @"0";
                NSString *strCUST_SHIP_QTY = @"0";
                NSString *strCUST_VOLUME = @"0";
                NSString *strPRICING_UNIT = @"0";
                NSString *strCharge_Type = @"";
                NSString *strRelease_New = @"Y";
                //Comments
                if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/C"])
                {
                    detail_lines++;
                    line_num++;
                    
                    strStockCode1 = @"/C";
                    
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                    self.strRefreshPrice = @"";
                    
                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ||  [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        
                    {
                        //Insert
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y11 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y11)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        
                        
                    }
                    else{
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                    }
                    
                    
                }
                //Freight
                else if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"])
                {
                    detail_lines++;
                    line_num++;
                    
                    strStockCode1 = @"/F";
                    
                    strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strExtension = [NSString stringWithFormat:@"%f",freightValue];
                    NSString* strfreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
                    totalTax += [strfreightTax floatValue];
                    
                    
                    //                    strPrice = [dictProductDetails objectForKey:@"Price"];
                    //                    strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                    //                    strTax = [dictProductDetails objectForKey:@"Tax"];
                    //                    totalTax += [strTax floatValue];
                    
                    strAltQuantity = @"0.00";
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    
                    strCUST_PRICE = strPrice;
                    strCUST_ORD_QTY  = @"1";
                    strCUST_SHIP_QTY = @"1";
                    strCUST_VOLUME = @"1";
                    strPRICING_UNIT = @"1";
                    strCharge_Type = @"N";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/F";
                    strDissection_Cos = @"";
                    self.strRefreshPrice = @"";
                    
                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        
                    {
                        //Insert
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?, ?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        
                        
                    }
                    else{
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                    }
                    
                }
                //Product
                else{
                    
                    if ([dictProductDetails objectForKey:@"Item"]) {
                        strStockCode1 = [dictProductDetails objectForKey:@"Item"];
                    }
                    else if ([dictProductDetails objectForKey:@"StockCode"]){
                        strStockCode1 = [dictProductDetails objectForKey:@"StockCode"];
                    }
                    
                    if ([dictProductDetails objectForKey:@"Price"]) {
                        strPrice = [dictProductDetails objectForKey:@"Price"];
                    }
                    
                    if ([dictProductDetails objectForKey:@"ExtnPrice"])
                    {
                        detail_lines++;
                        line_num++;
                        
                        strDissection = [dictProductDetails objectForKey:@"Dissection"];
                        strDissection_Cos = [dictProductDetails objectForKey:@"Dissection_Cos"];
                        strWeight = [dictProductDetails objectForKey:@"Weight"];
                        strVolume = [dictProductDetails objectForKey:@"Volume"];
                        strCost = [dictProductDetails objectForKey:@"AverageCost"];
                        strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                        
                        strQuantityOrdered = [dictProductDetails objectForKey:@"QuantityOrdered"];
                        totalQuantity += [strQuantityOrdered intValue];
                        
                        strWeight = [dictProductDetails objectForKey:@"Weight"];
                        
                        strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                        
                        totalWeight += [strWeight floatValue];
                        
                        strVolume = [dictProductDetails objectForKey:@"Volume"];
                        
                        strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                        
                        totalVolume += [strVolume floatValue];
                        
                        strGross = [dictProductDetails objectForKey:@"Gross"];
                        strTax = [dictProductDetails objectForKey:@"Tax"];
                        
                        totalTax += [strTax floatValue];
                        
                        self.strDecimalPlaces = @"2";
                        
                        strCUST_PRICE = strPrice;
                        strCUST_ORD_QTY  = strQuantityOrdered;
                        strCUST_SHIP_QTY = strQuantityOrdered;
                        strCUST_VOLUME = @"1";
                        strPRICING_UNIT = @"1";
                        strCharge_Type = @"";
                        strRelease_New = @"Y";
                        self.strRefreshPrice = @"N";
                        self.strConvFactor = @"1.0000";
                        
                        /*
                         if ([strQuantityOrdered intValue] > 0) {
                         strCustVolume = @"1";
                         strPricingUnit = @"1";
                         }
                         else{
                         strCustVolume = @"0";
                         strPricingUnit = @"0";
                         }
                         */
                        
                        //                        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        {
                            //Insert
                            NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                            BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                            
                            if (!y1)
                            {
                                isSomethingWrongHappened = TRUE;
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                break;
                            }
                            
                            
                        }
                        else{
                            NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                            
                            BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strChargetype,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                            
                            if (!y1)
                            {
                                isSomethingWrongHappened = TRUE;
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            
                        }
                    }
                    else if ([dictProductDetails objectForKey:@"isNewProduct"]) {
                        /*
                         strProdClass = [dict objectForKey:@"ProdClass"];
                         strProdGroup = [dict objectForKey:@"ProdGroup"];
                         strSubstitute = @"Y";
                         strBrand = @"..";
                         strUserSortSeq = @"..";
                         
                         y1 = [db executeUpdate:@"INSERT INTO `smcuspro_offline` (CUSTOMER_CODE, STOCK_CODE, DATE_LAST_PURCH, QTY_LAST_SOLD, VALUE_LAST_SALE, SALES_00, PRODUCT_CLASS, SUBSTITUTE_YN, DESCRIPTION, PROD_GROUP, BRAND, EDIT_STATUS, USER_SORT_SEQ,created_date,modified_date) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?)",strDebtor,strStockCode,createdDate, strQuantityOrdered, strPrice,strPrice,strProdClass,strSubstitute,strDescription,strProdGroup,strBrand,strEditStatus,strUserSortSeq,createdDate,createdDate];
                         
                         
                         
                         if (!y1)
                         {
                         isSomethingWrongHappened = TRUE;
                         
                         NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                         
                         @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                         
                         break;
                         }
                         
                         
                         */
                        
                    }
                    
                }
                
            }
            
            self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
            self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
            self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
            self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
            
            
            BOOL x;
            //            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || ! [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                
            {
                x = [db executeUpdate:@"UPDATE `soheader` SET TOTL_CARTON_QTY = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ? WHERE ORDER_NO = ?",strTotalCartonQty,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strOrderNum];
                self.strSuccessMessage = [NSString stringWithFormat:@"Order Updated Successfully!"];
                
            }
            else{
                
                x = [db executeUpdate:@"UPDATE `soheader_offline` SET TOTL_CARTON_QTY = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ? WHERE ORDER_NO = ?",strTotalCartonQty,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strOrderNum];
                
                self.strSuccessMessage = [NSString stringWithFormat:@"Order Updated Successfully!"];
            }
            
            if (!x)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            *rollback = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                //Prepare for new Order
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                alert.tag = TAG_50;
                [alert show];
                
                [arrProducts removeAllObjects];
                
//                FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//                [db open];
//                [self callViewQuoteDetailsDetailsFromDB:db];
//                [db close];
//                [_tblDetails reloadData];
//                [_tblHeader reloadData];
            });
            
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                NSLog(@"%@",strErrorMessage);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            });
            
            
        }
        
    }];
}


#pragma mark - Alert Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == TAG_50) {
        [self prepareForNewOrder];
    }
    
    if (alertView.tag == TAG_1300) {
        [segControl setSelectedIndex:0];
    }
    NSLog(@"%d",alertView.tag);
    //Error in connection alert
    if (alertView.tag == TAG_200) {
        if (buttonIndex == 1) {
            [self callInsertProfileOrderToDB];
        }
    }
    
    if (alertView.tag == TAG_400) {
        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
        {
            [self callInsertProfileOrderToDB];
            
        }
        else{
            [self callWSSaveProfileOrder];
        }
        
    }
    
    if(alertView.tag == 911)
    {
        if (buttonIndex == 1)
        {
            alertFreightValue = [[[alertView textFieldAtIndex:0] text] intValue];
            
            if(alertFreightValue > 0)
            {
                [self calcFreight];
            }
        }
    }
}


#pragma mark - Delegates
- (void)dismissStorePickUp:(NSString *)strData{
    
    if (![strData isEqualToString:@"|||||||||||||||||||||||||||||"]) {
        isStorePickUpOn = @"Y";
        [dictHeaderDetails setValue:strData forKey:@"StockPickUp"];
    }
    else{
        isStorePickUpOn = @"N";
        [_tblHeader reloadData];
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController dismissViewControllerAnimated:YES completion:Nil];
    
}

- (void)actiondismissPopOverForCashPickUp:(NSString*)strPickUpAmt IsChequeAvaliable:(BOOL)isChequeAvaliable{
    
    self.isCashPickUpOn = @"Y";
    
    self.strCashPickUp = strPickUpAmt;
    
    if (isChequeAvaliable){
        self.strChequeAvailable = @"Y";
    }
    else{
        self.strChequeAvailable = @"N";
    }
    
    [popoverController dismissPopoverAnimated:YES];
    
}

- (void)setDeliveryRun:(NSString *)strDelRun{
    isDeliveryRunSlected = YES;
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    self.strDeliveryRun = strDelRun;
    [_tblHeader reloadData];
}

- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation stock_code:(NSString *)stock_code
{
    isSearch = NO;
//    _txtSearchProfileOrderEntryProducts.text = @"";
    
    for (int i=0; i<[arrProducts count]; i++)
    {
        NSString *strCode;
        
        if ([[arrProducts objectAtIndex:i] objectForKey:@"ITEM"]) {
            strCode = [[arrProducts objectAtIndex:i] objectForKey:@"ITEM"];

        }
        else if([[arrProducts objectAtIndex:i] objectForKey:@"StockCode"]){
            
            strCode = [[arrProducts objectAtIndex:i] objectForKey:@"StockCode"];
            [[arrProducts objectAtIndex:i] setValue:strCode forKey:@"ITEM"];

            NSString *descStr =[[arrProducts objectAtIndex:i] objectForKey:@"Description"];
            [[arrProducts objectAtIndex:i] setValue:descStr forKey:@"DESCRIPTION"];
        }
        
        if([stock_code isEqualToString:strCode])
        {
            NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
            NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
            
            NSMutableDictionary *dictTotalPrice = [arrProducts objectAtIndex:i];
            [dictTotalPrice setValue:strTotalPrice forKey:@"ExtnPrice"];
//            [dictTotalPrice setValue:[dictCalculation objectForKey:@"ExtnPrice"] forKey:@"ExtnPrice"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"EXTENSION"] forKey:@"EXTENSION"];

            [dictTotalPrice setValue:strQuantityOrdered forKey:@"QUANTITY"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"GROSS"] forKey:@"Gross"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"TAX"] forKey:@"Tax"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"CUST_PRICE"] forKey:@"Price"];
            
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"PriceChnaged"] forKey:@"PriceChnaged"];
            
            [dictTotalPrice setValue:@"1" forKey:@"isEdit"];

            [_tblDetails reloadData];
            totalCost = 0;
            
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([obj objectForKey:@"ExtnPrice"] || [obj objectForKey:@"EXTENSION"]) {
                    NSString *strExtnPrice;
                    if ([obj objectForKey:@"ExtnPrice"]) {
                        strExtnPrice = [obj objectForKey:@"ExtnPrice"];
                        totalCost += [strExtnPrice floatValue];
                    }
                    else{
                        strExtnPrice = [obj objectForKey:@"EXTENSION"];
                        totalCost += [strExtnPrice floatValue];
                    }
                }
             
                
            }];
            
            lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
        }
    }
}

- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict{
    //[segControl setSelectedIndex:1];
    
    __block BOOL isCommentExist = NO;
    __block int prodIndex = 0;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"ITEM"] isEqualToString:@"/C"] || [[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
            prodIndex = idx;
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    if (!isCommentExist) {
        
        //Show delete button
        btnDelete.hidden = NO;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        //--Code changed to always add the comment at top
        [arrProducts addObject:dict];
        //        [arrProducts insertObject:dict atIndex:0];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [_tblDetails reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
        [_tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        
    }
    else{
        [arrProducts replaceObjectAtIndex:prodIndex withObject:dict];
        
        if (isCommentAddedOnLastLine) {
            [[arrProducts objectAtIndex:prodIndex] setObject:@"1" forKey:@"isEdit"];

            id object = [arrProducts objectAtIndex:prodIndex];
            [arrProducts removeObjectAtIndex:prodIndex];
            [arrProducts insertObject:object atIndex:[arrProducts count]];
        }
        [_tblDetails reloadData];
    }
}

- (void)showFreightView{
    //[segControl setSelectedIndex:1];
    __block BOOL isFreightExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/F"] || [[obj objectForKey:@"ITEM"] isEqualToString:@"/F"]) {
            isFreightExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isFreightExist)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter the freight value." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = 911;
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.placeholder = @"Enter the freight value";
        alertTextField.keyboardType = UIKeyboardTypeNumberPad;
        [alert show];
        //[self calcFreight];
    }
}

- (void)showCommentView{
    __block BOOL isCommentExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"] || [[obj objectForKey:@"ITEM"] isEqualToString:@"/C"]) {
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
        dataViewController.commentStr = @"";
        dataViewController.commntTAg = 0;
        dataViewController.delegate = self;
        dataViewController.productIndex = -1;
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
}

-(void)setCommentPosition:(BOOL)status
{
    self.isCommentAddedOnLastLine = status;
}



- (void)setDeliveryDate:(NSString *)strDeliveryDate{
    self.strDelDate = strDeliveryDate;
    strCopyDelDate = [self.strDelDate copy];
    [_tblHeader reloadData];
}

-(void)dismissPopover{
    [popoverController dismissPopoverAnimated:YES];
}

- (void) setupLoadMoreFooterView
{
    /*
     CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
     
     _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
     _loadMoreFooterView.backgroundColor = [UIColor clearColor];
     
     [_tblDetails addSubview:_loadMoreFooterView];
     */
}
- (void) repositionLoadMoreFooterView
{
    /*
     _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
     [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
     */
}


#pragma  mark - Searchbar Delegates


- (IBAction)filterSearchResult:(id)sender {
    NSLog(@"Search by : ");
    UISegmentedControl *segment=(UISegmentedControl*)sender;
    [segment setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];

    switch (segment.selectedSegmentIndex) {
        case 0:
            NSLog(@"Description");
            break;
        case 1:
            NSLog(@"Code");
            break;
        default:
            break;
    }
    
}



-(void)searchResultByDescription:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"Description  CONTAINS[cd] %@  OR DESCRIPTION  CONTAINS[cd] %@",stringText, stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [_tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void)searchResultByCode:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"StockCode  CONTAINS[cd] %@  OR ITEM  CONTAINS[cd] %@ ", stringText,stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [_tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length <= 0)
    {
        [arrSearchProducts removeAllObjects];
        isSearch = NO;
        [_tblDetails reloadData];
    }
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchTxt = [NSString stringWithFormat:@"%%%@%%", searchBar.text];
    
    isSearch = YES;
    [arrSearchProducts removeAllObjects];
    
    if (_SegmentSearch.selectedSegmentIndex == 0) {
        [self searchResultByDescription:searchTxt];
}
    else  if (_SegmentSearch.selectedSegmentIndex == 1){
        [self searchResultByCode:searchTxt];

    }
    
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delgates

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textfield
{
    CGPoint pointInTable = [textfield.superview convertPoint:textfield.frame.origin toView:self.tblHeader];
    CGPoint contentOffset = self.tblHeader.contentOffset;
    
    contentOffset.y = (pointInTable.y - textfield.inputAccessoryView.frame.size.height);
    
    //NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.tblHeader setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == TAG_500)
    {
        int tempCount = [textField.text length];
        self.strDelName = textField.text;
        if(tempCount > 34)
        {
            return NO;
        }
    }
    else if (textField.tag == TAG_600)
    {
        int tempCount = [textField.text length];
        self.strDelAddress1 = textField.text;
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_700)
    {
        int tempCount = [textField.text length];
        self.strDelAddress2 = textField.text;
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_800)
    {
        int tempCount = [textField.text length];
         self.strDelAddress3 = textField.text;
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_900)
    {
        int tempCount = [textField.text length];
         self.strDelSubUrb = textField.text;
        if(tempCount > 39)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1000)
    {
        int tempCount = [textField.text length];
         self.strDelPostCode = textField.text;
        if(tempCount > 9)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1100)
    {
        int tempCount = [textField.text length];
        self.strDelCountry = textField.text;

        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1200)
    {
        
        int tempCount = [textField.text length];
        strDelInst1 = textField.text;
        if(tempCount > 24)
        {
            return NO;
        }
    }
    else if (textField.tag == TAG_1300)
    {
       
        int tempCount = [textField.text length];
         strDelInst2 = textField.text;
        if(tempCount > 24)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1400)
    {
        int tempCount = [textField.text length];
         self.strCustOrder = textField.text;
        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    switch (textField.tag) {
        case TAG_500:
        {
            self.strDelName = textField.text;
        }
            break;
        case TAG_600:
        {
            self.strDelAddress1 = textField.text;
            
        }
            break;
            
        case TAG_700:
        {
            self.strDelAddress2 = textField.text;
        }
            break;
            
        case TAG_800:
        {
            self.strDelAddress3 = textField.text;
        }
            break;
            
        case TAG_900:
        {
            self.strDelSubUrb = textField.text;
        }
            break;
            
        case TAG_1000:
        {
            self.strDelPostCode = textField.text;
        }
            break;
            
        case TAG_1100:
        {
            self.strDelCountry = textField.text;
        }
            break;
            
        case TAG_1200:
        {
            self.strDelInst1 = textField.text;
        }
            break;
            
        case TAG_1300:
        {
            self.strDelInst2 = textField.text;
        }
            break;
            
        case TAG_1400:
        {
            self.strCustOrder = textField.text;
        }
            break;
            
            
        default:
            break;
    }

    [textField resignFirstResponder];
    
    if([textField.superview.superview isKindOfClass:[CustomCellGeneral class]])
    {
        CustomCellGeneral *cell = (CustomCellGeneral*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tblHeader indexPathForCell:cell];
        
        [self.tblHeader scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case TAG_500:
        {
            self.strDelName = textField.text;
        }
            break;
        case TAG_600:
        {
            self.strDelAddress1 = textField.text;
            
        }
            break;
            
        case TAG_700:
        {
            self.strDelAddress2 = textField.text;
        }
            break;
            
        case TAG_800:
        {
            self.strDelAddress3 = textField.text;
        }
            break;
            
        case TAG_900:
        {
            self.strDelSubUrb = textField.text;
        }
            break;
            
        case TAG_1000:
        {
            self.strDelPostCode = textField.text;
        }
            break;
            
        case TAG_1100:
        {
            self.strDelCountry = textField.text;
        }
            break;
            
        case TAG_1200:
        {
            self.strDelInst1 = textField.text;
        }
            break;
            
        case TAG_1300:
        {
            self.strDelInst2 = textField.text;
        }
            break;
            
        case TAG_1400:
        {
            self.strCustOrder = textField.text;
        }
            break;
            
            
        default:
            break;
    }
    
    
}

/*
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
 {
 [textField resignFirstResponder];
 
 if (textField.tag == 2) {
 [_tblDetails setContentOffset:CGPointMake(0, 0) animated:YES];
 }
 return YES;
 }
 */



#pragma mark - Check for detail
-(void)callGetSalesDetailsDetailsFromDB:(NSString *)debtorNum{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        @try {
            
            dictHeader = [[NSMutableDictionary alloc] init];
            
            //Get Customer Details
//            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM armaster WHERE TRIM(CODE) = ?",debtorNum];
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM soheader_offline WHERE TRIM(ORDER_NO) = ?",debtorNum];// AND trim(SALESMAN) = ? [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]

            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictDataArmaster = nil;
            if ([rs1 next]) {
                NSLog(@"########## %@",dictDataArmaster);
                
                dictDataArmaster = [rs1 resultDictionary];
                [dict setValue:[dictDataArmaster objectForKey:@"ORDER_NO"] forKey:@"Order_no"];

                [dict setValue:[dictDataArmaster objectForKey:@"DEBTOR"] forKey:@"Debtor"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_NAME"] forKey:@"Name"];
                
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS1"] trimSpaces] forKey:@"Address1"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS2"] trimSpaces] forKey:@"Address2"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS3"] trimSpaces]forKey:@"Address3"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_DATE"] forKey:@"DelDate"];
                [dict setValue:[dictDataArmaster objectForKey:@"DELIVERY_RUN"] forKey:@"DelRun"];
//
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_SUBURB"] forKey:@"Suburb"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_POST_CODE"] forKey:@"PostCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_COUNTRY"] forKey:@"Country"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_NAME"] forKey:@"DelName"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_SUBURB"] trimSpaces]  forKey:@"DelSuburb"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_POST_CODE"] trimSpaces] forKey:@"DelPostCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"PHONE"] forKey:@"Phone"];
                [dict setValue:[dictDataArmaster objectForKey:@"CONTACT"] forKey:@"contact"];

                [dict setValue:[dictDataArmaster objectForKey:@"EMAIL1"] forKey:@"Email"];
                [dict setValue:[dictDataArmaster objectForKey:@"FAX_NO"] forKey:@"Fax"];
                [dict setValue:[dictDataArmaster objectForKey:@"CONTACT1"] forKey:@"Contact"];
                [dict setValue:[dictDataArmaster objectForKey:@"CREDIT_LIMIT"] forKey:@"CreditLimit"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_COUNTRY"] trimSpaces] forKey:@"DelCountry"];
                [dict setValue:[dictDataArmaster objectForKey:@"BRANCH"] forKey:@"Branch"];
                [dict setValue:[dictDataArmaster objectForKey:@"PRICE_CODE"]  forKey:@"PriceCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"TOTAL_DUE"] forKey:@"Balance"];
                [dict setValue:[dictDataArmaster objectForKey:@"SALESMAN"]  forKey:@"Salesman"];
                [dict setValue:[dictDataArmaster objectForKey:@"SALES_BRANCH"] forKey:@"SalesBranch"];
                [dict setValue:[dictDataArmaster objectForKey:@"TERMS"] forKey:@"Terms"];
                [dict setValue:[dictDataArmaster objectForKey:@"TRADING_TERMS"] forKey:@"TradingTerms"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION1"] forKey:@"TaxExemption1"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION2"] forKey:@"TaxExemption2"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION3"] forKey:@"TaxExemption3"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION4"] forKey:@"TaxExemption4"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION5"] forKey:@"TaxExemption5"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION6"] forKey:@"TaxExemption6"];
                [dict setValue:[dictDataArmaster objectForKey:@"EXCHANGE_CODE"] forKey:@"ExchangeCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"ALLOW_PARTIAL"] forKey:@"AllowPartial"];
                [dict setValue:[dictDataArmaster objectForKey:@"CHARGE_TYPE"] forKey:@"Chargetype"];
                [dict setValue:[dictDataArmaster objectForKey:@"CARRIER"] forKey:@"Carrier"];
                [dict setValue:[dictDataArmaster objectForKey:@"EXPORT_DEBTOR"] forKey:@"ExportDebtor"];
                [dict setValue:[dictDataArmaster objectForKey:@"RATE"] forKey:@"ChargeRate"];
                [dict setValue:[dictDataArmaster objectForKey:@"HOLD"] forKey:@"Held"];
                
                [dict setValue:[dictDataArmaster objectForKey:@"CASH_REP_PICKUP"] forKey:@"CASH_REP_PICKUP"];
                [dict setValue:[dictDataArmaster objectForKey:@"PICKUP_INFO"] forKey:@"PICKUP_INFO"];

                
//                DATE_RAISED
                
                NSString *str_current_Date=[NSString stringWithFormat:@"%@",[dictDataArmaster objectForKey:@"DATE_RAISED"]];
                
//                NSString *str_current_Date = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:str] ;
                
                //Current date
                [dict setValue:str_current_Date forKey:@"OrderDate"];
                [dict setValue:@"0" forKey:@"Numboxes"];
                [dict setValue:@"N" forKey:@"Direct"];
                [dictHeaderDetails setObject:dict forKey:@"Data"];
                
            }
            
            [rs1 close];
            [self prepareHeaderData];
             [_tblHeader reloadData];
            return ;
            
          
            FMResultSet *rs2 = [db executeQuery:@"SELECT `DEBTOR`, `CONTACT`, `DEL_INST1` as DelInst1, `DEL_INST2` as DelInst2, `CUST_ORDER` as CustOrderno, `WAREHOUSE` as Warehouse from `soheader` WHERE trim(DEBTOR) = ?",[debtorNum trimSpaces]];

            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs2 next]) {
                NSDictionary *dictData = [rs2 resultDictionary];
                [dictData setValue:@"1" forKey:@"Status"];
                [dictHeader setObject:dictData forKey:@"Header"];
            }
            
            [rs2 close];
            
            //Year & Period raised
            //Year & Period raised calculation
            int currentMonth = [[AppDelegate getCurrentMonth] intValue];
            int currentYear = [[AppDelegate getCurrentYear] intValue];
            int newmonth = 0;
            if(currentMonth < 7)
            {
                newmonth = currentMonth+6;
                currentYear = currentYear - 1;
            }
            else
            {
                newmonth = currentMonth-6;
            }
            
            NSString *strPeriodRaised = [NSString stringWithFormat:@"%d",newmonth];
            NSDictionary *dictData = [NSDictionary new];
            [dict setValue:strPeriodRaised forKey:@"PeriodRaised"];
            [dict setValue:[NSString stringWithFormat:@"%d",currentYear] forKey:@"YearRaised"];
            [dictHeader setObject:dict forKey:@"Data"];
            dictData=nil;
            
            
            //            FMResultSet *rs3 = [db executeQuery:@"SELECT SA_PERIOD,SA_FIN_YEAR FROM syscont WHERE RECNUM = 1"];
            //
            //            if (!rs3)
            //            {
            //                [rs3 close];
            //                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            //                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            //            }
            //
            //            if ([rs3 next]) {
            //                NSDictionary *dictData = [rs3 resultDictionary];
            //                [dict setValue:[dictData objectForKey:@"SA_PERIOD"] forKey:@"PeriodRaised"];
            //                [dict setValue:[dictData objectForKey:@"SA_FIN_YEAR"] forKey:@"YearRaised"];
            //                [mainDict setObject:dict forKey:@"Data"];
            //                dictData=nil;
            //
            //            }
            
            
            //            NSArray *arrOldDeliveryRun = [[NSArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
            //                                                                           @"1",@"key",
            //                                                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN1"],@"value",
            //                                                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"2",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN2"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"3",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN3"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"4",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN4"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"5",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN5"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"6",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN6"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                            @"7",@"key",
            //                                            [dictDataArmaster objectForKey:@"DELIVERY_RUN7"],@"value",
            //                                            nil],
            //                                          nil];
            //
            //
            //            NSNumber *numCurrDayOfWeek = [NSNumber numberWithInteger: [AppDelegate getCurrentDayOfWeek]];
            //            int currDayOfWeek = [numCurrDayOfWeek intValue];
            //
            //            NSArray *arrFrom = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(currDayOfWeek, [arrOldDeliveryRun count] - currDayOfWeek)];
            //
            //            NSArray *arrTo = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(0, currDayOfWeek)];
            //
            //            NSArray *arrNewDeliveryRun = [arrFrom arrayByAddingObjectsFromArray:arrTo];
            //
            //            NSString *strDeliveryRun = nil;
            //            BOOL isDeliveryRunFound = FALSE;
            //
            //            NSString *strDeliveryDate = nil;
            //            NSString *strDropSequence = nil;
            //
            //            for (int i = 0; i < [arrNewDeliveryRun count]; i++) {
            //
            //                if (![[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"] isEqualToString:@""]) {
            //
            //                    //Execute one time
            //                    isDeliveryRunFound = TRUE;
            //                    strDeliveryRun = [[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"];
            //
            //                    int newDay = [[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"key"] intValue];
            //
            //                    //DropSequence
            //                    NSString *strDropSeqKey = [NSString stringWithFormat:@"DROP_SEQUENCE%d",newDay];
            //                    strDropSequence = [dictDataArmaster objectForKey:strDropSeqKey];
            //                    strDropSeqKey = nil;
            //
            //                    NSDate *today = [NSDate date];
            //                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            //                    [gregorian setLocale:[NSLocale currentLocale]];
            //
            //                    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
            //
            //                    if (newDay > currDayOfWeek) {
            //                        [nowComponents setWeek: [nowComponents week]];//Current Week
            //                    }
            //                    else{
            //                        [nowComponents setWeek: [nowComponents week] + 1]; //Next week
            //                    }
            //
            //                    newDay = ((newDay + 7) % 7) + 1; // Transforming so that monday = 2 and sunday = 1
            //
            //                    [nowComponents setWeekday:newDay];
            //                    [nowComponents setHour:8]; //8a.m.
            //                    [nowComponents setMinute:0];
            //                    [nowComponents setSecond:0];
            //
            //                    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
            //
            //                    strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:beginningOfWeek];
            //                    //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //
            //                    break;
            //                }
            //            }
            //
            //            if (!isDeliveryRunFound) {
            //                strDeliveryRun = [dictDataArmaster objectForKey:@"DELIVERY_RUN"];
            //
            //                NSDate *now = [NSDate date];
            //                int daysToAdd = 1;
            //                NSDate *nextDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            //                strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:nextDate];
            //                //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //                //DropSequence
            //                strDropSequence = [dictDataArmaster objectForKey:@"DROP_SEQUENCE"];
            //
            //            }
            //
            //            NSDictionary *dictDeliveryRun = [mainDict objectForKey:@"Data"];
            //            [dictDeliveryRun setValue:strDeliveryRun forKey:@"DeliveryRun"];
            //            [dictDeliveryRun setValue:strDeliveryDate forKey:@"DeliveryDate"];
            //            [dictDeliveryRun setValue:strDropSequence forKey:@"DropSequence"];
            
            FMResultSet *rs34= [db executeQuery:@"SELECT DELIVERY_RUN,DELIVERY_RUN1,DELIVERY_RUN2,DELIVERY_RUN3,DELIVERY_RUN4,DELIVERY_RUN5,DELIVERY_RUN6,DELIVERY_RUN7,DROP_SEQUENCE FROM armaster WHERE trim(`CODE`) = ?",[debtorNum trimSpaces]];
            
            if (!rs34)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs34 next]) {
                
                NSDictionary *dictData = [rs34 resultDictionary];
                //NSLog(@"%@",dictData);
                //NSString *strDeliveryRun;
                
                if(![[dictData objectForKey:@"DELIVERY_RUN"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN1"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN1"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN1"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN2"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN2"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN2"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN3"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN3"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN3"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN4"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN4"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN4"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN5"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN5"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN5"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN6"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN6"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN6"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN7"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN7"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN7"] trimSpaces]];
                }
                
                FMResultSet *rs4= [db executeQuery:@"SELECT VALUE as yPosition FROM sysdesc WHERE trim(`CODE`) = ?",strDeliveryRun];
                
                if (!rs4)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                //Calculate delivery date
                if ([rs4 next]) {
                    
                    NSDictionary *dictDataPosition = [rs4 resultDictionary];
                    
                    // NSLog(@"dictData4 %@", dictData);
                    NSRange range = [[dictDataPosition objectForKey:@"yPosition"] rangeOfString:@"Y"];
                    
                    int yposition = range.location + 1;
                    
                    int currDayOfWeek = [[AppDelegate getCurrentDayOfWeek2] intValue];
                    
                    int interval;
                    if (currDayOfWeek == 1 ) {
                        interval = 9;
                    }
                    else{
                        interval = currDayOfWeek + 1;
                    }
                    
                    interval = 9 - interval;
                    
                    int tempPosition = interval + yposition;
                    
                    int temp;
                    
                    if(tempPosition >= 7)
                    {
                        temp = tempPosition % 7;
                    }
                    else
                    {
                        temp = tempPosition;
                    }
                    
                    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*(temp)];
                    NSString *strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:newDate];
                    
                    NSDictionary *dict = [dictHeader objectForKey:@"Data"];
                    [dict setValue:strDeliveryDate forKey:@"DeliveryDate"];
                    [dict setValue:strDeliveryRun forKey:@"DeliveryRun"];
                    [dict setValue:[dictData objectForKey:@"DROP_SEQUENCE"] forKey:@"DropSequence"];
                    //DATE_ADD( CURDATE( ) , INTERVAL( ( 9 - IF( DAYOFWEEK( CURDATE( ) ) =1, 9, (DAYOFWEEK( CURDATE( ) ) +1 ) ) ) + %d )DAY)
                }
                
                
            }
            
            //            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `NEW_USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            
            //
            if (!rs6)
            {
                [rs6 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs6 next]) {
                
                NSDictionary *dictData = [rs6 resultDictionary];
                
                if ([[dictData objectForKey:@"Commenttxt"] isEqual:[NSNull null]]) {
                    [dictData setValue:@"False" forKey:@"Flag"];
                }
                else{
                    [dictData setValue:@"True" forKey:@"Flag"];
                }
                
                [dictHeader setObject:dictData forKey:@"Comments"];
                
            }
            else{
                NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
                [dictData setValue:@"False" forKey:@"Flag"];
                [dictHeader setObject:dictData forKey:@"Comments"];
                
            }
            
            //NSLog(@"mainDict : %@",mainDict);
            [rs6 close];
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    
    NSLog(@"Check - %@",dictHeader);
}

-(void)callGetProfileDetailsforProducts:(NSString *)debtorNum{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        @try {
            
          //  dictHeader = [[NSMutableDictionary alloc] init];
            
            //Get Customer Details
          /*  if(strOrderNum.length > 0){
                FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) FROM sodetail_offline where ORDER_NO = ?",strOrderNum];
                
                if (!rs1)
                {
                    [rs1 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                while ([rs1 next]) {
                    NSDictionary *dict = [rs1 resultDictionary];
                    line_num = [[dict objectForKey:@"MAX(LINE_NO)"]integerValue];                            }
            }
            line_num=line_num+1;*/

            
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM sodetail_offline WHERE TRIM(ORDER_NO) = ?",debtorNum];
            
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
        /*    if(strOrderNum.length > 0){
                FMResultSet *rs1 = [db executeQuery:@"SELECT MAX(LINE_NO) FROM sodetail_offline where ORDER_NO = ?",strOrderNum];
                
                if (!rs1)
                {
                    [rs1 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                while ([rs1 next]) {
                    NSDictionary *dict = [rs1 resultDictionary];
                    line_num = [[dict objectForKey:@"MAX(LINE_NO)"]integerValue];                            }
            }
            line_num=line_num+1;*/

            
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
           [arrProducts removeAllObjects];
            while ([rs1 next]) {
                NSDictionary *dictProduct = [rs1 resultDictionary];
                [arrProducts addObject:dictProduct];
                
                float extnPrice = [[dictProduct objectForKey:@"EXTENSION"] floatValue];
                totalCost += extnPrice;
                
                //dictProduct = nil;
            }
            lblTotalCost.text = [NSString stringWithFormat:@"%f",totalCost];
            [rs1 close];
            return ;
            
            
           
            FMResultSet *rs2 = [db executeQuery:@"SELECT `DEBTOR`, `CONTACT`, `DEL_INST1` as DelInst1, `DEL_INST2` as DelInst2, `CUST_ORDER` as CustOrderno, `WAREHOUSE` as Warehouse from `soheader` WHERE trim(DEBTOR) = ?",[debtorNum trimSpaces]];
            
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs2 next]) {
                NSDictionary *dictData = [rs2 resultDictionary];
                [dictData setValue:@"1" forKey:@"Status"];
                [dictHeader setObject:dictData forKey:@"Header"];
            }
            
            [rs2 close];
            
            //Year & Period raised
            //Year & Period raised calculation
            int currentMonth = [[AppDelegate getCurrentMonth] intValue];
            int currentYear = [[AppDelegate getCurrentYear] intValue];
            int newmonth = 0;
            if(currentMonth < 7)
            {
                newmonth = currentMonth+6;
                currentYear = currentYear - 1;
            }
            else
            {
                newmonth = currentMonth-6;
            }
            
            NSString *strPeriodRaised = [NSString stringWithFormat:@"%d",newmonth];
            NSDictionary *dictData = [NSDictionary new];
            [dict setValue:strPeriodRaised forKey:@"PeriodRaised"];
            [dict setValue:[NSString stringWithFormat:@"%d",currentYear] forKey:@"YearRaised"];
            [dictHeader setObject:dict forKey:@"Data"];
            dictData=nil;
            
            
            //            FMResultSet *rs3 = [db executeQuery:@"SELECT SA_PERIOD,SA_FIN_YEAR FROM syscont WHERE RECNUM = 1"];
            //
            //            if (!rs3)
            //            {
            //                [rs3 close];
            //                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            //                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            //            }
            //
            //            if ([rs3 next]) {
            //                NSDictionary *dictData = [rs3 resultDictionary];
            //                [dict setValue:[dictData objectForKey:@"SA_PERIOD"] forKey:@"PeriodRaised"];
            //                [dict setValue:[dictData objectForKey:@"SA_FIN_YEAR"] forKey:@"YearRaised"];
            //                [mainDict setObject:dict forKey:@"Data"];
            //                dictData=nil;
            //
            //            }
            
            
            //            NSArray *arrOldDeliveryRun = [[NSArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
            //                                                                           @"1",@"key",
            //                                                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN1"],@"value",
            //                                                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"2",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN2"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"3",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN3"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"4",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN4"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"5",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN5"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"6",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN6"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                            @"7",@"key",
            //                                            [dictDataArmaster objectForKey:@"DELIVERY_RUN7"],@"value",
            //                                            nil],
            //                                          nil];
            //
            //
            //            NSNumber *numCurrDayOfWeek = [NSNumber numberWithInteger: [AppDelegate getCurrentDayOfWeek]];
            //            int currDayOfWeek = [numCurrDayOfWeek intValue];
            //
            //            NSArray *arrFrom = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(currDayOfWeek, [arrOldDeliveryRun count] - currDayOfWeek)];
            //
            //            NSArray *arrTo = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(0, currDayOfWeek)];
            //
            //            NSArray *arrNewDeliveryRun = [arrFrom arrayByAddingObjectsFromArray:arrTo];
            //
            //            NSString *strDeliveryRun = nil;
            //            BOOL isDeliveryRunFound = FALSE;
            //
            //            NSString *strDeliveryDate = nil;
            //            NSString *strDropSequence = nil;
            //
            //            for (int i = 0; i < [arrNewDeliveryRun count]; i++) {
            //
            //                if (![[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"] isEqualToString:@""]) {
            //
            //                    //Execute one time
            //                    isDeliveryRunFound = TRUE;
            //                    strDeliveryRun = [[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"];
            //
            //                    int newDay = [[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"key"] intValue];
            //
            //                    //DropSequence
            //                    NSString *strDropSeqKey = [NSString stringWithFormat:@"DROP_SEQUENCE%d",newDay];
            //                    strDropSequence = [dictDataArmaster objectForKey:strDropSeqKey];
            //                    strDropSeqKey = nil;
            //
            //                    NSDate *today = [NSDate date];
            //                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            //                    [gregorian setLocale:[NSLocale currentLocale]];
            //
            //                    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
            //
            //                    if (newDay > currDayOfWeek) {
            //                        [nowComponents setWeek: [nowComponents week]];//Current Week
            //                    }
            //                    else{
            //                        [nowComponents setWeek: [nowComponents week] + 1]; //Next week
            //                    }
            //
            //                    newDay = ((newDay + 7) % 7) + 1; // Transforming so that monday = 2 and sunday = 1
            //
            //                    [nowComponents setWeekday:newDay];
            //                    [nowComponents setHour:8]; //8a.m.
            //                    [nowComponents setMinute:0];
            //                    [nowComponents setSecond:0];
            //
            //                    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
            //
            //                    strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:beginningOfWeek];
            //                    //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //
            //                    break;
            //                }
            //            }
            //
            //            if (!isDeliveryRunFound) {
            //                strDeliveryRun = [dictDataArmaster objectForKey:@"DELIVERY_RUN"];
            //
            //                NSDate *now = [NSDate date];
            //                int daysToAdd = 1;
            //                NSDate *nextDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            //                strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:nextDate];
            //                //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //                //DropSequence
            //                strDropSequence = [dictDataArmaster objectForKey:@"DROP_SEQUENCE"];
            //
            //            }
            //
            //            NSDictionary *dictDeliveryRun = [mainDict objectForKey:@"Data"];
            //            [dictDeliveryRun setValue:strDeliveryRun forKey:@"DeliveryRun"];
            //            [dictDeliveryRun setValue:strDeliveryDate forKey:@"DeliveryDate"];
            //            [dictDeliveryRun setValue:strDropSequence forKey:@"DropSequence"];
            
            FMResultSet *rs34= [db executeQuery:@"SELECT DELIVERY_RUN,DELIVERY_RUN1,DELIVERY_RUN2,DELIVERY_RUN3,DELIVERY_RUN4,DELIVERY_RUN5,DELIVERY_RUN6,DELIVERY_RUN7,DROP_SEQUENCE FROM armaster WHERE trim(`CODE`) = ?",[debtorNum trimSpaces]];
            
            if (!rs34)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs34 next]) {
                
                NSDictionary *dictData = [rs34 resultDictionary];
                //NSLog(@"%@",dictData);
               // NSString *strDeliveryRun;
                
                if(![[dictData objectForKey:@"DELIVERY_RUN"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN1"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN1"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN1"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN2"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN2"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN2"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN3"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN3"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN3"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN4"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN4"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN4"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN5"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN5"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN5"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN6"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN6"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN6"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN7"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN7"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN7"] trimSpaces]];
                }
                
                FMResultSet *rs4= [db executeQuery:@"SELECT VALUE as yPosition FROM sysdesc WHERE trim(`CODE`) = ?",strDeliveryRun];
                
                if (!rs4)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                //Calculate delivery date
                if ([rs4 next]) {
                    
                    NSDictionary *dictDataPosition = [rs4 resultDictionary];
                    
                    // NSLog(@"dictData4 %@", dictData);
                    NSRange range = [[dictDataPosition objectForKey:@"yPosition"] rangeOfString:@"Y"];
                    
                    int yposition = range.location + 1;
                    
                    int currDayOfWeek = [[AppDelegate getCurrentDayOfWeek2] intValue];
                    
                    int interval;
                    if (currDayOfWeek == 1 ) {
                        interval = 9;
                    }
                    else{
                        interval = currDayOfWeek + 1;
                    }
                    
                    interval = 9 - interval;
                    
                    int tempPosition = interval + yposition;
                    
                    int temp;
                    
                    if(tempPosition >= 7)
                    {
                        temp = tempPosition % 7;
                    }
                    else
                    {
                        temp = tempPosition;
                    }
                    
                    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*(temp)];
                    NSString *strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:newDate];
                    
                    NSDictionary *dict = [dictHeader objectForKey:@"Data"];
                    [dict setValue:strDeliveryDate forKey:@"DeliveryDate"];
                    [dict setValue:strDeliveryRun forKey:@"DeliveryRun"];
                    [dict setValue:[dictData objectForKey:@"DROP_SEQUENCE"] forKey:@"DropSequence"];
                    //DATE_ADD( CURDATE( ) , INTERVAL( ( 9 - IF( DAYOFWEEK( CURDATE( ) ) =1, 9, (DAYOFWEEK( CURDATE( ) ) +1 ) ) ) + %d )DAY)
                }
                
                
            }
            
            //            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `NEW_USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            
            //
            if (!rs6)
            {
                [rs6 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs6 next]) {
                
                NSDictionary *dictData = [rs6 resultDictionary];
                
                if ([[dictData objectForKey:@"Commenttxt"] isEqual:[NSNull null]]) {
                    [dictData setValue:@"False" forKey:@"Flag"];
                }
                else{
                    [dictData setValue:@"True" forKey:@"Flag"];
                }
                
                [dictHeader setObject:dictData forKey:@"Comments"];
                
            }
            else{
                NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
                [dictData setValue:@"False" forKey:@"Flag"];
                [dictHeader setObject:dictData forKey:@"Comments"];
                
            }
            
            //NSLog(@"mainDict : %@",mainDict);
            [rs6 close];
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    
    NSLog(@"Check - %@",dictHeader);
}

#pragma mark - Edit Order


- (IBAction)actionDeleteRows:(id)sender{
    
    if ([_tblDetails isEditing]) {
        // [self showOrEnableButtons];
        
//        btnUpload.enabled = TRUE;
        
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [_tblDetails setEditing:NO animated:YES];
    }
    else{
        
//        btnUpload.enabled = FALSE;
        
        // [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [_tblDetails setEditing:YES animated:YES];
    }
    
    [_tblDetails reloadData];
}





- (IBAction)actionEditOrder:(id)sender {
    NSLog(@"Edit Clicked");
    
    _editnewBtn.selected = ! _editnewBtn.selected;
 
    
    if (_editnewBtn.selected) {
        
      isEdit = YES;
            [_editnewBtn setTitle:@"Done" forState:UIControlStateNormal];
            [_editnewBtn setTitle:@"Done" forState:UIControlStateHighlighted];
          }
    else{
        if (isEdit == YES) {
            isEdit = NO;
            [_editnewBtn setTitle:@"Edit" forState:UIControlStateNormal];
            [_editnewBtn setTitle:@"Edit" forState:UIControlStateHighlighted];
            [self callInsertProfileOrderToDB];
            return;
        }
        
        isEdit = NO;
        [_editnewBtn setTitle:@"Edit" forState:UIControlStateNormal];
        [_editnewBtn setTitle:@"Edit" forState:UIControlStateHighlighted];
    }
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]) {
            }
            
            if ([strWebserviceType isEqualToString:@"WS_GET_PRODUCT_IN_DETAILS"]) {
//                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//                [databaseQueue   inDatabase:^(FMDatabase *db) {
//                    [self callViewQuoteDetailsDetailsFromDB:db];
//                    
//                }];
            }
            
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
//                [self callWSGetNewOrderNumber];
            }
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
//                [self callWSGetNewOrderNumber];
            }
            break;
        }
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


@end
