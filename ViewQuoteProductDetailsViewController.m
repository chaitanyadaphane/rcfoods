//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "ViewQuoteProductDetailsViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"

#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

#define TAG_100 100

@interface ViewQuoteProductDetailsViewController ()
{
    NSString *strChangedPrice;
}

@end

@implementation ViewQuoteProductDetailsViewController
@synthesize dictProductDetails,strSellingPrice,isFromViewQuoteDetails,delegate,productIndex,strPrice,strDebtor,isQuoteEditable,operationQueue,strExcl,strExtension,strTax,isTaxApplicable;

@synthesize tblDetails,lblProductName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lblProductName.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Description"] trimSpaces]];
    
    floatProfitPercentage = 0;
    strSellingPrice = [[NSString alloc] init];
    
    //1 by default
    quantityOrdered = 1;
    
    NSString *path;
    
    if (isFromViewQuoteDetails) {
        path = [AppDelegate getFileFromLocalDirectory:@"QuoteProductDetails" Type:@"plist"];
    }
    else{
        path = [AppDelegate getFileFromLocalDirectory:@"QuoteProductDetailsDetails" Type:@"plist"];
    }
    
    arrProductLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    self.isTaxApplicable = YES;
    
    //Cost Price
    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"AverageCost"]];
    
    operationQueue = [[NSOperationQueue alloc] init];
    
    if ([dictProductDetails objectForKey:@"SellingPrice"]) {
        self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"SellingPrice"]];
    }
    
    //Quote  -- Edit Quote
    if ([AppDelegate getAppDelegateObj].intCurrentMenuId == 1 && [AppDelegate getAppDelegateObj].intCurrentSubMenuId == 1){
    
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callGetTaxInfoFromDB:db];
                [self callGetDiscountFromDB:db];
                
            }];
            databaseQueue = nil;
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                if (isTaxApplicable) {
                    self.strTax = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Tax"]];
                }
                else{
                    self.strTax = @"NA";
                }
                
                self.strExcl = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Gross"]];
                self.strExtension = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"ExtnPrice"]];
                
                if ([dictProductDetails objectForKey:@"SellingPrice"]) {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"SellingPrice"]];
                }
                
                [tblDetails reloadData];
            }];
            
            
        }];
    }
    else{
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callGetTaxInfoFromDB:db];
                [self callGetDiscountFromDB:db];
                
            }];
            databaseQueue = nil;
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                [tblDetails reloadData];
            }];
            
            
        }];
        
    }
    
}

- (void)viewDidUnload{
    self.lblProductName = nil;
    self.tblDetails = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnDonePressed
{
    [self calcMargin];
    
//    if(floatProfitPercentage <= 0)
//    {
//        [AJNotificationView showNoticeInView:self.view
//                                        type:AJNotificationTypeRed
//                                       title:MARGIN_PERCENT_ERROR
//                             linedBackground:AJLinedBackgroundTypeDisabled
//                                   hideAfter:2.5f];
//        
//        [tblDetails reloadData];
//        return;
//    }

    
    // Store changed price before you check the margin greater than zero
   
    
    if (floatProfitPercentage >= MARGIN_MINIMUM_PERCENT_QUOTE) {
        self.strExcl= [NSString stringWithFormat:@"%f",[self calcExcl]];
        self.strTax= [NSString stringWithFormat:@"%f",[self calcTax]];
        self.strExtension= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
        
        [tblDetails reloadData];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:strExcl forKey:@"Gross"];
        [dict setValue:strExtension forKey:@"Extension"];
        [dict setValue:strTax forKey:@"Tax"];
        [dict setValue:strSellingPrice forKey:@"SellingPrice"];
        
        [self.delegate setPriceOfProduct:[strExtension floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict ];
        
        
    }
    else{
        
//        [AJNotificationView showNoticeInView:self.view
//                                        type:AJNotificationTypeRed
//                                       title:MARGIN_PERCENT_ERROR
//                             linedBackground:AJLinedBackgroundTypeDisabled
//                                   hideAfter:2.5f];
    }

}
#pragma mark - Text Field Delgates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  CGRect viewFrame = self.view.frame;
    // viewFrame.origin.y = upframeByYPosition;
    // [AppDelegate pullUpPushDownViews:self.view ByRect:viewFrame];
    if (textField.tag == TAG_100) {
        self.strSellingPrice = textField.text;
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == TAG_100) {
         strChangedPrice = textField.text;
        return [[AppDelegate getAppDelegateObj] allowFloatingNumbersOnly:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    }
    
    return TRUE;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag == TAG_100) {
        self.strSellingPrice = textField.text;
        [self calcMargin];
        if (floatProfitPercentage >= MARGIN_MINIMUM_PERCENT_QUOTE) {
            self.strExcl= [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax= [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strExtension= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:strExcl forKey:@"Gross"];
            [dict setValue:strExtension forKey:@"Extension"];
            [dict setValue:strTax forKey:@"Tax"];
            [dict setValue:strSellingPrice forKey:@"SellingPrice"];
            
            [self.delegate setPriceOfProduct:[strExtension floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict ];
            
            
        }
        else{
            
//            [AJNotificationView showNoticeInView:self.view
//                                            type:AJNotificationTypeRed
//                                           title:MARGIN_PERCENT_ERROR
//                                 linedBackground:AJLinedBackgroundTypeDisabled
//                                       hideAfter:2.5f];
        }
        
        
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self calcMargin];
    
//    if(floatProfitPercentage <= 0)
//    {
//        [AJNotificationView showNoticeInView:self.view
//                                        type:AJNotificationTypeRed
//                                       title:MARGIN_PERCENT_ERROR
//                             linedBackground:AJLinedBackgroundTypeDisabled
//                                   hideAfter:2.5f];
//        
//        [tblDetails reloadData];
//        return YES;
//    }
    
    
    // Store changed price before you check the margin greater than zero
    strChangedPrice = textField.text;
    if(![strChangedPrice isEqualToString:strSellingPrice])
    {
        strSellingPrice = strChangedPrice;
    }
    
      if (floatProfitPercentage >= MARGIN_MINIMUM_PERCENT_QUOTE) {
        self.strExcl= [NSString stringWithFormat:@"%f",[self calcExcl]];
        self.strTax= [NSString stringWithFormat:@"%f",[self calcTax]];
        self.strExtension= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
        
        [tblDetails reloadData];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:strExcl forKey:@"Gross"];
        [dict setValue:strExtension forKey:@"Extension"];
        [dict setValue:strTax forKey:@"Tax"];
        [dict setValue:strSellingPrice forKey:@"SellingPrice"];
        
        [self.delegate setPriceOfProduct:[strExtension floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict ];
        
        
    }
    else{
        
//        [AJNotificationView showNoticeInView:self.view
//                                        type:AJNotificationTypeRed
//                                       title:MARGIN_PERCENT_ERROR
//                             linedBackground:AJLinedBackgroundTypeDisabled
//                                   hideAfter:2.5f];
    }
    return YES;
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrProductLabels count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    
    CustomCellGeneral *cell = nil;
    
    if (isFromViewQuoteDetails) {
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell = [nib objectAtIndex:4];
            
        }
        
        if ([indexPath row] == 0) {
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"LineNo"]];
        }
        if ([indexPath row] == 1) {
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Item"]];
        }
        if ([indexPath row] == 2) {
            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"Price"] floatValue]];
        }
        
        if ([indexPath row] == 5) {
            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"Extension"] floatValue]];
        }
        
        if ([indexPath row] == 6) {
            //cell.lblValue.text = [NSString stringWithFormat:@"%f",floatProfitPercentage];
            cell.lblValue.text = [NSString stringWithFormat:@"%@%@",[dictProductDetails objectForKey:@"DiscPercent"],@"%"];
        }
        
    }
    else{
        
        //Selling Price
        if ([indexPath row] == 3) {
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:9];
                
            }
            
//            cell.txtGlowingValue.text = strSellingPrice;
            NSString* formattedNumber = [NSString stringWithFormat:@"%.02f",[strSellingPrice floatValue] ];
            cell.txtGlowingValue.text = formattedNumber;
            cell.txtGlowingValue.tag = TAG_100;
            cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
            cell.txtGlowingValue.delegate = self;
            
        }
        else{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:4];
                
            }
            
            //Stockcode
            if ([indexPath row] == 0) {
                
                if ([dictProductDetails objectForKey:@"StockCode"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"StockCode"]] ;
                }
                else if ([dictProductDetails objectForKey:@"Item"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Item"]] ;
                }
                
            }
            //Warehouse
            if ([indexPath row] == 1) {
                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Warehouse"]] ;
            }
            
            //Cost Price
            if ([indexPath row] == 2) {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strPrice floatValue]];
            }
            
            //Excl
            if ([indexPath row] == 4) {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strExcl floatValue]];
            }
            
            //Tax
            if ([indexPath row] == 5) {
                if (isTaxApplicable) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strTax floatValue]];
                }
                else{
                    cell.lblValue.text = @"NA";
                }
            }
            
            //Extension
            if ([indexPath row] == 6) {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strExtension floatValue]];
            }
            
            //Profit
            if ([indexPath row] == 7) {
                cell.lblValue.text = [NSString stringWithFormat:@"%.2f%@",floatProfitPercentage,@"%"];
            }
            
            //Available
            if ([indexPath row] == 8) {
                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Available"]];
            }
            
            //Allocated
            if ([indexPath row] == 9) {
                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Allocated"]];
            }
            
        }
        
    }
    
    cell.lblTitle.text = [[arrProductLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    
    
    return cell;
    
}


#pragma mark - Database Calls
-(void)callGetPriceFromDB:(FMDatabase *)db{
    
    NSLog(@"strDebtor %@",strDebtor);
    FMResultSet *rs1;
    @try {
        rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    if ([dictProductDetails objectForKey:@"Price1"]) {
                        self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                    }
                    else if([dictProductDetails objectForKey:@"Price"]){
                        self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                    }
                    
                }
                    break;
                case 1:
                {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
                    
                case 2:
                {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                }
                    break;
                    
                case 3:
                {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                }
                    break;
                    
                case 4:
                {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                }
                    break;
                    
                case 5:
                {
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                }
                    break;
                    
                    
                default:{
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
            }
            
            if ([strSellingPrice isEqual:[NSNull null]] || [strSellingPrice isEqualToString:@"(null)"]) {
                if([dictProductDetails objectForKey:@"Price"]){
                    self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                }
                
            }
            
        }
        
        [rs1 close];
        [self CalculatePrice];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}


-(void)CalculatePrice
{
    [self calcMargin];
    if (floatProfitPercentage >= MARGIN_MINIMUM_PERCENT_QUOTE)
    {
        self.strExcl= [NSString stringWithFormat:@"%f",[self calcExcl]];
        self.strTax= [NSString stringWithFormat:@"%f",[self calcTax]];
        self.strExtension= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
        
        [tblDetails reloadData];
    }
}


-(void)callGetTaxInfoFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT TAX_CODE1 FROM sysistax WHERE CODE = ?",[dictProductDetails objectForKey:@"StockCode"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            if ([[dictData objectForKey:@"TAX_CODE1"] intValue] == -1) {
                self.isTaxApplicable = NO;
            }
            else{
                self.isTaxApplicable = YES;
            }
            
            dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

-(void)callGetDiscountFromDB:(FMDatabase *)db{
    [self callGetPriceFromDB:db];

    @try {
//        FMResultSet *rs1 = [db executeQuery:@"SELECT DISCOUNT1 FROM sysdisct WHERE STOCK_CODE = ?",[dictProductDetails objectForKey:@"StockCode"]];
//        
//        if (!rs1)
//        {
//            [rs1 close];
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//        }
//        
//        if ([rs1 next]) {
//            
//            NSDictionary *dictData = [rs1 resultDictionary];
//            [rs1 close];
//            self.strSellingPrice = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"DISCOUNT1"]];
//            
//            [self calcMargin];
//            if (floatProfitPercentage > MARGIN_MINIMUM_PERCENT_QUOTE) {
//                self.strExcl= [NSString stringWithFormat:@"%f",[self calcExcl]];
//                self.strTax= [NSString stringWithFormat:@"%f",[self calcTax]];
//                self.strExtension= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
//                
//                [tblDetails reloadData];
//            }
//
//            
//        }
//        else{
//            [rs1 close];
//            [self callGetPriceFromDB:db];
//            
//        }
//        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

- (IBAction)saveProductDetails:(id)sender{
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

#pragma mark - Custom Methods
-(float)calcTax{
    //If tax applicable
    if (isTaxApplicable) {
        //Tax is 10% of total
        return  (quantityOrdered*[strSellingPrice floatValue]*0.1);
        
    }
    
    return 0;
}

-(float)calcExcl{
    return  (quantityOrdered*[strSellingPrice floatValue]);
}


-(float)calcExtnIncl{
    return ([self calcExcl] + [self calcTax]);
}

-(void)calcMargin{
    
    float floatCostPrice = [strPrice floatValue];
    
    //Selling price
    float floatSellingPrice = [strSellingPrice floatValue];
    
    //Profit percentage
    floatProfitPercentage = ((floatSellingPrice - floatCostPrice)/floatSellingPrice)*100;
    
}

@end
