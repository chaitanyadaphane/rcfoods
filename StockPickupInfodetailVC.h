//
//  StockPickupInfodetailVC.h
//  Blayney
//
//  Created by Pooja on 02/01/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockPickupInfodetailVC : UIViewController{
    
//    NSMutableDictionary *dictProductDetails;
    NSMutableDictionary *dictStockValue;
    
    NSMutableArray *arrStockPickupInfoTitle;
    NSMutableArray *arrStockPickupInfoValues;

    
    NSString *strPickupNo;
    NSString *strDebtorName;
    NSString *strRecnum;
    
    dispatch_queue_t backgroundQueueForStockProductDetails;
    FMDatabaseQueue *dbQueueStockProductDetails;
    
    BOOL isNetworkConnected;
    
    MBProgressHUD *spinner;
    
}
@property (nonatomic,strong) NSMutableArray *arrStockPickupInfoTitle;
@property (nonatomic,strong) NSMutableArray *arrStockPickupInfoValues;
@property (nonatomic,strong)NSMutableDictionary *dictStockValue;

@property (nonatomic,strong)NSString *strPickupNo;
@property (nonatomic,strong)NSString *strDebtorName;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@property (weak, nonatomic) IBOutlet UILabel *deborNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *resendBtn;
- (IBAction)actionResendButton:(id)sender;

@end
