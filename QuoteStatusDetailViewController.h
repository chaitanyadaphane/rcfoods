//
//  QuoteStatusDetailViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NDHTMLtoPDF.h"
@interface QuoteStatusDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate,ReachabilityRequest_delegate>{

    NSMutableDictionary *dictProductDetails;
    NSArray *arrQuoteLabels;
    NSString *strQuoteNum;
    BOOL isQuoteApproved;
}

@property (nonatomic,retain) NSMutableDictionary *dictProductDetails;
@property (nonatomic,assign) BOOL isQuoteApproved;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblQuoteName;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *btnPrint;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *btnEmail;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *tblDetails;
@property (nonatomic, strong)  NSString *strQuoteNum;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
@property (weak, nonatomic) IBOutlet UITableView *tblProductList;
@property (weak, nonatomic) IBOutlet UIView *vwProductList;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;

- (IBAction)openMail:(id)sender;
- (IBAction)sendToPrinter:(id)sender;
@end
