//
//  PopUpViewController.m
//  Blayney
//
//  Created by Pooja Mishra on 15/10/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import "PopUpViewController.h"
#import "CustomCellGeneral.h"

#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"
@interface PopUpViewController ()
{
    MBProgressHUD *spinner;
}
@end

@implementation PopUpViewController
@synthesize lastIndexPath,arrPopupData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.arrPopupData count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER13;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:12];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arrPopupData objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
    cell.lblTitle.text =  [NSString stringWithFormat:@"%@",[[arrPopupData objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
    
    if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
    int newRow = (int)[indexPath row];
    int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
    
    if(newRow != oldRow)
    {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        lastIndexPath = indexPath;
    }
    
    NSString *strCode = [[[arrPopupData objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [self.delegate setCustomPopupValue:strCode];
}


@end
