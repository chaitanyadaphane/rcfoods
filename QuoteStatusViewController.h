//
//  QuoteStatusViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"

@interface QuoteStatusViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate>{
    
    NSMutableArray *arrQuoteList; // The master content.
    NSMutableArray *arrQuoteListForWebservices;
    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
    
    dispatch_queue_t backgroundQueueForQuoteStatusList;
    FMDatabaseQueue *dbQueueStatusList;
    
    int currPage;
    int totalCount;
    int recordNumber;
    
    BOOL isNetworkConnected;
    
    NSString *strWebserviceType;
    NSOperationQueue *operationQueue;
}

@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrQuoteList;
@property (nonatomic, retain) NSMutableArray *arrQuoteListForWebservices;
@property (nonatomic, retain) IBOutlet UIView *vwNoQuotes;
@property (nonatomic, retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic, retain) IBOutlet UIView *vwTblQuotes;
@property (nonatomic, retain) IBOutlet UITableView *tblQuoteList;

@property (nonatomic,unsafe_unretained) IBOutlet UISearchBar *searchingBar;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;
@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;


#pragma mark - Custom Methods
-(void)callWSGetQuoteList:(int)pageIndex;
-(void)callGetQuoteListFromDB:(FMDatabase *)db;

@end
