//
//  CurrentOrderViewControllerDetail.m
//  Blayney
//
//  Created by Subodh on 9/30/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "CurrentOrderViewControllerDetail.h"
#import "CustomCellGeneral.h"
#define TAG_50 50
#define TAG_100 100
#define TAG_200 200
#define TAG_300 300
#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10

#define CURRENT_ORDER_DETAIL @"webservices_rcf/currorderstatus/view_currorderstatus.php?"

bool EditableMode = NO;
@interface CurrentOrderViewControllerDetail ()
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UITableView *tblheader;
@property (weak, nonatomic) IBOutlet UIView *vwSegmentView;
@property (strong, nonatomic) IBOutlet UIView *productDetailView;
@property (weak, nonatomic) IBOutlet UITableView *tblProductView;

@end

@implementation CurrentOrderViewControllerDetail
@synthesize spinner,strOrderNum;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    arrHeaderLabels = [[NSArray alloc] initWithObjects:@"Debtor",@"Name",@"Date Raised",@"Del Date",@"Cust Order",@"Status",@"Order Lines",@"Order Value", nil];
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, self.vwSegmentView.frame.size.width, self.vwSegmentView.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self setUpInitialView];
    
    [self CallWebserviceToGetData];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [self.vwContentView removeAllSubviews];
   // btnDelete.hidden = YES;
    
    if (EditableMode==NO) {
        //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    }
    
    
    switch (sender.SelectedIndex) {
        case 0:{
            [self.tblheader reloadData];
            [self.vwContentView addSubview:self.vwHeader];
        }
            break;
            
        case 1:{
            
            
            [self.tblProductView reloadData];

            if (EditableMode == NO) {
                
                if ([arrProducts count]) {
                   
                [self.vwContentView addSubview:self.productDetailView];
                }
                else{
                    //No products
                 //   [vwContentSuperview addSubview:vwNoProducts];
                   // [self showPointingAnimation];
                }
            }
            else{
                //[self doAfterDataFetched];
            }
        }
            break;
            
        default:
            break;
    }
    
}

- (void)setUpInitialView{
    [self.vwSegmentView removeAllSubviews];
    [self.vwSegmentView addSubview:segControl];
    //Header view by default
    [self.vwContentView addSubview:self.vwHeader];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
            NSLog(@"numberOfSectionsInTableView %d",[arrHeaderLabels count]);
        case TAG_50:return 2;break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}
-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
   
    switch (tableView.tag)
    {
            
       case TAG_50:
        {
            if(section == 0)
                return 1;
            else if(section == 1)
                return 8;
        }
            break;
            
            
        case TAG_100:
          
            return [arrProducts count];
            break;
            
        default:return 0;break;
    }
    return 0;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    CustomCellGeneral *cell = nil;
    
    switch (tableView.tag) {
        case TAG_50:{
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:3];
                
            }
            
            if ([indexPath section] == 0) {
                
                cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"OrderNo"] valueForKey:@"text"]];
                cell.lblTitle.text = @"Order No";
            }
            
            if ([indexPath section] == 1) {
                
                
                cell.lblTitle.text = [arrHeaderLabels objectAtIndex:indexPath.row];
                switch ([indexPath row]) {
                    case 0:
                        if ([[dictHeaderDetails objectForKey:@"Debtor"] objectForKey:@"text"]) {
                            
                            NSLog(@"-$$$-Debtor::%@----",[NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Debtor"] valueForKey:@"text"]]);
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Debtor"] valueForKey:@"text"]];
                            
                        }
                        break;
                        
                    case 1:
                        if ([[dictHeaderDetails objectForKey:@"Delname"] objectForKey:@"text"]) {
                            NSLog(@"-$$$-Delname::%@",[NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Delname"] valueForKey:@"text"]]);
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Delname"] valueForKey:@"text"]];
                        }
                        break;
                        
                    case 2:
                        if ([[dictHeaderDetails objectForKey:@"DateRaised"] objectForKey:@"text"]) {
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] valueForKey:@"text"]];
                            
                        }
                        break;
                        
                    case 3:
                        if ([[dictHeaderDetails objectForKey:@"DelDate"] objectForKey:@"text"]) {
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelDate"] valueForKey:@"text"]];
                        }
                        break;
                        
                        
                    case 4:
                        if ([[dictHeaderDetails objectForKey:@"CustOrder"] objectForKey:@"text"]) {
                            
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"CustOrder"] valueForKey:@"text"]];
                        }
                        break;
                        
                        
                    case 5:
                        if ([[dictHeaderDetails objectForKey:@"Status"] objectForKey:@"text"]) {
                            
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Status"] valueForKey:@"text"]];
                        }
                        break;
                        
                    case 6:
                        if ([[dictHeaderDetails objectForKey:@"DetailLines"] objectForKey:@"text"]) {
                            
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DetailLines"] valueForKey:@"text"]];
                        }
                        break;
                        
                    case 7:
                        if ([[dictHeaderDetails objectForKey:@"OrderValue"] objectForKey:@"text"]) {
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"OrderValue"] valueForKey:@"text"]];
                            [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.txtValue];
                            
                        }
                        break;
                        
                    default:break;
                }
            }
            
            return cell;
            
        }
            break;
        case TAG_100:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:@"cellIdentifier15"];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:14];
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                
            }
            
            if([arrProducts isKindOfClass:[NSArray class]]){
                
                cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"] valueForKey:@"text"]];
                
                if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] valueForKey:@"text"] floatValue]];
                }
            }else if ([arrProducts isKindOfClass:[NSDictionary class]]){
                
                cell.lblTitle.text = [NSString stringWithFormat:@"%@",[(NSDictionary *)[(NSDictionary *)arrProducts objectForKey:@"Description"] valueForKey:@"text"]];
                
                if ([(NSDictionary *)arrProducts objectForKey:@"Extension"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[(NSDictionary *)arrProducts objectForKey:@"Extension"] valueForKey:@"text"] floatValue]];
                }
                
            }
            return cell;
        }break;
            
        default:return nil;break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_50 && indexPath.section == 1 && indexPath.row == 1)
    {
    
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[[dictHeaderDetails objectForKey:@"Delname"] objectForKey:@"text"] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        }
}

-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    return labelHeighSize.height;
    
}

-(void)CallWebserviceToGetData
{
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        spinner.mode = MBProgressHUDModeIndeterminate;
        [spinner removeFromSuperview];
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
            [self callWSGetAvailable];
        }
        else
        {
            UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:INTERNET_OFFLINE_MESSAGE delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [aler show];
            aler = nil;
            
        }
        
    }];
}

-(void)callWSGetAvailable{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strValue=[NSString stringWithFormat:@"%@orderno=%@",CURRENT_ORDER_DETAIL,strOrderNum];
    NSString *strURL=[AppDelegate getServiceURL:strValue];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    @try {
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileInDirectory1 = [documentsDirectory stringByAppendingPathComponent:@"TestNew.plist"];
        [dictResponse writeToFile:fileInDirectory1 atomically:YES];
        [spinner removeFromSuperview];
        
        if([dictResponse isKindOfClass:[NSDictionary class]])
        {
            if([[[dictResponse objectForKey:@"Response"]objectForKey:@"Order"] objectForKey:@"Header"])
            {
                dictHeaderDetails = nil;
                dictHeaderDetails = [[NSMutableDictionary alloc]initWithDictionary:[[[dictResponse objectForKey:@"Response"]objectForKey:@"Order"] objectForKey:@"Header"]];
                [self.vwContentView addSubview:self.vwHeader];
            
            }
            
            if([[[dictResponse objectForKey:@"Response"]objectForKey:@"Order"] objectForKey:@"Detail"])
            {
                dictProductDetails = nil;
                dictProductDetails = [[NSMutableDictionary alloc]initWithDictionary:[[[dictResponse objectForKey:@"Response"]objectForKey:@"Order"] objectForKey:@"Detail"]];
                
                [arrProducts removeAllObjects];
                arrProducts = nil;
                if(!arrProducts){
                
                    arrProducts = [[NSMutableArray alloc] init];
                }
                
                if(dictProductDetails.count > 0){
                    if ([[dictProductDetails objectForKey:@"Lines"] isKindOfClass:[NSArray class]]) {
                        arrProducts = [dictProductDetails objectForKey:@"Lines"];
                    }
                    else{
                        [arrProducts addObject:[dictProductDetails objectForKey:@"Lines"]];
                    }
                }
                else{
                    
                }
                
                
            }
        }
        
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    @finally {
        _tblheader.dataSource = self;
        _tblheader.delegate = self;
        [_tblheader reloadData];
        [spinner removeFromSuperview];
    }
}
@end
