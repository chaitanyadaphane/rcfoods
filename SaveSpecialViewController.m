//
//  SaveSpecialViewController.m
//  Blayney
//
//  Created by POOJA MISHRA on 27/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import "SaveSpecialViewController.h"
#import "KGModal.h"
@interface SaveSpecialViewController ()

@end

@implementation SaveSpecialViewController
@synthesize productDetailDict,selectType;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    selectType = @"M";
    self.titleLable.text = @"Enter the percent margin:";
    
    NSLog(@"productDetailDict %@",productDetailDict);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - select segment

- (IBAction)selectOptionAction:(id)sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0)
    {
        NSLog(@"margin selected");
        selectType = @"M";
        self.priceTxt.text = @"";
        self.resultvalue.text = @"";
        self.titleLable.text = @"Enter the percent margin (%):";
    }
    else
    {
        NSLog(@"absolute selected");
        selectType = @"A";
        self.priceTxt.text = @"";
        self.resultvalue.text = @"";
        self.titleLable.text = @"Enter the absolute price ($):";
    }
}


-(void)calMarginVal:(NSString *)percent
{
//    (STANDARD_COST /(1 - (DISCOUNT1 / 100)))
    float percent_float = [percent floatValue];
    float standardCost = [[productDetailDict objectForKey:@"Cost"]integerValue];
    float specialPrice =  (standardCost / (1 - percent_float/100));
    self.resultvalue.text = [NSString stringWithFormat:@"$ %.2f",specialPrice];
}

- (IBAction)saveSpecialAction:(id)sender
{
    [[KGModal sharedInstance]hideAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Saved special price successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark - 
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([selectType isEqualToString:@"M"])
    {
        NSString *strPercentMargin = [NSString stringWithFormat:@"%@",self.priceTxt.text];
        [self calMarginVal:[strPercentMargin trimSpaces]];
    }
    else
    {
        NSString *strPercentMargin = [NSString stringWithFormat:@"%@",self.priceTxt.text];

          self.resultvalue.text = [NSString stringWithFormat:@"$ %.2f",[strPercentMargin floatValue]];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Database Call

-(void)callUpdateSysDisct:(NSString *)priceVal
{
    
}

@end
