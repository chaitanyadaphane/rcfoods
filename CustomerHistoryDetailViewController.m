//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "CustomerHistoryDetailViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"

#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

@interface CustomerHistoryDetailViewController ()

@end

@implementation CustomerHistoryDetailViewController
@synthesize dictCustomerHistoryDetails,strTranCode,isFromCustomerHistory,tblDetails,lblTansactionNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Path to the plist (in the application bundle)
    NSString *path;
    
    // Build the array from the plist
    if (isFromCustomerHistory) {
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerHistoryDetails" Type:@"plist"];
    }
    else{
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionHistoryPaymentDetails" Type:@"plist"];
    }
    
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    lblTansactionNumber.text = strTranCode;

}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblDetails = nil;
    self.lblTansactionNumber = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Text Field Delgates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  CGRect viewFrame = self.view.frame;
    // viewFrame.origin.y = upframeByYPosition;
    // [AppDelegate pullUpPushDownViews:self.view ByRect:viewFrame];
    
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrHeaderLabels count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier = CELL_IDENTIFIER5;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:4];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text = [[arrHeaderLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    if (isFromCustomerHistory) {
        switch ([indexPath row]) {
            case 0:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Type"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerHistoryDetails objectForKey:@"Type"]];
                }
            }
                break;
            case 1:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Reference"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Reference"] trimSpaces]];
                }
            }
                break;
                
            case 2:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"SalesPerson"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerHistoryDetails objectForKey:@"SalesPerson"]];
                }
            }
                break;
                
            case 3:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Date"] && ![[dictCustomerHistoryDetails objectForKey:@"Date"] isEqualToString:STANDARD_APP_DATE] && ![[dictCustomerHistoryDetails objectForKey:@"Date"] isEqualToString:STANDARD_SERVER_DATE] ) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictCustomerHistoryDetails objectForKey:@"Date"]]];
                }
                
                
            }
                break;
            case 4:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Amount"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictCustomerHistoryDetails objectForKey:@"Amount"]floatValue]];
                }
                
            }
                break;

                
            default:
                break;
        }

    }
    else{
        switch ([indexPath row]) {
            case 0:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Type"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerHistoryDetails objectForKey:@"Type"]];
                }
            }
                break;
                
            case 1:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"DatePaid"] && ![[dictCustomerHistoryDetails objectForKey:@"DatePaid"] isEqualToString:STANDARD_APP_DATE] && ![[dictCustomerHistoryDetails objectForKey:@"DatePaid"] isEqualToString:STANDARD_SERVER_DATE]) {
                    NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictCustomerHistoryDetails objectForKey:@"DatePaid"]];
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",strDate];
                }
            }
                break;
                
            case 2:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Reference"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Reference"] trimSpaces]];
                }

                
            }
                break;
            case 3:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Payment"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictCustomerHistoryDetails objectForKey:@"Payment"] floatValue]];
                }
                
            }
                break;
                
            case 4:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Invoice"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Invoice"] trimSpaces]];
                }
            
            }
                break;
                
            case 5:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"Total"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictCustomerHistoryDetails objectForKey:@"Total"] floatValue]];
                }
            }
                break;
                
            default:
                break;
         
        }
    }
    
    return cell;
    
}


- (IBAction)saveProductDetails:(id)sender{
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

@end
