//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "CustomerTransactionDetailViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"

@interface CustomerTransactionDetailViewController (){
    UIButton *listPriceBtn;
}

@end

@implementation CustomerTransactionDetailViewController
@synthesize dictTransactionDetails,strTranCode,isFromTransactionHistory,lblTansactionNumber,tblDetails,isFromCustomerSpecials,lblProductName,strProductName,lblTransactionLabel,strCost,strPrice,strMarkup,strDebtor,operationQueue,strStatus,strType,isListPriceVisible;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Path to the plist (in the application bundle)
    NSString *path;
    
    if ([dictTransactionDetails objectForKey:@"Status"]) {
        self.strStatus = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Status"]];
    }
    
    // Build the array from the plist
    if (isFromTransactionHistory) {
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionHistoryDetails" Type:@"plist"];
        lblTansactionNumber.text = strTranCode;
    }
    else if (isFromCustomerSpecials){
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerSpecialProductDetails" Type:@"plist"];
        
        lblProductName.hidden = NO;
        lblProductName.text = strProductName;
        lblTransactionLabel.hidden = YES;
        
        NSLog(@"dictTransactionDetails %@",dictTransactionDetails);
        
        if ([[dictTransactionDetails objectForKey:@"Type"] isEqualToString:@"A"]) {
            self.strType = @"Special price";
        }
        else if ([[dictTransactionDetails objectForKey:@"Type"] isEqualToString:@"N"]){
            self.strType = @"List price";
        }
        else{
            self.strType = @"Contract price";
        }
        
        if ([dictTransactionDetails objectForKey:@"Discount"]) {
            self.strPrice = [dictTransactionDetails objectForKey:@"Discount"];
            self.strCost = [dictTransactionDetails objectForKey:@"Cost"];
            self.strMarkup = [NSString stringWithFormat:@"%f",[self calcMarkup]];
        }
        else{
            operationQueue = [NSOperationQueue new];
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [operationQueue addOperationWithBlock:^{
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetDiscountFromDB:db];
                }];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    spinner.mode = MBProgressHUDModeIndeterminate;
                    [spinner removeFromSuperview];
                    self.strCost = [dictTransactionDetails objectForKey:@"Cost"];
                    self.strMarkup = [NSString stringWithFormat:@"%f",[self calcMarkup]];
                    [tblDetails reloadData];
                }];
                
            }];
            
        }
        
    }
    else{
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionDetails" Type:@"plist"];
        lblTansactionNumber.text = strTranCode;
    }
    
    arrHeaderLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    //Initially hide list price
    isListPriceVisible = NO;
    listPriceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [listPriceBtn setFrame:CGRectMake(435.0, 10.0, 30.0, 30.0)];
    [listPriceBtn setBackgroundColor:[UIColor clearColor]];
    [listPriceBtn addTarget:self action:@selector(showPriceList:) forControlEvents:UIControlEventTouchDown];

    //NSLog(@"dictTransactionDetails %@",dictTransactionDetails);
    //NSLog(@" afaf : %@",dictTransactionDetails);
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.lblTansactionNumber = nil;
    self.tblDetails = nil;
    self.lblTansactionNumber = nil;
    self.lblProductName = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(float)calcMarkup{
    return ((([strPrice floatValue]-[strCost floatValue])/[strCost floatValue]))*100;
}

#pragma mark - Text Field Delgates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  CGRect viewFrame = self.view.frame;
    // viewFrame.origin.y = upframeByYPosition;
    // [AppDelegate pullUpPushDownViews:self.view ByRect:viewFrame];
    
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrHeaderLabels count];

}

-(void)showPriceList:(id)sender{
    //    isListPriceVisible = (isListPriceVisible)? NO: YES;
    isListPriceVisible = !isListPriceVisible;
    
    if(isListPriceVisible){
        [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
//        [arrHeaderLabels addObject:@"Cost"];
//        [arrHeaderLabels addObject:@"Markup"];
      NSString * path = [AppDelegate getFileFromLocalDirectory:@"CustomerSpecialProductDetailsPlus" Type:@"plist"];
        arrHeaderLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];

    }
    else{
        [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        
        NSString * path = [AppDelegate getFileFromLocalDirectory:@"CustomerSpecialProductDetails" Type:@"plist"];
        arrHeaderLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
//        [arrHeaderLabels removeObject:@"Cost"];
//        [arrHeaderLabels removeObject:@"Markup"];
    }
    
    [tblDetails reloadData];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier = CELL_IDENTIFIER5;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:4];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    @try {
        cell.lblTitle.text = [[arrHeaderLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];

        if (isFromTransactionHistory) {

            switch ([indexPath row]) {
                case 0:
                {
                    if ([dictTransactionDetails objectForKey:@"Description"]) {
                        if([[dictTransactionDetails objectForKey:@"Description"] isKindOfClass:[NSString class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictTransactionDetails objectForKey:@"Description"] trimSpaces]];
                        }
                        else if([[dictTransactionDetails objectForKey:@"Description"] objectForKey:@"text"])
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictTransactionDetails objectForKey:@"Description"] objectForKey:@"text"] trimSpaces]];
                    }
                }
                    break;
                    
                case 1:
                {
                    if ([dictTransactionDetails objectForKey:@"Quantity"]) {
                        if(([[dictTransactionDetails objectForKey:@"Quantity"]isKindOfClass:[NSNumber class]]))
                        {
                            id number = [dictTransactionDetails objectForKey:@"Quantity"];
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[number stringValue]];
                        }
                        
                        else if(([[dictTransactionDetails objectForKey:@"Quantity"]isKindOfClass:[NSString class]]))
                        {
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Quantity"]];
                        }
                        else if([[dictTransactionDetails objectForKey:@"Quantity"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictTransactionDetails objectForKey:@"Quantity"] objectForKey:@"text"]];
                        }
                        
                    }
                }
                    break;
                    
                case 2:
                {
                    if ([dictTransactionDetails objectForKey:@"Price"]) {
                        
                        if(([[dictTransactionDetails objectForKey:@"Price"]isKindOfClass:[NSNumber class]]))
                        {
                            id number = [dictTransactionDetails objectForKey:@"Price"];
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[number stringValue]];
                        }
                        
                        else if(([[dictTransactionDetails objectForKey:@"Price"]isKindOfClass:[NSString class]]))
                        {
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Price"]];
                        }
                        else if([[dictTransactionDetails objectForKey:@"Price"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictTransactionDetails objectForKey:@"Price"] objectForKey:@"text"]floatValue]]];
                        }
                    }
                }
                    break;
                case 3:
                {
                    if ([dictTransactionDetails objectForKey:@"Discount"]) {
                        
                        if(([[dictTransactionDetails objectForKey:@"Discount"]isKindOfClass:[NSNumber class]]))
                        {
                            id number = [dictTransactionDetails objectForKey:@"Discount"];
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[number stringValue]];
                        }
                        
                        else if(([[dictTransactionDetails objectForKey:@"Discount"]isKindOfClass:[NSString class]]))
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Discount"]];
                        }
                        else if([[dictTransactionDetails objectForKey:@"Discount"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictTransactionDetails objectForKey:@"Discount"] objectForKey:@"text"]floatValue]]];
                        }
                    }
                    
                }
                    break;
                    
                case 4:
                {
                    if ([dictTransactionDetails objectForKey:@"Extension"]) {
                        
                        if(([[dictTransactionDetails objectForKey:@"Extension"]isKindOfClass:[NSNumber class]]))
                        {
                            id number = [dictTransactionDetails objectForKey:@"Extension"];
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[number stringValue]];
                        }
                        
                        else if(([[dictTransactionDetails objectForKey:@"Extension"]isKindOfClass:[NSString class]]))
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Extension"]];
                        }
                        else if([[dictTransactionDetails objectForKey:@"Extension"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictTransactionDetails objectForKey:@"Extension"] objectForKey:@"text"]floatValue]]];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        else if (isFromCustomerSpecials){
            
            if (isListPriceVisible == NO) {
                switch ([indexPath row]) {
                    case 0:
                    {
                        if ([dictTransactionDetails objectForKey:@"StockCode"]) {
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"StockCode"]];
                            NSLog(@"%@",[NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"StockCode"]]);
                        }
                    }
                        break;
                        
                    case 1:
                    {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strPrice floatValue]];
                    }
                        break;
                        
                    case 2:
                    {
                        //                    if ([dictTransactionDetails objectForKey:@"Cost"]) {
                        //                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strCost floatValue]];
                        //                    }
                        cell.lblValue.text = strType;
                        
                        
                    }
                        break;
                    case 3:
                    {
                        if (isListPriceVisible) {
                            [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
                        }
                        else{
                            [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
                        }
                        listPriceBtn.tag = 300;
                        listPriceBtn.hidden = NO;
                        [cell.contentView addSubview:listPriceBtn];
                        //                        cell.lblValue.text = @"List Price";
                        
                        //                        if ([dictTransactionDetails objectForKey:@"Cost"]) {
                        //                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strCost floatValue]];
                        //                        }
                        //                    cell.lblValue.text = [NSString stringWithFormat:@"%.2f%%",[strMarkup floatValue]];
                        //
                    }
                        break;
                        //                    case 4:
                        //                    {
                        //                        cell.lblValue.text = [NSString stringWithFormat:@"%.2f%%",[strMarkup floatValue]];
                        //
                        //                        //                    cell.lblValue.text = strType;
                        //
                        //
                        //                    }
                        break;
                        
                    default:
                        break;
                }
                
            }
            else if (isListPriceVisible == YES){
                switch ([indexPath row]) {
                    case 0:
                    {
                        if ([dictTransactionDetails objectForKey:@"StockCode"]) {
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"StockCode"]];
                            NSLog(@"%@",[NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"StockCode"]]);
                        }
                    }
                        break;
                        
                    case 1:
                    {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strPrice floatValue]];
                    }
                        break;
                        
                    case 2:
                    {
                        //                    if ([dictTransactionDetails objectForKey:@"Cost"]) {
                        //                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strCost floatValue]];
                        //                    }
                        cell.lblValue.text = strType;
                        
                        
                    }
                        break;
                    case 3:
                    {
                        if (isListPriceVisible) {
                            [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
                        }
                        else{
                            [listPriceBtn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
                        }
                        listPriceBtn.tag = 300;
                        listPriceBtn.hidden = NO;
                        [cell.contentView addSubview:listPriceBtn];
                        
                    }
                        break;
                    case 4:
                    {
                        
                        
                        //                        if ([dictTransactionDetails objectForKey:@"Cost"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strCost floatValue]];
                        //                        }
                    }
                        break;
                        
                    case 5:
                    {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.2f%%",[strMarkup floatValue]];
                    }
                        break;
                    default:
                        break;
                }
                
            }
        }
        else{
            switch ([indexPath row]) {
                case 0:
                {
                    if ([dictTransactionDetails objectForKey:@"Days"] && ![[dictTransactionDetails objectForKey:@"Date"] isEqualToString:STANDARD_APP_DATE] && ![[dictTransactionDetails objectForKey:@"Date"] isEqualToString:STANDARD_SERVER_DATE]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@ days",[dictTransactionDetails objectForKey:@"Days"]];
                    }
                    else{
                        cell.lblValue.text = @"0";
                    }
                }
                    break;
                    
                case 1:
                {
                    if ([dictTransactionDetails objectForKey:@"CUST_ORDER"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"CUST_ORDER"]];
                    }
                }
                    break;
                    
                case 2:
                {
                    if ([dictTransactionDetails objectForKey:@"Type"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"Type"]];
                    }
                    
                    
                }
                    break;
                case 3:
                {
                    if ([strStatus isEqualToString:STATUS_DELETE]) {
                        cell.lblValue.text = @"CLSD";
                    }
                    else{
                        cell.lblValue.text = strStatus;
                    }
                    
                }
                    break;
                    
                case 4:
                {
                    if ([dictTransactionDetails objectForKey:@"Date"] && ![[dictTransactionDetails objectForKey:@"Date"] isEqualToString:STANDARD_APP_DATE] && ![[dictTransactionDetails objectForKey:@"Date"] isEqualToString:STANDARD_SERVER_DATE]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictTransactionDetails objectForKey:@"Date"]]];
                    }
                    
                }
                    break;
                    
                case 5:
                {
                    if ([dictTransactionDetails objectForKey:@"Nett"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictTransactionDetails objectForKey:@"Nett"] floatValue]];
                    }
                    
                }
                    break;
                    
                case 6:
                {
                    if ([dictTransactionDetails objectForKey:@"AMOUNT_PAID"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictTransactionDetails objectForKey:@"AMOUNT_PAID"] floatValue]];
                    }
                    
                }
                    break;
                    
                case 7:
                {
                    if ([dictTransactionDetails objectForKey:@"BALANCE"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[dictTransactionDetails objectForKey:@"BALANCE"] floatValue]];
                    }
                    
                }
                    break;
                    
                default:
                    break;
            }
        }

    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    return cell;
    
}

#pragma mark - Custom Methods

-(void)callGetDiscountFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT DISCOUNT1 FROM sysdisct WHERE STOCK_CODE = ?",[dictTransactionDetails objectForKey:@"STOCK_CODE"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            self.strPrice = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"DISCOUNT1"]];
        }
        else{
            [self callGetPriceFromDB:db];
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

-(void)callGetPriceFromDB:(FMDatabase *)db{
    
    //NSLog(@"strDebtor %@",strDebtor);
    @try {
        
        //NSLog(@"SELECT PRICE_CODE FROM armaster WHERE CODE = %@adsafasfasfa",strDebtor);
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE1"]];                    
                }
                    break;
                case 1:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE1"]];
                }
                    break;
                    
                case 2:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE2"]];
                }
                    break;
                    
                case 3:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE3"]];
                }
                    break;
                    
                case 4:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE4"]];
                }
                    break;
                    
                case 5:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE5"]];
                }
                    break;
                    
                    
                default:{
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictTransactionDetails objectForKey:@"PRICE1"]];
                }
                    break;
            }
                    
        }
        
        [rs1 close];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

- (IBAction)saveProductDetails:(id)sender{
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

@end
