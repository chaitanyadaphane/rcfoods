//
//  EmailAddressBookViewController.h
//  Blayney
//
//  Created by Pooja on 31/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissEmailPopOverDelegate

- (void)actiondismissEmailPopOverStockPickUp:(NSString*)strTextField;
//- (void)actiondismissPickUpValue:(NSDictionary*)dictPickUp strTextField:(NSString*)strTextField;

@end

@interface EmailAddressBookViewController : UIViewController{
    id<DismissEmailPopOverDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained)id<DismissEmailPopOverDelegate>delegate;

@property(nonatomic,retain)NSMutableArray *arrData;
@property(strong, nonatomic)NSMutableArray *emailSelectedArr;

@property(nonatomic,retain)NSString *strTextField;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)actionDone:(id)sender;
- (IBAction)actionCancel:(id)sender;

@end
