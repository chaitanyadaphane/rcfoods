//
//  ProfileOrderDetailViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "ProfileMaintenanceViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteProductListViewController.h"
#import "ViewQuoteProductDetailsViewController.h"
#import "QuoteHistoryCustomerListEditableMode.h"
#import "TopViewController.h"
#import "KGModal.h"
#import "MJDetailViewController.h"
#import "ProfileMaintenanceDetailVC.h"
#import "MJDebtorViewController.h"

#define TAG_50 50
#define TAG_100 100
#define TAG_200 200
#define TAG_300 300
#define TAG_400 400
#define TAG_500 500

#define TAG_999 999

//Tags for editable fields
#define TAG_500 500
#define TAG_600 600
#define TAG_700 700
#define TAG_800 800
#define TAG_900 900
#define TAG_1000 1000
#define TAG_1100 1100
#define TAG_1200 1200
#define TAG_1300 1300
#define TAG_1400 1400

#define PROFILE_PRODUCT_LIST_WS @"profordentry/prodetailslist.php?"
#define QUOTE_SAVE_HEADER_WS @"quote/adddebtordetails_quote.php?"
#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

#define ROWS_PER_PAGE_FOR_PROFILE 5
#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10


UILabel *lblBalance;
UIImageView *imgSpecialBg;
@interface ProfileMaintenanceViewController ()
{
    BOOL _loadingInProgress;
    int alertFreightValue;
    float freightValue;
    
    BOOL _longPress;
    NSMutableArray *arrProductPurchaseHistory;
    UITableView *tblProductHistory;
    NSString *strStockCode;
    NSString *strCopyDelDate;
    
    NSMutableDictionary *dictCustomerDetails;
    NSMutableArray *arrCustomerTransactions;
    
    MJDebtorViewController *detailViewController;
    MJDetailViewController *detailViewController1;
    
    NSString *debtorWarehouse;
    NSString *isModified;
    
    NSMutableDictionary *deleteProductDictionary;
    NSMutableDictionary *addProductDictionary;
    
}
@property (nonatomic, strong) JFDepthView* depthView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentSearch;
- (IBAction)filterSearchResult:(id)sender;

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;
- (IBAction)btnShowSpecials:(id)sender;
@end

@implementation ProfileMaintenanceViewController

@synthesize arrHeaderLabels,arrProducts,arrSearchProducts,dictHeaderDetails,strQuoteNum,popoverController,strQuoteNumRecived,EditableMode,strOrderNum,strCashPickUp,strChequeAvailable,strDeliveryRun,strDebtor,lblTotalCost,isSearch,strOfflineOrderNum;

@synthesize isStockPickUpOn;

@synthesize strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,isStorePickUpOn,strDelInst1,strDelInst2,strCustOrder,isCashPickUpOn,isRecordsExist,strPrevDebtor;

@synthesize btnDelete,lblTotalLines;

@synthesize strPriceChanged;

@synthesize  isProfileOrder;
@synthesize dictStockPickup;

@synthesize btnLoadProfile,btnAddProducts,btnFinalise,btnSaveDetails,vwSegmentedControl,vwContentSuperview,vwDetails,vwHeader,vwNoProducts,imgArrowDownwards,imgArrowRightWards,keyboardToolBar,btnAddMenu,cntProfileOrders;

@synthesize strPhone,strEmail,strFax,strBalance,strAddress1,strAddress2,strAddress3,strSubUrb,strAllowPartial,strBranch,strCarrier,strChargeRate,strChargetype,strCountry,strDirect,strDropSequence,strExchangeCode,strExportDebtor,strHeld,strName,strNumboxes,strOrderDate,strPeriodRaised,strPostCode,strPriceCode,strSalesBranch,strSalesman,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strTradingTerms,strYearRaised,strStockPickup,strTotalCartonQty,strExchangeRate,strEditStatus,strActive,strStatus,strTotalWeight,strTotalVolume,strTotalTax,strTotalCost,strTotalLines,strRefreshPrice,strConvFactor,strBOQty,strOperator,strDecimalPlaces,strUploadDate,strSuccessMessage,strOrigCost,strDateRaised,strCommentDateCreated,strCommentFollowDate,strCommenttxt,strDelDate,strWarehouse,strContact,strSalesType,strCreditLimit,isCommentAddedOnLastLine,isFrightAddedOnLastLine;
@synthesize strPickupForm;
@synthesize arrStockPickupImg;


#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(!imgSpecialBg)
    {
        imgSpecialBg = [[UIImageView alloc]init];
    }
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [_tblDetails addGestureRecognizer:longPressRecognizer];
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    [_SegmentSearch setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];
    
    self.isCommentAddedOnLastLine = YES; // 14thNov
    self.isFrightAddedOnLastLine=YES;
    
    strQuoteNum = [[NSString alloc] init];
    operationQueue = [NSOperationQueue  new];
    
    strPrevDebtor = nil;
    recordNumber = 0;
    
    CATransform3D transform = CATransform3DIdentity;
    
    //Add layer to Pointing image
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"pointing_down" ofType:@"png"];
    UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
    layerPointingImage = [CALayer layer];
    layerPointingImage.contents = (id)downImage.CGImage;
    layerPointingImage.bounds = CGRectMake(0, 0, imgArrowDownwards.frame.size.width, imgArrowDownwards.frame.size.height);
    layerPointingImage.position = CGPointMake(10,30);
    layerPointingImage.transform = CATransform3DTranslate(transform, 0.0, 6,0.0);
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
    chkFinalize = NO;
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"ProfileOrderEntryHeader" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    arrProducts = [[NSMutableArray alloc] init];
    arrSearchProducts = [[NSMutableArray alloc] init];
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    
    backgroundQueueForNewQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForNewQuote", NULL);
    
    self.strDateRaised = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
    NSLog(@"%@",self.strDateRaised);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.strOperator = [prefs stringForKey:@"userName"];
    self.strSalesType = [prefs stringForKey:@"salesType"];
    self.strWarehouse = [prefs objectForKey:@"warehouse"];
    if (self.strWarehouse.length < 2) {
        self.strWarehouse = [NSString stringWithFormat:@"0%@",self.strWarehouse];
    }
    
    NSLog(@"self.strWarehouse  %@",self.strWarehouse);
    self.strOperatorName = [prefs objectForKey:@"members"];
    
    
    if ( [self.strOperatorName caseInsensitiveCompare:ADMIN] == NSOrderedSame ||  [self.strOperatorName caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
    }
    else
    {
        [self getBranchListNameForUser];
        
    }
    
    
    //By default
    [self setDefaults];
    
    cntViewWillAppear = 0;
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.lblTotalLines = nil;
    self.lblTotalCost = nil;
    self.btnDelete = nil;
    self.btnLoadProfile = nil;
    self.btnSaveDetails = nil;
    self.btnAddProducts = nil;
    self.btnFinalise = nil;
    self.vwSegmentedControl = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoProducts = nil;
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.imgArrowDownwards = nil;
    self.imgArrowRightWards = nil;
    self.keyboardToolBar = nil;
    self.btnAddMenu = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.strDateRaised = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
    self.strPriceChanged = @"N";
    //Counter is taken coz viewwillappear gets called twice.
    cntViewWillAppear++;
    
    if (cntViewWillAppear == 1) {
        
        //Notification to reload table when debtor is selected from Debtor list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHeader:) name:@"ReloadAddDebtorDetailsNotification" object:nil];
        
        //Notification to reload table when products added to details from product list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadAddProductDetailsNotification" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveOrderForProfile) name:@"SAVE" object:nil];
    }
    
    if (isProfileOrder) {
        NSLog(@"%@",strOfflineOrderNum);
        [self callOrderNumberFromDB];
    }
    
    // Store the view is profile order.
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    cntViewWillAppear = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Review Order

-(void)reviewOrderList{
    
    NSMutableArray *sortProfileArr = [NSMutableArray new];
    for (int i = 0; i < arrProducts.count; i++)
    {
        if (![[arrProducts objectAtIndex:i]objectForKey:@"FromSpecials" ]){
            [sortProfileArr addObject:[arrProducts objectAtIndex:i]];
        }
    }
    
    NSArray *myArray = [NSArray arrayWithArray:(NSArray*)sortProfileArr];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastSold" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedArray = [myArray sortedArrayUsingDescriptors:sortDescriptors];
    
    // 1.Empty SortedProfilearray and add profile Objects to the top
    if (sortProfileArr.count > 0) {
        [sortProfileArr removeAllObjects];
        sortProfileArr=[NSMutableArray arrayWithArray:sortedArray];
    }
    
    // 2. Add Special object to the bottom
    for (int i = 0; i < arrProducts.count; i++)
    {
        if ([[arrProducts objectAtIndex:i]objectForKey:@"FromSpecials" ]){
            id object = [arrProducts objectAtIndex:i];
            //        [arrProducts removeObjectAtIndex:i];
            [sortProfileArr addObject:object];
        }
    }
    
    // 3. All the orders woth quantity > 0 and commets and freight to the top
    for (int i = 0; i < sortProfileArr.count; i++)
    {
        if ([[sortProfileArr objectAtIndex:i]objectForKey:@"QuantityOrdered"])
        {
            // Check for Quantity Ordered
            if ([[[sortProfileArr objectAtIndex:i]objectForKey:@"QuantityOrdered"]integerValue] > 0) {
                id object = [sortProfileArr objectAtIndex:i];
                [sortProfileArr removeObjectAtIndex:i];
                [sortProfileArr insertObject:object atIndex:0];
            }
        }
        else if ([[sortProfileArr objectAtIndex:i]objectForKey:@"Item" ]){
            id object = [sortProfileArr objectAtIndex:i];
            [sortProfileArr removeObjectAtIndex:i];
            [sortProfileArr insertObject:object atIndex:0];
        }
        
    }
    
    // Merge all the formated objects to the arr Prod
    if (arrProducts.count > 0) {
        arrProducts = [sortProfileArr mutableCopy];
    }
    
    [_tblDetails setContentOffset:CGPointMake(0, 0)];
    [_tblDetails reloadData];
}

#pragma mark - Show Specials

- (IBAction)btnShowSpecials:(id)sender
{
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [db open];
    FMResultSet *results;
    @try {
        
        results = [db executeQuery:@"SELECT sam.SCM_RECNUM as ScmRecum, sam.DESCRIPTION as Description,sam.LOCATION as Location,sam.PRICE1 as Price1,sam.PRICE2 as Price2,sam.PRICE3 as Price3, sam.PRICE4 as Price4, sam.PRICE5 as Price5, sam.WAREHOUSE as Warehouse, sam.AVAILABLE as Available, sam.ON_HAND as OnHand, sam.PURCHASE_ORDER as PurchaseOrder, sam.SUBSTITUTE as Subsitute,sam.PICTURE_FIELD as Picture_field, sam.ALLOCATED as Allocated, sam.SALES_DISS as Dissection, sam.COST_DISS as Dissection_Cos, sam.WEIGHT as Weight, sam.VOLUME as Volume, sam.STANDARD_COST as Cost, sam.AVERAGE_COST as AverageCost, sam.LAST_SOLD as LastSold, sam.PROD_CLASS as ProdClass, sam.PROD_GROUP as ProdGroup, sys.KEY_FIELD as Customer, sys.STOCK_CODE as StockCode, sys.TYPE1 as Type1, sys.DISCOUNT1 as Discount,sys.CUST_CATEGORY as cust_category FROM samaster as sam JOIN sysdisct as sys ON sam.CODE = sys.STOCK_CODE WHERE sys.DATE_TO1 BETWEEN DATETIME('now') AND DATETIME('now','360 days') ORDER BY  sam.DESCRIPTION"];
        
        if (!results)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"*** %@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if (strDebtor.length ==  0) {
            strDebtor = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"];
        }
        
        NSMutableArray *arrMuteData = [NSMutableArray new];
        while ([results next])
        {
            @autoreleasepool {
                
                NSMutableDictionary *dictData = (NSMutableDictionary *)[results resultDictionary];
                
                [dictData setValue:@"0" forKey:@"q0"];
                [dictData setValue:@"0" forKey:@"q2"];
                [dictData setValue:@"0" forKey:@"q3"];
                [dictData setValue:@"0" forKey:@"q4"];
                [dictData setValue:@"Specials" forKey:@"FromSpecials"];
                
                NSString *customer =[NSString stringWithFormat:@"%@",[dictData objectForKey:@"Customer"] ] ;
                
                if ([[customer trimSpaces] isEqualToString:strDebtor]) {
                    NSLog(@"StockCode %@",[dictData objectForKey:@"StockCode"]);
                    [dictData setValue:@"ContractSpecial" forKey:@"ContractSpecial"];
                    NSLog(@"Dict Data - %@",dictData);
                    
                }
                [arrMuteData addObject:dictData];
            }
            
        }
        
        [results close];
        
        if([arrMuteData count] > 0)
        {
            for (int i=0; i<[arrMuteData count]; i++)
            {
                
                // check condition customer = empty AND cust_category= empty
                if([[[arrMuteData objectAtIndex:i] objectForKey:@"Customer"] isEqualToString:@""] && [[[arrMuteData objectAtIndex:i] objectForKey:@"cust_category"] isEqualToString:@""] )
                {
                    @autoreleasepool {
                        
                        NSString *strCheckStockCode = [[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"];
                        
                        if([arrProducts count] > 0)
                        {
                            BOOL isMatch = NO;
                            for (int j=0; j<[arrProducts count]; j++)
                            {
                                NSString *strCode = [[arrProducts objectAtIndex:j] objectForKey:@"StockCode"];
                                
                                if(![strCheckStockCode isEqualToString:strCode])
                                {
                                    isMatch = NO;
                                }
                                else
                                {
                                    isMatch = YES;
                                    break;
                                }
                            }
                            
                            if(!isMatch)//!isMatch
                            {
                                //--Add Special at top always
                                
                                // Avoid double up
                                if ([[arrProducts valueForKey:@"StockCode"] containsObject:[[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"]])
                                {
                                    //                                    NSLog(@"OBJECT EXISTS");
                                }
                                else{
                                    [arrProducts insertObject:[arrMuteData objectAtIndex:i] atIndex:0];
                                }
                            }
                        }
                        else
                        {
                            
                            // Avoid double up
                            if ([[arrProducts valueForKey:@"StockCode"] containsObject:[[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"]])
                            {
                                //                                NSLog(@"OBJECT EXISTS");
                            }
                            else{
                                [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                            }
                            
                        }
                        
                    }
                }
                else if ([[[arrMuteData objectAtIndex:i] objectForKey:@"Customer"] isEqualToString:strDebtor]) {
                    @autoreleasepool {
                        NSString *strCheckStockCode = [[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"];
                        
                        if([arrProducts count] > 0)
                        {
                            for (int j=0; j<[arrProducts count]; j++)
                            {
                                NSString *strCode = [[arrProducts objectAtIndex:j] objectForKey:@"StockCode"];
                                
                                if([strCheckStockCode isEqualToString:strCode]){
                                    [[arrProducts objectAtIndex:j] setObject:@"ContractSpecial" forKey:@"ContractSpecial"];
                                }
                            }
                            
                        }
                        else
                        {
                            if ([[arrProducts valueForKey:@"StockCode"] containsObject:[[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"]])
                            {
                                //                                NSLog(@"OBJECT EXISTS");
                            }
                            else{
                                [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                            }
                        }
                        
                    }
                }
                else{
                    
                    if ([[arrProducts valueForKey:@"StockCode"] containsObject:[[arrMuteData objectAtIndex:i]objectForKey:@"StockCode"]])
                    {
                        //                        NSLog(@"OBJECT EXISTS");
                    }
                    else{
                        [arrProducts insertObject:[arrMuteData objectAtIndex:i] atIndex:0];
                    }
                }
            }
        }
        
        int lastIndex = [arrProducts count];
        if (lastIndex)
        {
            [self showOrEnableButtons];
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwDetails];
            [_tblDetails reloadData];
            
        }
        else
        {
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwDetails];
            [_tblDetails reloadData];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Specials Available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    @catch (NSException *e) {
        [results close];
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }];
    }
    [db close];
}
- (IBAction)btnShowSpecialsNEW:(id)sender
{
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [db open];
    FMResultSet *results;
    @try {
        
        if (strDebtor.length ==  0) {
            strDebtor = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"];
        }
        
        @autoreleasepool {
            
            for (int i=0; i<[arrProducts count]; i++)
            {
                
                NSString *stockCode = [[arrProducts objectAtIndex:i]objectForKey:@"StockCode"];
                FMResultSet *rs = [db executeQuery:@"SELECT STOCK_CODE FROM sysdisct WHERE STOCK_CODE = ?",stockCode];
                
                if (!rs)
                {
                    [rs close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs next]) {
                    NSString *strStockCodeval = [[rs resultDictionary] objectForKey:@"STOCK_CODE"];
                    if (strStockCodeval.length > 0) {
                        [[arrProducts objectAtIndex:i] setValue:@"Specials" forKey:@"FromSpecials"];
                    }
                }
                [rs close];
            }
        }
        
        int lastIndex = [arrProducts count];
        if (lastIndex)
        {
            [self showOrEnableButtons];
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwDetails];
            [_tblDetails reloadData];
            
        }
        else
        {
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwDetails];
            [_tblDetails reloadData];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Specials Available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    @catch (NSException *e) {
        [results close];
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }];
    }
    [db close];
}

#pragma mark - Long Press
-(void)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:_tblDetails];
    
    NSIndexPath *indexPath = [_tblDetails indexPathForRowAtPoint:p];
    
    //NSLog(@"%d",indexPath.row);
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    
    if(isSearch)
    {
        strStockCode = [[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    else
    {
        strStockCode = [[arrProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSURL *url = [NSURL URLWithString:[AppDelegate getServiceURL:@"webservices_rcf/profordentry/arhisdetlist.php"]];
            
            
            ASIFormDataRequest *request1 = [ASIFormDataRequest requestWithURL:url];
            __weak ASIFormDataRequest *request = request1;
            [request addPostValue:strDebtor forKey:@"customer"];
            [request addPostValue:strStockCode forKey:@"stock_code"];
            
            [request startAsynchronous];
            [request setCompletionBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                NSString *responseString = [request responseString];
                
                NSDictionary *dictResponse = [XMLReader dictionaryForXMLString:responseString error:nil];
                
                
                NSString *strFalg = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Flag"] objectForKey:@"text"];
                
                if([strFalg isEqualToString:@"True"])
                {
                    if(!arrProductPurchaseHistory)
                    {
                        arrProductPurchaseHistory = [NSMutableArray new];
                    }
                    else
                    {
                        [arrProductPurchaseHistory removeAllObjects];
                    }
                    
                    NSMutableArray *arrTempArr = [NSMutableArray new];
                    
                    if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]])
                    {
                        [arrTempArr addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"]];
                    }
                    else
                    {
                        arrTempArr = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"];
                    }
                    
                    if([arrTempArr count] > 0)
                    {
                        for (int i=0;i<[arrTempArr count];i++)
                        {
                            NSLog(@"%@",[arrTempArr objectAtIndex:i]);
                            NSMutableDictionary *arrTempDict = [NSMutableDictionary new];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Price"] objectForKey:@"text"] forKey:@"Price"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"DateRaised"] objectForKey:@"text"] forKey:@"DateRaised"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"InvNo"] objectForKey:@"text"] forKey:@"InvNo"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Quantity"] objectForKey:@"text"] forKey:@"Quantity"];
                            
                            [arrProductPurchaseHistory addObject:arrTempDict];
                        }
                    }
                    
                    if([arrProductPurchaseHistory count] > 0)
                    {
                        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                        
                        tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                        [tblProductHistory setDataSource:self];
                        [tblProductHistory setDelegate:self];
                        tblProductHistory.tag = TAG_999;
                        
                        [contentView addSubview:tblProductHistory];
                        
                        [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                        [tblProductHistory reloadData];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                _longPress = NO;
            }];
            [request setFailedBlock:^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                //NSError *error = [request error];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Error loading the details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                _longPress = NO;
            }];
            
        }
    }
    else
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            //Get the data from DB
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                
                FMResultSet *rs1 = [db executeQuery:@"SELECT TRIM(DEBTOR), TRIM(STOCK_CODE), TRIM(QUANTITY) as QUANTITY, TRIM(PRICE) as PRICE, TRIM(DATE_RAISED) as DATE_RAISED, TRIM(TRAN_NO) as TRAN_NO from arhisdet WHERE DEBTOR = ? AND STOCK_CODE = ? ORDER BY DATE_RAISED desc",strDebtor,strStockCode];
                
                if (!rs1)
                {
                    [rs1 close];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to fetch the records." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                if(!arrProductPurchaseHistory)
                {
                    arrProductPurchaseHistory = [NSMutableArray new];
                }
                else
                {
                    [arrProductPurchaseHistory removeAllObjects];
                }
                
                while ([rs1 next])
                {
                    NSString *strDate = [rs1 stringForColumn:@"DATE_RAISED"];
                    
                    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                    [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *theDate = [inputFormatter dateFromString:strDate];
                    
                    NSDateFormatter *inputFormatterNew = [[NSDateFormatter alloc] init];
                    [inputFormatterNew setDateFormat:@"dd/MM/yy"];
                    
                    NSString *strNewDate = [inputFormatterNew stringFromDate:theDate];
                    
                    NSMutableDictionary *tempDict = [NSMutableDictionary new];
                    [tempDict setValue:[rs1 stringForColumn:@"PRICE"] forKey:@"Price"];
                    [tempDict setValue:strNewDate forKey:@"DateRaised"];
                    [tempDict setValue:[rs1 stringForColumn:@"TRAN_NO"] forKey:@"InvNo"];
                    [tempDict setValue:[rs1 stringForColumn:@"QUANTITY"] forKey:@"Quantity"];
                    
                    [arrProductPurchaseHistory addObject:tempDict];
                }
                
                if([arrProductPurchaseHistory count] > 0)
                {
                    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                    
                    tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                    [tblProductHistory setDataSource:self];
                    [tblProductHistory setDelegate:self];
                    tblProductHistory.tag = TAG_999;
                    
                    [contentView addSubview:tblProductHistory];
                    
                    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                    [tblProductHistory reloadData];
                    
                    _longPress = NO;
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                    
                    _longPress = NO;
                }
                
                [rs1 close];
            }];
        }
    }
}

- (void)changeCloseButtonType:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    KGModal *modal = [KGModal sharedInstance];
    KGModalCloseButtonType type = modal.closeButtonType;
    
    if(type == KGModalCloseButtonTypeLeft){
        modal.closeButtonType = KGModalCloseButtonTypeRight;
        [button setTitle:@"Close Button Right" forState:UIControlStateNormal];
    }else if(type == KGModalCloseButtonTypeRight){
        modal.closeButtonType = KGModalCloseButtonTypeNone;
        [button setTitle:@"Close Button None" forState:UIControlStateNormal];
    }else{
        modal.closeButtonType = KGModalCloseButtonTypeLeft;
        [button setTitle:@"Close Button Left" forState:UIControlStateNormal];
    }
}

#pragma mark -  Segmented Control Actions

-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    btnDelete.hidden = YES;
    [operationQueue cancelAllOperations];
    
    switch (sender.SelectedIndex)
    {
        case 0:
        {
            btnDelete.hidden = YES;
            [vwContentSuperview addSubview:vwHeader];
        }
            break;
            
        case 1:
        {
            
            if (isDeliveryRunSlected)
            {
                if (![strPrevDebtor isEqualToString:strDebtor]) {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [operationQueue addOperationWithBlock:^{
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        [databaseQueue   inDatabase:^(FMDatabase *db) {
                            [self callViewQuoteDetailsDetailsFromDB:db];
                        }];
                        
                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [self prepareView];
                            //                            [self showSpecials];
                            [_tblDetails reloadData];
                        }];
                    }];
                    
                }
                else{
                    [self prepareView];
                }
            }
            else{
                
                [vwContentSuperview addSubview:vwHeader];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select delivery run!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                alert.tag = TAG_1300;
                [alert show];
            }
        }
            
            break;
            
        default:
            break;
    }
}

#pragma mark Table view data source and delegate

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 0) {
                return nil;
            }
            else if (indexPath.section == 2 && [indexPath row]!=9) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strName;
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = strContact;
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = strEmail;
                    }
                        break;
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
            }
            else if (indexPath.section == 3 && [indexPath row]!=8) {
                return nil;
            }
            else if (indexPath.section == 4) {
                return nil;
            }
            //            else if (indexPath.section == 5) {
            //                return nil;
            //            }
            else if (indexPath.section == 5 ) {
                NSString *stringToCheck = nil;
                if ([strCommenttxt isKindOfClass:[NSNull class]] || [strCommenttxt isEqualToString:@""] || strCommenttxt == nil) {
                    return indexPath;
                    
                }
                else{
                    switch ([indexPath row]) {
                        case 0:
                        {
                            stringToCheck = strCommenttxt;
                        }
                            break;
                    }
                    
                    //We only don't want to allow selection on any cells which cannot be expanded
                    //                COMMENT_LABEL_MIN_HEIGHT
                    if([self getLabelHeightForString:stringToCheck] > 2)
                    {
                        return indexPath;
                    }
                    else {
                        return nil;
                    }
                    
                }
            }
            else{
                return indexPath;
            }
            
            
            
        }break;
            
        case TAG_100:return indexPath;break;
            
        default:return nil;break;
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strName;
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = strContact;
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = strEmail;
                    }
                        break;
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                }
            }
            else{
                return 60;break;
            }
        }
            break;
            
        case TAG_100:return 50;break;
            
        case TAG_999:return 50;break;
            
        default:return 0;break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == TAG_999)
    {
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 560, 80)];
        [tempView setBackgroundColor:[UIColor lightGrayColor]];
        
        CGSize maximumSize = CGSizeMake(300, 9999);
        NSString *myString = [NSString stringWithFormat:@"Customer: %@",strDebtor];
        UIFont *myFont = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize = [myString sizeWithFont:myFont
                                   constrainedToSize:maximumSize
                                       lineBreakMode:NSLineBreakByWordWrapping];
        
        CGSize maximumSize2 = CGSizeMake(300, 9999);
        NSString *myString2 = [NSString stringWithFormat:@"Stock Code: %@",strStockCode];
        UIFont *myFont2 = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize2 = [myString2 sizeWithFont:myFont2
                                     constrainedToSize:maximumSize2
                                         lineBreakMode:NSLineBreakByWordWrapping];
        
        UILabel *lblDebtor = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, myStringSize.width, myStringSize.height)];
        [lblDebtor setBackgroundColor:[UIColor clearColor]];
        [lblDebtor setText:myString];
        [lblDebtor setTextAlignment:NSTextAlignmentCenter];
        [lblDebtor setTextColor:[UIColor blackColor]];
        [lblDebtor setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        int x = 552 - myStringSize2.width;
        UILabel *lblStockCode = [[UILabel alloc] initWithFrame:CGRectMake(x, 10, myStringSize2.width, myStringSize2.height)];
        [lblStockCode setBackgroundColor:[UIColor clearColor]];
        [lblStockCode setText:myString2];
        [lblStockCode setTextAlignment:NSTextAlignmentCenter];
        [lblStockCode setTextColor:[UIColor blackColor]];
        [lblStockCode setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(8, 45, 60, 30)];
        [lblDate setBackgroundColor:[UIColor clearColor]];
        [lblDate setText:@"Date"];
        [lblDate setTextAlignment:NSTextAlignmentCenter];
        [lblDate setTextColor:[UIColor blackColor]];
        [lblDate setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblQuanty = [[UILabel alloc] initWithFrame:CGRectMake(120, 45, 90, 30)];
        [lblQuanty setBackgroundColor:[UIColor clearColor]];
        [lblQuanty setText:@"Quantity"];
        [lblQuanty setTextAlignment:NSTextAlignmentCenter];
        [lblQuanty setTextColor:[UIColor blackColor]];
        [lblQuanty setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(240,45, 90, 30)];
        [lblPrice setBackgroundColor:[UIColor clearColor]];
        [lblPrice setText:@"Price"];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];
        [lblPrice setTextColor:[UIColor blackColor]];
        [lblPrice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblInvoice = [[UILabel alloc] initWithFrame:CGRectMake(345,45, 200, 30)];
        [lblInvoice setBackgroundColor:[UIColor clearColor]];
        [lblInvoice setText:@"Invoice/CN No"];
        [lblInvoice setTextAlignment:NSTextAlignmentCenter];
        [lblInvoice setTextColor:[UIColor blackColor]];
        [lblInvoice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        [tempView addSubview:lblDebtor];
        [tempView addSubview:lblStockCode];
        [tempView addSubview:lblDate];
        [tempView addSubview:lblQuanty];
        [tempView addSubview:lblPrice];
        [tempView addSubview:lblInvoice];
        
        lblDate = nil;
        lblQuanty = nil;
        lblPrice = nil;
        lblInvoice = nil;
        
        return tempView;
    }
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    switch (tableView.tag)
    {
        case TAG_50:
            
            if([[AppDelegate getAppDelegateObj] CheckIfDemoApp])
            {
                return [arrHeaderLabels count] - 2;
            }
            else
                return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        case TAG_999:return 1;break;
            
        default:return 0;break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == TAG_999)
    {
        return 80.0;
    }
    else if(tableView.tag == TAG_100)
    {
        return 0.0;
    }
    else
    {
        return 40.0;
    }
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == TAG_50)
    {
        return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];
    }
    else if (tableView.tag == TAG_999)
    {
        return [arrProductPurchaseHistory count];
    }
    else
    {
        if (isSearch)
        {
            return [arrSearchProducts count];
        }
        else
        {
            return [arrProducts count];
        }
    }
    
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    CustomCellGeneral *cell = nil;
    
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    static NSString *CellIdentifier12 = CELL_IDENTIFIER12;
    static NSString *CellIdentifier14 = CELL_IDENTIFIER14;
    
    switch (tableView.tag) {
        case TAG_50:{
            
            NSArray *nib;
            
            if ([indexPath section] == 0) {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                switch ([indexPath row]) {
                    case 0:{
                        
                        if (cell == nil) {
                            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:4];
                        }
                        cell.lblValue.text = @"--";
                    }
                        break;
                }
            }
            else if ([indexPath section] == 1) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                
                switch ([indexPath row]) {
                    case 0:{
                        
                        //Debtor
                        if (cell == nil) {
                            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:3];
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            cell.txtValue.userInteractionEnabled = FALSE;
                            cell.txtValue.placeholder = @"Please Select";
                            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                            
                        }
                        
                        cell.txtValue.text = nil;
                        if (strDebtor) {
                            cell.txtValue.text = strDebtor;
                            segControl.enabled = YES;
                        }
                        
                    }
                        
                        break;
                }
            }
            else if ([indexPath section] == 2) {
                
                if ([indexPath row] == 9) {
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                    
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:3];
                        //Delivery Run
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.txtValue.userInteractionEnabled = FALSE;
                        cell.txtValue.placeholder = @"Please Select";
                        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                        
                    }
                    
                }
                else{
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                    
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:4];
                        lblBalance = [[UILabel alloc]initWithFrame:cell.lblValue.frame];
                    }
                    
                }
                
                
                cell.lblValue.text = @"";
                
                switch ([indexPath row]) {
                        
                    case 0:
                    {
                        
                        if (strName) {
                            cell.lblValue.text = strName;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        if (strPhone) {
                            cell.lblValue.text = strPhone;
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        if (strContact) {
                            cell.lblValue.text = strContact;
                        }
                        
                    }
                        break;
                    case 3:
                    {
                        
                        if (strEmail) {
                            cell.lblValue.text = strEmail;
                        }
                        
                    }
                        break;
                    case 4:
                    {
                        
                        if (strFax) {
                            cell.lblValue.text = strFax;
                        }
                        
                    }
                        break;
                    case 5:
                    {
                        lblBalance.text = @"";
                        if (strBalance) {
                            
                            NSString *str=  [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[strBalance floatValue]];
                            lblBalance.text = [NSString stringWithFormat:@"%@",str];
                            lblBalance.font = cell.lblValue.font;
                            lblBalance.backgroundColor = [UIColor grayColor];
                            lblBalance.textAlignment = NSTextAlignmentRight;
                            lblBalance.textColor = [UIColor whiteColor];
                            [cell addSubview:lblBalance];
                            
                            return cell;
                            
                        }
                    }
                        break;
                        
                    case 6:
                    {
                        if (strCreditLimit) {
                            cell.lblValue.text = @"$";
                            
                            cell.lblValue.text = [cell.lblValue.text stringByAppendingString:strCreditLimit];
                            
                        }
                        
                    }
                        break;
                        
                    case 7:
                    {
                        if (strTradingTerms)
                        {
                            if([[strTradingTerms trimSpaces] isEqualToString:@"14"])
                            {
                                cell.lblValue.text = @"14 days from invoice";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"C1"])
                            {
                                cell.lblValue.text = @"Cash On Delivery";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"I7"])
                            {
                                cell.lblValue.text = @"7 days from invoice";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"S7"])
                            {
                                cell.lblValue.text = @"7 days from statement";
                            }
                            else
                            {
                                cell.lblValue.text = @"30 days from invoice";
                            }
                        }
                        
                    }
                        break;
                        
                    case 8:
                    {
                        
                        if (strSalesman) {
                            cell.lblValue.text = strSalesman;
                        }
                    }
                        break;
                        
                        
                    case 9:
                    {
                        if (strDeliveryRun) {
                            cell.txtValue.text = strDeliveryRun;
                        }
                        
                    }
                        break;
                        
                        
                    default: {
                    }
                        break;
                }
                
                if (selectedSectionIndex == 2 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
                
                
            }
            else if ([indexPath section] == 3) {
                
                if ([indexPath row] == 7) {
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                    
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:4];
                        
                    }
                    
                }
                else if([indexPath row] == 8){
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                    //Debtor
                    if (cell == nil) {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:3];
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.txtValue.userInteractionEnabled = FALSE;
                        cell.txtValue.placeholder = @"Please Select";
                        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                        
                    }
                    
                }
                else{
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                    
                    if (cell == nil)
                    {
                        nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:9];
                    }
                    
                    cell.txtGlowingValue.keyboardType = UIKeyboardTypeNamePhonePad;
                    cell.txtGlowingValue.delegate = self;
                    
                }
                
                cell.lblValue.text = nil;
                cell.txtGlowingValue.text = nil;
                cell.txtValue.text = nil;
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_500;
                        if (strDelName) {
                            cell.txtGlowingValue.text = strDelName;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        
                        cell.txtGlowingValue.tag = TAG_600;
                        if (strDelAddress1) {
                            cell.txtGlowingValue.text = strDelAddress1;
                        }
                        
                        
                    }
                        break;
                    case 2:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_700;
                        if (strDelAddress2) {
                            cell.txtGlowingValue.text = strDelAddress2;
                        }
                        
                    }
                        break;
                    case 3:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_800;
                        if (strDelAddress3) {
                            cell.txtGlowingValue.text = strDelAddress3;
                        }
                        
                        
                    }
                        break;
                    case 4:
                    {
                        
                        
                        cell.txtGlowingValue.tag = TAG_900;
                        if (strDelSubUrb) {
                            cell.txtGlowingValue.text = strDelSubUrb;
                        }
                        
                    }
                        break;
                    case 5:
                    {
                        cell.txtGlowingValue.tag = TAG_1000;
                        if (strDelPostCode) {
                            cell.txtGlowingValue.text = strDelPostCode;
                        }
                        
                    }
                        break;
                    case 6:
                    {
                        cell.txtGlowingValue.tag = TAG_1100;
                        if (strDelCountry) {
                            cell.txtGlowingValue.text = strDelCountry;
                        }
                        
                    }
                        break;
                        
                    case 7:
                    {
                        
                        if (strOrderDate && ![strOrderDate isEqualToString:STANDARD_APP_DATE] && ![strOrderDate isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strOrderDate]];
                        }
                        
                    }
                        break;
                    case 8:
                    {
                        NSLog(@"strDelDate %@",strDelDate);
                        if (strDelDate && ![strDelDate isEqualToString:STANDARD_APP_DATE] && ![strDelDate isEqualToString:STANDARD_SERVER_DATE]) {
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSDate *delDate = [formatter dateFromString:strDelDate];
                            if (delDate == nil) {
                                cell.txtValue.text = strDelDate;
                            }
                            else{
                                cell.txtValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strDelDate];
                                
                            }
                        }
                    }
                        
                        break;
                    case 9:
                    {
                        cell.txtGlowingValue.tag = TAG_1200;
                        
                        if (![strDelInst1 isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strDelInst1;
                        }
                    }
                        break;
                        
                    case 10:
                    {
                        cell.txtGlowingValue.tag = TAG_1300;
                        if (![strDelInst2 isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strDelInst2;
                        }
                        
                    }
                        
                        break;
                        
                    case 11:
                    {
                        cell.txtGlowingValue.tag = TAG_1400;
                        if (![strCustOrder isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strCustOrder;
                        }
                    }
                        
                        break;
                        
                        
                    default:{
                        
                    }
                        break;
                }
            }
            //Cash pick up
            else if ([indexPath section] == 4) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier12];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:11];
                    
                }
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        if([isCashPickUpOn isEqualToString:@"Y"]){
                            [cell.swtch setOn:YES];
                        }
                        else{
                            [cell.swtch setOn:NO];
                        }
                        
                        cell.swtch.tag = TAG_200;
                        [cell.swtch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventValueChanged];
                    }
                        break;
                    default:{
                    }
                        break;
                }
            }
            
            //Stock pickup
            //            else if ([indexPath section] == 5) {
            //
            //                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier12];
            //
            //                if (cell == nil) {
            //                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            //                    cell = [nib objectAtIndex:11];
            //                }
            //
            //
            //                switch ([indexPath row]) {
            //                    case 0:
            //                    {
            //
            //                        if([isStockPickUpOn isEqualToString:@"Y"]){
            //                            [cell.swtch setOn:YES];
            //                        }
            //                        else{
            //                            [cell.swtch setOn:NO];
            //                        }
            //                        cell.swtch.tag = TAG_500;
            //                        [cell.swtch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventValueChanged];
            //
            //                    }
            //                        break;
            //                    default:{
            //
            //                    }
            //
            //                        break;
            //                }
            //            }
            
            //Comments
            else if ([indexPath section] == 6) {
                
                // NSLog(@"In 6");
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblValue.text = nil;
                switch ([indexPath row]) {
                        
                    case 0:
                    {
                        if (strCommenttxt) {
                            cell.lblValue.text = strCommenttxt;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        if (strCommentDateCreated && ![strCommentDateCreated isEqualToString:STANDARD_APP_DATE]&& ![strCommentDateCreated isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentDateCreated];
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        
                        if (strCommentFollowDate && ![strCommentFollowDate isEqualToString:STANDARD_APP_DATE]&& ![strCommentFollowDate isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentFollowDate];
                        }
                    }
                        break;
                        
                        
                    default: {
                        
                    }
                        break;
                }
                
                if (selectedSectionIndex == 6 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
                
            }
            
            cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            return cell;
        }
            break;
            
        case TAG_100:{
            
            //NSLog(@"%@",arrProducts);
            NSString *str = [NSString stringWithFormat:@"/C"];
            NSString *str1 = [NSString stringWithFormat:@"/F"];
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:@"cellIdentifier15"];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:14];
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                
            }
            
            if(isSearch)
            {
                
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"user_category_6"])
                {
                    if([[[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"user_category_6"] isEqualToString:@"GOLD"])
                    {
                        
                        cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];
                        //   [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
                    }
                    
                    
                }
                if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                    
                    NSString *str2 = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                    if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound) {
                        
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        }
                        else{
                            cell.lblValue.text = @"";
                        }
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                    else
                    {
                        //Freight
                        if ([str2 isEqualToString:str1]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                            
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            
                        }
                        
                        //Comment
                        if ([str2 isEqualToString:str]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = @"";
                            
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        }
                        
                    }
                }
                else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }
            else //For Product Array
            {
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                if([[[arrProducts objectAtIndex:indexPath.row] objectForKey:@"user_category_6"] isEqualToString:@"GOLD"])
                {
                    
                    cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];
                    //   [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
                }
                
                if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                    
                    NSString *str2 = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                    if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound) {
                        
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        }
                        else{
                            cell.lblValue.text = @"";
                        }
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                    else
                    {
                        //Freight
                        if ([str2 isEqualToString:str1]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                            
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            
                        }
                        
                        //Comment
                        if ([str2 isEqualToString:str]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = @"";
                            
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        }
                        
                        
                    }
                }
                else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        //                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        
                        //1stDec, 2014
                        cell.lblValue.text = [NSString stringWithFormat:@"%@    $ %.2f",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"QuantityOrdered"],[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }
            
            return cell;
            
        }break;
            
        case TAG_999:
        {
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier14];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:13];
            }
            
            int row = indexPath.row;
            cell.lblProdDate.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"DateRaised"];
            cell.lblProdQuantity.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Quantity"];
            cell.lblProdPrice.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Price"];
            cell.lblProdInv.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"InvNo"];
            
            return cell;
        }break;
        default:return nil;
            
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case TAG_50:{
            if ([indexPath section] == 1) {
                
                switch ([indexPath row]) {
                        //Debtor
                    case 0:
                    {
                        QuoteHistoryCustomerListViewController *dataViewController = [[QuoteHistoryCustomerListViewController alloc] initWithNibName:@"QuoteHistoryCustomerListViewController" bundle:[NSBundle mainBundle]];
                        dataViewController.isFromLoadProfile = NO;
                        dataViewController.isUserCallSchedule = NO;
                        
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                        break;
                        
                    default:
                        break;
                }
                
            }
            if ([indexPath section] == 2) {
                
                switch ([indexPath row]) {
                        //Delivery Run
                    case 9:
                    {
                        CustomCellGeneral *cell = (CustomCellGeneral *)[_tblHeader cellForRowAtIndexPath:indexPath];
                        
                        DeliveryRunViewController *dataViewController = [[DeliveryRunViewController alloc] initWithNibName:@"DeliveryRunViewController" bundle:[NSBundle mainBundle]];
                        
                        dataViewController.preferredContentSize =
                        CGSizeMake(300, 400);
                        
                        //create a popover controller
                        self.popoverController = [[UIPopoverController alloc]
                                                  initWithContentViewController:dataViewController];
                        
                        CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                        
                        dataViewController.delegate = self;
                        [self.popoverController presentPopoverFromRect:rect
                                                                inView:cell
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
                    }
                        break;
                        
                    default:{
                        //The user is selecting the cell which is currently expanded
                        //we want to minimize it back
                        if(selectedIndex == indexPath.row)
                        {
                            selectedIndex = -1;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            
                            return;
                        }
                        
                        //First we check if a cell is already expanded.
                        //If it is we want to minimize make sure it is reloaded to minimize it back
                        if(selectedIndex >= 0)
                        {
                            NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                            selectedIndex = indexPath.row;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                        }
                        
                        //Finally set the selected index to the new selection and reload it to expand
                        selectedIndex = indexPath.row;
                        selectedSectionIndex = indexPath.section;
                        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        
                        
                    }
                        break;
                }
                
            }
            
            if ([indexPath section] == 3 && [indexPath row] == 8) {
                CustomCellGeneral *cell = (CustomCellGeneral *)[_tblHeader cellForRowAtIndexPath:indexPath];
                
                DatePopOverController *dataViewController = [[DatePopOverController alloc] initWithNibName:@"DatePopOverController" bundle:[NSBundle mainBundle]];
                
                dataViewController.preferredContentSize =
                CGSizeMake(300, 300);
                dataViewController.isStockPickup = NO;
                dataViewController.isCallDate = NO;
                
                //create a popover controller
                self.popoverController = [[UIPopoverController alloc]
                                          initWithContentViewController:dataViewController];
                
                CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                
                dataViewController.delegate = self;
                [self.popoverController presentPopoverFromRect:rect
                                                        inView:cell
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
            }
            
            
            
            if ([indexPath section] == 6 && [indexPath row] == 0) {
                
                if (detailViewController1) {
                    [self dismissPopupViewControllerWithanimationType:nil];
                    detailViewController1= nil;
                }
                
                detailViewController1 = [[MJDetailViewController alloc] initWithNibName:@"MJDetailViewController" bundle:nil];
                
                detailViewController1.strMessage = strCommenttxt;
                [self presentPopupViewController:detailViewController1 animationType:3];
                
                return;
                
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    
                    return;
                }
                
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            
            
        }
            break;
            
        case TAG_100:{
            //**************ketaki code modified*************//
            /*  ProfileOrderProductDetailViewController *dataViewController = [[ProfileOrderProductDetailViewController alloc] initWithNibName:@"ProfileOrderProductDetailViewController" bundle:[NSBundle mainBundle]];
             
             NSString *substituteProduct = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"SUBSTITUTE"];
             NSMutableDictionary *dictDetailProd =    [self getSubstituteForObsProduct:[substituteProduct trimSpaces]];
             
             
             dataViewController.dictProductDetails = dictDetailProd;*/
            
            NSDictionary *dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
            
            //Comments
            if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/C"]){
                QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                
                dataViewController.commentStr = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Description"]];
                dataViewController.delegate = self;
                dataViewController.productIndex = [indexPath row];
                dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                
            }
            else if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"]){
            }
            else
            {
                if(isSearch)
                {
                    ProfileMaintenanceDetailVC *dataViewController = [[ProfileMaintenanceDetailVC alloc] initWithNibName:@"ProfileMaintenanceDetailVC" bundle:[NSBundle mainBundle]];
                    //NSLog(@"Product details : %@",[arrProducts objectAtIndex:[indexPath row]]);
                    dataViewController.strcustCategory=self.strCustCatagory;
                    NSString *statusCheck  = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"STATUS"];
                    NSString *substituteProduct = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"SUBSTITUTE"];
                    
                    
                    // Check Status  Product
                    if (statusCheck.length > 0 && [statusCheck isEqualToString:@"OB"] && substituteProduct.length > 0) {
                        NSMutableDictionary *dictDetailProd =    [self getSubstituteForObsProduct:[substituteProduct trimSpaces]];
                        dataViewController.strDebtor = [dictDetailProd objectForKey:@"CUSTOMER_CODE"];
                        dataViewController.dictProductDetails = dictDetailProd;
                        dataViewController.strWarehouse = [dictDetailProd objectForKey:@"Warehouse"];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.stock_code = [dictDetailProd objectForKey:@"StockCode"];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                    else
                    {
                        
                        dataViewController.strDebtor = strDebtor;
                        dataViewController.dictProductDetails = [arrSearchProducts objectAtIndex:[indexPath row]];
                        dataViewController.strWarehouse = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Warehouse"];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.stock_code = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                    
                    
                }
                else
                {
                    ProfileMaintenanceDetailVC *dataViewController = [[ProfileMaintenanceDetailVC alloc] initWithNibName:@"ProfileMaintenanceDetailVC" bundle:[NSBundle mainBundle]];
                    
                    dataViewController.strcustCategory=self.strCustCatagory;
                    NSString *statusCheck  = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"STATUS"];
                    NSString *substituteProduct = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"SUBSTITUTE"];
                    
                    
                    // Check Status  Product
                    if (statusCheck.length > 0 && [statusCheck isEqualToString:@"OB"] && substituteProduct.length > 0) {
                        NSMutableDictionary *dictDetailProd =    [self getSubstituteForObsProduct:[substituteProduct trimSpaces]];
                        dataViewController.strDebtor = [dictDetailProd objectForKey:@"CUSTOMER_CODE"];
                        dataViewController.dictProductDetails = dictDetailProd;
                        dataViewController.strWarehouse = [dictDetailProd objectForKey:@"Warehouse"];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.stock_code = [dictDetailProd objectForKey:@"StockCode"];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                    else{
                        dataViewController.strDebtor = strDebtor;
                        dataViewController.dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                        dataViewController.strWarehouse = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Warehouse"];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.stock_code = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                        
                    }
                }
            }
        }
            break;
    }
    
}


#pragma mark- Check in Sysdisct Temp table



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    switch (tableView.tag) {
            
        case TAG_50:{
            return NO;
        }break;
            
        case TAG_100:{
            /*   //New product,Freight,Comment
             if ([[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"] isEqualToString:@"1"]||[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"] isEqualToString:@"2"]) {
             return YES;
             }
             else{
             return NO;
             }
             */
            return YES;
            
        }break;
            
        default:
            return NO;
            break;
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag)
    {
        case TAG_100:
        {
            if (editingStyle == UITableViewCellEditingStyleDelete)
            {
                if(isSearch == YES && arrSearchProducts.count > 0)
                {
                    NSDictionary *dict = [arrSearchProducts objectAtIndex:[indexPath row]];
                    
                    [arrSearchProducts removeObjectAtIndex:[indexPath row]];
                    int indexval = [arrProducts indexOfObject:dict];
                    
                    if(indexval < arrProducts.count && indexval >= 0)
                    {
                        [arrProducts removeObjectAtIndex:indexval];
                        
                    }
                    
                    
//                    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//                    [tableView endUpdates];
                    
                    if ([dict objectForKey:@"ExtnPrice"])
                    {
                        NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
                        totalCost -= [strExtnPrice floatValue];
                        totalLines = totalLines - 1;
                    }
                    lblTotalLines.text = [NSString stringWithFormat:@"$%d",totalLines ];
                    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
                    
                                   [_tblDetails reloadData];
                    
                    [self repositionLoadMoreFooterView];
                    
                    if ([arrSearchProducts count] > cntProfileOrders)
                    {
                        //Show delete button
                    }
                    else
                    {
                        btnAddMenu.enabled = TRUE;
                        //                    [tableView setEditing:NO animated:YES]; 9th feb
                    }
                    
                    deleteProductDictionary = (NSMutableDictionary *)dict;
                    //                    if ([arrSearchProducts count] == 0) {
                    //                        [vwContentSuperview removeAllSubviews];
                    //                        [vwContentSuperview addSubview:vwNoProducts];
                    //                        [self showOrEnableButtons];
                    //                    }
                    //                    else
                    //                    {
                    if(deleteProductDictionary.count > 0)
                    {
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        [databaseQueue   inDatabase:^(FMDatabase *db) {
                            [self getSmcusproData:db];
                        }];
                        databaseQueue = nil;
                    }
                    
                    
                    //                    }
                    
                }
                else
                {
                    NSDictionary *dict = [arrProducts objectAtIndex:[indexPath row]];
                    
                    [arrProducts removeObjectAtIndex:[indexPath row]];
//                    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//                    [tableView endUpdates];
                    
                    if ([dict objectForKey:@"ExtnPrice"])
                    {
                        NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
                        totalCost -= [strExtnPrice floatValue];
                        totalLines = totalLines - 1;
                    }
                    lblTotalLines.text = [NSString stringWithFormat:@"$%d",totalLines ];
                    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
                    
                    [_tblDetails reloadData];
                    
                    [self repositionLoadMoreFooterView];
                    
                    if ([arrProducts count] > cntProfileOrders)
                    {
                        //Show delete button
                    }
                    else
                    {
                        btnAddMenu.enabled = TRUE;
                        //                    [tableView setEditing:NO animated:YES]; 9th feb
                    }
                    
                    deleteProductDictionary = (NSMutableDictionary *)dict;
                    if ([arrProducts count] == 0) {
                        [vwContentSuperview removeAllSubviews];
                        [vwContentSuperview addSubview:vwNoProducts];
                        [self showOrEnableButtons];
                    }
                    
                        
                        if(deleteProductDictionary.count > 0)
                        {
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue   inDatabase:^(FMDatabase *db) {
                                        [self getSmcusproData:db];
                                }];
                                databaseQueue = nil;
                        }
                    
                }
            }
        }break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:
        {
            if (section == 1) {
                return nil;
                
            }
            if (section == 4) {
                return @"Comments";
            }
        }break;
            
    }
    
    // Return the displayed title for the specified section.
    return nil;
}


#pragma mark - IBActions
//Load Profile
- (IBAction)actionShowQuoteHistory:(id)sender{
    //Change the selection
    [segControl setSelectedIndex:1];
    
    QuoteHistoryCustomerListEditableMode *dataViewController = [[QuoteHistoryCustomerListEditableMode alloc] initWithNibName:@"QuoteHistoryCustomerListEditableMode" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

-(void)saveOrderForProfile{
    
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts)
    {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        if ([dict objectForKey:@"Item"]){
            
            if (![dict objectForKey:@"ExtnPrice"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = FALSE;
                break;
            }
            else if (![dict objectForKey:@"ExtnPrice"]&& ![[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = TRUE;
                //  break;
            }
            else if ([dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = FALSE;
                break;
            }
            else{
                isCalculationPending = FALSE;
            }
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    
    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        [self callInsertProfileOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
    }
    else{
        //31stOct
        [self callInsertProfileOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
        //[self callWSSaveProfileOrder];
    }
}


//Delegate Method
- (void)actionSaveQuote:(int)finalizeTag
{
    
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts)
    {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        //        if ([dict objectForKey:@"Item"]){
        //
        //            if (![dict objectForKey:@"ExtnPrice"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
        //                isCalculationPending = FALSE;
        //                break;
        //            }
        //            else if (![dict objectForKey:@"ExtnPrice"]&& ![[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
        //                isCalculationPending = TRUE;
        //                //  break;
        //            }
        //            else if ([dict objectForKey:@"ExtnPrice"]) {
        //                isCalculationPending = FALSE;
        //                break;
        //            }
        //            else{
        //                isCalculationPending = FALSE;
        //            }
        //        }
        
        
        
        if ([dict objectForKey:@"ExtnPrice"])
        {
            isCalculationPending = false;
            break;
        }
        else if ([[dict objectForKey:@"Item"] isEqualToString:@"/C"]){
            isCalculationPending = false;
            break;
        }
        else
        {
            isCalculationPending = TRUE;
            
        }
    }
    
    
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have not selected any items." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    //    if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderOnHold"] isEqualToString:@"Y"]) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Order on hold" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //        alert.tag = TAG_400;
    //        [alert show];
    //    }
    //    else{
    
    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        [self callInsertProfileOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
    }
    else{
        //31stOct
        [self callInsertProfileOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
        //[self callWSSaveProfileOrder];
    }
}

//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([_tblDetails isEditing]) {
        [self showOrEnableButtons];
        //        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        //
        //        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        //        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        //
        //        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        //
        
        [_tblDetails setEditing:NO animated:YES];
        btnDelete.hidden = YES;
    }
    else{
        
        [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [_tblDetails setEditing:YES animated:YES];
    }
    
    //[_tblDetails reloadData];
}

- (IBAction)actionShowProductList:(id)sender{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (IBAction)actionDisplayQuoteMenu:(id)sender{
    
    //tagFlsh = 1;
    //[self flashFinal:btnAddMenu];
    
    QuoteMenuPopOverViewController *popoverContent = [[QuoteMenuPopOverViewController alloc] initWithNibName:@"QuoteMenuPopOverViewController" bundle:[NSBundle mainBundle]];
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.preferredContentSize =
    CGSizeMake(200, 300);
    
    //Finalise option not needed
    //    popoverContent.isFinalisedNeeded = NO;
    //    popoverContent.isCommentsNotNeeded = NO;
    //    popoverContent.isProfileOrderEntry = NO;
    //    popoverContent.isFromOfflineOrder = NO;
    popoverContent.isProfileMaintenance = YES;
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    popoverController.delegate = self;
    popoverContent.delegate = self;
    
    
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_x_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect rectBtnMenu = CGRectMake(btnAddMenu.frame.origin.x, self.view.frame.size.height - 40,btnAddMenu.frame.size.width,btnAddMenu.frame.size.height);
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:rectBtnMenu
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}

- (IBAction)actionShowComment:(id)sender{
    [self showCommentView];
}

- (IBAction)actionShowFreight:(id)sender{
    [self showFreightView];
}

#pragma mark - Switch
- (IBAction)switchToggled:(id)sender {
    //a switch was toggled.
    //maybe use it's tag property to figure out which one
    
    UISwitch *swtch = (UISwitch*)sender;
    UIView *view = swtch.superview; //Cell contentView
    CustomCellGeneral *cell = (CustomCellGeneral *)view.superview;
    
    //NSIndexPath *indexPath = [(UITableView*)cell.superview indexPathForCell:cell];
    //NSLog(@"%d %d ", indexPath.section, indexPath.row);
    
    switch (swtch.tag) {
        case TAG_200:{
            
            if (swtch.on) {
                
                self.isCashPickUpOn = @"Y";
                CashPickUpPopOverController *popoverContent = [[CashPickUpPopOverController alloc] initWithNibName:@"CashPickUpPopOverController" bundle:[NSBundle mainBundle]];
                
                //resize the popover view shown
                //in the current view to the view's size
                popoverContent.contentSizeForViewInPopover =
                CGSizeMake(300, 150);
                
                //create a popover controller
                self.popoverController = [[UIPopoverController alloc]
                                          initWithContentViewController:popoverContent];
                
                
                CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                
                popoverContent.delegate = self;
                [self.popoverController presentPopoverFromRect:rect
                                                        inView:cell
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
                
            }
            else{
                self.isCashPickUpOn = @"N";
                
            }
        }
            break;
            
        case TAG_300:{
            if (swtch.on) {
                isStorePickUpOn = @"Y";
                StorePickupViewController *obj = [[StorePickupViewController alloc] init];
                
                obj.delegate = self;
                
                UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:obj];
                nvc.navigationBarHidden = YES;
                obj.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
            }
            else{
                isStorePickUpOn = @"N";
            }
        }
            break;
            
            
        case TAG_500:{
            if (swtch.on) {
                isStockPickUpOn = @"Y";
                StockPickupViewController *obj = [[StockPickupViewController alloc] init];
                obj.delegate = self;
                
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:(strDebtor)?strDebtor:@"" forKey:@"debtor_number"];
                [dict setObject:(strDelName)?strDelName:@"" forKey:@"debtor_name"];
                [dict setObject:(strDateRaised)?strDateRaised:@"" forKey:@"date_raised"];
                
                //                strDateRaised
                
                obj.debtorDetailDict = dict;
                
                UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:obj];
                nvc.navigationBarHidden = YES;
                obj.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
                
            }
            else{
                isStockPickUpOn = @"N";
            }
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    /*
     if (scrollView.isDragging) {
     
     CGFloat endOfTable = [self endOfTableView:scrollView];
     
     if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
     [_loadMoreFooterView setState:StateNormal];
     
     } else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
     [_loadMoreFooterView setState:StatePulling];
     }
     }
     */
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    /*
     if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
     
     //if([arrProducts count] < totalCount){
     // Show loading footer
     _loadingInProgress = YES;
     [_loadMoreFooterView setState:StateLoading];
     
     [UIView beginAnimations:nil context:NULL];
     [UIView setAnimationDuration:0.2];
     _tblDetails.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
     [UIView commitAnimations];
     
     // Load some more data into table, pretend it took 3 seconds to get it
     //[self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
     //}
     }
     */
}


#pragma mark - PullToLoadMoreView helper methods


- (void) setupLoadMoreFooterView
{
    /*
     CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
     
     _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
     _loadMoreFooterView.backgroundColor = [UIColor clearColor];
     
     [_tblDetails addSubview:_loadMoreFooterView];
     */
}

- (void) loadMoreData
{
    /*
     [operationQueue addOperationWithBlock:^{
     
     FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
     [databaseQueue   inDatabase:^(FMDatabase *db) {
     [self callViewQuoteDetailsDetailsFromDB:db];
     
     }];
     
     [[NSOperationQueue mainQueue]addOperationWithBlock:^{
     [self doAfterDataFetch];
     }];
     }];
     */
}

- (void) didFinishLoadingMoreSampleData
{
    /*
     _loadingInProgress = NO;
     [_tblDetails setContentInset:UIEdgeInsetsMake(0.0f,0.0f, 0.0f, 0.0f)];
     
     if ([_loadMoreFooterView state] != StateNormal) {
     [_loadMoreFooterView setState:StateNormal];
     }
     */
}

- (CGFloat) tableViewContentHeight
{
    //NSLog(@"tblProductList.contentSize.height %f",tblProductList.contentSize.height);
    return _tblDetails.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    /*
     _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
     [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
     */
}


#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark - Database call

-(NSMutableDictionary *)getSubstituteForObsProduct:(NSString *)substituteProdStr{
    
    NSMutableDictionary *substituteProdDict = [[NSMutableDictionary alloc] init];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    
    
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            
            NSString *warehouseStr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces];
            if (warehouseStr.length < 2 ) {
                warehouseStr = [NSString stringWithFormat:@"0%@",warehouseStr];
            }
            
            
            FMResultSet *rs2 = [db executeQuery:@"SELECT AVAILABLE,STOCK_CODE,SUBSTITUTE,STATUS, PREF_SUPPLIER,MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, max(AVERAGE_COST) AS AVERAGE_COST,PICTURE_FIELD as picture_field , MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4,USER_CATEGORY_6 as user_category_6 from (SELECT a.AVAILABLE as AVAILABLE, a.PREF_SUPPLIER AS PREF_SUPPLIER, a.STATUS as STATUS,a.SUBSTITUTE as SUBSTITUTE,a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE,a.USER_CATEGORY_6 as user_category_6, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.AVERAGE_COST AS AVERAGE_COST,a.STANDARD_COST AS STANDARD_COST, a.PROD_GROUP as PROD_GROUP,  a.PICTURE_FIELD as picture_field ,                                                                                                                                                                                                                                                                                            (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                                                                                                                                                                   AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                                                                                                                                                              WHERE a.code= ?)",substituteProdStr];//AND a.code= ?  AND (a.`STATUS` = 'AC' OR a.`STATUS` = 'NO'))
            
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            isRecordsExist = FALSE;
            while ([rs2 next]) {
                
                isRecordsExist = TRUE;
                
                NSDictionary *dictData = [rs2 resultDictionary];
                
                [substituteProdDict setValue:[dictData objectForKey:@"CUSTOMER_CODE"] forKey:@"CUSTOMER_CODE"];
                [substituteProdDict setValue:[dictData objectForKey:@"AVAILABLE"] forKey:@"Available"];
                [substituteProdDict setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
                [substituteProdDict setValue:[dictData objectForKey:@"COST_DISS"] forKey:@"Dissection_Cos"];
                [substituteProdDict setValue:[dictData objectForKey:@"SALES_DISS"] forKey:@"Dissection"];
                [substituteProdDict setValue:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"LastSold"];
                [substituteProdDict setValue:[dictData objectForKey:@"STOCK_CODE"] forKey:@"StockCode"];
                
                [substituteProdDict setValue:[dictData objectForKey:@"PICTURE_FIELD"] forKey:@"Picture_field"];
                
                //            PREF_SUPPLIER
                [substituteProdDict setValue:[dictData objectForKey:@"PREF_SUPPLIER"] forKey:@"PREF_SUPPLIER"];
                [substituteProdDict setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
                [substituteProdDict setValue:[dictData objectForKey:@"PRICE2"] forKey:@"Price2"];
                [substituteProdDict setValue:[dictData objectForKey:@"PRICE3"] forKey:@"Price3"];
                [substituteProdDict setValue:[dictData objectForKey:@"PRICE4"] forKey:@"Price4"];
                [substituteProdDict setValue:[dictData objectForKey:@"PRICE5"] forKey:@"Price5"];
                [substituteProdDict setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
                [substituteProdDict setValue:[dictData objectForKey:@"q0"] forKey:@"q0"];
                [substituteProdDict setValue:[dictData objectForKey:@"q1"] forKey:@"q1"];
                [substituteProdDict setValue:[dictData objectForKey:@"q2"]forKey:@"q2"];
                [substituteProdDict setValue:[dictData objectForKey:@"q3"] forKey:@"q3"];
                [substituteProdDict setValue:[dictData objectForKey:@"q4"] forKey:@"q4"];
                [substituteProdDict setValue:[dictData objectForKey:@"STANDARD_COST"] forKey:@"Cost"];
                [substituteProdDict setValue:[dictData objectForKey:@"AVERAGE_COST"] forKey:@"AverageCost"];
                [substituteProdDict setValue:[dictData objectForKey:@"TAX_CODE1"] forKey:@"TAX_CODE1"];
                [substituteProdDict setValue:[dictData objectForKey:@"user_category_6"] forKey:@"user_category_6"];
                
                [substituteProdDict setValue:@"0" forKey:@"isNewProduct"];
                
                [substituteProdDict setValue:([dictData objectForKey:@"SUBSTITUTE"])?([dictData objectForKey:@"SUBSTITUTE"]):@"" forKey:@"SUBSTITUTE"];
                [substituteProdDict setValue:[dictData objectForKey:@"STATUS"]?[dictData objectForKey:@"STATUS"]:@"" forKey:@"STATUS"];
                
                
                NSLog(@"SUBSTITUTE %@",substituteProdDict);
                
            }
            
            [rs2 close];
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
            
        }
        
        // NSLog(@"arrProducts for edit action==%@",arrProducts);
    }];
    return substituteProdDict;
}

-(void)callGetNewOrderNumberFromDB{
    
    strWebserviceType = @"DB_GET_ORDER_NUMBER";
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            
            FMResultSet *rs = [db executeQuery:@"SELECT last_order_num FROM (SELECT ORDER_NO as last_order_num FROM soheader UNION SELECT ORDER_NO as last_order_num FROM soheader_offline)ORDER BY last_order_num DESC LIMIT 1"];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                
                NSString *strLastOrderNum = [[rs resultDictionary] objectForKey:@"last_order_num"];
                int numLastOrderNum = [strLastOrderNum integerValue];
                
                int numNewOrderNum  = ++numLastOrderNum;
                self.strOrderNum = [NSString stringWithFormat:@"%d",numNewOrderNum];
                
                [dictHeaderDetails setValue:strOrderNum forKey:@"OrderNum"];
                
                [_tblHeader reloadData];
                
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}


-(void)getBranchListNameForUser{
    
   /* FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            NSString *loggedInsalesman = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
            
            NSString *sqlQuery =[NSString stringWithFormat:@"SELECT BRANCH_LIST FROM syslogon WHERE LOGNAME =  '%@'",loggedInsalesman];
            
            FMResultSet *rs = [db executeQuery:sqlQuery];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                NSDictionary *dictData = [rs resultDictionary];
                NSString *strBranch1 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"BRANCH_LIST"]];
                [self callWSGetWarehouse:strBranch1];
                [[NSUserDefaults standardUserDefaults] setObject:strBranch1 forKey:@"BRANCH"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
    */
    
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            NSString *loggedInsalesman = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
            
            NSString *sqlQuery =[NSString stringWithFormat:@"SELECT BRANCH_LIST FROM syslogon WHERE LOGNAME =  '%@'",loggedInsalesman];
            
            FMResultSet *rs = [db executeQuery:sqlQuery];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                NSDictionary *dictData = [rs resultDictionary];
                NSString *strBranch1 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"BRANCH_LIST"]];
                if(strBranch1.length > 0 && ![strBranch1 containsString:@"null"])
                {
                    [[NSUserDefaults standardUserDefaults] setObject:strBranch1 forKey:@"BRANCH"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"00" forKey:@"BRANCH"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                }
                debtorWarehouse = @"00";
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}

-(void)callOrderNumberFromDB{
    
    strWebserviceType = @"DB_GET_ORDER_NUMBER";
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            //            NSString *sqlQuery = [NSString stringWithFormat:@"SELECT * FROM soheader_offline where ORDER_NO = %@",strOfflineOrderNum];
            NSString *sqlQuery = @"SELECT * FROM soheader_offline where ORDER_NO = 'R022'";
            
            FMResultSet *rs = [db executeQuery:sqlQuery];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                
                NSDictionary *OfflineOrderDict = [rs resultDictionary];
                NSLog(@"%@",OfflineOrderDict);
                
                //                [dictHeaderDetails setValue:strOrderNum forKey:@"OrderNum"];
                dictHeaderDetails = [NSMutableDictionary dictionaryWithDictionary:OfflineOrderDict];
                [_tblHeader reloadData];
                
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}



//Load product codes from
-(BOOL)callGetProductFromCustProfileDB:(FMDatabase *)db ProductCode:(NSString *)productCode{
    @try {
        
        
        FMResultSet *rs1 = [db executeQuery:@"SELECT STOCK_CODE FROM smcuspro WHERE CUSTOMER_CODE = ? AND STOCK_CODE = ?",strDebtor,productCode];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            [rs1 close];
            return TRUE;
        }
        
        [rs1 close];
        return FALSE;
        
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        return FALSE;
    }
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_GET_PRODUCT_IN_DETAILS";
    ///[arrProducts removeAllObjects];
    
    @try {
        
        //**********ketaki code modified***********//
        FMResultSet *rs0 = [db executeQuery:@"SELECT CUST_CATEGORY FROM armaster WHERE trim(CODE) = ?",[strDebtor trimSpaces]];
        
        if (!rs0)
        {
            [rs0 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs0 next]) {
            NSDictionary *dictData = [rs0 resultDictionary];
            self.strCustCatagory = [dictData objectForKey:@"CUST_CATEGORY"];
            NSLog(@"%@",self.strCustCatagory);
        }
        
        [rs0 close];
        //**************************************************************************************//
        
        
        
        
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE trim(CODE) = ?",[strDebtor trimSpaces]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            self.strPriceCode = [dictData objectForKey:@"PRICE_CODE"];
        }
        
        [rs1 close];
        
        
        //        FMResultSet *rs2 = [db executeQuery:@"SELECT STOCK_CODE, MAX(SCM_RECNUM) AS SCM_RECNUM, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, max(AVERAGE_COST) AS AVERAGE_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT                                                                                                                                                 a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.AVERAGE_COST AS AVERAGE_COST,a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1,                                                                                                                                                 a.`SCM_RECNUM` AS SCM_RECNUM,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ?) temp GROUP BY STOCK_CODE LIMIT ?,?",strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces],[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE_FOR_PROFILE]];
        
        //NSLog(@"strQuery::::%@",strQuery);
        
        //Change by subhu
        strDebtor = [strDebtor trimSpaces];
        NSString *warehouseStr = debtorWarehouse;
        if (warehouseStr.length < 2 ) {
            warehouseStr = [NSString stringWithFormat:@"0%@",warehouseStr];
        }
        
        
        
        FMResultSet *rs2 = [db executeQuery:@"SELECT (SUM(SALES_00) + SUM(SALES_01) + SUM(SALES_02) + SUM(SALES_03)) AS TOTAL, AVAILABLE,STOCK_CODE,SUBSTITUTE,STATUS, PREF_SUPPLIER,MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, max(AVERAGE_COST) AS AVERAGE_COST,PICTURE_FIELD as PICTURE_FIELD ,PROD_GROUP as PROD_GROUP , MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 ,USER_CATEGORY_6 as user_category_6 from (SELECT b.SALES_00 as SALES_00, b.SALES_01 as SALES_01, b.SALES_02 as SALES_02, b.SALES_03 as SALES_03, a.AVAILABLE as AVAILABLE, a.PREF_SUPPLIER AS PREF_SUPPLIER, a.STATUS as STATUS,a.SUBSTITUTE as SUBSTITUTE,a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.AVERAGE_COST AS AVERAGE_COST,a.STANDARD_COST AS STANDARD_COST,a.USER_CATEGORY_6 as user_category_6, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1, a.PROD_GROUP as PROD_GROUP, a.PICTURE_FIELD as picture_field ,                                                                                                                                                                                                                                                                                             (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 LEFT JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 LEFT JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ?) GROUP BY STOCK_CODE ORDER BY PROD_GROUP ,TOTAL Desc",strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,strDebtor,warehouseStr];//ORDER BY PROD_GROUP
        
        //[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces]//AND (a.`STATUS` = 'AC' OR a.`STATUS` = 'NO' OR a.`STATUS` = 'OB' ))
        
        if (!rs2)
        {
            [rs2 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        isRecordsExist = FALSE;
        while ([rs2 next]) {
            
            isRecordsExist = TRUE;
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            [dict setValue:[dictData objectForKey:@"CUSTOMER_CODE"] forKey:@"CUSTOMER_CODE"];
            [dict setValue:[dictData objectForKey:@"AVAILABLE"] forKey:@"Available"];
            [dict setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
            [dict setValue:[dictData objectForKey:@"COST_DISS"] forKey:@"Dissection_Cos"];
            [dict setValue:[dictData objectForKey:@"SALES_DISS"] forKey:@"Dissection"];
            [dict setValue:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"LastSold"];
            [dict setValue:[dictData objectForKey:@"STOCK_CODE"] forKey:@"StockCode"];
            
            [dict setValue:[dictData objectForKey:@"PICTURE_FIELD"] forKey:@"Picture_field"];
            [dict setValue:[dictData objectForKey:@"PROD_GROUP"] forKey:@"PROD_GROUP"];
            
            //            PREF_SUPPLIER
            [dict setValue:[dictData objectForKey:@"PREF_SUPPLIER"] forKey:@"PREF_SUPPLIER"];
            [dict setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
            [dict setValue:[dictData objectForKey:@"PRICE2"] forKey:@"Price2"];
            [dict setValue:[dictData objectForKey:@"PRICE3"] forKey:@"Price3"];
            [dict setValue:[dictData objectForKey:@"PRICE4"] forKey:@"Price4"];
            [dict setValue:[dictData objectForKey:@"PRICE5"] forKey:@"Price5"];
            [dict setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
            [dict setValue:[dictData objectForKey:@"q0"] forKey:@"q0"];
            [dict setValue:[dictData objectForKey:@"q1"] forKey:@"q1"];
            [dict setValue:[dictData objectForKey:@"q2"]forKey:@"q2"];
            [dict setValue:[dictData objectForKey:@"q3"] forKey:@"q3"];
            [dict setValue:[dictData objectForKey:@"q4"] forKey:@"q4"];
            [dict setValue:[dictData objectForKey:@"STANDARD_COST"] forKey:@"Cost"];
            [dict setValue:[dictData objectForKey:@"AVERAGE_COST"] forKey:@"AverageCost"];
            [dict setValue:[dictData objectForKey:@"TAX_CODE1"] forKey:@"TAX_CODE1"];
            [dict setValue:[dictData objectForKey:@"user_category_6"] forKey:@"user_category_6"];
            //[dict setValue:[dictData objectForKey:@"CUST_CATEGORY"] forKey:@"cust_catagory"];
            
            [dict setValue:@"0" forKey:@"isNewProduct"];
            
            [dict setValue:([dictData objectForKey:@"SUBSTITUTE"])?([dictData objectForKey:@"SUBSTITUTE"]):@"" forKey:@"SUBSTITUTE"];
            [dict setValue:[dictData objectForKey:@"STATUS"]?[dictData objectForKey:@"STATUS"]:@"" forKey:@"STATUS"];
            
            
            // Avoid double up
            if ([[arrProducts valueForKey:@"StockCode"] containsObject:[dict objectForKey:@"StockCode"]])
            {
                NSLog(@"OBJECT EXISTS");
            }
            else
            {
                [arrProducts addObject:dict];
            }
            
            
        }
        
        NSLog(@"COUNT   ############ %d",[arrProducts count]);
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:[arrProducts count]];
                    *stop = YES;
                }
            }];
        }
        
        if (isFrightAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/F"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:[arrProducts count]];
                    *stop = YES;
                }
            }];
        }
        
        self.cntProfileOrders = [arrProducts count];
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}


-(void)insertStockPickupToDB{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            BOOL y2;
            
            NSString *orderNo = @"";
            NSString *Pickup_no = @"";
            NSString *today_date = @"";
            NSString *requestedBy = @"";
            NSString *debtor = @"";
            NSString *debtor_name = @"";
            NSString *contact_phone_no = @"";
            NSString *contact_acc_no = @"";
            NSString *related_invoice = @"";
            NSString *product_code = @"";
            NSString *batch_no= @"";
            NSString *best_before_date= @"";
            NSString *email_to= @"";
            NSString *email_cc= @"";
            
            NSString *reasonReturn= @"";
            NSString *detailReasonReturn= @"";
            
            NSString *quantity = @"";
            NSString *returnVal = @"";
            
            NSString *batch_name = @"";
            NSString *product_name = @"";
            
            
            NSData *image_attched1= NULL;
            NSData *image_attched2= NULL;
            NSData *image_attched3= NULL;
            NSData *image_attched4= NULL;
            NSData *image_attched5= NULL;
            NSData *image_attched6= NULL;
            
            
            
            // Set PickUp form details
            if([dictHeaderDetails objectForKey:@"PickUpForm"])
            {
                self.strPickupForm = [dictHeaderDetails objectForKey:@"PickUpForm"];
                NSArray *stockPickupArr= [self.strPickupForm componentsSeparatedByString:@"|"];
                NSLog(@"%@",stockPickupArr);
                
                orderNo = strOrderNum;
                Pickup_no = [dictStockPickup objectForKey:@"pickup_no"];
                
                requestedBy = [dictStockPickup objectForKey:@"requested_by"];
                
                NSDate *todaysDate = [NSDate date];
                NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
                today_date = [dateFormat2 stringFromDate:todaysDate];
                
                debtor = [dictStockPickup objectForKey:@"debtor"];
                debtor_name =[dictStockPickup objectForKey:@"debtor_name"];
                contact_phone_no = [dictStockPickup objectForKey:@"Contact_Phone"];
                contact_acc_no = [dictStockPickup objectForKey:@"Contact_Person"];
                related_invoice =[dictStockPickup objectForKey:@"RelatedInvoice"];
                product_code = [dictStockPickup objectForKey:@"product_code"];
                batch_no= [dictStockPickup objectForKey:@"BatchNo"];
                best_before_date= [dictStockPickup objectForKey:@"BestBeforeDate"];
                email_to= [dictStockPickup objectForKey:@"EmailRecipient"];
                email_cc= [dictStockPickup objectForKey:@"EmailCC"];
                
                reasonReturn=[dictStockPickup objectForKey:@"ReasonReturn"];
                detailReasonReturn= [dictStockPickup objectForKey:@"detailReasonReturn"];
                
                returnVal = [dictStockPickup objectForKey:@"Return"];
                quantity = [dictStockPickup objectForKey:@"Quantity"];
                
                product_name = [dictStockPickup objectForKey:@"description"];
                batch_name = [dictStockPickup objectForKey:@"BatchDetail"];
                
                image_attched1= [dictStockPickup objectForKey:@"image0"];
                image_attched2=[dictStockPickup objectForKey:@"image1"];
                image_attched3= [dictStockPickup objectForKey:@"image2"];
                image_attched4= [dictStockPickup objectForKey:@"image3"];
                image_attched5= [dictStockPickup objectForKey:@"image4"];
                image_attched6= [dictStockPickup objectForKey:@"image5"];
                
            }
            //    BATCH_NAME varchar,PRODUCT_NAME varchar)
            
            
            y2 = [db executeUpdate:@"INSERT INTO `StockPickup` (ORDER_NO,PICKUP_NO,TODAY_DATE,REQUESTED_BY,DEBTOR,DEBTOR_NAME,CONTACT_PHONE_NUMBER,CONTACT_ACCOUNT_NO,RELATED_INVOICE_NO,PRODUCT_CODE,BATCH_NUMBER,BEST_BEFORE_DATE,EMAIL_TO,EMAIL_CC,IMAGE_ATTACHED1,IMAGE_ATTACHED2,IMAGE_ATTACHED3,IMAGE_ATTACHED4,IMAGE_ATTACHED5,IMAGE_ATTACHED6,REASON_OF_RETURN,DETAIL_REASON_RETURN,QUANTITY,RETURN,BATCH_NAME,PRODUCT_NAME) VALUES (?, ?, ?, ?, ?,?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?)", orderNo,Pickup_no, today_date,requestedBy, debtor,debtor_name,contact_phone_no,contact_acc_no,related_invoice,product_code,batch_no,best_before_date,email_to,email_cc,image_attched1,image_attched2,image_attched3,image_attched4,image_attched5,image_attched6,reasonReturn,detailReasonReturn,quantity,returnVal,batch_name,product_name];
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                NSLog(@"%@",strErrorMessage);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            });
        }
    }];
    
    
}



-(void)callInsertProfileOrderToDB{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            BOOL y2;
            
            self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
            //self.strTotalLines= [NSString stringWithFormat:@"%d",[arrProducts count]];
            //            if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            
            if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                
                self.strUploadDate = self.strDateRaised;
                
                int len = [strSalesType length];
                
                FMResultSet *rs = [db executeQuery:@"SELECT MAX(last_order_num) as lastOrderNum FROM (SELECT CAST(substr(ORDER_NO,?) AS INT) as last_order_num FROM soheader_offline WHERE ORDER_NO LIKE ?)",[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", strSalesType]];
                
                if (!rs)
                {
                    [rs close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                int numNewOrderNum;
                if ([rs next]) {
                    
                    if ([[[rs resultDictionary] objectForKey:@"lastOrderNum"] isEqual:[NSNull null]]) {
                        numNewOrderNum = 1;
                    }
                    else{
                        int numLastOrdNum = [[[rs resultDictionary] objectForKey:@"lastOrderNum"] intValue];
                        numNewOrderNum  = ++numLastOrdNum;
                    }
                }
                else{
                    numNewOrderNum = 1;
                }
                
                [rs close];
                
                self.strOrderNum = [NSString stringWithFormat:@"%@%d",strSalesType,numNewOrderNum];
                
                if([dictHeaderDetails objectForKey:@"StockPickUp"])
                {
                    self.strStockPickup = [dictHeaderDetails objectForKey:@"StockPickUp"];
                }
                
                // Set PickUp form details
                if([dictHeaderDetails objectForKey:@"PickUpForm"])
                {
                    self.strPickupForm = [dictHeaderDetails objectForKey:@"PickUpForm"];
                }
                
                bool Flag = NO;
                NSString *strINC = @"N";
                
                //--Check for Product with Price
                for (NSDictionary *dict in arrProducts) {
                    
                    if([dict objectForKey:@"PriceChnaged"])
                    {
                        if(Flag == NO)
                        {
                            if([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
                            {
                                Flag = YES;
                                strINC = [dict objectForKey:@"PriceChnaged"];
                                self.strPriceChanged =[dict objectForKey:@"PriceChnaged"];
                            }
                            
                        }
                    }
                }
                
                
                // Check for StockPickup form value
                NSString *pickup_num;
                if (dictStockPickup.count > 0) {
                    pickup_num = [dictStockPickup objectForKey:@"pickup_no"];
                }
                else{
                    pickup_num  = @"";
                }
                
                y2 = [db executeUpdate:@"INSERT INTO `soheader_offline` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS,PICKUP_INFO, PERIOD_RAISED,YEAR_RAISED,ORDER_VALUE,DETAIL_LINES, isUploadedToServer,isProfileOrder,isEdited,TOTL_CARTON_QTY,PriceChnaged,PICKUP_NO) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?, ?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strSalesman,strSalesman,strStockPickup,strPickupForm,strPeriodRaised,strPeriodRaised,strTotalCost,strTotalLines,[NSNumber numberWithInt:0],[NSNumber numberWithInt:1],[NSNumber numberWithInt:0],self.strTotalCartonQty,strPriceChanged,pickup_num];
                
            }
            else{
                //Insert
                y2 = [db executeUpdate:@"INSERT INTO `soheader` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS,PICKUP_INFO, PERIOD_RAISED,YEAR_RAISED,EXCHANGE_RATE,TOTL_CARTON_QTY,ACTIVE,EDIT_STATUS,ORDER_VALUE,DETAIL_LINES, `created_date`, `modified_date`,TOTL_CARTON_QTY) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strUploadDate,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strOperator,strOperator,strStockPickup,strPickupForm,strPeriodRaised,strYearRaised,strExchangeRate,strTotalCartonQty,strActive,strEditStatus,strTotalCost,strTotalLines,strUploadDate,strUploadDate,self.strTotalCartonQty];
                
                
            }
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            NSArray *components = [strUploadDate componentsSeparatedByString:@" "];
            NSString *strCreatedTime ;
            if(components.count > 1)
            {
                strCreatedTime = (NSString *)[components objectAtIndex:1];
            }
            else
            {
                components = [strUploadDate componentsSeparatedByString:@"T"];
                if(components.count > 1)
                {
                    strCreatedTime = (NSString *)[components objectAtIndex:1];
                }
            }
            NSString *strCreatedDate = (NSString *)[components objectAtIndex:0];
            
            float totalWeight = 0;
            float totalVolume = 0 ;
            float totalTax = 0;
            int totalQuantity = 0;
            
            int detail_lines = 0;
            int line_num = 0;
            
            
            for (NSMutableDictionary *dictProductDetails in arrProducts) {
                
                NSString *strStockCodenew = nil;
                NSString *strPrice = @"0";
                NSString *strDissection = @"0";
                NSString *strDissection_Cos = @"0";
                NSString *strWeight = @"0";
                NSString *strVolume = @"0";
                NSString *strCost = @"0";
                NSString *strExtension = @"0";
                NSString *strGross = @"0";
                NSString *strTax = @"0";
                NSString *strQuantityOrdered = @"0";
                
                NSString *strAltQuantity = @"0";
                NSString *strCUST_PRICE = @"0";
                NSString *strCUST_ORD_QTY  = @"0";
                NSString *strCUST_SHIP_QTY = @"0";
                NSString *strCUST_VOLUME = @"0";
                NSString *strPRICING_UNIT = @"0";
                NSString *strCharge_Type = @"";
                NSString *strRelease_New = @"Y";
                //Comments
                if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/C"])
                {
                    detail_lines++;
                    line_num++;
                    
                    strStockCodenew = @"/C";
                    
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                    self.strRefreshPrice = @"";
                    
                    //                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected )
                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ||  [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        
                    {
                        //Insert
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT OR REPLACE  INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        
                        
                    }
                    else{
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                    }
                    
                    
                }
                //Freight
                else if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"])
                {
                    detail_lines++;
                    line_num++;
                    
                    strStockCodenew = @"/F";
                    
                    strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strExtension = [NSString stringWithFormat:@"%f",freightValue];
                    NSString* strfreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
                    totalTax += [strfreightTax floatValue];
                    
                    
                    //                    strPrice = [dictProductDetails objectForKey:@"Price"];
                    //                    strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                    //                    strTax = [dictProductDetails objectForKey:@"Tax"];
                    //                    totalTax += [strTax floatValue];
                    
                    strAltQuantity = @"0.00";
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    
                    strCUST_PRICE = strPrice;
                    strCUST_ORD_QTY  = @"1";
                    strCUST_SHIP_QTY = @"1";
                    strCUST_VOLUME = @"1";
                    strPRICING_UNIT = @"1";
                    strCharge_Type = @"N";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/F";
                    strDissection_Cos = @"";
                    self.strRefreshPrice = @"";
                    
                    //                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                        
                    {
                        //Insert
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?, ?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            break;
                        }
                        
                        
                    }
                    else{
                        NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                        BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strfreightTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                    }
                    
                }
                //Product
                else{
                    
                    
                    if ([dictProductDetails objectForKey:@"Item"]) {
                        strStockCodenew = [dictProductDetails objectForKey:@"Item"];
                    }
                    else if ([dictProductDetails objectForKey:@"StockCode"]){
                        strStockCodenew = [dictProductDetails objectForKey:@"StockCode"];
                    }
                    
                    if ([dictProductDetails objectForKey:@"Price"]) {
                        strPrice = [dictProductDetails objectForKey:@"Price"];
                    }
                    
                    if ([dictProductDetails objectForKey:@"ExtnPrice"])
                    {
                        //--check for price is greater than zero
                        NSString *strExt = [dictProductDetails objectForKey:@"ExtnPrice"];
                        if(strExt.integerValue > 0)
                        {
                            detail_lines++;
                            line_num++;
                            
                            strDissection = [dictProductDetails objectForKey:@"Dissection"];
                            strDissection_Cos = [dictProductDetails objectForKey:@"Dissection_Cos"];
                            strWeight = [dictProductDetails objectForKey:@"Weight"];
                            strVolume = [dictProductDetails objectForKey:@"Volume"];
                            strCost = [dictProductDetails objectForKey:@"AverageCost"];
                            strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                            
                            strQuantityOrdered = [dictProductDetails objectForKey:@"QuantityOrdered"];
                            totalQuantity += [strQuantityOrdered intValue];
                            
                            strWeight = [dictProductDetails objectForKey:@"Weight"];
                            
                            strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                            
                            totalWeight += [strWeight floatValue];
                            
                            strVolume = [dictProductDetails objectForKey:@"Volume"];
                            
                            strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                            
                            totalVolume += [strVolume floatValue];
                            
                            strGross = [dictProductDetails objectForKey:@"Gross"];
                            strTax = [dictProductDetails objectForKey:@"Tax"];
                            
                            totalTax += [strTax floatValue];
                            
                            self.strDecimalPlaces = @"2";
                            
                            strCUST_PRICE = strPrice;
                            strCUST_ORD_QTY  = strQuantityOrdered;
                            strCUST_SHIP_QTY = strQuantityOrdered;
                            strCUST_VOLUME = @"1";
                            strPRICING_UNIT = @"1";
                            strCharge_Type = @"";
                            strRelease_New = @"Y";
                            self.strRefreshPrice = @"N";
                            self.strConvFactor = @"1.0000";
                            
                            /*
                             if ([strQuantityOrdered intValue] > 0) {
                             strCustVolume = @"1";
                             strPricingUnit = @"1";
                             }
                             else{
                             strCustVolume = @"0";
                             strPricingUnit = @"0";
                             }
                             */
                            
                            //                        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                            if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                            {
                                //Insert
                                NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                                BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                                
                                if (!y1)
                                {
                                    isSomethingWrongHappened = TRUE;
                                    
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    break;
                                }
                                
                                
                            }
                            else{
                                NSString *strLines = [NSString stringWithFormat:@"%d",line_num];
                                
                                BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, `created_date`, `modified_date`,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strChargetype,strStockCodenew,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,strUploadDate,strUploadDate,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                                
                                if (!y1)
                                {
                                    isSomethingWrongHappened = TRUE;
                                    
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                
                            }
                        }
                    }
                    else if ([dictProductDetails objectForKey:@"isNewProduct"]) {
                        /*
                         strProdClass = [dict objectForKey:@"ProdClass"];
                         strProdGroup = [dict objectForKey:@"ProdGroup"];
                         strSubstitute = @"Y";
                         strBrand = @"..";
                         strUserSortSeq = @"..";
                         
                         y1 = [db executeUpdate:@"INSERT INTO `smcuspro_offline` (CUSTOMER_CODE, STOCK_CODE, DATE_LAST_PURCH, QTY_LAST_SOLD, VALUE_LAST_SALE, SALES_00, PRODUCT_CLASS, SUBSTITUTE_YN, DESCRIPTION, PROD_GROUP, BRAND, EDIT_STATUS, USER_SORT_SEQ,created_date,modified_date) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?)",strDebtor,strStockCode,createdDate, strQuantityOrdered, strPrice,strPrice,strProdClass,strSubstitute,strDescription,strProdGroup,strBrand,strEditStatus,strUserSortSeq,createdDate,createdDate];
                         
                         
                         
                         if (!y1)
                         {
                         isSomethingWrongHappened = TRUE;
                         
                         NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                         
                         @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                         
                         break;
                         }
                         
                         
                         */
                        
                    }
                    
                }
                
            }
            
            self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
            self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
            self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
            self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
            
            
            BOOL x;
            //            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected || ! [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                
            {
                
                x = [db executeUpdate:@"UPDATE `soheader` SET TOTL_CARTON_QTY = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ? WHERE ORDER_NO = ?",strTotalCartonQty,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strOrderNum];
                self.strSuccessMessage = [NSString stringWithFormat:@"Order Successfully Added!\nYou can view the order under Offline Order --> Profile Order section"];
                // Store the view is profile order.
                [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else{
                
                x = [db executeUpdate:@"UPDATE `soheader_offline` SET TOTL_CARTON_QTY = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ? WHERE ORDER_NO = ?",strTotalCartonQty,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strOrderNum];
                
                self.strSuccessMessage = [NSString stringWithFormat:@"Order Successfully Added!\nYou can view the order under Offline Order --> Profile Order section"];
                // Store the view is profile order.
                [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            if (!x)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            *rollback = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                //Prepare for new Order
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                alert.tag = TAG_50;
                [alert show];
                
                [arrProducts removeAllObjects];
                
                FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [db open];
                [self callViewQuoteDetailsDetailsFromDB:db];
                [db close];
                [_tblDetails reloadData];
                [_tblHeader reloadData];
                // Store the view is profile order.
                [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            });
            
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                NSLog(@"%@",strErrorMessage);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                // Store the view is profile order.
                [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            });
            
            
        }
        
    }];
}


#pragma mark - WS call

-(void)callWSGetWarehouse:(NSString *)branch
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    strWebserviceType = @"WS_CUST_BRANCH";
    
    NSString *strURL = [NSString stringWithFormat:@"%@get_sowarehouse.php?branch=%@",WSURL,branch];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


-(void)callWSGetNewOrderNumber{
    
    strWebserviceType = @"WS_GET_ORDER_NUMBER";
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:GetOrderNo soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "</wws:GetOrderNo>"];
    
    //NSLog(@"soapXMLStr %@",soapXMLStr);
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#GetOrderNo" onView:self.view];
    
}

-(void)callWSGetProductsInDetails{
    strWebserviceType = @"WS_GET_PRODUCT_IN_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"customer=%@",strDebtor];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,PROFILE_PRODUCT_LIST_WS,parameters];
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}




-(void)callWSSaveProfileOrder
{
    @try
    {
        strWebserviceType = @"WS_PROFILE_ORDER_SAVE";
        
        float totalTax = 0;
        float totalWeight = 0;
        float totalVolume = 0;
        int totalQuantity = 0;
        
        int detail_lines = 0;
        
        NSString *strNewProduct = @"0";
        
        NSString *strItem = [[NSString alloc] init];
        
        int line_num = 0;
        //NSString *strItemOfNewProducts = nil;
        
        bool Flag = NO;
        NSString *strINC = @"N";
        
        //--Check for Product with Price
        for (NSDictionary *dict in arrProducts) {
            
            if([dict objectForKey:@"PriceChnaged"])
            {
                if(Flag == NO)
                {
                    if([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
                    {
                        Flag = YES;
                        strINC = [dict objectForKey:@"PriceChnaged"];
                    }
                    
                }
            }
            
            //--Send Y for temporary
            // strINC = @"Y";
            
            NSString *strStockCode1 = nil;
            NSString *strPrice = @"0";
            NSString *strDescription = nil;
            NSString *strExtension = @"0";
            NSString *strDissection = @"0";
            NSString *strDissection_Cos = @"0";
            NSString *strWeight = @"0";
            NSString *strVolume = @"0";
            NSString *strCost = @"0";
            NSString *strQuantityOrdered = @"0";
            NSString *strGross = @"0";
            NSString *strTax = @"0";
            
            //NSString *strCustVolume = nil;
            //NSString *strPricingUnit = nil;
            
            NSString *strProdClass = nil;
            NSString *strProdGroup = nil;
            
            NSString *strAltQuantity = @"0";
            NSString *strCUST_PRICE = @"0";
            NSString *strCUST_ORD_QTY  = @"0";
            NSString *strCUST_SHIP_QTY = @"0";
            NSString *strCUST_VOLUME = @"0";
            NSString *strPRICING_UNIT = @"0";
            NSString *strCharge_Type = @"";
            NSString *strRelease_New = @"";
            //Comments
            if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]){
                
                strDescription = [[dict objectForKey:@"Description"] trimSpaces];
                
                int NumberOflines = strDescription.length/30;
                
                if(NumberOflines%30 > 0)
                {
                    NumberOflines= NumberOflines +1;
                    
                }
                
                if(NumberOflines == 0)
                {
                    int z = 0;
                    NSRange rang = NSMakeRange(z, 30);
                    
                    detail_lines++;
                    line_num++;
                    
                    strStockCode1 = @"/C";
                    
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                    self.strRefreshPrice = @"";
                    NSString *strComemnt;
                    
                    
                    if(strDescription.length > z+30)
                    {
                        strComemnt = [strDescription substringWithRange:rang];
                        NSLog(@"s:::::::%@",strComemnt);
                    }
                    
                    else
                    {
                        
                        int r = strDescription.length-z;
                        NSRange rang = NSMakeRange(z, r);
                        strComemnt = [strDescription substringWithRange:rang ];
                        NSLog(@"s:::::::%@",strComemnt);
                    }
                    
                    
                    if ([strPriceCode isKindOfClass:[NSNumber class]])
                    {
                        id priceval = strPriceCode;
                        strPriceCode = [priceval stringValue];
                    }
                    
                    //==============Check fro HTML TAGS //==============
                    strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                    strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                    //==============Check fro HTML TAGS //==============
                    
                    strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode1 trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                    
                    
                    
                }
                for (int k = 0; k < NumberOflines; k++)
                {
                    int z = k*30;
                    NSRange rang = NSMakeRange(z, 30);
                    
                    detail_lines++;
                    line_num++;
                    
                    strStockCode1 = @"/C";
                    
                    strQuantityOrdered = @"1.00";
                    self.strConvFactor = @"0.0000";
                    strRelease_New = @"Y";
                    
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                    self.strRefreshPrice = @"";
                    NSString *strComemnt;
                    
                    
                    if(strDescription.length > z+30)
                    {
                        strComemnt = [strDescription substringWithRange:rang];
                        NSLog(@"s:::::::%@",strComemnt);
                    }
                    else
                    {
                        
                        int r = strDescription.length-z;
                        NSRange rang = NSMakeRange(z, r);
                        strComemnt = [strDescription substringWithRange:rang ];
                        NSLog(@"s:::::::%@",strComemnt);
                    }
                    
                    
                    if ([strPriceCode isKindOfClass:[NSNumber class]])
                    {
                        id priceval = strPriceCode;
                        strPriceCode = [priceval stringValue];
                    }
                    
                    //==============Check fro HTML TAGS //==============
                    strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                    strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                    //==============Check fro HTML TAGS //==============
                    
                    strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],strComemnt ,[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                    
                }
                
            }
            //Freight
            else if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
                
                detail_lines++;
                line_num++;
                
                
                
                strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
                strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
                strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
                strExtension = [NSString stringWithFormat:@"%f",freightValue];
                NSString* strfreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
                totalTax += [strfreightTax floatValue];
                
                strStockCode = @"/F";
                strDescription = [dict objectForKey:@"Description"];
                
                
                
                
                strAltQuantity = @"0.00";
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                strCUST_PRICE = strPrice;
                strCUST_ORD_QTY  = @"1";
                strCUST_SHIP_QTY = @"1";
                strCUST_VOLUME = @"1";
                strPRICING_UNIT = @"1";
                strCharge_Type = @"N";
                strRelease_New = @"Y";
                strDissection = @"/F";
                strDissection_Cos = @"";
                self.strRefreshPrice = @"";
                
                if([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id pricecode = strPriceCode;
                    
                    strPriceCode = [NSString stringWithFormat:@"%@",[pricecode stringValue]];
                }
                //==============Check fro HTML TAGS //==============
                strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
                strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],[strDescription trimSpaces],[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
            //Product
            else{
                
                strStockCode = [dict objectForKey:@"StockCode"];
                strDescription = [[dict objectForKey:@"Description"] trimSpaces];
                
                //==============Check fro HTML TAGS //==============
                strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
                strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============
                
                
                if ([dict objectForKey:@"Price"]) {
                    strPrice = [dict objectForKey:@"Price"];
                }
                
                if ([dict objectForKey:@"isNewProduct"]) {
                    strNewProduct = @"1";
                }
                else{
                    strNewProduct = @"0";
                }
                
                //For Products with calculations
                if ([[dict objectForKey:@"ExtnPrice"] floatValue] > 0) {
                    
                    detail_lines++;
                    line_num++;
                    
                    
                    
                    strDissection = [[dict objectForKey:@"Dissection"] trimSpaces];
                    strDissection_Cos = [[dict objectForKey:@"Dissection_Cos"] trimSpaces];
                    strWeight = [dict objectForKey:@"Weight"];
                    strVolume = [dict objectForKey:@"Volume"];
                    
                    strCost = [dict objectForKey:@"AverageCost"];
                    
                    strExtension = [dict objectForKey:@"ExtnPrice"];
                    
                    strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                    totalQuantity += [strQuantityOrdered intValue];
                    
                    strAltQuantity = strQuantityOrdered;
                    
                    strWeight = [dict objectForKey:@"Weight"];
                    
                    strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                    
                    totalWeight += [strWeight floatValue];
                    
                    strVolume = [dict objectForKey:@"Volume"];
                    
                    strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                    
                    totalVolume += [strVolume floatValue];
                    
                    strGross = [dict objectForKey:@"Gross"];
                    strTax = [dict objectForKey:@"Tax"];
                    
                    totalTax += [strTax floatValue];
                    
                    
                    self.strDecimalPlaces = @"2";
                    
                    strCUST_PRICE = strPrice;
                    strCUST_ORD_QTY  = strQuantityOrdered;
                    strCUST_SHIP_QTY = strQuantityOrdered;
                    strCUST_VOLUME = @"1";
                    strPRICING_UNIT = @"1";
                    strCharge_Type = @"";
                    strRelease_New = @"Y";
                    self.strRefreshPrice = @"N";
                    self.strConvFactor = @"1.0000";
                    
                    if([strPriceCode isKindOfClass:[NSNumber class]])
                    {
                        id pricecode = strPriceCode;
                        
                        strPriceCode = [NSString stringWithFormat:@"%@",[pricecode stringValue]];
                    }
                    
                    //                    //==============Check fro HTML TAGS //==============
                    //                    strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
                    //                    strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                    //                    //==============Check fro HTML TAGS //==============
                    //
                    strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[NSNumber numberWithInt:line_num],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],[strDescription trimSpaces],[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                    
                    
                    
                    /*
                     strItem = [strItem stringByAppendingFormat:@"<product><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><ORIG_ORD_QTY>%@</ORIG_ORD_QTY><ORD_QTY>%@</ORD_QTY><BO_QTY>%@</BO_QTY><CONV_FACTOR>%@</CONV_FACTOR><REFRESH_PRICE>%@</REFRESH_PRICE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><COST>%@</COST><ORIG_COST>%@</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><CREATE_OPERATOR>%@</CREATE_OPERATOR><EDIT_STATUS>%@</EDIT_STATUS><DECIMAL_PLACES>%@</DECIMAL_PLACES><LINE_NO>%@</LINE_NO><isNewProduct>%@</isNewProduct><RELEASE_New>%@</RELEASE_New><CHARGE_TYPE>%@</CHARGE_TYPE><CUST_PRICE>%@</CUST_PRICE><CUST_ORD_QTY>%@</CUST_ORD_QTY><CUST_SHIP_QTY>%@</CUST_SHIP_QTY><CUST_VOLUME>%@</CUST_VOLUME><PRICING_UNIT>%@</PRICING_UNIT></product>",strStockCode,strDescription,strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strEditStatus,strDecimalPlaces,[NSNumber numberWithInt:line_num],strNewProduct,strRelease_New,strCharge_Type,strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT];
                     */
                    
                    
                    if ([dict objectForKey:@"isNewProduct"]) {
                        
                        strProdClass = [dict objectForKey:@"ProdClass"];
                        strProdGroup = [dict objectForKey:@"ProdGroup"];
                        
                    }
                    
                    
                }
                else if ([dict objectForKey:@"isNewProduct"]) {
                    
                    strProdClass = [dict objectForKey:@"ProdClass"];
                    strProdGroup = [dict objectForKey:@"ProdGroup"];
                }
                
            }
        }
        
        self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
        self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
        self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
        self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
        
        if ([isCashPickUpOn isEqualToString:@"N"]) {
            strCashPickUp = @"0.00";
            strChequeAvailable = @"N";
        }
        
        
        //QUANTITY = ALT_QTY = ORIG_ORD_QTY = ORD_QTY = BO_QTY
        
        
        self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
        self.strTotalLines = [NSString stringWithFormat:@"%d",detail_lines];
        
        
        NSString *strCheckStockPickUp = [NSString new];
        if(isStorePickUpOn)
        {
            strCheckStockPickUp = [dictHeaderDetails objectForKey:@"StockPickUp"];
        }
        else
        {
            strCheckStockPickUp = @"N";
        }
        
        
        NSString *strCheckPickup = [NSString new];
        if(isStockPickUpOn)
        {
            strCheckPickup = [dictHeaderDetails objectForKey:@"PickUpForm"];
        }
        else
        {
            strCheckPickup = @"N";
        }
        
        NSString *iso8601String = @"";
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [NSLocale currentLocale];
        [dateFormatter setLocale:enUSPOSIXLocale];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        NSDate *now = [NSDate date];
        iso8601String = [dateFormatter stringFromDate:now];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSDate *dateRaised = [dateFormatter dateFromString:self.strDateRaised];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        // if(dateRaised)
        self.strDateRaised = [dateFormatter stringFromDate:dateRaised];
        
        
        if(self.strDateRaised == nil)
        {
            self.strDateRaised = iso8601String;
        }
        NSString *str = @"N";
        if([dictHeaderDetails objectForKey:@"StockPickUp"])
        {
            str = [dictHeaderDetails objectForKey:@"StockPickUp"];
        }
        
        //--Delivery
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        if(strCopyDelDate)
        {
            NSDate *dateDel = [dateFormatter dateFromString:strCopyDelDate];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
            if(dateDel)
                strCopyDelDate = [dateFormatter stringFromDate:dateDel];
            
        }
        else
        {
            strCopyDelDate = [self.strDelDate copy];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *dateDel = [dateFormatter dateFromString:strCopyDelDate];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
            if(dateDel)
                strCopyDelDate = [dateFormatter stringFromDate:dateDel];
        }
        if(strItem.length <=0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        NSString *strDelSuburblimit = [[strDelSubUrb trimSpaces]copy];
        
        if(strDelSuburblimit.length > 15)
        {
            strDelSuburblimit = [strDelSuburblimit substringToIndex:14];
        }
        
        //==============Check fro HTML TAGS //==============
        
        //==============Check fro HTML TAGS //==============
        strDelAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress1];
        strDelAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress2];
        strDelAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress3];
        strDelInst2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
        strDelInst1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
        strDelSuburblimit = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelSuburblimit];
        strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
        
        strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
        //==============Check fro HTML TAGS //==============
        
        
        NSString *soapXMLStr = [NSString stringWithFormat:
                                @"<SalesOrder>"
                                "<HEADER>"
                                "<OVERWRITE_SO>N</OVERWRITE_SO>"
                                "<CANCEL_SO>N</CANCEL_SO>"
                                "<INC_PRICE>%@</INC_PRICE>"
                                "<INC_WAREHOUSE>Y</INC_WAREHOUSE>"
                                "<DELIVERY_ADDRESS_TYPE>S</DELIVERY_ADDRESS_TYPE>"
                                "<DELIV_NUM>0</DELIV_NUM>"
                                "<SUB_SET_NUM/>"
                                "<SUPPLIED_LINE_NUMBERS>N</SUPPLIED_LINE_NUMBERS>"
                                "<ORDER_NO />"
                                "<WEBORDERID>0</WEBORDERID>"
                                "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                                "<CASH_COLLECT>%@</CASH_COLLECT>"
                                "<CASH_AMOUNT>%@</CASH_AMOUNT>"
                                "<CASH_REP_PICKUP>%@</CASH_REP_PICKUP>"
                                "<STOCK_RETURNS>%@</STOCK_RETURNS>"
                                "<DEBTOR>%@</DEBTOR>"
                                "<BRANCH>%@</BRANCH>"
                                "<SALES_BRANCH>%@</SALES_BRANCH>"
                                "<DEL_NAME>%@</DEL_NAME>"
                                "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                                "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                                "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                                "<DEL_SUBURB>%@</DEL_SUBURB>"
                                "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                                "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                                "<DEL_INST1>%@</DEL_INST1>"
                                "<DEL_INST2>%@</DEL_INST2>"
                                "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                                "<CHARGE_RATE>%@</CHARGE_RATE>"
                                "<DATE_RAISED>%@</DATE_RAISED>"
                                "<DEL_DATE>%@</DEL_DATE>"
                                "<CUST_ORDER/>"
                                "<SALESMAN>%@</SALESMAN>"
                                "<WAREHOUSE>%@</WAREHOUSE>"
                                "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                                "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                                "<DISC_PERCENT>0.00</DISC_PERCENT>"
                                "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                                "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                                "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                                "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                                "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                                "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                                "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                                "</HEADER>"
                                "%@"
                                "</SalesOrder>",strINC,[iso8601String trimSpaces],self.isCashPickUpOn,self.strCashPickUp,self.strChequeAvailable,str,[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strDelAddress1 trimSpaces],[strDelAddress2 trimSpaces],[strDelAddress3 trimSpaces],[strDelSuburblimit trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],strChargeRate ,[strDateRaised trimSpaces],strCopyDelDate,[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces ],strItem ];
        
        
        /*
         NSLog(@"soapXMLStr %@",soapXMLStr);
         spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         
         [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
         [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#InsertProfOrder" onView:self.view];
         */
        
        [spinner removeFromSuperview];
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/profordentry/insert_modules.php"];
        NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        /*
         soapXMLStr = [soapXMLStr stringByReplacingOccurrencesOfString:@"&" withString:@""];
         
         
         soapXMLStr = [soapXMLStr stringByReplacingOccurrencesOfString:@"'" withString:@" "];
         
         */
        
        ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
        [requestnew setDelegate:self];
        [requestnew setRequestMethod:@"POST"];
        [requestnew setPostValue:soapXMLStr forKey:@"xmlData"];
        NSLog(@"soapXMLStr %@",soapXMLStr);
        [requestnew setPostValue:@"1" forKey:@"transactionType"];
        [requestnew startAsynchronous];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error::%@",exception.description);
    }
}

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    [spinner removeFromSuperview];
    
    @try {
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
        NSLog(@"dictResponse:::%@",dictResponse);
        NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
        NSLog(@"text:::%@",text);
        
        if([[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"])
        {
            //      [NSString stringWithFormat:@"Quote generated. Quote number %@",[[[[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]
            
            if([[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"])
            {
                [spinner removeFromSuperview];
                [self prepareForNewOrder];
                UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Profile Order Created: Order Number:%@",[[[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aler show];
                aler = nil;
            }
            
            else
            {
                [spinner removeFromSuperview];
                UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Something went wrong. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aler show];
                aler = nil;
            }
        }
        else if([text containsString:@"true"])
        {
            [spinner removeFromSuperview];
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                NSLog(@"isModified  %@",isModified);
                
                if([isModified isEqualToString:@"0"])
                {
                    [self callAddItemInSmcuspro:db];
                }
                else if ([isModified isEqualToString:@"2"])
                {
                    [self callRemoveItemFromSmcuspro:db];
                }
                
            }];
            
        }
        else
        {
            NSString *strMessage = @"";
            if([isModified isEqualToString:@"0"])
            {
                strMessage = @"Unable to add this product";
            }
            else
            {
                strMessage = @"Unable to delete this product";
            }
            [spinner removeFromSuperview];
            UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [aler show];
            aler = nil;
        }
    }
    @catch (NSException *exception) {
        [spinner removeFromSuperview];
        NSLog(@"error::%@",exception.description);
    }
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    [spinner removeFromSuperview];
    NSLog(@"falied:::::");
    
}

#pragma mark - Alert Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == TAG_50) {
        [self prepareForNewOrder];
    }
    
    if (alertView.tag == TAG_1300) {
        [segControl setSelectedIndex:0];
    }
    
    //Error in connection alert
    if (alertView.tag == TAG_200) {
        if (buttonIndex == 1) {
            [self callInsertProfileOrderToDB];
        }
    }
    
    if (alertView.tag == TAG_400) {
        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
        {
            [self callInsertProfileOrderToDB];
            
        }
        else{
            [self callWSSaveProfileOrder];
        }
        
    }
    
    if(alertView.tag == 911)
    {
        if (buttonIndex == 1)
        {
            alertFreightValue = [[[alertView textFieldAtIndex:0] text] intValue];
            
            if(alertFreightValue > 0)
            {
                [self calcFreight];
            }
        }
    }
}


#pragma mark - Custom Methods

- (void)hideOrDisableButtons{
    btnLoadProfile.enabled = FALSE;
    btnSaveDetails.enabled = FALSE;
    btnAddProducts.enabled = FALSE;
    btnFinalise.enabled = FALSE;
    btnAddMenu.enabled = FALSE;
}
- (void)showOrEnableButtons{
    btnLoadProfile.enabled = TRUE;
    btnSaveDetails.enabled = TRUE;
    btnAddProducts.enabled = TRUE;
    btnFinalise.enabled = TRUE;
    btnAddMenu.enabled = TRUE;
}

- (void)showPointingAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.autoreverses = YES;
    animation.duration = 0.35;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    [layerPointingImage addAnimation:animation forKey:@"pulseAnimation"];
    [imgArrowDownwards.layer addSublayer:layerPointingImage];
}

- (void)setDefaults{
    self.isCashPickUpOn = @"N";
    self.strCashPickUp = @"0";
    self.strChequeAvailable = @"N";
    self.strStockPickup = @"N";
    self.strPickupForm = @"N";
    self.strExchangeRate = @"1.000000";
    self.strActive = @"N";
    self.strEditStatus = @"11";
    self.strStatus = STATUS;
    self.strRefreshPrice = @"N";
    self.strConvFactor = @"1.0000";
    self.strDecimalPlaces = @"2";
    self.strTotalCartonQty = self.strBOQty = self.strOrigCost = @"0.0000";
    self.strTotalLines = self.strTotalTax = self.strTotalVolume = self.strTotalWeight = @"0";
    
    isDeliveryRunSlected = NO;
    segControl.enabled = NO;
    isRecordsExist = FALSE;
    totalCost = 0;
    totalLines = 0;
    lblTotalLines.text =[NSString stringWithFormat:@"%d",totalLines];
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    
    self.cntProfileOrders = 0;
}

- (void)prepareForNewOrder{
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
    [arrProducts removeAllObjects];
    [dictHeaderDetails removeAllObjects];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwHeader];
    
    [self setDefaults];
    lblBalance.text = @"";
    self.strFax = nil;
    self.strBalance = nil;
    self.strEmail = nil;
    self.strPhone = nil;
    self.strCreditLimit = nil;
    self.strDelName = nil;
    self.strDelAddress1 = nil;
    self.strDelAddress2 = nil;
    self.strDelAddress3 = nil;
    self.strDelSubUrb = nil;
    self.strDelPostCode = nil;
    self.strDelCountry = nil;
    self.strDeliveryRun = nil;
    self.strDebtor = nil;
    self.strDelInst1 = nil;
    self.strDelInst2 = nil;
    self.strCustOrder = nil;
    self.strContact = nil;
    self.strDelDate = nil;
    self.strAddress1 = nil;
    self.strAddress2 = nil;
    self.strAddress3 = nil;
    self.strSubUrb = nil;
    self.strAllowPartial = nil;
    self.strBranch = nil;
    self.strCarrier = nil;
    self.strChargeRate = nil;
    self.strChargetype = nil;
    self.strCountry = nil;
    self.strDirect = nil;
    self.strDropSequence = nil;
    self.strExchangeCode = nil;
    self.strExportDebtor = nil;
    self.strHeld = nil;
    self.strName = nil;
    self.strNumboxes = nil;
    self.strOrderDate = nil;
    self.strPeriodRaised = nil;
    self.strPostCode = nil;
    self.strPriceCode = nil;
    self.strSalesBranch = nil;
    self.strSalesman = nil;
    self.strTaxExemption1 = nil;
    self.strTaxExemption2 = nil;
    self.strTaxExemption3 = nil;
    self.strTaxExemption4 = nil;
    self.strTaxExemption5 = nil;
    self.strTaxExemption6 = nil;
    self.strTradingTerms = nil;
    self.strYearRaised = nil;
    self.strUploadDate = nil;
    self.strSuccessMessage = nil;
    self.strCommenttxt = nil;
    self.strCommentDateCreated = nil;
    self.strCommentFollowDate = nil;
    
    //Change the selection
    [segControl setSelectedIndex:0];
    
    [_tblHeader reloadData];
}


-(void)doAfterDataFetch{
    if (isRecordsExist) {
        
        [_tblDetails reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        recordNumber += ROWS_PER_PAGE_FOR_PROFILE;
        
    }
    else{
        
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
    
}

- (void)prepareView
{
    if ([arrProducts count] > cntProfileOrders) {
        //Show delete button
        btnDelete.hidden = YES;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
    }
    else{
        btnDelete.hidden = YES;// btnDelete.hidden = YES;
        
    }
    
    if ([arrProducts count]) {
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [vwContentSuperview addSubview:vwDetails];
        
    }
    else{
        [vwContentSuperview addSubview:vwNoProducts];
        [self showPointingAnimation];
    }
    
}

-(void)calcFreight{
    
    // float freightValue = FREIGHT_VALUE + [self calcFreightTax];
    freightValue = alertFreightValue + [self calcFreightTax];
    
    totalCost += freightValue;
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    
    totalLines =totalLines + 1;
    lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
    
    NSString *strFreightValue = [NSString stringWithFormat:@"%f",freightValue];
    NSString *strFreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:@"0" forKey:@"Available"];
    [dict setValue:@"Freight" forKey:@"Description"];
    [dict setValue:@"0" forKey:@"Location"];
    [dict setValue:[NSString stringWithFormat:@"%f",FREIGHT_VALUE] forKey:@"Price"];
    [dict setValue:strFreightTax forKey:@"Tax"];
    [dict setValue:strFreightValue forKey:@"ExtnPrice"];
    [dict setValue:@"0" forKey:@"ScmRecum"];
    [dict setValue:@"-" forKey:@"StockCode"];
    [dict setValue:@"0" forKey:@"Warehouse"];
    [dict setValue:@"0" forKey:@"CheckFlag"];
    [dict setValue:@"/F" forKey:@"Item"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
    self.isFrightAddedOnLastLine=YES;
    
    
    //[_tblDetails reloadData];
    //[self repositionLoadMoreFooterView];
}

-(float)calcFreightTax
{
    //return (FREIGHT_VALUE*FREIGHT_TAX)/100;
    return (alertFreightValue*FREIGHT_TAX)/100;
}

-(void)prepareHeaderData{
    
    //Debtor's Info
    //NSLog(@"%@",dictHeaderDetails);
    self.strCreditLimit = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CreditLimit"]];
    self.strDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"];
    self.strPhone = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Phone"];
    self.strFax = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Fax"];
    self.strBalance = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Balance"]];
    self.strAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address1"];
    self.strAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address2"];
    self.strAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address3"];
    self.strSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Suburb"];
    self.strAllowPartial = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"AllowPartial"];
    self.strBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Branch"];
    self.strCarrier = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Carrier"];
    self.strContact = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Contact"];
    self.strChargeRate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ChargeRate"];
    self.strChargetype = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Chargetype"];
    self.strCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Country"];
    self.strDirect = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Direct"];
    self.strDropSequence = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DropSequence"];
    self.strExchangeCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExchangeCode"];
    self.strExportDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExportDebtor"];
    self.strHeld = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Held"];
    self.strName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Name"];
    self.strNumboxes = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Numboxes"];
    self.strOrderDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderDate"];
    self.strPeriodRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PeriodRaised"];
    self.strPriceCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PriceCode"];
    self.strSalesBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"SalesBranch"];
    self.strSalesman = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Salesman"];
    self.strTaxExemption1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption1"];
    self.strTaxExemption2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption2"];
    self.strTaxExemption3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption3"];
    self.strTaxExemption4 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption4"];
    self.strTaxExemption5 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption5"];
    self.strTaxExemption6 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption6"];
    self.strTradingTerms = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TradingTerms"];
    self.strYearRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"YearRaised"];
    self.strDelDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryDate"];
    self.strDelAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress1"];
    self.strDelAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress2"];
    self.strDelAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress3"];
    //    self.strDelName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelName"];
    self.strDelName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Name"];
    
    self.strDelSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelSuburb"];
    self.strCustCatagory=[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Cust_Category"];
    self.strDelPostCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelPostCode"];
    self.strDelCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelCountry"];
    
    self.strDelInst1 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"]) {
        self.strDelInst1 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"];
    }
    
    self.strDelInst2 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"]) {
        self.strDelInst2 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"];
    }
    
    self.strCustOrder = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"]) {
        self.strCustOrder = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"];
    }
    
    self.strCommenttxt = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Commenttxt"];
    self.strCommentDateCreated = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"DateCreated"];
    self.strCommentFollowDate = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"FollowDate"];
    
    NSLog(@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]);
    /*
     if (isEditable) {
     if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]){
     isStockPickUpOn = @"Y";
     self.strStockPickup = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]];
     }
     else{
     self.strStockPickup = @"N";
     isStockPickUpOn = @"N";
     }
     }
     */
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"]) {
        self.strDeliveryRun = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"];
        isDeliveryRunSlected = YES;
    }
    else{
        isDeliveryRunSlected = NO;
    }
    
    /*
     if (isEditable) {
     if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashCollect"] isEqualToString:@"1"]) {
     isCashPickUpOn = @"Y";
     
     self.strCashPickUp = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashAmount"];
     }
     else{
     isCashPickUpOn = @"N";
     self.strCashPickUp = @"0";
     }
     
     }
     */
    
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
    
}


#pragma mark - PopOverDelegates
//Add Products
- (void)showProductList{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (void)deleteProductList{
    
    if ([_tblDetails isEditing]) {
        [self showOrEnableButtons];
        
        //        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        //        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        //        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        //        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        //
        
        [_tblDetails setEditing:NO animated:YES];
        btnDelete.hidden = YES;
        
    }
    else{
        btnDelete.hidden = NO;
        [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [_tblDetails setEditing:YES animated:YES];
    }
    
    //[_tblDetails reloadData];
}


-(void)showSpecials
{
    NSLog(@"Specials Clicked");
    [self btnShowSpecialsNEW:nil];
}

-(void)dismissPopOver{
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    [popoverController dismissPopoverAnimated:YES];
    
    [self viewWillAppear:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [btnAddMenu setImage:[UIImage imageNamed:@"red_plus_down.png"] forState:UIControlStateNormal];
    [[self.view findFirstResponder] resignFirstResponder];
}


#pragma mark - Handle Notification

- (void)reloadTableViewData:(NSNotification *)notification {
    
    isSearch = NO;
    
    NSMutableDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
    
    //NSLog(@"dictSelectedProduct : %@",dictSelectedProduct);
    
    
    //    __block BOOL isScrollToBottom = YES;
    __block BOOL isScrollToBottom = YES;
    
    //Comments
    if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
    }
    //Freight
    else if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/F"]) {
        [dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
        // Avoid double up
        if ([[arrProducts valueForKey:@"StockCode"] containsObject:[dictSelectedProduct objectForKey:@"StockCode"]])
        {
            //            NSLog(@"OBJECT EXISTS");
        }
        else{
            [arrProducts insertObject:dictSelectedProduct atIndex:0];
        }
    }
    else{
        
        BOOL isProductExist = FALSE;
        
        if ([[arrProducts valueForKey:@"StockCode"] containsObject:[dictSelectedProduct objectForKey:@"StockCode"]]) {
            isProductExist = YES;
        }
        else{
            isProductExist = NO;
        }
        NSLog(@"%@",isProductExist?@"YES":@"NO");
        
        if (isProductExist) {
            NSString *strMessage = [NSString stringWithFormat:@"\"%@\"\n is already present in the Profile!",[dictSelectedProduct objectForKey:@"Description"]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            strMessage = nil;
        }
        else{
            [dictSelectedProduct setValue:strDebtor forKey:@"CUSTOMER_CODE"];
            [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
            
            __block BOOL toRemove = NO;
            if ([arrProducts count]) {
                
                [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if ([[obj objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]] || [[obj objectForKey:@"Item"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]]) {
                        
                        if ([[obj objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
                            
                        }
                        else{
                            if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
                                [arrProducts removeObjectAtIndex:idx];
                            }
                            
                        }
                        isScrollToBottom = NO;
                        toRemove = YES;
                        *stop = YES;
                    }
                    else{
                        toRemove = NO;
                    }
                }];
                
            }
            else{
                toRemove = NO;
                
            }
            
            if (!toRemove)
            {
                // Avoid double up
                if ([[arrProducts valueForKey:@"StockCode"] containsObject:[dictSelectedProduct objectForKey:@"StockCode"]])
                {
                    //                    NSLog(@"OBJECT EXISTS");
                }
                else{
                    [self getFromSamasterDB:[dictSelectedProduct objectForKey:@"StockCode"]];
                    [arrProducts insertObject:dictSelectedProduct atIndex:0];
                    
                }
            }
            
        }
    }
    
    if ([arrProducts count] > cntProfileOrders) {
        //Show delete button
        btnDelete.hidden = YES;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
    }
    else{
        btnDelete.hidden = YES;//        btnDelete.hidden = YES
    }
    
    int lastIndex = [arrProducts count];
    if (lastIndex) {
        
        [self showOrEnableButtons];
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"])
                {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                    
                }
                else if  ([[obj objectForKey:@"Item"] isEqualToString:@"/F"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                    
                }
                
            }];
        }
        
        
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [_tblDetails setContentOffset:CGPointMake(0, 0)];
        [_tblDetails reloadData];
        
        
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
}

- (void)reloadHeader:(NSNotification *)notification {
    
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    [operationQueue cancelAllOperations];
    
    [segControl setSelectedIndex:0];
    
    self.dictHeaderDetails = [notification.userInfo valueForKey:@"source"];
    
    if ([[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"] ) {
        
        _lblCustName.text = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Name"];
        
        
    }
    
    [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"]) {
        
        [self prepareHeaderData];
        
        //Comments Exist
        if ([[[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Flag"] isEqualToString:@"True"]) {
            // Path to the plist (in the application bundle)
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"ProfileOrderEntryHeaderWithComments" Type:@"plist"];
            
            // Build the array from the plist
            arrHeaderLabels = [NSArray arrayWithContentsOfFile:path];
        }
        else{
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"ProfileOrderEntryHeader" Type:@"plist"];
            
            // Build the array from the plist
            arrHeaderLabels = [NSArray arrayWithContentsOfFile:path];
        }
    }
    else{
        segControl.enabled = NO;
    }
    
    [_tblHeader reloadData];
    isSearch = NO;
    [arrSearchProducts removeAllObjects];
    
    if (arrProducts.count > 0) {
        recordNumber = 0;
        [arrProducts removeAllObjects];
        lblTotalCost.text =@"0.00";
        lblTotalLines.text = @"0";
    }
    
    
    // 9th Dec, 2014
    // Show detail view
    
    if (detailViewController) {
        [self dismissPopupViewControllerWithanimationType:nil];
        detailViewController = nil;
    }
    detailViewController = [[MJDebtorViewController alloc] initWithNibName:@"MJDebtorViewController" bundle:nil];
    [self DisplayData:[[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Debtor"]];
    NSLog(@"dictCustomerDetails:::::%@",dictCustomerDetails);
    
    NSString *Over90 =[dictCustomerDetails objectForKey:@"Over90"];
    NSString *Over60 =[dictCustomerDetails objectForKey:@"Over60"];
    NSString *Over30 =[dictCustomerDetails objectForKey:@"Over30"];
    NSString *Name =[dictCustomerDetails objectForKey:@"Name"];
    NSString *ageing_period = [dictCustomerDetails objectForKey:@"ageing_period"];
    
    NSDictionary *dict = [NSMutableDictionary new];
    [dict setValue:(Name)?Name:@"" forKey:@"Name"];
    [dict setValue:(strTradingTerms)?strTradingTerms:@"" forKey:@"TradingTerms"];
    [dict setValue:(Over90)?Over90:@"" forKey:@"Over90"];
    [dict setValue:(Over60)?Over60:@"" forKey:@"Over60"];
    [dict setValue:(Over30)?Over30:@"" forKey:@"Over30"];
    [dict setValue:(ageing_period)?ageing_period:@"" forKey:@"ageing_period"];
    [dict setValue:(strBalance)?strBalance:@"" forKey:@"Balance"];
    
    detailViewController.detailDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    [self presentPopupViewController:detailViewController animationType:3];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    NSLog(@"dictResponse %@",dictResponse);
    
    [spinner removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]){
        
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
            self.strOrderNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"orderno"] objectForKey:@"text"];
            
            [_tblHeader reloadData];
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    else if ([strWebserviceType isEqualToString:@"WS_GET_PRODUCT_IN_DETAILS"]){
        
        [arrProducts removeAllObjects];
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"];
                
                // Avoid double up
                if ([[arrProducts valueForKey:@"StockCode"] containsObject:[dict objectForKey:@"StockCode"]])
                {
                    //                    NSLog(@"OBJECT EXISTS");
                }
                else
                {
                    [arrProducts addObject:dict];
                }
            }
            else{
                self.arrProducts = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"];
            }
            
            if ([arrProducts count]) {
                
                //Show delete button
                btnDelete.hidden = YES;
                _tblDetails.editing = NO;
                [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                
                [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
                [vwContentSuperview addSubview:vwDetails];
                [_tblDetails reloadData];
            }
            else{
                [vwContentSuperview addSubview:vwNoProducts];
                [self showPointingAnimation];
            }
            
        }
        //False
        else{
            
            //No Products
            [vwContentSuperview addSubview:vwNoProducts];
            
            [self showPointingAnimation];
        }
        
    }
    else if ([strWebserviceType isEqualToString:@"WS_PROFILE_ORDER_SAVE"])
    {
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            
            self.strUploadDate = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"UploadDate"] objectForKey:@"text"];
            
            self.strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            self.strOrderNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"orderno"] objectForKey:@"text"];
            
            
            dispatch_async(backgroundQueueForNewQuote, ^(void) {
                [self callInsertProfileOrderToDB];
            });
            
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertProfOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
    else if([strWebserviceType isEqualToString:@"WS_CUST_BRANCH"])
    {
        [spinner removeFromSuperview];
        
        NSMutableData *responseData = (NSMutableData*)resData;
        
        NSError* error;
        NSMutableDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                            options:kNilOptions
                                                                              error:&error];
        
        NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
        
        NSString * result = [responseDict objectForKey:@"result"];
        if([result boolValue] == 1 )
        {
            debtorWarehouse = [responseDict objectForKey:@"warehouse"];
        }
        [spinnerObj removeFromSuperview];
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    [spinner removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]) {
        [self callGetNewOrderNumberFromDB];
    }
    if ([strWebserviceType isEqualToString:@"WS_PROFILE_ORDER_SAVE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:SAVE_LOCAL_MESSAGE delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = TAG_200;
        [alertView show];
    }
    
    if ([strWebserviceType isEqualToString:@"WS_GET_PRODUCT_IN_DETAILS"]) {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callViewQuoteDetailsDetailsFromDB:db];
            
        }];
        databaseQueue = nil;
    }
    
}


#pragma mark - Delegates


- (void)dismissStockPickUp:(NSString *)strData withDict:(NSMutableDictionary*)stockPickupDict{
    
    if (![strData isEqualToString:@"|||||||||||||||||||"]) {
        isStockPickUpOn = @"Y";
        [dictHeaderDetails setValue:strData forKey:@"PickUpForm"];
        dictStockPickup = [stockPickupDict mutableCopy];
    }
    else{
        isStockPickUpOn = @"N";
        [_tblHeader reloadData];
    }
    [[AppDelegate getAppDelegateObj].rootViewController dismissViewControllerAnimated:YES completion:Nil];
}



- (void)dismissStorePickUp:(NSString *)strData{
    
    if (![strData isEqualToString:@"|||||||||||||||||||||||||||||"]) {
        isStorePickUpOn = @"Y";
        [dictHeaderDetails setValue:strData forKey:@"StockPickUp"];
    }
    else{
        isStorePickUpOn = @"N";
        [_tblHeader reloadData];
    }
    [[AppDelegate getAppDelegateObj].rootViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (void)actiondismissPopOverForCashPickUp:(NSString*)strPickUpAmt IsChequeAvaliable:(BOOL)isChequeAvaliable{
    
    self.isCashPickUpOn = @"Y";
    
    self.strCashPickUp = strPickUpAmt;
    
    if (isChequeAvaliable){
        self.strChequeAvailable = @"Y";
    }
    else{
        self.strChequeAvailable = @"N";
    }
    
    [popoverController dismissPopoverAnimated:YES];
    
}

- (void)setDeliveryRun:(NSString *)strDelRun{
    isDeliveryRunSlected = YES;
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    self.strDeliveryRun = strDelRun;
    [_tblHeader reloadData];
}


- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation stock_code:(NSString *)stock_code
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(specialPriceUpdated1:) name:@"SpecialPriceUpdated" object:self];
    
    
    isSearch = NO;
    _txtSearchProfileOrderEntryProducts.text = @"";
    
    for (int i=0; i<[arrProducts count]; i++)
    {
        NSString *strCode = [[arrProducts objectAtIndex:i] objectForKey:@"StockCode"];
        
        if([stock_code isEqualToString:strCode])
        {
            NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
            NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
            
            NSMutableDictionary *dictTotalPrice = [arrProducts objectAtIndex:i];
            [dictTotalPrice setValue:strTotalPrice forKey:@"ExtnPrice"];
            [dictTotalPrice setValue:strQuantityOrdered forKey:@"QuantityOrdered"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"Gross"] forKey:@"Gross"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"Tax"] forKey:@"Tax"];
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"Price"] forKey:@"Price"];
            
            [dictTotalPrice setValue:[dictCalculation objectForKey:@"PriceChnaged"] forKey:@"PriceChnaged"];
            
            [_tblDetails reloadData];
            totalCost = 0;
            totalLines = 0;
            
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([obj objectForKey:@"ExtnPrice"]) {
                    NSString *strExtnPrice = [obj objectForKey:@"ExtnPrice"];
                    totalCost += [strExtnPrice floatValue];
                    totalLines = totalLines + 1;
                }
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
                    totalLines = totalLines + 1;
                }
                
            }];
            
            if ([strQuantityOrdered isEqualToString:@"0"]) {
                totalLines = totalLines - 1;
                NSLog(@"Product quantity is zero .... %d",totalLines);
            }
            
            lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
            lblTotalLines.text =[NSString stringWithFormat:@"%d",totalLines];
                
                if ([strQuantityOrdered isEqualToString:@"0"]) {
                        lblTotalLines.text = @"1";
                        
                }

        }
    }
    
    // Set price function
    
    if (totalLines > 0) {
        // Store number of quantity
        
        
        NSString *strTotalLine = [NSString stringWithFormat:@"%d",totalLines];
        [[NSUserDefaults standardUserDefaults] setObject:strTotalLine forKey:@"QuantityForProfile"];
        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    else{
        // Store number of quantity
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"QuantityForProfile"];
        //[[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"QuantityForProfile"];

        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}


- (void)specialPriceUpdated1:(NSNotification *)notification
{
    NSMutableDictionary *resultDict = notification.object;
    if(resultDict.count > 0)
    {
        NSString *stockcode = [resultDict objectForKey:@"StockCode"];
        NSString *strPrice = [resultDict objectForKey:@"Price"];
        NSString *strStatus = [resultDict objectForKey:@"STATUS"];
        if([arrProducts containsObject:stockcode])
        {
            NSPredicate *predicate =[NSPredicate predicateWithFormat:@"StockCode = %@",stockcode];
            NSArray *result =[arrProducts filteredArrayUsingPredicate:predicate];
            if(result.count > 0)
            {
                NSMutableDictionary *dic =[[result objectAtIndex:0]mutableCopy];
                NSUInteger index = [arrProducts indexOfObject:dic];
                [dic setObject:strPrice forKey:@"Price"];
                [dic setObject:strStatus forKey:@"STATUS"];
                [arrProducts replaceObjectAtIndex:index withObject:dic];
            }
        }
    }
}


- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict{
    
    __block BOOL isCommentExist = NO;
    __block int prodIndex = 0;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            prodIndex = idx;
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    if (!isCommentExist) {
        
        //Show delete button
        btnDelete.hidden = YES;
        _tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        //--Code changed to always add the comment at top
        totalLines = totalLines+1;
        lblTotalLines.text =[NSString stringWithFormat:@"%d",totalLines];
        [arrProducts addObject:dict];
        
        
        //        [arrProducts insertObject:dict atIndex:0];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [_tblDetails reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
        [_tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        
    }
    else{
        
        [arrProducts replaceObjectAtIndex:prodIndex withObject:dict];
        if (isCommentAddedOnLastLine) {
            id object = [arrProducts objectAtIndex:prodIndex];
            [arrProducts removeObjectAtIndex:prodIndex];
            
            [arrProducts insertObject:object atIndex:[arrProducts count]];
        }
        
        [_tblDetails reloadData];
        
    }
    if (totalLines > 0) {
        // Store number of quantity
        
        
        NSString *strTotalLine = [NSString stringWithFormat:@"%d",totalLines];
        [[NSUserDefaults standardUserDefaults] setObject:strTotalLine forKey:@"QuantityForProfile"];
        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    else{
        // Store number of quantity
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"QuantityForProfile"];
        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)showFreightView{
    //[segControl setSelectedIndex:1];
    __block BOOL isFreightExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/F"]) {
            isFreightExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isFreightExist)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter the freight value." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = 911;
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.placeholder = @"Enter the freight value";
        alertTextField.keyboardType = UIKeyboardTypeNumberPad;
        [alert show];
        //[self calcFreight];
    }
}

- (void)showCommentView{
    __block BOOL isCommentExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
        dataViewController.commentStr = @"";
        dataViewController.commntTAg = 0;
        dataViewController.delegate = self;
        dataViewController.productIndex = -1;
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
}

-(void)setCommentPosition:(BOOL)status
{
    self.isCommentAddedOnLastLine = status;
}

/*-(void)setFreightPosition:(BOOL)status
 {
 self.isFrightAddedOnLastLine = status;
 }*/
- (void)setDeliveryDate:(NSString *)strDeliveryDate{
    self.strDelDate = strDeliveryDate;
    strCopyDelDate = [self.strDelDate copy];
    [_tblHeader reloadData];
}


-(void)dismissPopover{
    [popoverController dismissPopoverAnimated:YES];
}

#pragma  mark - Searchbar Delegates


- (IBAction)filterSearchResult:(id)sender {
    NSLog(@"Search by : ");
    UISegmentedControl *segment=(UISegmentedControl*)sender;
    switch (segment.selectedSegmentIndex) {
        case 0:
            NSLog(@"Code");
            
            break;
        case 1:
            NSLog(@"Description");
            break;
        default:
            break;
    }
    
}



-(void)searchResultByDescription:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"Description CONTAINS[cd] %@", stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [_tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void)searchResultByCode:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"StockCode CONTAINS[cd] %@", stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [_tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length <= 0)
    {
        [arrSearchProducts removeAllObjects];
        isSearch = NO;
        [_tblDetails reloadData];
    }
    
    
}



-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchTxt = [NSString stringWithFormat:@"%%%@%%", searchBar.text];
    
    isSearch = YES;
    [arrSearchProducts removeAllObjects];
    
    if (_SegmentSearch.selectedSegmentIndex == 0) {
        [self searchResultByCode:searchTxt];
    }
    else  if (_SegmentSearch.selectedSegmentIndex == 1){
        [self searchResultByDescription:searchTxt];
    }
    
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delgates

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textfield
{
    CGPoint pointInTable = [textfield.superview convertPoint:textfield.frame.origin toView:self.tblHeader];
    CGPoint contentOffset = self.tblHeader.contentOffset;
    
    contentOffset.y = (pointInTable.y - textfield.inputAccessoryView.frame.size.height);
    
    [self.tblHeader setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == TAG_500)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 34)
        {
            return NO;
        }
    }
    else if (textField.tag == TAG_600)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_700)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_800)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_900)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 39)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1000)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 9)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1100)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1200)
    {
        strDelInst1 = textField.text;
        int tempCount = [textField.text length];
        
        if(tempCount > 24)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1300)
    {
        strDelInst2 = textField.text;
        int tempCount = [textField.text length];
        
        if(tempCount > 24)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1400)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if([textField.superview.superview isKindOfClass:[CustomCellGeneral class]])
    {
        CustomCellGeneral *cell = (CustomCellGeneral*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tblHeader indexPathForCell:cell];
        [self.tblHeader scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}

/*
 - (void)textFieldDidBeginEditing:(UITextField *)textField
 {
 _tblHeader.contentInset = UIEdgeInsetsMake(_tblHeader.contentInset.top,
 _tblHeader.contentInset.left,
 _tblHeader.contentInset.bottom+400,
 _tblHeader.contentInset.right);
 
 NSIndexPath *indexPath = [_tblHeader indexPathForCell:(CustomCellGeneral *)[(UIView *)[textField superview] superview]];
 [_tblHeader scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
 
 _tblHeader.contentInset = UIEdgeInsetsMake(_tblHeader.contentInset.top,
 _tblHeader.contentInset.left,
 _tblHeader.contentInset.bottom-400,
 _tblHeader.contentInset.right);
 
 }
 */
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case TAG_500:
        {
            self.strDelName = textField.text;
        }
            break;
        case TAG_600:
        {
            self.strDelAddress1 = textField.text;
            
        }
            break;
            
        case TAG_700:
        {
            self.strDelAddress2 = textField.text;
        }
            break;
            
        case TAG_800:
        {
            self.strDelAddress3 = textField.text;
        }
            break;
            
        case TAG_900:
        {
            self.strDelSubUrb = textField.text;
        }
            break;
            
        case TAG_1000:
        {
            self.strDelPostCode = textField.text;
        }
            break;
            
        case TAG_1100:
        {
            self.strDelCountry = textField.text;
        }
            break;
            
        case TAG_1200:
        {
            self.strDelInst1 = textField.text;
        }
            break;
            
        case TAG_1300:
        {
            self.strDelInst2 = textField.text;
        }
            break;
            
        case TAG_1400:
        {
            self.strCustOrder = textField.text;
        }
            break;
            
            
        default:
            break;
    }
    
    
}



#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]) {
                [self callGetNewOrderNumberFromDB];
            }
            
            if ([strWebserviceType isEqualToString:@"WS_GET_PRODUCT_IN_DETAILS"]) {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callViewQuoteDetailsDetailsFromDB:db];
                    
                }];
            }
            
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
                [self callWSGetNewOrderNumber];
            }
            
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
                [self callWSGetNewOrderNumber];
            }
            
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

-(void)DisplayData:(NSString*)strcust_code{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        [self callViewCustomerTransactionFromDB:db forCustCode:strcust_code];
    }];
    
}


#pragma mark - Database calls

-(void)callViewCustomerTransactionFromDB:(FMDatabase *)db forCustCode:(NSString*)custcode{
    
    strWebserviceType = @"DB_GET_CUSTOMER_TRANSACTION";
    FMResultSet *rs1;
    
    @try {
        
        rs1 = [db executeQuery:@"SELECT CODE as Code, CENTRAL_DEBTOR as CentralDebtor, NAME as Name, CURRENT as Current, PHONE as Phone, CONTACT1 as Contact1,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3,DEL_SUBURB as DelSuburb, EXCHANGE_CODE as ExchangeCode, OVER30 as Over30, CREDIT_LIMIT as CreditLimit, OVER60 as Over60, SALES_MTD as SalesMtd, SALES_YTD as SalesYtd, OVER90 as Over90, TERMS as Terms, PAYMENT_PROFILE as PaymentProfile, TRADING_TERMS as TradingTerms, LAST_PAID as LastPaid, HOLD as Hold, TOTAL_DUE as TotalDue,UNRELEASED_ORD as UnReleaseOrd, AGEING_PERIOD as ageing_period FROM armaster WHERE CODE=? ",custcode];
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
        }
        else{
            NSLog(@"No Details exist for this code");
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
    
    FMResultSet *rs2;
    @try {
        
        [arrCustomerTransactions removeAllObjects];
        rs2 = [db executeQuery:@"SELECT DEBTOR as Debtor, TRAN_NO as TranNo, DATE_RAISED as Date, CUST_ORDER, TYPE as Type, STATUS as Status, NETT as Nett, AMOUNT_PAID FROM artrans WHERE DEBTOR = ? ",custcode];
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([rs2 next]) {
            
            NSDictionary *dictData = [rs2 resultDictionary];
            //Balance calculation
            float balance = [[dictData objectForKey:@"Nett"] floatValue] - [[dictData objectForKey:@"AMOUNT_PAID"] floatValue];
            NSString *strBalance1 = [NSString stringWithFormat:@"%f",balance];
            
            [dictData setValue:strBalance1 forKey:@"BALANCE"];
            [dictCustomerDetails setValue:strBalance1 forKey:@"Balance"];
            
            NSDate *fromDate = [[SDSyncEngine sharedEngine] dateUsingStringFromAPI:[dictData objectForKey:@"Date"]];
            NSDate *toDate = [NSDate date];
            int days = [AppDelegate calcDaysBetweenTwoDate:fromDate ToDate:toDate];
            NSString *strDays = [NSString stringWithFormat:@"%d",days];
            
            [dictData setValue:strDays forKey:@"Days"];
            
            [arrCustomerTransactions addObject:dictData];
            
        }
        [rs2 close];
    }
    @catch (NSException* e) {
        
        // rethrow if not one of the two exceptions above
        [rs2 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
    }
    
    //}];
}

-(void)getFromSamasterDB:(NSString *)stockCode
{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        @try {
            FMResultSet *rs1 = [db executeQuery:@"SELECT * from samaster  WHERE CODE = ?",stockCode];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            addProductDictionary = [NSMutableDictionary new];
            
            NSDictionary *dictData = [NSDictionary new];
            if ([rs1 next]) {
                isModified = @"0";
                dictData = [rs1 resultDictionary];
                NSLog(@"dictdata from samaster %@",dictData);
                
                [addProductDictionary setObject:[strDebtor trimSpaces] forKey:@"CUSTOMER_CODE"];
                [addProductDictionary setObject:[[dictData objectForKey:@"CODE"]trimSpaces] forKey:@"STOCK_CODE"];
                [addProductDictionary setObject:[dictData objectForKey:@"BRAND"] forKey:@"BRAND"];
                
                NSDate *tempDate = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"LAST_SOLD"]];
                NSString *lastSoldDate =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempDate] ;
                [addProductDictionary setObject:lastSoldDate forKey:@"DATE_LAST_PURCH"];
                [addProductDictionary setObject:[dictData objectForKey:@"LAST_SOLD"] forKey:@"TEMP_DATE_LAST_PURCH"];
                
                
                NSDate *tempPromoDate = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"PROMO_STRT_DATE"]];
                NSString *datePromoted =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempPromoDate] ;
                [addProductDictionary setObject:datePromoted forKey:@"DATE_PROMOTED"];
                [addProductDictionary setObject:[dictData objectForKey:@"PROMO_STRT_DATE"] forKey:@"TEMP_DATE_PROMOTED"];
                
                [addProductDictionary setObject:[dictData objectForKey:@"PROMO_DAYS"] forKey:@"PROMO_DAYS"];
                [addProductDictionary setObject:[dictData objectForKey:@"LAST_COST"] forKey:@"VALUE_LAST_SALE"];
                [addProductDictionary setObject:[dictData objectForKey:@"PROD_CLASS"] forKey:@"PRODUCT_CLASS"];
                [addProductDictionary setObject:[[dictData objectForKey:@"PROD_GROUP"]trimSpaces] forKey:@"PROD_GROUP"];
                //                PROD_GROUP
                [addProductDictionary setObject:[[dictData objectForKey:@"SUBSTITUTE"]trimSpaces] forKey:@"SUBSTITUTE_YN"];// substitute  = "" ? N
                [addProductDictionary setObject:@"0" forKey:@"CYCLE_DAYS"];
                
                NSString *descriptionStr =[dictData objectForKey:@"DESCRIPTION"];
                descriptionStr = [descriptionStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp"];
                [addProductDictionary setObject:descriptionStr forKey:@"DESCRIPTION"];
                
                
                [addProductDictionary setObject:@"0.00" forKey:@"ORDER_QTY"];
                [addProductDictionary setObject:@"0.00" forKey:@"PRICE"];//0.000
                [addProductDictionary setObject:@"0.00" forKey:@"NOMINAL_MARGIN"];
                [addProductDictionary setObject:@"900000" forKey:@"USER_SORT_SEQ"];//?
                [addProductDictionary setObject:[dictData objectForKey:@"PRICE1"] forKey:@"SALES_00"];
                [addProductDictionary setObject:[dictData objectForKey:@"PRICE2"] forKey:@"SALES_01"];
                [addProductDictionary setObject:[dictData objectForKey:@"PRICE3"] forKey:@"SALES_02"];
                [addProductDictionary setObject:[dictData objectForKey:@"PRICE4"] forKey:@"SALES_03"];
                [addProductDictionary setObject:@"0.00" forKey:@"COST_01"];
                [addProductDictionary setObject:@"0.00" forKey:@"COST_02"];
                [addProductDictionary setObject:@"0.00" forKey:@"COST_03"];
                [addProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_1"] forKey:@"SPARE_NUM_1"];
                [addProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_2"] forKey:@"SPARE_NUM_2"];
                [addProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_3"] forKey:@"SPARE_NUM_3"];
                [addProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_4"] forKey:@"SPARE_NUM_4"];
                
                NSDate *tempSpareDate1 = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"SPARE_DATE_1"]];
                NSString *dateSpareDate1 =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempSpareDate1] ;
                [addProductDictionary setObject:dateSpareDate1 forKey:@"SPARE_DATE_1"];
                [addProductDictionary setObject:[dictData objectForKey:@"SPARE_DATE_1"] forKey:@"TEMP_SPARE_DATE_1"];
                
                
                NSDate *tempSpareDate2 = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:@"1900-01-01 00:00:00"];
                [addProductDictionary setObject:@"1900-01-01T00:00:00+11:00" forKey:@"SPARE_DATE_2"];
                [addProductDictionary setObject:tempSpareDate2 forKey:@"Temp_SPARE_DATE_2"];
                
                [addProductDictionary setObject:[dictData objectForKey:@"EDIT_STATUS"] forKey:@"EDIT_STATUS"];
                //                SPARE_DATE_1
            }
            [rs1 close];
            
            NSString *str = [NSString stringWithFormat:@"SELECT * from sodetail  WHERE DEBTOR = %@ AND ITEM = %@ ORDER BY `CREATE_DATE` DESC LIMIT 1",strDebtor,stockCode];
            
            NSLog(@"sodetail --> %@",str);
            FMResultSet *rs11 = [db executeQuery:@"SELECT * from `sodetail`  WHERE DEBTOR = ? AND ITEM = ? ORDER BY `CREATE_DATE` DESC LIMIT 1",strDebtor,stockCode];
            
            if (!rs11)
            {
                [rs11 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSDictionary *dictData1 = [NSDictionary new];
            if ([rs11 next]) {
                isModified = @"0";
                dictData1 = [rs1 resultDictionary];
                NSLog(@"dictdata from SODetail %@",dictData1);
                if([dictData1 objectForKey:@"COST"])
                {
                    [addProductDictionary setObject:[dictData1 objectForKey:@"COST"] forKey:@"COST_00"];
                    
                }
                else
                {
                    [addProductDictionary setObject:@"0.00" forKey:@"COST_00"];
                }
                if([dictData1 objectForKey:@"ORD_QTY"])
                {
                    [addProductDictionary setObject:[dictData1 objectForKey:@"ORD_QTY"] forKey:@"QTY_LAST_SOLD"];// Sodetail
                    
                }
                else
                {
                    [addProductDictionary setObject:@"0.00" forKey:@"QTY_LAST_SOLD"];// Sodetail
                    
                }
                
            }
            else
            {
                [addProductDictionary setObject:@"0.00" forKey:@"COST_00"];
                
                [addProductDictionary setObject:@"0.00" forKey:@"QTY_LAST_SOLD"];// Sodetail
                
            }
            [rs1 close];
            
            [self CallAddRemoveProductFromProfile:addProductDictionary];
            
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
    }];
    databaseQueue = nil;
}
#pragma mark - Remove From Profile


-(void)callRemoveItemFromSmcuspro:(FMDatabase *)db
{
    NSString *strStockCodexml = @"";
    if([deleteProductDictionary objectForKey:@"STOCK_CODE"])
    {
        strStockCodexml = [deleteProductDictionary objectForKey:@"STOCK_CODE"];
    }
    else if([deleteProductDictionary objectForKey:@"StockCode"])
        
    {
        strStockCodexml = [deleteProductDictionary objectForKey:@"StockCode"];
    }
    
    
    NSString *strQuery = [NSString stringWithFormat:@"DELETE FROM smcuspro WHERE  CUSTOMER_CODE = '%@' AND STOCK_CODE = '%@'",strDebtor,strStockCodexml];
    NSLog(@"delete query %@",strQuery);
    
    BOOL y1 = [db executeUpdate:strQuery];
    
    if (!y1)
    {
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
    }
    if (y1) {
        NSLog(@"Product deleted successfully");
        
    }
    [db close];
    [spinner removeFromSuperview];
    
    NSLog(@"isModified %@",isModified);
    
    
}

-(void)callAddItemInSmcuspro:(FMDatabase *)db
{
    NSString *strQuery = [NSString stringWithFormat:@"INSERT INTO smcuspro (CUSTOMER_CODE, STOCK_CODE,DATE_LAST_PURCH,DATE_PROMOTED,PROMO_DAYS,QTY_LAST_SOLD,VALUE_LAST_SALE,PRODUCT_CLASS,SUBSTITUTE_YN, CYCLE_DAYS,DESCRIPTION,ORDER_QTY,PRICE,NOMINAL_MARGIN,USER_SORT_SEQ,PROD_GROUP,BRAND,SALES_00,SALES_01,SALES_02,SALES_03,COST_00,COST_01,COST_02,COST_03,CREATION_DATE,EDIT_STATUS,SPARE_NUM_1,SPARE_NUM_2,SPARE_NUM_3,SPARE_NUM_4,SPARE_DATE_1,SPARE_DATE_2) VALUES (%@, %@, %@, %@, %@,%@,%@, %@, %@, %@, %@,%@, %@, %@, %@, %@,%@, %@, %@, %@, %@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@)",[addProductDictionary objectForKey:@"CUSTOMER_CODE"],[addProductDictionary objectForKey:@"STOCK_CODE"],[addProductDictionary objectForKey:@"DATE_LAST_PURCH"],[addProductDictionary objectForKey:@"DATE_PROMOTED"],[addProductDictionary objectForKey:@"PROMO_DAYS"],[addProductDictionary objectForKey:@"QTY_LAST_SOLD"],[addProductDictionary objectForKey:@"VALUE_LAST_SALE"],[addProductDictionary objectForKey:@"PRODUCT_CLASS"],[addProductDictionary objectForKey:@"SUBSTITUTE_YN"],[addProductDictionary objectForKey:@"CYCLE_DAYS"],[addProductDictionary objectForKey:@"DESCRIPTION"],[addProductDictionary objectForKey:@"ORDER_QTY"],[addProductDictionary objectForKey:@"PRICE"],[addProductDictionary objectForKey:@"NOMINAL_MARGIN"],[addProductDictionary objectForKey:@"USER_SORT_SEQ"],[addProductDictionary objectForKey:@"PROD_GROUP"],[addProductDictionary objectForKey:@"BRAND"],[addProductDictionary objectForKey:@"SALES_00"],[addProductDictionary objectForKey:@"SALES_01"],[addProductDictionary objectForKey:@"SALES_02"],[addProductDictionary objectForKey:@"SALES_03"],[addProductDictionary objectForKey:@"COST_00"],[addProductDictionary objectForKey:@"COST_01"],[addProductDictionary objectForKey:@"COST_02"],[addProductDictionary objectForKey:@"COST_03"],[addProductDictionary objectForKey:@"CREATION_DATE"],[addProductDictionary objectForKey:@"EDIT_STATUS"],[addProductDictionary objectForKey:@"SPARE_NUM_1"],[addProductDictionary objectForKey:@"SPARE_NUM_2"],[addProductDictionary objectForKey:@"SPARE_NUM_3"],[addProductDictionary objectForKey:@"SPARE_NUM_4"],[addProductDictionary objectForKey:@"SPARE_DATE_1"],[addProductDictionary objectForKey:@"SPARE_DATE_2"]];
    NSLog(@"strQuery %@",strQuery);
    
    
    BOOL result = [db executeUpdate:@"INSERT OR REPLACE INTO smcuspro (CUSTOMER_CODE, STOCK_CODE,DATE_LAST_PURCH,DATE_PROMOTED,PROMO_DAYS,QTY_LAST_SOLD,VALUE_LAST_SALE,PRODUCT_CLASS,SUBSTITUTE_YN, CYCLE_DAYS,DESCRIPTION,ORDER_QTY,PRICE,NOMINAL_MARGIN,USER_SORT_SEQ,PROD_GROUP,BRAND,SALES_00,SALES_01,SALES_02,SALES_03,COST_00,COST_01,COST_02,COST_03,CREATION_DATE,EDIT_STATUS,SPARE_NUM_1,SPARE_NUM_2,SPARE_NUM_3,SPARE_NUM_4,SPARE_DATE_1,SPARE_DATE_2) VALUES (?, ?, ?, ?, ?,?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?)",[addProductDictionary objectForKey:@"CUSTOMER_CODE"],[addProductDictionary objectForKey:@"STOCK_CODE"],[addProductDictionary objectForKey:@"TEMP_DATE_LAST_PURCH"],[addProductDictionary objectForKey:@"TEMP_DATE_PROMOTED"],[addProductDictionary objectForKey:@"PROMO_DAYS"],[addProductDictionary objectForKey:@"QTY_LAST_SOLD"],[addProductDictionary objectForKey:@"VALUE_LAST_SALE"],[addProductDictionary objectForKey:@"PRODUCT_CLASS"],[addProductDictionary objectForKey:@"SUBSTITUTE_YN"],[addProductDictionary objectForKey:@"CYCLE_DAYS"],[addProductDictionary objectForKey:@"DESCRIPTION"],[addProductDictionary objectForKey:@"ORDER_QTY"],[addProductDictionary objectForKey:@"PRICE"],[addProductDictionary objectForKey:@"NOMINAL_MARGIN"],[addProductDictionary objectForKey:@"USER_SORT_SEQ"],[addProductDictionary objectForKey:@"PROD_GROUP"],[addProductDictionary objectForKey:@"BRAND"],[addProductDictionary objectForKey:@"SALES_00"],[addProductDictionary objectForKey:@"SALES_01"],[addProductDictionary objectForKey:@"SALES_02"],[addProductDictionary objectForKey:@"SALES_03"],[addProductDictionary objectForKey:@"COST_00"],[addProductDictionary objectForKey:@"COST_01"],[addProductDictionary objectForKey:@"COST_02"],[addProductDictionary objectForKey:@"COST_03"],[NSDate date],[addProductDictionary objectForKey:@"EDIT_STATUS"],[addProductDictionary objectForKey:@"SPARE_NUM_1"],[addProductDictionary objectForKey:@"SPARE_NUM_2"],[addProductDictionary objectForKey:@"SPARE_NUM_3"],[addProductDictionary objectForKey:@"SPARE_NUM_4"],[addProductDictionary objectForKey:@"TEMP_SPARE_DATE_1"],[addProductDictionary objectForKey:@"TEMP_SPARE_DATE_2"]];
    [spinner removeFromSuperview];
    if (!result)
    {
        [db close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
    }
    
    if (result) {
        NSLog(@"Product added successfully");
        
    }
    
    NSLog(@"isModified %@",isModified);
    [db close];
    
    
}



-(void)getSmcusproData:(FMDatabase *)db{
    
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT * from smcuspro  WHERE STOCK_CODE = ? AND CUSTOMER_CODE = ?",[deleteProductDictionary objectForKey:@"StockCode"],strDebtor];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        NSDictionary *dictData = [NSDictionary new];
        if ([rs1 next]) {
            isModified = @"2";
            dictData = [rs1 resultDictionary];
            NSLog(@"dictdata from smcuspro %@",dictData);
            NSLog(@"DeleteDataDictionary %@",deleteProductDictionary);
            
            [deleteProductDictionary setObject:[dictData objectForKey:@"CUSTOMER_CODE"] forKey:@"CUSTOMER_CODE"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"STOCK_CODE"] forKey:@"STOCK_CODE"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"BRAND"] forKey:@"BRAND"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"QTY_LAST_SOLD"] forKey:@"QTY_LAST_SOLD"];
            
            //            QTY_LAST_SOLD
            NSDate *tempDate = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"DATE_LAST_PURCH"]];
            NSString *lastSoldDate =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempDate] ;
            [deleteProductDictionary setObject:lastSoldDate forKey:@"DATE_LAST_PURCH"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"TEMP_DATE_LAST_PURCH"];
            
            
            NSDate *tempPromoDate = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"DATE_PROMOTED"]];
            NSString *datePromoted =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempPromoDate] ;
            [deleteProductDictionary setObject:datePromoted forKey:@"DATE_PROMOTED"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"DATE_PROMOTED"] forKey:@"TEMP_DATE_PROMOTED"];
            
            [deleteProductDictionary setObject:[dictData objectForKey:@"PROMO_DAYS"] forKey:@"PROMO_DAYS"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"VALUE_LAST_SALE"] forKey:@"VALUE_LAST_SALE"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"PRODUCT_CLASS"] forKey:@"PRODUCT_CLASS"];
            [deleteProductDictionary setObject:[[dictData objectForKey:@"PROD_GROUP"]trimSpaces] forKey:@"PROD_GROUP"];
            //                PROD_GROUP
            [deleteProductDictionary setObject:@"N" forKey:@"SUBSTITUTE_YN"];// substitute  = "" ? N
            [deleteProductDictionary setObject:@"0" forKey:@"CYCLE_DAYS"];
            
            NSString *descriptionStr =[dictData objectForKey:@"DESCRIPTION"];
            descriptionStr = [descriptionStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp"];
            [deleteProductDictionary setObject:descriptionStr forKey:@"DESCRIPTION"];
            
            
            [deleteProductDictionary setObject:[dictData objectForKey:@"ORDER_QTY"] forKey:@"ORDER_QTY"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"PRICE"] forKey:@"PRICE"];//0.000
            [deleteProductDictionary setObject:[dictData objectForKey:@"NOMINAL_MARGIN"] forKey:@"NOMINAL_MARGIN"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"USER_SORT_SEQ"] forKey:@"USER_SORT_SEQ"];//?
            [deleteProductDictionary setObject:[dictData objectForKey:@"SALES_00"] forKey:@"SALES_00"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SALES_01"] forKey:@"SALES_01"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SALES_02"] forKey:@"SALES_02"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SALES_03"] forKey:@"SALES_03"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"COST_00"] forKey:@"COST_00"];
            
            [deleteProductDictionary setObject:[dictData objectForKey:@"COST_01"] forKey:@"COST_01"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"COST_02"] forKey:@"COST_02"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"COST_03"] forKey:@"COST_03"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_1"] forKey:@"SPARE_NUM_1"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_2"] forKey:@"SPARE_NUM_2"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_3"] forKey:@"SPARE_NUM_3"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SPARE_NUM_4"] forKey:@"SPARE_NUM_4"];
            
            NSDate *tempSpareDate1 = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"SPARE_DATE_1"]];
            NSString *dateSpareDate1 =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempSpareDate1] ;
            [deleteProductDictionary setObject:dateSpareDate1 forKey:@"SPARE_DATE_1"];
            [deleteProductDictionary setObject:[dictData objectForKey:@"SPARE_DATE_1"] forKey:@"TEMP_SPARE_DATE_1"];
            
            
            NSDate *tempSpareDate2 = [[SDSyncEngine sharedEngine]dateUsingStringFromAPIXml:[dictData objectForKey:@"SPARE_DATE_2"]];
            NSString *dateSpareDate2 =[[SDSyncEngine sharedEngine]dateStringForAPIUsingDateXml:tempSpareDate2] ;
            [deleteProductDictionary setObject:dateSpareDate2 forKey:@"SPARE_DATE_2"];
            [deleteProductDictionary setObject:tempSpareDate2 forKey:@"Temp_SPARE_DATE_2"];
            
            [deleteProductDictionary setObject:[dictData objectForKey:@"EDIT_STATUS"] forKey:@"EDIT_STATUS"];
            
            
            [self CallAddRemoveProductFromProfile:deleteProductDictionary];
        }
        else
        {
            isModified = @"0";
        }
        NSLog(@"isModified %@",isModified);
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
}


-(void)CallAddRemoveProductFromProfile:(NSDictionary *)dict
{
    NSString *soapRequest = [self createxml:(NSMutableDictionary *)dict];
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/customer_profile/manage_smcuspro.php"];
    NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
    [requestnew setDelegate:self];
    [requestnew setRequestMethod:@"POST"];
    [requestnew setPostValue:soapRequest forKey:@"xmlString"];
    [requestnew setPostValue:@"update" forKey:@"action"];
    NSLog(@"soapXMLStr %@",soapRequest);
    [requestnew startAsynchronous];
    
}

-(NSString *)createxml:(NSMutableDictionary*)objectDict
{
    NSLog(@"objectDict : %@",objectDict);
    
    
    // Replace & with amp;
    NSString *descriptionStr= [objectDict objectForKey:@"DESCRIPTION"];
    if([descriptionStr containsString:@"&"] && ![descriptionStr containsString:@"&amp;"])
    {
        descriptionStr = [descriptionStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    }
    NSString *createdDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDateXml:[NSDate date]];
    
    
    
    NSString *strStockCodexml = @"";
    if([objectDict objectForKey:@"STOCK_CODE"])
    {
        strStockCodexml = [objectDict objectForKey:@"STOCK_CODE"];
    }
    else if([objectDict objectForKey:@"StockCode"])
        
    {
        strStockCodexml = [objectDict objectForKey:@"StockCode"];
    }
    
    NSString *soapXml = @"";
    
    soapXml = [NSString stringWithFormat:@"<DocumentElement>"
               "<SMCUSPRO>"
               "<CUSTOMER_CODE>%@</CUSTOMER_CODE>"
               "<STOCK_CODE>%@</STOCK_CODE>"
               "<DATE_LAST_PURCH>%@</DATE_LAST_PURCH>"
               "<DATE_PROMOTED>%@</DATE_PROMOTED>"
               "<PROMO_DAYS>%@</PROMO_DAYS>"
               "<QTY_LAST_SOLD>%@</QTY_LAST_SOLD>"
               "<VALUE_LAST_SALE>%@</VALUE_LAST_SALE>"
               "<PRODUCT_CLASS>%@</PRODUCT_CLASS>"
               "<SUBSTITUTE_YN>%@</SUBSTITUTE_YN>"
               "<CYCLE_DAYS>%@</CYCLE_DAYS>"
               "<DESCRIPTION>%@</DESCRIPTION>"
               "<ORDER_QTY>%@</ORDER_QTY>"
               "<PRICE>%@</PRICE>"
               "<NOMINAL_MARGIN>%@</NOMINAL_MARGIN>"
               "<USER_SORT_SEQ>%@</USER_SORT_SEQ>"
               "<PROD_GROUP>%@</PROD_GROUP>"
               "<BRAND>%@</BRAND>"
               "<SALES_00>%@</SALES_00>"
               "<SALES_01>%@</SALES_01>"
               "<SALES_02>%@</SALES_02>"
               "<SALES_03>%@</SALES_03>"
               "<COST_00>%@</COST_00>"
               "<COST_01>%@</COST_01>"
               "<COST_02>%@</COST_02>"
               "<COST_03>%@</COST_03>"
               "<CREATION_DATE>%@</CREATION_DATE>"
               "<EDIT_STATUS>%@</EDIT_STATUS>"
               "<SPARE_NUM_1>%@</SPARE_NUM_1>"
               "<SPARE_NUM_2>%@</SPARE_NUM_2>"
               "<SPARE_NUM_3>%@</SPARE_NUM_3>"
               "<SPARE_NUM_4>%@</SPARE_NUM_4>"
               "<SPARE_DATE_1>%@</SPARE_DATE_1>"
               "<SPARE_DATE_2>%@</SPARE_DATE_2>"
               "<SPARE_STR />"
               "<MODIFIED_FLAG>%@</MODIFIED_FLAG>"
               "</SMCUSPRO>"
               "</DocumentElement>",[objectDict objectForKey:@"CUSTOMER_CODE"],strStockCodexml,[objectDict objectForKey:@"DATE_LAST_PURCH"],[objectDict objectForKey:@"DATE_PROMOTED"],[objectDict objectForKey:@"PROMO_DAYS"],[objectDict objectForKey:@"QTY_LAST_SOLD"],[objectDict objectForKey:@"VALUE_LAST_SALE"],[objectDict objectForKey:@"PRODUCT_CLASS"],@"N",[objectDict objectForKey:@"CYCLE_DAYS"],descriptionStr,[objectDict objectForKey:@"ORDER_QTY"],[objectDict objectForKey:@"PRICE"],[objectDict objectForKey:@"NOMINAL_MARGIN"],[objectDict objectForKey:@"USER_SORT_SEQ"],[objectDict objectForKey:@"PROD_GROUP"],[objectDict objectForKey:@"BRAND"],[objectDict objectForKey:@"SALES_00"],[objectDict objectForKey:@"SALES_01"],[objectDict objectForKey:@"SALES_02"],[objectDict objectForKey:@"SALES_03"],[objectDict objectForKey:@"COST_00"],[objectDict objectForKey:@"COST_01"],[objectDict objectForKey:@"COST_02"],[objectDict objectForKey:@"COST_03"],createdDate,[objectDict objectForKey:@"EDIT_STATUS"],[objectDict objectForKey:@"SPARE_NUM_1"],[objectDict objectForKey:@"SPARE_NUM_2"],[objectDict objectForKey:@"SPARE_NUM_3"],[objectDict objectForKey:@"SPARE_NUM_4"],[objectDict objectForKey:@"SPARE_DATE_1"],[objectDict objectForKey:@"SPARE_DATE_2"],isModified];
    
    NSLog(@"Soap xml : %@",soapXml);
    return soapXml;
}


@end
