//
//  ProfileMaintenanceViewController.h
//  Blayney
//
//  Created by Pooja Mishra on 07/02/17.
//  Copyright © 2017 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import "QuoteMenuPopOverViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CashPickUpPopOverController.h"
#import "DeliveryRunViewController.h"
#import "ProfileOrderProductDetailViewController.h"
#import "StorePickupViewController.h"
#import "PullToLoadMoreView.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "QuoteProductComment.h"
#import "DatePopOverController.h"
#import "JFDepthView.h"
#import "StockPickupViewController.h"

#import <MessageUI/MessageUI.h>


@interface ProfileMaintenanceViewController :UIViewController <ASIHTTPRequest_delegate,UITextFieldDelegate,ReachabilityRequest_delegate,DismissPopOverDelegateForMenuList,UIPopoverControllerDelegate,DismissPopOverDelegateForCashPickUpDelegate,SetDeliveryRunDelegate,SetPriceDelegate,StorePickupViewDelegate,StockPickupViewDelegate,SetCommentDelegate,SetDeliveryDateDelegate,UITextFieldDelegate,JFDepthViewDelegate,UISearchBarDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>{
    
    
    NSArray *arrHeaderLabels;
    NSMutableArray *arrProducts;
    
    CALayer *layerPointingImage;
    
    MBProgressHUD *spinner;
    
    SEFilterControl *segControl;
    
    NSMutableDictionary *dictHeaderDetails;
    
    NSString *strWebserviceType;
    
    dispatch_queue_t backgroundQueueForNewQuote;
    FMDatabaseQueue *dbQueueNewProfileOrder;
    
    //Reachability *checkInt;
    //NetworkStatus internetStatus;
    
    BOOL isNetworkConnected;
    NSString *strQuoteNum;
    
    UIPopoverController *popoverController;
    
    BOOL chkFinalize;
    
    NSString *strOrderNum;
    
    NSString *isCashPickUpOn;
    NSString *isStorePickUpOn;
    NSString *isStockPickUpOn;
    
    NSString *strCashPickUp;
    NSString *strChequeAvailable;
    
    NSString *strDeliveryRun;
    
    FMDatabaseQueue *dbQueueSalesOrderEntry;
    
    NSString *strPrevDebtor;
    
    NSString *strDebtor;
    NSString *strDelName;
    NSString *strDelAddress1;
    NSString *strDelAddress2;
    NSString *strDelAddress3;
    NSString *strDelSubUrb;
    NSString *strCustCatagory;
    NSString *strDelPostCode;
    NSString *strDelCountry;
    NSString *strDelInst1;
    NSString *strDelInst2;
    NSString *strCustOrder;
    
    NSString *strPriceChanged;
    
    float totalCost;
    int totalLines;
    
    BOOL isDeliveryRunSlected;
    
    NSOperationQueue *operationQueue;
    
    int recordNumber;
    
    PullToLoadMoreView* _loadMoreFooterView;
    
    BOOL isRecordsExist;
    
    NSMutableArray *arrProductCodes;
    
    NSInteger selectedSectionIndex;
    NSInteger selectedIndex;
    
    int cntViewWillAppear;
    BOOL isSearch;
    
    BOOL isProfileOrder;
}
@property (weak, nonatomic) IBOutlet UISearchBar *txtSearchProfileOrderEntryProducts;
@property (weak, nonatomic) IBOutlet UILabel *lblCustName;

@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) NSMutableArray *arrSearchProducts;


//StockPickup Dict
@property (nonatomic,retain) NSDictionary *dictStockPickup;
@property (nonatomic,retain) NSMutableArray *arrStockPickupImg;



@property (nonatomic,retain) NSDictionary *dictHeaderDetails;
@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) UIPopoverController *popoverController;
@property (nonatomic,retain) NSString *strQuoteNumRecived;
@property (nonatomic, assign) BOOL EditableMode;

//isProfileOrder
//strOfflineOrderNum
@property (nonatomic, assign) BOOL isProfileOrder;
@property (nonatomic,assign) NSString *strOfflineOrderNum;



@property (nonatomic,retain) NSString *isCashPickUpOn;
@property (nonatomic,retain) NSString *strOrderNum;
@property (nonatomic,retain) NSString *strCashPickUp;
@property (nonatomic,retain) NSString *strChequeAvailable;
@property (nonatomic,assign) BOOL isSearch;

@property (nonatomic,retain) NSString *strWarehouse;
@property (nonatomic,retain) NSString *strOperatorName;
@property (nonatomic,retain) NSString *strPrevDebtor;
@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,retain) NSString *strDeliveryRun;
@property (nonatomic,retain) NSString *strDelName;
@property (nonatomic,retain) NSString *strDelAddress1;
@property (nonatomic,retain) NSString *strDelAddress2;
@property (nonatomic,retain) NSString *strDelAddress3;
@property (nonatomic,retain) NSString *strDelSubUrb;
@property (nonatomic,retain) NSString *strCustCatagory;
@property (nonatomic,retain) NSString *strDelPostCode;
@property (nonatomic,retain) NSString *strDelCountry;
@property (nonatomic,retain) NSString *strContact;
@property (nonatomic,retain) NSString *strDelDate;
@property (nonatomic,retain) NSString *isStorePickUpOn; // Store Pickup
@property (nonatomic,retain) NSString *isStockPickUpOn;// Stock Pickup



@property (nonatomic,retain) NSString *strPriceChanged;


@property (nonatomic,retain) NSString *strDelInst1;
@property (nonatomic,retain) NSString *strDelInst2;
@property (nonatomic,retain) NSString *strCustOrder;

@property (nonatomic,retain)NSString *strPhone;
@property (nonatomic,retain)NSString *strEmail;
@property (nonatomic,retain)NSString *strFax;
@property (nonatomic,retain)NSString *strBalance;
@property (nonatomic,retain)NSString *strAddress1;
@property (nonatomic,retain)NSString *strAddress2;
@property (nonatomic,retain)NSString *strAddress3;
@property (nonatomic,retain)NSString *strSubUrb;
@property (nonatomic,retain)NSString *strAllowPartial;
@property (nonatomic,retain)NSString *strBranch;
@property (nonatomic,retain)NSString *strCarrier;
@property (nonatomic,retain)NSString *strChargeRate;
@property (nonatomic,retain)NSString *strChargetype;
@property (nonatomic,retain)NSString *strCountry;
@property (nonatomic,retain)NSString *strDirect;
@property (nonatomic,retain)NSString *strDropSequence;
@property (nonatomic,retain)NSString *strExchangeCode;
@property (nonatomic,retain)NSString *strExportDebtor;
@property (nonatomic,retain)NSString *strHeld;
@property (nonatomic,retain)NSString *strName;
@property (nonatomic,retain)NSString *strNumboxes;
@property (nonatomic,retain)NSString *strOrderDate;
@property (nonatomic,retain)NSString *strPeriodRaised;
@property (nonatomic,retain)NSString *strPostCode;
@property (nonatomic,retain)NSString *strPriceCode;
@property (nonatomic,retain)NSString *strSalesBranch;
@property (nonatomic,retain)NSString *strSalesman;
@property (nonatomic,retain)NSString *strTaxExemption1;
@property (nonatomic,retain)NSString *strTaxExemption2;
@property (nonatomic,retain)NSString *strTaxExemption3;
@property (nonatomic,retain)NSString *strTaxExemption4;
@property (nonatomic,retain)NSString *strTaxExemption5;
@property (nonatomic,retain)NSString *strTaxExemption6;
@property (nonatomic,retain)NSString *strTradingTerms;
@property (nonatomic,retain)NSString *strYearRaised;
@property (nonatomic,retain)NSString *strStockPickup;
@property (nonatomic,retain)NSString *strPickupForm;

@property (nonatomic,retain)NSString *strTotalWeight;
@property (nonatomic,retain)NSString *strTotalVolume;
@property (nonatomic,retain)NSString *strTotalTax;
@property (nonatomic,retain)NSString *strTotalCost;
@property (nonatomic,retain)NSString *strTotalLines;
@property (nonatomic,retain)NSString *strOperator;
@property (nonatomic,retain)NSString *strUploadDate;
@property (nonatomic,retain)NSString *strSuccessMessage;
@property (nonatomic,retain)NSString *strDateRaised;
@property (nonatomic,retain)NSString *strCommenttxt;
@property (nonatomic,retain)NSString *strCommentDateCreated;
@property (nonatomic,retain)NSString *strCommentFollowDate;
@property (nonatomic,retain)NSString *strSalesType;
@property (nonatomic,retain)NSString *strCreditLimit;

//Static Values
@property (nonatomic,retain)NSString *strExchangeRate;
@property (nonatomic,retain)NSString *strTotalCartonQty;
@property (nonatomic,retain)NSString *strActive;
@property (nonatomic,retain)NSString *strEditStatus;
@property (nonatomic,retain)NSString *strStatus;
@property (nonatomic,retain)NSString *strRefreshPrice;
@property (nonatomic,retain)NSString *strBOQty;
@property (nonatomic,retain)NSString *strConvFactor;
@property (nonatomic,retain)NSString *strDecimalPlaces;
@property (nonatomic,retain)NSString *strOrigCost;

@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblTotalLines;

@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblTotalCost;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnDelete;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnLoadProfile;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnSaveDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddProducts;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnFinalise;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoProducts;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowDownwards;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowRightWards;
@property (nonatomic,unsafe_unretained) IBOutlet UIToolbar *keyboardToolBar;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddMenu;


@property(nonatomic,assign) BOOL isRecordsExist;
@property(nonatomic,assign) int cntProfileOrders;
@property (nonatomic,assign)BOOL isCommentAddedOnLastLine;
@property (nonatomic,assign)BOOL isFrightAddedOnLastLine;

#pragma mark - IBAction
- (IBAction)actionShowQuoteHistory:(id)sender;
- (IBAction)actionShowProductList:(id)sender;
- (IBAction)actionDeleteRows:(id)sender;

- (void)showOrEnableButtons;
- (void)hideOrDisableButtons;
- (void)setDeliveryRun:(NSString *)strDelRun;
-(void)callWSSaveProfileOrder;
@end
