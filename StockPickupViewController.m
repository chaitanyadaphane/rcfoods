//
//  StockPickupViewController.m
//  Blayney
//
//  Created by Pooja on 17/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//
#import <AddressBookUI/AddressBookUI.h>
#import "StockPickupViewController.h"
#import "PhotoCell.h"


@interface StockPickupViewController ()
{
    NSMutableArray *sectionHeaderArr;
    
    NSMutableArray *arrAllStockCode;
    NSMutableArray *arrFilteredStockCode;
    
    
    NSMutableArray *arrAllCustCode;
    NSMutableArray *arrFilterCustCode;
    
    NSMutableArray *photoArr;
    
    NSMutableArray *storeImageData;

    NSString *pickup_no;
    StockCodeViewController *viewCnt;
    EmailAddressBookViewController *emailAddresView;
    
    NSMutableArray *emailAddressArr;

}
@end

@implementation StockPickupViewController
@synthesize popoverController,delegate;
@synthesize popoverControllerForImage;
@synthesize debtorDetailDict;

- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    photoArr = [NSMutableArray new];
    storeImageData = [NSMutableArray new];
    
    selectedRow = 0;
    
    
    // set initial contentoffset value
    [_scrollview setContentOffset:CGPointMake(0, 0)];
    _emailRecipientTxt.text = @"michelle@jlstewart.com.au";

    // Set textview for Reasoon
    _reasonTextView.layer.cornerRadius = 5;
    _reasonTextView.text = @"";
    
    if (debtorDetailDict.count > 0)
    {
        _custNameTxt.text = [debtorDetailDict objectForKey:@"debtor_name"];
        _custAccNoTxt.text = [debtorDetailDict objectForKey:@"debtor_number"];
        
        NSString *todaysDatestr= [debtorDetailDict objectForKey:@"date_raised"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date1 = [dateFormat dateFromString:todaysDatestr];
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
        NSString *dateRaisedStr = [dateFormat2 stringFromDate:date1];
        
        _dateRaised.text=dateRaisedStr;
    }

    
    _requestedByTxt.text = [self getmemberName];
    // Fetch data
    [self getStockCodeData];
    [self getCustomerData];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Textview

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        NSLog(@"Return pressed, do whatever you like here");
        [textView resignFirstResponder];
        return NO; // or true, whetever you's like
    }
    
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
        [_scrollview setContentOffset:CGPointMake(0, 200)];
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    [_scrollview setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _emailRecipientTxt) {
        [_scrollview setContentOffset:CGPointMake(0, 100)];
    }
    else if(textField == _emailCcTxt){
        
        [_scrollview setContentOffset:CGPointMake(0, 100)];

        // Request authorization to Address Book
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    // First time access has been granted, add the contact
                    [self listPeopleInAddressBook:addressBookRef];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"User denied access" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    
                    [alert show];
                    
                    // User denied access
                    // Display an alert telling user the contact could not be added
                }
            });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
            // The user has previously given access, add the contact
            [self listPeopleInAddressBook:addressBookRef];
        }
        else {
            // The user has previously denied access
            // Send an alert telling user to change privacy setting in settings app
        }
    }
    else if (textField == _productCodeTxt || textField == _descriptionTxt){
         [_scrollview setContentOffset:CGPointMake(0, 150)];
    }
    else if (textField == _quantityTxt){
        [_scrollview setContentOffset:CGPointMake(0, 250)];
    }
}




- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook
{
    if(!emailAddressArr)
    {
    emailAddressArr = [NSMutableArray new];
        NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        
        //    NSLog(@"allPeople %@",allPeople);
        
        for (NSInteger i = 0; i < [allPeople count]; i++) {
            ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
            ABMultiValueRef emails = ABRecordCopyValue(person,kABPersonEmailProperty);
            CFIndex numberofemails = ABMultiValueGetCount(emails);
            for (CFIndex i = 0; i < numberofemails; i++) {
                NSString *emailsAddress = CFBridgingRelease(ABMultiValueCopyValueAtIndex(emails, i));
                NSLog(@"  emailsAddress:%@", emailsAddress);
                [emailAddressArr addObject:emailsAddress];
            }
            CFRelease(emails);
        }

    }
    
    NSLog(@"Email Arr >>> %@",emailAddressArr);
    
    NSMutableArray *sortedArray= [NSMutableArray new];
    sortedArray = (NSMutableArray *)[emailAddressArr sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    if(!emailAddresView)
    {
        emailAddresView = [[EmailAddressBookViewController alloc] initWithNibName:@"EmailAddressBookViewController" bundle:[NSBundle mainBundle]];
    }
    
    // Assign sorted data to Email PopView
    NSLog(@"Sorted Alphabitically Arr >>> %@",sortedArray);

    emailAddresView.arrData = [sortedArray mutableCopy];
    
    if([self.popoverController isPopoverVisible])
    {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:emailAddresView];
    emailAddresView.delegate = self;
    
    [self.popoverController presentPopoverFromRect:_emailCcTxt.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
  
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_scrollview setContentOffset:CGPointMake(0, 0)];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_scrollview setContentOffset:CGPointMake(0, 0)];

    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    if(_emailRecipientTxt.text.length > 0){
        NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailReg];
        if (([emailTest evaluateWithObject:_emailRecipientTxt.text] != YES))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Email"
                                                        message:@"Email should be abc@example.com Format" delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    if(textField == _productCodeTxt ||  textField == _descriptionTxt)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"CODE beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode1";
        viewCnt.isStockCode = YES;
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    if(textField == _emailCcTxt)
    {
        // Request authorization to Address Book
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    // First time access has been granted, add the contact
                    [self listPeopleInAddressBook:addressBookRef];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"User denied access" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    
                    [alert show];
                }
            });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
            // The user has previously given access, add the contact
            [self listPeopleInAddressBook:addressBookRef];
        }
        else {
            // The user has previously denied access
            // Send an alert telling user to change privacy setting in settings app
        }
  
        
    }

    return  YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _productCodeTxt)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"CODE beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode1";
            viewCnt.isStockCode = YES;
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 150);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == _descriptionTxt)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"DESCRIPTION beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode1";
            viewCnt.isStockCode = YES;
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 150);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == _custAccNoTxt)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
                NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
                NSPredicate *regexString = [NSPredicate predicateWithFormat:@"CODE beginswith[cd] %@",indexRegex];
                NSArray *filteredArray = [arrAllCustCode filteredArrayUsingPredicate:regexString];
                arrFilterCustCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            
            
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilterCustCode;
            viewCnt.strTextField = @"txtCustCode";
            viewCnt.isStockCode = YES;
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 150);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            NSLog(@"Crashes if textfield is NULL");
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == _custNameTxt)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"DESCRIPTION beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllCustCode filteredArrayUsingPredicate:regexString];
            arrFilterCustCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            
            
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilterCustCode;
            viewCnt.strTextField = @"txtCustCode";
            viewCnt.isStockCode = YES;
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 150);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            NSLog(@"Crashes if textfield is NULL");
            [self textFieldShouldClear:textField];
        }
    }
    if(textField == _returnTxt)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn1";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    if(textField == _emailCcTxt){

    }

    return YES;
}


- (IBAction)actionAddEmailCc:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Add Email CC" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
    alertView.tag = 5;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeEmailAddress];
    [alertView show];

    

}


#pragma mark - dismiss stockPickup form


- (void)actiondismissEmailPopOverStockPickUp:(NSString*)strTextField{
    
    if (strTextField.length > 0) {
        _emailCcTxt.text = strTextField;
    }
    emailAddressArr = nil;
    [self.popoverController dismissPopoverAnimated:YES];
}

- (IBAction)actionDismiss:(id)sender {
    
    // Create Pickup id
    [self generateStockPickupId];
    
    // Pickup id
    if([pickup_no length] <=0)
    {
        pickup_no = @"";
    }
    
    // Customer Account No
    if([_custAccNoTxt.text length] <=0)
    {
        _custAccNoTxt.text = @"";
    }
    // Customer Name
    if([_custNameTxt.text length] <=0)
    {
        _custNameTxt.text = @"0";
    }
    // Product No.
    if([_productCodeTxt.text length] <=0)
    {
        _productCodeTxt.text = @"";
    }
    
    // Product Description
    if([_descriptionTxt.text length] <=0)
    {
        _descriptionTxt.text = @"";
    }
    
    // Contact Person
    if([_contactPersonTxt.text length] <=0)
    {
        _contactPersonTxt.text = @"";
    }
    // Contact Phone
    if([_contactPhoneTxt.text length] <=0)
    {
        _contactPhoneTxt.text = @"";
    }
    // Batch No.
    if([_batchNoTxt.text length] <=0)
    {
        _batchNoTxt.text = @"";
    }
    // Batch Detail
    if([_batchDetailsTxt.text length] <=0)
    {
        _batchDetailsTxt.text = @"";
    }
    // Best Before Detail
    if(_bestBeforeDateTxt.text.length <= 0)
    {
        _bestBeforeDateTxt.text = @"";
    }
    
    // Email Details
    if(_emailRecipientTxt.text.length <= 0)
    {
        _emailRecipientTxt.text = @"";
    }
    if(_emailCcTxt.text.length <= 0)
    {
        _emailCcTxt.text = @"";
    }
    
    //Reason of return
    if(_reasonReturnTxt.text.length <= 0)
    {
        _reasonReturnTxt.text = @"";
    }
    if(_reasonTextView.text.length <= 0)
    {
        _reasonTextView.text = @"";
    }
    
    // Invoice Attached
    if(_invoiceNoTxt.text.length <= 0)
    {
        _invoiceNoTxt.text = @"";
    }
    
    // Quantity
    if (_quantityTxt.text.length <= 0) {
        _quantityTxt.text = @"0";
    }

    // Images Attached
    NSData *image0Data = NULL;
    NSData *image1Data = NULL;
    NSData *image2Data = NULL;
    NSData *image3Data = NULL;
    NSData *image4Data = NULL;
    NSData *image5Data = NULL;

    

    NSMutableDictionary *dictVal = [NSMutableDictionary new];
    
    //    pickup_no
    [dictVal setObject:pickup_no forKey:@"pickup_no"];
    [dictVal setObject:_requestedByTxt.text forKey:@"requested_by"];
    [dictVal setObject:_custNameTxt.text forKey:@"debtor_name"];
    [dictVal setObject:_custAccNoTxt.text forKey:@"debtor"];
    [dictVal setObject:_productCodeTxt.text forKey:@"product_code"];
    [dictVal setObject:_descriptionTxt.text forKey:@"description"];
    [dictVal setObject:_contactPersonTxt.text forKey:@"Contact_Person"];
    [dictVal setObject:_contactPhoneTxt.text forKey:@"Contact_Phone"];
    [dictVal setObject:_emailRecipientTxt.text forKey:@"EmailRecipient"];
    [dictVal setObject:_emailCcTxt.text forKey:@"EmailCC"];
    
    [dictVal setObject:_batchNoTxt.text forKey:@"BatchNo"];
    [dictVal setObject:_batchDetailsTxt.text forKey:@"BatchDetail"];
    [dictVal setObject:_bestBeforeDateTxt.text forKey:@"BestBeforeDate"];
    
    [dictVal setObject:_reasonReturnTxt.text forKey:@"ReasonReturn"];
    [dictVal setObject:_reasonTextView.text forKey:@"detailReasonReturn"];
    [dictVal setObject:_returnTxt.text forKey:@"Return"];
    
    [dictVal setObject:_invoiceNoTxt.text forKey:@"RelatedInvoice"];
    
    [dictVal setObject:_quantityTxt.text forKey:@"Quantity"];
    
    
    
    NSMutableArray *storedImgArr = [NSMutableArray new];
    
    if(photoArr.count > 0)
    {
        for (int i = 0; i < photoArr.count; i++)
        {
            NSMutableDictionary *dictStoredImages = [NSMutableDictionary new];
            NSString *imageName = @"";
            if (i == 0)
            {
                imageName =[photoArr objectAtIndex:i];
                image0Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image0"];
                [dictStoredImages setObject:image0Data forKey:@"imageContent0"];
                
            }
            if (i == 1)
            {
                imageName = [photoArr objectAtIndex:i];
                image1Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image1"];
                [dictStoredImages setObject:image1Data forKey:@"imageContent1"];
                
            }
            if (i == 2)
            {
                imageName = [photoArr objectAtIndex:i];
                image2Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image2"];
                [dictStoredImages setObject:image2Data forKey:@"imageContent2"];
                
            }
            if (i == 3)
            {
                imageName = [photoArr objectAtIndex:i];
                image3Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image3"];
                [dictStoredImages setObject:image3Data forKey:@"imageContent3"];
                
            }
            if (i == 4)
            {
                imageName = [photoArr objectAtIndex:i];
                image4Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image4"];
                [dictStoredImages setObject:image4Data forKey:@"imageContent4"];
            }
            if (i == 5)
            {
                imageName = [photoArr objectAtIndex:i];
                image5Data = [storeImageData objectAtIndex:i];
                [dictStoredImages setObject:imageName forKey:@"image5"];
                [dictStoredImages setObject:image5Data forKey:@"imageContent5"];
            }
            [storedImgArr addObject:dictStoredImages];
        }
    }
    
    
    
    [dictVal setObject:(image0Data != NULL)?image0Data:@"" forKey:@"image0"];
    [dictVal setObject:(image1Data != NULL)?image1Data:@"" forKey:@"image1"];
    [dictVal setObject:(image2Data != NULL)?image2Data:@"" forKey:@"image2"];
    [dictVal setObject:(image3Data != NULL)?image3Data:@"" forKey:@"image3"];
    [dictVal setObject:(image4Data != NULL)?image4Data:@"" forKey:@"image4"];
    [dictVal setObject:(image5Data != NULL)?image5Data:@"" forKey:@"image5"];
    
//    NSString *strData = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",_custAccNoTxt.text,_custNameTxt.text,_productCodeTxt.text,_descriptionTxt.text,_emailRecipientTxt.text,_emailCcTxt.text,_bestBeforeDateTxt.text,_batchNoTxt.text,_batchDetailsTxt.text,_contactPersonTxt.text,_contactPhoneTxt.text,_invoiceNoTxt.text,_reasonReturnTxt.text,_reasonTextView.text,image0,image1,image2,image3,image4,image5];
    
    NSString *strData = [NSString stringWithFormat:@"%@|%@|%@|%@|%@||0|||||0|||||0|||||0|||||0|||||0|||||0|||",_productCodeTxt.text,_quantityTxt.text,_reasonReturnTxt.text,_returnTxt.text,_reasonTextView.text];

    [self.delegate dismissStockPickUp:strData withDict:dictVal];
}

- (IBAction)actionBestBeforeDate:(id)sender {
    DatePopOverController *dataViewController = [[DatePopOverController alloc] initWithNibName:@"DatePopOverController" bundle:[NSBundle mainBundle]];
    dataViewController.contentSizeForViewInPopover =
    CGSizeMake(300, 300);
    dataViewController.isStockPickup = YES;
    dataViewController.isCallDate = NO;

    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:dataViewController];
    
//    CGRect rect = CGRectMake(_bestBeforeDateTxt.bounds.origin.x+420, _bestBeforeDateTxt.bounds.origin.y+25, 0, 0);
    
    dataViewController.delegate = self;
    [self.popoverController presentPopoverFromRect:_bestBeforeDateTxt.frame
                                            inView:self.scrollview
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];

}

- (void)actiondismissPickUpValue:(NSDictionary*)dictPickUp strTextField:(NSString*)strTextField{
    
    if([strTextField isEqualToString:@"txtStockCode1"])
    {
        _productCodeTxt.text = [dictPickUp objectForKey:@"CODE"];
        _descriptionTxt.text = [dictPickUp objectForKey:@"DESCRIPTION"];
    }
    else if([strTextField isEqualToString:@"txtCustCode"])
    {
        _custAccNoTxt.text = [dictPickUp objectForKey:@"CODE"];
        _custNameTxt.text = [dictPickUp objectForKey:@"DESCRIPTION"];
    }
    [self.popoverController dismissPopoverAnimated:YES];
}



- (void)setDeliveryDate:(NSString *)strDeliveryDate{
    if (strDeliveryDate.length > 0) {
        _bestBeforeDateTxt.text = strDeliveryDate;
    }
    else{
        _bestBeforeDateTxt.text = @"";
    }
}

-(void)actiondismissPopOverStockPickUp:(NSString *)strPickUpCode strTextField:(NSString *)strTextField
{
    if (strPickUpCode.length > 0) {
        _returnTxt.text = strPickUpCode;
    }
    else{
        _returnTxt.text = @"";
    }
    [self.popoverController dismissPopoverAnimated:YES];
}


- (IBAction)actionPopViewForTextField:(id)sender {
}

- (IBAction)actionReturnValues:(id)sender {
    if(!viewCnt)
    {
        viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
    }
    viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
    viewCnt.strTextField = @"txtReturn1";
     viewCnt.isStockCode = NO;
    
    //resize the popover view shown
    //in the current view to the view's size
    viewCnt.contentSizeForViewInPopover = CGSizeMake(100,80);
    
    if([self.popoverController isPopoverVisible])
    {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:viewCnt];
    
    viewCnt.delegate = self;
    
    [self.popoverController presentPopoverFromRect:_returnTxt.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (void)actiondismissPopOverForReason:(NSString *)strReasonName{
    
    if (strReasonName.length > 0) {
        _reasonReturnTxt.text = strReasonName;
    }
    else{
        _reasonReturnTxt.text = @"";
    }
    [self.popoverController dismissPopoverAnimated:YES];
}


-(void)dismissPopover{
    if ([popoverControllerForImage isPopoverVisible]) {
        [popoverControllerForImage dismissPopoverAnimated:YES];
    }
    else{
        [popoverController dismissPopoverAnimated:YES];
    }
}

#pragma mark - Popview Action 
-(IBAction)actionDisplayReason:(id)sender{
    
    [[UIApplication sharedApplication] resignFirstResponder];
    
    UIButton *btn = (UIButton *)sender;
    tagTxtReason = btn.tag;
    ReasonListPopOverViewController *popoverContent = [[ReasonListPopOverViewController alloc] initWithNibName:@"ReasonListPopOverViewController" bundle:[NSBundle mainBundle]];
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    //popoverController.delegate = self;
    popoverContent.delegate = self;
    
    //present the popover view non-modal with a
    [self.popoverController presentPopoverFromRect:btn.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}


#pragma mark - Upload Photo


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"PhotoCell";

    [collectionView1 registerNib:[UINib nibWithNibName:@"PhotoCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:identifier];

    PhotoCell *cell = [collectionView1 dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.layer setBorderColor:(__bridge CGColorRef)([UIColor blueColor])];
    [cell.layer setBorderWidth: 1.0];
    [cell.layer setCornerRadius:0.0];

    if (indexPath.row == 0)
    {
        UIImageView *photo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];

        UIImage*image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"capture_image_button" ofType:@"png"]];
        [photo setImage:image];
        [cell.contentView addSubview:photo];

    }
    else
    {
        UIImageView *photo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];

        if((indexPath.row-1 < storeImageData.count) && (indexPath.row >  0))
        {
            UIImage *image = [UIImage imageWithData:[storeImageData objectAtIndex:indexPath.row-1]];
            [photo setImage:image];
        }
        else{
            [photo setBackgroundColor:[UIColor whiteColor]];
        }
        [cell.contentView addSubview:photo];

    }

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView1 didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if(storeImageData.count > 4){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Maximum limit for upload is 5" delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        else{
            
            if (indexPath.row <= storeImageData.count)
            {
                NSLog(@"Add Photo");
                [self call];
                selectedRow = (int)indexPath.row;
            }
        }
    }
}


#pragma mark -
#pragma mark - getMediaFromSource


//-(void)takePhotoFromCamera{
//    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unsupported!"
//                                                        message:@"Camera does not support photo capturing."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
//    else{
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        
//        [self presentViewController:picker animated:YES completion:NULL];
//        
//    }
//}
-(void)generateStockPickupId
{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {

        FMResultSet *rs = [db executeQuery:@"SELECT PICKUP_NO from StockPickup"];
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        int numNewOrderNum = 0;
        while ([rs next]) {
                numNewOrderNum  = numNewOrderNum + 1;
        }
        
        // Add one value
        numNewOrderNum = numNewOrderNum + 1;
        
        NSLog(@"numNewOrderNum %d",numNewOrderNum);
        pickup_no = [NSString stringWithFormat:@"%d",numNewOrderNum];
        [rs close];

    }];
    
}


- (void)imagePickerController:(UIImagePickerController *)thePicker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [thePicker popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:YES completion:Nil];
    
    NSData *data = UIImagePNGRepresentation(chosenImage);
    [storeImageData addObject:data];
    
    
    // set using timestamp
    
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setNumberStyle: kCFNumberFormatterNoStyle];
//    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[NSDate date] timeIntervalSince1970]]];
//    
    
    int timestampupdate = [[NSDate date] timeIntervalSince1970];
    NSString  *imagewithtimestamp = [NSString stringWithFormat:@"%d",timestampupdate];
    NSString *imageName =[NSString stringWithFormat:@"P%@",imagewithtimestamp] ;
    [photoArr addObject:imageName];;
    NSLog(@"%@",photoArr);

    if (selectedRow == 5)
    {
        [storeImageData replaceObjectAtIndex:0 withObject:data];
    }
    else
    {
    }

//    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString  *documentsDirectory = [paths objectAtIndex:0];
//    
//    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"StoreImages/"];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    BOOL isDir;
//    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
//    if (fileExists)
//    {
//        if (isDir) {
//            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
//        }
//    }
//    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
//    if (dirCreated) {
//        [storeImageData writeToFile:folderPath atomically:NO];
//    }

    
    [self.collectionView reloadData];
  }

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [popoverControllerForImage dismissPopoverAnimated:YES];
    }];
}


-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    NSLog(@"%@",NSStringFromCGRect(_closeBn.frame));
}


#pragma mark - GetData


-(NSString *)getmemberName{
    
     NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    
    return (strSalesman)?strSalesman:@"";
}

-(void)getStockCodeData{
    
    arrAllStockCode = [NSMutableArray new];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs = [db executeQuery:@"SELECT CODE,DESCRIPTION FROM samaster"];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        else
        {
            while ([rs next])
            {
                /*
                 NSString *strData = [NSString stringWithFormat:@"%@ - %@",[rs stringForColumn:@"CODE"],[rs stringForColumn:@"DESCRIPTION"]];
                 [arrAllStockCode addObject:strData];
                 */
                
                NSMutableDictionary *dict = [NSMutableDictionary new];
                NSString *strcode = [NSString stringWithFormat:@"%@",[[rs stringForColumn:@"CODE"] trimSpaces]];
                NSString *strDesc = [NSString stringWithFormat:@"%@",[[rs stringForColumn:@"DESCRIPTION"] trimSpaces]];
                
                [dict setObject:(strcode)?strcode:@"" forKey:@"CODE"];
                [dict setObject:(strDesc)?strDesc:@"" forKey:@"DESCRIPTION"];
                [arrAllStockCode addObject:dict];
                
            }
            
            [rs close];
        }
    }];
}

-(void)getCustomerData{
    
    arrAllCustCode = [NSMutableArray new];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs = [db executeQuery:@"SELECT CODE,NAME FROM armaster"];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        else
        {
            while ([rs next])
            {
                NSMutableDictionary *dict = [NSMutableDictionary new];
                NSString *strcode = [NSString stringWithFormat:@"%@",[[rs stringForColumn:@"CODE"] trimSpaces]];
                NSString *strDesc = [NSString stringWithFormat:@"%@",[[rs stringForColumn:@"NAME"] trimSpaces]];
                
                [dict setObject:(strcode)?strcode:@"" forKey:@"CODE"];
                [dict setObject:(strDesc)?strDesc:@"" forKey:@"DESCRIPTION"];
                [arrAllCustCode addObject:dict];
            }
            
            [rs close];
        }
    }];
}


-(void)call
{
    if ([popoverControllerForImage isPopoverVisible]) {
        [popoverControllerForImage dismissPopoverAnimated:YES];
        
    } else {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = YES;
        
        // create an PopoverController
        popoverControllerForImage = [[UIPopoverController alloc]
                                     initWithContentViewController:imagePicker];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [popoverControllerForImage presentPopoverFromRect:_collectionView.frame
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                                     animated:YES];
        }];
    }
}
- (IBAction)addEmailcc:(id)sender {
    
//    UIAlertView* dialog = [[UIAlertView alloc] init];
//    [dialog setDelegate:self];
//    [dialog setTitle:@"Add Email CC"];
//    [dialog setMessage:@" "];
//    [dialog addButtonWithTitle:@"Cancel"];
//    [dialog addButtonWithTitle:@"OK"];
//    dialog.tag = 5;
//    
//    UITextField *emailcctxt = [[UITextField alloc] initWithFrame:CGRectMake(20.0, 45.0, 245.0, 25.0)];
//    [emailcctxt setKeyboardType:UIKeyboardTypeEmailAddress];
//    [emailcctxt becomeFirstResponder];
//    [emailcctxt setBackgroundColor:[UIColor whiteColor]];
//    [dialog addSubview:emailcctxt];
//    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
//    [dialog setTransform: moveUp];
//    [dialog show];
//    
  }



#pragma mark
#pragma mark - UIAlertView Delegate

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    
    if (alertView.tag == 5) {
        NSString *inputText = [[alertView textFieldAtIndex:0] text];
        if([inputText length] >= 1 )
        {
            return YES;
        }
        else
        {
            return NO;
        }

    }
        return YES;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 5) {
        if (buttonIndex == 1) {
            UITextField * alertTextField = [alertView textFieldAtIndex:0];
            NSLog(@"alerttextfiled - %@",alertTextField.text);
            if (_emailCcTxt.text.length == 0) {
                _emailCcTxt.text = [_emailCcTxt.text stringByAppendingString:[NSString stringWithFormat:@"%@",alertTextField.text]];

            }
            else{
                _emailCcTxt.text = [_emailCcTxt.text stringByAppendingString:[NSString stringWithFormat:@",%@",alertTextField.text]];
  
            }
           
        }
    }
    
    
}
@end
