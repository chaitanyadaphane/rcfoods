                                                                                                                                          //
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "StockInfoProductDetailsViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "AsyncImageView.h"
#import "KGModal.h"
#import "MJProductImgViewController.h"


#define STOCK_PRODUCT_INFO_WS @"stockinfo/view_stockinfoform.php?"
#define PRODUCT_IMAGE_URL @"https://www.scmcentral.com.au/webservices_rcf/productimages/Large/"


@interface StockInfoProductDetailsViewController ()
{
    UIImage *productImg;
    MJProductImgViewController *prodImgViewControllerObj;
}

@end

NSString *strWareHouseCopy;
@implementation StockInfoProductDetailsViewController
@synthesize dictProductDetails,strProductName,strProductCode,strWarehouse,arrProductsDetailsForWebservices;
@synthesize tblDetails,lblProductName,arrSalesMonthAgoForYear0,arrSalesMonthAgoForYear1,dictSalesQuantityDetails,spinner,arrProductLabels,isCostMarginVisible;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"StockInfoProductDetailsWithoutCostMargin" Type:@"plist"];
    
    // Build the array from the plist
    arrProductLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
    arrProductsDetailsForWebservices = [[NSMutableArray alloc] init];
    dictProductDetails = [[NSMutableDictionary alloc] init];
    arrSalesMonthAgoForYear0 = [[NSMutableArray alloc] init];
    arrSalesMonthAgoForYear1 = [[NSMutableArray alloc] init];
    dictSalesQuantityDetails = [[NSMutableDictionary alloc] init];
  
    strWareHouseCopy = [strWarehouse copy];
    lblProductName.text = [strProductName trimSpaces];
    
    self.isCostMarginVisible = NO;
    
    if (!backgroundQueueForStockProductDetails) {
        backgroundQueueForStockProductDetails = dispatch_queue_create("com.nanan.myscmipad.bgqueueForStockProductDetails", NULL);
    }
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        [self callWSGetProductListDetails];
    }
    else
    {
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(backgroundQueueForStockProductDetails, ^(void) {
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetProductListDetailsFromDB:db];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [spinner removeFromSuperview];
                [tblDetails reloadData];
            });
        });
    }
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.lblProductName = nil;
    self.tblDetails = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}


#pragma mark - Text Field Delgates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  CGRect viewFrame = self.view.frame;
    // viewFrame.origin.y = upframeByYPosition;
    // [AppDelegate pullUpPushDownViews:self.view ByRect:viewFrame];
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.section == 0)
    {
        if (indexPath.row == 1) {
            return 80;
        }
    }
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return [arrProductLabels count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)[arrProductLabels objectAtIndex:section] count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier17 = CELL_IDENTIFIER17;

    CustomCellGeneral *cell ;
    CustomCellGeneral *cell17 ;

    
    if(indexPath.section == 0 && indexPath.row == 1)
    {
        cell17 = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier17];

        if (cell17 == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell17 = [nib objectAtIndex:17];
            
        }
    }
    else
    {
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];

        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell = [nib objectAtIndex:4];
            
        }
    }
   
    @try {
        
        cell.lblTitle.text = [[(NSArray *)[arrProductLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        
        cell.lblValue.text = nil;
        switch ([indexPath section]) {
            case 0:{
                if ([indexPath row] == 0) {
                    if ([dictProductDetails objectForKey:@"Code"]) {
                        if([[dictProductDetails objectForKey:@"Code"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Code"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Code"]];
                        }
                        else if([[dictProductDetails objectForKey:@"Code"]objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Code"]objectForKey:@"text"]];
                        }
                        else
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Code"]];
                    }
                }
                
            }
                
                break;
                
            case 1:{
                if (isCostMarginVisible) {
                    if ([indexPath row] == 0) {
//                        [dictProductDetails objectForKey:@"AverageCost"]
                        if ([dictProductDetails objectForKey:@"Cost"]) {
                            if([[dictProductDetails objectForKey:@"Cost"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Cost"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",[[dictProductDetails objectForKey:@"Cost"] floatValue]];
                            }
                            else if([[dictProductDetails objectForKey:@"Cost"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",[[[dictProductDetails objectForKey:@"Cost"] objectForKey:@"text"] floatValue]];
                            }
                            else{
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",[[dictProductDetails objectForKey:@"Cost"] floatValue]];
                            }
                        }
                        //                        [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
                    }
                    if ([indexPath row] == 1) {
                        if ([dictProductDetails objectForKey:@"Margin"]) {
                            NSString *percent = @"%";
                            
                            if([[dictProductDetails objectForKey:@"Margin"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Margin"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%.2f %@",[[dictProductDetails objectForKey:@"Margin"] floatValue],percent];
                            }
                            
                            else if([[dictProductDetails objectForKey:@"Margin"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@%% %@",[[dictProductDetails objectForKey:@"Margin"]objectForKey:@"text"],percent];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@%% %@",[dictProductDetails objectForKey:@"Margin"],percent];
                        }
                        
                    }
                    if ([indexPath row] == 4) {
                        if ([dictProductDetails objectForKey:@"MtdQty"]) {
                            if([[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"MtdQty"] floatValue]];
                            }
                            else if([[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"MtdQty"]];
                        }
                    }
                    if ([indexPath row] == 2) {
                        
                        if ([dictProductDetails objectForKey:@"OnHand"]) {
                            
                            if([[dictProductDetails objectForKey:@"OnHand"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"OnHand"] isKindOfClass:[NSNumber class]])
                            {
                                //cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"OnHand"] floatValue]];
                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"OnHand"] ];
                            }
                            
                            else if([[dictProductDetails objectForKey:@"OnHand"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"OnHand"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"OnHand"]];
                        }
                    }
                    if ([indexPath row] == 3) {
                        if ([dictProductDetails objectForKey:@"Available"]) {
                            
                            if([[dictProductDetails objectForKey:@"Available"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Available"] isKindOfClass:[NSNumber class]])
                            {
                                //                  cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"Available"] floatValue]];
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Available"]];
                                
                            }
                            else if([[dictProductDetails objectForKey:@"Available"] objectForKey:@"text"])
                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Available"]objectForKey:@"text"]];
                            
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Available"]];
                        }
                    }
                    if ([indexPath row] == 5) {
                        if ([dictProductDetails objectForKey:@"Location"]) {
                            
                            if([[dictProductDetails objectForKey:@"Location"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Location"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Location"] ];
                            }
                            
                            else if([[dictProductDetails objectForKey:@"Location"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Location"]objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Location"]];
                        }
                    }
                    if ([indexPath row] == 6) {
                        if ([dictProductDetails objectForKey:@"PurchaseOrder"]) {
                            
                            if([[dictProductDetails objectForKey:@"PurchaseOrder"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"PurchaseOrder"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%.f",[[dictProductDetails objectForKey:@"PurchaseOrder"] floatValue]];
                            }
                            
                            else if([[dictProductDetails objectForKey:@"PurchaseOrder"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"PurchaseOrder"]objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"PurchaseOrder"]];
                        }
                    }
                    //Date of arrival
                    if ([indexPath row] == 7) {
                        if ([dictProductDetails objectForKey:@"DeliveryDate"] && ![[dictProductDetails objectForKey:@"DeliveryDate"] isEqual:[NSNull null]] ) {
                            
                            NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"DeliveryDate"]];
                            
                            if ([strDate isEqualToString:DEFAULT_DATE]) {
                                cell.lblValue.text = NOT_ORDERED;
                            }
                            else{
                                cell.lblValue.text = strDate;
                            }
                        }
                        else{
                            cell.lblValue.text = NOT_ORDERED;
                        }
                    }
                    if ([indexPath row] == 8) {
                        if ([dictProductDetails objectForKey:@"Allocated"]) {
                            if([[dictProductDetails objectForKey:@"Allocated"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Allocated"] isKindOfClass:[NSNumber class]])
                            {
                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"Allocated"] floatValue]];
                            }
                            
                            else if([[dictProductDetails objectForKey:@"Allocated"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Allocated"]objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Allocated"]];
                        }
                    }
                    
                }
                else{
                    if ([indexPath row] == 0) {
                        if ([dictProductDetails objectForKey:@"OnHand"]) {
                            
                            if([[dictProductDetails objectForKey:@"OnHand"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"OnHand"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"OnHand"]];
                                
                            }
                            
                            else if([[dictProductDetails objectForKey:@"OnHand"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"OnHand"]objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"OnHand"]];
                        }
                    }
                    if ([indexPath row] == 1) {
                        if ([dictProductDetails objectForKey:@"Available"]) {
                            
                            if([[dictProductDetails objectForKey:@"Available"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Available"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Available"]];
                                
                            }
                            else if([[dictProductDetails objectForKey:@"Available"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Available"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Available"]];
                        }
                    }
                    
                    if ([indexPath row] == 2) {
                        if ([dictProductDetails objectForKey:@"MtdQty"]) {
                            if([[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"MtdQty"] floatValue]];
                            }
                            else if([[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"MtdQty"]];
                        }
                    }
                    
                    if ([indexPath row] == 3) {
                        if ([dictProductDetails objectForKey:@"Location"]) {
                            if([[dictProductDetails objectForKey:@"Location"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Location"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Location"] ];
                            }
                            
                            else  if([[dictProductDetails objectForKey:@"Location"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Location"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Location"]];
                        }
                    }
                    if ([indexPath row] == 4) {
                        if ([dictProductDetails objectForKey:@"PurchaseOrder"]) {
                            
                            if([[dictProductDetails objectForKey:@"PurchaseOrder"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"PurchaseOrder"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"PurchaseOrder"]] ;
                                [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
                                cell.lblValue.text = [cell.lblValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                                return cell;
                            }
                            else if([[dictProductDetails objectForKey:@"PurchaseOrder"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"PurchaseOrder"]objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"PurchaseOrder"]];
                        }
                    }
                    //Date of arrival
                    if ([indexPath row] == 5) {
                        if ([dictProductDetails objectForKey:@"DeliveryDate"]&&![[dictProductDetails objectForKey:@"DeliveryDate"] isEqual:[NSNull null]]) {
                            
                            NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"DeliveryDate"]];
                            
                            if ([strDate isEqualToString:DEFAULT_DATE]) {
                                cell.lblValue.text = NOT_ORDERED;
                            }
                            else{
                                cell.lblValue.text = strDate;
                            }
                        }
                        else{
                            cell.lblValue.text = NOT_ORDERED;
                        }
                    }
                    if ([indexPath row] == 6) {
                        if ([dictProductDetails objectForKey:@"Allocated"]) {
                            if([[dictProductDetails objectForKey:@"Allocated"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Allocated"] isKindOfClass:[NSNumber class]])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"Allocated"] floatValue]];
                            }
                            else if([[dictProductDetails objectForKey:@"Allocated"] objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Allocated"] objectForKey:@"text"]];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Allocated"]];
                        }
                    }
                    
                }
            }
                
                break;
                
            case 2:{
                if ([indexPath row] == 0) {
                    if([dictProductDetails objectForKey:@"Price1"])
                    {
                        
                        if([[dictProductDetails objectForKey:@"Price1"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Price1"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price1"] floatValue]];

                        }
                        
                        else if([[dictProductDetails objectForKey:@"Price1"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price1"] floatValue]];

                        }
                        else
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price1"] floatValue]];

                        }
                    }
                    
                }
                if ([indexPath row] == 1) {
                    if ([dictProductDetails objectForKey:@"Price2"]) {
                        if([[dictProductDetails objectForKey:@"Price2"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Price2"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price2"] floatValue]];
                        }
                        
                        else if([[dictProductDetails objectForKey:@"Price2"] objectForKey:@"text"])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price2"] floatValue]];
                        }
                        else
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price2"] floatValue]];
                    }
                }
                if ([indexPath row] == 2) {
                    if ([dictProductDetails objectForKey:@"Price3"]) {
                        if([[dictProductDetails objectForKey:@"Price3"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Price3"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price3"] floatValue]];
                        }
                        
                        else if([[dictProductDetails objectForKey:@"Price3"]objectForKey:@"text"])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price3"] floatValue]];
                        }
                        else
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price3"] floatValue]];
                    }
                }
                if ([indexPath row] == 3) {
                    if ([dictProductDetails objectForKey:@"Price4"]) {
                        if([[dictProductDetails objectForKey:@"Price4"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"Price4"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price4"] floatValue]];
                        }
                        
                        
                        else if([[dictProductDetails objectForKey:@"Price4"]objectForKey:@"text"])
                        {
                            
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price4"] floatValue]];
                        }
                        else
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Price4"] floatValue]];
                    }
                }

                if ([indexPath row] == 5) {
                    if ([dictProductDetails objectForKey:@"MtdQty"]) {
                        if([[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSString class]] || [[dictProductDetails objectForKey:@"MtdQty"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%.2f",[[dictProductDetails objectForKey:@"MtdQty"] floatValue]];
                        }
                        
                        else if([[dictProductDetails objectForKey:@"MtdQty"]objectForKey:@"text"])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"MtdQty"]objectForKey:@"text"]];
                        }
                        else
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"MtdQty"]];
                    }
                }
            }
                break;
                
            case 3:{
                if ([indexPath row] == 0) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales1Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales1Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 1) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales2Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales2Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 2) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales3Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales3Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 3) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales4Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales4Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 4) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales5Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales5Month"] floatValue]];
                    }
                }
                if ([indexPath row] == 5) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales6Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales6Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 6) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales7Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales7Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 7) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales8Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales8Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 8) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales9Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales9Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 9) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales10Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales10Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 10) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales11Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales11Month"]floatValue]];
                    }
                }
                if ([indexPath row] == 11) {
                    if ([dictSalesQuantityDetails objectForKey:@"Sales12Month"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[[dictSalesQuantityDetails objectForKey:@"Sales12Month"]floatValue]];
                    }
                }
                
            }
                break;
            default:
                break;
        }
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"Error:::%@",exception.description);
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width,30)];
  
    UILabel *headerLabel;
  
    if(IOS_Version < 7.0)
    {
      headerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, headerView.frame.size.width-120.0, headerView.frame.size.height)];
    }
    else
    {
        headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, headerView.frame.size.width-120.0, headerView.frame.size.height)];
    }
  
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.font = [UIFont boldSystemFontOfSize:22];
  
    headerLabel.backgroundColor = [UIColor clearColor];
  
    [headerView addSubview:headerLabel];
  
    switch (section) {
        case 1:{
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
          
            if (isCostMarginVisible) {
                [button setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
            }
            else{
                [button setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
            }
            
            //headerView.frame.size.width/2.0
            [button setFrame:CGRectMake(435.0, 3.0, 30.0, 30.0)];
            button.tag = section;
            button.hidden = NO;
            [button setBackgroundColor:[UIColor clearColor]];
            [button addTarget:self action:@selector(showCostMargin:) forControlEvents:UIControlEventTouchDown];
            [headerView addSubview:button];
            headerLabel.text = @"Stock info";
        }
            break;
        case 2:{
            headerLabel.text = @"Price";
        }break;
            
        case 3:{
            headerLabel.text = @"Sales (months ago)";
        }break;
            
        default:{
            headerLabel.text = @"";
            
        }break;
    }
    
    return headerView;
}

#pragma mark - Custom methods


-(IBAction)showImage:(id)sender
{
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    
    if (prodImgViewControllerObj) {
        [self dismissPopupViewControllerWithanimationType:nil];
        prodImgViewControllerObj = nil;
    }
    prodImgViewControllerObj = [[MJProductImgViewController alloc] initWithNibName:@"MJProductImgViewController" bundle:nil];
    
    prodImgViewControllerObj.view.frame = CGRectMake(10, 10, 300, 300);
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PRODUCT_IMAGE_URL,[dictProductDetails objectForKey:@"Picture_field"]];
    
    prodImgViewControllerObj.prod_img_url =imageUrl;
    // .....
    prodImgViewControllerObj.productImg.imageURL = [NSURL URLWithString:imageUrl];
    [contentView addSubview:prodImgViewControllerObj.view];
    //    [self presentPopupViewController:prodImgViewControllerObj
    //                       animationType:3];
    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
    
}



-(void)showCostMargin:(id)sender{
    isCostMarginVisible = (isCostMarginVisible)? NO: YES;
    
    UIButton *btn = (UIButton *)sender;
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray addObject:[NSIndexPath indexPathForRow:0 inSection:btn.tag]];
    [tempArray addObject:[NSIndexPath indexPathForRow:1 inSection:btn.tag]];
    
    NSMutableArray *arr = [arrProductLabels objectAtIndex:btn.tag];
    
    if(isCostMarginVisible){
        [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
        NSMutableDictionary *dictCost = [[NSMutableDictionary alloc] init];
        [dictCost setValue:@"Cost" forKey:@"Label"];
        [arr insertObject:dictCost atIndex:0];
        
        NSMutableDictionary *dictMargin = [[NSMutableDictionary alloc] init];
        [dictMargin setValue:@"Margin" forKey:@"Label"];
        [arr insertObject:dictMargin atIndex:1];
        
        [tblDetails beginUpdates];
        [tblDetails insertRowsAtIndexPaths:(NSArray *)tempArray withRowAnimation:UITableViewRowAnimationBottom];
        [tblDetails endUpdates];

    }
    else{
        [btn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        [arr removeObjectAtIndex:0];
        [arr removeObjectAtIndex:0];
        [tblDetails beginUpdates];
        [tblDetails deleteRowsAtIndexPaths:(NSArray *)tempArray  withRowAnimation:UITableViewRowAnimationFade];
        [tblDetails endUpdates];

    }
}

#pragma mark - Database calls
-(void)callGetProductListDetailsFromDB:(FMDatabase *)db{
    
    FMResultSet *results1 = [db executeQuery:@"SELECT a.CODE as Code, a.DESCRIPTION as Description, a.ON_HAND as OnHand, a.AVAILABLE as Available, a.ALLOCATED as Allocated, a.PURCHASE_ORDER as PurchaseOrder, a.PRICE1 as Price1, a.PRICE2 as Price2, a.PRICE3 as Price3, a.PRICE4 as Price4, a.PRICE5 as Price5, a.PICTURE_FIELD as Picture_field, a.LOCATION as Location, a.MTD_QTY as MtdQty,a.MARGIN01 as Margin,a.STANDARD_COST as Cost,a.AVERAGE_COST as AverageCost,b.DELIVERY_DATE as DeliveryDate FROM samaster a LEFT OUTER JOIN podetail b ON TRIM(a.CODE) = TRIM(b.STOCK_CODE) WHERE TRIM(a.CODE) = ?   AND TRIM(a.WAREHOUSE) = ? ORDER BY b.DELIVERY_DATE desc",[strProductCode trimSpaces],[strWarehouse trimSpaces]];//  AND a.STATUS ='OB'

  
    if ([results1 next])
    {
        self.dictProductDetails = [NSMutableDictionary new];
        self.dictProductDetails = (NSMutableDictionary *)[results1 resultDictionary];
    }
    else
    {
        NSLog(@"No Details exist for this code");
    }
    
    if (!results1)
    {
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
    }
    
    //FMResultSet *results2 = [db executeQuery:@"SELECT sa.CODE , s.* FROM samaster sa , sahisbud s WHERE ((sa.WAREHOUSE || sa.CODE) = s.INDEX_FIELD)  AND s.YEAR = ? AND sa.CODE = ? AND sa.WAREHOUSE = ?",@"1",strProductCode,strWarehouse];
    
    if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
//        NSString *strWarehouseCode = [NSString stringWithFormat:@"%@%@",strWareHouseCopy ,strProductCode ];
        
        NSString *strWarehouseCode = [NSString stringWithFormat:@"%@%@",strWareHouseCopy ,strProductCode ];

        
//              FMResultSet *results2 = [db executeQuery:@"SELECT QUANTITY01,QUANTITY02,QUANTITY03,QUANTITY04,QUANTITY05,QUANTITY06,QUANTITY07,QUANTITY08,QUANTITY09,QUANTITY10,QUANTITY11,QUANTITY12,YEAR FROM sahisbud WHERE trim(INDEX_FIELD) = ? ORDER BY YEAR ASC",[strWarehouseCode trimSpaces]];
        FMResultSet *results2 = [db executeQuery:@"SELECT QUANTITY01,QUANTITY02,QUANTITY03,QUANTITY04,QUANTITY05,QUANTITY06,QUANTITY07,QUANTITY08,QUANTITY09,QUANTITY10,QUANTITY11,QUANTITY12,YEAR FROM sahisbud WHERE REPLACE(INDEX_FIELD,' ','')  = ? ORDER BY YEAR ASC",[strWarehouseCode trimSpaces]];

      
        if (!results2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([results2 next]) {
            NSDictionary *dictData = [results2 resultDictionary];
            
            if ([[dictData objectForKey:@"YEAR"] intValue] == 0){
                [arrSalesMonthAgoForYear0 addObject:dictData];
            }
            
            if ([[dictData objectForKey:@"YEAR"] intValue] == 1){
                [arrSalesMonthAgoForYear1 addObject:dictData];
            }
            
        }
        
        //NSLog(@"arrSalesMonthAgoForYear0 :%@",arrSalesMonthAgoForYear0);
        //NSLog(@"arrSalesMonthAgoForYear1 :%@",arrSalesMonthAgoForYear1);
        //if ([arrSalesMonthAgoForYear0 count] && [arrSalesMonthAgoForYear1 count]) {
            int currentMonth = [[AppDelegate getCurrentMonth] intValue];
            
            //July start of the financial year for Australia
            if(currentMonth == 7)
            {
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales12Month"];
                    
                }
            }
            
            if(currentMonth == 8)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales1Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales12Month"];
                }
            }
            
            if(currentMonth == 9)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales2Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales12Month"];
                }
                
            }
            
            if(currentMonth == 10)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales3Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales12Month"];
                }
            }
            
            if(currentMonth == 11)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales4Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales12Month"];
                }
            }
            
            if(currentMonth == 12)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales5Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales12Month"];
                }
                
            }
            
            if(currentMonth == 1)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales6Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales12Month"];
                }
            }
            
            if(currentMonth == 2)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales7Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales12Month"];
                }
                
            }
            
            if(currentMonth == 3)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales8Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales12Month"];
                }
            }
            
            if(currentMonth == 4)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales9Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales12Month"];
                }
                
            }
            
            if(currentMonth == 5)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales10Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales11Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales12Month"];
                }
                
            }
            
            if(currentMonth == 6)
            {
                if ([arrSalesMonthAgoForYear0 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY11"] forKey:@"Sales1Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY10"] forKey:@"Sales2Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY09"] forKey:@"Sales3Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY08"] forKey:@"Sales4Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY07"] forKey:@"Sales5Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY06"] forKey:@"Sales6Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY05"] forKey:@"Sales7Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY04"] forKey:@"Sales8Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY03"] forKey:@"Sales9Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY02"] forKey:@"Sales10Month"];
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear0 objectAtIndex:0] objectForKey:@"QUANTITY01"] forKey:@"Sales11Month"];
                }
                
                if ([arrSalesMonthAgoForYear1 count]) {
                    [dictSalesQuantityDetails setValue:[[arrSalesMonthAgoForYear1 objectAtIndex:0] objectForKey:@"QUANTITY12"] forKey:@"Sales12Month"];
                }
            }
            
        //}
    }
    
}

#pragma mark - WS calls

-(void)callWSGetProductListDetails{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_STOCK_PRODUCT_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"code=%@&warehouse=%@",strProductCode,strWarehouse];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,STOCK_PRODUCT_INFO_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    if ([strWebserviceType isEqualToString:@"WS_STOCK_PRODUCT_DETAILS"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Flag"]) {
            
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    self.dictProductDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Data"];
                    
                    NSDictionary *dictTempSalesQuantityDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"SalesQuantity"];
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales1Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales1Month"] objectForKey:@"text"] forKey:@"Sales1Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales2Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales2Month"] objectForKey:@"text"] forKey:@"Sales2Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales3Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales3Month"] objectForKey:@"text"] forKey:@"Sales3Month"];
                    }
                    
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales4Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales4Month"] objectForKey:@"text"] forKey:@"Sales4Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales5Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales5Month"] objectForKey:@"text"] forKey:@"Sales5Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales6Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales6Month"] objectForKey:@"text"] forKey:@"Sales6Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales7Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales7Month"] objectForKey:@"text"] forKey:@"Sales7Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales8Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales8Month"] objectForKey:@"text"] forKey:@"Sales8Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales9Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales9Month"] objectForKey:@"text"] forKey:@"Sales9Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales10Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales10Month"] objectForKey:@"text"] forKey:@"Sales10Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales11Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales11Month"] objectForKey:@"text"] forKey:@"Sales11Month"];
                    }
                    
                    if ([[dictTempSalesQuantityDetails objectForKey:@"Sales12Month"] objectForKey:@"text"]) {
                        [dictSalesQuantityDetails setValue:[[dictTempSalesQuantityDetails objectForKey:@"Sales12Month"] objectForKey:@"text"] forKey:@"Sales12Month"];
                    }
                    
                    dictTempSalesQuantityDetails = nil;
                    
                }
                
                dispatch_async(backgroundQueueForStockProductDetails, ^(void) {
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        @try
                        {
                            BOOL y;
                            
                            NSString *strCode = @"";
                            if ([[dictProductDetails objectForKey:@"Code"] objectForKey:@"text"]) {
                                strCode = [[dictProductDetails objectForKey:@"Code"] objectForKey:@"text"];
                            }
                            
                            NSString *strDescription = @"";
                            if ([[dictProductDetails objectForKey:@"Description"] objectForKey:@"text"]) {
                                strDescription = [[dictProductDetails objectForKey:@"Description"] objectForKey:@"text"];
                            }
                            
                            NSString *strLocation = @"";
                            if ([[dictProductDetails objectForKey:@"Location"] objectForKey:@"text"]) {
                                strLocation = [[dictProductDetails objectForKey:@"Location"] objectForKey:@"text"];
                            }
                            
                            FMResultSet *rs = [db executeQuery:@"SELECT Recnum FROM samaster WHERE Recnum = ?",[[dictProductDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                            
                            //[[dictProductDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]
                            
                            if (!rs) {
                                [rs close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            if ([rs next]) {
                                
                                y = [db executeUpdate:@"UPDATE samaster SET ALLOCATED = ?,AVAILABLE = ?,CODE = ?,DESCRIPTION = ?,LOCATION = ?,ON_HAND = ?,PRICE1 = ?,PRICE2 = ?,PRICE3 = ?,PRICE4 = ?,PRICE5 = ?,PURCHASE_ORDER = ?,MTD_QTY = ?,RECNUM = ?,AVERAGE_COST = ? WHERE Recnum = ?",
                                     [[dictProductDetails objectForKey:@"Allocated"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Available"] objectForKey:@"text"],
                                     strCode,
                                     strDescription,
                                     strLocation,
                                     [[dictProductDetails objectForKey:@"OnHand"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Price1"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Price2"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Price3"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Price4"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Price5"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"PurchaseOrder"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Recnum"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Cost"] objectForKey:@"text"],
                                     [[dictProductDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                                
                            }
                            
                            //                            else{
                            //                                y = [db executeUpdate:@"INSERT OR REPLACE INTO samaster (ALLOCATED,AVAILABLE,CODE,DESCRIPTION,LOCATION,ON_HAND,PRICE1,PRICE2,PRICE3,PRICE4,PRICE5,PURCHASE_ORDER,MTD_QTY,RECNUM,SCM_RECNUM,AVERAGE_COST) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            //                                     [[dictProductDetails objectForKey:@"Allocated"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Available"] objectForKey:@"text"],
                            //                                     strCode,
                            //                                     strDescription,
                            //                                     strLocation,
                            //                                     [[dictProductDetails objectForKey:@"OnHand"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Price1"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Price2"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Price3"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Price4"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Price5"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"PurchaseOrder"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"MtdQty"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Recnum"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],
                            //                                     [[dictProductDetails objectForKey:@"Cost"] objectForKey:@"text"]];
                            //                            }
                            
                            [rs close];
                            
                            if (!y)
                            {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            *rollback = NO;
                            
                            [self callGetProductListDetailsFromDB:db];
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                [spinnerObj removeFromSuperview];
                                [tblDetails reloadData];
                            });
                            
                        }
                        @catch(NSException* e)
                        {
                            *rollback = YES;
                            // rethrow if not one of the two exceptions above
                            self.dictProductDetails = nil;
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                [spinnerObj removeFromSuperview];
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                
                            });
                            //[db close];
                        }
                        
                    }];
                    
                });
            }
            else{
                
                [spinnerObj removeFromSuperview];
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Message"] objectForKey:@"text"];

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
        else{
            
//            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"StockForm"] objectForKey:@"Message"] objectForKey:@"text"];
            
            NSString *strErrorMessage = @"Couldn't fetch data";

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods

-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

-(void)NSDicttest:(NSDictionary *)dict
{
//  if ([dict isKindOfClass:[NSDictionary class]]) {
//   
//    NSArray *ary = [dict allKeys];
//    NSString *strdc = @"";
//    for(int k = 0; k < ary.count; k++)
//    {
//      objectForKey:@"Label"
//      NSString *st = [NSString stringWithFormat:@"objectForKey:@%@,[ary objectAtIndex:k]];
//      strdc = [strdc stringByAppendingString:st];
//      NSLog(@"strdc:::%@::::%@",strdc,[ary objectAtIndex:k]);
//    }
//      
//  }

}

@end
