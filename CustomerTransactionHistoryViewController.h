//
//  CustomerTransactionViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"

#define TAG_50 50
#define TAG_100 100

@interface CustomerTransactionHistoryViewController : UIViewController<ASIHTTPRequest_delegate>{
    
    SEFilterControl *segControl;
    
    NSArray *arrHeaderLabels;

    NSString *strWebserviceType;
    
    NSString *strTranCode;
    
    NSMutableDictionary *dictCustomerDetails;
    NSMutableArray *arrCustomerTransactions;
    
    BOOL isFromDebtorHistory;
    
    BOOL isNetworkConnected;
    BOOL isFromCustomerSpecials;
    
    dispatch_queue_t backgroundQueueForViewCustomerTransactionHistory;
    FMDatabaseQueue *dbQueueViewCustomerTransactionHistory;
    
}

@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSString *strTranCode;
@property (nonatomic,retain) NSMutableDictionary *dictCustomerDetails;
@property (nonatomic,retain) NSMutableArray *arrCustomerTransactions;
@property (nonatomic,assign) BOOL isFromDebtorHistory;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblHeader;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoTransactions;
@property (nonatomic,retain) IBOutlet UIView *vwNoPayment;
@property (nonatomic,retain) IBOutlet UIView *vwNoSalesHistory;

@property (nonatomic,retain) MBProgressHUD *spinner;

-(void)callViewCustomerTransactionHistoryFromDB:(FMDatabase *)db;

@end
