//
//  CustomerTransactionViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CustomerTransactionViewController.h"
#import "CustomCellGeneral.h"
#import "CustomerTransactionHistoryViewController.h"
#import "CustomerTransactionDetailViewController.h"
#import "CustomerHistoryDetailViewController.h"

#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10

#define CUSTOMER_TRANSACTION_WS @"custrans/view_custrans.php?"
#define CUSTOMER_HISTORY_WS @"custhistory/view_custhistory.php?"

@interface CustomerTransactionViewController ()
{
    MBProgressHUD *spinner;
    BOOL isPeriod30;
}
@end

@implementation CustomerTransactionViewController

@synthesize arrHeaderLabels,strCustCode,dictCustomerDetails,arrCustomerTransactions,isFromDebtorHistory;
@synthesize tblHeader,tblDetails,vwContentSuperview,vwSegmentedControl,vwHeader,vwDetails,vwNoTransactions,lblHeader,lblInfoText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    isPeriod30 = NO;
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
    dictCustomerDetails = [[NSMutableDictionary alloc] init];
    arrCustomerTransactions = [[NSMutableArray alloc] init];
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [tblDetails addGestureRecognizer:longPressRecognizer];
    
    if (!backgroundQueueForViewCustomerTransaction) {
        backgroundQueueForViewCustomerTransaction = dispatch_queue_create("com.nanan.myscmipad.bgqueueForViewCustomerTransaction", NULL);
    }
    
    
    NSString *path;
    if (isFromDebtorHistory) {
        lblHeader.text = @"Customer History Info";
        
        // Path to the plist (in the application bundle)
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerHistoryHeader" Type:@"plist"];
        
        // Build the array from the plist
        arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
        
////        dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
//        
//            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//            [databaseQueue   inDatabase:^(FMDatabase *db) {
//                [self callViewCustomerHistoryFromDB:db];
//            }];
//            dispatch_async(dispatch_get_main_queue(), ^(void) {
//                [tblHeader reloadData];
//            });
//        
////        });

        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
            [self callWSViewCustomerHistory];
        }
        else{
        
            dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callViewCustomerHistoryFromDB:db];
                    
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [tblHeader reloadData];
                });
            });
        }
    }
    else{
        lblHeader.text = @"Customer Transaction Info";
        // Path to the plist (in the application bundle)
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionHeader" Type:@"plist"];
        
        // Build the array from the plist
        arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
        
//            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//            [databaseQueue   inDatabase:^(FMDatabase *db) {
//                [self callViewCustomerTransactionFromDB:db];
//                
//            }];
//            
//            dispatch_async(dispatch_get_main_queue(), ^(void) {
//                [tblHeader reloadData];
//            });
            
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
            [self callWSViewCustomerTransaction];
        }
        else{
            dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callViewCustomerTransactionFromDB:db];
                    
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [tblHeader reloadData];
                });
                
            });
        }
        
    }
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.vwContentSuperview = nil;
    self.vwSegmentedControl = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoTransactions = nil;
    self.lblHeader = nil;
    self.lblInfoText = nil;
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self removeFadeInFadeOutAnimations];
    
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
            [tblHeader reloadData];
        }
            break;
            
        case 1:{
            
            if ([arrCustomerTransactions count]) {
                    if(isFromDebtorHistory)
                    {
                            NSMutableArray *tempArr = [NSMutableArray new];
                            for (NSDictionary *dic in arrCustomerTransactions) {
                                    
                                    NSMutableDictionary *newDic = [NSMutableDictionary new];
                                    NSArray *arr = [dic allKeys];
                                    for (NSString *str in arr) {
                                            [newDic setValue:[[dic valueForKey:str] valueForKey:@"text"] forKey:str];
                                    }
                                    [tempArr addObject:newDic];
                            }
                            arrCustomerTransactions = [tempArr mutableCopy];

                    }
                  
                            [vwContentSuperview addSubview:vwDetails];
                            lblInfoText.text = LONG_PRESS_CUSTOMER_HISTORY_INFO_MESSAGE;
                            [self addFadeInFadeOutAnimations];
                            [tblDetails reloadData];
                    
            }
            else{
                //No Transactions
                [vwContentSuperview addSubview:vwNoTransactions];
                
            }
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding

            if (indexPath.section == 1 && ([indexPath row] != 1 || [indexPath row] != 2)) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictCustomerDetails objectForKey:@"Name"];
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = [dictCustomerDetails objectForKey:@"DeliveryAddress"];
                    }
                        break;
                        
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
            }
            else{
                return nil;break;
            }
            
            
            
        }break;
            
        case TAG_100:return indexPath;break;
            
        default:return nil;break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 1) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictCustomerDetails objectForKey:@"Name"];
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = [dictCustomerDetails objectForKey:@"DeliveryAddress"];
                    }
                        break;
                        
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                    //return 60;
                }
            }
            else{
                return 60;break;
            }
            
            
            
        }break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];break;
            
        case TAG_100:return [arrCustomerTransactions count];break;
            
        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier9 = CELL_IDENTIFIER9;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
    @try {
        switch (tableView.tag) {
            case TAG_50:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:4];
                    
                }
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
                
                if (selectedSectionIndex == 1 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
                
                
                if (isFromDebtorHistory)
                {
                    if ([indexPath section] == 0)
                    {
                        switch ([indexPath row])
                        {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"Code"]) {
                                    
                                    if([[dictCustomerDetails objectForKey:@"Code"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Code"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Code"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Code"]objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Code"] objectForKey:@"text"]];
                                    }
                                    else
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Code"]];
                                    }
                                    return cell;
                                }
                            default:break;
                        }
                    }
                    
                    
                    if ([indexPath section] == 1) {
                        switch ([indexPath row]) {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"Name"]) {
                                    if([[dictCustomerDetails objectForKey:@"Name"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Name"] trimSpaces]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Name"]objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Name"]objectForKey:@"text"] trimSpaces]];
                                    }
                                }
                                
                                return cell;
                                break;
                                
                            default:break;
                        }
                    }
                    
                    // }
                    
                    
                    return cell;
                    
                }
                else{
                    
                    if ([indexPath section] == 0) {
                        switch ([indexPath row]) {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"CentralDebtor"]) {
                                    if([[dictCustomerDetails objectForKey:@"CentralDebtor"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"CentralDebtor"]trimSpaces]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"CentralDebtor"]objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"CentralDebtor"]objectForKey:@"text"]trimSpaces]];
                                    }
                                    return cell;
                                }break;
                            default:break;
                        }
                    }
                    
                    
                    if ([indexPath section] == 1) {
                        switch ([indexPath row]) {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"Name"]) {
                                    if([[dictCustomerDetails objectForKey:@"Name"] isKindOfClass:[NSString class]])
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Name"] trimSpaces]];
                                    else if([[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"] trimSpaces]];
                                    }
                                }
                                 return cell;
                                break;
                            case 1:
                                
                                if ([dictCustomerDetails objectForKey:@"DeliveryAddress"]) {
                                    
                                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"DeliveryAddress"]];
                                }
                                 return cell;
                                break;
                                
                            case 2:
                                
                                if ([dictCustomerDetails objectForKey:@"DelSuburb"]) {
                                    
                                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"DelSuburb"]];
                                }
                                 return cell;
                                break;
                                
                            case 3:
                                
                                if ([dictCustomerDetails objectForKey:@"Phone"]) {
                                    if([[dictCustomerDetails objectForKey:@"Phone"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Phone"] trimSpaces]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Phone"]objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Phone"]objectForKey:@"text"] trimSpaces]];
                                    }
                                    
                                }
                                 return cell;
                                break;
                            case 4:
                                
                                if ([dictCustomerDetails objectForKey:@"Contact1"]) {
                                    if([[dictCustomerDetails objectForKey:@"Contact1"] isKindOfClass:[NSString class]])
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Contact1"] trimSpaces]];
                                    else if([[[dictCustomerDetails objectForKey:@"Contact1"] objectForKey:@"text"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Contact1"] objectForKey:@"text"] trimSpaces]];
                                    }
                                }
                                 return cell;
                                break;
                                
                            default:break;
                        }
                    }
                    
                    if ([indexPath section] == 2) {
                        
                        switch ([indexPath row]) {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"Terms"]) {
                                    cell.lblValue.text = [NSString stringWithFormat:@"%@ days",[dictCustomerDetails objectForKey:@"Terms"]];
                                    
                                }
                                 return cell;
                                break;
                                
                            case 1:
                                if ([dictCustomerDetails objectForKey:@"TradingTerms"]) {
                                    if([[dictCustomerDetails objectForKey:@"TradingTerms"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] getTradingTerms:[dictCustomerDetails objectForKey:@"TradingTerms"]]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] getTradingTerms:[[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"]]];
                                        
                                    }
                                    
                                }
                                 return cell;
                                break;
                            case 2:
                                if ([dictCustomerDetails objectForKey:@"Hold"]) {
                                    if([[dictCustomerDetails objectForKey:@"Hold"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Hold"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Hold"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Hold"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                 return cell;
                                break;
                            case 3:
                                if ([dictCustomerDetails objectForKey:@"CreditLimit"]) {
                                    if([[dictCustomerDetails objectForKey:@"CreditLimit"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"CreditLimit"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"CreditLimit"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"CreditLimit"]objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"CreditLimit"]objectForKey:@"text"]];
                                    }
                                    else
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"CreditLimit"]];
                                    
                                    
                                }
                                return cell;
                                break;
                                
                            case 4:
                                if ([dictCustomerDetails objectForKey:@"CreditAvail"]) {
                                    
                                    if([[dictCustomerDetails objectForKey:@"CreditAvail"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"CreditAvail"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"CreditAvail"]];
                                    }
                                    
                                    else if([[dictCustomerDetails objectForKey:@"CreditAvail"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"CreditAvail"] objectForKey:@"text"]];
                                    }
                                    else
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"CreditAvail"]];
                                }
                                return cell;
                                break;
                                
                            case 5:
                                if ([dictCustomerDetails objectForKey:@"PaymentProfile"]) {
                                    if([[dictCustomerDetails objectForKey:@"PaymentProfile"] isKindOfClass:[NSString class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"PaymentProfile"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"PaymentProfile"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"PaymentProfile"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                 return cell;
                                break;
                                
                            case 6:
                                if ([dictCustomerDetails objectForKey:@"ExchangeCode"]) {
                                    
                                    if([[dictCustomerDetails objectForKey:@"ExchangeCode"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"ExchangeCode"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"ExchangeCode"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"]];
                                    }
                                    
                                    else
                                        
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"ExchangeCode"]];
                                    
                                }
                                 return cell;
                                break;
                                
                            default:{
                                cell.lblTitle.textColor = [UIColor darkGrayColor];
                            }
                                break;
                        }
                        
                    }
                    
                    if ([indexPath section] == 3) {
                        switch ([indexPath row]) {
                            case 0:
                                if ([dictCustomerDetails objectForKey:@"Current"]) {
                                    if([[dictCustomerDetails objectForKey:@"Current"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Current"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Current"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Current"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Current"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                return cell;
                                break;
                                
                            case 1:
                                
                                if([[dictCustomerDetails objectForKey:@"LastPaid"] isKindOfClass:[NSString class]])
                                {
                                    if ([dictCustomerDetails objectForKey:@"LastPaid"] && ![[dictCustomerDetails objectForKey:@"LastPaid"] isEqualToString:STANDARD_APP_DATE] && ![[dictCustomerDetails objectForKey:@"LastPaid"] isEqualToString:STANDARD_SERVER_DATE]) {
                                        
                                        NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictCustomerDetails objectForKey:@"LastPaid"]];
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",strDate];
                                        
                                    }
                                }
                                else if ([[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"] && ![[[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"]isEqualToString:STANDARD_APP_DATE] && ![[[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"] isEqualToString:STANDARD_SERVER_DATE]) {
                                    
                                    NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"]];
                                    cell.lblValue.text = [NSString stringWithFormat:@"%@",strDate];
                                    
                                }
                                 return cell;
                                break;
                                
                            case 2:
                                
                                if ([dictCustomerDetails objectForKey:@"SalesMtd"]) {
                                    if([[dictCustomerDetails objectForKey:@"SalesMtd"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"SalesMtd"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[dictCustomerDetails objectForKey:@"SalesMtd"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"SalesMtd"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[[dictCustomerDetails objectForKey:@"SalesMtd"] objectForKey:@"text"]];
                                    }
                                }
                                break;
                                
                            case 3:
                                
                                if ([dictCustomerDetails objectForKey:@"SalesYtd"]) {
                                    if([[dictCustomerDetails objectForKey:@"SalesYtd"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"SalesYtd"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[dictCustomerDetails objectForKey:@"SalesYtd"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"SalesYtd"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[[dictCustomerDetails objectForKey:@"SalesYtd"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                break;
                                
                            case 4:
                                if ([dictCustomerDetails objectForKey:@"TotalDue"]) {
                                    if([[dictCustomerDetails objectForKey:@"TotalDue"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"TotalDue"] isKindOfClass:[NSNumber class]])
                                    {
                                        
                                        if([[dictCustomerDetails objectForKey:@"TotalDue"] isKindOfClass:[NSString class]])
                                        {
                                            float totaldue = [[dictCustomerDetails objectForKey:@"TotalDue"]  floatValue];
                                            cell.lblValue.text = [NSString stringWithFormat:@"$ %f.2f",totaldue];
                                        }
                                        else
                                        {
                                            id totaldue = [dictCustomerDetails objectForKey:@"TotalDue"]  ;
                                            float tdue = [totaldue floatValue];
                                            
                                            cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",tdue];
                                            
                                            //cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"TotalDue"]];
                                        }
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"])
                                    {
                                        
                                        if([[[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"] isKindOfClass:[NSString class]])
                                        {
                                            float totaldue = [[[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"] floatValue];
                                            cell.lblValue.text = [NSString stringWithFormat:@"$ %f.2f",totaldue];
                                        }
                                        else
                                            
                                            cell.lblValue.text = [[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"];
                                    }
                                    
                                }
                                break;
                                
                            case 5:
                                if (isPeriod30) {
                                    cell.lblTitle.text = @"Over30";
                                }
                                else{
                                    cell.lblTitle.text = @"Over7";
                                }
                                
                                if ([dictCustomerDetails objectForKey:@"Over30"])
                                {
                                    if([[dictCustomerDetails objectForKey:@"Over30"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Over30"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Over30"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Over30"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Over30"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                return cell;
                                break;
                                
                            case 6:
                                if (isPeriod30) {
                                    cell.lblTitle.text = @"Over60";
                                }
                                else{
                                    cell.lblTitle.text = @"Over14";
                                }

                                
                                if ([dictCustomerDetails objectForKey:@"Over60"]) {
                                    if([[dictCustomerDetails objectForKey:@"Over60"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Over60"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Over60"]];
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Over60"] objectForKey:@"text"])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Over60"] objectForKey:@"text"]];
                                    }
                                    
                                }
                                return cell;
                                break;
                            case 7:
                                if (isPeriod30) {
                                    cell.lblTitle.text = @"Over90";
                                }
                                else{
                                    cell.lblTitle.text = @"Over21";
                                }

                                
                                if ([dictCustomerDetails objectForKey:@"Over90"]) {
                                    if([[dictCustomerDetails objectForKey:@"Over90"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Over90"] isKindOfClass:[NSNumber class]])
                                    {
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictCustomerDetails objectForKey:@"Over90"] floatValue]]];
//
                                    }
                                    else if([[dictCustomerDetails objectForKey:@"Over90"] objectForKey:@"text"])
                                    {
                                        
                                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerDetails objectForKey:@"Over90"] objectForKey:@"text"] floatValue]]];

                                    }
                                }
                                return cell;
                                break;
                                
                            default:
                                break;
                        }
                        
                        
                    }
//                    [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
                    return cell;
                }
                
            }break;
                
            case TAG_100:{
                
                if (tableView.editing) {
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier9];
                    
                    if (cell == nil)
                    {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        
                        cell = [nib objectAtIndex:8];
                    }
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]];
                    
                    NSString *strType = @"";
                    
                    
                    // Invoice
                    if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DI"])
                    {
                        strType = @"Invoice";
                    }
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DC"])
                        
                    {
                        strType = @"Credit note";
                    }
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DR"])
                        
                    {
                        strType = @"Receipt";
                    }
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DD"])
                        
                    {
                        strType = @"Debit Journal";
                    }
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DJ"])
                        
                    {
                        strType = @"Credit Journal";
                    }
                    else
                    {
                        strType = @"";
                    }
                    
                    cell.lblValue.text = strType;
                }
                else{
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
                    
                    if (cell == nil)
                    {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:5];
                    }
                    
                    
                    // 6th June
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]];
                    
                    NSString *strType = @"";
                    
                    
                    // Invoice
                    if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isKindOfClass:[NSString class]]){
                        if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DI"])
                        {
                            strType = @"Invoice";
                        }
                        
                    }
                    if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DI"])
                    {
                        strType = @"Invoice";
                    }
                
                    
                    // Credit note
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DC"])
                    {
                        strType = @"Credit note";
                    }
//                    else if([[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DC"])
//                    {
//                        strType = @"Credit note";
//                    }
                    
                    // Receipt
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DR"])
                    {
                        strType = @"Receipt";
                    }
//                    else if([[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DR"])
//                    {
//                        strType = @"Receipt";
//                    }

                    // Debit Journal
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DD"])
                    {
                        strType = @"Debit Journal";
                    }
//                    else if([[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"]objectForKey:@"text"] isEqualToString:@"DD"])
//                    {
//                        strType = @"Debit Journal";
//                    }
                    
                    // Credit Journal
                    else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] isEqualToString:@"DJ"])
                    {
                        strType = @"Credit Journal";
                    }
//                    else if([[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DJ"])
//                    {
//                        strType = @"Credit Journal";
//                    }
                    else
                    {
                        strType = @"";
                    }
                    
                    if([strType isEqualToString:@"Invoice"])
                    {
                        if([[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"] isKindOfClass:[NSString class]])
                        {
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"]floatValue]]];
                        }
                        else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"] isKindOfClass:[NSNumber class]])
                        {
                            id banance = [[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"];
                            cell.lblValue.text = [banance stringValue];
                        }
                        else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"] objectForKey:@"text"] )
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"BALANCE"]objectForKey:@"text"]floatValue]]];
                        }
                        else{
                            NSString *amount= [NSString stringWithFormat:@"$ %@",[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"Amount"]];
                            cell.lblValue.text = amount;
                        }
                    }
                    else
                        {
                            if([[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"] isKindOfClass:[NSString class]])
                            {
                                cell.lblValue.text = [[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"];
                            }
                            else if([[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"] isKindOfClass:[NSNumber class]])
                            {
                                id banance = [[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"];
                                cell.lblValue.text = [banance stringValue];
                            }
                            else if([[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"]  )
                            {
                                cell.lblValue.text = [[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"AMOUNT_PAID"];
                            }
                            else
                            {
                                
                                NSString *amount= [NSString stringWithFormat:@"$ %@",[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"Amount"]];
                                cell.lblValue.text = amount;
                            }
                          
                    }
                    
                    if ([strType isEqualToString:@""]) {
                        strType = @"    ---      ";
                    }
                    
                    NSString *strDateRaised = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]]objectForKey:@"Date"]];

                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *date1 = [dateFormat dateFromString:strDateRaised];
                    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                    [dateFormat2 setDateFormat:@"dd-MM-yy"];
                    NSString *strDate = [dateFormat2 stringFromDate:date1];
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@    %@    Raised %@",strType,[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"],strDate];
  
                }
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                return cell;
            }break;
                
            default:
                return nil;
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error:::%@",exception.description);
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case TAG_50:{
            if (indexPath.section == 1) {
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                  
                    return;
                }
              
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
              
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
          

        }break;
        
        case TAG_100:{
            CustomerTransactionHistoryViewController *dataViewController = [[CustomerTransactionHistoryViewController alloc] initWithNibName:@"CustomerTransactionHistoryViewController" bundle:[NSBundle mainBundle]];
          
            dataViewController.strTranCode = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]];
          
            dataViewController.isFromDebtorHistory = NO;
            /*if (isFromDebtorHistory) {
                dataViewController.isFromDebtorHistory = YES;
            }
            else{
                dataViewController.isFromDebtorHistory = NO;
            }*/
          
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
          
        }break;
    }
  
}


#pragma mark - Custom methods
- (void)removeFadeInFadeOutAnimations{
    [lblInfoText.layer removeAllAnimations];
}

- (void)addFadeInFadeOutAnimations{
    [lblInfoText.layer addAnimation:[[AppDelegate getAppDelegateObj] fadeInFadeOutAnimation]
                               forKey:@"animateOpacity"];
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
  
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
  
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
  
}



#pragma mark - Database calls
-(void)callViewCustomerTransactionFromDB:(FMDatabase *)db{
  
    strWebserviceType = @"DB_GET_CUSTOMER_TRANSACTION";
    FMResultSet *rs1;
    @try {
      
        rs1 = [db executeQuery:@"SELECT CODE as Code, CENTRAL_DEBTOR as CentralDebtor, NAME as Name, CURRENT as Current, PHONE as Phone, CONTACT1 as Contact1,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3,DEL_SUBURB as DelSuburb, EXCHANGE_CODE as ExchangeCode, OVER30 as Over30, CREDIT_LIMIT as CreditLimit, OVER60 as Over60, SALES_MTD as SalesMtd, SALES_YTD as SalesYtd, OVER90 as Over90, TERMS as Terms, PAYMENT_PROFILE as PaymentProfile, OVER120 as Over120, TRADING_TERMS as TradingTerms, LAST_PAID as LastPaid, HOLD as Hold, TOTAL_DUE as TotalDue,UNRELEASED_ORD as UnReleaseOrd, AGEING_PERIOD as ageing_period FROM armaster WHERE CODE=? ",strCustCode];
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            self.dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
            
            
            // Check for ageing period 30 OR 7
            NSString *ageing_period = [NSString stringWithFormat:@"%@",[self.dictCustomerDetails objectForKey:@"ageing_period"]];
            if ([ageing_period isEqualToString:@"30"]) {
                isPeriod30 = YES;
            }
            else if([ageing_period isEqualToString:@"7"]){
                isPeriod30 = NO;
            }
            else{
                isPeriod30 = NO;
            }
            
            NSString *strDelAdd = [[NSString alloc] init];
            
            if (![[dictCustomerDetails objectForKey:@"DelAddress1"] isEqual:[NSNull null]]&&![[dictCustomerDetails objectForKey:@"DelAddress1"] isEqualToString:@""]) {
                strDelAdd = [strDelAdd stringByAppendingString:[dictCustomerDetails objectForKey:@"DelAddress1"]];
            }
         
            if (![[dictCustomerDetails objectForKey:@"DelAddress2"] isEqual:[NSNull null]]&&![[dictCustomerDetails objectForKey:@"DelAddress2"] isEqualToString:@""]) {
                strDelAdd = [strDelAdd stringByAppendingString:@","];
                strDelAdd = [strDelAdd stringByAppendingString:[dictCustomerDetails objectForKey:@"DelAddress2"]];
            }
            if (![[dictCustomerDetails objectForKey:@"DelAddress3"] isEqual:[NSNull null]]&&![[dictCustomerDetails objectForKey:@"DelAddress3"] isEqualToString:@""]) {
                strDelAdd = [strDelAdd stringByAppendingString:@","];
                strDelAdd = [strDelAdd stringByAppendingString:[dictCustomerDetails objectForKey:@"DelAddress3"]];
            }
            
            if ([strDelAdd hasPrefix:@","]) {
                strDelAdd = [strDelAdd substringFromIndex:1];
            }            
          
            [dictCustomerDetails setValue:strDelAdd forKey:@"DeliveryAddress"];
            
           //Calculate Credit Availability
            float creditAvail = [[dictCustomerDetails objectForKey:@"CreditLimit"] floatValue] - [[dictCustomerDetails objectForKey:@"UnReleaseOrd"] floatValue] - [[dictCustomerDetails objectForKey:@"TotalDue"] floatValue];
            
            NSString *strCreditAvail = [NSString stringWithFormat:@"%.2f",creditAvail];
            [dictCustomerDetails setValue:strCreditAvail forKey:@"CreditAvail"];
            
            
        }
        else{
            NSLog(@"No Details exist for this code");
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
    
    FMResultSet *rs2;
    @try {
        
        [arrCustomerTransactions removeAllObjects]; // 29th may
        //NSLog(@"SELECT DEBTOR as Debtor, TRAN_NO as TranNo, DATE_RAISED as Date, CUST_ORDER, TYPE as Type, STATUS as Status, NETT as Nett, AMOUNT_PAID FROM artrans WHERE DEBTOR = %@ ",strCustCode);
        rs2 = [db executeQuery:@"SELECT DEBTOR as Debtor, TRAN_NO as TranNo, DATE_RAISED as Date, CUST_ORDER, TYPE as Type, STATUS as Status, NETT as Nett, AMOUNT_PAID FROM artrans WHERE DEBTOR = ? ",strCustCode];
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs2 next]) {
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            //Balance calculation
            float balance = [[dictData objectForKey:@"Nett"] floatValue] - [[dictData objectForKey:@"AMOUNT_PAID"] floatValue];
            NSString *strBalance = [NSString stringWithFormat:@"%f",balance];
            
            [dictData setValue:strBalance forKey:@"BALANCE"];
            
            NSDate *fromDate = [[SDSyncEngine sharedEngine] dateUsingStringFromAPI:[dictData objectForKey:@"Date"]];
            NSDate *toDate = [NSDate date];
            int days = [AppDelegate calcDaysBetweenTwoDate:fromDate ToDate:toDate];
            NSString *strDays = [NSString stringWithFormat:@"%d",days];
            
            [dictData setValue:strDays forKey:@"Days"];
            
            [arrCustomerTransactions addObject:dictData];
            
        }
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });

    }
    
    //}];
}

-(void)callViewCustomerHistoryFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_GET_CUSTOMER_HISTORY";
    FMResultSet *rs1;
    @try {
        
        rs1 = [db executeQuery:@"SELECT SCM_RECNUM,RECNUM, CODE as Code, NAME as Name, EXCHANGE_CODE as ExchangeCode, DATE_CREATED as DateRaised, created_date, modified_date from armaster WHERE CODE = ? ",strCustCode];
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            self.dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
            
        }
        else{
            NSLog(@"No Details exist for this code");
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
    }
    
    FMResultSet *rs2;
    @try {
        
        [arrCustomerTransactions removeAllObjects];
        
        rs2 = [db executeQuery:@"SELECT SCM_RECNUM, RECNUM, DEBTOR as Debtor, TYPE as Type, TRAN_NO as TranNo, NETT as Amount, SALESMAN as SalesPerson, CUST_ORDER as Reference, DATE_RAISED as Date FROM arhishea WHERE DEBTOR = ? ORDER BY DATE_RAISED DESC",strCustCode];
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([rs2 next]) {
            NSDictionary *dictData = [rs2 resultDictionary];
            [arrCustomerTransactions addObject:dictData];
        }
        
        [rs2 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
    }
    
    //[spinner hideLoadingView];
}

#pragma mark - WS Methods
-(void)callWSViewCustomerTransaction{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_CUSTOMER_TRANSACTION";
    
    NSString *parameters = [NSString stringWithFormat:@"code=%@",strCustCode];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_TRANSACTION_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewCustomerHistory
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_CUSTOMER_HISTORY";
    
    NSString *parameters = [NSString stringWithFormat:@"code=%@",strCustCode];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_HISTORY_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Long Press
-(void)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tblDetails];
    
    NSIndexPath *indexPath = [tblDetails indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else{
        
        if (isFromDebtorHistory) {
            CustomerHistoryDetailViewController *dataViewController = [[CustomerHistoryDetailViewController alloc] initWithNibName:@"CustomerHistoryDetailViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strTranCode = [[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"];
            
            dataViewController.dictCustomerHistoryDetails  = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            dataViewController.isFromCustomerHistory = YES;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
        else{
            CustomerTransactionDetailViewController *dataViewController = [[CustomerTransactionDetailViewController alloc] initWithNibName:@"CustomerTransactionDetailViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strTranCode = [[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"];
            
            dataViewController.dictTransactionDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            dataViewController.isFromTransactionHistory = FALSE;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
    }
    
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"dictResponse %@", dictResponse);
    
    if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_TRANSACTION"]){
        
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Artrans"] objectForKey:@"Transaction"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Artrans"] objectForKey:@"Transaction"];
                
                [arrCustomerTransactions addObject:dict];
            }
            else{
                self.arrCustomerTransactions = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Artrans"] objectForKey:@"Transaction"];
            }
            
            
            self.dictCustomerDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Data"];
            
            dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictCustomerDetails objectForKey:@"RECNUM"] objectForKey:@"text"]) {
                            //@throw [NSException exceptionWithName:@"Response Issue" reason:@"Scm Recnum can't be Null" userInfo:nil];
                        }
                        
                        
//                        FMResultSet *rs = [db executeQuery:@"SELECT SCM_RECNUM FROM armaster WHERE SCM_RECNUM = ?",[[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];
//                        

                      FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM armaster WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                 
                      if (!rs) {
                             [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        NSString *strCentralDebtor = @"";
                        if ([[dictCustomerDetails objectForKey:@"CentralDebtor"] objectForKey:@"text"]) {
                            strCentralDebtor = [[dictCustomerDetails objectForKey:@"CentralDebtor"] objectForKey:@"text"];
                        }
                        
                        NSString *strCode = @"";
                        if ([[dictCustomerDetails objectForKey:@"Code"] objectForKey:@"text"]) {
                            strCode = [[dictCustomerDetails objectForKey:@"Code"] objectForKey:@"text"];
                        }
                        
                        NSString *strContact1 = @"";
                        if ([[dictCustomerDetails objectForKey:@"Contact1"] objectForKey:@"text"]) {
                            strContact1 = [[dictCustomerDetails objectForKey:@"Contact1"] objectForKey:@"text"];
                        }
                        
                        NSString *strDelAddress1 = @"";
                        if ([[dictCustomerDetails objectForKey:@"DelAddress1"] objectForKey:@"text"]) {
                            strDelAddress1 = [[dictCustomerDetails objectForKey:@"DelAddress1"] objectForKey:@"text"];
                        }
                        
                        NSString *strDelAddress2 = @"";
                        if ([[dictCustomerDetails objectForKey:@"DelAddress2"] objectForKey:@"text"]) {
                            strDelAddress2 = [[dictCustomerDetails objectForKey:@"DelAddress2"] objectForKey:@"text"];
                        }
                        
                        NSString *strDelAddress3 = @"";
                        if ([[dictCustomerDetails objectForKey:@"DelAddress3"] objectForKey:@"text"]) {
                            strDelAddress3 = [[dictCustomerDetails objectForKey:@"DelAddress3"] objectForKey:@"text"];
                        }
                        
                        NSString *strDelSuburb = @"";
                        if ([[dictCustomerDetails objectForKey:@"DelSuburb"] objectForKey:@"text"]) {
                            strDelSuburb = [[dictCustomerDetails objectForKey:@"DelSuburb"] objectForKey:@"text"];
                        }

                    
                        NSString *strCreditLimit = @"";
                        if ([[dictCustomerDetails objectForKey:@"CreditLimit"] objectForKey:@"text"]) {
                            strCreditLimit = [[dictCustomerDetails objectForKey:@"CreditLimit"] objectForKey:@"text"];
                        }
                        
                        NSString *strCreditAvailability = @"";
                        if ([[dictCustomerDetails objectForKey:@"CreditAvail"] objectForKey:@"text"]) {
                            strCreditAvailability = [[dictCustomerDetails objectForKey:@"CreditAvail"] objectForKey:@"text"];
                        }

                        
                        NSString *strCurrent= @"";
                        if ([[dictCustomerDetails objectForKey:@"Current"] objectForKey:@"text"]) {
                            strCurrent = [[dictCustomerDetails objectForKey:@"Current"] objectForKey:@"text"];
                        }
                        
                        NSString *strExchangeCode= @"";
                        if ([[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"]) {
                            strExchangeCode = [[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"];
                        }
                        
                        NSString *strHold= @"";
                        if ([[dictCustomerDetails objectForKey:@"Hold"] objectForKey:@"text"]) {
                            strHold = [[dictCustomerDetails objectForKey:@"Hold"] objectForKey:@"text"];
                        }
                        
                        NSString *strLastPaid= @"";
                        if ([[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"]) {
                            strLastPaid = [[dictCustomerDetails objectForKey:@"LastPaid"] objectForKey:@"text"];
                        }
                        
                        NSString *strName= @"";
                        if ([[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"]) {
                            strName = [[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"];
                        }
                        
                        NSString *strOver120= @"";
                        if ([[dictCustomerDetails objectForKey:@"Over120"] objectForKey:@"text"]) {
                            strOver120 = [[dictCustomerDetails objectForKey:@"Over120"] objectForKey:@"text"];
                        }
                        
                        NSString *strOver30= @"";
                        if ([[dictCustomerDetails objectForKey:@"Over30"] objectForKey:@"text"]) {
                            strOver30 = [[dictCustomerDetails objectForKey:@"Over30"] objectForKey:@"text"];
                        }
                        
                        NSString *strOver60= @"";
                        if ([[dictCustomerDetails objectForKey:@"Over60"] objectForKey:@"text"]) {
                            strOver60 = [[dictCustomerDetails objectForKey:@"Over60"] objectForKey:@"text"];
                        }
                        
                        NSString *strOver90= @"";
                        if ([[dictCustomerDetails objectForKey:@"Over90"] objectForKey:@"text"]) {
                            strOver90 = [[dictCustomerDetails objectForKey:@"Over90"] objectForKey:@"text"];
                        }
                        
                        NSString *strPaymentProfile= @"";
                        if ([[dictCustomerDetails objectForKey:@"PaymentProfile"] objectForKey:@"text"]) {
                            strPaymentProfile = [[dictCustomerDetails objectForKey:@"PaymentProfile"] objectForKey:@"text"];
                        }
                        
                        NSString *strPhone= @"";
                        if ([[dictCustomerDetails objectForKey:@"Phone"] objectForKey:@"text"]) {
                            strPhone = [[dictCustomerDetails objectForKey:@"Phone"] objectForKey:@"text"];
                        }
                        
                        NSString *strSalesMtd= @"";
                        if ([[dictCustomerDetails objectForKey:@"SalesMtd"] objectForKey:@"text"]) {
                            strSalesMtd = [[dictCustomerDetails objectForKey:@"SalesMtd"] objectForKey:@"text"];
                        }
                        
                        NSString *strSalesYtd= @"";
                        if ([[dictCustomerDetails objectForKey:@"SalesYtd"] objectForKey:@"text"]) {
                            strSalesYtd = [[dictCustomerDetails objectForKey:@"SalesYtd"] objectForKey:@"text"];
                        }
                        
                        NSString *strTerms= @"";
                        if ([[dictCustomerDetails objectForKey:@"Terms"] objectForKey:@"text"]) {
                            strTerms = [[dictCustomerDetails objectForKey:@"Terms"] objectForKey:@"text"];
                        }
                        
                        NSString *strTotalDue= @"";
                        if ([[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"]) {
                            strTotalDue = [[dictCustomerDetails objectForKey:@"TotalDue"] objectForKey:@"text"];
                        }
                        
                         NSString *strTradingTerms= @"";
                        if ([[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"]) {
                            strTradingTerms = [[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"];
                            
                        }
                        
                        NSString *strUnreleasedOrder= @"0.00";
                        if ([[dictCustomerDetails objectForKey:@"UnreleasedOrder"] objectForKey:@"text"]) {
                            strUnreleasedOrder = [[dictCustomerDetails objectForKey:@"UnreleasedOrder"] objectForKey:@"text"];
                            
                        }
        
                        BOOL y;
                        if ([rs next]) {
//                            y = [db executeUpdate:@"UPDATE  `armaster` SET `SCM_RECNUM` = ?, `RECNUM` = ?, `CENTRAL_DEBTOR` = ?, `CODE` = ?,`CONTACT1` = ?,DEL_ADDRESS1 = ?,DEL_ADDRESS2 = ?,DEL_ADDRESS3 = ?,DEL_SUBURB = ?, `CREDIT_LIMIT` = ?,`CURRENT` = ?,`EXCHANGE_CODE` = ?,`HOLD` = ?,`LAST_PAID` = ?,`NAME` = ?,`OVER120` = ?,`OVER30` = ?,`OVER60` = ?,`OVER90` = ?,`PAYMENT_PROFILE` = ?,`PHONE` = ?,`SALES_MTD` = ?,`SALES_YTD` = ?,`TERMS` = ?,`TOTAL_DUE` = ?,`TRADING_TERMS` = ?, UNRELEASED_ORD = ?,`created_date` = ?, `modified_date` = ? WHERE SCM_RECNUM = ?", [[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCentralDebtor,strCode, strContact1,strDelAddress1,strDelAddress2,strDelAddress3,strDelSuburb,strCreditLimit,strCurrent,strExchangeCode,strHold,strLastPaid,strName,strOver120,strOver30,strOver60,strOver90,strPaymentProfile,strPhone,strSalesMtd,strSalesYtd,strTerms,strTotalDue,strTradingTerms,strUnreleasedOrder,[[dictCustomerDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];
                          
                            y = [db executeUpdate:@"UPDATE  `armaster` SET  `RECNUM` = ?, `CENTRAL_DEBTOR` = ?, `CODE` = ?,`CONTACT1` = ?,DEL_ADDRESS1 = ?,DEL_ADDRESS2 = ?,DEL_ADDRESS3 = ?,DEL_SUBURB = ?, `CREDIT_LIMIT` = ?,`CURRENT` = ?,`EXCHANGE_CODE` = ?,`HOLD` = ?,`LAST_PAID` = ?,`NAME` = ?,`OVER120` = ?,`OVER30` = ?,`OVER60` = ?,`OVER90` = ?,`PAYMENT_PROFILE` = ?,`PHONE` = ?,`SALES_MTD` = ?,`SALES_YTD` = ?,`TERMS` = ?,`TOTAL_DUE` = ?,`TRADING_TERMS` = ?, UNRELEASED_ORD = ? WHERE `RECNUM` = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCentralDebtor,strCode, strContact1,strDelAddress1,strDelAddress2,strDelAddress3,strDelSuburb,strCreditLimit,strCurrent,strExchangeCode,strHold,strLastPaid,strName,strOver120,strOver30,strOver60,strOver90,strPaymentProfile,strPhone,strSalesMtd,strSalesYtd,strTerms,strTotalDue,strTradingTerms,strUnreleasedOrder,[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        }
                        else{
                          
                          y = [db executeUpdate:@"INSERT INTO `armaster` ( `RECNUM`, `CENTRAL_DEBTOR`, `CODE`,`CONTACT1`,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,`CREDIT_LIMIT`,`CURRENT`,`EXCHANGE_CODE`,`HOLD`,`LAST_PAID`,`NAME`,`OVER120`,`OVER30`,`OVER60`,`OVER90`,`PAYMENT_PROFILE`,`PHONE`,`SALES_MTD`,`SALES_YTD`,`TERMS`,`TOTAL_DUE`,`TRADING_TERMS`,UNRELEASED_ORD) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?,?,?)",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCentralDebtor,strCode, strContact1, strDelAddress1,strDelAddress2,strDelAddress3,strDelSuburb,strCreditLimit,strCurrent,strExchangeCode,strHold,strLastPaid,strName,strOver120,strOver30,strOver60,strOver90,strPaymentProfile,strPhone,strSalesMtd,strSalesYtd,strTerms,strTotalDue,strTradingTerms,strUnreleasedOrder];
                        }
                        
                        [rs close];
                        
                        if (!y)
                        {
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                        for (NSDictionary *dict in arrCustomerTransactions){
                            
                            if (![[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"]) {
                                //@throw [NSException exceptionWithName:@"Response Issue" reason:@"ScmRecnum can't be Null" userInfo:nil];
                            }
                            
                            NSString *strApplied = @"";
                            if ([[dict objectForKey:@"Applied"] objectForKey:@"text"]) {
                                strApplied= [[dict objectForKey:@"Applied"] objectForKey:@"text"];
                            }
                            
                            NSString *strDate = @"";
                            if ([[dict objectForKey:@"Date"] objectForKey:@"text"]) {
                                strDate= [[dict objectForKey:@"Date"] objectForKey:@"text"];
                            }
                            
                            
                            NSString *strDebtor = @"";
                            if ([[dict objectForKey:@"Debtor"] objectForKey:@"text"]) {
                                strDebtor= [[dict objectForKey:@"Debtor"] objectForKey:@"text"];
                            }
                            
                            NSString *strNett = @"";
                            if ([[dict objectForKey:@"Nett"] objectForKey:@"text"]) {
                                strNett= [[dict objectForKey:@"Nett"] objectForKey:@"text"];
                            }
                            
                            NSString *strReference = @"";
                            if ([[dict objectForKey:@"Reference"] objectForKey:@"text"]) {
                                strReference= [[dict objectForKey:@"Reference"] objectForKey:@"text"];
                            }
                            
                            NSString *strStatus = @"";
                            if ([[dict objectForKey:@"Status"] objectForKey:@"text"]) {
                                strStatus= [[dict objectForKey:@"Status"] objectForKey:@"text"];
                            }
                            
                            NSString *strTranNo = @"";
                            if ([[dict objectForKey:@"TranNo"] objectForKey:@"text"]) {
                                strTranNo= [[dict objectForKey:@"TranNo"] objectForKey:@"text"];
                            }

                            NSString *strType = @"";
                            if ([[dict objectForKey:@"Type"] objectForKey:@"text"]) {
                                strType= [[dict objectForKey:@"Type"] objectForKey:@"text"];
                            }
                            
                            NSString *strRecnum = [[dict objectForKey:@"Recnum"] objectForKey:@"text"];
                    
                            FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM artrans WHERE RECNUM = ?",strRecnum];
                            
                            if (!rs) {
                                
                                [rs close];
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                
                                break;
                            }
                            
                            BOOL y;
                            if ([rs next]) {
//                                y = [db executeUpdate:@"UPDATE `artrans` SET  `SCM_RECNUM` = ?, `RECNUM`= ?, `AMOUNT_PAID`= ?, `DATE_RAISED`= ?, `DEBTOR`= ?, `NETT`= ?,`CUST_ORDER`= ?, `STATUS`= ?,`TRAN_NO`= ?,`TYPE`= ?,`created_date`= ?, `modified_date`= ? WHERE SCM_RECNUM = ?", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strApplied,strDate, strDebtor, strNett,strReference,strStatus,strTranNo,strType,[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"],strScmRecnum];
//                            }
                              
                              y = [db executeUpdate:@"UPDATE `artrans` SET  `RECNUM`= ?, `AMOUNT_PAID`= ?, `DATE_RAISED`= ?, `DEBTOR`= ?, `NETT`= ?,`CUST_ORDER`= ?, `STATUS`= ?,`TRAN_NO`= ?,`TYPE`= ? WHERE RECNUM = ?", strRecnum, strApplied,strDate, strDebtor, strNett,strReference,strStatus,strTranNo,strType,strRecnum];
       
                              
                            }
                            else{
                                y = [db executeUpdate:@"INSERT OR REPLACE INTO `artrans` ( `RECNUM`, `AMOUNT_PAID`, `DATE_RAISED`, `DEBTOR`, `NETT`,`CUST_ORDER`, `STATUS`,`TRAN_NO`,`TYPE`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)",[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strApplied,strDate, strDebtor, strNett,strReference ,strStatus,strTranNo,strType];
                            }
                            
                            [rs close];
                            
                            if (!y)
                            {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            
                        
                        }
                    
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });

                    }
                    
                    @finally {
                        [self callViewCustomerTransactionFromDB:db];
                    }
                     
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [spinnerObj removeFromSuperview];
                            [tblHeader reloadData];
                            //dispatch_release(backgroundQueueForViewCustomerTransaction);
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                    });
                    
                }];
                
            });
            
        }
        //False
        else{
            
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_HISTORY"]){
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"PaymentDetails"] objectForKey:@"Transaction"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"PaymentDetails"] objectForKey:@"Transaction"];
                
                [arrCustomerTransactions addObject:dict];
            }
            else{
                self.arrCustomerTransactions = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"PaymentDetails"] objectForKey:@"Transaction"]mutableCopy];
            }
            
            
            self.dictCustomerDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"Data"];
            
            
            
            dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]) {
                            //@throw [NSException exceptionWithName:@"Response Issue" reason:@"Scm Recnum can't be Null" userInfo:nil];
                        }
                        
                                                        
                        NSString *strCode = @"";
                        if ([[dictCustomerDetails objectForKey:@"Code"] objectForKey:@"text"]) {
                            strCode = [[dictCustomerDetails objectForKey:@"Code"] objectForKey:@"text"];
                        }
                        
                        
                        NSString *strName= @"";
                        if ([[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"]) {
                            strName = [[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"];
                        }
                        
                        NSString *strDateRaised= @"";
                        if ([[dictCustomerDetails objectForKey:@"DateRaised"] objectForKey:@"text"]) {
                            strDateRaised = [[dictCustomerDetails objectForKey:@"DateRaised"] objectForKey:@"text"];
                        }
                        
                        NSString *strExchangeCode= @"";
                        if ([[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"]) {
                            strExchangeCode = [[dictCustomerDetails objectForKey:@"ExchangeCode"] objectForKey:@"text"];
                        }
                        
//                        
//                        FMResultSet *rs = [db executeQuery:@"SELECT SCM_RECNUM FROM armaster WHERE SCM_RECNUM = ?",[[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];
                      
                      FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM armaster WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                      
                        if (!rs) {
                            [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        BOOL y;
                        if ([rs next]) {
//                            y = [db executeUpdate:@"UPDATE  `armaster` SET `SCM_RECNUM` = ?, `RECNUM`= ?, `CODE`= ?, `NAME`= ?,`DATE_CREATED`= ?, `EXCHANGE_CODE`= ?,`created_date`= ?, `modified_date`= ? WHERE SCM_RECNUM = ?", [[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCode,strName, strDateRaised, strExchangeCode,[[dictCustomerDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];
                        
                      y = [db executeUpdate:@"UPDATE  `armaster` SET  `RECNUM`= ?, `CODE`= ?, `NAME`= ?,`DATE_CREATED`= ?, `EXCHANGE_CODE`= ? WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCode,strName, strDateRaised, strExchangeCode,[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        }
                        else{
//                            y = [db executeUpdate:@"INSERT OR REPLACE INTO `armaster` (`SCM_RECNUM`, `RECNUM`, `CODE`, `NAME`,`DATE_CREATED`, `EXCHANGE_CODE`,`created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCode,strName, strDateRaised, strExchangeCode,[[dictCustomerDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
                          
                          y = [db executeUpdate:@"INSERT OR REPLACE INTO `armaster` (`RECNUM`, `CODE`, `NAME`,`DATE_CREATED`, `EXCHANGE_CODE`) VALUES ( ?, ?, ?, ?, ?)",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCode,strName, strDateRaised, strExchangeCode];

                        }
                        
                        [rs close];
                        if (!y)
                        {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        for (NSDictionary *dict in arrCustomerTransactions){
                            
                            NSString *strScmRecnum = [[dict objectForKey:@"Recnum"] objectForKey:@"text"];
                            
                            if (![[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"]) {
                                //@throw [NSException exceptionWithName:@"Response Issue" reason:@"ScmRecnum can't be Null" userInfo:nil];
                            }
                            
                            FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM arhishea WHERE RECNUM = ?",strScmRecnum];
                            
                            if (!rs) {
                                
                                [rs close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            NSString *strAmount= @"";
                            if ([[dict objectForKey:@"Amount"] objectForKey:@"text"]) {
                                strAmount = [[dict objectForKey:@"Amount"] objectForKey:@"text"];
                            }

                            NSString *strDate= @"";
                            if ([[dict objectForKey:@"Date"] objectForKey:@"text"]) {
                                strDate = [[dict objectForKey:@"Date"] objectForKey:@"text"];
                            }

                            NSString *strDebtor= @"";
                            if ([[dict objectForKey:@"Debtor"] objectForKey:@"text"]) {
                                strDebtor = [[dict objectForKey:@"Debtor"] objectForKey:@"text"];
                            }

                            NSString *strReference= @"";
                            if ([[dict objectForKey:@"Reference"] objectForKey:@"text"]) {
                                strReference = [[dict objectForKey:@"Reference"] objectForKey:@"text"];
                            }

                            NSString *strSalesPerson = @"";
                            if ([[dict objectForKey:@"SalesPerson"] objectForKey:@"text"]) {
                                strSalesPerson  = [[dict objectForKey:@"SalesPerson"] objectForKey:@"text"];
                            }
                            
                            NSString *strTranNo= @"";
                            if ([[dict objectForKey:@"TranNo"] objectForKey:@"text"]) {
                                strTranNo = [[dict objectForKey:@"TranNo"] objectForKey:@"text"];
                            }
                            
                            NSString *strType= @"";
                            if ([[dict objectForKey:@"Type"] objectForKey:@"text"]) {
                                strType = [[dict objectForKey:@"Type"] objectForKey:@"text"];
                            }
                            
                        
                            BOOL y;
                            if ([rs next]) {
//                                y = [db executeUpdate:@"UPDATE `arhishea` SET `SCM_RECNUM` = ?, `RECNUM`= ?, `NETT`= ?, `DATE_RAISED`= ?, `DEBTOR`= ?, `CUST_ORDER`= ?,`SALESMAN`= ?, `TRAN_NO`= ?,`TYPE`= ?,`created_date`= ?, `modified_date`= ? WHERE SCM_RECNUM = ?", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strAmount,strDate, strDebtor, strReference,strSalesPerson,strTranNo,strType,[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"],strScmRecnum];
                              y = [db executeUpdate:@"UPDATE `arhishea` SET  `RECNUM`= ?, `NETT`= ?, `DATE_RAISED`= ?, `DEBTOR`= ?, `CUST_ORDER`= ?,`SALESMAN`= ?, `TRAN_NO`= ?,`TYPE`= ? WHERE RECNUM = ?",[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strAmount,strDate, strDebtor, strReference,strSalesPerson,strTranNo,strType,[dict objectForKey:@"Recnum"]];
                              
                            }
                            else{
//                                y = [db executeUpdate:@"INSERT INTO `arhishea` (`SCM_RECNUM`, `RECNUM`, `NETT`, `DATE_RAISED`, `DEBTOR`, `CUST_ORDER`,`SALESMAN`, `TRAN_NO`,`TYPE`,`created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strAmount,strDate, strDebtor, strReference,strSalesPerson,strTranNo,strType,[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
                                  y = [db executeUpdate:@"INSERT INTO `arhishea` ( `RECNUM`, `NETT`, `DATE_RAISED`, `DEBTOR`, `CUST_ORDER`,`SALESMAN`, `TRAN_NO`,`TYPE`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)",[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strAmount,strDate, strDebtor, strReference,strSalesPerson,strTranNo,strType];
//                      
                            }
                            
                            [rs close];
                            
                            if (!y)
                            {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                        }
                        
                        //[db commit];
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });

                    }
                    
//                    @finally {
//                        [self callViewCustomerHistoryFromDB:db];
//                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            //dispatch_release(backgroundQueueForViewCustomerTransaction);
                            [spinnerObj removeFromSuperview];
                            [tblHeader reloadData];
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                    });
                    
                }];
                
            });
            
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustHistory"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
