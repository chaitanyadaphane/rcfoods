//
//  SDSyncEngine.m
//  SignificantDates
//
//  Created by Chris Wagner on 7/1/12.
//

#import "SDSyncEngine.h"
#import "DownloadUrlOperation.h"


#define SYNC_IN_PROGRESS_MESSAGE @"Sync in progress! Please Wait ..."
#define SYNC_COMPLETE_MESSAGE @"Sync Completed. You are up to date!"
#define BulkInsertion  @"bulkInsetion"
NSString * const kSDSyncEngineInitialCompleteKey = @"SDSyncEngineInitialSyncCompleted";
NSString * const kSDSyncEngineSyncCompletedNotificationName = @"SDSyncEngineSyncCompleted";

@interface SDSyncEngine ()

@property (nonatomic, strong) NSMutableArray *registeredClassesToSync;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation SDSyncEngine

@synthesize syncInProgress = _syncInProgress;

@synthesize registeredClassesToSync = _registeredClassesToSync;
@synthesize dateFormatter = _dateFormatter;
@synthesize downloadOperationQueue,panel,isQueueCancelled,numDatabaseDumped;

#define BulkInsertion  @"bulkInsetion"

+ (SDSyncEngine *)sharedEngine {
    static SDSyncEngine *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[SDSyncEngine alloc] init];
    });
    
    return sharedEngine;
}

- (void)registerNSManagedObjectClassToSync:(NSString *)tableName {
    if (!self.registeredClassesToSync) {
        self.registeredClassesToSync = [NSMutableArray array];
    }
    
    if (![self.registeredClassesToSync containsObject:tableName]) {
        [self.registeredClassesToSync addObject:tableName];
    } else {
        NSLog(@"Unable to register %@ as it is already registered", tableName);
    }
}

-(void)DeleteAndStartSync
{
    if (!self.syncInProgress) {
        self.isQueueCancelled = NO;
        
        topWindow = [AppDelegate getAppDelegateObj].window;
        
        
        self.panel = [AJNotificationView showNoticeInView:topWindow.rootViewController.view
                                                     type:AJNotificationTypeBlue
                                                    title:SYNC_IN_PROGRESS_MESSAGE
                                          linedBackground:AJLinedBackgroundTypeAnimated
                                                hideAfter:0];
        
        
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = YES;
        [self didChangeValueForKey:@"syncInProgress"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self FlushAndDownloadData];
        });
    }
}


- (void)startSync {
    if (!self.syncInProgress) {
        self.isQueueCancelled = NO;
        
        topWindow = [AppDelegate getAppDelegateObj].window;
        
        
        self.panel = [AJNotificationView showNoticeInView:topWindow.rootViewController.view
                                                     type:AJNotificationTypeBlue
                                                    title:SYNC_IN_PROGRESS_MESSAGE
                                          linedBackground:AJLinedBackgroundTypeAnimated
                                                hideAfter:0];
        
        
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = YES;
        [self didChangeValueForKey:@"syncInProgress"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self downloadDataForRegisteredObjects:YES];
        });
    }
    
}

- (void)executeSyncCompletedOperations {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setInitialSyncCompleted];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kSDSyncEngineSyncCompletedNotificationName
         object:nil];
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = NO;
        [self didChangeValueForKey:@"syncInProgress"];
        
        
        //Remove observer
        for (NSOperation *req in [downloadOperationQueue operations]) {
            [req removeObserver:self forKeyPath:@"isFinished"];
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SyncDataDownloadFinished" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SyncDataDownloadFailed" object:nil];
        
        if (!isQueueCancelled) {
            
            NSDate *date = [NSDate date];
            
            // format it
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm"];
            //[dateFormat setLocale:[NSLocale currentLocale]];
            // convert it to a string
            NSString *dateString = [dateFormat stringFromDate:date];
           // NSLog(@"%@",dateString);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inDatabase:^(FMDatabase *db) {
                BOOL rs = [db executeUpdate:@"UPDATE tbl_last_application_sync SET last_sync_date = ?",dateString];
                
                if (rs) {
                    NSString *strStatus = [NSString stringWithFormat:@"Updated at %@",dateString];
                    [AppDelegate getAppDelegateObj].rootViewController.lblStatusText.text = strStatus;
                }
            }];
            
            databaseQueue = nil;
            
        }
        
    });
}

-(void)showSyncErrorMsg:(NSString*)err
{
    if (panel)
        [panel hide];
    
    self.panel = [AJNotificationView showNoticeInView:topWindow.rootViewController.view
                                                 type:AJNotificationTypeRed
                                                title:err
                                      linedBackground:AJLinedBackgroundTypeDisabled
                                            hideAfter:2.5f];
}

- (BOOL)initialSyncComplete {
    return [[[NSUserDefaults standardUserDefaults] valueForKey:kSDSyncEngineInitialCompleteKey] boolValue];
}

- (void)setInitialSyncCompleted {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kSDSyncEngineInitialCompleteKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)mostRecentUpdatedAtDateForEntityWithName:(NSString *)entityName {
    __block NSDate *date = nil;
    
    __block NSString *dateString = nil;
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT last_sync_date FROM tbl_lastSyncDates WHERE table_name = ? ",entityName];
        
        if ([results next]){
            NSDictionary *dictData = [results resultDictionary];
            NSString *strDateFromDb = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"last_sync_date"]];
            date = [self dateUsingStringFromAPI:strDateFromDb];
            dateString = [self dateStringForAPIUsingDate:date];
            
            dateString = [dateString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        else{
            NSLog(@"table doesnt exist in db");
        }
        
    }];
    
    return dateString;
}

- (BOOL)setUpdatedLastDateForEntityWithName:(NSString *)entityName NewSyncDate:(NSString *)newSyncDate Database:(FMDatabase *)db{
    //[[AppDelegate getAppDelegateObj].database open];

    BOOL y = [db executeUpdate:@"UPDATE tbl_lastSyncDates SET last_sync_date = ? WHERE table_name = ?",newSyncDate,entityName];
    
    //[[AppDelegate getAppDelegateObj].database close];
    
    return y;
}

-(void)FlushAndDownloadData
{
    cntWebserviceNumber = 0;
    numDatabaseDumped = 0;
    
    downloadOperationQueue = [NSOperationQueue new];
    // set maximum operations possible
    [downloadOperationQueue setMaxConcurrentOperationCount:1];
    
    [self.downloadOperationQueue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataAvailable:)
     name:@"SyncDataDownloadFinished"
     object:nil ] ;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataUnAvailable:)
     name:@"SyncDataDownloadFailed"
     object:nil ];
    
    [self callSamasterWebservicewithDatabasechanges:YES];
    [self callArmasterWebservicewithDatabasechanges:YES];
    [self callsysdescWebservicewithDatabasechanges:YES];
    [self callsysdisctWebservicewithDatabasechanges:YES];
    [self callsysistaxWebservicewithDatabasechanges:YES];
    [self callSmcusprobudWebWebservicewithDatabasechanges:YES];
    [self callsoquoheaWebservicewithDatabasechanges:YES];
    [self callSoquodetWebservicewithDatabasechanges:YES];
    [self callSoheaderWebWebservicewithDatabasechanges:YES];
    [self callArtransWebservicewithDatabasechanges:YES];
    [self callArhisheaWebservicewithDatabasechanges:YES];
    [self callArhisdetWebservicewithDatabasechanges:YES];
    [self callArpaymentWebservicewithDatabasechanges:YES];
    [self callArcommentWebservicewithDatabasechanges:YES];
    [self callsyscontWebservicewithDatabasechanges:YES];
    [self callSahisbudWebWebservicewithDatabasechanges:YES];
    [self callSodetailWebWebservicewithDatabasechanges:YES];
    [self callPodetailWebWebservicewithDatabasechanges:YES];
    [self callsacntrctWebservicewithDatabasechanges:YES];
//    [self callsmdaytimWebservicewithDatabasechanges:YES];
    [self callSyslogonWebservicewithDatabasechanges:YES];
    
    return;
}


- (void)downloadDataForRegisteredObjects:(BOOL)useUpdatedAtDate {
    
    cntWebserviceNumber = 0;
    numDatabaseDumped = 0;
    
    downloadOperationQueue = [NSOperationQueue new];
    // set maximum operations possible
    [downloadOperationQueue setMaxConcurrentOperationCount:1];
    [self.downloadOperationQueue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataAvailable:) name:@"SyncDataDownloadFinished" object:nil ] ;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dataUnAvailable:) name:@"SyncDataDownloadFailed" object:nil ];

    
    [self callSamasterWebservicewithDatabasechanges:NO];
    [self callArmasterWebservicewithDatabasechanges:NO];
    [self callsysdescWebservicewithDatabasechanges:NO];
    [self callsysdisctWebservicewithDatabasechanges:NO];
    [self callsysistaxWebservicewithDatabasechanges:NO];
    [self callSmcusprobudWebWebservicewithDatabasechanges:NO];
    [self callsoquoheaWebservicewithDatabasechanges:NO];
    [self callSoquodetWebservicewithDatabasechanges:NO];
    [self callSoheaderWebWebservicewithDatabasechanges:NO];
    [self callArtransWebservicewithDatabasechanges:NO];
    [self callArhisheaWebservicewithDatabasechanges:NO];
    [self callArhisdetWebservicewithDatabasechanges:NO];
    [self callArpaymentWebservicewithDatabasechanges:NO];
    [self callArcommentWebservicewithDatabasechanges:NO];
    [self callsyscontWebservicewithDatabasechanges:NO];
    [self callSahisbudWebWebservicewithDatabasechanges:NO];
    [self callSodetailWebWebservicewithDatabasechanges:NO];
    [self callPodetailWebWebservicewithDatabasechanges:NO];//*
    [self callsacntrctWebservicewithDatabasechanges:NO];
    //    [self callsmdaytimWebservicewithDatabasechanges:NO];
    [self callSyslogonWebservicewithDatabasechanges:NO];
    
    return;
}

-(void)callDeleteMessages{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,DELETE_MESSAGES_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    NSLog(@"delete messages strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 25;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}

-(void)callDeleteSpecials{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,DELETE_SPECIALS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    NSLog(@"delete messages strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 26;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}


#pragma mark - Upload WS calls
-(void)callUploadDeletedOrders{
        
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_DELETE_WS];
    NSLog(@"uploadDeletedOrders strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 50;
    operation.isUploading = YES;
    operation.isOrders = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}


-(void)callUploadDeletedQuotes
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_DELETE_WS];
    NSLog(@"uploadDeletedQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 51;
    operation.isUploading = YES;
    operation.isQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}

//Upload Order headers
-(void)callUploadOrdersHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadOrders strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 100;
    operation.isUploading = YES;
    operation.isOrders = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];    
}

//Upload Order Details
-(void)callUploadOrdersDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 101;
    operation.isUploading = YES;
    operation.isOrders = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}

//Upload Quote Headers
-(void)callUploadQuotesHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 102;
    operation.isUploading = YES;
    operation.isQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}

//Upload Quote Details
-(void)callUploadQuotesDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 103;
    operation.isUploading = YES;
    operation.isQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}

//Upload Offline Quote Headers
-(void)callUploadOfflineQuotesHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 104;
    operation.isUploading = YES;
    operation.isOfflineQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}

//Upload Offline Quote Details
-(void)callUploadOfflineQuotesDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    NSLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 105;
    operation.isUploading = YES;
    operation.isOfflineQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
}


#pragma mark - Download WS calls

-(void)callMembersWebservice
{
    //NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"members"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,MEMBER_WS_NEW,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];

    NSLog(@"members strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 1;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callArmasterWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"armaster"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"armaster strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 2;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callArtransWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"artrans"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARTRANS_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"artrans strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 3;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callArcommentWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arcoment"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARCOMMENT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"ARCOMMENT_WS strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 4;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callNewsamasterWebservice
{    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,NEWSAMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    NSLog(@"new_samaster strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 5;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callPodetailWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"podetail"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,PODETAIL_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"PODETAIL_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 6;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSahisbudWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sahisbud"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SAHISBUD_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SAHISBUD_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 7;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSamasterWebservice
{
  
  NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"samaster"];
  
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SAMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
  
  NSLog(@"SAMASTER_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 8;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
  
}

-(void)callSmcusproWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"smcuspro"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SMCUSPRO_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SMCUSPRO_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 9;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSodetailWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sodetail"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SODETAIL_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SODETAIL_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 10;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSoheaderWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soheader"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOHEADER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SOHEADER_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 11;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSoquoheaWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soquohea"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOQUOHEA_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SOQUOHEA_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 12;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSoquodetWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soquodet"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOQUODET_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SOQUODET_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 13;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}


-(void)callSpecialsWebservice
{
    //NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"specials"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SPECIALS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    NSLog(@"SPECIALS_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 14;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}


-(void)callSysdescWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdesc"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSDESC_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSDESC_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 15;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSysdisctWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdisct"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSDISCT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSDISCT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 16;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}


-(void)callSyslogonWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdisct"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSLOGON_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSLOGON_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 17;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSysistaxWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysistax"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSISTAX_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSISTAX_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 18;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSysusrnoWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysistax"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSUSRNO_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSUSRNO_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 19;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callArpaymentWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arpaymnt"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARPAYMENT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"ARPAYMENT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 20;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callArhisheaWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arhishea"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARHISHEA_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"ARHISHEA_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 21;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}


-(void)callArhisdetWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arhisdet"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARHISDET_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"ARHISDET_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 22;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callNoticemsgsWebservice
{    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,NOTICEMSGS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    NSLog(@"NOTICEMSGS_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 23;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];// operation starts as soon as its added
    
}

-(void)callSysContWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"syscont"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSCONT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSLog(@"SYSCONT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 24;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}



#pragma mark - KVO Observing/ WS Handler
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)operation change:(NSDictionary *)change context:(void *)context {
    NSString *source = nil;
    //NSData *data = nil;
    NSError *error = nil;
//    NSLog(@"[self.downloadOperationQueue.operations count] : %d",[self.downloadOperationQueue.operations count]);
    
    if ([operation isKindOfClass:[DownloadUrlOperation class]])
    {
        DownloadUrlOperation *downloadOperation = (DownloadUrlOperation *)operation;
        source = [NSString stringWithFormat:@"%d",downloadOperation.tag];
      
      NSLog(@"source:::::::::::::::::::::::::::%@::::::::::::::::::::::::",source);
      
        //get total queue size by the first success and add 1 back
        if (queueSize ==0) {
            queueSize = [[downloadOperationQueue operations] count] +1.0;
        }
        
        float progress = (float)(queueSize-[[downloadOperationQueue operations] count])/queueSize;
        
        NSLog(@"progress %f",progress);
        
        dispatch_async(dispatch_get_main_queue(), ^(void)
        {
            float progValue =progress*100;
            NSString *strProgress;
            
            
            // 12th June
            if(progValue >= 100)
            {
                NSString *progStr = [NSString stringWithFormat:@"%.0f",progValue];
                strProgress = [NSString stringWithFormat:@"%@ (%@ %@)",SYNC_IN_PROGRESS_MESSAGE,[progStr substringToIndex:2],@"%"];
            }
            else
            {
                 strProgress = [NSString stringWithFormat:@"%@ (%.0f %@)",SYNC_IN_PROGRESS_MESSAGE,progress*100,@"%"];
            }

//            NSString *strProgress = [NSString stringWithFormat:@"%@ (%.0f %@)",SYNC_IN_PROGRESS_MESSAGE,progress*100,@"%"];
            
            self.panel.titleLabel.text = strProgress;
        });
        
        if (source) {
            
            //Specials & Notice messages
            //if (downloadOperation.tag == 14 || downloadOperation.tag == 23) {
            // data = [downloadOperation data];
            //}
            
            error = [downloadOperation error];
        }
        
        if (source) {
            if (error != nil) {
                // handle error
                // Notify that we have got an error downloading this data;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SyncDataDownloadFailed"
                                                                    object:self
                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", error, @"error", nil]];
                
                
                //Armaster
                if ([source isEqualToString:@"2"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Armaster Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 12;
                    [alert show];
                }
                //Artrans
                else if ([source isEqualToString:@"3"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Artrans Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 13;
                    [alert show];
                }
                //Arcoment
                else if ([source isEqualToString:@"4"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Armaster Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 14;
                    [alert show];
                }
                
                //New samaster
                else if ([source isEqualToString:@"5"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Samaster Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 15;
                    [alert show];
                }
                //Podetail
                else if ([source isEqualToString:@"6"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Podetail Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 16;
                    [alert show];
                }
                //sahisbud
                else if ([source isEqualToString:@"7"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sahisbud Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 17;
                    [alert show];
                }
                //Samaster
                else if ([source isEqualToString:@"8"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Samaster Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 18;
                    [alert show];
                }
                //Smcuspro
                else if ([source isEqualToString:@"9"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Smcuspro Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 19;
                    [alert show];
                }
                //Sodetail
                else if ([source isEqualToString:@"10"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sodetail Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 110;
                    [alert show];
                }
                //Soheader
                else if ([source isEqualToString:@"11"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Soheader Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 111;
                    [alert show];
                }
                //soquohea
                else if ([source isEqualToString:@"12"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquohea Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 112;
                    [alert show];
                }
                //soquodet
                else if ([source isEqualToString:@"13"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquodet Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 113;
                    [alert show];
                }
                //Specials
                else if ([source isEqualToString:@"14"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Specials Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 114;
                    [alert show];
                }
                //sysdesc
                else if ([source isEqualToString:@"15"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdesc Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 115;
                    [alert show];
                }
                //sysdisct
                else if ([source isEqualToString:@"16"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdisct Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 116;
                    [alert show];
                }
                //sysistax
                else if ([source isEqualToString:@"18"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysistax Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 118;
                    [alert show];
                }
                //arpayment
                else if ([source isEqualToString:@"20"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arpayment Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 120;
                    [alert show];
                }
                //Arhishea
                else if ([source isEqualToString:@"21"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arhishea Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 121;
                    [alert show];
                }
                //Arhisdet
                else if ([source isEqualToString:@"22"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arhisdet Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 122;
                    [alert show];
                }
                //NoticeMessages
                else if ([source isEqualToString:@"23"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"NoticeMessages Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 123;
                    [alert show];
                }
                //syscont
                else if ([source isEqualToString:@"24"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"syscont Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 124;
                    [alert show];
                }
                //syscont
                else if ([source isEqualToString:@"25"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"syscont Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 125;
                    [alert show];
                }
                //syslogon
                else if ([source isEqualToString:@"1000"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"syslogon Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 126;
                    [alert show];
                }
                //smdaytim
                else if ([source isEqualToString:@"10011"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"syslogon Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 127;
                    [alert show];
                }
                //sacntrct
                else if ([source isEqualToString:@"1002"]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sacntrct Sync Error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",Nil];
                    alert.tag = 128;
                    [alert show];
                }
            }
            else{
                NSLog(@"CHECK HERE ##@##@@#$#T&^*");
            }
    
        }
        
    }
    //Check Queue is empty or not
    else if (operation == self.downloadOperationQueue && [keyPath isEqualToString:@"operations"])
    {
        if ([self.downloadOperationQueue.operations count] == 0)
        {
            // Do something here when your queue has completed
            NSLog(@"queue has completed");
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if (panel) {
                    [panel hide];
                    
                    [AJNotificationView showNoticeInView:topWindow.rootViewController.view
                                                                 type:AJNotificationTypeGreen
                                                                title:SYNC_COMPLETE_MESSAGE
                                                      linedBackground:AJLinedBackgroundTypeDisabled
                                                            hideAfter:2.5f];
                    
                }
                
            });

            [self executeSyncCompletedOperations];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strProgress = [NSString stringWithFormat:@"%@ (0 %@)",SYNC_IN_PROGRESS_MESSAGE,@"%"];
                NSLog(@"strProgress %@",strProgress);
                self.panel.titleLabel.text = strProgress;
                strProgress = nil;
            });

        }
    }
    
    else {
        [super observeValueForKeyPath:keyPath ofObject:operation
                               change:change context:context];
    }
}


#pragma mark - Notification handler

- (void)dataAvailable:(NSNotification *)notification {
    NSString *source = [notification.userInfo valueForKey:@"source"];
    
    //NSLog(@"Dict response: %@",dictResponse);
    
    //Members table
    if ([source isEqualToString:@"1"]) {
  
    }
    //Armaster
    else if ([source isEqualToString:@"2"]) {
        
    }
    //Artrans
    else if ([source isEqualToString:@"3"]) {
    
    }
    //Arcoment
    else if ([source isEqualToString:@"4"]) {
 
    }
    
    //New samaster
    else if ([source isEqualToString:@"5"]) {
        
        
    }
    //Podetail
    else if ([source isEqualToString:@"6"]) {
        
        
        
    }
    //sahisbud
    else if ([source isEqualToString:@"7"]) {
        
    }
    
    //Samaster
    else if ([source isEqualToString:@"8"]) {
        
        
    }
    //Smcuspro
    else if ([source isEqualToString:@"9"]) {
        
        
    }
    //Sodetail
    else if ([source isEqualToString:@"10"]) {
        
        
    }
    //Soheader
    else if ([source isEqualToString:@"11"]) {
        
        
        
    }
    //soquohea
    else if ([source isEqualToString:@"12"]) {
        
    }
    //soquodet
    else if ([source isEqualToString:@"13"]) {
        
        
    }
    //Specials
    else if ([source isEqualToString:@"14"]) {
    }
    //sysdesc
    else if ([source isEqualToString:@"15"]) {
        
    }
    //sysdisct
    else if ([source isEqualToString:@"16"]) {
        
        
        
    }
    //sysistax
    else if ([source isEqualToString:@"18"]) {
        
    }
    //arpayment
    else if ([source isEqualToString:@"20"]) {
        
        
        
    }
    //Arhishea
    else if ([source isEqualToString:@"21"]) {
        
    }
    //Arhisdet
    else if ([source isEqualToString:@"22"]) {
    }
    //NoticeMessages
    else if ([source isEqualToString:@"23"]) {
        
    }
    //syscont
    else if ([source isEqualToString:@"24"]) {
        
    }
    //syslogon
    else if ([source isEqualToString:@"30"]) {
        
    }
    
    
}

- (void)dataUnAvailable:(NSNotification *)notification {
    NSLog(@"Source : %@",[notification.userInfo valueForKey:@"source"]);
    NSLog(@"Error : %@",[notification.userInfo valueForKey:@"error"]);
    
    NSError *error= [notification.userInfo valueForKey:@"error"];
    
    //No internet
    if (error.code == -1009 || error.code == -1005 || error.code == -1004||error.code == -1001) {
        
        NSString *strError = error.localizedDescription;
        [self showSyncErrorMsg:strError];
        [downloadOperationQueue cancelAllOperations];
    }
    
    //Cancelled error code
    if (error.code == 123) {
        self.isQueueCancelled = YES;
    }
}

- (void)initializeDateFormatter {
    if (!self.dateFormatter) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        [self.dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    }
}

- (NSDate *)dateUsingStringFromAPIXml:(NSString *)dateString {
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [self.dateFormatter dateFromString:dateString];
}


- (NSString *)dateStringForAPIUsingDateXml:(NSDate *)date {
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSString *dateString = [self.dateFormatter stringFromDate:date];
    dateString  = [dateString stringByAppendingString:@"+11:00"];
    return dateString;
}

- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString {
    [self initializeDateFormatter];
    return [self.dateFormatter dateFromString:dateString];
}

- (NSString *)dateStringForAPIUsingDate:(NSDate *)date {
    [self initializeDateFormatter];
    NSString *dateString = [self.dateFormatter stringFromDate:date];
    return dateString;
}


#pragma mark - File Management

- (NSURL *)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)JSONDataRecordsDirectory{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL URLWithString:@"JSONRecords/" relativeToURL:[self applicationCacheDirectory]];
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:[url path]]) {
        [fileManager createDirectoryAtPath:[url path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return url;
}

- (void)writeJSONResponse:(id)response toDiskForClassWithName:(NSString *)className {
    NSURL *fileURL = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    if (![(NSDictionary *)response writeToFile:[fileURL path] atomically:YES]) {
        NSLog(@"Error saving response to disk, will attempt to remove NSNull values and try again.");
        // remove NSNulls and try again...
        NSArray *records = [response objectForKey:@"results"];
        NSMutableArray *nullFreeRecords = [NSMutableArray array];
        for (NSDictionary *record in records) {
            NSMutableDictionary *nullFreeRecord = [NSMutableDictionary dictionaryWithDictionary:record];
            [record enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                if ([obj isKindOfClass:[NSNull class]]) {
                    [nullFreeRecord setValue:nil forKey:key];
                }
            }];
            [nullFreeRecords addObject:nullFreeRecord];
        }
        
        NSDictionary *nullFreeDictionary = [NSDictionary dictionaryWithObject:nullFreeRecords forKey:@"results"];
        
        if (![nullFreeDictionary writeToFile:[fileURL path] atomically:YES]) {
            NSLog(@"Failed all attempts to save reponse to disk: %@", response);
        }
    }
}

- (void)deleteJSONDataRecordsForClassWithName:(NSString *)className {
    NSURL *url = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    NSError *error = nil;
    BOOL deleted = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
    if (!deleted) {
        NSLog(@"Unable to delete JSON Records at %@, reason: %@", url, error);
    }
}

- (NSDictionary *)JSONDictionaryForClassWithName:(NSString *)className {
    NSURL *fileURL = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    return [NSDictionary dictionaryWithContentsOfURL:fileURL];
}

- (NSArray *)JSONDataRecordsForClass:(NSString *)className sortedByKey:(NSString *)key {
    NSDictionary *JSONDictionary = [self JSONDictionaryForClassWithName:className];
    NSArray *records = [JSONDictionary objectForKey:@"results"];
    return [records sortedArrayUsingDescriptors:[NSArray arrayWithObject:
                                                 [NSSortDescriptor sortDescriptorWithKey:key ascending:YES]]];
}

/*
-(NSString *)callCreateSoapStringSalesOrder:(int)finalizeTag
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserName = [prefs stringForKey:@"userName"];
    
    
    NSString *strDeliveryDate = @"";
    NSString *strContact = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strAllowPartial = @"";
    NSString *strBranch = @"";
    NSString *strCarrier = @"";
    NSString *strChargeRate = @"";
    NSString *strChargetype = @"";
    NSString *strCountry = @"";
    NSString *strDirect = @"";
    NSString *strDropSequence = @"";
    NSString *strExchangeCode = @"";
    NSString *strExportDebtor = @"";
    NSString *strHeld = @"";
    NSString *strNumboxes = @"0";
    NSString *strOrderDate = @"";
    NSString *strPeriodRaised = @"";
    NSString *strPostCode = @"";
    NSString *strPriceCode = @"";
    NSString *strSalesBranch = @"";
    NSString *strSalesman = @"";
    NSString *strTaxExemption1 = @"";
    NSString *strTaxExemption2 = @"";
    NSString *strTaxExemption3 = @"";
    NSString *strTaxExemption4 = @"";
    NSString *strTaxExemption5 = @"";
    NSString *strTaxExemption6 = @"";
    NSString *strTradingTerms = @"";
    NSString *strYearRaised = @"";
    NSString *strStockPickup = @"";
    NSString *strWarehouse = @"";
    NSString *strCashRepPickUp = @"";
    NSString *strCashAmount = @"";
    NSString *strCashCollect = @"";
    NSString *strDateRaised = @"";
    NSString *strDelName = @"";
    NSString *strOperator = @"";
    NSString *strStatus = @"";
    NSString *strOrderNum = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strDeliveryRun = @"";
    NSString *strDebtor = @"";
    NSString *strCustOrder = @"";
    NSString *strFuelLevy = @"";
    NSString *strOrderValue = @"";
    NSString *strTotalLines = @"";
    
    if ([dictHeaderDetails objectForKey:@"ORDER_VALUE"]) {
        strOrderValue = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ORDER_VALUE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DETAIL_LINES"]) {
        strTotalLines = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DETAIL_LINES"]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"DEL_NAME"]) {
        strDelName = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_NAME"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"OPERATOR"]) {
        strOperator = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"OPERATOR"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"WAREHOUSE"]&&![[dictHeaderDetails objectForKey:@"WAREHOUSE"] isEqual:[NSNull null]]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"WAREHOUSE"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"STATUS"]) {
        strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"STATUS"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ORDER_NO"]) {
        strOrderNum = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ORDER_NO"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_INST1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_INST1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_INST2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_INST2"]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"DELIVERY_RUN"]) {
        strDeliveryRun = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DELIVERY_RUN"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEBTOR"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEBTOR"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DATE_RAISED"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DATE_RAISED"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CUST_ORDER"]) {
        strCustOrder = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CUST_ORDER"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_AMOUNT"]) {
        strCashAmount = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_AMOUNT"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_COLLECT"]) {
        strCashCollect = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_COLLECT"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"]) {
        strCashRepPickUp = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CASH_REP_PICKUP"]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"DEL_DATE"]) {
        strDeliveryDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_DATE"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CONTACT"]) {
        strContact = [[dictHeaderDetails objectForKey:@"CONTACT"] trimSpaces];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS2"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"DEL_ADDRESS3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_ADDRESS3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_SUBURB"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_SUBURB"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ALLOW_PARTIAL"]) {
        strAllowPartial = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ALLOW_PARTIAL"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"BRANCH"]) {
        strBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"BRANCH"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CARRIER_CODE"]) {
        strCarrier = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CARRIER_CODE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CHARGE_RATE"]) {
        strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_RATE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"CHARGE_TYPE"]) {
        strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_TYPE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DEL_COUNTRY"]) {
        strCountry = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_COUNTRY"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DIRECT"]) {
        strDirect = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DIRECT"]];
        
    }
    if ([dictHeaderDetails objectForKey:@"DROP_SEQ"]) {
        strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DROP_SEQ"]];
    }
    if ([dictHeaderDetails objectForKey:@"EXCHANGE_CODE"]) {
        strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EXCHANGE_CODE"]];
    }
    if ([dictHeaderDetails objectForKey:@"EXPORT_DEBTOR"]) {
        strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"EXPORT_DEBTOR"]];
    }
    if ([dictHeaderDetails objectForKey:@"HELD"]) {
        strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"HELD"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"NUM_BOXES"]) {
        strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"NUM_BOXES"]];
    }
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderDate"]) {
        strOrderDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderDate"]];
    }
    if ([dictHeaderDetails objectForKey:@"PERIOD_RAISED"]) {
        strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PERIOD_RAISED"]];
    }
    if ([dictHeaderDetails objectForKey:@"DEL_POST_CODE"]) {
        strPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_POST_CODE"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"PRICE_CODE"]) {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PRICE_CODE"]];
    }
    if ([dictHeaderDetails objectForKey:@"SALES_BRANCH"]) {
        strSalesBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"SALES_BRANCH"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"SALESMAN"]) {
        strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SALESMAN"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION1"]) {
        strTaxExemption1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION1"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION2"]) {
        strTaxExemption2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION2"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION3"]) {
        strTaxExemption3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION3"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION4"]) {
        strTaxExemption4 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION4"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION5"]) {
        strTaxExemption5 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION5"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"TAX_EXEMPTION6"]) {
        strTaxExemption6 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"TAX_EXEMPTION6"] trimSpaces]];
    }
    if ([dictHeaderDetails objectForKey:@"TRADING_TERMS"]) {
        strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TRADING_TERMS"]];
    }
    if ([dictHeaderDetails objectForKey:@"YEAR_RAISED"]) {
        strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YEAR_RAISED"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"STOCK_RETURNS"]){
        strStockPickup = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"STOCK_RETURNS"]];
    }
    
    
    NSString *strStockCode = @"";
    NSString *strPrice = @"";
    NSString *strDescription = @"";
    NSString *strExtension = @"";
    NSString *strItem = @"";
    NSString *strDissection = @"";
    NSString *strDissection_Cos = @"";
    NSString *strWeight = @"";
    NSString *strVolume = @"";
    NSString *strCost = @"";
    NSString *strQuantityOrdered = @"";
    NSString *strGross = @"";
    NSString *strTax = @"";
    
    for (NSDictionary *dict in arrProducts) {
        
        //Comments
        if ([dict objectForKey:@"ITEM"] && [[dict objectForKey:@"ITEM"] isEqualToString:@"/C"]){
            
            strStockCode = @"/C";
            strDescription = [[dict objectForKey:@"DESCRIPTION"] trimSpaces];
            strItem = [strItem stringByAppendingFormat:@"<product><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>0</PRICE><EXTENSION>0</EXTENSION><QUANTITY>0</QUANTITY><ALT_QTY>0</ALT_QTY><ORIG_ORD_QTY>0</ORIG_ORD_QTY><ORD_QTY>0</ORD_QTY><BO_QTY>0</BO_QTY><CONV_FACTOR>0</CONV_FACTOR><REFRESH_PRICE>0</REFRESH_PRICE><DISSECTION>0</DISSECTION><DISSECTION_COS>0</DISSECTION_COS><WEIGHT>0</WEIGHT><VOLUME>0</VOLUME><COST>0</COST><ORIG_COST>0</ORIG_COST><GROSS>0</GROSS><TAX>0</TAX><CREATE_OPERATOR>%@</CREATE_OPERATOR></product>",strStockCode,strDescription,strUserName];
            
        }
        //Product
        else{
            
            
            strStockCode = [dict objectForKey:@"ITEM"];
            strPrice = [dict objectForKey:@"PRICE"];
            strDissection = [[dict objectForKey:@"DISSECTION"] trimSpaces];
            strDissection_Cos = [[dict objectForKey:@"DISSECTION_COS"] trimSpaces];
            strWeight = [dict objectForKey:@"WEIGHT"];
            strVolume = [dict objectForKey:@"VOLUME"];
            
            strDescription = [[dict objectForKey:@"DESCRIPTION"] trimSpaces];
            strCost = [dict objectForKey:@"COST"];
            
            if ([dict objectForKey:@"EXTENSION"]) {
                strExtension = [dict objectForKey:@"EXTENSION"];
            }
            
            strQuantityOrdered = [dict objectForKey:@"QUANTITY"];
            
            
            strVolume = [dict objectForKey:@"VOLUME"];
            
            
            strGross = [dict objectForKey:@"GROSS"];
            strTax = [dict objectForKey:@"TAX"];
            
            
            strItem = [strItem stringByAppendingFormat:@"<product><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><ORIG_ORD_QTY>%@</ORIG_ORD_QTY><ORD_QTY>%@</ORD_QTY><BO_QTY>0.0000</BO_QTY><CONV_FACTOR>1.0000</CONV_FACTOR><REFRESH_PRICE>N</REFRESH_PRICE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><COST>%@</COST><ORIG_COST>0.0000</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><CREATE_OPERATOR>%@</CREATE_OPERATOR></product>",strStockCode,strDescription,strPrice,strExtension,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strQuantityOrdered,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strGross,strTax,strUserName];
        }
    }
    
    strFuelLevy = @"Y";
    //QUANTITY = ALT_QTY = ORIG_ORD_QTY = ORD_QTY = BO_QTY
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:InsertSalesOrder soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "<salesOrdHeader xsi:type=\"wws:SalesOrdHeaRequest\">"
                            "<DEBTOR>%@</DEBTOR>"
                            "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"//editable
                            "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"//editable
                            "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"//editable
                            "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"//editable
                            "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"//editable
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"//editable
                            "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"//editable
                            "<CONTACT>%@</CONTACT>"//editable
                            "<DEL_DATE><![CDATA[%@]]></DEL_DATE>"
                            "<DATE_RAISED><![CDATA[%@]]></DATE_RAISED>"//todays date
                            "<CUST_ORDER>%@</CUST_ORDER>"//editable
                            "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"//editable
                            "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"//editable
                            "<STATUS><![CDATA[%@]]></STATUS>"//always 1
                            "<DIRECT><![CDATA[%@]]></DIRECT>"
                            "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
                            "<BRANCH><![CDATA[%@]]></BRANCH>"
                            "<PRICE_CODE><![CDATA[%@]]></PRICE_CODE>"
                            "<SALESMAN><![CDATA[%@]]></SALESMAN>"
                            "<SALES_BRANCH><![CDATA[%@]]></SALES_BRANCH>"
                            "<TRADING_TERMS><![CDATA[%@]]></TRADING_TERMS>"
                            "<DELIVERY_RUN><![CDATA[%@]]></DELIVERY_RUN>"
                            "<TAX_EXEMPTION1><![CDATA[%@]]></TAX_EXEMPTION1>"
                            "<TAX_EXEMPTION2><![CDATA[%@]]></TAX_EXEMPTION2>"
                            "<TAX_EXEMPTION3><![CDATA[%@]]></TAX_EXEMPTION3>"
                            "<TAX_EXEMPTION4><![CDATA[%@]]></TAX_EXEMPTION4>"
                            "<TAX_EXEMPTION5><![CDATA[%@]]></TAX_EXEMPTION5>"
                            "<TAX_EXEMPTION6><![CDATA[%@]]></TAX_EXEMPTION6>"
                            "<EXCHANGE_CODE><![CDATA[%@]]></EXCHANGE_CODE>"
                            "<ALLOW_PARTIAL><![CDATA[%@]]></ALLOW_PARTIAL>"
                            "<CHARGE_TYPE><![CDATA[%@]]></CHARGE_TYPE>"
                            "<CARRIER_CODE><![CDATA[%@]]></CARRIER_CODE>"
                            "<EXPORT_DEBTOR><![CDATA[%@]]></EXPORT_DEBTOR>"
                            "<DROP_SEQ><![CDATA[%@]]></DROP_SEQ>"
                            "<HELD><![CDATA[%@]]></HELD>"
                            "<CHARGE_RATE><![CDATA[%@]]></CHARGE_RATE>"
                            "<NUM_BOXES><![CDATA[%@]]></NUM_BOXES>"
                            "<CASH_COLLECT><![CDATA[%@]]></CASH_COLLECT>"
                            "<CASH_AMOUNT><![CDATA[%@]]></CASH_AMOUNT>"
                            "<CASH_REP_PICKUP><![CDATA[%@]]></CASH_REP_PICKUP>"
                            "<OPERATOR><![CDATA[%@]]></OPERATOR>"
                            "<STOCK_RETURNS><![CDATA[%@]]></STOCK_RETURNS>"
                            "<PERIOD_RAISED><![CDATA[%@]]></PERIOD_RAISED>"
                            "<YEAR_RAISED><![CDATA[%@]]></YEAR_RAISED>"
                            "<DETAIL_LINES><![CDATA[%@]]></DETAIL_LINES>"
                            "<ORDER_VALUE><![CDATA[%@]]></ORDER_VALUE>"
                            "</salesOrdHeader>"
                            "<salesOrdDetails xsi:type=\"wws:SalesOrdDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                            "%@"
                            "</salesOrdDetails>"
                            "</wws:InsertSalesOrder>",strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strPostCode,strCountry,strContact,strDeliveryDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,strCashCollect,strCashAmount,strCashRepPickUp,strUserName,strStockPickup,strPeriodRaised,strYearRaised,strTotalLines,strOrderValue,strItem];
    
    NSLog(@"soapXMLStr %@",soapXMLStr);
    
    return soapXMLStr;
}*/

#pragma mark --New Methods as per database changes


-(void)callsacntrctWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
        //--Flush All Records
        [self DeleteAllRecordsFromTable:@"sacntrct"];
    }
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sacntrct"];
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
        strRowversion = strRecNum = @"1";
        NSLog(@"nil nil11");
    }
    else
    {
        strRecNum = [dictinfo objectForKey:@"RECNUM"];
        strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
        if(strRowversion.length <=0)
        {
            strRowversion = strRecNum = @"1";
        }
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SACNTRCT_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSLog(@"SACNTRCT strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 1002;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    
}

-(void)callsmdaytimWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
        //--Flush All Records
        [self DeleteAllRecordsFromTable:@"smdaytim"];
    }
    
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"smdaytim"];
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
        strRowversion = strRecNum = @"1";
        NSLog(@"nil nil11");
    }
    else
    {
        strRecNum = [dictinfo objectForKey:@"RECNUM"];
        strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
        if(strRowversion.length <=0)
        {
            strRowversion = strRecNum = @"1";
        }
        
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SMDAYTIM_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSLog(@"SMDAYTIM_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 10011;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    
}


-(void)callSyslogonWebservicewithDatabasechanges:(bool)Flush
{
    
    if(Flush == YES)
    {
        //--Flush All Records
        [self DeleteAllRecordsFromTable:@"syslogon"];
    }
    
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"syslogon"];
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
        strRowversion = strRecNum = @"1";
        NSLog(@"nil nil11");
    }
    else
    {
        strRecNum = [dictinfo objectForKey:@"RECNUM"];
        strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
        if(strRowversion.length <=0)
        {
            strRowversion = strRecNum = @"1";
        }
        
    }

    
//    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdisct"];
    
//    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSLOGON_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SYSLOGON_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];

    
    NSLog(@"SYSLOGON_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 1000;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callsysistaxWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"sysistax"];
    }
    
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sysistax"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SYSISTAX_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
  
  NSLog(@"sysistax_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.strFlush = @"";
  if(Flush == YES)
  {
      operation.strFlush = BulkInsertion;
  }
  operation.tag = 18;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
  
}


-(void)callsysdisctWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"sysdisct"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sysdisct"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SYSDISCT_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
  
  NSLog(@"SYSDISCT_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 16;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}


-(void)callsysdescWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"sysdesc"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sysdesc"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
   strRowversion = strRecNum = @"1";
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SYSDESC_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
  
  NSLog(@"syscont strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 15;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}


-(void)callsyscontWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"syscont"];
    }
    
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"syscont"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SYSCONT_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
 
  NSLog(@"syscont strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 24;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}


-(void)callsoquoheaWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"soquohea"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"soquohea"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    
//  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SOQUOHEA_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,SOQUOHEA_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];

  NSLog(@"SOQUOHEA_NEW strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 12;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}



//soquodet
-(void)callSoquodetWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
        //--Flush All Records
        [self DeleteAllRecordsFromTable:@"soquodet"];
    }
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"soquodet"];
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
        strRowversion = strRecNum = @"1";
        NSLog(@"nil nil11");
    }
    else
    {
        strRecNum = [dictinfo objectForKey:@"RECNUM"];
        strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
        if(strRowversion.length <=0)
        {
            strRowversion = strRecNum = @"1";
        }
        
    }
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SOQUODET_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,SOQUODET_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
    
    NSLog(@"SOQUODET_WS strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 13;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}



//--Soheader
-(void)callSoheaderWebWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"soheader"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"soheader"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,SOHEADER_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
  
  NSLog(@"soheader strUrl : %@",strUrl);
  
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 11;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}


//Sodetail
-(void)callSodetailWebWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"sodetail"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sodetail"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
//  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SODETAIL_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,SODETAIL_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
    
  
  NSLog(@"SODETAIL_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.strFlush = @"";
  if(Flush == YES)
  {
      operation.strFlush = BulkInsertion;
  }
  operation.tag = 10;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
   [downloadOperationQueue addOperation:operation];
}

//Smcuspro
-(void)callSmcusprobudWebWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"smcuspro"];
    }
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"smcuspro"];
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
      strRowversion = strRecNum = @"1";
      NSLog(@"nil nil11");
    }
    else
    {
      strRecNum = [dictinfo objectForKey:@"RECNUM"];
      strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
      if(strRowversion.length <=0)
      {
        strRowversion = strRecNum = @"1";
      }
      
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SMCUSPRO_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSLog(@"Smcuspro strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 9;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
  
}
//Sahibud
-(void)callSahisbudWebWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"sahisbud"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"sahisbud"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SAHISBUD_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
  
  NSLog(@"sahisbud strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 7;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}
//Podetail
-(void)callPodetailWebWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
        //--Flush All Records
        [self DeleteAllRecordsFromTable:@"podetail"];
    }
    
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"podetail"];
    
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
        strRowversion = strRecNum = @"1";
        NSLog(@"nil nil11");
    }
    else
    {
        strRecNum = [dictinfo objectForKey:@"RECNUM"];
        strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
        if(strRowversion.length <=0)
        {
            strRowversion = strRecNum = @"1";
        }
        
    }
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,PODETAIL_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,PODETAIL_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
    
    
    NSLog(@"PODETAIL_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 6;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}
//--Armaster
-(void)callArmasterWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"armaster"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"armaster"];
  
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,ARMASTER_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];

  NSLog(@"armaster strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 2;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation star
}


//--Artran
-(void)callArtransWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"artrans"];
    }
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"artrans"];
    
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
      strRowversion = strRecNum = @"1";
      NSLog(@"nil nil11");
    }
    else
    {
      strRecNum = [dictinfo objectForKey:@"RECNUM"];
      strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
      if(strRowversion.length <=0)
      {
        strRowversion = strRecNum = @"1";
      }
      
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];

    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,ARTRANS_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
    NSLog(@"artrans strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 3;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
  
}
//--Arhis
-(void)callArhisheaWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"arhishea"];
    }
    NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"arhishea"];
    
    NSString *strRowversion,*strRecNum;
    
    //--1st time with no data
    if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
    {
      strRowversion = strRecNum = @"1";
      NSLog(@"nil nil11");
    }
    else
    {
      strRecNum = [dictinfo objectForKey:@"RECNUM"];
      strRowversion = [dictinfo objectForKey:@"RowVersion"];
    }
    
    if(![strRowversion isKindOfClass:[NSNumber class]])
    {
      if(strRowversion.length <=0)
      {
        strRowversion = strRecNum = @"1";
      }
      
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,ARHISHEA_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];

    
    NSLog(@"arhishea strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 21;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
  
}
//--Arpayment
-(void)callArpaymentWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"arpaymnt"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"arpaymnt"];
  
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];

    
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,ARPAYMENT_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
  NSLog(@"ARPAYMENT_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 20;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added

}


//--Arhisdet
-(void)callArhisdetWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
    [self DeleteAllRecordsFromTable:@"arhisdet"];
    }
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"arhisdet"];
  
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
  
//  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,ARHISDET_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserMember = [NSString stringWithFormat:@"members=%@&salesman=%@",[prefs objectForKey:@"members"],[prefs objectForKey:@"userName"]];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@&%@",WSURL,ARHISDET_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion],strUserMember];
    
  NSLog(@"ARHISDET_WS strUrl : %@",strUrl);
    
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 22;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}


//--Samaster
-(void)callSamasterWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
    //--Flush All Records
     [self DeleteAllRecordsFromTable:@"samaster"];
    }
    
  NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"samaster"];
  
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }

  }
  
  NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,SAMASTER_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];
  
  NSLog(@"SAMASTER_WS strUrl : %@",strUrl);
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  operation.tag = 8;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}


//--Arcomment
-(void)callArcommentWebservicewithDatabasechanges:(bool)Flush
{
    if(Flush == YES)
    {
  //--Flush All Records
  [self DeleteAllRecordsFromTable:@"arcoment"];
    }
 NSDictionary *dictinfo = [self ReturnLastRowandRecnum:@"arcoment"];
  NSString *strRowversion,*strRecNum;
  
  //--1st time with no data
  if([dictinfo objectForKey:@"RECNUM"]== [NSNull null] && [dictinfo objectForKey:@"RowVersion"]== [NSNull null])
  {
    strRowversion = strRecNum = @"1";
    NSLog(@"nil nil11");
  }
  else
  {
    strRecNum = [dictinfo objectForKey:@"RECNUM"];
    strRowversion = [dictinfo objectForKey:@"RowVersion"];
  }
  
  if(![strRowversion isKindOfClass:[NSNumber class]])
  {
    if(strRowversion.length <=0)
    {
      strRowversion = strRecNum = @"1";
    }
    
  }
 NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&%@",WSURL,ARCOMMENT_NEW,[NSString stringWithFormat:@"recnum=%@",strRecNum],[NSString stringWithFormat:@"rowversion=%@",strRowversion]];

  
  NSURL *url = [NSURL URLWithString:strUrl];
  DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
  if(Flush == YES)
  {
  operation.strFlush = BulkInsertion;
  }
   operation.tag = 4;
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
  [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
}


- (NSDictionary *)ReturnLastRowandRecnum:(NSString *)tableName {
  
  FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
  __block NSDictionary *dictData;
  NSString *strQuery = [NSString stringWithFormat:@"SELECT Recnum,RowVersion FROM %@ ORDER BY  RowVersion desc limit 1",tableName];
  [databaseQueue  inDatabase:^(FMDatabase *db) {
    FMResultSet *results = [db executeQuery:strQuery];
    
    if ([results next]){
      dictData = [results resultDictionary];
      
      }
    else{
      //NSLog(@"table doesnt exist in db");
    }
    
  }];
  
  return dictData;
}

-(void)DeleteAllRecordsFromTable:(NSString *)tableName
{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            
            BOOL Result;
            NSString *strQuerty = [NSString stringWithFormat:@"Delete From %@",tableName];
             Result = [db executeUpdate:strQuerty];
            
            
            if (Result == NO)
            {
              
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            else
            {
                NSLog(@"Deleted data::%@",tableName);
            }
            
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}

-(BOOL)CheckforNullString:(NSString *)str
{
  if(![str isKindOfClass:[NSString class]])
  {
    return NO;
  }
  if (str == (id)[NSNull null] || str.length == 0 )
  {
    return YES ;
  }
  return NO;
  
}


#pragma mark -
#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    

    
    // Armaster
    if (alertView.tag == 12) {
        if (buttonIndex == 1) {
            [self callArmasterWebservicewithDatabasechanges:YES];
        }
    }
    
    //Artrans
    if (alertView.tag == 13) {
        if (buttonIndex == 1) {
            [self callArtransWebservicewithDatabasechanges:YES];
        }
    }
    
    //Arcomment
    if (alertView.tag == 14) {
        if (buttonIndex == 1) {
            [self callArcommentWebservicewithDatabasechanges:YES];
        }
    }
    
    //New samaster
    if (alertView.tag == 15) {
        if (buttonIndex == 1) {
            [self callSamasterWebservicewithDatabasechanges:YES];
        }
    }
    //Podetail
    if (alertView.tag == 16) {
        if (buttonIndex == 1) {
            [self callPodetailWebWebservicewithDatabasechanges:YES];
        }
    }
    //sahisbud
    if (alertView.tag == 17) {
        if (buttonIndex == 1) {
            [self callSahisbudWebWebservicewithDatabasechanges:YES];
        }
    }
    //Samaster
    if (alertView.tag == 18) {
        if (buttonIndex == 1) {
             [self callSamasterWebservicewithDatabasechanges:YES];
        }
    }
    //Smcuspro
    if (alertView.tag == 19) {
        if (buttonIndex == 1) {
          [self callSmcusprobudWebWebservicewithDatabasechanges:YES];
        }
    }
    //Sodetail
    if (alertView.tag == 110) {
        if (buttonIndex == 1) {
            [self callSodetailWebWebservicewithDatabasechanges:YES];
        }
    }
    //Soheader
    if (alertView.tag == 111) {
        if (buttonIndex == 1) {
           [self callSoheaderWebWebservicewithDatabasechanges:YES];
        }
    }
    //soquohea
    if (alertView.tag == 112) {
        if (buttonIndex == 1) {
            [self callsoquoheaWebservicewithDatabasechanges:YES];
        }
    }
    //soquodet
    if (alertView.tag == 113) {
        if (buttonIndex == 1) {
            [self callSoquodetWebservicewithDatabasechanges:YES];
        }
    }

    //sysdesc
    if (alertView.tag == 115) {
        if (buttonIndex == 1) {
              [self callsysdescWebservicewithDatabasechanges:YES];
        }
    }
    //sysdisct
    if (alertView.tag == 116) {
        if (buttonIndex == 1) {
          [self callsysdisctWebservicewithDatabasechanges:YES];
        }
    }
    //sysistax
    if (alertView.tag == 118) {
        if (buttonIndex == 1) {
           [self callsysistaxWebservicewithDatabasechanges:YES];
        }
    }
    //arpayment
    if (alertView.tag == 120) {
        if (buttonIndex == 1) {
         [self callArpaymentWebservicewithDatabasechanges:YES];
        }
    }
    //Arhishea
    if (alertView.tag == 121) {
        if (buttonIndex == 1) {
           [self callArhisheaWebservicewithDatabasechanges:YES];
        }
    }
    //Arhisdet
    if (alertView.tag == 122) {
        if (buttonIndex == 1) {
            [self callArhisdetWebservicewithDatabasechanges:YES];
        }
    }
    //syscont
    if (alertView.tag == 124) {
        if (buttonIndex == 1) {
           [self callsyscontWebservicewithDatabasechanges:YES];
        }
    }
    
    // syslogon
    if (alertView.tag == 126) {
        if (buttonIndex == 1) {
            [self callSyslogonWebservicewithDatabasechanges:YES];
        }
    }

    // smdaytim
    if (alertView.tag == 127) {
        if (buttonIndex == 1) {
            [self callsmdaytimWebservicewithDatabasechanges:YES];
        }
    }
    
    // sacntrct
    if (alertView.tag == 128) {
        if (buttonIndex == 1) {
            [self callsmdaytimWebservicewithDatabasechanges:YES];
        }
    }
}

@end
