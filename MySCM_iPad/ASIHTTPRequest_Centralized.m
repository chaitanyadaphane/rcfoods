

#import "ASIHTTPRequest_Centralized.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"


@implementation ASIHTTPRequest_Centralized

@synthesize delegate,spinner,isWithoutSpinner;

-(void)CallingWebservice:(NSString *)strURL onView:(UIView *)onView
{
    NSLog(@"strURL==%@",strURL);
    
//    self.isWithoutSpinner = NO;
//    self.spinner = [MBProgressHUD showHUDAddedTo:onView animated:YES];
    
	NSURL *url = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setTimeOutSeconds:10];
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}


-(void)CallingWebservicePost:(NSString *)strURL onView:(UIView *)onView
{
  NSLog(@"strURL==%@",strURL);
  
  //    self.isWithoutSpinner = NO;
  //    self.spinner = [MBProgressHUD showHUDAddedTo:onView animated:YES];
  
	NSURL *url = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
  request = [[ASIHTTPRequest alloc] initWithURL:url];
  [request setTimeOutSeconds:10];
  [request setDelegate:self];
  [request setRequestMethod:@"POST"];
  [request startAsynchronous];
}

-(void)CallingWebserviceWithoutSpinner:(NSString *)strURL
{
    self.isWithoutSpinner = YES;
	NSURL *url = [NSURL URLWithString:[strURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setTimeOutSeconds:10];
    [request setDelegate:self];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}


-(void)CallingWebservicePost:(NSString *)soapXMLStr soapAction:(NSString*) strSoapAction onView:(UIView *)onView
{    
//	self.isWithoutSpinner = NO;
//    spinner = [MBProgressHUD showHUDAddedTo:onView animated:YES];
    
    NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wws=\"%@\">"
                             "<soapenv:Header/>"
                             "<soapenv:Body>"
                                 "%@"
                             "</soapenv:Body>"
        "</soapenv:Envelope>",WSURL_WSDL,soapXMLStr];
    
    //NSLog(@"soapMessage %@ " , soapMessage);
    NSURL *url = [NSURL URLWithString:WSURL_WSDL];
    
    request = [[ASIHTTPRequest alloc] initWithURL:url];
    [request setTimeOutSeconds:30];    
    [request setDelegate:self];
       
    [request addRequestHeader:@"Content-Type" value:@"text/xml; charset=utf-8"];
    [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%d",[soapMessage length]]];
    [request addRequestHeader:@"SOAPAction" value:strSoapAction];
    [request setPostLength:[soapMessage length]];
    
    [request setRequestMethod:@"POST"];
    [request setPostBody:[[[soapMessage dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]autorelease]];
    
    [request startAsynchronous];
    
}


#pragma mark - ASIHTTP Request delegates

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    expectedLength = [requestParameter contentLength];
	currentLength = 0;
    
    if (!isWithoutSpinner) {
        spinner.mode = MBProgressHUDModeIndeterminate;
    }
    
    // Use when fetching text data
    NSString *responseString = [requestParameter responseString];
    ////NSLog(@"%@",responseString);
    
    if (!isWithoutSpinner) {
        [delegate ASIHTTPRequest_Success:responseString nsMutableData:[requestParameter responseData] Spinner:spinner ExpectedLength:expectedLength CurrentLength:currentLength];
    }
    else{
        
        // CrashesALWAYS CHECKPOINT
        [delegate ASIHTTPRequest_Success:responseString nsMutableData:[requestParameter responseData] Spinner:nil ExpectedLength:expectedLength CurrentLength:currentLength];
    }

    requestParameter = nil;
    request = nil;
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    
    NSError *error = [requestParameter error];
    [delegate ASIHTTPRequest_Error:error];
    
    requestParameter = nil;
    request = nil;
    
    if (!isWithoutSpinner) {
        if (spinner) {
            [spinner hide:YES];
        }

    }
    
       
}

//Cancel Request
-(void)cancelRequest{
    
    [request setDelegate:nil];
    [request cancel];
    [request  release];
    request = nil;
}

//Return current request
-(ASIHTTPRequest *)getCurrentRequest{
    return request;
}

-(void)dealloc
{
    [super dealloc];
    
    delegate = nil;
    request = nil;
}

@end
