//
//  QuantityCell.h
//  RCFoods
//
//  Created by Pooja Mishra on 30/03/17.
//  Copyright © 2017 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuantityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *plusbtn;
@property (weak, nonatomic) IBOutlet UIButton *minusbtn;
@property (weak, nonatomic) IBOutlet UITextField *quantityTextfield;

@end
