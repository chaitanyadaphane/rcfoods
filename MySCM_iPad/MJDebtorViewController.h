//
//  MJDebtorViewController
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDebtorViewController : UIViewController{
    
}
@property (nonatomic,retain) NSMutableDictionary *detailDict;


@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblView;

//@property (nonatomic,unsafe_unretained) IBOutlet UITextView *txtView;
@end
