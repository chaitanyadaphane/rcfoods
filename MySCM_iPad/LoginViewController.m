	//
//  LoginViewController.m
//  MySCM
//
//  Created by Ashutosh Dingankar on 03/12/12.
//  Copyright (c) 2012 Ashutosh Dingankar. All rights reserved.
//

#import "LoginViewController.h"
#import "RootViewController.h"
#import "NSString+MD5.h"

#define LOGINWS @"login.php?"

int const tagOftxtUsername = 1;
int const tagOftxtPassword = 2;
float const upframeByYPosition = 38;
float const downframeByYPosition = 0;


@interface LoginViewController ()
@end

@implementation LoginViewController
@synthesize spinner,txtPassword,txtUsername,btnLogin,scView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].isNetworkConnected = YES;
        }
        
        // Custom initialization
    }
    return self;
}


#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGSize viewSize = [AppDelegate getRotatedViewSize:self.view];
    self.view.frame = CGRectMake(0, 0, viewSize.width, viewSize.height);
    spinner.hidden = TRUE;
    backgroundQueueForLogin = dispatch_queue_create("com.nanan.myscmipad.bgqueueForLogin", NULL);
    NSString *usernameStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]];
    NSString *passwordStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"password"]];
    
    if(usernameStr.length > 0  && ![usernameStr containsString:@"null"])
    {
        txtUsername.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]];
        if (passwordStr.length > 0  && ![passwordStr containsString:@"null"])
        {
            txtPassword.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"password"]];
        }
        else
        {
            txtPassword.text = @"";
        }
    }
    else
    {
        txtUsername.text = @"";
        txtPassword.text = @"";
    }
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   // self.txtPassword.text = @"";
    if([[AppDelegate getAppDelegateObj]CheckIfDemoApp])
    {
        self.imgBackGround.image = [UIImage imageNamed:@"bly_login.png"];
    }
}


- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.btnLogin = nil;
//    self.txtPassword = nil;
    self.spinner = nil;
//    self.txtUsername = nil;
    self.scView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - actionLoginClicked
-(IBAction)actionLoginClicked:(id)sender{
    
    NSString *strUserName = txtUsername.text;
    NSString *strPassword = txtPassword.text;
    
    BOOL isValidUserName = [AppDelegate validateBlank:strUserName];
    BOOL isValidPassword = [AppDelegate validateBlank:strPassword];
    
    UIAlertView *alert;
    if (isValidUserName) {
                       
        alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid username" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
         [alert show];
    }
    else if (isValidPassword){
             
        alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];

        [alert show];
    }
    else{
        
        [self disableView];
        spinner.hidden = FALSE;
        [spinner startAnimating];
        
        if ([AppDelegate getAppDelegateObj].isNetworkConnected) {
            [self callWSForLogin];
        }
        else{
            
            #ifdef DemoAppTest
            {
               [self callGetLoginDetailsFromDB];
                return;
            }
            #endif
            {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:@"Do you want to proceed anyway?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
            alert.tag = 100;
            [alert show];
            }
        }
    }
}


#pragma mark - Text Field Delgates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    scView.frame=CGRectMake(scView.frame.origin.x, scView.frame.origin.y-80, scView.frame.size.width, scView.frame.size.height );

//    if (textField == txtPassword) {
//        scView.frame=CGRectMake(scView.frame.origin.x, scView.frame.origin.y-80, scView.frame.size.width, scView.frame.size.height );
//    }
//    else{
//        scView.frame=CGRectMake(scView.frame.origin.x, scView.frame.origin.y-upframeByYPosition, scView.frame.size.width, scView.frame.size.height );
//        
//    }
    //    else{
    //        scView.frame=CGRectMake(scView.frame.origin.x, scView.frame.origin.y-upframeByYPosition, scView.frame.size.width, scView.frame.size.height );
    //    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //     scView.frame=CGRectMake(scView.frame.origin.x, scView.frame.origin.y+upframeByYPosition, scView.frame.size.width, scView.frame.size.height );
    scView.frame=CGRectMake(scView.frame.origin.x, 0, scView.frame.size.width, scView.frame.size.height );
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    scView.frame=CGRectMake(scView.frame.origin.x,0, scView.frame.size.width, scView.frame.size.height );
    return YES;
}

#pragma mark - Custom Methods
-(void)enableView{
    txtUsername.enabled = YES;
    txtPassword.enabled = YES;
    btnLogin.enabled = YES;
}
-(void)disableView{
    txtUsername.enabled = NO;
    txtPassword.enabled = NO;
    btnLogin.enabled = NO;
}

#pragma mark - Database calls
-(void)callGetLoginDetailsFromDB
{
    dispatch_async(backgroundQueueForLogin, ^(void) {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            
            @try {
                
                NSString *usernameStr =txtUsername.text;
                FMResultSet *rs = [db executeQuery:@"SELECT * FROM Members WHERE Username = ? AND Password = ?",[usernameStr uppercaseString],[txtPassword.text MD5]];
                
                
                
                if (!rs) {
                    [rs close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs next]){
                    //[spinner stopAnimating];
                    [self enableView];
                    
                    NSDictionary *dictResult = [rs resultDictionary];
                    
                    //Set the sales type with prefix R
                    NSString *strSalesType = [NSString stringWithFormat:@"R%@",[dictResult objectForKey:@"Username"]];
                    
                    NSString *strUserName = [dictResult objectForKey:@"Username"];
                    
                    NSString *strMembers= [dictResult objectForKey:@"members"];
                    NSString *strWarehouse;
                    
                 // Set warehouse for BLayney from Syslogon in case of Sales
                    if ([strMembers isEqualToString:ADMIN] || [strMembers isEqualToString:TELE_SALES] ) {
                        strWarehouse = [dictResult objectForKey:@"warehouse"];
                    }
                    else{
                        FMResultSet *rs11 = [db executeQuery:@"SELECT * FROM syslogon WHERE LOGNAME = ?",[usernameStr uppercaseString]];
                        if (!rs11) {
                            [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        if ([rs11 next]){
                            NSDictionary *dictResultForSales = [rs11 resultDictionary];
                            strWarehouse = [dictResultForSales objectForKey:@"DEFAULT_WH"];
                        }

                        [rs11 close];
                    }
                    
                    // Set warehouse values if 0 -> 01
                    if (strWarehouse.length < 2) {
                        strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
                    }
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setValue:strSalesType forKey:@"salesType"];
                    [prefs setValue:strUserName forKey:@"userName"];
                    [prefs setValue:self.txtPassword.text forKey:@"password"];
                    [prefs setValue:strWarehouse forKey:@"warehouse"];
                    [prefs setValue:strMembers forKey:@"members"];
                    [prefs synchronize];

                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [spinner stopAnimating];
                        [self dismissVerticalPressed];
                        
                    });
                    
                }
                else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [spinner stopAnimating];
                        [self enableView];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Access Denied" message:@"You are not authorized user!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [alert show];
                    });
                                   
                }
                
                [rs close];
            }
            @catch (NSException* e) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [AJNotificationView showNoticeInView:self.view
                                                    type:AJNotificationTypeRed
                                                   title:strErrorMessage
                                         linedBackground:AJLinedBackgroundTypeDisabled
                                               hideAfter:2.5f];
                });
            }
        }];
    });
    
}


#pragma mark - WS calls
-(void)callWSForLogin
{
    strWebserviceType = @"WSLOGIN";
    NSString *parameters = [NSString stringWithFormat:@"username=%@&password=%@&format='xml'",[txtUsername.text uppercaseString],txtPassword.text];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,LOGINWS,parameters];
    
    DLog(@"Login Url : %@",strURL);
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebserviceWithoutSpinner:strURL ];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    DLog(@"%@", dictResponse);
    
    [spinner stopAnimating];
    spinner.hidden = TRUE;
    
    [self enableView];
    if ([strWebserviceType isEqualToString:@"WSLOGIN"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            //Set the sales type with prefix R
            NSString *strSaleType = [NSString stringWithFormat:@"R%@",[[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Data"] objectForKey:@"Username"] objectForKey:@"text"]];
            
            NSString *strMembers = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Data"] objectForKey:@"members"] objectForKey:@"text"];
            
            NSString *strUserName = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Data"] objectForKey:@"Username"] objectForKey:@"text"];
            
            NSString *strWarehouse = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Data"] objectForKey:@"warehouse"] objectForKey:@"text"];
            
            NSString *struser_schedule = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Data"] objectForKey:@"user_schedule"] objectForKey:@"text"];
            
            
            // Set warehouse values if 0 -> 01
            if (strWarehouse.length < 2) {
                strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
            }
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            [prefs setValue:strUserName forKey:@"userName"];
             [prefs setValue:self.txtPassword.text forKey:@"password"];
            [prefs setValue:strSaleType forKey:@"salesType"];
            [prefs setValue:strMembers forKey:@"members"];
            [prefs setValue:strWarehouse forKey:@"warehouse"];
            
            [prefs setValue:struser_schedule?struser_schedule:@"" forKey:@"user_schedule"];
            [prefs setObject:@"00" forKey:@"BRANCH"];
            
            [prefs synchronize];
            
            [self dismissVerticalPressed];
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Login"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    [spinner stopAnimating];
    spinner.hidden = TRUE;
    [self enableView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:@"Do you want to proceed anyway?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alert.tag = 100;
    [alert show];
    //strErrorMessage = nil;
}


#pragma mark - Curtain view Method
- (void)dismissVerticalPressed
{
    RootViewController *root = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:[NSBundle mainBundle]];
    [AppDelegate getAppDelegateObj].rootViewController = root;
    [AppDelegate getAppDelegateObj].window.rootViewController = root;
    [[AppDelegate getAppDelegateObj].window makeKeyAndVisible];
    //[[SDSyncEngine sharedEngine] startSync];
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        
        if (buttonIndex == alertView.cancelButtonIndex) {
            [spinner stopAnimating];
            [self enableView];
        }
        else{
            [self callGetLoginDetailsFromDB];
        }
    }
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
    }
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
