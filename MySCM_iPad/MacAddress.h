

#import <Foundation/Foundation.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@interface MacAddress : NSObject
{
}
- (NSString *)getMacAddress;

@end
