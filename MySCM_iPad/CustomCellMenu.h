//
//  CustomCellMenu.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 06/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SubMenu;

@interface CustomCellMenu : UITableViewCell{

}

@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnDisclosure;
@property (weak, nonatomic) IBOutlet UIButton *btnSync;
@property (nonatomic, strong) SubMenu *subMenu;
@property (nonatomic, retain) IBOutlet HJManagedImageV *imgBackgroundView;
@end
