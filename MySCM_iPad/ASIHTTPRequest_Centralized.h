

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"


@protocol ASIHTTPRequest_delegate <NSObject>
//-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj;
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
-(void)ASIHTTPRequest_Error:(id)error;
@end


@interface ASIHTTPRequest_Centralized : NSObject<ASIHTTPRequestDelegate>
{
    id <ASIHTTPRequest_delegate> delegate;
    ASIHTTPRequest *request;
    MBProgressHUD *spinner;
    
    long long expectedLength;
	long long currentLength;
    BOOL isWithoutSpinner;
}

@property(nonatomic,assign) id <ASIHTTPRequest_delegate> delegate;
@property(nonatomic,retain)  MBProgressHUD *spinner;
@property(nonatomic,assign)  BOOL isWithoutSpinner;

-(void)CallingWebserviceWithoutSpinner:(NSString *)strURL;
-(void)CallingWebservice:(NSString *)strURL onView:(UIView *)onView;
-(void)CallingWebservicePost:(NSString *)soapXMLStr soapAction:(NSString*) strSoapAction onView:(UIView *)onView;

-(void)cancelRequest;
-(ASIHTTPRequest *)getCurrentRequest;
@end
