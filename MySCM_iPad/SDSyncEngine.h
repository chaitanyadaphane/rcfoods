//
//  SDSyncEngine.h
//  SignificantDates
//
//  Created by Chris Wagner on 7/1/12.
//

#import <Foundation/Foundation.h>

typedef enum {
    SDObjectSynced = 0,
} SDObjectSyncStatus;

@interface SDSyncEngine : NSObject{
    NSOperationQueue *downloadOperationQueue;
    int cntWebserviceNumber;
    
    AJNotificationView *panel;
    
    BOOL isQueueCancelled;
    
    int numDatabaseDumped;
    UIWindow *topWindow;
    int queueSize;
}

@property (atomic, readonly) BOOL syncInProgress;
@property (atomic, readonly) NSOperationQueue *downloadOperationQueue;
@property (nonatomic,retain)AJNotificationView *panel;
@property (assign)BOOL isQueueCancelled;
@property (assign) int numDatabaseDumped;

+ (SDSyncEngine *)sharedEngine;

- (void)registerNSManagedObjectClassToSync:(NSString *)tableName;
- (void)startSync;
- (NSString *)mostRecentUpdatedAtDateForEntityWithName:(NSString *)entityName;
- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString;
- (NSString *)dateStringForAPIUsingDate:(NSDate *)date;
- (NSString *)dateStringForAPIUsingDateXml:(NSDate *)date;
- (NSDate *)dateUsingStringFromAPIXml:(NSString *)dateString;

- (BOOL)setUpdatedLastDateForEntityWithName:(NSString *)entityName NewSyncDate:(NSString *)newSyncDate Database:(FMDatabase *)db;
-(void)DeleteAndStartSync;
@end
