//
//  CustomCellGeneral.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 11/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "CustomCellGeneral.h"
#import "MJShowPhotoViewController.h"
#import "KGModal.h"

@implementation CustomCellGeneral
@synthesize imgViewNoticeBg;
@synthesize lblDate,lblMessage,btnMessage,lblCreatedBy,btnReadUnread,btnAttachment,imgViewCellBackground,btnSelectsalesperson;
@synthesize lblStockCode,lblProdDesc,lblProductGroup,lblWarehouse,lblLocation,lblPrice,lblDateAdded;
@synthesize lblCustomerName,lblQuoteNo;
@synthesize lblTitle,txtValue;
@synthesize lblValue;
@synthesize btnDetailView;
@synthesize txtGlowingValue;
@synthesize btnQuoteStatus,lblCashAmount,imgBackgroundView;
@synthesize imgProduct;
@synthesize imagesArr;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
            }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


- (void)layoutSubviews {
    [super layoutSubviews];
        
    if ([self.reuseIdentifier isEqualToString:CELL_IDENTIFIER3]||[self.reuseIdentifier isEqualToString:CELL_IDENTIFIER6]||[self.reuseIdentifier isEqualToString:CELL_IDENTIFIER7]||[self.reuseIdentifier isEqualToString:CELL_IDENTIFIER8]||[self.reuseIdentifier isEqualToString:CELL_IDENTIFIER9]||[self.reuseIdentifier isEqualToString:CELL_IDENTIFIER11]) {
        self.backgroundColor = [UIColor clearColor];
        [self.imgBackgroundView clear];
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"menu_bar" ofType:@"png"];
        self.imgBackgroundView.image = [UIImage imageWithContentsOfFile:strImage];
        strImage = nil;
        [[AppDelegate getAppDelegateObj].obj_Manager manage:self.imgBackgroundView];
    }
    else{
        
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        if (self.editing) {
            [self sendSubviewToBack:self.contentView];
        }
    }
    
}


@end
