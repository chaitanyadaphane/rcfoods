

#import "ASIHTTPRequest_CentralizedHelper.h"

@implementation ASIHTTPRequest_CentralizedHelper

static ASIHTTPRequest_CentralizedHelper * _sharedHelper;

+ (ASIHTTPRequest_CentralizedHelper *) sharedHelper
{
    if (_sharedHelper != nil) 
	{
        return _sharedHelper;
    }
    _sharedHelper = [[ASIHTTPRequest_CentralizedHelper alloc] init];
    return _sharedHelper;
}

- (id)init 
{
    if ((self = [super init]))
	{                
    }
    return self;
}

@end
