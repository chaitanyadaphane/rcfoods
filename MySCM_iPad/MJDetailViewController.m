//
//  MJDetailViewController.m
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import "MJDetailViewController.h"

@implementation MJDetailViewController
@synthesize txtView,strMessage;

-(void)viewDidLoad{
    [super viewDidLoad];
    
    txtView.text = strMessage;
}

-(void)viewDidUnload{
    [super viewDidUnload];
    self.txtView = nil;
}

@end
