//
//  LoginViewController.h
//  MySCM
//
//  Created by Ashutosh Dingankar on 03/12/12.
//  Copyright (c) 2012 Ashutosh Dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,UIAlertViewDelegate>{
    
    NSString *strWebserviceType;
    
    FMDatabaseQueue *dbQueueLogin;
    BOOL isNetworkConnected;
    dispatch_queue_t backgroundQueueForLogin;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBackGround;

@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic,unsafe_unretained) IBOutlet UITextField *txtUsername;
@property (nonatomic,unsafe_unretained) IBOutlet UITextField *txtPassword; 
@property (nonatomic,unsafe_unretained) IBOutlet IBOutlet UIButton *btnLogin;
@property (nonatomic,unsafe_unretained) IBOutlet UIScrollView *scView;

#pragma mark - IBActions
-(IBAction)actionLoginClicked:(id)sender;

-(void)callGetLoginDetailsFromDB;
@end
