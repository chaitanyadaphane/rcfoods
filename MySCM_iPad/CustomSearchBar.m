//
//  CustomSearchBar.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/08/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CustomSearchBar.h"

@implementation CustomSearchBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews {
    
    UITextField *searchField;
    NSUInteger numViews = [self.subviews count];
    for(int i = 0; i < numViews; i++) {
        if([[self.subviews objectAtIndex:i] isKindOfClass:[UITextField class]]) { //conform?
            searchField = [self.subviews objectAtIndex:i];
        }
    }
    if(!(searchField == nil)) {
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadResults)];
        singleTap.numberOfTapsRequired = 1;
        searchField.leftView.userInteractionEnabled = YES;
        [searchField.leftView addGestureRecognizer:singleTap];
    }
    
    [super layoutSubviews];
}

-(void)loadResults{
    [self.delegate searchBarSearchButtonClicked:self];
}

@end
