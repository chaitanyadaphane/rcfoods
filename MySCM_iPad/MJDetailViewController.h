//
//  MJDetailViewController.h
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDetailViewController : UIViewController{
    
}
@property (nonatomic,retain) NSString *strMessage;

@property (nonatomic,unsafe_unretained) IBOutlet UITextView *txtView;
@end

