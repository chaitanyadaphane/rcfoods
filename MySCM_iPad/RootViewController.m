/*
 This module is licenced under the BSD license.
 
 Copyright (C) 2011 by raw engineering <nikhil.jain (at) raweng (dot) com, reefaq.mohammed (at) raweng (dot) com>.
 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
//  RootView.m
//  StackScrollView
//
//  Created by Reefaq on 2/24/11.
//  Copyright 2011 raw engineering . All rights reserved.
//

#import "RootViewController.h"
#import "MenuViewController.h"
#import "DashboardViewController.h"

@interface UIViewExt : UIView {} 
@end


@implementation UIViewExt
- (UIView *) hitTest: (CGPoint) pt withEvent: (UIEvent *) event 
{   
	
	UIView* viewToReturn=nil;
	CGPoint pointToReturn;
	
	UIView* uiRightView = (UIView*)[[self subviews] objectAtIndex:1];
	
	if ([[uiRightView subviews] objectAtIndex:0]) {
		
		UIView* uiStackScrollView = [[uiRightView subviews] objectAtIndex:0];	
		
		if ([[uiStackScrollView subviews] objectAtIndex:1]) {	 
			
			UIView* uiSlideView = [[uiStackScrollView subviews] objectAtIndex:1];	
			
			for (UIView* subView in [uiSlideView subviews]) {
				CGPoint point  = [subView convertPoint:pt fromView:self];
				if ([subView pointInside:point withEvent:event]) {
					viewToReturn = subView;
					pointToReturn = point;
				}
				
			}
		}
		
	}
	
	if(viewToReturn != nil) {
		return [viewToReturn hitTest:pointToReturn withEvent:event];		
	}
	return [super hitTest:pt withEvent:event];
}

@end

@implementation RootViewController
@synthesize menuViewController, stackScrollViewController,statusView,lblStatusText,isNetworkConnected,timerForMessages,lblLoggedInText;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
                        
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
                
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)setBattery:(NSString *)batteryLevel{
    batterLevel = [batteryLevel floatValue];

    UIImageView    *battery = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, _batteryImage.frame.size.width - 5,_batteryImage.frame.size.height )];
//        battery.image = battery;
    batteryLevelIndicator = [[UIView alloc] initWithFrame:CGRectMake(1, 1 , (battery.frame.size.width  * batterLevel)/100, _batteryImage.frame.size.height-2)];
    batteryLevelIndicator.backgroundColor = [UIColor greenColor];
    batteryLevelIndicator.layer.cornerRadius = 3;
    batteryLevelIndicator.layer.masksToBounds = YES;
    battery.contentMode = UIViewContentModeScaleAspectFill;
    battery.backgroundColor = [UIColor clearColor];
    battery.clipsToBounds = YES;
    
    [battery addSubview:batteryLevelIndicator];
    [_batteryImage addSubview:battery];
}



-(void)SetTimer
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy, hh:mm:ss a"];
    NSString *todaysDate = [dateFormatter stringFromDate: currentTime];
    [_lblDateandTime setText:todaysDate];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];

}

-(void)updateTime
{
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
        UIDevice *myDevice = [UIDevice currentDevice];
        [myDevice setBatteryMonitoringEnabled:YES];
        double batLeft = (float)[myDevice batteryLevel] * 100;
        NSString * levelLabel = [NSString stringWithFormat:@"%.f%%", batLeft];
        _lblBattery.text = levelLabel;
        
        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMMM yyyy, hh:mm:ss a"];
//        dd mmm yyyy hh:mm:ss a   dd-MM-yyyy
        NSString *todaysDate = [dateFormatter stringFromDate: currentTime];
        [_lblDateandTime setText:todaysDate];
        
        
        
        
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"%.f",batLeft);
    NSString * levelLabel = [NSString stringWithFormat:@"%.f%%", batLeft];
    _lblBattery.text = levelLabel;
    
    [self setBattery:levelLabel];
    [self SetTimer];
    
    self.timerForMessages = [NSTimer scheduledTimerWithTimeInterval:60
                                                             target:self
                                                           selector:@selector(animateFunction)
                                                           userInfo:nil
                                                            repeats:YES];
    
    CGSize viewSize = [AppDelegate getRotatedViewSize:self.view];
    self.view.frame = CGRectMake(0, 0, viewSize.width, viewSize.height);
    
    
    rootView = [[UIViewExt alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height-20)];
    rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth + UIViewAutoresizingFlexibleHeight;
    [rootView setBackgroundColor:[UIColor clearColor]];
    
    leftMenuView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LEFTMENU_WIDTH, self.view.frame.size.height)];
    leftMenuView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    menuViewController = [[MenuViewController alloc] initWithFrame:CGRectMake(0, 0, leftMenuView.frame.size.width, leftMenuView.frame.size.height)];
    [menuViewController.view setBackgroundColor:[UIColor clearColor]];
    [menuViewController viewWillAppear:FALSE];
    [menuViewController viewDidAppear:FALSE];
    [leftMenuView addSubview:menuViewController.view];
    
    rightSlideView = [[UIView alloc] initWithFrame:CGRectMake(leftMenuView.frame.size.width, 0, rootView.frame.size.width - leftMenuView.frame.size.width, rootView.frame.size.height)];
    rightSlideView.autoresizingMask = UIViewAutoresizingFlexibleWidth + UIViewAutoresizingFlexibleHeight;
    stackScrollViewController = [[StackScrollViewController alloc] init];
    [stackScrollViewController.view setFrame:CGRectMake(0, 0, rightSlideView.frame.size.width, rightSlideView.frame.size.height)];
    [stackScrollViewController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth + UIViewAutoresizingFlexibleHeight];
    [stackScrollViewController viewWillAppear:FALSE];
    [stackScrollViewController viewDidAppear:FALSE];
    [rightSlideView addSubview:stackScrollViewController.view];
    
    [rootView addSubview:leftMenuView];
    [rootView addSubview:rightSlideView];
    
    [self.view addSubview:statusView];
    [self.view addSubview:rootView];
    
    //Display logged in Username on the top
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserName = [prefs objectForKey:@"userName"];
    lblLoggedInText.text = [NSString stringWithFormat:@"Welcome, %@",strUserName];
    
    [self.lblStatusText.layer addAnimation:[[AppDelegate getAppDelegateObj] fadeInFadeOutAnimationForStatusBar]
                                    forKey:@"animateOpacity"];
    
    //Check Network Connectivity
    Reachability *serverReach = [Reachability reachabilityWithHostName:HOST_NAME];
    
    NetworkStatus netStatus = [serverReach currentReachabilityStatus];
    if (netStatus==ReachableViaWiFi) {
        self.isNetworkConnected = YES;
        self.lblStatusText.text = SERVER_ONLINE_MESSAGE;
    } else if(netStatus==ReachableViaWWAN) {
        self.isNetworkConnected = YES;
        self.lblStatusText.text = SERVER_ONLINE_MESSAGE;
    } else {
        self.isNetworkConnected = NO;
        self.lblStatusText.text = SERVER_OFFLINE_MESSAGE;
    }
    
    
    //Show Dashboard by default
    DashboardViewController *dataViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:[NSBundle mainBundle]];
    [stackScrollViewController addViewInSlider:dataViewController invokeByController:menuViewController isStackStartView:TRUE];
    [stackScrollViewController.view removeAllGestures];
    [dataViewController release];
    
    
}

-(void)animateFunction{

    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs1 = [db executeQuery:@"SELECT last_sync_date FROM tbl_last_application_sync"];
        
        while ([rs1 next]) {
            NSDictionary *dict = [rs1 resultDictionary];
            NSLog(@"%@",[dict objectForKey:@"last_sync_date"]);
            
            NSString *strMessage = @"";
            if ([[dict objectForKey:@"last_sync_date"] isEqualToString:@""]) {
                strMessage = [NSString stringWithFormat:@"%@",APP_REQUIRES_SYNC];
            }
            else{
                strMessage = [NSString stringWithFormat:@"%@ %@",LAST_UPDATED_ON,[dict objectForKey:@"last_sync_date"]];
            }
            
            
            self.lblStatusText.text = strMessage;
        }
        
        [rs1 close];

    }];
      
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[menuViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	[stackScrollViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[menuViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
	[stackScrollViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}	
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [super dealloc];
}

-(IBAction)actionLogout:(id)sender{
    
    if ([SDSyncEngine sharedEngine].syncInProgress){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sync in progress! Please wait..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Do you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];
        
    }
}


#pragma mark - Custom Methods
- (void)setInternetStatus{
    if (isNetworkConnected) {
        lblStatusText.text = SERVER_ONLINE_MESSAGE;
    }
    else{
        lblStatusText.text = SERVER_OFFLINE_MESSAGE;
    }
}


#pragma mark - Alert Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        
        [rootView removeFromSuperview];
        [menuViewController release];
        [stackScrollViewController release];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSDictionary *userDefaultsDictionary = [userDefaults dictionaryRepresentation];
        NSString *strWebDatabaseDirectory = [userDefaultsDictionary objectForKey:@"WebDatabaseDirectory"];
        NSString *strWebKitLocalStorageDatabasePathPreferenceKey = [userDefaultsDictionary objectForKey:@"WebKitLocalStorageDatabasePathPreferenceKey"];
        
          //Remove all persistent storage
        
        [userDefaults removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
        
        if (strWebDatabaseDirectory) {
            [userDefaults setObject:strWebDatabaseDirectory forKey:@"WebDatabaseDirectory"];}
        if (strWebKitLocalStorageDatabasePathPreferenceKey) {
            [userDefaults setObject:strWebKitLocalStorageDatabasePathPreferenceKey forKey:@"WebKitLocalStorageDatabasePathPreferenceKey"];}
        
        NSString *strFirstTime = @"FirstTime";
        
        [userDefaults setValue:strFirstTime forKey:@"FirstTime"];
        
        [userDefaults synchronize];
        
    
        if ([[[SDSyncEngine sharedEngine].downloadOperationQueue operations] count]) {
            [[SDSyncEngine sharedEngine].downloadOperationQueue cancelAllOperations];
        }
        
        if ([[ASIHTTPRequest_CentralizedHelper sharedHelper] getCurrentRequest]) {
            [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
        }
        
        //Stop timer
        [self.timerForMessages invalidate];
        timerForMessages = nil;
        
        [AppDelegate getAppDelegateObj].window.rootViewController = (UIViewController *)([AppDelegate getAppDelegateObj].loginViewController);

    }
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            self.isNetworkConnected = NO;            
            break;
        }
        case ReachableViaWiFi:
        {
            self.isNetworkConnected = YES;            
            break;
        }
        case ReachableViaWWAN:
        {
            self.isNetworkConnected = YES;
            break;
        }
    }
    
    [self setInternetStatus];
    
}

@end
