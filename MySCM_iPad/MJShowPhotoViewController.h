//
//  MJShowPhotoViewController.h
//  Blayney
//
//  Created by Pooja on 05/01/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJShowPhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,strong)NSMutableArray *imageDataArr;

@end
