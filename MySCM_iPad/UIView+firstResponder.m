

#import "UIView+firstResponder.h"

@implementation UIView (firstResponder)

- (UIView *)findFirstResponder
{
	if ([self isFirstResponder]) {
		return self;
	}
	
	for (UIView *subview in [self subviews]) {
		UIView *firstResponder = [subview findFirstResponder];
        
		if (nil != firstResponder) {
            if ([firstResponder isKindOfClass:[UITextField class]]) {
                NSLog(@"text field tag :%d",firstResponder.tag);
            }
            return firstResponder;
		}
	}
	
	return nil;
}


@end
