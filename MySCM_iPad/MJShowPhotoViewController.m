//
//  MJShowPhotoViewController.m
//  Blayney
//
//  Created by Pooja on 05/01/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import "MJShowPhotoViewController.h"
#import "PhotoCell.h"

@interface MJShowPhotoViewController ()

@end

@implementation MJShowPhotoViewController
@synthesize imageDataArr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Upload Photo


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageDataArr count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"PhotoCell";
    
    [collectionView1 registerNib:[UINib nibWithNibName:@"PhotoCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:identifier];
    
    PhotoCell *cell = [collectionView1 dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.layer setBorderColor:(__bridge CGColorRef)([UIColor blueColor])];
    [cell.layer setBorderWidth: 1.0];
    [cell.layer setCornerRadius:0.0];
    UIImageView *photo=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    
    UIImage *image = [UIImage imageWithData:[imageDataArr objectAtIndex:indexPath.row]];
    [photo setImage:image];

    [cell.contentView addSubview:photo];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView1 didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

}



@end
