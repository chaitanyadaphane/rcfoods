 /*
 This module is licenced under the BSD license.
 
 Copyright (C) 2011 by raw engineering <nikhil.jain (at) raweng (dot) com, reefaq.mohammed (at) raweng (dot) com>.
 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
//  MenuViewController.m
//  StackScrollView
//
//  Created by Reefaq on 2/24/11.
//  Copyright 2011 raw engineering . All rights reserved.
//

#import "MenuViewController.h"
#import "SectionInfo.h"
#import "CustomCellMenu.h"
#import "Menu.h"
#import "SubMenu.h"
#import "DashboardViewController.h"
#import "ProfileOrderEntryViewController.h"
#import "OpenPdfWebViewController.h"

NSIndexPath *indexPathSelected;
@implementation MenuViewController
@synthesize tblMenu,btnDashBoard;
@synthesize arrMenu,sectionInfoArray,openSectionIndex,uniformRowHeight,playDictionariesArray;


#pragma mark -
#pragma mark View lifecycle

- (id)initWithFrame:(CGRect)frame {
    if (self = [super init]) {
		[self.view setFrame:frame];
        
        UIView* footerView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
		tblMenu.tableFooterView = footerView;
        [footerView release];
        
		//[self.view addSubview:_tableView];
		
		UIView* verticalLineView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, -5, 1, self.view.frame.size.height)];
		[verticalLineView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
		[verticalLineView setBackgroundColor:[UIColor clearColor]];
		[self.view addSubview:verticalLineView];
		[self.view bringSubviewToFront:verticalLineView];
        [verticalLineView release];
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        		
	}
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.tblMenu.sectionHeaderHeight = HEADER_HEIGHT;
	
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dash_bg_hover" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    //Set dashboard button as selected
    btnDashBoard.imageView.image = img;
    
    /*
     The section info array is thrown away in viewWillUnload, so it's OK to set the default values here. If you keep the section information etc. then set the default values in the designated initializer.
     */
    uniformRowHeight = DEFAULT_ROW_HEIGHT;
    openSectionIndex = NSNotFound;
    
    [self setUpMenuArray];
    
    //NSLog(@"Menu : %@",arrMenu);
    
    if ((self.sectionInfoArray == nil) || ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.tblMenu])) {
		
        // For each play, set up a corresponding SectionInfo object to contain the default height for each row.
		NSMutableArray *infoArray = [[NSMutableArray alloc] init];
		
		for (Menu *menu in arrMenu) {
			
			SectionInfo *sectionInfo = [[SectionInfo alloc] init];
			sectionInfo.menu = menu;
			sectionInfo.open = NO;
			
      NSNumber *defaultRowHeight = [NSNumber numberWithInteger:DEFAULT_ROW_HEIGHT];
			NSInteger countOfQuotations = [[sectionInfo.menu subMenus] count];
			for (NSInteger i = 0; i < countOfQuotations; i++) {
				[sectionInfo insertObject:defaultRowHeight inRowHeightsAtIndex:i];
			}
			
			[infoArray addObject:sectionInfo];
            [sectionInfo release];
            sectionInfo=nil;
		}
		
		self.sectionInfoArray = infoArray;
        
        [infoArray release];
        infoArray=nil;
        
        [self.tblMenu reloadData];
	}
    
    //NSLog(@"SectionInfo array : %@",self.sectionInfoArray);
    
    
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if([[AppDelegate getAppDelegateObj]CheckIfDemoApp])
    {
        self.imgLogo.image = [UIImage imageNamed:@"bly_logo@2x.png"];
    }
    
    
    // Profile and Sales Order
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isSaleOrderDetailCheck"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForSale"];
    
    // Quote Order
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isQuoteCreated"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForQuote"];

    [[NSUserDefaults standardUserDefaults]synchronize];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    // To reduce memory pressure, reset the section info array if the view is unloaded.
	self.sectionInfoArray = nil;
    self.tblMenu = nil;
    self.btnDashBoard = nil;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark Table view data source and delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return [self.arrMenu count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
	NSInteger numStoriesInSection = [[sectionInfo.menu subMenus] count];
    
    //NSLog(@"Integer : %d",numStoriesInSection);
    return sectionInfo.open ? numStoriesInSection : 0;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = @"menuCellIdentifier";
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellMenu *cell = (CustomCellMenu *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCellMenu" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    [cell.imgBackgroundView clear];
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"sub-menu-bg" ofType:@"png"];
    cell.imgBackgroundView.image = [UIImage imageWithContentsOfFile:strImage];
    strImage = nil;
    
    [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
    
    Menu *menu = (Menu *)[[self.sectionInfoArray objectAtIndex:indexPath.section] menu];
    
    if ((menu.menu_id == (NSNumber*)5)) {
        NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        menu.name = [NSString stringWithFormat:@"%@ : %@",menu.name,version];
    }
    cell.subMenu = [menu.subMenus objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}


-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    
    /*
     Create the section header views lazily.
     */
    
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    if (!sectionInfo.headerView) {
		NSString *playName = sectionInfo.menu.name;
        NSString *imageName = sectionInfo.menu.image;
        BOOL hasChild = ([sectionInfo.menu.subMenus count]) ? TRUE:FALSE;
        
        sectionInfo.headerView = [[SectionHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.tblMenu.bounds.size.width, HEADER_HEIGHT) title:playName imageName:imageName hasChild:hasChild section:section delegate:self];
        
        sectionInfo.headerView.btnHeader.tag = section;
        
        //NSString *strBadgeString = [AppDelegate getAppDelegateObj].strNumOfflineQuoteMessages;
    
        
        [sectionInfo.headerView.btnHeader addTarget:self action:@selector(actionShowMenuContent:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
        
    }
    
    return sectionInfo.headerView;
}

/*
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return [[sectionInfo objectInRowHeightsAtIndex:indexPath.row] floatValue];
    // Alternatively, return rowHeight.
}
*/

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
 
    
    
    NSString  *profileOrderQty = @"0";
    profileOrderQty = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForProfile"];
    
    
    NSString *saleOrderEntry = @"0";
    saleOrderEntry = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForSale"];
   
    NSString *QuoteOrderCreated = @"0";
    QuoteOrderCreated = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForQuote"];
    
    if ([profileOrderQty integerValue] > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this order" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1234;
        [alert show];
    }
    else if([saleOrderEntry integerValue] > 0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this order" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1235;
        [alert show];
    }
    else if ([QuoteOrderCreated integerValue] > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this quote" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1236;
        [alert show];
    }
    else{
        
        Menu *menu = (Menu *)[[self.sectionInfoArray objectAtIndex:indexPath.section] menu];
        
        CustomCellMenu *cell = (CustomCellMenu *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"sub-menu-hover-bg" ofType:@"png"];
        cell.imgBackgroundView.image = [UIImage imageWithContentsOfFile:strImage];
        strImage = nil;
        
        [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
        
        [AppDelegate getAppDelegateObj].intCurrentMenuId = [menu.menu_id intValue];
        [AppDelegate getAppDelegateObj].intCurrentSubMenuId = [cell.subMenu.submenu_id intValue];
        
        NSLog(@"%@",cell.subMenu.hasPage?@"YES":@"NO");
        
        if ([cell.subMenu.name isEqualToString:@"LOGOUT"]){
            [[AppDelegate getAppDelegateObj].rootViewController actionLogout:nil];
            
        }
        else if ([cell.subMenu.name isEqualToString:@"SYNC"]){
            
            if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SYNC_NOT_PERFORM_MESSAGE2 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            else{
                if ([SDSyncEngine sharedEngine].syncInProgress){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sync in already progress!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SYNC_ALERT_MESSAGE delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"SYNC",nil];
                    alert.tag = 1000;

                    [alert show];
                    
                }
            }
            
        }
        
        else if ([cell.subMenu.name isEqualToString:@"FLUSH & SYNC"]){
            
            if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SYNC_NOT_PERFORM_MESSAGE2 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];

                [alert show];
            }
            else{
                if ([SDSyncEngine sharedEngine].syncInProgress){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sync in already progress!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SYNC_Delete_ALERT_MESSAGE delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"SYNC",nil];
                    alert.tag = 100;
                    [alert show];
                }
            }
        }
        else if ([cell.subMenu.hasPage isEqualToString:@"YES"])
        {
            if (NSClassFromString(cell.subMenu.classToRedirect) != nil)
            {
                //Add slide gesture
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addSlideGesture];
                
                Class rootController = NSClassFromString(cell.subMenu.classToRedirect);
                id dataViewController = [[rootController alloc] initWithNibName:cell.subMenu.classToRedirect bundle:[NSBundle mainBundle]];
                
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:[AppDelegate getAppDelegateObj].rootViewController.menuViewController  isStackStartView:TRUE];
                
                [dataViewController release];
            }
            
            else if([cell.subMenu.name isEqualToString:@"Specials"])      {
                //Open Special PDF.
                [self downloadPDFContent];
                
            }
            else{
                
                NSLog(@"We are testing issue for iOS version 8.1.3  #NSClassFromString(cell.subMenu.classToRedirect) is RETURNING");
            }
        }
        else{
            
            NSLog(@"We are testing issue for iOS version 8.1.3 haspage is NO");
        }
        
        [self.tblMenu reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEFAULT_ROW_HEIGHT;
}

#pragma mark Section header delegate

-(void)sectionHeaderView:(SectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"menu-hover-bg" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    sectionHeaderView.btnHeader.imageView.image = img;
    
    [img release];
    img = nil;
    
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:sectionOpened];
	
	sectionInfo.open = YES;
   
    NSInteger countOfRowsToInsert = [sectionInfo.menu.subMenus count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
		
		SectionInfo *previousOpenSection = [self.sectionInfoArray objectAtIndex:previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"menu-bg" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        previousOpenSection.headerView.btnHeader.imageView.image = img;
        
        NSInteger countOfRowsToDelete = [previousOpenSection.menu.subMenus count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
            
        }
    }
    
    // Style the animation so that there's a smooth flow in either direction.
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // Apply the updates.
    [self.tblMenu beginUpdates];
    [self.tblMenu insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tblMenu deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tblMenu endUpdates];
    self.openSectionIndex = sectionOpened;
    
    [indexPathsToInsert release];
    indexPathsToInsert=nil;
    [indexPathsToDelete release];
    indexPathsToDelete = nil;
    
}


-(void)sectionHeaderView:(SectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"menu-bg" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    sectionHeaderView.btnHeader.imageView.image = img;
    
    [img release];
    img=nil;
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:sectionClosed];
	
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tblMenu numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        
        //sectionInfo.headerView.btnHeader.imageView.image = [UIImage imageNamed:@"menu_bar.png"];
        
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        
        
        [self.tblMenu deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
        
        [indexPathsToDelete release];
        indexPathsToDelete = nil;
        
        
    }
    self.openSectionIndex = NSNotFound;
}


#pragma mark - Download PDF

-(void)downloadPDFContent
{
    
    
   dispatch_queue_t backgroundQueueForSpecials = dispatch_queue_create("com.nanan.myscmipad.bgqueueForSpecials", NULL);
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        NSString *strSpecialsURL = [NSString stringWithFormat:@"%@%@%@",WSURL,SPECIALS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
        NSURL *url = [NSURL URLWithString:strSpecialsURL];
        __block  ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];//__weak
        [request setCompletionBlock:^{
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
          
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    dispatch_async(backgroundQueueForSpecials, ^(void){
                        
                        NSMutableArray *arrMembers = [[NSMutableArray alloc] init];
                        
                        if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                            [arrMembers addObject:dict];
                        }
                        else{
                            arrMembers = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                        }
                        
                        __block NSString *strSpecialsPdfUrl = Nil ;
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            @try
                            {
                                [db executeUpdate:@"DELETE FROM specials"];
                                
                                for (NSDictionary *dict in arrMembers){
                                    
                                    
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `specials` (`SCM_RECNUM`, `title`, `pdffile`, `pdfthumbnail`, `published`, `created_date`, `modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?)",
                                              [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                                              [[dict objectForKey:@"title"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdffile"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdfthumbnail"] objectForKey:@"text"],
                                              [[dict objectForKey:@"published"] objectForKey:@"text"],
                                              [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                              [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                    
                                    strSpecialsPdfUrl =[NSString stringWithFormat:@"%@",[[dict objectForKey:@"pdffile"] objectForKey:@"text"]];;
                                    
                                    if (![strSpecialsPdfUrl isEqualToString:NO_ATTACHMENT_FOUND])
                                    {
                                        NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strSpecialsPdfUrl];
                                    
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strPdfUrl];
                                    }
                                }
                                
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"specials" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                            }
                            @catch(NSException* e)
                            {
                                NSLog(@"Error : specials: %@",[e description]);
                            }
                            
                            NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,(NSString *)strSpecialsPdfUrl];
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                NSLog(@"DOWNLOADED IMAGES & PDF'S");
                                
//                                ![strSpecialsPdfUrl isEqualToString:@""] ||
                                if (strAttachmentUrl != Nil) {
                                    //file exist
                                    
//                                    NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,(NSString *)strSpecialsPdfUrl];
                                    
                                    OpenPdfWebViewController *dataViewController = [[OpenPdfWebViewController alloc] initWithNibName:@"OpenPdfWebViewController" bundle:[NSBundle mainBundle]];
                                    
                                    dataViewController.webviewLink = strAttachmentUrl;
                                    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:dataViewController];
                                    nvc.navigationBarHidden = YES;
                                    dataViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                                    
                                    [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
                                }
                                else{
                                    //file does not exist
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attachment not available!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                    
                                    [alert show];
                                }
                            });
                            
                        }];
                        
                        databaseQueue=nil;
                    });
                    
                    
                    
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    NSLog(@"Error : specials: %@",strErrorMessage);
                    
                }
                
            }
            else{
                dispatch_async(backgroundQueueForSpecials, ^(void){
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    
                    [databaseQueue inDatabase:^(FMDatabase *db) {
                        [db executeUpdate:@"DELETE FROM specials"];
                    }];
                    
                    databaseQueue = nil;
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                       
                       
                    });
                });
                
            }
        }];
        [request setFailedBlock:^{
            // NSError *error = [request error];
        }];
        [request startAsynchronous];
        
    }
    else
    {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        FMResultSet *rs3 = [db executeQuery:@"SELECT * FROM specials WHERE published = 1"];
        
        if (!rs3) {
            NSLog(@"Error %@",[db lastError]);
        }
        else{
            
            NSMutableDictionary *dictSpecials = [NSMutableDictionary new];
            NSString *strSpecialsPdfUrl;
            if ([rs3 next])
            {
                NSDictionary *dictData = [rs3 resultDictionary];
                
                [dictSpecials setValue:[dictData objectForKey:@"SCM_RECNUM"] forKey:@"ScmRecnum"];
                [dictSpecials setValue:[dictData objectForKey:@"created_date"] forKey:@"CreatedDate"];
                [dictSpecials setValue:[dictData objectForKey:@"modified_date"] forKey:@"ModifiedDate"];
                [dictSpecials setValue:[dictData objectForKey:@"published"] forKey:@"published"];
                [dictSpecials setValue:[dictData objectForKey:@"pdfthumbnail"] forKey:@"pdfthumbnail"];
                [dictSpecials setValue:[dictData objectForKey:@"title"] forKey:@"title"];
                [dictSpecials setValue:[dictData objectForKey:@"pdffile"] forKey:@"pdffile"];
                
                strSpecialsPdfUrl = [dictData objectForKey:@"pdffile"];
                
                dictData=nil;
                
                if (![strSpecialsPdfUrl isEqualToString:@""]) {
                    //file exist
                    
                    NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strSpecialsPdfUrl];
                    
                    OpenPdfWebViewController *dataViewController = [[OpenPdfWebViewController alloc] initWithNibName:@"OpenPdfWebViewController" bundle:[NSBundle mainBundle]];
                    
                    dataViewController.webviewLink = strAttachmentUrl;
                    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:dataViewController];
                    nvc.navigationBarHidden = YES;
                    dataViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                    
                    [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
                }
                else{
                    //file does not exist
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attachment not available!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [alert show];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SERVER_OFFLINE_MESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
        
        [rs3 close];
    
    }
}

#pragma mark - Alert Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
    {
        if(buttonIndex == 1)
        {
            [[SDSyncEngine sharedEngine] DeleteAndStartSync];
        }
    }
    else if(alertView.tag == 1000)
    {
        if (buttonIndex == 1)
        {
            [[SDSyncEngine sharedEngine] startSync];
        }
    }
  
    else if(alertView.tag == 1234){
        if(buttonIndex == 1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SAVE" object:nil userInfo:nil];
        }
        else if (buttonIndex == 0){
            // Update the view is profile order.
                        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isProfileOrderDetailCheck"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForProfile"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dash_bg_hover" ofType:@"png"];
            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
            
            [btnDashBoard setImage:img forState:UIControlStateNormal];
            DashboardViewController *dataViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:[NSBundle mainBundle]];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:TRUE];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.view removeAllGestures];
            [dataViewController release];
        }
    }
    else if(alertView.tag == 1235){
        if(buttonIndex == 1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SAVE_SALES" object:nil userInfo:nil];
        }
        else if (buttonIndex == 0){
                        // Update the view is profile order.
                        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isSaleOrderDetailCheck"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForSale"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
            //
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dash_bg_hover" ofType:@"png"];
            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
            
            [btnDashBoard setImage:img forState:UIControlStateNormal];
            DashboardViewController *dataViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:[NSBundle mainBundle]];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:TRUE];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.view removeAllGestures];
            [dataViewController release];
        }
    }
    else if(alertView.tag == 1236){
        if(buttonIndex == 1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SAVE_QUOTE_CREATED" object:nil userInfo:nil];
        }
        else if (buttonIndex == 0){
            //            // Update the view is profile order.
            //            [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isSaleOrderDetailCheck"];
            //            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForSale"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            //
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dash_bg_hover" ofType:@"png"];
            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
            
            [btnDashBoard setImage:img forState:UIControlStateNormal];
            DashboardViewController *dataViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:[NSBundle mainBundle]];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:TRUE];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.view removeAllGestures];
            [dataViewController release];
        }
    }
    
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [tblMenu release];
    [arrMenu release];
    [sectionInfoArray release];
    [super dealloc];
}

//Set Up Menus & SubMenus
- (void)setUpMenuArray {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strMember = [prefs objectForKey:@"members"];
    
    NSURL *url = nil;
    if ([strMember isEqualToString:ADMIN]) {
        url = [[NSBundle mainBundle] URLForResource:@"MenusForAdmin" withExtension:@"plist"]; 
    }
    else if ([strMember isEqualToString:TELE_SALES]) {
        url = [[NSBundle mainBundle] URLForResource:@"MenusForTeleSales" withExtension:@"plist"];
    }
    else if ([strMember isEqualToString:SALES]) {
        url = [[NSBundle mainBundle] URLForResource:@"MenusForSales" withExtension:@"plist"];
    }
         
      
   self.playDictionariesArray = [NSArray arrayWithContentsOfURL:url];
   
    NSMutableArray *playsArray = [NSMutableArray arrayWithCapacity:[playDictionariesArray count]];
    
    //NSLog(@"playsArray - %@",playsArray);
    for (NSDictionary *playDictionary in playDictionariesArray) {
        
        Menu *play = [[Menu alloc] init];
        play.name = [playDictionary objectForKey:@"Name"];
        play.image = [playDictionary objectForKey:@"Image"];
        play.menu_id = [playDictionary objectForKey:@"Menu_Id"];
        NSString *str = [NSString stringWithFormat:@"%@",[playDictionary objectForKey:@"Menu_Id"]];
        
        if (([str isEqualToString:@"5"])) {
            NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
            play.name = [NSString stringWithFormat:@"%@ : %@",[playDictionary objectForKey:@"Name"],version];
            
        }
        
        NSArray *quotationDictionaries = [playDictionary objectForKey:@"SubMenu"];
        NSMutableArray *quotations = [NSMutableArray arrayWithCapacity:[quotationDictionaries count]];
        
        for (NSDictionary *quotationDictionary in quotationDictionaries) {
            
            SubMenu *subMenu = [[SubMenu alloc] init];
            
            subMenu.submenu_id = [quotationDictionary objectForKey:@"Submenu_Id"];
            subMenu.name = [quotationDictionary objectForKey:@"Name"];
            subMenu.image = [quotationDictionary objectForKey:@"Image"];
            subMenu.hasPage = [quotationDictionary objectForKey:@"HasPage"];//(BOOL)
            subMenu.classToRedirect = [quotationDictionary objectForKey:@"ClassToRedirect"];

            //[subMenu setValuesForKeysWithDictionary:quotationDictionary];
            
            [quotations addObject:subMenu];
            [subMenu release];
            subMenu=nil;
        }
        play.subMenus = quotations;
        
        [playsArray addObject:play];
        [play release];
        play=nil;
    }
    
    self.arrMenu = playsArray;
    
   // [playDictionariesArray release];
    //playDictionariesArray=nil;
}


#pragma mark - Show Dashboard
- (IBAction)actionShowDashboard:(id)sender
{

    NSString  *profileOrderQty = @"0";
    profileOrderQty = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForProfile"];
    
    NSString *saleOrderEntry = @"0";
    saleOrderEntry = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForSale"];
    
    NSString *QuoteOrderCreated = @"0";
    QuoteOrderCreated = [[NSUserDefaults standardUserDefaults]objectForKey:@"QuantityForQuote"];
    
    if ([profileOrderQty integerValue] > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this order" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1234;
        [alert show];
    }
    else if([saleOrderEntry integerValue] > 0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this order" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1235;
        [alert show];
    }
    else if ([QuoteOrderCreated integerValue] > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to save this quote" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1236;
        [alert show];
    }
    else
    {
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dash_bg_hover" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDashBoard setImage:img forState:UIControlStateNormal];
        DashboardViewController *dataViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:[NSBundle mainBundle]];
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:TRUE];
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.view removeAllGestures];
        [dataViewController release];
        
    }
}


#pragma mark - Display Menu Content
-(IBAction)actionShowMenuContent:(id)sender{
    
    UIButton *btnMenu = (UIButton *)sender;    
    SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:btnMenu.tag];    
    [sectionInfo.headerView toggleOpenWithUserAction:YES];    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end

