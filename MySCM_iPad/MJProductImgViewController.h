//
//  MJProductImgViewController.h
//  Blayney
//
//  Created by POOJA MISHRA on 20/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MJProductImgViewController : UIViewController
@property (weak, nonatomic) IBOutlet AsyncImageView *productImg;
@property(strong,nonatomic)NSString *prod_img_url;
@end
