//
//  AppDelegate.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 05/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "AppDelegate.h"
#import "StartStartViewController.h"
#import "LoginViewController.h"
#import "RootViewController.h"
#include <sys/xattr.h>


@implementation AppDelegate
@synthesize rootViewController;
@synthesize intCurrentMenuId,intCurrentSubMenuId,intEditQuoteProduct;
@synthesize database,databaseQueue,counterForStatusMessages,strLastAppSyncDate,strEmailIdOfLoggedInUser,strNumOfflineQuoteMessages,backgroundQueueForApplication,isNetworkConnected,isServerOffline,startViewController,obj_Manager,dateFormatter;
@synthesize PDFCreator;


#pragma mark - Initialization

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //------------ Push Notification ------------
    
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
//    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
//    {
//        
//        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    }
//    else{
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
//    }
//#else
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
//#endif
    
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
//    [self createHTML];
    
    if([self CheckIfDemoApp])
    {
    }
    else{
        [self CreatePushNotification];
    }
    
    
    [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationLandscapeLeft animated:NO];
    
    //[self updateOrientation];
    
    [self checkNetworkConnectivity];
    
    backgroundQueueForApplication = dispatch_queue_create("com.nanan.myscmipad.bgqueueForApplication", NULL);
    
    counterForLoadDB = 0;
    counterForStatusMessages = 0;
    self.strEmailIdOfLoggedInUser = @"test@test.com";
    
    onlyNumbersCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    [self loadLocalDB];
    
    // --- Database ----
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDir]];

    
    //--- Check for first time to show Terms and conditions ---
    strFirstTime = [[NSUserDefaults standardUserDefaults]objectForKey:@"FirstTime"];
    
    if (strFirstTime == nil) {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
        
        self.startViewController = [[StartStartViewController alloc] initWithNibName:@"StartStartViewController" bundle:[NSBundle mainBundle]];
        self.termsAndConditionVC = [[TermsAndConditionVC alloc] initWithNibName:@"TermsAndConditionVC" bundle:[NSBundle mainBundle]];
        
        self.window.rootViewController = self.termsAndConditionVC;
        [self.window makeKeyAndVisible];
    }
    else{
        self.obj_Manager = [[HJObjManager alloc] initWithLoadingBufferSize:6 memCacheSize:20];
        NSString* cacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/imgcache/"] ;
        HJMOFileCache* fileCache = [[HJMOFileCache alloc] initWithRootPath:cacheDirectory];
        self.obj_Manager.fileCache = fileCache;
        fileCache.fileCountLimit = 100;
        fileCache.fileAgeLimit=60*60*24*7;
        [fileCache trimCacheUsingBackgroundThread];
        
        //Create Sync Folder
        [self createSyncFolder];
        
        //Sleep for 3 seconds
       // [NSThread sleepForTimeInterval:3];
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
        self.startViewController = [[StartStartViewController alloc] initWithNibName:@"StartStartViewController" bundle:[NSBundle mainBundle]];
            self.window.rootViewController = self.startViewController;
        self.window.makeKeyAndVisible;

    }
    
    
    return YES;
}



#pragma mark getServiceURL
+(NSString *)getServiceURL:(id)strServiceName
{
    return [NSString stringWithFormat:@"%@%@",SERVER_URL,strServiceName];
}

+(AppDelegate *)AppDelegateInstance
{
    return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}

#pragma mark --PushNotification Deligates

-(void)CreatePushNotification
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:
(NSData *)deviceToken
{
    NSCharacterSet *angleBrackets = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    NSString *strdeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:angleBrackets];
    strdeviceToken = [strdeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"DEVIce toke::%@",strdeviceToken);
    [[NSUserDefaults standardUserDefaults]setObject:strdeviceToken forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
//    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,PUSHNOTIFICATION_WS,[NSString stringWithFormat:@"device_token=%@",strdeviceToken]];
//    
//    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
//    
//    ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
//    [requestnew setDelegate:self];
//    [requestnew setRequestMethod:@"POST"];
//    [requestnew setTimeOutSeconds:300];
//    [requestnew setPostValue:strdeviceToken forKey:@"xmlData"];
//    [requestnew startAsynchronous];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:
(NSError *)error
{
    DLog(@"Failed to register for remote notifications: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:
(NSDictionary *)userInfo
{
    sleep(2);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    @try {
        NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
        DLog(@"text::Push notification::%@",text);
    }
    @catch (NSException *exception) {
        DLog(@"Error:::");
    }
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    DLog(@"Failed:::::");
}


#pragma mark - document directory skip backup
-(BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)fileURL
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]])
    {
        return NO;
    }
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer isEqualToString:@"5.0.1"])
    {
        const char* filePath = [[fileURL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    }
    else if([currSysVer isEqualToString:@"5.0"])
    {
        assert([[NSFileManager defaultManager] fileExistsAtPath: [fileURL path]]);
        const char* filePath = [[fileURL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    }
    else if (&NSURLIsExcludedFromBackupKey)
    {
        NSError *error = nil;
        BOOL result = [fileURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        if (result == NO)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}

#pragma mark - Create Html file
-(void)createHTML{
    // get user input
    
    NSString *userText = @"Hello, world!";
    NSString *html = [NSString stringWithFormat:@"<html><body>%@</body><html>", userText];
    NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filename = [docsFolder stringByAppendingPathComponent:@"sample.html"];
    NSError *error;
    [html writeToFile:filename atomically:NO encoding:NSUTF8StringEncoding error:&error];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    //------ Code to run sync when application enters foreground ------
    /*
    if (![self.window.rootViewController isKindOfClass:[LoginViewController class]]) {
        
        // [NSThread sleepForTimeInterval:3];
        
        [self checkNetworkConnectivity];
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
            [[SDSyncEngine sharedEngine] startSync];
        }
    }
    */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (counterForLoadDB == 0) {
        counterForLoadDB = 1;
    }
    else{
        [self loadLocalDB];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    DLog(@"Crashed");
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/"];
    
    //Remove footprints
    for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
        NSString *exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
        [AppDelegate removeFolderFromDocumentsDirectory:exactfilePath];
    }
    //Close the database
    [[AppDelegate getAppDelegateObj].database close];
    
    
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    NSString *strmessage = @"Please clear unused background applications for better performace and to avoid memory issues!";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Critical" message:strmessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)createSyncFolder{
    NSString *dataPath = [AppDelegate getFileFromDocumentsDirectory:SYNCFOLDER];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError* error;
  
    if([fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error])
    {
        DLog(@"%@ created subdirectory",SYNCFOLDER);
    }
    else
    {
        DLog(@"%@ Directory Already Exist",SYNCFOLDER);
    }
    
}


-(NSString *)HandleHTMLCharacters:(NSString *)strRequest
{
    
    strRequest = [strRequest stringByReplacingOccurrencesOfString:@"&" withString:@""];
    strRequest = [strRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
    strRequest = [strRequest stringByReplacingOccurrencesOfString:@">" withString:@""];
    strRequest = [strRequest stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    strRequest = [strRequest stringByReplacingOccurrencesOfString:@"'" withString:@""];
    
    return strRequest;
}

#pragma mark - Application's Documents directory

#pragma mark - Date formatter
- (void)initializeDateFormatter {
    if (!self.dateFormatter) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"dd-MM-yyyy"];
    }
}

- (void)initializeUnixDateFormatter {
    if (!self.dateFormatter) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
}

- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString {
    [self initializeDateFormatter];
    return [self.dateFormatter dateFromString:dateString];
}

- (NSString *)dateStringForAPIUsingDate:(NSDate *)date {
    [self initializeDateFormatter];
    NSString *dateString = [self.dateFormatter stringFromDate:date];
    return dateString;
}

- (NSString *)dateStringForAPIUsingUnixDate:(NSDate *)date {
    [self initializeUnixDateFormatter];
    NSString *dateString = [self.dateFormatter stringFromDate:date];
    return dateString;
}

+ (int)calcDaysBetweenTwoDate:(NSDate *)fromDate ToDate:(NSDate *)toDate{
    NSTimeInterval distanceBetweenDates = [toDate timeIntervalSinceDate:fromDate];
    int daysBetweenDates = distanceBetweenDates / 3600 / 24;
    return daysBetweenDates;
}

+ (NSString *)getCurrentYear{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *)getCurrentMonth{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [dateFormatter stringFromDate:[NSDate date]];
}


- (NSString *)setDateInAustrailianFormat:(NSString *)strDate{
    
    NSString *strAusDate = @"";
    if (![strDate isEqualToString:@""]) {
        NSArray* arrDate = [strDate componentsSeparatedByString: @" "];
        NSString* date = [arrDate objectAtIndex: 0];
        NSString* time = [arrDate objectAtIndex: 1];
        
        NSArray* arrDatePieces = [date componentsSeparatedByString: @"-"];
        strAusDate = [NSString stringWithFormat:@"%@-%@-%@ %@",[arrDatePieces objectAtIndex:2],[arrDatePieces objectAtIndex:1],[arrDatePieces objectAtIndex:0],time];
    }
    
    
    return strAusDate;
}

- (NSString *)setDateOnlyInAustrailianFormat:(NSString *)strDate{
    
    NSString *strAusDate = @"";
    if (![strDate isEqualToString:@""]) {
        NSArray* arrDate = [strDate componentsSeparatedByString: @" "];
        NSString* date = [arrDate objectAtIndex: 0];
        
        NSArray* arrDatePieces = [date componentsSeparatedByString: @"-"];
        strAusDate = [NSString stringWithFormat:@"%@-%@-%@",[arrDatePieces objectAtIndex:2],[arrDatePieces objectAtIndex:1],[arrDatePieces objectAtIndex:0]];
    }
    return strAusDate;
}

+(NSString *)getCurrentDayOfWeek2
{
    NSDate *today = [NSDate date];
    NSDateFormatter *myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"c"]; //day number like 7 for saturday.
    
    NSString *dayofWeek = [myFormatter stringFromDate:today];
    return dayofWeek;
}

+(NSInteger)getCurrentDayOfWeek{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *today = [NSDate date];
    NSDateComponents *compForWeekday = [gregorian components:(NSWeekdayCalendarUnit) fromDate:today];
    NSInteger weekDayAsNumber = [compForWeekday weekday]; // The week day as number but with sunday starting as 1
    
    weekDayAsNumber = ((weekDayAsNumber + 5) % 7) + 1; // Transforming so that monday = 1 and sunday = 7
    
    return weekDayAsNumber;
}


#pragma mark - Get Document Directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (AppDelegate*)getAppDelegateObj{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (NSString *)getFileFromLocalDirectory:(NSString *)resourceName Type:(NSString *)type{
    return [[NSBundle mainBundle] pathForResource:resourceName ofType:type];
}

+ (NSString *)getFileFromDocumentsDirectory:(NSString *)resourceName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:resourceName];
    return path;
}

+ (BOOL)removeFolderFromDocumentsDirectory:(NSString *)resourceName
{
    return [[NSFileManager defaultManager] removeItemAtPath:resourceName error:nil];
}

- (void)loadLocalDB{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
    NSLog(@"writableDBPath %@",writableDBPath);
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) {
        
    }
    else{
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        
        if (!success) {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
    
    self.database = [FMDatabase databaseWithPath:writableDBPath];
    self.databaseQueue = [FMDatabaseQueue databaseQueueWithPath:writableDBPath];
    
    NSLog(@"writableDBPath %@",writableDBPath);
    
    if (![database open]) {
        [AJNotificationView showNoticeInView:self.loginViewController.view
                                        type:AJNotificationTypeRed
                                       title:@"Unable to open database! Please try again later."
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        
        return;
    }
}


#pragma mark - Email Validation

+ (BOOL) validateEmail: (NSString *) EmailID {
    NSString *emailRegex = @"[0-9a-z._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:EmailID];
}

+ (BOOL) validateBlank: (NSString *) text{
    return ([text isEqualToString:@""])? TRUE:FALSE;
}




#pragma mark - Data Cache

- (NSString *)getFileFromCacheDir:(NSString *)strURL{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *strFileName = [strURL getFileNameFromURL];
    NSString *pdfCachePath = [NSString stringWithFormat:@"%@/%@",
                              cachePath,
                              strFileName];
    
    return pdfCachePath;
}

-(void)downloadFileToCacheFromURL:(NSString *)strURL{

    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *strFileName = [strURL getFileNameFromURL];
    NSString *pdfCachePath = [NSString stringWithFormat:@"%@/%@",
                              cachePath,
                              strFileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pdfCachePath]) {
        DLog(@"Cached file found, using it");
    }
    else {
        NSData *pdfData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:strURL]];
        [pdfData writeToFile:pdfCachePath atomically:YES];
        
    }
    
}

-(void)removefileFromCache:(NSString *)strURL{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *strFileName = [strURL getFileNameFromURL];
    NSString *pdfCachePath = [NSString stringWithFormat:@"%@/%@",
                              cachePath,
                              strFileName];

    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pdfCachePath]) {
        DLog(@"Cached file found, using it");
        [[NSFileManager defaultManager] removeItemAtPath:pdfCachePath error:nil];
    }

}

-(NSString *) md5:(NSString *) str {
    unsigned char result[16];
    
    //----Changes for IOS8
    //const char *cStr = [str UTF8String];
    //CC_MD5( cStr, strlen(cStr), result );
    
    
    NSString *str12 =[NSString stringWithFormat:
                    @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                    result[0], result[1], result[2], result[3],
                    result[4], result[5], result[6], result[7],
                    result[8], result[9], result[10], result[11],
                    result[12], result[13], result[14], result[15]
                    ];
    return str12;
}

-(NSString *)getTradingTerms:(NSString *)strTradingTermCode{
    if([[strTradingTermCode trimSpaces] isEqualToString:@"14"])
    {
        return @"14 days from invoice";
    }
    else if([[strTradingTermCode trimSpaces] isEqualToString:@"C1"])
    {
        return @"Cash On Delivery";
    }
    else if([[strTradingTermCode trimSpaces] isEqualToString:@"I7"])
    {
        return @"7 days from invoice";
    }
    else if([[strTradingTermCode trimSpaces] isEqualToString:@"S7"])
    {
        return @"7 days from statement";
    }
    else
    {
        return @"30 days from invoice";
    }

}
/*========Used for PEFODDS
-(NSString *)getTradingTerms:(NSString *)strTradingTermCode{
  if(![strTradingTermCode isKindOfClass:[NSString class]] )
  {
    return @"";
  }
    if ([strTradingTermCode isEqualToString: @"01"]) {
        return @"Payment on Delivery" ;
    } else if ([strTradingTermCode isEqualToString: @"02"]) {
        return @"Tuesday FOllowing";
    } else if ([strTradingTermCode isEqualToString: @"03"]) {
        return @"Paymt Prior Despacth";
    } else if ([strTradingTermCode isEqualToString: @"04"]) {
        return @"7 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"05"]) {
        return @"14 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"06"]) {
        return @"7 days from EOM";
    } else if ([strTradingTermCode isEqualToString: @"07"]) {
        return @"30 days from EOM";
    } else if ([strTradingTermCode isEqualToString: @"08"]) {
        return @"14 days from EOM";
    } else if ([strTradingTermCode isEqualToString: @"09"]) {
       return @"30 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"10"]) {
        return @"45 days from EOM";
    } else if ([strTradingTermCode isEqualToString: @"11"]) {
        return @"2 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"12"]) {
        return @"8 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"13"]) {
        return @"1 days from invoice";
    } else if ([strTradingTermCode isEqualToString: @"90"]) {
        return @"Payment Plan";
    } else {
        return @"Recovery Action";
    }
}

 */


- (NSString *)getMacAddress {
    MacAddress *ObjMacAddress = [[MacAddress alloc]init];
    NSString *deviceMacAddress = [ObjMacAddress getMacAddress];
    ObjMacAddress=nil;
    return deviceMacAddress;
}


#pragma mark - Animation

- (CABasicAnimation *)fadeInFadeOutAnimation{
    //Create an animation with pulsating effect
    CABasicAnimation *theAnimation;
    
    //within the animation we will adjust the "opacity"
    //value of the layer
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    //animation lasts 0.4 seconds
    theAnimation.duration=3.0;
    //and it repeats forever
    theAnimation.repeatCount= HUGE_VAL;
    
    //we want a reverse animation
    theAnimation.autoreverses=YES;
    //justify the opacity as you like (1=fully visible, 0=unvisible)
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.1];
    
    return theAnimation;
}

- (CABasicAnimation *)fadeInFadeOutAnimationForStatusBar{
    //Create an animation with pulsating effect
    CABasicAnimation *theAnimation;
    
    //within the animation we will adjust the "opacity"
    //value of the layer
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    //animation lasts 0.4 seconds
    theAnimation.duration=0.6;
    //and it repeats forever
    theAnimation.repeatCount= HUGE_VAL;
    
    //we want a reverse animation
    theAnimation.autoreverses=YES;
    //justify the opacity as you like (1=fully visible, 0=unvisible)
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.5];
    
    return theAnimation;
}

- (void)startSpinAnimation:(UIButton *)btn{
    btn.userInteractionEnabled = FALSE;
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2*M_PI)];
    rotation.duration = 1.1; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [btn.layer addAnimation:rotation forKey:@"Spin"];
}

- (void)stopSpinAnimation:(UIButton *)btn{
    btn.userInteractionEnabled = TRUE;
    [btn.layer removeAnimationForKey:@"Spin"];
}

+ (CGSize)getRotatedViewSize:(UIView *)view
{
    BOOL isLandscape = UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    
    float max = MAX(view.bounds.size.width, view.bounds.size.height);
    float min = MIN(view.bounds.size.width, view.bounds.size.height);
    
    return (isLandscape ? CGSizeMake(max, min):CGSizeMake(min, max));
}

+ (void)pullUpPushDownViews:(UIView *)view ByRect:(CGRect)frame{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [view setFrame:frame];
    [UIView commitAnimations];
    
}



#pragma mark - Database calls
-(void)callGetEmailIdOfloggedInUser:(FMDatabase *)db{
    

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *strUserName = [prefs objectForKey:@"userName"];
    
    FMResultSet *res;
        @try {
            
            res = [[AppDelegate getAppDelegateObj].database executeQuery:@"SELECT EmailId FROM members WHERE Username = ?",strUserName] ;
            
            if (!res)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([res next]) {
                NSDictionary *dict = [res resultDictionary];
                self.strEmailIdOfLoggedInUser = [dict objectForKey:@"EmailId"];
            }
            
            [res close];
            
        }
        
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            [res close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
            
            DLog(@"%@",strErrorMessage);
            
        }
    
    
}

#pragma mark - WS calls
-(void)callWSForLoginList
{
    NSString *last_sync_date_loginlist = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"members"];
    
    NSString *parameters = [NSString stringWithFormat:@"last_syncDate=%@",last_sync_date_loginlist];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,LOGINLISTWS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebserviceWithoutSpinner:strURL];
}


#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    
    if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"Flag"]) {
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            NSMutableArray *loginListArr = [[NSMutableArray alloc]init];
            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"Data"];
                [loginListArr addObject:dict];
            }
            else{
                loginListArr = [[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"Data"];
            }
            
            dispatch_async(backgroundQueueForApplication, ^(void) {
                
                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    @try
                    {
                        for (NSDictionary *dict in loginListArr){
                            
                            BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `members` (`SCM_RECNUM`, `Username`, `EmailId`, `Password`, `members`, `created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?)",
                                      [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                                      [[dict objectForKey:@"Username"] objectForKey:@"text"],
                                      [[dict objectForKey:@"EmailId"] objectForKey:@"text"],
                                      [[dict objectForKey:@"Password"] objectForKey:@"text"],
                                      [[dict objectForKey:@"members"] objectForKey:@"text"],
                                      [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                      [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                            
                            if (!y)
                            {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                        }
                        
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"members" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        //Commit here
                        [db commit];
                        
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });

                        //[db close];
                    }
                }];
                
            });
            
        }
        else{
            dispatch_async(backgroundQueueForApplication, ^(void) {
                
                [dbQueueLoginList inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    @try{
                        
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"members" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        //Commit here
                        [db commit];
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                        //[db close];
                    }
                }];
                
            });
        }
    }
    else{
        
        dispatch_async(backgroundQueueForApplication, ^(void) {
            
            [dbQueueLoginList inTransaction:^(FMDatabase *db, BOOL *rollback) {
                @try{
                    
                    //Update the New "Last sync" date
                    NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"LoginList"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                    
                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"noticemsgs" NewSyncDate:strNewSyncDate Database:db];
                    
                    if (!y) {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    
                    //Commit here
                    [db commit];
                }
                @catch(NSException* e)
                {
                    *rollback = YES;
                    // rethrow if not one of the two exceptions above
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                    });
                    
                    //[db close];
                }
            }];
            
        });
    }
    
    ////////////
}



- (void)checkNetworkConnectivity{
    //Set the Reachability observer
    [Reachability_CentralizedHelper sharedHelper].delegate = nil;
    [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
    [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
    
    Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
    NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
    
    if(internetStatus == NotReachable)
    {
        self.rootViewController.isNetworkConnected= NO;
        self.isNetworkConnected = NO;
        self.rootViewController.lblStatusText.text = SERVER_OFFLINE_MESSAGE;
    }
    else{
        self.rootViewController.isNetworkConnected= YES ;
        self.isNetworkConnected = YES;
        self.rootViewController.lblStatusText.text = SERVER_ONLINE_MESSAGE;
    }
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            DLog(@"Not connected");
            self.rootViewController.isNetworkConnected = NO;
            self.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            DLog(@"Connected");
            self.rootViewController.isNetworkConnected = YES;
            self.isNetworkConnected = YES;    
            break;
        }
        case ReachableViaWWAN:
        {
            self.rootViewController.isNetworkConnected = YES;
            self.isNetworkConnected = YES;
            break;
        }
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
    
}


-(NSString *)SendEmail:(NSMutableDictionary *)dictHeader aryProducts:(NSMutableArray *)aryProducts strHtmlName:(NSString *)strHtmlName
{
    @try {
        //Read from HTML file
        NSError* error = nil;
       // strHtmlName = @"1";
        NSString *path = [[NSBundle mainBundle] pathForResource: strHtmlName ofType: @"html"];
        NSString *strHTLM = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
        
        if([dictHeader objectForKey:@"Customer"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($code)" withString:[[dictHeader objectForKey:@"Customer"]objectForKey:@"text"]];
        }
        
        if([[dictHeader objectForKey:@"Phone"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($phone)" withString:[[dictHeader objectForKey:@"Phone"] objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Phone"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($phone)" withString:[[dictHeader objectForKey:@"Phone"] objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Rate"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"Rate"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"Rate"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($rate)" withString:[rate stringValue]];
            }
            else
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($rate)" withString:[[dictHeader objectForKey:@"Rate"]objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Email"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($email)" withString:[[dictHeader objectForKey:@"Email"]objectForKey:@"text"]];
            
        }
        
        if([[dictHeader objectForKey:@"Name"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($name)" withString:[[dictHeader objectForKey:@"Name"] objectForKey:@"text"]];
        }

        if([[dictHeader objectForKey:@"Contact"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($contact)" withString:[[dictHeader objectForKey:@"Contact"]objectForKey:@"text" ]];
            
        }

        if([[dictHeader objectForKey:@"Email"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($email)" withString:[[dictHeader objectForKey:@"Email"]objectForKey:@"text"]];
            
        }

        if([[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($creditLimit)" withString:[rate stringValue]];
            }
            else
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($creditLimit) " withString:[[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"]];
            
        }

        if([[dictHeader objectForKey:@"PriceCode"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"PriceCode"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"PriceCode"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($prCode)" withString:[rate stringValue]];
            }
            else
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($prCode)" withString:[[dictHeader objectForKey:@"PriceCode"]objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Fax"]objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($faxno)" withString:[[dictHeader objectForKey:@"Fax"]objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Balance"] objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"Balance"] objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"Balance"] objectForKey:@"text"];
                NSString *strval = [NSString stringWithFormat:@"$ %.2f",[rate floatValue]];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($bal)" withString:strval];
            }
            else

            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($bal)" withString:[[dictHeader objectForKey:@"Balance"] objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($tradingTerms)" withString:[rate stringValue]];
            }
            else

            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($tradingTerms)" withString:[[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"]];
            
        }

        NSString *strProducts = @"";
        NSString *strProductshtml = @"";
        if(aryProducts.count > 0)
        {
            for (int j = 0; j < aryProducts.count; j++) {
                strProducts = [NSString stringWithFormat:@"<tr>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td7\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\"></p>"
                                "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\"></p>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\"></p>"
                              "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\"></p>"
                               "</td>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "</tr>",[[[aryProducts objectAtIndex:j] objectForKey:@"StockCode"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Description"] objectForKey:@"text"],[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[[aryProducts objectAtIndex:j] objectForKey:@"Price"] objectForKey:@"text"] floatValue]]];
                
             
                               
                               // s = [NSString stringWithFormat:@"<br>%@</br>",strProducts];
     
                strProductshtml = [strProductshtml stringByAppendingString:strProducts];

                
            }
            
        }
        strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"CustoMerOrderToReplace" withString:strProductshtml];
            return strHTLM;
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return @"";
    }
   
      return @"";
}


-(NSString *)SendEmailForSpecialProduct:(NSMutableDictionary *)dictHeader aryProducts:(NSMutableArray *)aryProducts strHtmlName:(NSString *)strHtmlName
{
    @try {
        //Read from HTML file
        NSError* error = nil;
        // strHtmlName = @"1";
        NSString *path = [[NSBundle mainBundle] pathForResource: strHtmlName ofType: @"html"];
        NSString *strHTLM = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
        
        if([[dictHeader objectForKey:@"Customer"]objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($code)" withString:[[dictHeader objectForKey:@"Customer"]objectForKey:@"text"]];
        }
        
        if([[dictHeader objectForKey:@"Phone"]objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($phone)" withString:[[dictHeader objectForKey:@"Phone"]objectForKey:@"text"]];
            
        }
//        if([[dictHeader objectForKey:@"Phone"]objectForKey:@"text"])
//        {
//            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($phone)" withString:[[dictHeader objectForKey:@"Phone"]objectForKey:@"text"]];
//            
//        }
        if([[dictHeader objectForKey:@"Rate"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"Rate"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"Rate"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($rate)" withString:[rate stringValue]];
            }
            else
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($rate)" withString:[[dictHeader objectForKey:@"Rate"]objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"Email"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($email)" withString:[[dictHeader objectForKey:@"Email"] objectForKey:@"text"]];
            
        }
        
        if([[dictHeader objectForKey:@"Name"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($name)" withString:[[dictHeader objectForKey:@"Name"] objectForKey:@"text"]];
            
        }
        
        if([[dictHeader objectForKey:@"Contact"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($contact)" withString:[[dictHeader objectForKey:@"Contact"] objectForKey:@"text"]];
            
        }
        
        if([[dictHeader objectForKey:@"Email"] objectForKey:@"text"])
        {
            strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($email)" withString:[[dictHeader objectForKey:@"Email"] objectForKey:@"text"]];
            
        }
        
        if([[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($creditLimit)" withString:[rate stringValue]];
            }
            else if ([[[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"] isEqualToString:@""]){
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($creditLimit) " withString:@"--"];

            }
            else{
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($creditLimit) " withString:[[dictHeader objectForKey:@"CreditLimit"]objectForKey:@"text"]];
            }
            
        }
        

        if([[dictHeader objectForKey:@"Fax"]objectForKey:@"text"])
        {
            if ([[dictHeader objectForKey:@"Fax"]objectForKey:@"text"]) {
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($faxno)" withString:[[dictHeader objectForKey:@"Fax"]objectForKey:@"text"]];

            }
            else{
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($faxno)" withString:@"--"];

            }
            
        }
        if([[dictHeader objectForKey:@"Discount"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"Balance"] objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"Balance"] objectForKey:@"text"];
                NSString *strval = [NSString stringWithFormat:@"$ %.2f",[rate floatValue]];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($bal)" withString:strval];
            }
            else
                
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($bal)" withString:[[dictHeader objectForKey:@"Balance"] objectForKey:@"text"]];
            
        }
        if([[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"])
        {
            if([[[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"] isKindOfClass:[NSNumber class]])
            {
                id rate = [[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"];
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($tradingTerms)" withString:[rate stringValue]];
            }
            else{
                
                strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"trim($tradingTerms)" withString:[[dictHeader objectForKey:@"TradingTerms"]objectForKey:@"text"]];
            }
            
        }
        
        NSString *strProducts = @"";
        NSString *strProductshtml = @"";
        
        if(aryProducts.count > 0)
        {
            for (int j = 0; j < aryProducts.count; j++) {
                
                strProducts = [NSString stringWithFormat:@"<tr>"
                               "<td valign=\"middle\" class=\"td9\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td7\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "<td valign=\"middle\" class=\"td8\">"
                               "<p class=\"p2\">%@</p>"
                               "</td>"
                               "</tr>",[[aryProducts objectAtIndex:j] objectForKey:@"StockCode"],[[aryProducts objectAtIndex:j] objectForKey:@"Description"] ,[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[aryProducts objectAtIndex:j] objectForKey:@"Discount"] floatValue]]];
                
                
//                [[aryProducts objectAtIndex:j] objectForKey:@"Discount"]
                // s = [NSString stringWithFormat:@"<br>%@</br>",strProducts];
                
                strProductshtml = [strProductshtml stringByAppendingString:strProducts];
                
                
            }
            
        }
        strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"CustoMerOrderToReplace" withString:strProductshtml];
        
        return strHTLM;
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return @"";
    }
    
    return @"";
}


-(BOOL)CheckIfDemoApp
{
#ifdef DemoAppTest
    {
        DLog(@"Welcome to DemoAppTest");
        return YES;
    }
    
#elif OriginalApplication
    {
        return NO;
        DLog(@"Welcome to OriginalApplication");
    }
#endif
}


#pragma mark - Display data

-(NSString*)ConvertStringIntoCurrencyFormatNew:(float)VieToShowfloat
{
    @try {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:2];
        NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:VieToShowfloat]];
        NSString *newStr = [NSString stringWithFormat:@"$ %@",numberAsString];
        DLog(@"%@", newStr);
        return newStr;
    }
    @catch (NSException *exception) {
        DLog(@"Error::%@",exception.description);
    }
}


-(NSString*)showFloatFormatValue:(float)VieToShowfloat
{
    @try {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:2];
        NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:VieToShowfloat]];
        NSString *newStr = [NSString stringWithFormat:@"%@",numberAsString];
        DLog(@"%@", newStr);
        return newStr;
    }
    @catch (NSException *exception) {
        DLog(@"Error::%@",exception.description);
    }
}

- (BOOL)allowNumbersOnly:(NSString *)string{
    
    if ([string length] > 0) {
        unichar c = [string characterAtIndex:0];
        if ([onlyNumbersCharacterSet characterIsMember:c])
        {
            return YES;
        }
        else
        {
            return NO;
            
        }
        
    }
    else{
        return YES;
    }
}

- (BOOL)allowFloatingNumbersOnly: (NSString *) string
{
    NSString *regex = @"^([0-9]*[.]{0,1}[0-9]*)$";
    
    NSPredicate *rp = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([rp evaluateWithObject:string])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



-(void)ConvertStringIntoCurrencyFormat:(id )VieToShow
{
    @try {
        if([VieToShow isKindOfClass:[UILabel class]])
        {
            UILabel *lblDis = VieToShow;
            NSArray *aryVal = [lblDis.text componentsSeparatedByString:@"."];
            NSNumberFormatter *num = [[NSNumberFormatter alloc] init];
            [num setPositiveFormat:@"#,##0"];
            [num setZeroSymbol:@","];
            BOOL flag;
            if([lblDis.text rangeOfString:@"$"].location == NSNotFound)
            {
                flag = NO;
            }
            else
            {
                flag = YES;
                lblDis.text = [lblDis.text stringByReplacingOccurrencesOfString:@"$ " withString:@""];
                lblDis.text = [lblDis.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            }
            NSNumber *numval = [NSNumber numberWithDouble:[lblDis.text doubleValue] ];
            if(numval.doubleValue > 0)
            {
                [num setNumberStyle: NSNumberFormatterCurrencyStyle];
                NSString *strVal = [num stringFromNumber:numval];
                NSString *strDec;
                if(aryVal.count > 1)
                {
                    strDec = [aryVal objectAtIndex:1];
                    if([strDec isKindOfClass:[NSString class]])
                    {
                        
                        if(strDec.length > 2)
                        {
                            strDec = [strDec substringToIndex:2];
                           // float val = [strDec floatValue];
                          //  strDec = [NSString stringWithFormat:@"%.2f",val];
                            
                        }
                    }
                }
                else
                    strDec = @"0";
                
                if(aryVal.count > 1)
                    lblDis.text = [NSString stringWithFormat:@"%@.%@",strVal,strDec];
                else
                    lblDis.text = [NSString stringWithFormat:@"%@",strVal];
            }
            lblDis.text = [NSString stringWithFormat:@"$ %@",lblDis.text];
            if(numval.doubleValue<=0)
            lblDis.text = [NSString stringWithFormat:@"$ 0.00"];    
        }
        else if([VieToShow isKindOfClass:[UITextField class]])
        {
            UITextField *lblDis = VieToShow;
            NSArray *aryVal = [lblDis.text componentsSeparatedByString:@"."];
            NSNumberFormatter *num = [[NSNumberFormatter alloc] init];
            [num setPositiveFormat:@"#,##0"];
            [num setZeroSymbol:@","];
            BOOL flag;
            if([lblDis.text rangeOfString:@"$"].location == NSNotFound)
            {
                flag = NO;
            }
            else
            {
                flag = YES;
                lblDis.text = [lblDis.text stringByReplacingOccurrencesOfString:@"$ " withString:@""];
                lblDis.text = [lblDis.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            }
            NSNumber *numval = [NSNumber numberWithDouble:[lblDis.text doubleValue] ];
            if(numval.doubleValue > 0)
            {
                [num setNumberStyle: NSNumberFormatterCurrencyStyle];
                NSString *strVal = [num stringFromNumber:numval];
                NSString *strDec;
                if(aryVal.count > 1)
                {
                    strDec = [aryVal objectAtIndex:1];
                    if([strDec isKindOfClass:[NSString class]])
                    {
                        
                        if(strDec.length > 2)
                        {
                            strDec = [strDec substringToIndex:2];
                            // float val = [strDec floatValue];
                            //  strDec = [NSString stringWithFormat:@"%.2f",val];
                            
                        }
                    }
                }
                else
                    strDec = @"0";
                
                if(aryVal.count > 1)
                    lblDis.text = [NSString stringWithFormat:@"%@.%@",strVal,strDec];
                else
                    lblDis.text = [NSString stringWithFormat:@"%@",strVal];
            }
            lblDis.text = [NSString stringWithFormat:@"$ %@",lblDis.text];
            if(numval.doubleValue<=0)
                lblDis.text = [NSString stringWithFormat:@"$ 0.00"];
        }
        
    }
    @catch (NSException *exception) {
        DLog(@"Error::%@",exception.description);
    }
}



@end
