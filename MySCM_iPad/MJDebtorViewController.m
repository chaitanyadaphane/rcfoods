//
//  MJDebtorViewController.m
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import "MJDebtorViewController.h"
#import "CustomCellGeneral.h"

@implementation MJDebtorViewController{
    BOOL isPeriod30;
}
@synthesize detailDict;
@synthesize tblView;

-(void)viewDidLoad{
    [super viewDidLoad];
    isPeriod30 = NO;
    NSLog(@"%@",detailDict);
    
}

-(void)viewDidUnload{
    [super viewDidUnload];
    detailDict = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Check for age period 30 OR 7
    NSString *ageingPeriodVal = [NSString stringWithFormat:@"%@",[detailDict objectForKey:@"ageing_period"]];
    
    if ([ageingPeriodVal isEqualToString:@"30"]) {
        isPeriod30 = YES;
    }
    else if([ageingPeriodVal isEqualToString:@"7"]){
        isPeriod30 = NO;
    }
    else{
        isPeriod30 = NO;
    }
}

#pragma mark - Tableview delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 60.0;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return  detailDict.count;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    CustomCellGeneral *cell = nil;
    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:4];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == 0) {
        cell.lblTitle.text =@"Name";
        cell.lblValue.text = [detailDict objectForKey:@"Name"];

    }
    else if (indexPath.row == 1){
        cell.lblTitle.text =@"TradingTerms";
        NSString *strTradingTerms = [NSString stringWithFormat:@"%@",[detailDict objectForKey:@"TradingTerms"] ];
        if([[strTradingTerms trimSpaces] isEqualToString:@"14"])
        {
            cell.lblValue.text = @"14 days from invoice";
        }
        else if([[strTradingTerms trimSpaces] isEqualToString:@"C1"])
        {
            cell.lblValue.text = @"Cash On delivery";
        }
        else if([[strTradingTerms trimSpaces] isEqualToString:@"I7"])
        {
            cell.lblValue.text = @"7 days from invoice";
        }
        else if([[strTradingTerms trimSpaces] isEqualToString:@"S7"])
        {
            cell.lblValue.text = @"7 days from statement";
        }
        else
        {
            cell.lblValue.text = @"30 days from invoice";
        }
        
    }
    else if (indexPath.row == 2){
        cell.lblTitle.text =@"Balance";
        cell.lblValue.text =[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[detailDict objectForKey:@"Balance"]floatValue]];

    }
    else if (indexPath.row == 3)
    {
        if (isPeriod30)
        {
            cell.lblTitle.text =@"Over30";
        }
        else
        {
            cell.lblTitle.text =@"Over7";
        }
        
        cell.lblValue.text =[[AppDelegate getAppDelegateObj] showFloatFormatValue:[[detailDict objectForKey:@"Over30"]floatValue]];
    }
    else if (indexPath.row == 4)
    {
        if (isPeriod30)
        {
            cell.lblTitle.text =@"Over60";
        }
        else
        {
            cell.lblTitle.text =@"Over14";
        }
        
        cell.lblValue.text =[[AppDelegate getAppDelegateObj] showFloatFormatValue:[[detailDict objectForKey:@"Over60"]floatValue]];
    }

    else if (indexPath.row == 5)
    {
        if (isPeriod30)
        {
            cell.lblTitle.text =@"Over90";
        }
        else
        {
            cell.lblTitle.text =@"Over21";
        }

        cell.lblValue.text =[[AppDelegate getAppDelegateObj] showFloatFormatValue:[[detailDict objectForKey:@"Over90"]floatValue]];
    }
    
    return cell;
}


@end
