//
//  CustomCellMenu.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 06/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "CustomCellMenu.h"
#import "SubMenu.h"

@implementation CustomCellMenu
@synthesize imgMenu,lblMenu,subMenu,btnDisclosure,btnSync,imgBackgroundView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)awakeFromNib
{
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setSubMenu:(SubMenu *)newSubMenu {
    
    if (subMenu != newSubMenu) {
        subMenu = newSubMenu;

        lblMenu.text = subMenu.name;
        
        /*
        if ([subMenu.name isEqualToString:@"SYNC"]) {
            btnDisclosure.hidden = FALSE;
            btnSync.hidden = FALSE;
        }
        else{
            btnSync.hidden = TRUE;
        }*/
        
        if ([subMenu.hasPage isEqualToString:@"YES"]) {
            btnDisclosure.hidden = FALSE;
        }
        else{
            btnDisclosure.hidden = TRUE;
        }

    }
}



@end
