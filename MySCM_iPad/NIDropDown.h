//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod:(NIDropDown *) sender withSelectedDict:(NSMutableDictionary *)dict;

@optional
- (void) getindexValue: (NIDropDown *)sender withindex:(NSIndexPath*)index withTitle:(NSString*)title;

@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;

-(void)hideDropDown:(UIButton *)b;
-(id)showDropDown:(UIButton *)b withHeight:(CGFloat *)height withData:(NSArray *)arr withImages :(NSArray *)imgArr WithDirection:(NSString *)direction On:(UIView *)vw;
@end
