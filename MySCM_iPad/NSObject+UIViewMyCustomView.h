//
//  NSObject+UIViewMyCustomView.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (MyCustomView)

//Remove all subviews
-(void)removeAllSubviews;

//Remove all Gestures
-(void)removeAllGestures;
@end
