

#import <UIKit/UIKit.h>

@interface UIView (firstResponder)

- (UIView *)findFirstResponder;

@end
