

#import <UIKit/UIKit.h>
#import "SEFilterKnob.h"
#import "AppDelegate.h"
@interface SEFilterControl : UIControl
{
    
    AppDelegate *appDelegate;

}
-(id) initWithFrame:(CGRect) frame Titles:(NSArray *) titles;
-(void) setSelectedIndex:(int)index;
-(void) setTitlesColor:(UIColor *)color;
-(void) setTitlesFont:(UIFont *)font;
-(void) setHandlerColor:(UIColor *)color;
-(id) TestInitWithFrame:(CGRect) frame Titles:(NSArray *) titles controlTag:(int)tag;

@property(nonatomic, retain) UIColor *progressColor;
@property(nonatomic, readonly) int SelectedIndex;
@end
