//
//  CustomCellGeneral.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 11/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLGlowingTextField.h"
#import "AsyncImageView.h"

@interface CustomCellGeneral : UITableViewCell{
    IBOutlet UIImageView *imgViewNoticeBg;
    NSMutableArray *imagesArr;
    
    

}

@property (nonatomic,strong)NSMutableArray *imagesArr;
@property (weak, nonatomic) IBOutlet UIButton *productImageBtn;
@property (weak, nonatomic) IBOutlet UIImageView *product_imageview;

@property (nonatomic, retain) IBOutlet UIImageView *imgViewNoticeBg;
@property (weak, nonatomic) IBOutlet UIButton *showPhotoBtn;

//ProductPurchaseHistory
@property (nonatomic, retain) IBOutlet UILabel *lblProdDate;
@property (nonatomic, retain) IBOutlet UILabel *lblProdQuantity;
@property (nonatomic, retain) IBOutlet UILabel *lblProdPrice;
@property (nonatomic, retain) IBOutlet UILabel *lblProdInv;

//Notice Board
@property (nonatomic, retain) IBOutlet UILabel *lblDate;
@property (nonatomic, retain) IBOutlet UILabel *lblMessage;
@property (nonatomic, retain) IBOutlet UIButton *btnMessage;
@property (nonatomic, retain) IBOutlet UILabel *lblCreatedBy;
@property (nonatomic, retain) IBOutlet UIButton *btnReadUnread;
@property (nonatomic, retain) IBOutlet UIButton *btnAttachment;
@property (nonatomic, retain) IBOutlet UIButton *btnSelectsalesperson;

//New Products
@property (nonatomic, retain) IBOutlet UILabel *lblStockCode;
@property (nonatomic, retain) IBOutlet UILabel *lblProdDesc;
@property (nonatomic, retain) IBOutlet UILabel *lblProductGroup;
@property (nonatomic, retain) IBOutlet UILabel *lblWarehouse;
@property (nonatomic, retain) IBOutlet UILabel *lblLocation;
@property (nonatomic, retain) IBOutlet UILabel *lblPrice;
@property (nonatomic, retain) IBOutlet UILabel *lblDateAdded;

//Quote History
@property (nonatomic, retain) IBOutlet UILabel *lblCustomerName;
@property (nonatomic, retain) IBOutlet UILabel *lblQuoteNo;
@property (nonatomic, retain) IBOutlet UIImageView *imgViewCellBackground;

//Add Quote
@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UITextField *txtValue;


//View Quote
@property (nonatomic, retain) IBOutlet UILabel *lblValue;

//Review Cell list
@property (weak, nonatomic) IBOutlet UILabel *lblcode;
@property (weak, nonatomic) IBOutlet UILabel *prodDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;


//Quote Detail List
@property (nonatomic, retain) IBOutlet UIButton *btnDetailView;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;

//Quote Glowing text field
@property (nonatomic, retain) IBOutlet UITextField *txtGlowingValue;
@property (weak, nonatomic) IBOutlet UIButton *plusQuantityBtn;
@property (weak, nonatomic) IBOutlet UIButton *minusQuantityBtn;

//Quote Status
@property (nonatomic, retain) IBOutlet UIButton *btnQuoteStatus;

//Quote Status
@property (nonatomic, retain) IBOutlet UISwitch *swtch;

@property (nonatomic, retain) IBOutlet UILabel *lblCashAmount;

@property (nonatomic, retain) IBOutlet HJManagedImageV *imgBackgroundView;

@property (nonatomic,retain) IBOutlet UIImageView *imgProduct;

@end
