//
//  MJProductImgViewController.m
//  Blayney
//
//  Created by POOJA MISHRA on 20/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import "MJProductImgViewController.h"

@interface MJProductImgViewController ()

@end

@implementation MJProductImgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.prod_img_url.length > 0 && ![self.prod_img_url containsString:@"null"])
    {
        self.productImg.imageURL = [NSURL URLWithString:self.prod_img_url];
        if(self.productImg.image == NULL)
        {
            self.productImg.image = [UIImage imageNamed:@"place_holder.png"];
        }
    }
    else
    {
        self.productImg.image = [UIImage imageNamed:@"place_holder.png"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
