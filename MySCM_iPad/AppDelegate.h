//
//  AppDelegate.h
//  Blayney_iPad
//
//  Created by ashutosh dingankar on 05/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "NSString+MD5.h"
#import "MacAddress.h"
#import "HJObjManager.h"
#import "NDHTMLtoPDF.h"
#import "TermsAndConditionVC.h"
#define IOS_Version [[[UIDevice currentDevice] systemVersion]floatValue]

#define ADMIN @"admin"
#define TELE_SALES @"telesales"
#define SALES @"sales"
//https://www.scmcentral.com.au/webservices_rcf/
#define SERVER_URL @"https://www.scmcentral.com.au/" // RCF
#define HOST_NAME @"www.scmcentral.com.au"   // RCF
//http://scm.blayneyfoods.com.au:82/
#define WSURL SERVER_URL@"webservices_rcf/"
#define WSURL_WSDL WSURL@"wwservice.php?wsdl"

#define NOTICE_ATTACHMENT_URL SERVER_URL@"public/upload_noticemsgs/"
#define PDF_ATTACHMENT_URL SERVER_URL@"public/upload_pdf/"
#define IMAGE_ATTACHMENT_URL SERVER_URL@"public/upload_thumbnail/"

#define DELETE_SPECIALS_WS @"sync/deletespecials.php?"
#define DELETE_MESSAGES_WS @"sync/deletemessages.php?"
#define MEMBER_WS @"sync/members.php?"
#define ARMASTER_WS @"sync/armaster.php?"
#define ARTRANS_WS @"sync/artrans.php?"
#define ARCOMMENT_WS @"sync/arcoment.php?"
#define NEWSAMASTER_WS @"sync/new_samaster.php?"
#define PODETAIL_WS @"sync/podetail.php?"
#define SAHISBUD_WS @"sync/sahisbud.php?"
#define SAMASTER_WS @"sync/samaster.php?"
#define SMCUSPRO_WS @"sync/smcuspro.php?"
#define SODETAIL_WS @"sync/sodetail.php?"
#define SOHEADER_WS @"sync/soheader.php?"
#define SOQUOHEA_WS @"sync/soquohea.php?"
#define SOQUODET_WS @"sync/soquodet.php?"
#define SPECIALS_WS @"sync/specials.php?"
#define SYSDESC_WS @"sync/sysdesc.php?"
#define SYSDISCT_WS @"sync/sysdisct.php?"
#define SYSLOGON_WS @"sync2/syslogon.php?"
#define SYSISTAX_WS @"sync/sysistax.php?"
#define SYSUSRNO_WS @"sync/sysusrno.php?"
#define ARPAYMENT_WS @"sync/arpaymnt.php?"
#define ARHISHEA_WS @"sync/arhishea.php?"
#define ARHISDET_WS @"sync/arhisdet.php?"
#define NOTICEMSGS_WS @"sync2/noticemsgs.php?"
#define SYSCONT_WS @"sync/syscont.php?"
#define SYSCONT_WS @"sync/syscont.php?"
#define CurrentOrderStatus_WS @"currorderstatus/list_currorderstatus.php?"
#define QuoteStatusStatus_WS @"quotestatus/listquotes.php?"
#define QuoteDetails_WS @"quotestatus/view_quotes.php?"
#define SOHEADER_ @"soheader_"
#define SODETAIL_ @"sodetail_"
#define SOQUOHEA_ @"soquohea_"
#define SOQUODET_ @"soquodet_"
#define SOQUOHEAOFFLINE_ @"soquoheaoffline_"
#define SOQUODETOFFLINE_ @"soquodetoffline_"
#define SODETAIL_DELETE_ @"sodetail_delete_"
#define SOQUODET_DELETE_ @"soquodet_delete_"


#define UPLOAD_FILE_WS @"uploadSync.php"
#define UPLOAD_DELETE_WS @"uploadDeleteSync.php"

#define LEFTMENU_WIDTH 250
#define DEFAULT_ROW_HEIGHT 60
#define HEADER_HEIGHT 70
#define DATACONTROLLER_WIDTH 477
#define LANDSCAPE_SCREEN_WIDTH_WITHOUT_STATUS_BAR 1004
#define LANDSCAPE_SCREEN_HEIGHT 768

#define ROWS_PER_PAGE 1500

#define SPINNER_HIDE_TIME 0.2
#define HUD_COMPLETION_IMAGE @"37x-Checkmark.png"

#define INTERNET_OFFLINE_MESSAGE @"You are using offline mode !"
#define SERVER_ONLINE_MESSAGE @"RCFoods server is connected !"
#define SERVER_OFFLINE_MESSAGE @"RCFoods server is currently offline !"
#define ERROR_IN_CONNECTION_MESSAGE @"Error in connection! Please try again later !"
#define SYNC_NOT_PERFORM_MESSAGE1 @"Can't perform sync this time as internet connection is not available !"
#define SYNC_NOT_PERFORM_MESSAGE2 @"Can't perform sync this time as Blayney server is currently offline !"
#define SERVER_TAKING_TIME_MESSAGE @"RCFoods server is taking time"
#define FETCH_LOCALLY_MESSAGE @"Data will be fetch from local database!"

#define SYNC_ALERT_MESSAGE @"Sync will take time.\nPlease stay connected to internet!"

#define SYNC_Delete_ALERT_MESSAGE @"Data will get flushed & Sync will take time.\nPlease stay connected to internet!"
#define LAST_UPDATED_ON @"Last Updated On"
#define APP_REQUIRES_SYNC @"Application Requires Sync! Hit Sync Under Settings Menu."
#define DATABASE_NAME @"mydatabaseRCF.sqlite"

#define LOGINLISTWS @"loginlist.php?" // JLS

#define SELECT_VALUE @"Select Value"


#define TO_RECIPIENT @"info.dayspringmedia.com.au"
#define CC_RECIPIENT @"shival.pandya@wwindia.com"

#define SERVER_CSV_PATH @"C/Program Files (x86)/Apache Software Foundation/Apache2.2/htdocs/"

#define SYNCFOLDER @"SyncFolder/"

#define UPLOADMAXCOUNT 3

#define UPLOAD_BEFORE_STATUS_MESSAGE_WITHOUT_LONG_PRESS @"Select the order and press \"Send\" to submit to head office."

#define UPLOAD_BEFORE_STATUS_ORDER_MESSAGE @"Select the order and press \"Send\" to submit to head office.\nPress the order longer to edit"

#define UPLOAD_BEFORE_STATUS_QUOTE_MESSAGE @"Select the quote and press \"Send\" to submit to head office.\nPress the quote longer to edit"

#define UPLOAD_RUNNING_STATUS_MESSAGE @"Upload in progress... Please do not refresh!!!"
#define UPLOAD_AFTER_STATUS_MESSAGE @"Upload complete."

#define SAVE_LOCAL_MESSAGE @"Do you want to save changes to local database?"

#define QUANTITY_GREATER_THAN_0 @"Quantity should be greater than 0!"
#define LOWER_PRICE_MESSAGE @"Price is lower than allowed margin!"
#define MARGIN_PERCENT_ERROR @"Price is too low!"

#define STANDARD_SYNC_DATE @"1900-01-01%2000:00:00"
#define STANDARD_APP_DATE @"1900-01-01 00:00:00"
#define STANDARD_SERVER_DATE @"1900-01-01 00:00:00"

#define MARGIN_MINIMUM_PERCENT 0

#define MARGIN_MINIMUM_PERCENT_QUOTE 0

#define ENTER_QUANTITY_HERE_MESSAGE @"Enter quantity here"
#define ENTER_PRICE_HERE_MESSAGE @"Enter price here"

#define LONG_PRESS_CUSTOMER_HISTORY_INFO_MESSAGE @"Press the transaction to see Payment info & press longer to see details!"

#define FREIGHT_VALUE 25.0f
#define FREIGHT_TAX 10.0f

#define CHECK_IMAGE @"checked.png"
#define UNCHECK_IMAGE @"unchecked.png"

#define CELL_IDENTIFIER1 @"cellIdentifier1"
#define CELL_IDENTIFIER2 @"cellIdentifier2"
#define CELL_IDENTIFIER3 @"cellIdentifier3"
#define CELL_IDENTIFIER4 @"cellIdentifier4"
#define CELL_IDENTIFIER5 @"cellIdentifier5"
#define CELL_IDENTIFIER6 @"cellIdentifier6"
#define CELL_IDENTIFIER7 @"cellIdentifier7"
#define CELL_IDENTIFIER8 @"cellIdentifier8"
#define CELL_IDENTIFIER9 @"cellIdentifier9"
#define CELL_IDENTIFIER10 @"cellIdentifier10"
#define CELL_IDENTIFIER11 @"cellIdentifier11"
#define CELL_IDENTIFIER12 @"cellIdentifier12"
#define CELL_IDENTIFIER13 @"cellIdentifier13"
#define CELL_IDENTIFIER14 @"cellIdentifier14"

#define CELL_IDENTIFIER16 @"cellIdentifier16"
#define CELL_IDENTIFIER17 @"cellIdentifier17"
#define CELL_IDENTIFIER18 @"cellIdentifier18"
#define CELL_IDENTIFIER19 @"cellIdentifier19"

#define CELL_IDENTIFIER25 @"cellIdentifier25"


#define NOT_ORDERED @"Not ordered"
#define DEFAULT_DATE @"01-01-1970"

#define NO_ATTACHMENT_FOUND @""

#define STATUS @"1"
#define STATUS_DELETE @"9"


//============New webservices as per New Database changes=============
#define SAMASTER_NEW @"sync2/samaster.php?"
#define ARCOMMENT_NEW @"sync2/arcoment.php?"
#define ARHISDET_NEW @"sync2/arhisdet.php?"
#define ARPAYMENT_NEW @"sync2/arpaymnt.php?"
#define ARHISHEA_NEW @"sync2/arhishea.php?"
#define ARTRANS_NEW @"sync2/artrans.php?"
#define ARMASTER_NEW @"sync2/armaster.php?"
#define PODETAIL_NEW @"sync2/podetail.php?"
#define SAHISBUD_NEW @"sync2/sahisbud.php?"
#define SMCUSPRO_NEW @"sync2/smcuspro.php?"
#define SODETAIL_NEW @"sync2/sodetail.php?"
#define SOHEADER_NEW @"sync2/soheader.php?"
#define SOQUODET_NEW @"sync2/soquodet.php?"
#define SOQUOHEA_NEW @"sync2/soquohea.php?"
#define SYSCONT_NEW @"sync2/syscont.php?"
#define SYSDESC_NEW @"sync2/sysdesc.php?"
#define SYSDISCT_NEW @"sync2/sysdisct.php?"
#define SYSISTAX_NEW @"sync2/sysistax.php?"
#define PUSHNOTIFICATION_WS @"ios_push/device_tokens.php?"
#define SYSLOGON_NEW @"sync2/syslogon.php?"
#define SMDAYTIM_NEW @"sync2/smdaytim.php?"
#define SACNTRCT_NEW @"sync2/sacntrct.php?"
#define MEMBER_WS_NEW @"sync2/members.php?"


//https://www.scmcentral.com.au/webservices_rcf/

#define LOG_IT
#ifdef LOG_IT
#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#	define DLog(...)
#endif


@class StartStartViewController;
@class LoginViewController;
@class RootViewController;





@interface AppDelegate : UIResponder <UIApplicationDelegate,ReachabilityRequest_delegate>{
    FMDatabase *database;
    FMDatabaseQueue *databaseQueue;
    
    //dispatch_queue_t backgroundQueueForApplication;
    FMDatabaseQueue *dbQueueLoginList;
    
    int counterForLoadDB;
    BOOL isNetworkConnected;
    
    NSMutableArray *arrOfStatusMessages;
    
    int counterForStatusMessages;
    
    NSString *strNumOfflineQuoteMessages;
    
    BOOL isServerOffline;
    NSString  *strFirstTime;
    
    int cntCurrentMessageIndex;
    
    NSCharacterSet *onlyNumbersCharacterSet;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TermsAndConditionVC *termsAndConditionVC;

@property (strong, nonatomic) StartStartViewController *startViewController;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (nonatomic, retain) RootViewController *rootViewController;
@property (nonatomic, assign) int intCurrentMenuId;
@property (nonatomic, assign) int intCurrentSubMenuId;
@property (nonatomic, assign) int intEditQuoteProduct;
@property (nonatomic, retain) FMDatabase *database;
@property (nonatomic, retain) FMDatabaseQueue *databaseQueue;
//--Changes for IOS8
//@property (readwrite, strong, nonatomic) __attribute__((NSObject)) dispatch_queue_t backgroundQueueForApplication;
//@property (readwrite, strong, nonatomic)  __attribute__((NSObject)) dispatch_queue_t backgroundQueueForApplication;
@property (nonatomic, assign) dispatch_queue_t backgroundQueueForApplication;
//@property (readwrite, strong, nonatomic) __attribute__((NSObject)) dispatch_queue_t backgroundQueueForApplication;
@property (nonatomic, assign) int counterForStatusMessages;
@property (nonatomic, retain) NSString *strLastAppSyncDate;
@property (nonatomic,retain)  NSString *strEmailIdOfLoggedInUser;
@property (nonatomic,retain) NSString *strNumOfflineQuoteMessages;
@property (nonatomic,assign) BOOL isNetworkConnected;
@property (nonatomic,assign)  BOOL isServerOffline;
@property (nonatomic, retain) HJObjManager *obj_Manager;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;
- (NSURL *)applicationDocumentsDirectory;

#pragma mark - Custom Methods
+ (AppDelegate*)getAppDelegateObj;

- (void)createSyncFolder;
+ (BOOL) validateEmail: (NSString *) EmailID;
+ (BOOL) validateBlank: (NSString *) text;
+ (CGSize)getRotatedViewSize:(UIView *)view;
+ (void)pullUpPushDownViews:(UIView *)view ByRect:(CGRect)frame;
+ (NSString *)getFileFromLocalDirectory:(NSString *)resourceName Type:(NSString *)type;
+ (NSString *)getFileFromDocumentsDirectory:(NSString *)resourceName;
- (void)downloadFileToCacheFromURL:(NSString *)strURL;
- (void)loadLocalDB;
- (void)callWSForLoginList;
+ (int)calcDaysBetweenTwoDate:(NSDate *)fromDate ToDate:(NSDate *)toDate;
+ (NSString *)getCurrentYear;
+ (NSString *)getCurrentMonth;
- (NSString *)setDateInAustrailianFormat:(NSString *)strDate;
- (NSString *)setDateOnlyInAustrailianFormat:(NSString *)strDate;
- (NSString *)getTradingTerms:(NSString *)strTradingTermCode;
+ (NSInteger)getCurrentDayOfWeek;
+ (NSString *)getCurrentDayOfWeek2;
+ (BOOL)removeFolderFromDocumentsDirectory:(NSString *)resourceName;
- (NSString *)getMacAddress;
- (void)callGetEmailIdOfloggedInUser:(FMDatabase *)db;
- (BOOL)allowNumbersOnly:(NSString *)string;
- (BOOL)allowFloatingNumbersOnly:(NSString *)string;
- (CABasicAnimation *)fadeInFadeOutAnimation;
- (CABasicAnimation *)fadeInFadeOutAnimationForStatusBar;
- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString;
- (NSString *)dateStringForAPIUsingDate:(NSDate *)date;
- (NSString *)dateStringForAPIUsingUnixDate:(NSDate *)date;
- (void)startSpinAnimation:(UIButton *)btn;
- (void)stopSpinAnimation:(UIButton *)btn;
- (void)removefileFromCache:(NSString *)strURL;
- (NSString *)getFileFromCacheDir:(NSString *)strURL;

-(NSString *)SendEmail:(NSMutableDictionary *)dictHeader aryProducts:(NSMutableArray *)aryProducts strHtmlName:(NSString *)strHtmlName;
-(NSString *)SendEmailForSpecialProduct:(NSMutableDictionary *)dictHeader aryProducts:(NSMutableArray *)aryProducts strHtmlName:(NSString *)strHtmlName;
//-(NSString *)GeneratePDF:(NSString *)strHtml viewTodis1:(UIView *)vw;
-(BOOL)CheckIfDemoApp;
-(void)ConvertStringIntoCurrencyFormat:(id )VieToShow;
-(NSString *)HandleHTMLCharacters:(NSString *)strRequest;
 +(NSString *)getServiceURL:(id)strServiceName;
-(NSString*)showFloatFormatValue:(float)VieToShowfloat;

-(NSString*)ConvertStringIntoCurrencyFormatNew:(float)VieToShowfloat;



@end
