//
//  NSObject+UIViewMyCustomView.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "NSObject+UIViewMyCustomView.h"

@implementation UIView (MyCustomView)

//Remove all subviews
-(void)removeAllSubviews{
    
    for (UIView *subview in [self subviews]) {
        [subview removeFromSuperview];
    }
}

//Remove all Gestures
-(void)removeAllGestures{
    
    NSArray * arrGestStackScroll = [self gestureRecognizers];
    
    //Remove Gesture
    for (UIGestureRecognizer *recognizer in arrGestStackScroll)
    {
        [self removeGestureRecognizer:recognizer];
    }
    
}

@end
