//
//  CurrentOrderViewControllerDetail.h
//  Blayney
//
//  Created by Subodh on 9/30/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
@interface CurrentOrderViewControllerDetail : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    NSArray *arrHeaderLabels;
    NSMutableArray *arrProducts;
    NSInteger selectedSectionIndex;
    NSInteger selectedIndex;
    NSMutableDictionary *dictHeaderDetails;
    NSMutableDictionary *dictProductDetails;

     SEFilterControl *segControl;
    
}
@property (nonatomic,retain) NSString *strOrderNum;
@property (weak, nonatomic) IBOutlet UIView *vwContentView;
@property (nonatomic, retain) MBProgressHUD *spinner;
@end
