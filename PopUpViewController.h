//
//  PopUpViewController.h
//  Blayney
//
//  Created by Pooja Mishra on 15/10/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SetCustomPopupDelegate
- (void)setCustomPopupValue:(NSString *)strValue;
@end

@interface PopUpViewController : UIViewController{
    NSMutableArray *arrPopupData;
    id<SetCustomPopupDelegate> __unsafe_unretained  delegate;
}
@property(nonatomic,retain)NSMutableArray *arrPopupData;
@property(unsafe_unretained)id<SetCustomPopupDelegate>delegate;
@property (nonatomic, retain) NSIndexPath *lastIndexPath;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblPopUp;
@end
