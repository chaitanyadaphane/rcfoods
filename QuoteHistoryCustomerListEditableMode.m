//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "QuoteHistoryCustomerListEditableMode.h"
#import "CustomCellGeneral.h"
#import "CustomerTransactionViewController.h"
#import "AddQuoteViewController.h"
#import "CustomerOrderFormViewController.h"
#import "OfflineAddQuoteViewController.h"

#define QUOTE_HISTORY_CUSTOMER_LIST_WS @"quote/custquote.php?"
#define QUOTE_DEBTOR_DETAILS_WS @"quote/debtordetails_quote.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface QuoteHistoryCustomerListEditableMode ()
{
    BOOL _loadingInProgress;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;
@end

@implementation QuoteHistoryCustomerListEditableMode
@synthesize filteredListContentEdit,arrCustomer,arrCustomerForWebservices,spinner,tblQuoteList,isSearchMode,localSpinner,lastIndexPath,mainDict;

@synthesize searchingBar,vwResults,vwContentSuperview,vwNoResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"viewDidLoad QuoteHistoryCustomerListEditableMode");
    
    filteredListContentEdit = [[NSMutableArray alloc] init];
    arrCustomer = [[NSMutableArray alloc] init];
    
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    
    //First page
    currPage = 1;
    totalCount = 0;
    recordNumber = 0;
    
    operationQueue = [NSOperationQueue new];
    
    backgroundQueueForCustomerListEdit = dispatch_queue_create("com.nanan.myscmipad.bgqueueForCustomerListEdit", NULL);
    
    
    [localSpinner startAnimating];
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            
            FMResultSet *res;
            @try {
                res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM armaster"] ;
                
                if (!res)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
            }
            @catch (NSException *e) {
                [res close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
                
                NSLog(@"%@",strErrorMessage);
            }
            
            if ([res next]) {
                totalCount = [[[res resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [res close];
            
            [self callGetCustomerListFromDB:db];
            
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
            [localSpinner stopAnimating];
        }];
    }];
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblQuoteList = nil;
    self.searchingBar = nil;
    self.vwResults = nil;
    self.vwContentSuperview = nil;
    self.vwNoResults = nil;
    self.localSpinner = nil;
}

- (void)doAfterDataFetched{
    if([arrCustomer count] < totalCount){
        
        [tblQuoteList reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        
        recordNumber += ROWS_PER_PAGE;
        
    }
    else{
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [self.filteredListContentEdit count];
    }
	else
	{
        return [self.arrCustomer count];
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:7];
    }
    
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    if (isSearchMode)
	{
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[filteredListContentEdit objectAtIndex:[indexPath row]] objectForKey:@"Name"]];
        cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[filteredListContentEdit objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
    }
	else
	{
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Name"]];
        cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
    }
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    //Edit Quote Entry Module
    if (menuId == 1){
        
        if (subMenuId == 1) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[AddQuoteViewController class]]) {
                
                //cell.btnDetailView.hidden = true;
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
        
        if (subMenuId == 3) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[OfflineAddQuoteViewController class]]) {
                
                //cell.btnDetailView.hidden = true;
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    //Quote Module
    if (menuId == 1){
        
        //Quote History Enquiry
        if (subMenuId == 1 ) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[AddQuoteViewController class]]) {
                
                CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                int newRow = [indexPath row];
                int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                
                if(newRow != oldRow)
                {
                    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                    oldCell.accessoryType = UITableViewCellAccessoryNone;
                    lastIndexPath = indexPath;
                }
                
                
                if (isSearchMode)
                {
                    NSString *strCode = [[[filteredListContentEdit objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [self callGetViewQuoteDetailsDetailsFromDB:strCode];
                }
                else{
                    NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [self callGetViewQuoteDetailsDetailsFromDB:strCode];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                    object:self
                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
            }
            
            
        }
    }
    
    //Offline quote
    if (subMenuId == 3 ) {
        
        int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
        
        int indexOfPreviousController =  indexOfCurrentController -1;
        
        id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
        
        if ([classOfPreviousController isKindOfClass:[OfflineAddQuoteViewController class]]) {
            
            CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
            int newRow = [indexPath row];
            int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
            
            if(newRow != oldRow)
            {
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
                lastIndexPath = indexPath;
            }
            
            if (isSearchMode)
            {
                NSString *strCode = [[[filteredListContentEdit objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                [self callGetViewQuoteDetailsDetailsFromDB:strCode];
                
            }
            else{
                NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                [self callGetViewQuoteDetailsDetailsFromDB:strCode];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
        }
    }
    
}



#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrCustomer count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetCustomerListFromDB:db];
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
    
}

- (void) didFinishLoadingMoreSampleData
{
	_loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContentEdit removeAllObjects]; // First clear the filtered array.
    
    [operationQueue cancelAllOperations];
    
    if ([scope isEqualToString:@"Debtor Name"] && ![searchText isEqualToString:@""]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerListByDebtorName:db DebtorName:searchText];
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContentEdit count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblQuoteList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
                
                
            }];
        }];
        
    }
    else if ([scope isEqualToString:@"Debtor Number"] && ![searchText isEqualToString:@""]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerListByDebtorCode:db DebtorCode:searchText];
                
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContentEdit count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblQuoteList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
                
                
            }];
        }];
    }
    
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblQuoteList reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        self.isSearchMode = YES;
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblQuoteList reloadData];
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

#pragma mark - Database Calls
-(void)callGetCustomerListFromDB:(FMDatabase *)db{
    strWebserviceType = @"DB_QUOTE_HISTORY";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    NSString *strMember = [prefs objectForKey:@"members"];
    NSString *username = [salesType substringFromIndex:1];
    NSString *strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];

    FMResultSet *rs;
    
    @try {
        
        if ( [strMember caseInsensitiveCompare:ADMIN] == NSOrderedSame ||  [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch,BRANCH as Branch,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3 FROM armaster ORDER BY NAME LIMIT ?,?",[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch,BRANCH as Branch,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3 FROM armaster WHERE BRANCH = ? ORDER BY NAME LIMIT ?,?",strBranchList,[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        }
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrCustomer addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}
-(void)callGetCustomerListByDebtorName:(FMDatabase *)db DebtorName:(NSString *)debtorName{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    NSString *username = [NSString stringWithFormat:@"%@",[salesType substringFromIndex:1]];
    NSString *strMember = [prefs objectForKey:@"members"];
    NSString *strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];

    FMResultSet *rs;
    
    @try {
        
       if ( [strMember caseInsensitiveCompare:ADMIN] == NSOrderedSame ||  [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE NAME LIKE ?",[NSString stringWithFormat:@"%%%@%%", debtorName]];
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE BRANCH = ? AND NAME LIKE ?",strBranchList,[NSString stringWithFormat:@"%%%@%%", debtorName]];
        }
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContentEdit addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
}
-(void)callGetCustomerListByDebtorCode:(FMDatabase *)db DebtorCode:(NSString *)debtorCode{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
     NSString *strMember = [prefs objectForKey:@"members"];
    NSString *username = [NSString stringWithFormat:@"%@",[salesType substringFromIndex:1]];
    NSString *strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];

    
    FMResultSet *rs;
    
    @try {
        
        if ( [strMember caseInsensitiveCompare:ADMIN] == NSOrderedSame ||  [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE CODE LIKE ?",[NSString stringWithFormat:@"%%%@%%", debtorCode]];
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE BRANCH = ? AND CODE LIKE ?",strBranchList,[NSString stringWithFormat:@"%%%@%%", debtorCode]];
        }
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContentEdit addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

-(void)callGetViewQuoteDetailsDetailsFromDB:(NSString *)strCode{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db) {
        @try {
            
            mainDict = [[NSMutableDictionary alloc] init];
            
            //Get Customer Details
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM armaster WHERE CODE = ?",strCode];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSDictionary *dictDataArmaster = nil;
            if ([rs1 next]) {
                
                dictDataArmaster = [rs1 resultDictionary];
                
                [mainDict setValue:[dictDataArmaster objectForKey:@"CODE"] forKey:@"Debtor"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"NAME"] forKey:@"Name"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS1"] forKey:@"Address1"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS2"] forKey:@"Address2"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS3"] forKey:@"Address3"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS1"] forKey:@"DelAddress1"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS2"] forKey:@"DelAddress2"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS3"] forKey:@"DelAddress3"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SUBURB"] forKey:@"Suburb"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"POST_CODE"] forKey:@"PostCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"COUNTRY"] forKey:@"Country"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_NAME"] forKey:@"DelName"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_SUBURB"]  forKey:@"DelSuburb"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_POST_CODE"] forKey:@"DelPostCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_COUNTRY"] forKey:@"DelCountry"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"CONTACT1"] forKey:@"Contact"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"BRANCH"] forKey:@"Branch"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"PRICE_CODE"] forKey:@"PriceCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SALESMAN"]  forKey:@"Salesman"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SALES_BRANCH"] forKey:@"SalesBranch"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"TRADING_TERMS"] forKey:@"TradingTerms"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"EXCHANGE_CODE"] forKey:@"ExchangeCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"CHARGE_TYPE"] forKey:@"Chargetype"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"EXPORT_DEBTOR"] forKey:@"ExportDebtor"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"RATE"] forKey:@"ChargeRate"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"HOLD"] forKey:@"Held"];
                [mainDict setValue:@"0" forKey:@"Numboxes"];
                [mainDict setValue:@"N" forKey:@"Direct"];
                NSString *str_current_Date = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
                [mainDict setValue:str_current_Date forKey:@"DateRaised"];
                
            }
            
            [rs1 close];
            
            
            //Get Customer Details
            FMResultSet *rs2 = [db executeQuery:@"SELECT `DEBTOR`, `CONTACT`, `DEL_INST1` as DelInst1, `DEL_INST2` as DelInst2, `CUST_ORDER` as CustOrderno, `WAREHOUSE` as Warehouse from `soheader` WHERE DEBTOR = ?",strCode];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs2 next]) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[rs2 resultDictionary]];
                [mainDict addEntriesFromDictionary:dict];
                
            }
            
            [rs2 close];
            
            //Year & Period raised
            FMResultSet *rs3 = [db executeQuery:@"SELECT SA_PERIOD,SA_FIN_YEAR FROM syscont WHERE RECNUM = 1"];
            
            if (!rs3)
            {
                [rs3 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs3 next]) {
                NSDictionary *dictData = [rs3 resultDictionary];
                [mainDict setValue:[dictData objectForKey:@"SA_PERIOD"] forKey:@"PeriodRaised"];
                [mainDict setValue:[dictData objectForKey:@"SA_FIN_YEAR"] forKey:@"YearRaised"];
                dictData=nil;
            }
            
            
            NSArray *arrOldDeliveryRun = [[NSArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                           @"1",@"key",
                                                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN1"],@"value",
                                                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"2",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN2"],@"value",
                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"3",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN3"],@"value",
                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"4",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN4"],@"value",
                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"5",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN5"],@"value",
                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"6",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN6"],@"value",
                                           nil],
                                          [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"7",@"key",
                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN7"],@"value",
                                           nil],
                                          nil];
            
            
            NSNumber *numCurrDayOfWeek = [NSNumber numberWithInteger: [AppDelegate getCurrentDayOfWeek]];
            int currDayOfWeek = [numCurrDayOfWeek intValue];
            
            NSArray *arrFrom = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(currDayOfWeek, [arrOldDeliveryRun count] - currDayOfWeek)];
            
            NSArray *arrTo = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(0, currDayOfWeek)];
            
            NSArray *arrNewDeliveryRun = [arrFrom arrayByAddingObjectsFromArray:arrTo];
            
            NSString *strDeliveryRun = nil;
            BOOL isDeliveryRunFound = FALSE;
            
            NSString *strDeliveryDate = nil;
            NSString *strDropSequence = nil;
            
            for (int i = 0; i < [arrNewDeliveryRun count]; i++) {
                
                if (![[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"] isEqualToString:@""]) {
                    
                    //Execute one time
                    isDeliveryRunFound = TRUE;
                    strDeliveryRun = [[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"];
                    
                    int newDay = [[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"key"] intValue];
                    
                    //DropSequence
                    NSString *strDropSeqKey = [NSString stringWithFormat:@"DROP_SEQUENCE%d",newDay];
                    strDropSequence = [dictDataArmaster objectForKey:strDropSeqKey];
                    strDropSeqKey = nil;
                    
                    NSDate *today = [NSDate date];
                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    [gregorian setLocale:[NSLocale currentLocale]];
                    
                    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
                    
                    if (newDay > currDayOfWeek) {
                        [nowComponents setWeek: [nowComponents week]];//Current Week
                    }
                    else{
                        [nowComponents setWeek: [nowComponents week] + 1]; //Next week
                    }
                    
                    newDay = ((newDay + 7) % 7) + 1; // Transforming so that monday = 2 and sunday = 1
                    
                    [nowComponents setWeekday:newDay];
                    [nowComponents setHour:8]; //8a.m.
                    [nowComponents setMinute:0];
                    [nowComponents setSecond:0];
                    
                    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
                    
                    strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:beginningOfWeek];
                    //NSLog(@"strDeliveryDate %@",strDeliveryDate);
                    
                    
                    break;
                }
            }
            
            if (!isDeliveryRunFound) {
                strDeliveryRun = [dictDataArmaster objectForKey:@"DELIVERY_RUN"];
                
                NSDate *now = [NSDate date];
                int daysToAdd = 1;
                NSDate *nextDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
                strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:nextDate];
                //NSLog(@"strDeliveryDate %@",strDeliveryDate);
                
                //DropSequence
                strDropSequence = [dictDataArmaster objectForKey:@"DROP_SEQUENCE"];
                
            }
            
            [mainDict setValue:strDeliveryRun forKey:@"DeliveryRun"];
            [mainDict setValue:strDeliveryDate forKey:@"DeliveryDate"];
            [mainDict setValue:strDropSequence forKey:@"DropSequence"];
            
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    
}

/*-(void)callGetViewQuoteDetailsDetailsFromDB:(NSString *)strCode{
 
 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
 [databaseQueue   inDatabase:^(FMDatabase *db) {
 
 FMResultSet *rs;
 FMResultSet *rs2;
 
 NSMutableDictionary *mainDict;
 @try {
 
 BOOL isEverythingFine = YES;
 
 //Get Customer Details
 rs = [db executeQuery:@"SELECT CODE as Debtor, NAME as Name, DEL_NAME as DelName, ADDRESS1 as Address1, ADDRESS2 as Address2, ADDRESS3 as Address3, DEL_ADDRESS1 as DelAddress1, DEL_ADDRESS2 as DelAddress2, DEL_ADDRESS3 as DelAddress3,BRANCH as Branch FROM armaster WHERE CODE = ?",strCode];
 
 if (!rs)
 {
 isEverythingFine = NO;
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
 }
 
 if ([rs next]) {
 mainDict = [[NSMutableDictionary alloc] initWithDictionary:[rs resultDictionary]];
 }
 
 [rs close];
 
 //Get Customer Details
 rs2 = [db executeQuery:@"SELECT RELEASE_TYPE as ReleaseType, DEL_DATE as DeliveryDate, EXPIRY_DATE as ExpiryDate, DEL_INST1 as DelInst1, DEL_INST2 as DelInst2, WAREHOUSE as Warehouse FROM soquohea WHERE DEBTOR= ? ",strCode];
 
 if (!rs2)
 {
 isEverythingFine = NO;
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
 }
 
 if ([rs2 next]) {
 NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[rs2 resultDictionary]];
 [mainDict addEntriesFromDictionary:dict];
 
 }
 
 [rs2 close];
 
 
 if (isEverythingFine) {
 [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
 object:self
 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
 }
 
 
 
 }
 @catch (NSException* e) {
 // rethrow if not one of the two exceptions above
 [rs close];
 
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
 [alert show];
 
 
 }
 }];
 }*/

#pragma mark - WS calls
-(void)callWSGetCustomerList:(int)pageIndex
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    
    strWebserviceType = @"WS_QUOTE_HISTORY";
    //Fetch last sync date
    NSString *last_sync_date = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"armaster"];
    NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d&last_syncDate=%@&username=%@",pageIndex,ROWS_PER_PAGE,last_sync_date,salesType];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_HISTORY_CUSTOMER_LIST_WS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewQuoteDetailsDetails:(NSString *)debtorNum
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DEBTOR_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"debtor=%@",debtorNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DEBTOR_DETAILS_WS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
//    NSLog(@"%@", dictResponse);
    
    
    [spinner removeFromSuperview];
    
    NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_HISTORY"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Flag"]){
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                if(!self.arrCustomerForWebservices){
                    arrCustomerForWebservices = [[NSMutableArray alloc] init];
                }
                
                if (_loadMoreFooterView == nil) {
                    [self setupLoadMoreFooterView];
                }
                
                //Total Records
                totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"];
                    [arrCustomerForWebservices addObject:dict];
                }
                else{
                    
                    self.arrCustomerForWebservices = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"];
                    [arrCustomerForWebservices addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"]];
                }
                
                [tblQuoteList reloadData];
                
                
                if([arrCustomerForWebservices count] < totalCount){
                    [self repositionLoadMoreFooterView];
                    
                    // Dismiss loading footer
                    [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                    
                    currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                    
                    //Next page
                    currPage ++;
                    
                    recordNumber += ROWS_PER_PAGE;
                    
                }
                else{
                    [_loadMoreFooterView removeFromSuperview];
                    _loadMoreFooterView=nil;
                }
            }
            //False
            else{
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            }
        }
        //No more updates
        else if([[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"UpdateInfo"]){
            
            
            //Total Records
            totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
            
            
            dispatch_async(backgroundQueueForCustomerListEdit, ^(void) {
                
                [dbQueueCustomerListEdit inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try
                    {
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"armaster" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        //Commit here
                        [db commit];
                        
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                        
                        //[db close];
                    }
                    
                    @finally {
                        
                        [self callGetCustomerListFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        
                        
                        @try
                        {
                            
                            [tblQuoteList reloadData];
                            
                            [spinnerObj hide:YES];
                            
                            if([arrCustomer count] < totalCount){
                                [self repositionLoadMoreFooterView];
                                
                                // Dismiss loading footer
                                [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                                
                                currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                                
                                //Next page
                                currPage ++;
                                
                                recordNumber += ROWS_PER_PAGE;
                                
                            }
                            else{
                                [_loadMoreFooterView removeFromSuperview];
                                _loadMoreFooterView=nil;
                            }
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                        
                    });
                    
                }];
                
            });
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_QUOTE_DEBTOR_DETAILS"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            [spinnerObj hide:YES];
            
            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Data"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
        
    }
    
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}
#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            break;
        }
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


@end
