//
//  SalesOrderProductDetailsViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 24/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "SalesOrderProductDetailsViewController.h"
#import "CustomCellGeneral.h"
#import "AsyncImageView.h"
#import "KGModal.h"
#import "MJProductImgViewController.h"
#import "QuantityCell.h"

#define TAG_100 100
#define TAG_200 200

#define PRODUCT_GET_AVAILABLE_WS @"salesorder/getavailable.php?"
#define ALLOW_SPECIAL_PRICE @"Specials"
#define PRODUCT_IMAGE_URL @"https://www.scmcentral.com.au/webservices_rcf/productimages/Large/"

@interface SalesOrderProductDetailsViewController ()
{
    NSString *strTYPE1;
    NSString *strOriginalPrice;
    NSString *strOriginalMargin;

    bool firstTime;
    
    BOOL isSpecialValChanged;
    NSString *isProductFromSacontrct;
    MJProductImgViewController *prodImgViewControllerObj;
    
    UIImage *productImg;
}
@end

BOOL isCostMarginVisible;
@implementation SalesOrderProductDetailsViewController
@synthesize arrProductLabels,dictProductDetails,lblProductName,tblDetails,strQuantityOrdered,delegate,productIndex,strPrice,strDebtor,isTaxApplicable,operationQueue,strCode,strWebserviceType,strAvailable,strWarehouse,isEditable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        firstTime = YES;
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    isCostMarginVisible = YES;
    isProductFromSacontrct= @"N";
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"SalesOrderProductDetail" Type:@"plist"];
    
    // Build the array from the plist
    arrProductLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    lblProductName.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"Description"] trimSpaces]];
    
    self.isTaxApplicable = YES;
    tax = 0;
    
    operationQueue = [[NSOperationQueue alloc] init];
    //self.isAllValid = YES;
    
    self.strWarehouse = [dictProductDetails objectForKey:@"Warehouse"];
    
    if([dictProductDetails objectForKey:@"Available"])
    self.strAvailable = [dictProductDetails objectForKey:@"Available"];
    
    if(isEditable)
    {
        if([[dictProductDetails objectForKey:@"isNewProduct"] isEqualToString:@"1"])
        {
            [self actionReset];
        }
        else
        {
            price = [[dictProductDetails objectForKey:@"Price"] floatValue];
            strOriginalPrice = [NSString stringWithFormat:@"%f",price];

            totalWithOutTax = [[dictProductDetails objectForKey:@"Gross"] floatValue];
            totalWithTax= [[dictProductDetails objectForKey:@"ExtnPrice"] floatValue];
            tax = [[dictProductDetails objectForKey:@"Tax"] floatValue];
            
            
            strQuantityOrdered = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"QuantityOrdered"]];
            
            if([[dictProductDetails objectForKey:@"Available"] isKindOfClass:[NSNull class]])
            {
                strAvailable = [NSString stringWithFormat:@"0.00"];
            }
            else
            {
                strAvailable = [NSString stringWithFormat:@"0.00"];;
            }
        }
    }
    else
    {
        [self actionReset];
    }
    
    margin = [self calcMargin];
    // Set Original Margin value
    strOriginalMargin=[NSString stringWithFormat:@"%f",margin];
    
    if([dictProductDetails objectForKey:@"StockCode"]){
        self.strCode = [dictProductDetails objectForKey:@"StockCode"];
    }
    else if([dictProductDetails objectForKey:@"Item"]){
        self.strCode = [dictProductDetails objectForKey:@"Item"];
    }
    
    //Change by subhu
    //Initially set empty value
     lastPriceSales = @"--";
    

    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
//        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
//            //Margin Calculation
//            price = [strPrice floatValue];
//            strOriginalPrice = [NSString stringWithFormat:@"%f",price];
//            
//            //                float cost = [[dictProductDetails objectForKey:@"Cost"] floatValue];
//            //1stDec
//            float cost = [[dictProductDetails objectForKey:@"Cost"] floatValue];
//            margin = ((price - cost) / price) * 100;
            
//            NSURL *url = [NSURL URLWithString: [AppDelegate getServiceURL:@"webservices_rcf/salesorder/getlastpurchase.php"]];
//            
//            ASIFormDataRequest *request1 = [ASIFormDataRequest requestWithURL:url];
//            __weak ASIFormDataRequest *request = request1;
//            [request addPostValue:strDebtor forKey:@"customer"];
//            [request addPostValue:self.strCode forKey:@"stock_code"];
//            [request startAsynchronous];
//            [request setCompletionBlock:^{
//                // Use when fetching text data
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    [spinner hide:YES];
//                    [spinner removeFromSuperview];
//                });
//                
//                NSString *responseString = [request responseString];
//                
//                NSDictionary *dictResponse = [XMLReader dictionaryForXMLString:responseString error:nil];
//                
//                if([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"LastPurchase"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"])
//                {
//                    lastPriceSales = [NSString stringWithFormat:@"%@",[[[[[dictResponse objectForKey:@"Response"] objectForKey:@"LastPurchase"] objectForKey:@"Data"] objectForKey:@"Price"] objectForKey:@"text"]];
//                }
//                
//                [tblDetails reloadData];
//            }];
//            [request setFailedBlock:^{
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    [spinner hide:YES];
//                    [spinner removeFromSuperview];
//                    [tblDetails reloadData];
//                });
//            }];
//            
            //[self callWSGetAvailable];
//        }];
        [tblDetails reloadData];
    }else
    {
        
        
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                ;;
                [self callGetTaxInfoFromDB:db];
                [self callGetDiscountFromDB:db];
            }];
        }];
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    isCostMarginVisible = NO;
    isSpecialValChanged = NO;
 strQuantityOrdered = @"0";
    //Change by subhu
//    @try {
//        if(firstTime)
//        {
//            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
//
//            NSMutableArray *arr = [arrProductLabels objectAtIndex:1];
//            
//            [tempArray addObject:[NSIndexPath indexPathForRow:0 inSection:1]];
//            
//            [arr removeObjectAtIndex:0];
//            
//            
//            [tblDetails reloadData];
//            firstTime = NO;
//        }
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Eror::%@",exception.description);
//    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.lblProductName = nil;
    self.tblDetails = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Database calls

-(void)callGetPriceFromDB:(FMDatabase *)db
{
    //NSLog(@"strDebtor %@",strDebtor);
    @try {
        
        //NSLog(@"SELECT PRICE_CODE FROM armaster WHERE CODE = %@adsafasfasfa",strDebtor);
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",[strDebtor trimSpaces]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    if ([dictProductDetails objectForKey:@"Price1"]) {
                        self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                    }
                    else if([dictProductDetails objectForKey:@"Price"]){
                        self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                    }
                    
                }
                    break;
                case 1:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
                    
                case 2:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                }
                    break;
                    
                case 3:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                }
                    break;
                    
                case 4:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                }
                    break;
                    
                case 5:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                }
                    break;
                    
                    
                default:{
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
            }
            
            if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"]) {
                if([dictProductDetails objectForKey:@"Price"]){
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                }
                
            }
            
        }
        
        [rs1 close];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}


-(void)callGetDiscountFromDB:(FMDatabase *)db
{
    @try
    {
        float discount = 0;
        float priceCode = 0;
        
        //For Last Price
        FMResultSet *rs = [db executeQuery:@"SELECT PRICE FROM arhisdet WHERE STOCK_CODE = ? AND DEBTOR = ? ORDER BY DATE_RAISED desc LIMIT 1",self.strCode,strDebtor];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs next])
        {
            lastPriceSales = [rs stringForColumn:@"PRICE"];
        }
        [rs close];
        
        
        //For Price Code - NEW LOGIC SACNTRCT
        
        FMResultSet *rs001 = [db executeQuery:@"SELECT AGREED_SELL_PRC,MARGIN_PCT  FROM sacntrct WHERE CODE = ? AND STOCK_CODE = ? AND FROM_DATE <  DATETIME('now')  and TO_DATE > DATETIME('now') LIMIT 1",strDebtor,strCode];
        
        
        if (!rs001){
            [rs001 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        NSDictionary *dictSacntrct = [NSDictionary new];
        if ([rs001 next])
        {
            dictSacntrct = [rs001 resultDictionary];
        }
        [rs001 close];

        if (dictSacntrct.count > 0) {
            self.strPrice = [dictSacntrct objectForKey:@"AGREED_SELL_PRC"];
            margin = [[dictSacntrct objectForKey:@"MARGIN_PCT"]floatValue];
            isProductFromSacontrct = @"Y";
        }
        else{
            //For Price Code
            FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE trim(CODE) = ?",[strDebtor trimSpaces]];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs1 next]) {
                NSDictionary *dictData = [rs1 resultDictionary];
                switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                    case 0:
                    {
                        if ([dictProductDetails objectForKey:@"Price1"])
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                        }
                        else if([dictProductDetails objectForKey:@"Price"])
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                    }
                        break;
                        
                    case 2:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
                    }
                        break;
                        
                    case 3:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
                    }
                        break;
                        
                    case 4:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
                    }
                        break;
                        
                    case 5:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
                    }
                        break;
                        
                        
                    default:{
                        priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                    }
                        break;
                        
                }
                
                if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
                {
                    if([dictProductDetails objectForKey:@"Price"])
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                    }
                    
                }
                
            }
            else
            {
                priceCode = 0;
            }
            
            [rs1 close];
        
        }
        

        //For Dicount
        
        FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", [dictProductDetails objectForKey:@"StockCode"], [strDebtor trimSpaces]];
        
        FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ''  AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", [dictProductDetails objectForKey:@"StockCode"]];
        
        float DiscountVal1 = 0;
        float DiscountVal2 = 0;
        
        float discountValwithKey1 = 0;
        float discountValwithKey2 = 0;
        
        
        float discountValwithOutKey1 = 0;
        float discountValwithOutKey2 = 0;
        
        
        if ([rs21 next])
        {
            NSDictionary *dictData = [rs21 resultDictionary];
            [rs21 close];
            strTYPE1 = [dictData objectForKey:@"TYPE1"];
            
            if ([strTYPE1 isEqualToString:@"P"]) {
                priceCode = priceCode - (priceCode * 0.05);
                discountValwithKey1 = priceCode;
            }
            else if ([strTYPE1 isEqualToString:@"M"]){
                
                NSString *discount1Val = [dictData objectForKey:@"DISCOUNT1"];
                NSString *strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                
                margin =[[dictData objectForKey:@"DISCOUNT1"]floatValue];
                
                CGFloat sellprice = ([strCost floatValue] / ( 1- ([discount1Val floatValue] / 100)));
                strPrice = [NSString stringWithFormat:@"%.2f",sellprice];
                return;
            }
            else{
                discountValwithKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                discountValwithKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                
                if (discountValwithKey1 == 0) {
                    DiscountVal1 =discountValwithKey2;
                }
                else if (discountValwithKey2 == 0){
                    DiscountVal1 = discountValwithKey1;
                }
                else{
                    if (discountValwithKey1 > discountValwithKey2 ) {
                        DiscountVal1 = discountValwithKey2;
                    }
                    else{
                        DiscountVal1 = discountValwithKey1;
                    }
                }
            }
        }
        
        
        if ([rs221 next]) {
            NSDictionary *dictData = [rs221 resultDictionary];
            [rs221 close];
            strTYPE1 = [dictData objectForKey:@"TYPE1"];
            if ([strTYPE1 isEqualToString:@"P"]) {
                priceCode = priceCode - (priceCode * 0.05);
                discountValwithOutKey1 = priceCode;
            }
            else if ([strTYPE1 isEqualToString:@"M"]){
                
                NSString *discount1Val = [dictData objectForKey:@"DISCOUNT1"];
                NSString *strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                
                margin =[[dictData objectForKey:@"DISCOUNT1"]floatValue];
                
                CGFloat sellprice = ([strCost floatValue] / ( 1- ([discount1Val floatValue] / 100)));
                strPrice = [NSString stringWithFormat:@"%.2f",sellprice];
                return;
            }
            else{
                discountValwithOutKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                discountValwithOutKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                
                if (discountValwithOutKey1 == 0) {
                    DiscountVal2 =discountValwithOutKey2;
                }
                else if (discountValwithOutKey2 == 0){
                    DiscountVal2 = discountValwithOutKey1;
                }
                else{
                    if (discountValwithOutKey1 > discountValwithOutKey2 ) {
                        DiscountVal2 = discountValwithOutKey2;
                    }
                    else{
                        DiscountVal2 = discountValwithOutKey1;
                    }
                }
            }
        }
     
        if (DiscountVal1 == 0) {
            discount = DiscountVal2;
        }
        
        
        else if (DiscountVal2 == 0){
            discount = DiscountVal1;
        }
        else{
            if (DiscountVal1 > DiscountVal2 ){
                discount = DiscountVal2;
            }
            else{
                discount = DiscountVal1;
            }
        }
        
        if (discount == 0) {
            strTYPE1 = ALLOW_SPECIAL_PRICE;
        }
        
        if(priceCode > 0 && discount > 0)
        {
            if(priceCode > discount)
            {
                self.strPrice = [NSString stringWithFormat:@"%f",discount];
            }
            else
            {
                self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
            }
            [tblDetails reloadData];
        }
        else if (priceCode == 0)
        {
            self.strPrice = [NSString stringWithFormat:@"%f",discount];
            [tblDetails reloadData];
        }
        else if (discount == 0)
        {
            self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
            [tblDetails reloadData];
        }
        else
        {
            self.strPrice = @"0";
            [tblDetails reloadData];
        }
        
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    [tblDetails reloadData];
}

-(void)callGetTaxInfoFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT TAX_CODE1 FROM sysistax WHERE CODE = ?",[dictProductDetails objectForKey:@"StockCode"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            if ([[dictData objectForKey:@"TAX_CODE1"] intValue] == -1) {
                self.isTaxApplicable = NO;
            }
            else{
                self.isTaxApplicable = YES;
            }
            
            dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    [tblDetails reloadData];
}

-(void)callGetAvailabilityFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT AVAILABLE FROM samaster WHERE CODE = ? AND WAREHOUSE = ?",strCode,[dictProductDetails objectForKey:@"Warehouse"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            //NSLog(@"[strQuantityOrdered intValue] %@",strQuantityOrdered);
//            if ([strQuantityOrdered intValue] > [[dictData objectForKey:@"AVAILABLE"] intValue]) {
//                
//                NSString *strMessage = [NSString stringWithFormat:@"Order quanity is more than Available(%@)",[dictData objectForKey:@"AVAILABLE"]];
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                [alert show];
//                
//                strMessage = nil;
//            }
//            else{
//                [self actionSetPrice];
//            }
            
            // 31stMarch2015 : Change request
            [self actionSetPrice];

            
            dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
}

-(void)callUpdateAvailableToDB:(FMDatabase *)db{
    
    @try {
        BOOL y = [db executeUpdate:@"UPDATE samaster SET AVAILABLE = ? WHERE WAREHOUSE = ? AND CODE = ?",strAvailable,strWarehouse,strCode];
        
        if (!y)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
        
    }
    
}

#pragma mark -
#pragma mark Table view data source and delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width,30)];
    
    if(section == 1)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (isCostMarginVisible) {
            [button setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
        }
        else{
            [button setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        }
        
        //headerView.frame.size.width/2.0
        [button setFrame:CGRectMake(435.0, 3.0, 30.0, 30.0)];
        button.tag = section;
        button.hidden = NO;
        [button setBackgroundColor:[UIColor clearColor]];
        [button addTarget:self action:@selector(showCostMargin:) forControlEvents:UIControlEventTouchDown];
        [headerView addSubview:button];
        
    }
    
    return headerView;
}
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    //Change by subhu
    if (indexPath.section == 0)
    {
        if (indexPath.row == 1) {
            return 80;
        }
    }
    else if (indexPath.section==1)
    {
        if(!isCostMarginVisible && ([indexPath row] == 0 ||  [indexPath row] == 1))
        {
            return 0;
        }else{
            return 60;
        }
    }
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return [arrProductLabels count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)[arrProductLabels objectAtIndex:section] count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    static NSString *CellIdentifier17 = CELL_IDENTIFIER17;
    
    CustomCellGeneral *cell = Nil;
    NSArray *nib;
    if ([indexPath section] == 0)
    {
        switch ([indexPath row]) {
            case 0:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                if ([dictProductDetails objectForKey:@"StockCode"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"StockCode"]];
                }
            }
                break;
                
            case 1:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier17];
                AsyncImageView *imgVw;
                
                UIButton *showProdBbtn;
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:17];
                    imgVw  = [[AsyncImageView alloc]initWithFrame:CGRectMake(tableView.frame.size.width - 80, 10, 60, 60)];
                    imgVw.backgroundColor =[UIColor clearColor];
                    imgVw.contentMode = UIViewContentModeScaleAspectFill;
                    [cell addSubview:imgVw];
                    
                    showProdBbtn = [[UIButton alloc]initWithFrame:CGRectMake(tableView.frame.size.width-80, 0, 80, 80)];
                    showProdBbtn.backgroundColor = [UIColor clearColor];
                    [showProdBbtn addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
                    [cell addSubview:showProdBbtn];
                    
                    [showProdBbtn sendSubviewToBack:imgVw];
                    
                    
                }
                
                
                
                if ([dictProductDetails objectForKey:@"Picture_field"] )
                {
                    
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PRODUCT_IMAGE_URL,[dictProductDetails objectForKey:@"Picture_field"]];
                    imgVw.imageURL =[NSURL URLWithString:imageUrl];
                    NSLog(@"Picture_field %@",imageUrl);
                    if(imgVw.image == Nil)
                    {
                        imgVw.image =[UIImage imageNamed:@"place_holder.png"];
                    }
                    
                }
                else
                {
                    imgVw.image = [UIImage imageNamed:@"place_holder.png"];
                }
                
                productImg = imgVw.image;
            }
                break;
                
            default: {
                
            }
                break;
        }
    }
    else if ([indexPath section] == 1)
    {
        //Change by subhu
        switch ([indexPath row]) {
                
                //--margin
            case 0:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                NSLog(@"strMargin::::::::%f",margin);
                if(isCostMarginVisible)
                    cell.lblValue.text = [NSString stringWithFormat:@"%.2f%@",margin,@"%"];
                else
                {
                    cell.lblTitle.text = @"";
                    cell.lblValue.text = @"";
                }
            }
                break;
            case 1:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                //Cost -> AverageCost
                if ([dictProductDetails objectForKey:@"Cost"]) {
                    if(isCostMarginVisible)
                    {
                        cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Cost"] floatValue]];
                    }
                    else
                    {
                        cell.lblTitle.text = @"";
                        cell.lblValue.text = @"";
                    }
                }
            }
                break;
                
                //Available
            case 2:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[strAvailable floatValue]];
            }
                break;
                
                //Quantity
            case 3:
            {
                QuantityCell  * qtycell = (QuantityCell *)[tableView dequeueReusableCellWithIdentifier:@"QuantityCell"];
                //UITextField *textQuantity;
                if (qtycell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"QuantityCell" owner:self options:nil];
                    qtycell = [nib objectAtIndex:0];
                    
//                    qtycell.backgroundColor = [UIColor lightTextColor];
                    
                    [qtycell.minusbtn addTarget:self action:@selector(getMinusQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [qtycell.plusbtn addTarget:self action:@selector(getPlusQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                qtycell.quantityTextfield.placeholder = ENTER_QUANTITY_HERE_MESSAGE;
                qtycell.quantityTextfield.text =[NSString stringWithFormat:@"%d",[strQuantityOrdered integerValue]];
                
                return qtycell;
            }
                break;
                
                //Price
            case 4:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:9];
                    
                    cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
                    cell.txtGlowingValue.delegate = self;
                }
                NSString *tempStr =[dictProductDetails objectForKey:@"PREF_SUPPLIER"];
                NSString *prefSupplierStr = [tempStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if ([prefSupplierStr isEqualToString:@"REPAN1"] || [prefSupplierStr isEqualToString:@"REPAN2"] || [prefSupplierStr isEqualToString:@"REPAN3"] || [prefSupplierStr isEqualToString:@"REPAN4"]) {
                    //1stDec, 2014
                    if (margin < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"]){
                        cell.txtGlowingValue.enabled = YES;
                        
                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        cell.txtGlowingValue.tag = TAG_200;
                        //***********ketaki-code modified************//
                        
                        
                        if ([strPrice isEqualToString:@""])
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                        }
                        else
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                            // [activityView stopAnimating];
                            // activityView.hidden=true;
                            spinner.mode = MBProgressHUDModeIndeterminate;
                            [spinner removeFromSuperview];
                        }
                        
                    }
                    else{
                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        cell.txtGlowingValue.tag = TAG_200;
                        
                        if ([strPrice isEqualToString:@""])
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                            //[activityView startAnimating];
                            
                        }
                        else
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                            //[activityView stopAnimating];
                            // activityView.hidden=true;
                            spinner.mode = MBProgressHUDModeIndeterminate;
                            [spinner removeFromSuperview];
                        }
                        
                    }
                    
                }
                else{
                    cell.txtGlowingValue.enabled = YES;
                    
                    cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                    cell.txtGlowingValue.tag = TAG_200;
                    //***********ketaki-code modified************//
                    
                    if ([strPrice isEqualToString:@""])
                    {
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                        // [activityView startAnimating];
                    }
                    else
                    {
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        //                        [activityView stopAnimating];
                        //                        activityView.hidden=true;
                        spinner.mode = MBProgressHUDModeIndeterminate;
                        [spinner removeFromSuperview];
                    }
                    //cell.txtGlowingValue.text = [N
                }
            }
                break;
                
                //LastPrice
                //            case 5:
                //            {
                //                //Change by subhu
                //                if ([lastPrice isEqualToString:@"--"]) {
                //                    cell.lblValue.text = [NSString stringWithFormat:@"%@",lastPrice];
                //                }
                //                else{
                //                    cell.lblValue.text = [NSString stringWithFormat:@"$ %@",lastPrice];
                //                }
                //            }
                //                break;
                
                // LastSold
            case 5:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                if ([dictProductDetails objectForKey:@"LastSold"] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_APP_DATE] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_SERVER_DATE]) {
                    cell.lblValue.text = [dictProductDetails objectForKey:@"LastSold"];
                    cell.lblValue.text = [NSString stringWithFormat:@"%@", [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"LastSold"]]];
                }
                else
                    cell.lblValue.text = @"--";
                
            }
                break;
                
                //Excl
            case 6:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",totalWithOutTax];
            }
                break;
                
                //Tax
            case 7:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                if (isTaxApplicable) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",tax];
                }
                else{
                    cell.lblValue.text = @"NA";
                }
            }
                break;
                
                //ExtnIncl
            case 8:
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblTitle.text = @"";
                cell.lblValue.text = @"";
                
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",totalWithTax];
            }
                break;
            default:{
                
            }
                break;
        }
        
        NSLog(@"cell.lblValue.text : %@",cell.lblValue.text);
    }
    else if ([indexPath section] == 2)
    {
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        
        if (cell == nil) {
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            cell = [nib objectAtIndex:4];
        }
        
        switch ([indexPath row]) {
                
            case 0:
                
            {
                if ([dictProductDetails objectForKey:@"q0"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q0"]];
                }
                
            }
                
                break;
                
                //-1
            case 1:
                
            {
                
                if ([dictProductDetails objectForKey:@"q1"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q1"]];
                }
            }
                
                break;
                
                //-2
            case 2:
                
            {
                
                if ([dictProductDetails objectForKey:@"q2"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q2"]];
                    
                }
            }
                
                break;
                
                //-3
            case 3:
                
            {
                
                if ([dictProductDetails objectForKey:@"q3"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q3"]];
                }
            }
                
                break;
                
                
                //-4
            case 4:
            {
                if ([dictProductDetails objectForKey:@"q4"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q4"]];
                }
            }
                break;
                
            default:{
            }
                break;
        }
        
    }
    
    
    
    
    NSArray *array=[arrProductLabels objectAtIndex:[indexPath section]];
    // NSLog(@"array : %@",array);
    
    if([[[array objectAtIndex:[indexPath row]] objectForKey:@"Label"] isEqualToString:@"Price"])
    {
        cell.lblTitle.text = @"";
        
        if([strTYPE1 isEqualToString:@"A"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Special Price($)"];
        }
        else if([strTYPE1 isEqualToString:@"N"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"List Price($)"];
        }
        else
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Contract Price($)"];
        }
    }
    else
    {
        if (indexPath.section==1 && ([indexPath row] == 0 ||  [indexPath row] == 1))
        {
            if(isCostMarginVisible)
            {
                cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            }else
                cell.lblTitle.text = @"";
        }else
        {
            cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        }
    }
    
    return cell;
    
}
/*
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    static NSString *CellIdentifier17 = CELL_IDENTIFIER17;
    static NSString *CellIdentifier18 = CELL_IDENTIFIER18;
    
    CustomCellGeneral *cell;
    
    NSArray *nib;
    
    if ([indexPath section] == 0)
    {
        switch ([indexPath row]) {
            case 0:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                if ([dictProductDetails objectForKey:@"StockCode"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"StockCode"]];
                }
            }
                break;
                
            case 1:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier17];
                AsyncImageView *imgVw;
                
                UIButton *showProdBbtn;
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:17];
                    imgVw  = [[AsyncImageView alloc]initWithFrame:CGRectMake(tableView.frame.size.width - 80, 10, 60, 60)];
                    imgVw.backgroundColor =[UIColor clearColor];
                    imgVw.contentMode = UIViewContentModeScaleAspectFill;
                    [cell addSubview:imgVw];
                    
                    showProdBbtn = [[UIButton alloc]initWithFrame:CGRectMake(tableView.frame.size.width-80, 0, 80, 80)];
                    showProdBbtn.backgroundColor = [UIColor clearColor];
                    [showProdBbtn addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
                    [cell addSubview:showProdBbtn];
                    
                    [showProdBbtn sendSubviewToBack:imgVw];
                }
                
                if ([dictProductDetails objectForKey:@"Picture_field"] )
                {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PRODUCT_IMAGE_URL,[dictProductDetails objectForKey:@"Picture_field"]];
                    imgVw.imageURL =[NSURL URLWithString:imageUrl];
                    NSLog(@"Picture_field %@",imageUrl);
                    if(imgVw.image == Nil)
                    {
                        imgVw.image =[UIImage imageNamed:@"place_holder.png"];
                    }
                    
                }
                else
                {
                    imgVw.image = [UIImage imageNamed:@"place_holder.png"];
                }
                
                productImg = imgVw.image;
            }
                break;
                
            default: {
                
            }
                break;
        }
    }
    else if ([indexPath section] == 1)
    {
        //Change by subhu
        if ([indexPath row] == 3) {
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier18];
            
            if (cell == nil) {
                nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:18];
                
                cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
                cell.txtGlowingValue.delegate = self;
            }
            // minus button
            [[cell.minusQuantityBtn layer] setBorderWidth:0.5f];
            [[cell.minusQuantityBtn layer] setBorderColor:[UIColor blackColor].CGColor];
            [cell.minusQuantityBtn addTarget:self action:@selector(getMinusQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
            
            // Plus button
            [[cell.plusQuantityBtn layer] setBorderWidth:0.5f];
            [[cell.plusQuantityBtn layer] setBorderColor:[UIColor blackColor].CGColor];
            [cell.plusQuantityBtn addTarget:self action:@selector(getPlusQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
        }
        else if ([indexPath row] == 4)
        {
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
            
            if (cell == nil) {
                nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:9];
                
                cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
                cell.txtGlowingValue.delegate = self;
            }
            
        }
        else{
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            
            if (cell == nil) {
                nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:4];
            }
        }
        
        cell.lblTitle.text = @"";
        cell.lblValue.text = @"";
        
        switch ([indexPath row]) {
                
                //--margin
            case 0:
            {
                NSLog(@"strMargin::::::::%f",margin);
                if(isCostMarginVisible)
                    cell.lblValue.text = [NSString stringWithFormat:@"%.2f%@",margin,@"%"];
                else
                {
                    cell.lblTitle.text = @"";
                    cell.lblValue.text = @"";
                }
            }
                break;
            case 1:
            {
                //Cost -> AverageCost
                if ([dictProductDetails objectForKey:@"Cost"]) {
                    if(isCostMarginVisible)
                    {
                        cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Cost"] floatValue]];
                    }
                    else
                    {
                        cell.lblTitle.text = @"";
                        cell.lblValue.text = @"";
                    }
                }
            }
                break;
                
                //Available
            case 2:
            {
                cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[strAvailable floatValue]];
            }
                break;
                
                //Quantity
            case 3:
            {
                cell.txtGlowingValue.placeholder = ENTER_QUANTITY_HERE_MESSAGE;
                cell.txtGlowingValue.tag = TAG_100;
                if(strQuantityOrdered.length == 0)
                {
                    strQuantityOrdered = @"0";
                }
                cell.txtGlowingValue.text = strQuantityOrdered;
                cell.txtGlowingValue.userInteractionEnabled=([strPrice isEqualToString:@""])?false:true;
            }
                break;
                
                //Price
            case 4:
            {
                NSString *tempStr =[dictProductDetails objectForKey:@"PREF_SUPPLIER"];
                NSString *prefSupplierStr = [tempStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if ([prefSupplierStr isEqualToString:@"REPAN1"] || [prefSupplierStr isEqualToString:@"REPAN2"] || [prefSupplierStr isEqualToString:@"REPAN3"] || [prefSupplierStr isEqualToString:@"REPAN4"]) {
                    //1stDec, 2014
                    if (margin < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"]){
                        cell.txtGlowingValue.enabled = YES;
                        
                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        cell.txtGlowingValue.tag = TAG_200;
 
                        
                        if ([strPrice isEqualToString:@""])
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                        }
                        else
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                            // [activityView stopAnimating];
                            // activityView.hidden=true;
                            spinner.mode = MBProgressHUDModeIndeterminate;
                            [spinner removeFromSuperview];
                        }
                        
                        //                         cell.txtGlowingValue.text=([strPrice isEqualToString:@""])?[NSString stringWithFormat:@"%@",strPrice]:[NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        // cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        
                        
                    }
                    else{
                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        cell.txtGlowingValue.tag = TAG_200;
 
                        if ([strPrice isEqualToString:@""])
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                            //[activityView startAnimating];
                            
                        }
                        else
                        {
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                            //[activityView stopAnimating];
                            // activityView.hidden=true;
                            spinner.mode = MBProgressHUDModeIndeterminate;
                            [spinner removeFromSuperview];
                        }
                        
                    }
                    
                }
                else{
                    cell.txtGlowingValue.enabled = YES;
                    
                    cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                    cell.txtGlowingValue.tag = TAG_200;
                    //***********ketaki-code modified*****
                    
                    if ([strPrice isEqualToString:@""])
                    {
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",strPrice];
                        // [activityView startAnimating];
                    }
                    else
                    {
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        //                        [activityView stopAnimating];
                        //                        activityView.hidden=true;
                        spinner.mode = MBProgressHUDModeIndeterminate;
                        [spinner removeFromSuperview];
                    }
                    //cell.txtGlowingValue.text = [N
                }
            }
                break;
                
                //LastPrice
                //            case 5:
                //            {
                //                //Change by subhu
                //                if ([lastPrice isEqualToString:@"--"]) {
                //                    cell.lblValue.text = [NSString stringWithFormat:@"%@",lastPrice];
                //                }
                //                else{
                //                    cell.lblValue.text = [NSString stringWithFormat:@"$ %@",lastPrice];
                //                }
                //            }
                //                break;
                
                // LastSold
            case 5:
            {
                if ([dictProductDetails objectForKey:@"LastSold"] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_APP_DATE] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_SERVER_DATE]) {
                    cell.lblValue.text = [dictProductDetails objectForKey:@"LastSold"];
                    cell.lblValue.text = [NSString stringWithFormat:@"%@", [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"LastSold"]]];
                }
                else
                    cell.lblValue.text = @"--";
            }
                break;
                
                //Excl
            case 6:
            {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",totalWithOutTax];
            }
                break;
                
                //Tax
            case 7:
            {
                if (isTaxApplicable) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",tax];
                }
                else{
                    cell.lblValue.text = @"NA";
                }
            }
                break;
                
                //ExtnIncl
            case 8:
            {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",totalWithTax];
            }
                break;
            default:{
                
            }
                break;
        }
        
        NSLog(@"cell.lblValue.text : %@",cell.lblValue.text);
    }
    else if ([indexPath section] == 2)
    {
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        
        if (cell == nil) {
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            cell = [nib objectAtIndex:4];
        }
        
        switch ([indexPath row]) {
                
            case 0:
                
            {
                if ([dictProductDetails objectForKey:@"q0"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q0"]];
                }
                
            }
                
                break;
                
                //-1
            case 1:
                
            {
                
                if ([dictProductDetails objectForKey:@"q1"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q1"]];
                }
            }
                
                break;
                
                //-2
            case 2:
                
            {
                
                if ([dictProductDetails objectForKey:@"q2"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q2"]];
                    
                }
            }
                
                break;
                
                //-3
            case 3:
                
            {
                
                if ([dictProductDetails objectForKey:@"q3"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q3"]];
                }
            }
                
                break;
                
                
                //-4
            case 4:
            {
                if ([dictProductDetails objectForKey:@"q4"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q4"]];
                }
            }
                break;
                
            default:{
            }
                break;
        }
        
    }
    
    
    
    
    NSArray *array=[arrProductLabels objectAtIndex:[indexPath section]];
    // NSLog(@"array : %@",array);
    
    if([[[array objectAtIndex:[indexPath row]] objectForKey:@"Label"] isEqualToString:@"Price"])
    {
        cell.lblTitle.text = @"";
        
        if([strTYPE1 isEqualToString:@"A"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Special Price($)"];
        }
        else if([strTYPE1 isEqualToString:@"N"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"List Price($)"];
        }
        else
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Contract Price($)"];
        }
    }
    else
    {
        if (indexPath.section==1 && ([indexPath row] == 0 ||  [indexPath row] == 1))
        {
            if(isCostMarginVisible)
            {
                cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            }else
                cell.lblTitle.text = @"";
        }else
        {
            cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        }
    }
    
    return cell;
    
}
*/

#pragma mark - Text Field Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == TAG_100 || textField.tag == TAG_200) {
        [tblDetails setContentOffset:CGPointMake(0, 150) animated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == TAG_100) {
        return [[AppDelegate getAppDelegateObj] allowNumbersOnly:string];
    }
    
    if (textField.tag == TAG_200) {
        return [[AppDelegate getAppDelegateObj] allowFloatingNumbersOnly:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    }
    
    return TRUE;
}

/*
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[CustomCellGeneral class]])
    {
        CustomCellGeneral *cell = (CustomCellGeneral*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tblDetails indexPathForCell:cell];
        
        [self.tblDetails scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    }
    
    return YES;
}
*/

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    //self.isAllValid = YES;
    
    if (textField.tag == TAG_100) {
        self.strQuantityOrdered = textField.text;
    }
    else if (textField.tag == TAG_200) {
        
        self.strPrice = textField.text;
        price = [self.strPrice floatValue];
        
    }
    
    [self CalculateData];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag == 2) {
        [tblDetails setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return YES;
}

#pragma mark - Custom Methods

-(IBAction)getMinusQuantityAction:(id)sender
{
  
        int quantity = [strQuantityOrdered intValue];
        quantity --;
        if( quantity >= 0)// quantity should not be negative i.e less than 0
        {
            strQuantityOrdered = [NSString stringWithFormat:@"%d",quantity];
        }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:1];

    [self.tblDetails beginUpdates];
    [self.tblDetails reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tblDetails endUpdates];
}

-(IBAction)getPlusQuantityAction:(id)sender
{
    if (strQuantityOrdered.length > 0)
    {
        int quantity = [strQuantityOrdered intValue];
        quantity ++;
        strQuantityOrdered = [NSString stringWithFormat:@"%d",quantity];
        [self.tblDetails reloadData];
    }
}

-(IBAction)showImage:(id)sender
{
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    
    if (prodImgViewControllerObj) {
        [self dismissPopupViewControllerWithanimationType:nil];
        prodImgViewControllerObj = nil;
    }
    prodImgViewControllerObj = [[MJProductImgViewController alloc] initWithNibName:@"MJProductImgViewController" bundle:nil];
    
    prodImgViewControllerObj.view.frame = CGRectMake(10, 10, 300, 300);
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PRODUCT_IMAGE_URL,[dictProductDetails objectForKey:@"Picture_field"]];
    
    prodImgViewControllerObj.prod_img_url =imageUrl;
    // .....
    prodImgViewControllerObj.productImg.imageURL = [NSURL URLWithString:imageUrl];
    [contentView addSubview:prodImgViewControllerObj.view];
    //    [self presentPopupViewController:prodImgViewControllerObj
    //                       animationType:3];
    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
    
}

-(float)calcMargin{
    
    NSString *strCost = @"0.00";
    if(isEditable)
    {
        if([[dictProductDetails objectForKey:@"isNewProduct"] isEqualToString:@"1"])
        {
            if([dictProductDetails objectForKey:@"Cost"])
            {
                strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                NSLog(@"price %f",price);
                CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
                return  (rounded_up);
            }
        }
        else
        {
            if([dictProductDetails objectForKey:@"Cost"])
            {
                strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                NSLog(@"price %f",price);
                CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
                return  (rounded_up);
            }
            else
            {
                strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                NSLog(@"price %f",price);
                CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
                return  (rounded_up);
            }
        }
    }
    else
    {
        if([dictProductDetails objectForKey:@"Cost"])
        {
            strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
            NSLog(@"price %f",price);
            //Change by subhu
           // return  (((price -  [strCost floatValue]) / price) * 100);
            if (price>0)
            {
                CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
                return  (rounded_up);
            }else
                return  0.00;
        }
    }
    CGFloat roundedVal = ceilf(((price -  [strCost floatValue]) / price) * 100);
    return  (roundedVal);
    
    
//    NSString *strCost = @"0.00";
//    if ([dictProductDetails objectForKey:@"Cost"]) {
//        strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
//        NSLog(@"price %f",price);
//        return  (((price -  [strCost floatValue]) / price) * 100);
//    }
//    
//    return  (((price -  [strCost floatValue]) / price) * 100);
    
}

- (void)actionReset{
    //Sales order -  Customer order check
    if ([AppDelegate getAppDelegateObj].intCurrentMenuId == 0 && [AppDelegate getAppDelegateObj].intCurrentSubMenuId == 2){
        //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
        
        if ([dictProductDetails objectForKey:@"QuantityOrdered"]) {
            self.strQuantityOrdered = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"QuantityOrdered"]];
        }
        else
        {
            self.strQuantityOrdered = @"0";
        }
        
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callGetDiscountFromDB:db];
                [self callGetTaxInfoFromDB:db];
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                
                if (isTaxApplicable) {
                    tax = [[dictProductDetails objectForKey:@"Tax"] floatValue];
                }
                
                totalWithOutTax = [[dictProductDetails objectForKey:@"Gross"] floatValue];
                totalWithTax= [[dictProductDetails objectForKey:@"ExtnPrice"] floatValue];
                price = [strPrice floatValue];
                
                if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                    //[self callWSGetAvailable];
                }
                else{
                    self.strAvailable = [dictProductDetails objectForKey:@"Available"];
                    [tblDetails reloadData];
                }
            }];
            
        }];
        
    }
    else{
        
        if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:@"Specials"])
        {
            NSLog(@"From Specials");


            if([[dictProductDetails objectForKey:@"Type1"] isEqualToString:@"A"])
            {
                price = [[dictProductDetails objectForKey:@"Discount"] floatValue];
            }
            else if([[dictProductDetails objectForKey:@"Type1"] isEqualToString:@"P"])
            {
                float priceCode = 0;
                FMDatabase *db = [[FMDatabase alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];

                [db open];
                //For Price Code
                FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",[strDebtor trimSpaces]];
                
                if (!rs1)
                {
                    [rs1 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs1 next]) {
                    NSDictionary *dictData = [rs1 resultDictionary];
                    
                    //NSLog(@"dictData %@",dictData);
                    
                    switch ([[dictData objectForKey:@"PRICE_CODE"] intValue])
                    {
                        case 0:
                        {
                            if ([dictProductDetails objectForKey:@"Price1"])
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                                
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                            }
                            else if([dictProductDetails objectForKey:@"Price"])
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                            }
                            
                        }
                            break;
                        case 1:
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                        }
                            break;
                            
                        case 2:
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                        }
                            break;
                            
                        case 3:
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                        }
                            break;
                            
                        case 4:
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                        }
                            break;
                            
                        case 5:
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                        }
                            break;
                            
                            
                        default:{
                            priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                        }
                            break;
                            
                            
                            
                    }
                    
                    if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
                    {
                        if([dictProductDetails objectForKey:@"Price"])
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                            //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                        }
                        
                    }
                    
                    float discount_value = [[dictProductDetails objectForKey:@"Discount"] floatValue];
                    
                    float finalPrice = priceCode - ((priceCode *discount_value)/100);
                    price = finalPrice;
                    
                    
                }
                else
                {
                    priceCode = 0;
                }
                
                [rs1 close];
                [db close];
            }
            
            
                //NSLog(@"%@",dictProductDetails);
                @try {
                    float discount = 0;
                    float priceCode = 0;
                    FMDatabase *db = [[FMDatabase alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];

                    [db open];

                    //For Price Code
                    FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE trim(CODE) = ?",[strDebtor trimSpaces]];
                    
                    if (!rs1)
                    {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    
                    if ([rs1 next]) {
                        NSDictionary *dictData = [rs1 resultDictionary];
                        
                        //NSLog(@"dictData %@",dictData);
                        
                        switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                            case 0:
                            {
                                if ([dictProductDetails objectForKey:@"Price1"])
                                {
                                    priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                                    
                                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                                }
                                else if([dictProductDetails objectForKey:@"Price"])
                                {
                                    priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                                }
                                
                            }
                                break;
                            case 1:
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                            }
                                break;
                                
                            case 2:
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                            }
                                break;
                                
                            case 3:
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                            }
                                break;
                                
                            case 4:
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                            }
                                break;
                                
                            case 5:
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                            }
                                break;
                                
                                
                            default:{
                                priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                            }
                                break;
                                
                                
                                
                        }
                        
                        if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
                        {
                            if([dictProductDetails objectForKey:@"Price"])
                            {
                                priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                                //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                            }
                            
                        }
                        
                    }
                    else
                    {
                        priceCode = 0;
                    }
                    
                    [rs1 close];
                    
                    //For Dicount
                    
                    //        FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND date() between DATE_FROM1 and DATE_TO1",[dictProductDetails objectForKey:@"StockCode"], [strDebtor trimSpaces]];
                    //        hgj
                    //        if (!rs21)
                    //        {
                    //            [rs21 close];
                    //            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    //            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    //        }
                    //
                    //        if ([rs21 next]) {
                    
                    //            NSDictionary *dictData = [rs21 resultDictionary];
                    //            [rs21 close];
                    //            discount = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    //            strTYPE1 = [dictData objectForKey:@"TYPE1"];
                    //            //self.strPrice = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"DISCOUNT1"]];
                    //        }
                    //        else{
                    //            //[self callGetPriceFromDB:db];
                    //            discount = 0;
                    //        }
                    
                    //For Dicount
                    FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND date() between DATE_FROM1 and DATE_TO1", [dictProductDetails objectForKey:@"StockCode"], [strDebtor trimSpaces]];
                    
                    FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = '' AND date() between DATE_FROM1 and DATE_TO1", [dictProductDetails objectForKey:@"StockCode"]];
                    
                    float discount1 = 0;
                    float discount2 = 0;
                    
                    
                    if ([rs21 next])
                    {
                        NSDictionary *dictData = [rs21 resultDictionary];
                        [rs21 close];
                        strTYPE1 = [dictData objectForKey:@"TYPE1"];
                        
                        if ([strTYPE1 isEqualToString:@"P"]) {
                            priceCode = priceCode - (priceCode * 0.05);
                            discount1 = priceCode;
                        }
                        else{
                            discount1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                        }
                    }
                    if ([rs221 next]) {
                        NSDictionary *dictData = [rs221 resultDictionary];
                        [rs221 close];
                        strTYPE1 = [dictData objectForKey:@"TYPE1"];
                        if ([strTYPE1 isEqualToString:@"P"]) {
                            priceCode = priceCode - (priceCode * 0.05);
                            discount2 = priceCode;
                        }
                        else{
                            discount2 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                        }
                    }
                    
                    
                    if (discount1 == 0) {
                        discount =discount2;
                    }
                    
                    
                    else if (discount2 == 0){
                        discount = discount1;
                    }
                    else{
                        if (discount1 > discount2 ) {
                            discount = discount2;
                        }
                        else{
                            discount = discount1;
                        }
                    }
                    
                    if (discount == 0) {
                        strTYPE1 = ALLOW_SPECIAL_PRICE;
                    }
                    
                    
                    
                    //
                    //        if ([rs21 next])
                    //        {
                    //            NSDictionary *dictData = [rs21 resultDictionary];
                    //            [rs21 close];
                    //            discount = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    //            strTYPE1 = [dictData objectForKey:@"TYPE1"];
                    //        }
                    //        else
                    //        {
                    //            if ([rs221 next]) {
                    //                NSDictionary *dictData = [rs221 resultDictionary];
                    //                [rs221 close];
                    //                discount = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    //                strTYPE1 = [dictData objectForKey:@"TYPE1"];
                    //                //self.strPrice = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"DISCOUNT1"]];
                    //            }
                    //            else{
                    //                discount = 0;
                    //            }
                    //            
                    //            //[self callGetPriceFromDB:db];
                    //        }
                    
                    
                    
                    
                    if(priceCode > 0 && discount > 0)
                    {
                        if(priceCode > discount)
                        {
                            self.strPrice = [NSString stringWithFormat:@"%f",discount];
                        }
                        else
                        {
                            self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
                        }
                        [tblDetails reloadData];
                    }
                    else if (priceCode == 0)
                    {
                        self.strPrice = [NSString stringWithFormat:@"%f",discount];
                        [tblDetails reloadData];
                    }
                    else if (discount == 0)
                    {
                        self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
                        
                        [tblDetails reloadData];
                    }
                    else
                    {
                        self.strPrice = @"0";
                        
                        [tblDetails reloadData];
                    }
                    
                    
                }@catch (NSException* e) {
                    // rethrow if not one of the two exceptions above
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }];
                    
                }
                [tblDetails reloadData];
        }
        else
        {
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [operationQueue addOperationWithBlock:^{
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetDiscountFromDB:db];
                    [self callGetTaxInfoFromDB:db];
                    [tblDetails reloadData];
                }];
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    spinner.mode = MBProgressHUDModeIndeterminate;
                    [spinner removeFromSuperview];
                    price = [strPrice floatValue];
                    
                    margin = [self calcMargin];
                    
                    strOriginalPrice = [NSString stringWithFormat:@"%f",price];
                    
                    [tblDetails reloadData];

                    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                        //[self callWSGetAvailable];
                    }
                    else{
                        self.strAvailable = [dictProductDetails objectForKey:@"Available"];
                        [tblDetails reloadData];
                    }
                    
                }];
                
            }];
        }
        
    }
    
}

- (IBAction)actionAvailabilityCheck:(id)sender{
    
    [[self.view findFirstResponder] resignFirstResponder];
    
    if ([strQuantityOrdered floatValue] <= 0) {
        
        //self.isAllValid = NO;
        
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:QUANTITY_GREATER_THAN_0
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        return;
    }
    
    ///--For Special
    if([dictProductDetails objectForKey:@"FromSpecials"])
    {
        if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:ALLOW_SPECIAL_PRICE])
        {
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
                
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
                totalWithTax = totalWithOutTax + tax;
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                     margin = [self calcMargin];
                    if(margin <= 0)
                    {
                        [AJNotificationView showNoticeInView:self.view
                                                        type:AJNotificationTypeRed
                                                       title:MARGIN_PERCENT_ERROR
                                             linedBackground:AJLinedBackgroundTypeDisabled
                                                   hideAfter:2.5f];
                        
                        [tblDetails reloadData];
                        return;
                    }
                     [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetAvailabilityFromDB:db];
                        margin = [self calcMargin];
                        if(margin <= 0)
                        {
                            [AJNotificationView showNoticeInView:self.view
                                                            type:AJNotificationTypeRed
                                                           title:MARGIN_PERCENT_ERROR
                                                 linedBackground:AJLinedBackgroundTypeDisabled
                                                       hideAfter:2.5f];
                            
                            [tblDetails reloadData];
                            return;
                        }

                        [tblDetails reloadData];
                    }];
                    databaseQueue = nil;
                    
                }
            }
        }
    }
   else if(strTYPE1.length > 0)
    {
//        if ([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"]| [strTYPE1 isEqualToString:ALLOW_SPECIAL_PRICE])
          if ([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"])
        {
            totalWithOutTax = price * [strQuantityOrdered intValue];
            
            //If tax applicable
            if (isTaxApplicable) {
                //Tax is 10% of total
                tax = 0.1 * totalWithOutTax;
            }
            else{
                tax = 0;
            }
            
            totalWithTax = totalWithOutTax + tax;
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
              
                margin = [self calcMargin];
                
              /*   11thNov
               if(margin <= 0)
                {
                    [AJNotificationView showNoticeInView:self.view
                                                    type:AJNotificationTypeRed
                                                   title:MARGIN_PERCENT_ERROR
                                         linedBackground:AJLinedBackgroundTypeDisabled
                                               hideAfter:2.5f];
                    
                    [tblDetails reloadData];
                    return;
                }
               */

                  [self actionSetPrice];
                [tblDetails reloadData];
            }
            else{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
                
            }
        }
        else
        {
            margin = [self calcMargin];
            if (margin <= MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
            {
                [AJNotificationView showNoticeInView:self.view
                                                type:AJNotificationTypeRed
                                               title:MARGIN_PERCENT_ERROR
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                
                [tblDetails reloadData];
                return;
                
            }
            else
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
            
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
                totalWithTax = totalWithOutTax + tax;
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetAvailabilityFromDB:db];
                    }];
                    databaseQueue = nil;
                    
                }
                
                
            }
        }
    }
    else
    {
        margin = [self calcMargin];
        
        
        if(margin <= 0)
        {
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
        }

        //Dont check for Special
   
        if([dictProductDetails objectForKey:@"FromSpecials"])
        {
            if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:@"Specials"])
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
                
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
                totalWithTax = totalWithOutTax + tax;
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    //--Dont check for Avaliable
                    /*
                     FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                     [databaseQueue   inDatabase:^(FMDatabase *db) {
                     [self callGetAvailabilityFromDB:db];
                     }];
                     databaseQueue = nil;
                     */
                    [self actionSetPrice];
                    
                }

            }
        }
       else if (margin < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else
        {
            totalWithOutTax = price * [strQuantityOrdered intValue];
            
            //If tax applicable
            if (isTaxApplicable) {
                //Tax is 10% of total
                tax = 0.1 * totalWithOutTax;
            }
            else{
                tax = 0;
            }
            
            totalWithTax = totalWithOutTax + tax;
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
                [tblDetails reloadData];
            }
            else{
                //--Dont check for Avaliable
                /*
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
                 */
                [self actionSetPrice];
                
            }
            
            
        }
    }
}

-(void)CalculateData
{
    [[self.view findFirstResponder] resignFirstResponder];
    
    if ([strQuantityOrdered floatValue] <= 0) {
        
        //self.isAllValid = NO;
        
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:QUANTITY_GREATER_THAN_0
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        return;
    }
    
    
    //-- For Contract Special
    
    if ([dictProductDetails objectForKey:@"ContractSpecial"]) {
        totalWithOutTax = price * [strQuantityOrdered intValue];
        
        //If tax applicable
        if (isTaxApplicable) {
            //Tax is 10% of total
            tax = 0.1 * totalWithOutTax;
        }
        else{
            tax = 0;
        }
        totalWithTax = totalWithOutTax + tax;
        if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
        {
            margin = [self calcMargin];
            if (strOriginalMargin.floatValue != margin) {
                isSpecialValChanged = YES;
            }
            else{
                isSpecialValChanged = NO;
            }
            
            [self actionSetPrice];
            [tblDetails reloadData];
        }
        else{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetAvailabilityFromDB:db];
                margin = [self calcMargin];
                if (strOriginalMargin.floatValue != margin) {
                    isSpecialValChanged = YES;
                }
                else{
                    isSpecialValChanged = NO;
                }
                
                
                [tblDetails reloadData];
            }];
            databaseQueue = nil;
            
        }
        
        NSLog(@"Original Margin %f",strOriginalMargin.floatValue);
        NSLog(@"New margin %f",margin);
        
        if (isSpecialValChanged == YES) {
            if (margin < MARGIN_MINIMUM_PERCENT )//strOriginalMargin.floatValue > margin ||
            {
                NSLog(@"##### ##### ##### #####");
                NSLog(@"PLEASE CHECK THE CONDITION ----- >");
                [AJNotificationView showNoticeInView:self.view
                                                type:AJNotificationTypeRed
                                               title:MARGIN_PERCENT_ERROR
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                [tblDetails reloadData];
                return;
                
            }
        }
    }
    

    
    ///--For Special
    if([dictProductDetails objectForKey:@"FromSpecials"])
    {
        if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:ALLOW_SPECIAL_PRICE])
        {
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
                
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
              
                
                totalWithTax = totalWithOutTax + tax;
                
              

                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    margin = [self calcMargin];
                    if (strOriginalMargin.floatValue != margin) {
                        isSpecialValChanged = YES;
                    }
                    else{
                        isSpecialValChanged = NO;
                    }
                    
                    if(margin <= 0)
                    {
                        [AJNotificationView showNoticeInView:self.view
                                                        type:AJNotificationTypeRed
                                                       title:MARGIN_PERCENT_ERROR
                                             linedBackground:AJLinedBackgroundTypeDisabled
                                                   hideAfter:2.5f];
                        
                        [tblDetails reloadData];
                        return;
                    }
                    [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetAvailabilityFromDB:db];
                        margin = [self calcMargin];
                        if (strOriginalMargin.floatValue != margin) {
                            isSpecialValChanged = YES;
                        }
                        else{
                            isSpecialValChanged = NO;
                        }
                        
                        if(margin <= 0)
                        {
                            [AJNotificationView showNoticeInView:self.view
                                                            type:AJNotificationTypeRed
                                                           title:MARGIN_PERCENT_ERROR
                                                 linedBackground:AJLinedBackgroundTypeDisabled
                                                       hideAfter:2.5f];
                            
                            [tblDetails reloadData];
                            return;
                        }
                        
                        [tblDetails reloadData];
                    }];
                    databaseQueue = nil;
                    
                }
                
                NSLog(@"Original Margin %f",strOriginalMargin.floatValue);
                NSLog(@"New margin %f",margin);

                if (isSpecialValChanged == YES) {
                    if ( margin < MARGIN_MINIMUM_PERCENT)//strOriginalMargin.floatValue > margin ||
                    {
                        NSLog(@"##### ##### ##### #####");
                        NSLog(@"PLEASE CHECK THE CONDITION ----- >");
                        [AJNotificationView showNoticeInView:self.view
                                                        type:AJNotificationTypeRed
                                                       title:MARGIN_PERCENT_ERROR
                                             linedBackground:AJLinedBackgroundTypeDisabled
                                                   hideAfter:2.5f];
                        [tblDetails reloadData];
                        return;
                        
                    }
                }
            }
        }
    }
    else if(strTYPE1.length > 0)
    {
        //        if ([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"]| [strTYPE1 isEqualToString:ALLOW_SPECIAL_PRICE])
        if ([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"])
        {
            totalWithOutTax = price * [strQuantityOrdered intValue];
            
            //If tax applicable
            if (isTaxApplicable) {
                //Tax is 10% of total
                tax = 0.1 * totalWithOutTax;
            }
            else{
                tax = 0;
            }
            
            totalWithTax = totalWithOutTax + tax;
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                
                margin = [self calcMargin];
               
                
                /* 11thNov
                if(margin <= 0)
                {
                    [AJNotificationView showNoticeInView:self.view
                                                    type:AJNotificationTypeRed
                                                   title:MARGIN_PERCENT_ERROR
                                         linedBackground:AJLinedBackgroundTypeDisabled
                                               hideAfter:2.5f];
                    
                    [tblDetails reloadData];
                    return;
                }
                */
                [self actionSetPrice];
                [tblDetails reloadData];
            }
            else{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
                
            }
        }
        else
        {
            margin = [self calcMargin];
            if (margin < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
            {
                [AJNotificationView showNoticeInView:self.view
                                                type:AJNotificationTypeRed
                                               title:MARGIN_PERCENT_ERROR
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                
                [tblDetails reloadData];
                return;
                
            }
            else
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
                
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
                totalWithTax = totalWithOutTax + tax;
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetAvailabilityFromDB:db];
                    }];
                    databaseQueue = nil;
                    
                }
                
                
            }
        }
    }
    else
    {
        margin = [self calcMargin];
        
        
        if(margin <= 0)
        {
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
        }
        
        //Dont check for Special
        
        if([dictProductDetails objectForKey:@"FromSpecials"])
        {
            if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:@"Specials"])
            {
                totalWithOutTax = price * [strQuantityOrdered intValue];
                
                //If tax applicable
                if (isTaxApplicable) {
                    //Tax is 10% of total
                    tax = 0.1 * totalWithOutTax;
                }
                else{
                    tax = 0;
                }
                
                totalWithTax = totalWithOutTax + tax;
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    [self actionSetPrice];
                    [tblDetails reloadData];
                }
                else{
                    //--Dont check for Avaliable
                    /*
                     FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                     [databaseQueue   inDatabase:^(FMDatabase *db) {
                     [self callGetAvailabilityFromDB:db];
                     }];
                     databaseQueue = nil;
                     */
                    [self actionSetPrice];
                    
                }
                
            }
        }
        else if (margin < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else
        {
            totalWithOutTax = price * [strQuantityOrdered intValue];
            
            //If tax applicable
            if (isTaxApplicable) {
                //Tax is 10% of total
                tax = 0.1 * totalWithOutTax;
            }
            else{
                tax = 0;
            }
            
            totalWithTax = totalWithOutTax + tax;
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
                [tblDetails reloadData];
            }
            else{
                
                //--Dont check for Avaliable
                
                /*
                 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                 [databaseQueue   inDatabase:^(FMDatabase *db) {
                 [self callGetAvailabilityFromDB:db];
                 }];
                 databaseQueue = nil;
                 */
                
                [self actionSetPrice];
                
            }
            
            
        }
    }

}

-(void)actionSetPrice{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    NSString *strTotal = [NSString stringWithFormat:@"%f",totalWithOutTax];
    NSString *strTax = [NSString stringWithFormat:@"%f",tax];
    self.strPrice  = [NSString stringWithFormat:@"%f",price];
    
    [dict setValue:strTotal forKey:@"Gross"];
    [dict setValue:strTax forKey:@"Tax"];
    [dict setValue:strPrice forKey:@"Price"];
    
    NSLog(@"strOriginalPrice:::%@ ::: Price:::%@",strOriginalPrice,strPrice);
    if([strOriginalPrice floatValue] == [strPrice floatValue] )
    {
        [dict setValue:@"N" forKey:@"PriceChnaged"];
        NSLog(@"NOT changed");
        
    }
    else
    {
        [dict setValue:@"Y" forKey:@"PriceChnaged"];
        NSLog(@"Changed ::::");
    }
    
    if ([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"N"]) {
         [self.delegate setPriceOfProduct:totalWithTax ProductIndex:productIndex QuantityOrdered:[strQuantityOrdered intValue] DictCalculation:dict];
    }
    else if ([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"Y"] && [self calcMargin] >= MARGIN_MINIMUM_PERCENT ){
        [self.delegate setPriceOfProduct:totalWithTax ProductIndex:productIndex QuantityOrdered:[strQuantityOrdered intValue] DictCalculation:dict];
    }
    else{
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:MARGIN_PERCENT_ERROR
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        [tblDetails reloadData];
        return;
        
    }

    
//    [self.delegate setPriceOfProduct:totalWithTax ProductIndex:productIndex QuantityOrdered:[strQuantityOrdered intValue] DictCalculation:dict];
}

#pragma mark - WS calls

-(void)callWSGetAvailable{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.strWebserviceType = @"WS_PRODUCT_GET_AVIALABLE_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"stock_code=%@&warehouse=%@",strCode,strWarehouse];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,PRODUCT_GET_AVAILABLE_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];

    [spinner removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_PRODUCT_GET_AVIALABLE_DETAILS"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"]) {
            
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                self.strAvailable = [NSString stringWithFormat:@"%@",[[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] objectForKey:@"Available"] objectForKey:@"text"]];
                
                [operationQueue addOperationWithBlock:^{
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callUpdateAvailableToDB:db];
                    }];
                    databaseQueue = nil;
                    
                }];
                
            }
            else{
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
        else{
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [tblDetails reloadData];
        }];
        
    }
    else if ([strWebserviceType isEqualToString:@"WS_CHECK_AVIALABLITY"]){
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            [self actionSetPrice];
            [tblDetails reloadData];
                
        }
        else if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"False"]) {
        
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@(%@)",[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"message"] objectForKey:@"text"],[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"available"] objectForKey:@"text"]];
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        
        }
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
}

#pragma mark - Custom methods

//Change by Subhu
-(void)showCostMargin:(id)sender{
    @try {
        isCostMarginVisible = (isCostMarginVisible)? NO: YES;
        
        UIButton *btn = (UIButton *)sender;
        
        if(isCostMarginVisible)
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
        }
        else
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        }
        
        [tblDetails reloadData];
    }
    @catch (NSException *exception) {
        NSLog(@"Eror::%@",exception.description);
    }
}

/*
-(void)showCostMargin:(id)sender
{
    isCostMarginVisible = (isCostMarginVisible)? NO: YES;
    
    UIButton *btn = (UIButton *)sender;
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];

    
    [tempArray addObject:[NSIndexPath indexPathForRow:0 inSection:btn.tag]];
    [tempArray addObject:[NSIndexPath indexPathForRow:1 inSection:btn.tag]];

    
    NSMutableArray *arr = [arrProductLabels objectAtIndex:btn.tag];
    
    if(isCostMarginVisible){
        [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
        NSMutableDictionary *dictMargin = [[NSMutableDictionary alloc] init];
        [dictMargin setValue:@"Margin" forKey:@"Label"];
        [arr insertObject:dictMargin atIndex:0];
        
        NSMutableDictionary *dictCost = [[NSMutableDictionary alloc] init];
        [dictCost setValue:@"Cost" forKey:@"Label"];
        [arr insertObject:dictCost atIndex:1];
        
        [tblDetails beginUpdates];
        [tblDetails insertRowsAtIndexPaths:(NSArray *)tempArray withRowAnimation:UITableViewRowAnimationBottom];
        [tblDetails endUpdates];
        
    }
    else{
        [btn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        [arr removeObjectAtIndex:0];
        [arr removeObjectAtIndex:1];


        [tblDetails beginUpdates];
        [tblDetails deleteRowsAtIndexPaths:(NSArray *)tempArray  withRowAnimation:UITableViewRowAnimationFade];
        [tblDetails endUpdates];
        
    }
}*/


@end
