//
//  SalesOrderProductDetailsViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 24/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuantityCell.h"

@protocol SetPriceDelegate
- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation;
@end

@interface SalesOrderProductDetailsViewController : UIViewController<UITextFieldDelegate,ASIHTTPRequest_delegate>{
    NSMutableArray *arrProductLabels;
    NSMutableDictionary *dictProductDetails;
    
    float price;
    float totalWithOutTax;
    float tax;
    float totalWithTax;
    float margin;
    
    NSString *strQuantityOrdered;
    
    id<SetPriceDelegate> __unsafe_unretained  delegate;
    
    int productIndex;
    
    BOOL isNetworkConnected;
    
    NSString *lastPriceSales;

    NSString *strPrice;
    NSString *strDebtor;
    NSString *strCode;
    
    BOOL isTaxApplicable;
    MBProgressHUD *spinner;
    BOOL isEditable;
}

@property (nonatomic,assign) BOOL isEditable;
@property(nonatomic,retain) NSMutableArray *arrProductLabels;
@property(nonatomic,retain) NSMutableDictionary *dictProductDetails;
@property(nonatomic,retain) NSString *strQuantityOrdered;
@property(nonatomic,retain) NSString *strPrice;
@property(nonatomic,retain) NSString *strDebtor;
@property(nonatomic,retain) NSString *strCode;
@property(nonatomic,retain) NSString *strWebserviceType;
@property (nonatomic,retain) NSString *strAvailable;
@property (nonatomic,retain) NSString *strWarehouse;
//@property (nonatomic,retain) NSString *stock_code;

@property(unsafe_unretained)id<SetPriceDelegate>delegate;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblProductName;

@property(assign)int productIndex;
@property(nonatomic,assign) BOOL isTaxApplicable;
@property (nonatomic,retain) NSOperationQueue *operationQueue;

- (IBAction)actionAvailabilityCheck:(id)sender;
@end
