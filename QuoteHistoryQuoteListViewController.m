//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "QuoteHistoryQuoteListViewController.h"
#import "CustomCellGeneral.h"
#import "ViewQuoteDetailsViewController.h"

#import "AddQuoteViewController.h"

#define QUOTE_HISTORY_QUOTE_LIST_WS @"quote/quotehistory.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface QuoteHistoryQuoteListViewController ()
{
    BOOL _loadingInProgress;
    MBProgressHUD *spinner;
}
- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;

@property (nonatomic) BOOL viewIsIn;

@end

@implementation QuoteHistoryQuoteListViewController
@synthesize listContent,filteredListContent,arrQuotes,strCustCode,btnLoad,arrQuoteDetails,arrQuotesrForWebservices;

@synthesize tblQuoteList,vwContentSuperview,vwCustomLoadProfileTip,vwNoQuotes,vwTblQuotes,localSpinner;

@synthesize searchingBar,vwNoResults,isSearchMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

-(void)ReloadQuoteHistoryTBl
{
  [self loadMoreData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrQuotes = [[NSMutableArray alloc] init];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(ReloadTBlQuoteHistory)
                                               name:@"ReloadData" object:nil];

  
    //First page
    currPage = 1;
    
    totalCount = 0;
    self.isSearchMode = NO;
    
    id dataViewController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:0];
    
    if ([dataViewController isKindOfClass: [QuoteHistoryCustomerListViewController class]]){
        isQuoteEntryModule = NO;
    }
    else{
        isQuoteEntryModule = YES;
    }
    
    backgroundQueueForQuoteList = dispatch_queue_create("com.nanan.myscmipad.bgqueueForQuoteList", NULL);
    
    
    operationQueue = [NSOperationQueue new];
    
    [localSpinner startAnimating];
    [operationQueue addOperationWithBlock:^{
        
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM soquohea"] ;
            if (!results)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                //return;
            }
            
            if ([results next]) {
                totalCount = [[[results resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [results close];
            [self callGetQuoteListFromDB:db];
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
            [localSpinner stopAnimating];
        }];
    }];
    
    
}
-(void)ReloadTBlQuoteHistory
{
  NSLog(@"RE:::::::::load");
  [arrQuotes removeAllObjects];
  [self loadMoreData];
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblQuoteList = nil;
    self.vwContentSuperview = nil;
    self.vwCustomLoadProfileTip = nil;
    self.vwNoQuotes = nil;
    self.vwTblQuotes = nil;
    self.vwNoResults = nil;
    self.searchingBar = nil;
}

- (void)doAfterDataFetched{
    if ([arrQuotes count]) {
        searchingBar.hidden = NO;
        [self.searchingBar setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];

        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwTblQuotes];
        
        if([arrQuotes count] < totalCount){
            [self repositionLoadMoreFooterView];
            
            // Dismiss loading footer
            [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
            recordNumber += ROWS_PER_PAGE;
            
            [tblQuoteList reloadData];
            
        }
        else{
            
            [_loadMoreFooterView removeFromSuperview];
            _loadMoreFooterView=nil;
        }
        
    }
    else{
        
        searchingBar.hidden = YES;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoQuotes];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Cancel WS calls
-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  NSLog(@"viewWillAppear Quote history");
  
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}


#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [self.filteredListContent count];
    }
	else
	{
        return [self.arrQuotes count];
    }
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:7];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
           
    if (isSearchMode)
	{
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"]];
        cell.lblQuoteNo.text = @"";
    
    }
	else
	{
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[arrQuotes objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"]];
        cell.lblQuoteNo.text = @"";
        
    }
    
    
    if (isQuoteEntryModule) {
        cell.btnDetailView.tag = [indexPath row];
        [cell.btnDetailView addTarget:self action:@selector(actionShowQuoteDetails:)forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.searchingBar resignFirstResponder];
    
    if (isQuoteEntryModule) {
        
//        CustomCellGeneral *cell = (CustomCellGeneral *)[tblQuoteList cellForRowAtIndexPath:indexPath];
//        
//        UIColor *backgroundColor = [UIColor orangeColor];
//        UIColor *textColor = [UIColor colorWithRed:134.0/255.0 green:74.0/255.0 blue:110.0/255.0 alpha:1.0];
        /*
        popTipView = [[CMPopTipView alloc] initWithCustomView:vwCustomLoadProfileTip];
        
        popTipView.delegate = self;
        popTipView.tag = indexPath.row;
        //popTipView.disableTapToDismiss = YES;
        //popTipView.preferredPointDirection = PointDirectionUp;
        if (backgroundColor && ![backgroundColor isEqual:[NSNull null]]) {
            popTipView.backgroundColor = backgroundColor;
        }
        if (textColor && ![textColor isEqual:[NSNull null]]) {
            popTipView.textColor = textColor;
        }
        
        popTipView.dismissTapAnywhere = YES;
        //[popTipView autoDismissAnimated:YES atTimeInterval:3.0];
        [popTipView presentPointingAtView:cell inView:self.view animated:YES];*/
    }
    else{
        
        NSString *strQuoteNum;
        
        if (isSearchMode) {
            strQuoteNum = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"]];
        }
        else{
            strQuoteNum = [NSString stringWithFormat:@"%@",[[arrQuotes objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"]];
        }
        
        AddQuoteViewController *dataViewController = [[AddQuoteViewController alloc] initWithNibName:@"AddQuoteViewController" bundle:[NSBundle mainBundle]];
        dataViewController.strQuoteNumRecived = strQuoteNum;
        dataViewController.EditableMode = YES;
        dataViewController.btnSelectAll.hidden= YES;
        
        NSLog(@"dataViewController.EditableMode==%d",dataViewController.EditableMode);
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
    
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrQuotes count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    //[spinner showLoadingView:self.view size:2];
    
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetQuoteListFromDB:db];
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
}

- (void) didFinishLoadingMoreSampleData
{
	_loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
	if ([scope isEqualToString:@"QuoteNo"])
    {
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetQuoteListByQuoteFromDB:db Quote:searchText];
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContent count]) {
                    [vwContentSuperview addSubview:vwTblQuotes];
                    [tblQuoteList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
                
                
            }];
        }];
    }
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwTblQuotes];
    [tblQuoteList reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        self.isSearchMode = YES;
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwTblQuotes];
    [tblQuoteList reloadData];
}

#pragma mark - IBActions
//Yes

/*- (IBAction)actionLoadProfile:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: popTipView.tag inSection:0];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^(void) {
        dispatch_apply([arrQuotes count], queue, ^(size_t idx) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            id obj = [arrQuotes objectAtIndex: idx];
            
            if ([[[obj objectForKey:@"CheckFlag"]  objectForKey:@"text"] isEqualToString:CHECKFLAG_YES]) {
                [obj removeObjectForKey:@"CheckFlag"];
                [dict setValue:CHECKFLAG_NO forKey:@"text"];
                [obj setObject:dict forKey:@"CheckFlag"];
                
            }
        });
        
        dispatch_queue_t mainqueue=dispatch_get_main_queue();
        dispatch_async(mainqueue, ^{
            NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
            NSMutableDictionary *dictProduct = [arrQuotes objectAtIndex:[indexPath row]];
            [dictProduct removeObjectForKey:@"CheckFlag"];
            
            [dict1 setValue:CHECKFLAG_YES forKey:@"text"];
            [dictProduct setObject:dict1 forKey:@"CheckFlag"];
            
            //[spinner showLoadingView:self.view size:2];
            [self callWSViewQuoteDetailsDetails:popTipView.tag];
            
        });
    });
    
}*/

//No
- (IBAction)actionDismissTipView:(UIButton *)sender{
    //[popTipView dismissAnimated:YES];
}

- (void)actionShowQuoteDetails:(UIButton *)sender{
    NSString *strQuoteNum = [[[arrQuotes objectAtIndex:sender.tag] objectForKey:@"QuoteNo"] objectForKey:@"text"];
    
    ViewQuoteDetailsViewController *dataViewController = [[ViewQuoteDetailsViewController alloc] initWithNibName:@"ViewQuoteDetailsViewController" bundle:[NSBundle mainBundle]];
    
    dataViewController.strQuoteNum = strQuoteNum;
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

#pragma mark - Database calls
-(void)callGetQuoteListFromDB:(FMDatabase *)db
{
    @try {
        
        FMResultSet *rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,QUOTE_NO as QuoteNo,DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun, created_date, modified_date FROM soquohea WHERE trim(DEBTOR)=? AND STATUS = ? ORDER BY QUOTE_NO ASC LIMIT ?,?",[strCustCode trimSpaces],STATUS,[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
         
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrQuotes addObject:dictData];
            
        }
        
        //NSLog(@"arrQuotes %@",arrQuotes);
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

        
    }
}

-(void)callGetQuoteListByQuoteFromDB:(FMDatabase *)db Quote:(NSString *)quote{
    
    FMResultSet *rs;
    
    @try {
        
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,QUOTE_NO as QuoteNo,DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun, created_date, modified_date FROM soquohea WHERE DEBTOR=? AND STATUS = ? AND QUOTE_NO LIKE ?",strCustCode,STATUS,[NSString stringWithFormat:@"%%%@%%", quote]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

    }
    
}

#pragma mark - WS calls
-(void)callWSGetQuoteList:(int)pageIndex
{
    NSLog(@"callWSGetQuoteList");
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_HISTORY";
    NSString *last_sync_date = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"soquohea"];
    
    NSString *parameters = [NSString stringWithFormat:@"custId=%@&page=%d&limit=%d&last_syncDate=%@",strCustCode,pageIndex,ROWS_PER_PAGE,last_sync_date];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_HISTORY_QUOTE_LIST_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewQuoteDetailsDetails:(int)index
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DETAILS_DETAILS";
    NSString *strQuoteNum = [[[arrQuotes objectAtIndex:index] objectForKey:@"QuoteNo"] objectForKey:@"text"];
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",strQuoteNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DETAILS_DETAILS_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    
    [spinner removeFromSuperview];
    
    NSLog(@"ASIHTTPRequest_Success");
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"dictResponse ==================%@", dictResponse);
    
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_HISTORY"]){
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwTblQuotes];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Flag"])
        {
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"])
            {
                
                if(!self.arrQuotesrForWebservices){
                    arrQuotesrForWebservices = [[NSMutableArray alloc] init];
                }
                
                if (_loadMoreFooterView == nil) {
                    [self setupLoadMoreFooterView];
                }
                
                //Total Records
                totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"];
                    [arrQuotesrForWebservices addObject:dict];
                }
                else{
                    
                    [arrQuotesrForWebservices addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"]];
                }
                
                dispatch_async(backgroundQueueForQuoteList, ^(void) {
                    
                    [dbQueueQuoteList inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        
                        BOOL isSomethingWrongHappened = FALSE;
                        
                        @try
                        {
                            
                            for (NSDictionary *dict in arrQuotesrForWebservices){
                                                            
                                BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `soquohea` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`, `DEL_NAME`, `DELIVERY_RUN`, `DEL_DATE`, `DEBTOR`, `CONTACT`, `created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [[dict objectForKey:@"ScmRecum"] objectForKey:@"text"],[[dict objectForKey:@"Recum"] objectForKey:@"text"], [[dict objectForKey:@"QuoteNo"] objectForKey:@"text"],[[dict objectForKey:@"DelName"] objectForKey:@"text"], [[dict objectForKey:@"DelRun"] objectForKey:@"text"], [[dict objectForKey:@"DelDate"] objectForKey:@"text"],[[dict objectForKey:@"Debtor"] objectForKey:@"text"],[[dict objectForKey:@"Contact"] objectForKey:@"text"],[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
                                
                                
                                if (!y)
                                {
                                    isSomethingWrongHappened = TRUE;
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                            }
                            
                             *rollback = NO;
                            [db commit];
                            
                        }
                        
                        @catch(NSException* e){
                            *rollback = YES;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                
                            });
                            
                        }
                        
                        
                        @finally {
                            [self callGetQuoteListFromDB:db];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            @try
                            {
                                [spinnerObj hide:YES];
                                [tblQuoteList reloadData];
                                
                                if([arrQuotes count] < totalCount){
                                    [self repositionLoadMoreFooterView];
                                    
                                    // Dismiss loading footer
                                    [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                                    
                                    currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                                    
                                    //Next page
                                    currPage ++;
                                    
                                    recordNumber += ROWS_PER_PAGE;
                                    
                                }
                                else{
                                    
                                    [_loadMoreFooterView removeFromSuperview];
                                    _loadMoreFooterView=nil;
                                }
                                
                            }
                            @catch(NSException *e)
                            {
                                NSLog(@"Exception : %@",[e reason]);
                                //[db close];
                            }
                            
                            
                        });
                        
                    }];
                    
                });
            }
            //False
            else{
                
                /*NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Message"] objectForKey:@"text"];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 
                 [alert show];*/
                
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwNoQuotes];
                
            }
        }
        //No more updates
        else if([[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"UpdateInfo"]){
            dispatch_async(backgroundQueueForQuoteList, ^(void) {
                
                [dbQueueQuoteList inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try
                    {
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquohea" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        //Commit here
                        [db commit];
                        
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                        //[db close];
                    }
                    
                    @finally {
                        [self callGetQuoteListFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {                            
                            [spinnerObj hide:YES];
                            [tblQuoteList reloadData];
                            
                            if ([arrQuotes count]) {
                                if([arrQuotes count] < totalCount){
                                    [self repositionLoadMoreFooterView];
                                    
                                    // Dismiss loading footer
                                    [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                                    
                                    currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                                    
                                    //Next page
                                    currPage ++;
                                    
                                    recordNumber += ROWS_PER_PAGE;
                                    
                                }
                                else{
                                    
                                    [_loadMoreFooterView removeFromSuperview];
                                    _loadMoreFooterView=nil;
                                }
                                
                            }
                            else{
                                [vwContentSuperview removeAllSubviews];
                                [vwContentSuperview addSubview:vwNoQuotes];
                            }
                            
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                    });
                    
                }];
                
            });
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_QUOTE_DETAILS_DETAILS"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            if(!self.arrQuoteDetails){
                arrQuoteDetails = [[NSMutableArray alloc] init];
            }
            
            [arrQuoteDetails removeAllObjects];
            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
                [arrQuoteDetails addObject:dict];
                
            }
            else{
                self.arrQuoteDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
            }
            
            ///NSString *path = [AppDelegate getFileFromDocumentsDirectory:@"QuoteDetails.plist"];
            //[arrQuoteDetails writeToFile:path atomically:YES];
           
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController automatedSlideToRight];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:arrQuoteDetails, @"source", nil]];
            
            
            [tblQuoteList reloadData];
            
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}



@end
