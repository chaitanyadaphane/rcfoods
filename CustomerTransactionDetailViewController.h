//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerTransactionDetailViewController : UIViewController<UITextFieldDelegate>{
    
    NSString *strWebserviceType;
    
    NSMutableDictionary *dictTransactionDetails;
    NSMutableArray *arrHeaderLabels;
    
    NSString *strTranCode;
    NSString *strProductName;
    
    BOOL isFromTransactionHistory;
    
    NSString *strCost;
    NSString *strPrice;
    NSString *strMarkup;
    NSString *strDebtor;
    MBProgressHUD *spinner;
    
    
    BOOL isListPriceVisible;
}

@property (nonatomic,retain) NSMutableDictionary *dictTransactionDetails;
@property (nonatomic,retain) NSString *strTranCode;
@property (nonatomic,assign) BOOL isFromTransactionHistory;
@property (nonatomic,assign) BOOL isListPriceVisible;

@property (nonatomic,assign) BOOL isFromCustomerSpecials;
@property (nonatomic,retain) NSString *strProductName;
@property (nonatomic,retain) NSString *strCost;
@property (nonatomic,retain) NSString *strPrice;
@property (nonatomic,retain) NSString *strMarkup;
@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,retain) NSOperationQueue *operationQueue;
@property (nonatomic,retain) NSString *strStatus;
@property (nonatomic,retain) NSString *strType;

@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblTansactionNumber;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblProductName;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblTransactionLabel;
@end
