//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "OrderListViewController.h"
#import "ASINetworkQueue.h"

@interface OrderListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate>{
   MBProgressHUD *spinner;
    
    bool isSearchMode ;
    
    
}

@property (nonatomic, retain) NSMutableArray *arrQuoteList;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic,retain) NSString *strWebserviceType;
@property (nonatomic,retain) IBOutlet UISearchBar *searchingBar;
@end
