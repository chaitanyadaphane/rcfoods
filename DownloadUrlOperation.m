//
//  DownloadUrlOperation.m
//  OperationsDemo
//
//  Created by Ankit Gupta on 6/6/11.
//  Copyright 2011 Pulse News. All rights reserved.
//

#import "DownloadUrlOperation.h"
#import "CHCSVParser.h"

@implementation DownloadUrlOperation{
    int current_index;
}

@synthesize error = error_, data = data_;
@synthesize connectionURL = connectionURL_,unZip = unZip_;
@synthesize tag,isUploading,soapMessage,soapAction,_lines,_currentLine,_insertSQL,isOrders,isQuotes,isOfflineQuotes;
@synthesize strFlush;

#pragma mark -
#pragma mark Initialization & Memory Management
#define BulkInsertion  @"bulkInsetion"
#define NumberofRowsInsert 400

- (id)initWithURL:(NSURL *)url
{
    if( (self = [super init]) )
    {
        connectionURL_ = [url copy];
    }
    return self;
}

- (void)dealloc
{
    if( connection_ ) {
        [connection_ cancel]; [connection_ release]; connection_ = nil;
    }
    [connectionURL_ release];
    connectionURL_ = nil;
    
    [data_ release];
    data_ = nil;
    
    [error_ release];
    error_ = nil;
    
    [unZip_ release];
    unZip_ = nil;
    
    [_lines release];
    _lines = nil;
    
    [unZip_ release];
    unZip_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

// This method is just for convenience. It cancels the URL connection if it
// still exists and finishes up the operation.
- (void)done
{
    if( connection_ ) {
        [connection_ cancel];
        [connection_ release];
        connection_ = nil;
    }
    
    // Alert anyone thsat we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}
-(void)canceled {
    // Code for being cancelled
    error_ = [[NSError alloc] initWithDomain:@"DownloadUrlOperation"
                                        code:123
                                    userInfo:nil];
    [self done];
    
}


- (void)start
{
    // Ensure that this operation starts on the main threads
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    unZip_ = [[ZipArchive alloc] init];
    
    // Ensure that the operation should exute
    if( finished_ || [self isCancelled] ) { [self done]; return; }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    if (isUploading) {
        
        if (isOrders) {
            if (tag == 50) {
                [self prepareDeletedOrdersWritingToCsv];
            }
            else if (tag == 100) {
                [self prepareOrdersHeaderForWritingToCsv];
                
            }
            else if (tag == 101) {
                [self prepareOrdersDetailsForWritingToCsv];
            }
        }
        
        if (isQuotes) {
            if (tag == 51) {
                [self prepareDeletedQuotesWritingToCsv];
            }
            else if (tag == 102) {
                [self prepareQuotesHeaderForWritingToCsv];
            }
            else if (tag == 103) {
                [self prepareQuotesDetailsForWritingToCsv];
            }
        }
        
        if (isOfflineQuotes) {
            if (tag == 104) {
                [self prepareOfflineQuotesHeaderForWritingToCsv];
                
            }
            else if (tag == 105) {
                [self prepareOfflineQuotesDetailsForWritingToCsv];
            }
        }
        
    }
    else{
        
        NSURLRequest *request=[NSURLRequest requestWithURL:connectionURL_ cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:600.0];//600.0
        
        // Create the NSURLConnection--this could have been done in init, but we delayed
        // until no in case the operation was never enqueued or was cancelled before starting
        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                      delegate:self];
    }
    
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing_;
}

- (BOOL)isFinished
{
    return finished_;
}

#pragma mark - Write To CSV

- (void)prepareDeletedOrdersWritingToCsv{
    
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
        
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"sodetail delete created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT ORDER_NO,ITEM FROM sodetail WHERE isDeleted = 1"];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SODETAIL_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter1 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:NSUTF8StringEncoding delimiter:'^'];
            [csvExportWriter1 writeField:@"ORDER_NO"];
            [csvExportWriter1 writeField:@"ITEM"];
            [csvExportWriter1 finishLine];
            
            while([rs1 next]) {
                NSDictionary *resultRow = [rs1 resultDictionary];
                //iterate over the dictionary
                [csvExportWriter1 writeField:[resultRow objectForKey:@"ORDER_NO"]];
                [csvExportWriter1 writeField:[resultRow objectForKey:@"ITEM"]];
                resultRow = nil;
                [csvExportWriter1 finishLine];
            }
            //Cleanup
            [rs1 close];
            
            [csvExportWriter1 closeStream];
            [csvExportWriter1 release];
            csvExportWriter1 = nil;
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SODETAIL_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Delete Order Detail: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Delete Order Detail: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                    }
                    else{
                        NSLog(@"Upload Delete Order Detail : File not found");
                    }
                    
                }
            }
            
            zipfile = nil;
            strNewfilename = nil;
            strZipName = nil;
        }];
        [databaseQueue release];
        databaseQueue=nil;
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Delete Order Detail Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    
    folderPath = nil;
}

- (void)prepareDeletedQuotesWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"Upload Delete Quote Detail created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT QUOTE_NO,ITEM FROM soquodet WHERE isDeleted = 1"];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOQUODET_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter1 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:NSUTF8StringEncoding delimiter:'^'];
            [csvExportWriter1 writeField:@"QUOTE_NO"];
            [csvExportWriter1 writeField:@"ITEM"];
            [csvExportWriter1 finishLine];
            
            while([rs1 next]) {
                NSDictionary *resultRow = [rs1 resultDictionary];
                //iterate over the dictionary
                [csvExportWriter1 writeField:[resultRow objectForKey:@"QUOTE_NO"]];
                [csvExportWriter1 writeField:[resultRow objectForKey:@"ITEM"]];
                resultRow = nil;
                [csvExportWriter1 finishLine];
            }
            
            //Cleanup
            [rs1 close];
            
            [csvExportWriter1 closeStream];
            [csvExportWriter1 release];
            csvExportWriter1 = nil;
            
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SOQUODET_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Delete Quote Detail: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Delete Quote Detail: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                    }
                    else{
                        NSLog(@"Upload Delete Quote Detail : File not found");
                    }
                    
                }
            }
            
            zipfile = nil;
            strNewfilename = nil;
            strZipName = nil;
        }];
        [databaseQueue release];
        databaseQueue=nil;
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Delete Quote Detail Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    
    folderPath = nil;
}

//Upload orders which are edited in offline mode
- (void)prepareOrdersHeaderForWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soheader/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"sodetail created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM soheader WHERE isUploadedToServer = ?",[NSNumber numberWithInt:0]];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOHEADER_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter1 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(soheader)"];
            
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"] || [[resultRow objectForKey:@"name"] isEqualToString:@"SCM_RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"ID" ]||[[resultRow objectForKey:@"name"] isEqualToString:@"modified_date"])) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter1 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
            }
            
            [rs close];
            
            [csvExportWriter1 finishLine];
            
            while([rs1 next]) {
                NSDictionary *resultRow = [rs1 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter1 writeField:[resultRow objectForKey:columnName]];
                }
                
                resultRow = nil;
                [csvExportWriter1 finishLine];
            }
            
            //Cleanup
            [rs1 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter1 closeStream];
            [csvExportWriter1 release];
            csvExportWriter1 = nil;
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SOHEADER_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Order Header: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Order Header: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                    }
                    else{
                        NSLog(@"Upload Order Header : File not found");
                    }
                    
                }
            }
            
            zipfile = nil;
            strNewfilename = nil;
            strZipName = nil;
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Order Header Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    folderPath = nil;
}

- (void)prepareOrdersDetailsForWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"sodetail created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            //Get sodetail details
            FMResultSet *rs2 = [db executeQuery:@"SELECT * FROM sodetail WHERE isUploadedToServer = ?",[NSNumber numberWithInt:0]];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SODETAIL_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter2 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(sodetail)"];
            
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"] || [[resultRow objectForKey:@"name"] isEqualToString:@"SCM_RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"ID" ]||[[resultRow objectForKey:@"name"] isEqualToString:@"modified_date"])) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter2 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
                
            }
            
            [rs close];
            
            [csvExportWriter2 finishLine];
            
            while([rs2 next]) {
                NSDictionary *resultRow = [rs2 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter2 writeField:[resultRow objectForKey:columnName]];
                }
                
                resultRow = nil;
                [csvExportWriter2 finishLine];
            }
            
            //Cleanup
            [rs2 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter2 closeStream];
            [csvExportWriter2 release];
            csvExportWriter2 = nil;
            
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SODETAIL_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Order Detail: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Order Detail: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        //zipfile = [documentsDirectory stringByAppendingPathComponent:@"JLStewart_Specials_Sept1210.pdf"] ;
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                        
                    }
                    else{
                        NSLog(@"Upload Order Detail : File not found");
                    }
                    
                }
            }
            zipfile = nil;
            strNewfilename = nil;
            strZipName = nil;
            
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Order Detail Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    folderPath = nil;
}

//Upload quotes which are edited in offline mode
- (void)prepareQuotesHeaderForWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"soquohea created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM soquohea WHERE isUploadedToServer = ?",[NSNumber numberWithInt:0]];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter1 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(soquohea)"];
            
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"] || [[resultRow objectForKey:@"name"] isEqualToString:@"SCM_RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"ID" ]||[[resultRow objectForKey:@"name"] isEqualToString:@"modified_date"])) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter1 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
                
            }
            
            [rs close];
            
            [csvExportWriter1 finishLine];
            
            while([rs1 next]) {
                NSDictionary *resultRow = [rs1 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter1 writeField:[resultRow objectForKey:columnName]];
                }
                
                resultRow = nil;
                [csvExportWriter1 finishLine];
            }
            
            //Cleanup
            [rs1 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter1 closeStream];
            [csvExportWriter1 release];
            csvExportWriter1 = nil;
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Quote Header: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Quote Header: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                        
                    }
                    else{
                        NSLog(@"Upload Quote Header : File not found");
                    }
                    
                }
            }
            
            strZipName = nil;
            zipfile = nil;
            strNewfilename = nil;
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Quote Header Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    folderPath = nil;
}

- (void)prepareQuotesDetailsForWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"soquodet created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            //Get sodetail details
            FMResultSet *rs2 = [db executeQuery:@"SELECT * FROM soquodet WHERE isUploadedToServer = ?",[NSNumber numberWithInt:0]];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter2 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(soquodet)"];
            
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"] || [[resultRow objectForKey:@"name"] isEqualToString:@"SCM_RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"RECNUM"]||[[resultRow objectForKey:@"name"] isEqualToString:@"ID" ]||[[resultRow objectForKey:@"name"] isEqualToString:@"modified_date"])) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter2 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
                
            }
            
            [rs close];
            
            [csvExportWriter2 finishLine];
            
            while([rs2 next]) {
                NSDictionary *resultRow = [rs2 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter2 writeField:[resultRow objectForKey:columnName]];
                }
                
                resultRow = nil;
                [csvExportWriter2 finishLine];
            }
            
            //Cleanup
            [rs2 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter2 closeStream];
            [csvExportWriter2 release];
            csvExportWriter2 = nil;
            
            
            NSString *strZipName  = [NSString stringWithFormat:@"%@%@.zip",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Upload Quote Detail: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Upload Quote Detail: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        
                        //zipfile = [documentsDirectory stringByAppendingPathComponent:@"JLStewart_Specials_Sept1210.pdf"] ;
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                        
                    }
                    else{
                        NSLog(@"Upload Quote Detail : File not found");
                    }
                    
                }
            }
            strZipName = nil;
            zipfile = nil;
            strNewfilename = nil;
            
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Upload Quote Detail Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    folderPath = nil;
}

//Upload all quotes which are created in offline mode
- (void)prepareOfflineQuotesHeaderForWritingToCsv
{
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
        
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"soquohea created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {

            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM soquohea_offline"];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter1 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(soquohea_offline)"];
            
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isEdited"] || [[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"])) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter1 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
            }
            
            [rs close];
            
            [csvExportWriter1 finishLine];
            
            while([rs1 next]) {
                NSDictionary *resultRow = [rs1 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter1 writeField:[resultRow objectForKey:columnName]];
                }
                
                resultRow = nil;
                [csvExportWriter1 finishLine];
            }
            
            //Cleanup
            [rs1 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter1 closeStream];
            [csvExportWriter1 release];
            csvExportWriter1 = nil;
            
            NSString *strZipName = [NSString stringWithFormat:@"%@%@.zip",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Offline Upload Quote Header: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Offline Upload Quote Header: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                    }
                    else{
                        NSLog(@"Offline Upload Quote Header : File not found");
                    }
                    
                }
            }
            strZipName = nil;
            zipfile = nil;
            strNewfilename = nil;
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"Offline Upload Quote Header Exception : %@",[e description]];
        NSLog(@"%@",strErrorMessage);
        
        [self canceled];
        
    }
    
    folderPath = nil;
    
}


- (void)prepareOfflineQuotesDetailsForWritingToCsv{
    
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSStringEncoding encodingA = NSUTF8StringEncoding;
    
    __block NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath isDirectory:&isDir];
    if (fileExists)
    {
        if (isDir) {
            [AppDelegate removeFolderFromDocumentsDirectory:folderPath];
        }
    }
    
    BOOL dirCreated = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    if (dirCreated) {
        NSLog(@"soquodet created");
    }
    
    @try {
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            //Get sodetail details
            FMResultSet *rs2 = [db executeQuery:@"SELECT * FROM soquodet_offline"];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strNewfilename = [NSString stringWithFormat:@"%@%@.csv",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            NSString *filepath = [folderPath stringByAppendingPathComponent:strNewfilename];
            
            NSOutputStream *exportStream = [NSOutputStream outputStreamToFileAtPath:filepath append:NO];
            
            CHCSVWriter *csvExportWriter2 = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:encodingA delimiter:'^'];
            
            
            FMResultSet *rs = [db executeQuery:@"PRAGMA table_info(soquodet_offline)"];
            NSMutableArray *orderedKeys = [[NSMutableArray alloc] init];
            
            while([rs next]) {
                NSDictionary *resultRow = [rs resultDictionary];
                if (!([[resultRow objectForKey:@"name"] isEqualToString:@"isUploadedToServer"] )) {
                    [orderedKeys addObject:[resultRow objectForKey:@"name"]];
                    [csvExportWriter2 writeField:[resultRow objectForKey:@"name"]];
                }
                resultRow = nil;
            }
            
            [rs close];
            
            [csvExportWriter2 finishLine];
            
            while([rs2 next]) {
                NSDictionary *resultRow = [rs2 resultDictionary];
                //iterate over the dictionary
                for (NSString *columnName in orderedKeys) {
                    [csvExportWriter2 writeField:[resultRow objectForKey:columnName]];
                }
                resultRow = nil;
                [csvExportWriter2 finishLine];
            }
            
            //Cleanup
            [rs2 close];
            [orderedKeys release];
            orderedKeys = nil;
            [csvExportWriter2 closeStream];
            [csvExportWriter2 release];
            csvExportWriter2 = nil;
            
            
            NSString *strZipName  = [NSString stringWithFormat:@"%@%@.zip",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]];
            
            
            NSString* zipfile = [folderPath stringByAppendingPathComponent:strZipName];
            
            if([unZip_ CreateZipFile2:zipfile])
            {
                NSLog(@"Offline Upload Quote Detail: Zip File Created");
                if([unZip_ addFileToZip:filepath newname:strNewfilename])
                {
                    
                    [unZip_ CloseZipFile2];
                    NSLog(@"Offline Upload Quote Detail: File Added to zip");
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
                        [[NSFileManager defaultManager] removeItemAtPath:filepath error:nil];
                        
                        
                        //zipfile = [documentsDirectory stringByAppendingPathComponent:@"JLStewart_Specials_Sept1210.pdf"] ;
                        NSData *data = [NSData dataWithContentsOfFile:zipfile];
                        
                        
                        NSURLRequest *request = [self postRequestWithURL:nil
                                                                    data:data
                                                                fileName:strZipName];
                        
                        
                        
                        connection_ = [[NSURLConnection alloc] initWithRequest:request
                                                                      delegate:self];
                        
                        
                    }
                    else{
                        NSLog(@"Offline Upload Quote Detail : File not found");
                    }
                    
                }
            }
            strZipName = nil;
            zipfile = nil;
            strNewfilename = nil;
            
        }];
        
        [databaseQueue release];
        databaseQueue=nil;
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Offline Upload Quote Detail Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
        [self canceled];
    }
    folderPath = nil;
}

#pragma mark - Prepare request To upload
-(NSURLRequest *)postRequestWithURL: (NSString *)url

                               data: (NSData *)data
                           fileName: (NSString*)fileName
{
    
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *myboundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",myboundary];
    [urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postData = [NSMutableData data]; //[NSMutableData dataWithCapacity:[data length] + 512];
    [postData appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", myboundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n", fileName]dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[NSData dataWithData:data]];
    [postData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", myboundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urlRequest setHTTPBody:postData];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [urlRequest setURL:connectionURL_];
    
    return urlRequest;
}

#pragma mark -
#pragma mark Delegate Methods for NSURLConnection

// The connection failed
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    
    NSLog(@"TAG  --- > %d",tag);
    //Clean up the entire sync folder
    NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/"];
    
    //Remove footprints
    for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
        NSString *exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
        [AppDelegate removeFolderFromDocumentsDirectory:exactfilePath];
    }
    
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
        return;
    }
    else {
        [data_ release];
        data_ = nil;
        error_ = [error retain];
        [self done];
    }
}

// The connection received more data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
        return;
    }
    
    //[data_ appendData:data];
    if (tag == 501) {
        
    }
     if (tag == 555){
        NSLog(@"Notification generated ");
    }
    
    if (tag == 1) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/members"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"members.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //armaster
    else if (tag == 2) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/armaster"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"armaster.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //artrans
    else if (tag == 3) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/artrans"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"artrans.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //arcoment
    else if (tag == 4) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arcoment"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arcoment.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //new_samaster
    else if (tag == 5) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/new_samaster"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"new_samaster.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //podetail
    else if (tag == 6) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/podetail"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"podetail.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sahisbud
    else if (tag == 7) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sahisbud"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sahisbud.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //samaster
    else if (tag == 8) {
      
      @try {
        //--check if we have updated data
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data error:nil];
        if([[[[dictResponse objectForKey:@"Response"]objectForKey:@"samaster"]objectForKey:@"UpdateInfo"] objectForKey:@"text"])
        {
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[[[[dictResponse objectForKey:@"Response"]objectForKey:@"samaster"]objectForKey:@"UpdateInfo"] objectForKey:@"text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [alert show];
          return;
          
        }

      }
      @catch (NSException *exception) {
        NSLog(@"Error::%@",exception.description);
      }
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/samaster"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"samaster.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //smcuspro
    else if (tag == 9) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smcuspro"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"smcuspro.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sodetail
    else if (tag == 10) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sodetail.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //soheader
    else if (tag == 11) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soheader"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soheader.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //soquohea
    else if (tag == 12) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquohea.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //soquodet
    else if (tag == 13) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquodet.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sysdesc
    else if (tag == 15) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdesc"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdesc.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sysdisct
    else if (tag == 16) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdisct"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdisct.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sysistax
    else if (tag == 18) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysistax"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysistax.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //arpaymnt
    else if (tag == 20) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arpaymnt"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arpaymnt.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //arhishea
    else if (tag == 21) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhishea"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhishea.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //arhisdet
    else if (tag == 22) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhisdet"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhisdet.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //syslogon
    else if (tag == 1000) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/syslogon"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"syslogon.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //smdaytim
    else if (tag == 10011) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smdaytim"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"smdaytim.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //sacntrct
    else if (tag == 1002) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sacntrct"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sacntrct.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }

    else if (tag == 25) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/messages"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"messages.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    //delete messages
    else if (tag == 26) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/specials"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"specials.zip"];
        
        // Append data to end of file
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    else{
        [data_ appendData:data];
    }
    
}

// Initial response
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
        return;
    }
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger statusCode = [httpResponse statusCode];
  
    if( statusCode == 200 ) {
        
        //members
        if (tag == 1) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/members/"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"members.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        
        //armaster
        else if (tag == 2) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/armaster/"];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"armaster.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //artrans
        else if (tag == 3) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/artrans/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"artrans.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //arcoment
        else if (tag == 4) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arcoment/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"arcoment.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //new_samaster
        else if (tag == 5) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/new_samaster/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"new_samaster.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //podetail
        else if (tag == 6) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/podetail/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"podetail.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        //sahisbud
        else if (tag == 7) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sahisbud/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"sahisbud.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //samaster
        else if (tag == 8) {
          
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/samaster/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"samaster.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
        //smcuspro
        else if (tag == 9) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smcuspro/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"smcuspro.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
        
        //sodetail
        else if (tag == 10) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"sodetail.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        //soheader
        else if (tag == 11) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soheader/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"soheader.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        //soquohea
        else if (tag == 12) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquohea.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
        //soquodet
        else if (tag == 13) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquodet.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
        //sysdesc
        else if (tag == 15) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdesc/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdesc.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //sysdisct
        else if (tag == 16) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdisct/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdisct.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        //sysistax
        else if (tag == 18) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysistax/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysistax.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
        //arpaymnt
        else if (tag == 20) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arpaymnt/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"arpaymnt.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //arhishea
        else if (tag == 21) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhishea/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhishea.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
        }
        //arhisdet
        else if (tag == 22) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhisdet/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhisdet.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        
         else if (tag == 1000)
        {
            
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/syslogon/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"syslogon.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
            
            
        }
         else if (tag == 10011){
             
             NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString  *documentsDirectory = [paths objectAtIndex:0];
             
             NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smdaytim/"];
             
             NSFileManager *fileManager = [NSFileManager defaultManager];
             
             BOOL isDir;
             BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
             
             if (fileExists)
             {
                 if (isDir) {
                     [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                 }
                 
             }
             
             BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
             
             if (dirCreated) {
                 NSString *filePath = [dataPath stringByAppendingPathComponent:@"smdaytim.zip"];
                 
                 // Attempt to open the file and write the downloaded data to it
                 if (![fileManager fileExistsAtPath:filePath]) {
                     [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                 }
                 
             }
             
         }
         else if (tag == 1002){
             
             NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString  *documentsDirectory = [paths objectAtIndex:0];
             
             NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sacntrct/"];
             
             NSFileManager *fileManager = [NSFileManager defaultManager];
             
             BOOL isDir;
             BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
             
             if (fileExists)
             {
                 if (isDir) {
                     [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                 }
                 
             }
             
             BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
             
             if (dirCreated) {
                 NSString *filePath = [dataPath stringByAppendingPathComponent:@"sacntrct.zip"];
                 
                 // Attempt to open the file and write the downloaded data to it
                 if (![fileManager fileExistsAtPath:filePath]) {
                     [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                 }
                 
             }
             
         }
        
        //delete messages
        else if (tag == 25) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/messages/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"messages.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }
        //delete specials
        else if (tag == 26) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/specials/"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            BOOL fileExists = [fileManager fileExistsAtPath:dataPath isDirectory:&isDir];
            
            if (fileExists)
            {
                if (isDir) {
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                }
                
            }
            
            BOOL dirCreated = [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
            
            if (dirCreated) {
                NSString *filePath = [dataPath stringByAppendingPathComponent:@"specials.zip"];
                
                // Attempt to open the file and write the downloaded data to it
                if (![fileManager fileExistsAtPath:filePath]) {
                    [fileManager createFileAtPath:filePath contents:nil attributes:nil];
                }
                
            }
        }

        else{
            NSUInteger contentSize = [httpResponse expectedContentLength] > 0 ? [httpResponse expectedContentLength] : 0;
            data_ = [[NSMutableData alloc] initWithCapacity:contentSize];
        }
        
        
    } else {
        NSString* statusError  = [NSString stringWithFormat:NSLocalizedString(@"HTTP Error: %ld", nil), statusCode];
        NSDictionary* userInfo = [NSDictionary dictionaryWithObject:statusError forKey:NSLocalizedDescriptionKey];
        error_ = [[NSError alloc] initWithDomain:@"DownloadUrlOperation"
                                            code:statusCode
                                        userInfo:userInfo];
        [self done];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
        return;
    }
  
    //Delete Orders
    else if (tag == 50) {
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Deleted Orders dictResponse %@",dictResponse);
        // NSString *responseString = [connection responseString];
        // NSLog(@"responseData %@",responseString);
        NSLog(@"Uploaded Deleted Orders to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SODETAIL_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"]];
            }
            
            //NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                for (NSDictionary *dict in arrSuccess){
                    
                    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
                    if ([[dict objectForKey:@"Item"] isKindOfClass:[NSArray class]]) {
                        arrItems = [dict objectForKey:@"Item"];
                    }
                    else{
                        [arrItems addObject:[dict objectForKey:@"Item"]];
                    }
                    
                    for (int i = 0; i < [arrItems count]; i++) {
                        NSString *strOrderNum = [[[dict objectForKey:@"OrderNo"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        //NSLog(@"strOrderNum %@",strOrderNum);
                        [db executeUpdate:@"UPDATE sodetail SET STATUS = ? WHERE ORDER_NO = ? AND ITEM = ?",STATUS_DELETE,strOrderNum,[[arrItems objectAtIndex:i] objectForKey:@"text"]];
                        
                        strOrderNum = nil;
                    }
                    
                    //[arrItems release];
                    //arrItems = nil;
                }
                
            }];
            
            [databaseQueue release];
            databaseQueue=nil;
            
        }

        [self done];
    }
    //Delete Quotes
    else if (tag == 51) {
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Deleted Quotes dictResponse %@",dictResponse);
        NSLog(@"Uploaded Deleted Quotes to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOQUODET_DELETE_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }

        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]];
            }
            
            //NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                for (NSDictionary *dict in arrSuccess){
                    
                    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
                    if ([[dict objectForKey:@"Item"] isKindOfClass:[NSArray class]]) {
                        arrItems = [dict objectForKey:@"Item"];
                    }
                    else{
                        [arrItems addObject:[dict objectForKey:@"Item"]];
                    }
                    
                    for (int i = 0; i < [arrItems count]; i++) {
                        NSString *strQuote = [[[dict objectForKey:@"QuoteNo"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSLog(@"strQuote %@",strQuote);
                        [db executeUpdate:@"UPDATE soquodet SET STATUS = ? WHERE QUOTE_NO = ? AND ITEM = ?",STATUS_DELETE,strQuote,[[arrItems objectAtIndex:i] objectForKey:@"text"]];
                        
                        strQuote = nil;
                    }
                    
                }
                
            }];
            
            [databaseQueue release];
            databaseQueue=nil;
            
        }
        [self done];
    }
    //Upload Orders Headers
    else if (tag == 100) {
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Orders Headers dictResponse %@",dictResponse);
        // NSString *responseString = [connection responseString];
        // NSLog(@"responseData %@",responseString);
        NSLog(@"Uploaded Orders Headers to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soheader/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOHEADER_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
       
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]];
            }
            
            NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                
                for (int k = 0;k < [arrSuccess count];k++){
                    
                    [db executeUpdate:@"UPDATE soheader SET isUploadedToServer = 1 WHERE ORDER_NO = ?",[[arrSuccess objectAtIndex:k] objectForKey:@"text"]];
                    
                }
                
            }];
            [databaseQueue release];
            databaseQueue=nil;
            
            // [arrSuccess release];
            //arrSuccess = nil;
            
        }

        [self done];
    }
    //Upload Orders Details
    else if (tag == 101) {
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Orders Details dictResponse %@",dictResponse);
        NSLog(@"Uploaded Orders Details to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SODETAIL_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
      
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Order"]];
            }
            
            //NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                for (NSDictionary *dict in arrSuccess){
                    
                    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
                    if ([[dict objectForKey:@"Item"] isKindOfClass:[NSArray class]]) {
                        arrItems = [dict objectForKey:@"Item"];
                    }
                    else{
                        [arrItems addObject:[dict objectForKey:@"Item"]];
                    }
                    
                    for (int i = 0; i < [arrItems count]; i++) {
                        NSString *strOrderNum = [[[dict objectForKey:@"OrderNo"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSLog(@"strOrderNum %@",strOrderNum);
                        [db executeUpdate:@"UPDATE sodetail SET isUploadedToServer = 1 WHERE ORDER_NO = ? AND ITEM = ?",strOrderNum,[[arrItems objectAtIndex:i] objectForKey:@"text"]];
                        
                        strOrderNum = nil;
                    }
                    
                    [arrItems release];
                    arrItems = nil;
                }
                
            }];
            
            [databaseQueue release];
            databaseQueue=nil;
            
        }
        
        [self done];
    }
    //Upload Quote Headers
    else if (tag == 102) {
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Quotes Headers dictResponse %@",dictResponse);
        // NSString *responseString = [connection responseString];
        // NSLog(@"responseData %@",responseString);
        NSLog(@"Uploaded Quotes Headers to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
       
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]];
            }
            
            NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {

                for (int k = 0;k < [arrSuccess count];k++){
                    
                    [db executeUpdate:@"UPDATE soquohea SET isUploadedToServer = 1 WHERE QUOTE_NO = ?",[[arrSuccess objectAtIndex:k] objectForKey:@"text"]];
                    
                }
                
            }];
            [databaseQueue release];
            databaseQueue=nil;
            
            // [arrSuccess release];
            //arrSuccess = nil;
            
        }

        
        [self done];
    }
    
    //Upload Quote Details
    else if (tag == 103) {
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Quotes Details dictResponse %@",dictResponse);
        // NSString *responseString = [connection responseString];
        // NSLog(@"responseData %@",responseString);
        NSLog(@"Uploaded Quotes Details to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
       
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]];
            }
            
            //NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                for (NSDictionary *dict in arrSuccess){
                    
                    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
                    if ([[dict objectForKey:@"Item"] isKindOfClass:[NSArray class]]) {
                        arrItems = [dict objectForKey:@"Item"];
                    }
                    else{
                        [arrItems addObject:[dict objectForKey:@"Item"]];
                    }
                    
                    for (int i = 0; i < [arrItems count]; i++) {
                        NSString *strQuote = [[[dict objectForKey:@"QuoteNo"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSLog(@"strQuote %@",strQuote);
                        [db executeUpdate:@"UPDATE soquodet SET isUploadedToServer = 1 WHERE QUOTE_NO = ? AND ITEM = ?",strQuote,[[arrItems objectAtIndex:i] objectForKey:@"text"]];
                        
                        strQuote = nil;
                    }
                    
                    [arrItems release];
                    arrItems = nil;
                }
                
            }];
            
            [databaseQueue release];
            databaseQueue=nil;
            
        }

        //[arrSuccess release];
        //arrSuccess = nil;

        [self done];
    }
    //Upload Offline Quote Headers
    else if (tag == 104) {
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOQUOHEA_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
       
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Offline Quotes Headers dictResponse %@",dictResponse);
        NSLog(@"Uploaded Offline Headers to server");
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Success"]];
            }
            
            NSLog(@"arrSuccess %@",arrSuccess);
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                
                for (int k = 0;k < [arrSuccess count];k++){
                    
                    [db executeUpdate:@"DELETE FROM soquohea_offline WHERE QUOTE_NO = ?",[[arrSuccess objectAtIndex:k] objectForKey:@"text"]];
                                       
                }
                
            }];
        
            [databaseQueue release];
            databaseQueue=nil;
            
           // [arrSuccess release];
            //arrSuccess = nil;
            
        }
        
        dictResponse = nil;
        
        [self done];
    }
    //Upload Offline Quote Details
    else if (tag == 105) {
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        NSLog(@"Uploaded Offline Quotes Details dictResponse %@",dictResponse);
        // NSString *responseString = [connection responseString];
        // NSLog(@"responseData %@",responseString);
        NSLog(@"Uploaded Offline Quotes Details to server");
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet/"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.zip",SOQUODET_,[[AppDelegate getAppDelegateObj] getMacAddress]]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
        }
        
        NSMutableArray *arrSuccess = [[NSMutableArray alloc] init];
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]) {
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"] isKindOfClass:[NSArray class]]) {
                arrSuccess = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"];
            }
            else{
                [arrSuccess addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"SuccessID"] objectForKey:@"Quote"]];
            }
            
            //NSLog(@"arrSuccess %@",arrSuccess);
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                for (NSDictionary *dict in arrSuccess){
                    
                    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
                    if ([[dict objectForKey:@"Item"] isKindOfClass:[NSArray class]]) {
                        arrItems = [dict objectForKey:@"Item"];
                    }
                    else{
                        [arrItems addObject:[dict objectForKey:@"Item"]];
                    }
                    
                    for (int i = 0; i < [arrItems count]; i++) {
                        NSString *strQuote = [[[dict objectForKey:@"QuoteNo"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        //NSLog(@"strQuote %@",strQuote);
                        [db executeUpdate:@"DELETE FROM soquodet_offline WHERE QUOTE_NO = ? AND ITEM = ?",strQuote,[[arrItems objectAtIndex:i] objectForKey:@"text"]];
                        
                        strQuote = nil;
                    }
                    
                    [arrItems release];
                    arrItems = nil;
                }
                
            }];
            
            [databaseQueue release];
            databaseQueue=nil;
            
        }

        [self done];
    }
    
    else if (tag == 1)
    {
        //members
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/members"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"members.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath])
                {
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"members exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        // NSLog(@"members exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"members File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];
                        
                        
                        if([_lines count] > 1){
                            
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO members (%@) VALUES (",headerString];
                            
                            //Value placeholders
                            for (int i = 0;i < [[_lines objectAtIndex:0]count]; i++ )
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                [db executeUpdate:@"DELETE FROM members"];
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            // isEveryThingFine = NO;
                                            //NSLog(@"Problem in recordNumber = %d",k);
                                            //NSLog(@"rowValueArray %@",[_lines objectAtIndex:k]);
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        y = nil;
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"members Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"members_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"members" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"members Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"members timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                        }
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"members Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"members : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
    }
    
    //syslogon
    else if (tag == 1000)
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/syslogon"];
        
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"syslogon.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"syslogon File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        [p setDelegate:self];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];
                        
                        //=========================Changes as per new Db=========================
                        [self syslogonUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"Error message: %@",[e description]);
                            
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"Armaster : File not found");
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"samaster : File not found");
                            
                        });
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
    }
    
    if (tag == 10011)
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smdaytim"];
        
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"smdaytim.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"smdaytim File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        [p setDelegate:self];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];
                        
                        //=========================Changes as per new Db=========================
                        [self smdaytimUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"Error message: %@",[e description]);
                            
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"smdaytim : File not found");
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"smdaytim : File not found");
                            
                        });
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
        
    }
    //sacntrct
    else if (tag == 1002) {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sacntrct"];
        
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sacntrct.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"sacntrct File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        [p setDelegate:self];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];
                        
                        //=========================Changes as per new Db=========================
                        [self sacntrctUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"Error message: %@",[e description]);
                            
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sacntrct : File not found");
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"sacntrct : File not found");
                            
                        });
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
        
    }
    else if (tag == 2) {//armaster
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/armaster"];
        
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"armaster.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            
            //NSError *error = nil;
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"armaster exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"armaster exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"Armaster File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];
                      
                      //=========================Changes as per new Db=========================
                        [self armaster_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO armaster (%@) VALUES (",headerString];
                            
                            //Value placeholders
                            for (int  i = 0 ; i < [[_lines objectAtIndex:0]count]; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                          
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                          
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"armaster Error message: %@",[e description]);
                                            
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"armaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"armaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"armaster Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"armaster timestamp not updated!!!");
                              
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                        }
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"Error message: %@",[e description]);
                        
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"Armaster : File not found");
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"samaster : File not found");
                      
                        });
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 3) {//artrans
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/artrans"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"artrans.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"artrans File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        [p release];
                        
                        [self artrans_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO artrans (%@) VALUES (",headerString];
                            
                            //Value placeholders
                            for (int i = 0;i < [[_lines objectAtIndex:0]count]; i++ )
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                           
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"artrans Error message: %@",[e description]);
                                        });
                                    }
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"artrans_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"artrans" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"artrans Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"artrans timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                            
                        }
                                               
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"artrans Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"artrans : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 4) {//arcoment
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arcoment"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arcoment.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"arcoment File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [p release];

                      
                      //===============Check to update or insert based on Recnum ///===============
                        [self arcoment_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                      
                      //===============Check to update or insert based on Recnum ///====================
                      
                      
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arcoment (%@) VALUES (",headerString];
                            
                            //Value placeholders
//                            for (id value in [_lines objectAtIndex:0])
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                           
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arcoment Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arcoment_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arcoment" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"arcoment Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arcoment timestamp not updated!!!");
                                }
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }                       
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"arcoment Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"arcoment : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 5) {//new_samaster
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/new_samaster"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"new_samaster.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"new_samaster File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO new_samaster (%@) VALUES (",headerString];

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                [db executeUpdate:@"DELETE FROM new_samaster"];
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"new_samaster Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"new_samaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"new_samaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"new_samaster Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"new_samaster timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                        }
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"new_samaster Error message: %@",[e description]);
                     
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"new_samaster : File not found");
                       
                    });
                    
                }
                
                [self done];
                
            });
        }
    else{
        NSLog(@"Unable to open file");
        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
        [self done];
    }

    }
    else if (tag == 6) {//podetail
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/podetail"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"podetail.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"podetail File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
                      //=======changes as per new database===========
                        [self podetail_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO podetail (%@) VALUES (",headerString];

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"podetail Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"podetail_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"podetail" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"podetail Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"podetail timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                        }
                                              
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"podetail Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"podetail : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 7) {//sahisbud
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sahisbud"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sahisbud.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"sahisbud File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        [self sahisbud_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sahisbud (%@) VALUES (",headerString];
                            
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sahisbud Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sahisbud_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sahisbud" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"sahisbud Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sahisbud timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                        }
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"sahisbud Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sahisbud : File not found");
                    });
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 8) {//samaster
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/samaster"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"samaster.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"samaster File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        if([_lines count]){
                          
                          ///===============Check to update or insert based on Recnum ///===============
                            [self samaster_forbulkinsertion:_lines exactfileName:exactfileName];
                            [self done];
                          return ;
                          
                          //===============Check to update or insert based on Recnum ///====================
                          
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];

                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO samaster (%@) VALUES (",headerString];
                          
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                          
                          
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                       
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"samaster Error message: %@",[e description]);
                                           
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"samaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"samaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"samaster Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"samaster timestamp not updated!!!");

                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }
                        
                                              
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"samaster Error message: %@",[e description]);

                        });
                    }
                    
                    [pool drain];
                  
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"samaster : File not found");

                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 9) { //smcuspro
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/smcuspro"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"smcuspro.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"smcuspro File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];                        
                                                
                      //=======Changes as per new database changes============
                        [self smcuspro_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return;
                        
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE smcuspro SET "];
                            
                            //Value placeholders
                            for (id value in [_lines objectAtIndex:0])
                            {
                                NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                                updateSQL = [updateSQL stringByAppendingString:setString];
                                setString = nil;
                            }
                            
                            updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                            updateSQL = [updateSQL stringByAppendingString:@" WHERE CUSTOMER_CODE = ? AND STOCK_CODE = ?"];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO smcuspro (%@) VALUES (",headerString];
                            
                            //Value placeholders
                            for (int  i = 0 ; i < [[_lines objectAtIndex:0]count]; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        
                                        NSString *strCustCode = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:2]];
                                        
                                        NSString *strStockCode = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:3]];
                                        
                                        FMResultSet *rs = [db executeQuery:@"SELECT CUSTOMER_CODE,STOCK_CODE FROM smcuspro WHERE CUSTOMER_CODE = ? AND STOCK_CODE = ?",strCustCode,strStockCode];
                                        
                                        strCustCode = nil;
                                        strStockCode = nil;
                                        
                                        if (!rs) {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        BOOL y;
                                        
                                        //If exist
                                        if ([rs next]){
                                            [(NSMutableArray *)[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"CUSTOMER_CODE"]];
                                            [(NSMutableArray *)[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"STOCK_CODE"]];
                                            y = [db executeUpdate:updateSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                        }
                                        else{
                                            y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        }
                                        
                                        [rs close];
                                        rs = nil;
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                    }
                                    @catch(NSException* e)
                                    {
                                        NSLog(@"smcuspro Error message: %@",[e description]);
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"smcuspro_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"smcuspro" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"smcuspro Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"smcuspro timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }
                      
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"smcuspro Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"smcuspro : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 10) {//sodetail
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sodetail"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sodetail.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"sodetail File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        
                        [p parse];
                        [p release];
                      
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
                      //========Chnages as per new database========
                        [self sodetail_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE sodetail SET "];
                            
                            //Value placeholders
                            for (id value in [_lines objectAtIndex:0])
                            {
                                NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                                updateSQL = [updateSQL stringByAppendingString:setString];
                                setString = nil;
                            }
                            
                            updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                            updateSQL = [updateSQL stringByAppendingString:@" WHERE ORDER_NO = ? AND ITEM = ?"];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO sodetail (%@) VALUES (",headerString];
                            

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        
                                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:2]];
                                        NSString *strItemNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:9]];
                                        FMResultSet *rs = [db executeQuery:@"SELECT ORDER_NO,ITEM FROM sodetail WHERE ORDER_NO = ? AND ITEM = ?",strOrderNum,strItemNum];
                                        
                                        strOrderNum = nil;
                                        strItemNum = nil;
                                        
                                        if (!rs) {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        BOOL y;
                                        
                                        //If exist
                                        if ([rs next]){
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"ORDER_NO"]];
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"ITEM"]];
                                            y = [db executeUpdate:updateSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                        }
                                        else{
                                            y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        }
                                        
                                        [rs close];
                                        rs = nil;
                                        
                                        
                                        if (!y)
                                        {
                                   
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sodetail Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sodetail_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sodetail" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"sodetail Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sodetail timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                        }
                        
                                               
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"sodetail Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sodetail : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 11) { //soheader
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soheader"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soheader.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"soheader File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        
                        [p setDelegate:self];
                        
                        [p parse];
                        [p release];
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
                      //==Chnages as per new datatbase
                        
                        [self soheader_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            
                            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE soheader SET "];
                            
                            //Value placeholders
                            for (id value in [_lines objectAtIndex:0])
                            {
                                NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                                updateSQL = [updateSQL stringByAppendingString:setString];
                                setString = nil;
                            }
                            
                            updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                            updateSQL = [updateSQL stringByAppendingString:@" WHERE ORDER_NO = ?"];
                            
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO soheader (%@) VALUES (",headerString];

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count] - 1;k++){
                                    
                                    @try
                                    {
                                        NSLog(@"K:%d:%@",k,[_lines objectAtIndex:k]);
                                        
                                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:2]];
                                        FMResultSet *rs = [db executeQuery:@"SELECT ORDER_NO FROM soheader WHERE ORDER_NO = ?",strOrderNum];
                                        
                                        strOrderNum = nil;
                                        
                                        if (!rs) {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        BOOL y = FALSE;
                                        
                                        //If exist
                                        if ([rs next]){
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"ORDER_NO"]];
                                            y = [db executeUpdate:updateSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                        }
                                        else{
                                            y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        }
                                        
                                        [rs close];
                                        rs = nil;
                                        
                                        if (!y)
                                        {
                                    
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        NSLog(@"soheader Error message: %@",[e description]);
                                    }
                                    
                                    NSLog(@"K : %d",k);
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soheader_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soheader" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"soheader Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soheader timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }
                        
                                              
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"soheader Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"soheader : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 12) {//soquohea
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquohea"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquohea.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"soquohea exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"soquohea exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"soquohea File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:NO];
                        
                        NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
                        [self soquohea_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE soquohea SET "];
                            
                            //Value placeholders
                            for (id value in [_lines objectAtIndex:0])
                            {
                                NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                                updateSQL = [updateSQL stringByAppendingString:setString];
                                setString = nil;
                            }
                            
                            updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                            updateSQL = [updateSQL stringByAppendingString:@" WHERE QUOTE_NO = ?"];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO soquohea (%@) VALUES (",headerString];
                            

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        
                                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:2]];
                                        FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strOrderNum];
                                        
                                        strOrderNum = nil;
                                        
                                        if (!rs) {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        BOOL y;
                                        
                                        //If exist
                                        if ([rs next]){
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"QUOTE_NO"]];
                                            y = [db executeUpdate:updateSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                        }
                                        else{
                                            y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        }
                                        
                                        [rs close];
                                        rs = nil;
                                        
                                        if (!y)
                                        {
                                         
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        y = nil;
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"soquohea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquohea_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquohea" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"soquohea Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soquohea timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                            
                        }
                        
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"soquohea Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"soquohea : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
    }
    else if (tag == 13) {//soquodet
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/soquodet"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"soquodet.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"soquodet exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"soquodet File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        [self soquodet_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                        return ;
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE soquodet SET "];
                            
                            //Value placeholders
                            for (id value in [_lines objectAtIndex:0])
                            {
                                NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                                updateSQL = [updateSQL stringByAppendingString:setString];
                                setString = nil;
                            }
                            
                            updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                            updateSQL = [updateSQL stringByAppendingString:@" WHERE QUOTE_NO = ? AND ITEM = ?"];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO soquodet (%@) VALUES (",headerString];
                            

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:2]];
                                        NSString *strItemNum = [NSString stringWithFormat:@"%@",[[_lines objectAtIndex:k] objectAtIndex:8]];
                                        FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO,ITEM FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strOrderNum,strItemNum];
                                        
                                        strOrderNum = nil;
                                        
                                        if (!rs) {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        BOOL y;
                                        
                                        //If exist
                                        if ([rs next]){
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"QUOTE_NO"]];
                                            [[_lines objectAtIndex:k] addObject:[[rs resultDictionary] objectForKey:@"ITEM"]];
                                            y = [db executeUpdate:updateSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                            [[_lines objectAtIndex:k] removeLastObject];
                                        }
                                        else{
                                            y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        }
                                        
                                        [rs close];
                                        rs = nil;
                                        
                                        if (!y)
                                        {
                                          
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                        y = nil;
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"soquodet Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquodet_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquodet" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"soquodet Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soquodet timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            [databaseQueue release];
                            databaseQueue=nil;
                        }
                        
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"soquodet Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"soquodet : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
    }
    
    else if (tag == 14){ //Specials
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        
        if ([dictResponse objectForKey:@"Response"]) {
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSMutableArray *arrMembers = [[NSMutableArray alloc] init];
                        
                        if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                            [arrMembers addObject:dict];
                        }
                        else{
                            arrMembers = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                        }
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            @try
                            {
                                [db executeUpdate:@"DELETE FROM specials"];
                                
                                for (NSDictionary *dict in arrMembers){
                                    
                                    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `specials` (`SCM_RECNUM`, `title`, `pdffile`, `pdfthumbnail`, `published`, `created_date`, `modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?)",
                                              [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                                              [[dict objectForKey:@"title"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdffile"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdfthumbnail"] objectForKey:@"text"],
                                              [[dict objectForKey:@"published"] objectForKey:@"text"],
                                              [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                              [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                    NSString *strThumbnailName = [[dict objectForKey:@"pdfthumbnail"] objectForKey:@"text"];
                                    
                                    if (![strThumbnailName isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strThumbnailUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strThumbnailName];
                                        
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strThumbnailUrl];
                                    }
                                    
                                    NSString *strPdfName =[[dict objectForKey:@"pdffile"] objectForKey:@"text"];
                                    
                                    if (![strPdfName isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strPdfName];
                                            
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strPdfUrl];
                                    }
                                    
                                    [pool drain];
                                    
                                }
                                
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"specials" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                
                                
                                //Commit here
                                //[db commit];
                                
                                
                            }
                            @catch(NSException* e)
                            {
                                NSLog(@"Error : specials: %@",[e description]);
                                //[db close];
                            }
                            
                            //*rollback = YES;
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                NSLog(@"DOWNLOADED IMAGES & PDF'S");
                                [self done];
                            });
                            
                            
                        }];
                        
                        //[arrMembers release];
                        //arrMembers = nil;
                        [databaseQueue release];
                        databaseQueue=nil;
                    });
                    
                    
                    
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    NSLog(@"Error : specials: %@",strErrorMessage);
                    
                }
                
            }
            else{
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    
                    [databaseQueue inDatabase:^(FMDatabase *db) {
                        @try{
                            [db executeUpdate:@"DELETE FROM specials"];
                            
                            //Update the New "Last sync" date
                            NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                            
                            BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"specials" NewSyncDate:strNewSyncDate Database:db];
                            
                            if (!y) {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            strNewSyncDate = nil;
                            
                        }
                        @catch(NSException* e)
                        {
                            //*rollback = YES;
                            // rethrow if not one of the two exceptions above
                            
                            NSLog(@"Error : specials: %@",[e description]);
                            //[db close];
                        }
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [self done];
                        });
                        
                        
                    }];
                    
                    [databaseQueue release];
                    databaseQueue=nil;
                });
                
                
                
                
            }
        }
        
        
        dictResponse= nil;
        
    }
    
    else if (tag == 15) {//sysdesc
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdesc"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdesc.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"sysdesc exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"sysdesc exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"sysdesc File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                                                
                        //NSLog(@"raw difference: %f", (end-start));
                      //======Changes as per new database=======
                        
//                      [self callsysdescUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        
                        [self sysdesc_forbulkinsertion:_lines exactfileName:exactfileName];
                      [self done];
                      return ;
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdesc (%@) VALUES (",headerString];
                            
                            //Value placeholders
//                            for (id value in [_lines objectAtIndex:0])
//                            {
//                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
//                            }
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            // isEveryThingFine = NO;
                                            //NSLog(@"sysdesc Problem in recordNumber = %d",k);
                                            //NSLog(@"sysdesc rowValueArray %@",[_lines objectAtIndex:k]);
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sysdesc Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdesc_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdesc" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"sysdesc Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sysdesc timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                            
                        }
                        
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"sysdesc Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sysdesc : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 16) {//sysdisct
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysdisct"];
        
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysdisct.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            
            //NSError *error = nil;
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        NSLog(@"sysdisct exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    //NSLog(@"Beginning... sysdisct ....");
                    
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    NSStringEncoding encoding = 0;
                    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                    CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                    [p setRecognizesBackslashesAsEscapes:YES];
                    [p setSanitizesFields:YES];
                    [p setStripsLeadingAndTrailingWhitespace:YES];
                    
                    //NSLog(@"sysdisct encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                    
                    [p setDelegate:self];
                    
                    //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                    [p parse];
                    [p release];
                    
                    //Remove footprints
                    [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                    
                  //=======Changes as per new database and webservice
//                  [self callsysdisctUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                    
                    [self sysdisct_forbulkinsertion:_lines exactfileName:exactfileName];
                  [self done];
                  return ;
                  
                    //NSLog(@"sysdisct raw difference: %f", (end-start));
                    
                    if([_lines count]){
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdisct (%@) VALUES (",headerString];
                        

                        for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                        {
                            insertSQL = [insertSQL stringByAppendingString:@"?,"];
                        }
                        insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                        insertSQL = [insertSQL stringByAppendingString:@")"];
                        
                        //NSLog(@"_insertSQL 1%@",_insertSQL);
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            for (int k = 1;k < [_lines count]-1;k++){
                                
                                @try
                                {
                                    BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                    
                                    if (!y)
                                    {
                                        // isEveryThingFine = NO;
                                        //NSLog(@"sysdisct Problem in recordNumber = %d",k);
                                        //NSLog(@"sysdisct rowValueArray %@",[_lines objectAtIndex:k]);
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                }
                                @catch(NSException* e)
                                {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^(void){
                                        NSLog(@"sysdisct Error message: %@",[e description]);
                                    });
                                    
                                }
                                
                            }
                            
                            
                            NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                            
                            NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdisct_"])];
                            
                            NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                            
                            NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                            
                            arrDate = nil;
                            
                            BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdisct" NewSyncDate:strNewSyncDate Database:db];
                            
                            if (y) {
                                NSLog(@"sysdisct Successfully Dumped!!!");
                            }
                            else{
                                NSLog(@"sysdisct timestamp not updated!!!");
                            }
                            
                            y = nil;
                            strNewSyncDate = nil;
                            
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                    
                    
                    
                    [pool drain];
                    
                }
                
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sysdisct : File not found");
                    });
                    
                }
                
                
                [self done];
            });
            
            
            
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 18) {//sysistax
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/sysistax"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"sysistax.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"sysistax exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"sysistax exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"sysistax File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
                      //===Chnages as per new webservice changes
//                      [self callsysistaxUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        
                        [self sysistax_forbulkinsertion:_lines exactfileName:exactfileName];
                      [self done];
                      return ;
                        //NSLog(@"raw difference: %f", (end-start));
                        
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES (",headerString];
                            
                            //Value placeholders
//                            for (id value in [_lines objectAtIndex:0])
//                            {
//                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
//                            }
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            // isEveryThingFine = NO;
                                            //NSLog(@"sysistax Problem in recordNumber = %d",k);
                                            //NSLog(@"sysistax rowValueArray %@",[_lines objectAtIndex:k]);
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sysistax Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysistax_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysistax" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"sysistax Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sysistax timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }
                        
                                              
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"sysistax Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"sysistax : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 20) {//arpaymnt
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arpaymnt"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arpaymnt.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"arpaymnt exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"arpaymnt exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"arpaymnt File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        
                        [p release];
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
//                      [self arpaymentdetUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        [self arpaymnt_forbulkinsertion:_lines exactfileName:exactfileName];
                        [self done];
                      return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arpaymnt (%@) VALUES (",headerString];
                            

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            // isEveryThingFine = NO;
                                            //NSLog(@"arpaymnt Problem in recordNumber = %d",k);
                                            //NSLog(@"arpaymnt rowValueArray %@",[_lines objectAtIndex:k]);
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arpaymnt Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arpaymnt_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arpaymnt" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"arpaymnt Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arpaymnt timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                                

                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                                                       
                        }
                                            
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"arpaymnt Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"arpaymnt : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 21) { //arhishea
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhishea"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhishea.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"arhishea exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"arhishea exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"arhishea File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        [p setDelegate:self];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        if([_lines count]){
                            [self arhishea_forbulkinsertion:_lines exactfileName:exactfileName];
                          [self done];
                          return ;
                          
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhishea (%@) VALUES (",headerString];

                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhishea_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhishea" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"arhishea Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arhishea timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                        }
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"arhishea Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"arhishea : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 22) { //arhisdet
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/arhisdet"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"arhisdet.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"arhisdet File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        
                        [p release];
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                      
//                      [self arhisdetUpdateORInsertBasedonRecum:_lines exactfileName:exactfileName];
                        [self arhisdet_forbulkinsertion:_lines exactfileName:exactfileName];
                      [self done];
                      return ;
                      
                        if([_lines count]){
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhisdet (%@) VALUES (",headerString];
                            
                            //Value placeholders
//                            for (id value in [_lines objectAtIndex:0])
//                            {
//                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
//                            }
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            //NSLog(@"_insertSQL 1%@",_insertSQL);
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inDatabase:^(FMDatabase *db) {
                                
                                for (int k = 1;k < [_lines count]-1;k++){
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
                                        
                                        if (!y)
                                        {
                                            // isEveryThingFine = NO;
                                            //NSLog(@"arhisdet Problem in recordNumber = %d",k);
                                            //NSLog(@"arhisdet rowValueArray %@",[_lines objectAtIndex:k]);
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhisdet Error message: %@",[e description]);
                                        });
                                    }
                                    
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhisdet_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhisdet" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    NSLog(@"arhisdet Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arhisdet timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                        }
                        
                                                
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"arhisdet Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"arhisdet : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }

    }
    else if (tag == 23){//notice messages
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        
        if ([dictResponse objectForKey:@"Response"]) {
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        NSMutableArray *arrMembers = [[NSMutableArray alloc] init];
                        
                        if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"];
                            [arrMembers addObject:dict];
                        }
                        else{
                            arrMembers = [[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"];
                        }
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            @try
                            {
                                [db executeUpdate:@"DELETE FROM noticemsgs"];
                                
                                for (NSDictionary *dict in arrMembers){
                                    
                                    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                                    
                                    NSString *strAttachment = @"";
                                    if ([[dict objectForKey:@"ATTACHMENT"] objectForKey:@"text"]) {
                                        strAttachment = [[dict objectForKey:@"ATTACHMENT"] objectForKey:@"text"];
                                    }
                                    
                                    NSString *strMessageContent = @"";
                                    if ([[dict objectForKey:@"MESSAGE_CONTENT"] objectForKey:@"text"]) {
                                        strMessageContent = [[dict objectForKey:@"MESSAGE_CONTENT"] objectForKey:@"text"];
                                    }
                                    
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `noticemsgs` (`MESSAGE_ID`, `DATE`, `TO`, `MESSAGE_CONTENT`, `CREATED_BY`, `READ_FLAG`, `ATTACHMENT`,`STATUS`,`created_date`,`modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?)",
                                              [[dict objectForKey:@"MESSAGE_ID"] objectForKey:@"text"],
                                              [[dict objectForKey:@"DATE"] objectForKey:@"text"],
                                              [[dict objectForKey:@"TO"] objectForKey:@"text"],
                                              strMessageContent,
                                              [[dict objectForKey:@"CREATED_BY"] objectForKey:@"text"],
                                              [[dict objectForKey:@"READ_FLAG"] objectForKey:@"text"],
                                              strAttachment,
                                              [[dict objectForKey:@"STATUS"] objectForKey:@"text"],
                                              [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                              [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                    /*
                                    NSString *strPdfName =[[dict objectForKey:@"ATTACHMENT"] objectForKey:@"text"];
                                    
                                    if (![strPdfName isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        //Download Pdf
                                        if (![[NSFileManager defaultManager] fileExistsAtPath:[AppDelegate getFileFromDocumentsDirectory:strPdfName]]) {
                                            
                                            NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strPdfName];
                                            
                                            [[AppDelegate getAppDelegateObj] downloadPDFToDocDirFromURL:strPdfUrl];
                                            
                                        }
                                    }
                                    */
                                    [pool drain];
                                }
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"noticemsgs" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                
                                //Commit here
                                //[db commit];
                                
                                
                            }
                            @catch(NSException* e)
                            {
                                NSLog(@"Error : noticemsgs %@",[e description]);
                                //[db close];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                NSLog(@"Notice messages files downloaded");
                                [self done];
                            });
                            
                            
                        }];
                        
                        //[arrMembers release];
                        //arrMembers = nil;
                        [databaseQueue release];
                        databaseQueue = nil;
                    });
                    
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    NSLog(@"Error : noticemsgs: %@",strErrorMessage);
                    
                }
                
            }
            else{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                
                [databaseQueue inDatabase:^(FMDatabase *db) {
                    @try{
                        
                        [db executeUpdate:@"DELETE FROM noticemsgs"];
                        /*FMResultSet *rs = [db executeQuery:@"SELECT ATTACHMENT FROM noticemsgs"];
                        
                        while ([rs next]) {
                            
                            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                            
                            NSDictionary *dictTemp = [rs resultDictionary];
                            
                            NSString *strPdfName =[dictTemp objectForKey:@"ATTACHMENT"];
                            
                            if (![strPdfName isEqualToString:NO_ATTACHMENT_FOUND]) {
                                //Download Pdf
                                if (![[NSFileManager defaultManager] fileExistsAtPath:[AppDelegate getFileFromDocumentsDirectory:strPdfName]]) {
                                    
                                    NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strPdfName];
                                    
                                    [[AppDelegate getAppDelegateObj] downloadPDFToDocDirFromURL:strPdfUrl]     ;
                                    
                                }
                            }
                            
                            [pool drain];
                            
                        }
                        
                        [rs close];
                         */
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"noticemsgs" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                                                
                        //Commit here
                        ////[db commit];
                    }
                    @catch(NSException* e)
                    {
                        //*rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSLog(@"Notice messages Error %@",[e description]);
                        
                        //[db close];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        NSLog(@"Notice messages files downloaded");
                        [self done];
                    });
                    
                    
                }];
                
                [databaseQueue release];
                databaseQueue=nil;
                
            }
        }
        
        dictResponse = nil;
    }
    
    else if (tag == 555){
        NSLog(@"Notification generated ");
    }
    else if (tag == 24)
    {
        //syscont
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data_ error:nil];
        // NSLog(@"%@",dictResponse);
        
        if ([dictResponse objectForKey:@"Response"])
        {
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Flag"]){
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"])
                {
                    //Insert the record in syscont table
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSMutableArray *arrData = [[NSMutableArray alloc] init];
                        
                        if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Data"];
                            [arrData addObject:dict];
                        }
                        else{
                            arrData = [[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Data"];
                        }
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            @try
                            {
                                [db executeUpdate:@"DELETE FROM specials"];
                                
                                for (NSDictionary *dict in arrData){
                                    
                                    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `syscont` (`SCM_RECNUM`, `ADDRESS`, `COMPANY`, `PHONE`, `POST_CODE`, `RECNUM`, `SA_FIN_YEAR`,SA_PERIOD,SUBURB,RowVersion) VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?)",
                                              @"1",
                                              [[dict objectForKey:@"ADDRESS"] objectForKey:@"text"],
                                              [[dict objectForKey:@"COMPANY"] objectForKey:@"text"],
                                              [[dict objectForKey:@"PHONE"] objectForKey:@"text"],
                                              [[dict objectForKey:@"POST_CODE"] objectForKey:@"text"],
                                              [[dict objectForKey:@"RECNUM"] objectForKey:@"text"],
                                              [[dict objectForKey:@"SA_FIN_YEAR"] objectForKey:@"text"],
                                              [[dict objectForKey:@"SA_PERIOD"] objectForKey:@"text"],
                                              [[dict objectForKey:@"SUBURB"] objectForKey:@"text"],
                                              [[dict objectForKey:@"RowVersion"] objectForKey:@"text"]];//--Added RowVersion
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                    [pool drain];
                                    
                                }
                                
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"syscont" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                
                                
                                //Commit here
                                //[db commit];
                                
                                
                            }
                            @catch(NSException* e)
                            {
                                NSLog(@"Error : syscont: %@",[e description]);
                                //[db close];
                            }
                            
                            //*rollback = YES;
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                [self done];
                            });
                            
                            
                        }];
                        [databaseQueue release];
                        databaseQueue=nil;
                    });
                    
                    
                    
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"syscont"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    NSLog(@"Error : syscont: %@",strErrorMessage);
                    
                }
                
            }
            else{}
        }
        else
        {
          dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self done];
          });
          
        }
      
        dictResponse= nil;
    }
    else if (tag == 25) { //deletemessages
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/messages"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"messages.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"delete messages File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        //NSLog(@"raw difference: %f", (end-start));
                        
                        if ([_lines count] > 1) {
                            for (int k = 1;k < [_lines count]-1;k++){
                                @try
                                {
                                    NSString *strFileNameToRemove = [[_lines objectAtIndex:k] objectAtIndex:5];
                                    if (![strFileNameToRemove isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strFileUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strFileNameToRemove];
                                        [[AppDelegate getAppDelegateObj] removefileFromCache:strFileUrl];
                                    }
                                    else{
                                        NSLog(@"attachement not found");
                                    }
                                    strFileNameToRemove = nil;
                                    
                                }
                                @catch(NSException* e)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^(void){
                                        NSLog(@"syscont Error message: %@",[e description]);
                                    });
                                }
                                
                            }
                            
                        }
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"delete messages Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"delete messages : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file delete messages");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
        
    }
    else if (tag == 26) { //deletespecials
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"SyncFolder/specials"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:@"specials.zip"];
        
        if ([unZip_ UnzipOpenFile: filePath]) {
            // 2
            BOOL ret = [unZip_ UnzipFileTo: dataPath overWrite: YES];
            if (NO == ret){} [unZip_ UnzipCloseFile];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *exactfilePath = nil;
                NSString *exactfileName = nil;
                
                for (NSString *filename in [[NSFileManager defaultManager] enumeratorAtPath:dataPath]){
                    
                    NSDictionary *det =	[[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
                    NSString *fileType = [det objectForKey:NSFileType];
                    
                    if( [fileType isEqualToString:NSFileTypeDirectory] == YES && ([[filename pathExtension] caseInsensitiveCompare:@"csv"] == NSOrderedSame) ){
                        exactfileName = filename;
                        exactfilePath = [NSString stringWithFormat:@"%@/%@",dataPath,filename];
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        break;
                    }
                }
                
                
                if (exactfilePath) {
                    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
                    
                    //NSLog(@"Beginning...");
                    
                    NSStringEncoding encoding = 0;
                    
                    @try{
                        //NSLog(@"arhisdet exactfilePath %@",exactfilePath);
                        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:exactfilePath];
                        if (stream == nil) {
                            @throw [NSException exceptionWithName:@"File Error" reason:@"delete specials File Not Read" userInfo:nil];
                        }
                        
                        CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:'^'];
                        [p setRecognizesBackslashesAsEscapes:YES];
                        [p setSanitizesFields:YES];
                        [p setStripsLeadingAndTrailingWhitespace:YES];
                        
                        //NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
                        
                        [p setDelegate:self];
                        
                        //NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
                        [p parse];
                        [p release];
                        
                        //Remove footprints
                        [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
                        
                        //NSLog(@"raw difference: %f", (end-start));
                        
                        if ([_lines count] > 1) {
                            for (int k = 1;k < [_lines count]-1;k++){
                                @try
                                {
                                    
                                    NSString *strFileNameToRemove = [[_lines objectAtIndex:k] objectAtIndex:2];
                                    if (![strFileNameToRemove isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strFileUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strFileNameToRemove];
                                        [[AppDelegate getAppDelegateObj] removefileFromCache:strFileUrl];
                                    }
                                    else{
                                        NSLog(@"specials pdf attachement not found");
                                    }
                                    
                                    strFileNameToRemove = [[_lines objectAtIndex:k] objectAtIndex:3];
                                    if (![strFileNameToRemove isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strFileUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strFileNameToRemove];
                                        [[AppDelegate getAppDelegateObj] removefileFromCache:strFileUrl];
                                    }
                                    else{
                                        NSLog(@"thumbnail attachement not found");
                                    }

                                    strFileNameToRemove = nil;
                                    
                                }
                                @catch(NSException* e)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^(void){
                                        NSLog(@"syscont Error message: %@",[e description]);
                                    });
                                }
                                
                            }
                            
                        }
                        
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSLog(@"delete messages Error message: %@",[e description]);
                        });
                    }
                    
                    [pool drain];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"delete messages : File not found");
                    });
                    
                }
                
                [self done];
                
            });
        }
        else{
            NSLog(@"Unable to open file delete messages");
            //Remove footprints
            [AppDelegate removeFolderFromDocumentsDirectory:dataPath];
            [self done];
        }
        
    }
    else {
        [self done];
    }
    
    
}


- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}


#pragma mark - CHCSV Parser delegates
- (void)parserDidBeginDocument:(CHCSVParser *)parser {
    
    [_lines release];
    _lines = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber {
    _currentLine = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex
{
    //field = [field stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [_currentLine addObject:field];
}
- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber {
    
    [_lines addObject:_currentLine];
    [_currentLine release];
    _currentLine = nil;
    
}
- (void)parserDidEndDocument:(CHCSVParser *)parser {
    NSLog(@"Lines Count - %d",[_lines count]);
    NSLog(@"parser ended:");
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
    NSLog(@"ERROR: %@", error);
}


#pragma mark  //===============Check to update or insert based on Recnum ///====================


-(void)callsysistaxUpdateORInsertbulkInsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Start::::  Sysistax -------- >  %@",[NSDate date]);
    
    if([aryLines count]){
        NSMutableArray *aryArgumentList = [[NSMutableArray alloc] init];
        @autoreleasepool {
            
            int numberoftimes = aryLines.count/100;
            int remaning = aryLines.count%100;
            
            NSString *strQuestion = @"";
            int loop = 0;
            for (int z = 0; z <1; z=z+1)
            {
                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES ",headerString];
                
                loop = loop + 100;
                NSLog(@"z:::::\n%d,numberoftimes:::\n%d::loop:%d\n",z,numberoftimes,loop);
                
                for (int k = z*100; k < loop; k=k+1) {
                    [aryArgumentList addObject:[aryLines objectAtIndex:k+1]];
                }
                
                [headerString release];
                headerString = nil;
                
                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                strQuestion = @"";
                
                for (int iz = 0; iz < 1 ; iz++) {
                    NSArray *ary = [aryArgumentList objectAtIndex:iz];
                    strQuestion = [NSString stringWithFormat:@"(%@,'%@','%@',%@,%@,%@,%@,%@,%@,%@,%@)",[ary objectAtIndex:0],[ary objectAtIndex:1],[ary objectAtIndex:2],[ary objectAtIndex:3],[ary objectAtIndex:4],[ary objectAtIndex:5],[ary objectAtIndex:6],[ary objectAtIndex:7],[ary objectAtIndex:8],[ary objectAtIndex:9],[ary objectAtIndex:10]];
                    insertSQL = [insertSQL stringByAppendingString:strQuestion];
                    insertSQL = [insertSQL stringByAppendingString:@","];
                }
                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                
                [databaseQueue inDatabase:^(FMDatabase *db) {
                    
                    NSLog(@"insertSQL:::::::::%@",insertSQL);
                    @try {
                        
                        BOOL s = [db executeUpdate:insertSQL];
                        [aryArgumentList removeAllObjects];
                        if (!s)
                        {
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }else{
                        }
                        
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Eror:::%@",exception.description);
                    }
                    
                    [db release];
                    db = nil;
                    
                }];
            }
            
            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
            
            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES ",headerString];
            
            NSLog(@",Final::::::::::::::::::::::remaning:::\n%d::loop:%d\n",remaning,loop);
            
            [aryArgumentList removeAllObjects];
            
            for (int k = loop+1; k < aryLines.count; k++) {
                [aryArgumentList addObject:[aryLines objectAtIndex:k]];
            }
            
            [headerString release];
            headerString = nil;
            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
            
            strQuestion = @"";
            
            for (int iz = 0; iz < aryArgumentList.count; iz++) {
                NSArray *ary = [aryArgumentList objectAtIndex:iz];
                NSLog(@"ary::::::::::%@",ary);
                if(ary.count > 10)
                {
                    strQuestion = [NSString stringWithFormat:@"(%@,'%@','%@',%@,%@,%@,%@,%@,%@,%@,%@)",[ary objectAtIndex:0],[ary objectAtIndex:1],[ary objectAtIndex:2],[ary objectAtIndex:3],[ary objectAtIndex:4],[ary objectAtIndex:5],[ary objectAtIndex:6],[ary objectAtIndex:7],[ary objectAtIndex:8],[ary objectAtIndex:9],[ary objectAtIndex:10]];
                    insertSQL = [insertSQL stringByAppendingString:strQuestion];
                    insertSQL = [insertSQL stringByAppendingString:@","];
                }
            }
            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            
            [databaseQueue inDatabase:^(FMDatabase *db) {
                
                NSLog(@"insertSQL:::::::::%@",insertSQL);
                @try {
                    
                    BOOL s = [db executeUpdate:insertSQL];
                    [aryArgumentList removeAllObjects];
                    if (!s)
                    {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }else{
                        
                    }
                }
                @catch (NSException *exception) {
                    NSLog(@"Eror:::%@",exception.description);
                }
                
                [db release];
                db = nil;
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSLog(@"End ::::  Sysistax -------- >  %@",[NSDate date]);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sysiytax Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            });
        }
    }
    NSLog(@"END::::::::::::::%@",[NSDate date]);
}


//sysistax
-(void)callsysistaxUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            NSLog(@"START  ::::  Sysistax -------- >  %@",[NSDate date]);
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                }
                
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //                                for (id value in [_lines objectAtIndex:0])
                                //                                {
                                //                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //                                }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:j]];
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sysistax Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysistax_"])];
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    arrDate = nil;
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysistax" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        NSLog(@"sysistax Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sysistax timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                    }
                    
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"END  ::::  Sysistax -------- >  %@",[NSDate date]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sysiytax Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
}


//sysdist
-(void)callsysdisctUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                    NSLog(@":::::");
                    
                }
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdisct (%@) VALUES (",headerString];
                                
                   
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sysdisct Error message: %@",[e description]);
                                        });
                                        
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdisct_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdisct" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
//                                        NSLog(@"sysdisct Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sysdisct timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                                
                            }
                            
                        }
                    }
                    
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sysdisct Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
  @catch (NSException *exception) {
    
  }

}

//sysdesc
-(void)callsysdescUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                    NSLog(@":::::");
                    
                }
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdesc (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //for (id value in [aryLines objectAtIndex:0])
                                //{
                                //insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //}
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sysdesc Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdesc_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdesc" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"sysdesc Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sysdesc timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue=nil;
                            }
                            
                        }
                    }
                    
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sysdesc Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
  @catch (NSException *exception) {
    
  }
}

//soquohea
-(void)callsoquoheaUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
  @try {
    FMResultSet *rs2;
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [db open];
    BOOL flag = NO;
    
    if(aryLines.count > 1)
    {
      for (int j = 1;j < aryLines.count-1; j++) {
        NSArray *arylastobj= [aryLines objectAtIndex:j];
     
        if(arylastobj.count > 0)
        {
            NSString *strQuery = [NSString stringWithFormat:@"SELECT Recnum FROM soquohea WHERE Recnum = %@",[arylastobj objectAtIndex:0]];
            NSLog(@"::strQuery:%@",strQuery);
            rs2 = [db executeQuery:strQuery];
            flag = NO;
            while ([rs2 next]) {
                NSString *strQuerylocal = [NSString stringWithFormat:@"UPDATE soquohea SET QUOTE_NO = '%@', DEBTOR = '%@', BRANCH = '%@', SALES_BRANCH = '%@', STATUS = '%@', CONTACT = '%@', DEL_NAME = '%@', DEL_ADDRESS1 = '%@', DEL_ADDRESS2 = '%@', DEL_ADDRESS3 = '%@', DEL_SUBURB = '%@', DEL_POST_CODE = '%@', DEL_COUNTRY = '%@', DEL_INST1 = '%@', DEL_INST2 = '%@', DATE_RAISED = '%@', PERIOD_RAISED = '%@', YEAR_RAISED = '%@', DEL_DATE = '%@', EXPIRY_DATE = '%@', CUST_ORDER = '%@', DEPARTMENT = '%@', DIRECT = '%@', RELEASE_TYPE = '%@', SALESMAN = '%@', COMMISSION = '%@', WAREHOUSE = '%@', EXCHANGE_CODE = '%@', EXCHANGE_RATE = '%@', PRICE_CODE = '%@', TRADING_TERMS = '%@', NUM_BOXES = '%@',HELD = '%@', DETAIL_LINES = '%@', WEIGHT = '%@', VOLUME = '%@', TAX_AMOUNT1 = '%@', TAX_AMOUNT2 = '%@', TAX_AMOUNT3 = '%@', TAX_AMOUNT4 = '%@', TAX_AMOUNT5 = '%@', TAX_AMOUNT6 = '%@', TAX = '%@', CARRIER = '%@', QUOTE_VALUE = '%@', DELIVERY_RUN = '%@', DROP_SEQ = '%@', CHARGE_TYPE = '%@', CHARGE_RATE = '%@', ACTIVE = '%@', EXPORT_DEBTOR = '%@', EDIT_STATUS = '%@',  CASH_COLLECT = '%@', CASH_AMOUNT = '%@', CASH_REP_PICKUP = '%@', STOCK_RETURNS = '%@', RowVersion = '%@' where Recnum = '%@' ",[arylastobj objectAtIndex:1],[arylastobj objectAtIndex:2],[arylastobj objectAtIndex:3],[arylastobj objectAtIndex:4],[arylastobj objectAtIndex:5],[arylastobj objectAtIndex:6],[arylastobj objectAtIndex:7],[arylastobj objectAtIndex:8],[arylastobj objectAtIndex:9],[arylastobj objectAtIndex:10],[arylastobj objectAtIndex:11],[arylastobj objectAtIndex:12],[arylastobj objectAtIndex:13],[arylastobj objectAtIndex:14],[arylastobj objectAtIndex:15],[arylastobj objectAtIndex:16],[arylastobj objectAtIndex:17],[arylastobj objectAtIndex:18],[arylastobj objectAtIndex:19],[arylastobj objectAtIndex:20],[arylastobj objectAtIndex:21],[arylastobj objectAtIndex:22],[arylastobj objectAtIndex:23],[arylastobj objectAtIndex:24],[arylastobj objectAtIndex:25], [arylastobj objectAtIndex:26],[arylastobj objectAtIndex:27],[arylastobj objectAtIndex:28],[arylastobj objectAtIndex:29],[arylastobj objectAtIndex:30],[arylastobj objectAtIndex:31],[arylastobj objectAtIndex:32],[arylastobj objectAtIndex:33],[arylastobj objectAtIndex:34],[arylastobj objectAtIndex:35],[arylastobj objectAtIndex:36],[arylastobj objectAtIndex:37],[arylastobj objectAtIndex:38],[arylastobj objectAtIndex:39],[arylastobj objectAtIndex:40],[arylastobj objectAtIndex:41],[arylastobj objectAtIndex:42],[arylastobj objectAtIndex:43],[arylastobj objectAtIndex:44],[arylastobj objectAtIndex:45],[arylastobj objectAtIndex:46],[arylastobj objectAtIndex:47],[arylastobj objectAtIndex:48],[arylastobj objectAtIndex:49],[arylastobj objectAtIndex:50],[arylastobj objectAtIndex:51],[arylastobj objectAtIndex:52],[arylastobj objectAtIndex:53],[arylastobj objectAtIndex:54],[arylastobj objectAtIndex:55],[arylastobj objectAtIndex:56],[arylastobj lastObject],[arylastobj objectAtIndex:0]];
                
                
                bool y = [db executeUpdate:strQuerylocal];
                if(y)
                {
                    //NSLog(@"YES updatin executed sucessfully");
                }
                
                NSLog(@"updation:::::::::::::::::::::::::");
            }
            
            if(!flag)
            {
                @autoreleasepool
                {
                    if([aryLines count]){
                        NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE soquohea SET "];
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                            updateSQL = [updateSQL stringByAppendingString:setString];
                            setString = nil;
                        }
                        
                        updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                        updateSQL = [updateSQL stringByAppendingString:@" WHERE QUOTE_NO = ?"];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO soquohea (%@) VALUES (",headerString];
                        
                        //Value placeholders
                        //                for (id value in [aryLines objectAtIndex:0])
                        //                {
                        //                  insertSQL = [insertSQL stringByAppendingString:@"?,"];
                        //                }
                        for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                        {
                            insertSQL = [insertSQL stringByAppendingString:@"?,"];
                        }
                        insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                        insertSQL = [insertSQL stringByAppendingString:@")"];
                        
                        //NSLog(@"_insertSQL 1%@",_insertSQL);
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            @try
                            {
                                
                                NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[aryLines objectAtIndex:j] objectAtIndex:2]];
                                FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strOrderNum];
                                
                                strOrderNum = nil;
                                
                                if (!rs) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                BOOL y;
                                
                                //If exist
                                if ([rs next]){
                                    [[aryLines objectAtIndex:j] addObject:[[rs resultDictionary] objectForKey:@"QUOTE_NO"]];
                                    y = [db executeUpdate:updateSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                    [[aryLines objectAtIndex:j] removeLastObject];
                                }
                                else{
                                    y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                }
                                
                                [rs close];
                                rs = nil;
                                
                                if (!y)
                                {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                y = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"soquohea Error message: %@",[e description]);
                                });
                            }
                            
                            NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                            
                            NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquohea_"])];
                            
                            NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                            
                            NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                            
                            arrDate = nil;
                            
                            BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquohea" NewSyncDate:strNewSyncDate Database:db];
                            
                            if (y) {
                                //NSLog(@"soquohea Successfully Dumped!!!");
                            }
                            else{
                                NSLog(@"soquohea timestamp not updated!!!");
                            }
                            
                            y = nil;
                            strNewSyncDate = nil;
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue=nil;
                        
                    }
                    
                }
            }
            
        }
      }
    }
      dispatch_async(dispatch_get_main_queue(), ^(void){
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquohea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      });
  }
  @catch (NSException *exception) {
    
  }
    

    
}

//Soquodet
-(void)callSoquodetUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        
      FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
      [db open];
      BOOL flag = NO;
      
      if(aryLines.count > 1)
      {
        for (int j = 1;j < aryLines.count-1; j++) {
          NSArray *arylastobj= [aryLines objectAtIndex:j];
          if(j == aryLines.count-1)
          {
            NSLog(@":::::");
            
          }
          if(arylastobj.count > 0)
          {
            if(!flag)
            {
              @autoreleasepool
              {
                if([aryLines count]){
                  NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                  
                  NSString *updateSQL = [NSString stringWithFormat:@"UPDATE soquodet SET "];
                  
                  //Value placeholders
                  for (id value in [_lines objectAtIndex:0])
                  {
                    NSString *setString = [NSString stringWithFormat:@"%@ = ?,",value];
                    updateSQL = [updateSQL stringByAppendingString:setString];
                    setString = nil;
                  }
                  
                  updateSQL = [updateSQL substringToIndex:[updateSQL length] - 1];
                  updateSQL = [updateSQL stringByAppendingString:@" WHERE QUOTE_NO = ? AND ITEM = ?"];
                  
                  NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soquodet (%@) VALUES (",headerString];
                  
                  //Value placeholders
//                  for (id value in [_lines objectAtIndex:0])
//                  {
//                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
//                  }
                    for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                    {
                        insertSQL = [insertSQL stringByAppendingString:@"?,"];
                    }
                  insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                  insertSQL = [insertSQL stringByAppendingString:@")"];
                  
                  
                  [headerString release];
                  headerString = nil;
                  
                  FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                  
                  [databaseQueue inDatabase:^(FMDatabase *db) {
                    
                    //for (int k = 1;k < [_lines count]-1;k++){
                      
                      @try
                      {
                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[aryLines objectAtIndex:j] objectAtIndex:2]];
                        NSString *strItemNum = [NSString stringWithFormat:@"%@",[[aryLines objectAtIndex:j] objectAtIndex:8]];
                        FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO,ITEM FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strOrderNum,strItemNum];
                        
                        strOrderNum = nil;
                        
                        if (!rs) {
                          NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                          @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        BOOL y;
                        
                        //If exist
                        if ([rs next]){
                          [[aryLines objectAtIndex:j] addObject:[[rs resultDictionary] objectForKey:@"QUOTE_NO"]];
                          [[aryLines objectAtIndex:j] addObject:[[rs resultDictionary] objectForKey:@"ITEM"]];
                          y = [db executeUpdate:updateSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                          [[aryLines objectAtIndex:j] removeLastObject];
                          [[aryLines objectAtIndex:j] removeLastObject];
                        }
                        else{
                          y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                        }
                        
                        [rs close];
                        rs = nil;
                        
                        if (!y)
                        {
                          // isEveryThingFine = NO;
                          //NSLog(@"soheader Problem in recordNumber = %d",k);
                          //NSLog(@"soheader rowValueArray %@",[_lines objectAtIndex:k]);
                          NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                          @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        y = nil;
                        
                      }
                      @catch(NSException* e)
                      {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                          NSLog(@"soquodet Error message: %@",[e description]);
                        });
                      }
                      
                   // }
                    
                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                    
                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquodet_"])];
                    
                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                    
                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                    
                    arrDate = nil;
                    
                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquodet" NewSyncDate:strNewSyncDate Database:db];
                    
                    if (y) {
                      NSLog(@"soquodet Successfully Dumped!!!");
                    }
                    else{
                      NSLog(@"soquodet timestamp not updated!!!");
                    }
                    
                    y = nil;
                    strNewSyncDate = nil;
                    
                  }];
                  [databaseQueue release];
                  databaseQueue=nil;

                }
                
              }
            }
            
          }
        }
      }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Soquodet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
      
    }
}

//Soheader
-(void)SoheaderUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                    NSLog(@":::::");
                    
                }
                if(arylastobj.count > 0)
                {
                    flag = NO;
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soheader (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //for (id value in [aryLines objectAtIndex:0])
                                //{
                                //insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //}
                                for (int i = 0; i < [[aryLines objectAtIndex:0] count];i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    
                                    @try
                                    {
                                        
                                        NSString *strOrderNum = [NSString stringWithFormat:@"%@",[[aryLines objectAtIndex:j] objectAtIndex:2]];
                                        strOrderNum = nil;
                                        
                                        BOOL y = FALSE;
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        NSLog(@"soheader Error message: %@",[e description]);
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soheader_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soheader" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"soheader Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"soheader timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                    }
                    
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Soheader Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
    
}

//Sodetail
//======================bulk insertion============
-(void)SoDetailBulkInsetion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Start::::::::::::::%@",[NSDate date]);
    if([aryLines count]){
        
        NSMutableArray *aryArgumentList = [[NSMutableArray alloc] init];
        @autoreleasepool {
            
            int numberoftimes = aryLines.count/NumberofRowsInsert;
            int remaning = aryLines.count%NumberofRowsInsert;
            NSString *strQuestion = @"";
            numberoftimes = 1;
            int loop = 0;
            for (int z = 0; z <numberoftimes; z=z+1)
            {
                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES ",headerString];
                
                loop = loop + 100;
                NSLog(@"z:::::\n%d,numberoftimes:::\n%d::loop:%d\n",z,numberoftimes,loop);
                
                for (int k = z*100; k < loop; k=k+1) {
                    
                    [aryArgumentList addObject:[aryLines objectAtIndex:k+1]];
                }
                
                [headerString release];
                headerString = nil;
                
                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                strQuestion = @"";
                
                for (int iz = 0; iz < 1; iz++) {
                    NSArray *ary = [aryArgumentList objectAtIndex:iz];
                    strQuestion = [NSString stringWithFormat:@"(%@,%@,%@,%@,%@,%@,%@,'%@','%@',%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@)",[ary objectAtIndex:0],[ary objectAtIndex:1],[ary objectAtIndex:2],[ary objectAtIndex:3],[ary objectAtIndex:4],[ary objectAtIndex:5],[ary objectAtIndex:6],[ary objectAtIndex:7],[ary objectAtIndex:8],[ary objectAtIndex:9],[ary objectAtIndex:10],[ary objectAtIndex:11],[ary objectAtIndex:12],[ary objectAtIndex:13],[ary objectAtIndex:14],[ary objectAtIndex:15],[ary objectAtIndex:16],[ary objectAtIndex:17],[ary objectAtIndex:18],[ary objectAtIndex:19],[ary objectAtIndex:20],[ary objectAtIndex:21],[ary objectAtIndex:22],[ary objectAtIndex:23],[ary objectAtIndex:24],[ary objectAtIndex:25],[ary objectAtIndex:26],[ary objectAtIndex:27],[ary objectAtIndex:28],[ary objectAtIndex:29],[ary objectAtIndex:30],[ary objectAtIndex:31],[ary objectAtIndex:32],[ary objectAtIndex:33],[ary objectAtIndex:34],[ary objectAtIndex:35],[ary objectAtIndex:36],[ary objectAtIndex:37],[ary objectAtIndex:38],[ary objectAtIndex:39],[ary objectAtIndex:40],[ary objectAtIndex:41],[ary objectAtIndex:42],[ary objectAtIndex:43],[ary objectAtIndex:44]];
                    insertSQL = [insertSQL stringByAppendingString:strQuestion];
                    insertSQL = [insertSQL stringByAppendingString:@","];
                }
                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                
                [databaseQueue inDatabase:^(FMDatabase *db) {
                    
                    NSLog(@"insertSQL:::::::::%@",insertSQL);
                    @try {
                        
                        BOOL s = [db executeUpdate:insertSQL];
                        [aryArgumentList removeAllObjects];
                        if (!s)
                        {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }else{
                            
                        }
                        
                        
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Eror:::%@",exception.description);
                    }
                    
                    [db release];
                    db = nil;
                    
                }];
            }
            
            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
            
            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) VALUES ",headerString];
            
            NSLog(@",Final::::::::::::::::::::::remaning:::\n%d::loop:%d\n",remaning,loop);
            
            [aryArgumentList removeAllObjects];
            
            for (int k = loop+1; k < aryLines.count; k++) {
                [aryArgumentList addObject:[aryLines objectAtIndex:k]];
            }
            
            [headerString release];
            headerString = nil;
            
            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
            strQuestion = @"";
            
            for (int iz = 0; iz < aryArgumentList.count; iz++) {
                NSArray *ary = [aryArgumentList objectAtIndex:iz];
                NSLog(@"ary::::::::::%@",ary);
                if(ary.count > 10)
                {
                    strQuestion = [NSString stringWithFormat:@"(%@,'%@','%@',%@,%@,%@,%@,%@,%@,%@,%@)",[ary objectAtIndex:0],[ary objectAtIndex:1],[ary objectAtIndex:2],[ary objectAtIndex:3],[ary objectAtIndex:4],[ary objectAtIndex:5],[ary objectAtIndex:6],[ary objectAtIndex:7],[ary objectAtIndex:8],[ary objectAtIndex:9],[ary objectAtIndex:10]];
                    insertSQL = [insertSQL stringByAppendingString:strQuestion];
                    insertSQL = [insertSQL stringByAppendingString:@","];
                }
            }
            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            
            [databaseQueue inDatabase:^(FMDatabase *db) {
                
                NSLog(@"insertSQL:::::::::%@",insertSQL);
                @try {
                    
                    BOOL s = [db executeUpdate:insertSQL];
                    [aryArgumentList removeAllObjects];
                    if (!s)
                    {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }else{
                        
                    }
                    
                    
                }
                @catch (NSException *exception) {
                    NSLog(@"Eror:::%@",exception.description);
                }
                
                [db release];
                db = nil;
                
            }];
            
        }
    }
    
    NSLog(@"END::::::::::::::%@",[NSDate date]);
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sodetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    });
}



-(void)SodetailUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Sodetail insert ");
    @try
    {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sodetail (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //for (id value in [aryLines objectAtIndex:0])
                                // {
                                // insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //}
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    @try
                                    {
                                        BOOL y;
                                        y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sodetail Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sodetail_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sodetail" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"sodetail Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sodetail timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;              }
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sodetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)SmcusproUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Insert smcuspro ");
    @try
    {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                }
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO smcuspro (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                // for (id value in [aryLines objectAtIndex:0])
                                // {
                                //insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                // }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"smcuspro_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"smcuspro" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        // NSLog(@"smcuspro Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"smcuspro timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"smcuspro Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            });
        }
    }
    @catch (NSException *exception) {
        
    }
    
}


//--Sahisbud
-(void)SahisbudUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@" Insert Sahisbud");
    @try
    {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                }
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sahisbud (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //for (id value in [aryLines objectAtIndex:0])
                                //{
                                //insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //}
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sahisbud_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sahisbud" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"sahisbud Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sahisbud timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Sahisbud Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
    @catch (NSException *exception) {
        
    }
}

//Podetail
-(void)PodetailUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Insert POdetail");
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                }
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO podetail (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //                for (id value in [aryLines objectAtIndex:0])
                                //                {
                                //                  insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //                }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"podetail_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"podetail" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"podetail Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"podetail timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Podetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
  @catch (NSException *exception) {
    
  }
}


-(void)sacntrctUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sacntrct (%@) VALUES (",headerString];
                                
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"sacntrct Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sacntrct_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sacntrct" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        NSLog(@"sacntrct Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"sacntrct timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sacntrct Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
    @catch (NSException *exception) {
    }
    
}


-(void)smdaytimUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO smdaytim (%@) VALUES (",headerString];
                                
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"smdaytim Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"smdaytim_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"smdaytim" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        NSLog(@"smdaytim Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"smdaytim timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"smdaytim Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
    @catch (NSException *exception) {
    }
}


-(void)syslogonUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
           
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO syslogon (%@) VALUES (",headerString];
                            
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inDatabase:^(FMDatabase *db) {
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                     
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"Syslogon Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"syslogon_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"syslogon" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                                            NSLog(@"syslogon Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"syslogon timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"syslogon Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
    @catch (NSException *exception) {
        
    }
    
}

//Armaster
-(void)ArmasterUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
  @try {
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [db open];
    BOOL flag = NO;
    
    if(aryLines.count > 1)
    {
      for (int j = 1;j < aryLines.count-1; j++) {
        // NSLog(@":::%@",[aryLines objectAtIndex:j]);
        NSArray *arylastobj= [aryLines objectAtIndex:j];
        if(j == aryLines.count-1)
        {
          NSLog(@":::::");
          
        }
        if(arylastobj.count > 0)
        {
          
          if(!flag)
          {
            @autoreleasepool
            {
              if([aryLines count]){
                NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO armaster (%@) VALUES (",headerString];
                
                //Value placeholders
//                for (id value in [aryLines objectAtIndex:0])
//                {
//                  insertSQL = [insertSQL stringByAppendingString:@"?,"];
//                }
                  for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                  {
                      insertSQL = [insertSQL stringByAppendingString:@"?,"];
                  }
                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                insertSQL = [insertSQL stringByAppendingString:@")"];
                
                //NSLog(@"_insertSQL 1%@",_insertSQL);
                
                [headerString release];
                headerString = nil;
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue inDatabase:^(FMDatabase *db) {
                  
                  //for (int k = 1;k < [_lines count]-1;k++){
                    
                    @try
                    {
                      BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                      
                      if (!y)
                      {
                        // isEveryThingFine = NO;
                        //NSLog(@"arhishea Problem in recordNumber = %d",k);
                        //NSLog(@"arhishea rowValueArray %@",[_lines objectAtIndex:k]);
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                      }
                      
                    }
                    @catch(NSException* e)
                    {
                      dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSLog(@"arhishea Error message: %@",[e description]);
                      });
                    }
                    
                 // }
                  
                  NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                  
                  NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"armaster_"])];
                  
                  NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                  
                  NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                  
                  arrDate = nil;
                  
                  BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"armaster" NewSyncDate:strNewSyncDate Database:db];
                  
                  if (y) {
//                    NSLog(@"artrans Successfully Dumped!!!");
                  }
                  else{
                    NSLog(@"artrans timestamp not updated!!!");
                  }
                  
                  y = nil;
                  strNewSyncDate = nil;
                  
                }];
                
                [databaseQueue release];
                databaseQueue = nil;
              }
              
            }
            
          }
        }
        
      }
    }
      dispatch_async(dispatch_get_main_queue(), ^(void){
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Armaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      });
      
  }
  @catch (NSException *exception) {
    
  }

}
///Artrans
-(void)ArtransUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO artrans (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //                            for (id value in [aryLines objectAtIndex:0])
                                //                            {
                                //                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //                            }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    //   }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"artrans_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"artrans" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //                                    NSLog(@"artrans Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"artrans timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Artrans Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
}

//Arhishea
-(void)arhisheaUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    @try {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhishea (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                //                for (id value in [aryLines objectAtIndex:0])
                                //                {
                                //                  insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                //                }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arhishea Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhishea_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhishea" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        //NSLog(@"arhishea Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"arhishea timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                            }
                        }
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arhishea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
    
}


//Arpayment
-(void)arpaymentdetUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Insert Arpaymnt");
    
    @try {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        @autoreleasepool
                        {
                            if([aryLines count]){
                                NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                                
                                NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arpaymnt (%@) VALUES (",headerString];
                                
                                //Value placeholders
                                // for (id value in [aryLines objectAtIndex:0])
                                //  {
                                // insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                // }
                                for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                                {
                                    insertSQL = [insertSQL stringByAppendingString:@"?,"];
                                }
                                insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                                insertSQL = [insertSQL stringByAppendingString:@")"];
                                
                                [headerString release];
                                headerString = nil;
                                
                                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                                
                                [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                    @try
                                    {
                                        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                        
                                        if (!y)
                                        {
                                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                        }
                                        
                                    }
                                    @catch(NSException* e)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                            NSLog(@"arpaymnt Error message: %@",[e description]);
                                        });
                                    }
                                    
                                    NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                    
                                    NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arpaymnt_"])];
                                    
                                    NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                    
                                    NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                    
                                    arrDate = nil;
                                    
                                    BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arpaymnt" NewSyncDate:strNewSyncDate Database:db];
                                    
                                    if (y) {
                                        // NSLog(@"arpaymnt Successfully Dumped!!!");
                                    }
                                    else{
                                        NSLog(@"arpaymnt timestamp not updated!!!");
                                    }
                                    
                                    y = nil;
                                    strNewSyncDate = nil;
                                    
                                }];
                                
                                [databaseQueue release];
                                databaseQueue = nil;
                                
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arpayment Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}

//--Arhisdet
-(void)arhisdetUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"Insert Arhisdet");
    @try {
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count-1; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool {
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhisdet (%@) VALUES (",headerString];
                            
                  
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                
                                BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                
                                if (!y)
                                {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhisdet_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhisdet" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"arhisdet Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arhisdet timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue=nil;
                        }
                        
                    }
                    
                }
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arhisdet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
    
}

//--Arcomment
-(void)ArcommetUpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"insert Arcomment::::::::");
    
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(j == aryLines.count-1)
                {
                }
                if(arylastobj.count > 0)
                {
                    
                    if(!flag)
                    {
                        NSString *headerString = [[NSString alloc] initWithString:[[aryLines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arcoment (%@) VALUES (",headerString];
                        
                        //Value placeholders
                        //                        for (id value in [_lines objectAtIndex:0])
                        //                        {
                        //                            insertSQL = [insertSQL stringByAppendingString:@"?,"];
                        //                        }
                        
                        for (int i = 0; i < [[_lines objectAtIndex:0] count];i++ )
                        {
                            insertSQL = [insertSQL stringByAppendingString:@"?,"];
                        }
                        insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                        insertSQL = [insertSQL stringByAppendingString:@")"];
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            @try
                            {
                                BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[aryLines objectAtIndex:j]];
                                
                                if (!y)
                                {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"arcoment Error message: %@",[e description]);
                                });
                            }
                            
                            NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                            
                            NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arcoment_"])];
                            
                            NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                            
                            NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                            
                            arrDate = nil;
                            
                            BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arcoment" NewSyncDate:strNewSyncDate Database:db];
                            
                            if (y) {
                                //NSLog(@"arcoment Successfully Dumped!!!");
                            }
                            else{
                                NSLog(@"arcoment timestamp not updated!!!");
                            }
                            
                            y = nil;
                            strNewSyncDate = nil;
                            
                        }];
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Arcomment Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
        
    }
}


-(void)UpdateORInsertBasedonRecum:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"insert Samaster::::::::");
    @try {
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int j = 1;j < aryLines.count; j++) {
                NSArray *arylastobj= [aryLines objectAtIndex:j];
                if(arylastobj.count > 0)
                {
                    if(!flag)
                    {
                        @autoreleasepool {
                            
                            NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                            
                            NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO samaster (%@) VALUES (",headerString];
                            
                            //Value placeholders
                            //                            for (id value in [_lines objectAtIndex:0])
                            //                            {
                            //                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            //                            }
                            
                            for (int i = 0; i < [[_lines objectAtIndex:0]count] ; i++)
                            {
                                insertSQL = [insertSQL stringByAppendingString:@"?,"];
                            }
                            insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
                            insertSQL = [insertSQL stringByAppendingString:@")"];
                            
                            [headerString release];
                            headerString = nil;
                            
                            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                            
                            [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                                
                                @try
                                {
                                    BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:j]];
                                    
                                    if (!y)
                                    {
                                        
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                }
                                @catch(NSException* e)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^(void){
                                        NSLog(@"samaster Error message: %@",[e description]);
                                    });
                                }
                                
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"samaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"samaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"samaster Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"samaster timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                                
                            }];
                            
                            [databaseQueue release];
                            databaseQueue = nil;
                            
                        }
                        
                    }
                }
            }
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Samaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}



#pragma mark -
#pragma mark AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            
        }
    }
}

-(void)InsertFirstTimeIntoSamaster:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
  
  @autoreleasepool {
    
    NSLog(@"Insert:::::::::::::::::::::::::::");
    NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
    
    NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO samaster (%@) VALUES (",headerString];
    
    //Value placeholders
    
    for (int k = 1; k < aryLines.count; k++) {
      
     
      //NSLog(@":::::::%@",[aryLines objectAtIndex:k]);
      NSArray *aryRowval = [aryLines objectAtIndex:k];
      
      for (int z = 0;z < aryRowval.count;z++) {
           insertSQL = [insertSQL stringByAppendingString:[NSString stringWithFormat:@"'%@',",[aryRowval objectAtIndex:z]]];
      
      }
      
//      NSLog(@"_insertSQL 1%@",insertSQL);
      aryRowval = nil;
      [aryRowval release];
      
    }
    insertSQL = [insertSQL substringToIndex:[insertSQL length] - 1];
    insertSQL = [insertSQL stringByAppendingString:@")"];
    
//    NSLog(@"_insertSQL 1%@",_insertSQL);
    
    [headerString release];
    headerString = nil;
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    
    [databaseQueue inDatabase:^(FMDatabase *db) {
      
        for (int k = 1;k < [_lines count]-1;k++){
      
      @try
      {
        BOOL y = [db executeUpdate:insertSQL withArgumentsInArray:[_lines objectAtIndex:k]];
        
//        NSLog(@"insertSQL::::::::::%@",insertSQL);
        if (!y)
        {
          // isEveryThingFine = NO;
          //NSLog(@"samaster Problem in recordNumber = %d",k);
          //NSLog(@"samaster rowValueArray %@",[_lines objectAtIndex:k]);
          NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
          @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
      }
      @catch(NSException* e)
      {
        dispatch_async(dispatch_get_main_queue(), ^(void){
          NSLog(@"samaster Error message: %@",[e description]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Samaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert.tag = 11;
            [alert show];
        });
      }
      
      //  }
      
      NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
      
      NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"samaster_"])];
      
      NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
      
      NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
      
      arrDate = nil;
      
      BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"samaster" NewSyncDate:strNewSyncDate Database:db];
      
      if (y) {
        NSLog(@"samaster Successfully Dumped!!!");
      }
      else{
        NSLog(@"samaster timestamp not updated!!!");
      }
      
      y = nil;
      strNewSyncDate = nil;
        }
    }];
    
    [databaseQueue release];
    databaseQueue = nil;
    
  }
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Samaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    });
    
}



#pragma mark - Bulk Insertion


-(void)samaster_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  samaster ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO samaster (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                    //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                    
                                }
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"samaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"samaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"samaster Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"samaster timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"samaster Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"samaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                        
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"samaster Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"samaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}



-(void)smcuspro_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  smcuspro ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO smcuspro (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"smcuspro_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"smcuspro" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"smcuspro Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"smcuspro timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"smcuspro Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"smcuspro Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                        
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"smcuspro Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"smcuspro Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}



-(void)sysistax_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  sysistax::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysistax (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysistax_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysistax" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"sysistax Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sysistax timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"sysistax Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysistax Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                        
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"Sysistax Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysistax Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)sysdesc_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  sysdesc ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdesc (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdesc_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdesc" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"sysdesc Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sysdesc timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"sysdesc Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdesc Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                        
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"sysdesc Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdesc Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)sysdisct_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  sysdisct ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sysdisct (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sysdisct_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sysdisct" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"sysdisct Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sysdisct timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"sysdisct Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdisct Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                        
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"sysdisct Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sysdisct Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}

//soquohea
-(void)soquohea_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  soquohea ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soquohea (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquohea_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquohea" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"soquohea Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soquohea timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"soquohea Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquohea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"soquohea Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquohea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


//soquodet
-(void)soquodet_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  soquodet ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soquodet (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soquodet_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soquodet" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"soquodet Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soquodet timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"soquodet Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquodet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"soquodet Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soquodet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


//soheader
-(void)soheader_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  soheader ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soheader (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"soheader_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"soheader" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"soheader Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"soheader timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"soheader Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soheader Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"soheader Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"soheader Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)sodetail_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  sodetail ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sodetail (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sodetail_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sodetail" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"sodetail Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sodetail timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"sodetail Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sodetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"sodetail Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sodetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)sahisbud_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  sahisbud ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO sahisbud (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"sahisbud_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"sahisbud" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"sahisbud Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"sahisbud timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"sahisbud Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sahisbud Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"sahisbud Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"sahisbud Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}

-(void)podetail_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  podetail ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO podetail (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"podetail_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"podetail" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"podetail Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"podetail timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"podetail Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"podetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"podetail Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"podetail Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)armaster_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  armaster ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO armaster (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"armaster_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"armaster" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"podetail Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"podetail timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"armaster Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"armaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"armaster Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"armaster Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)artrans_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  artrans ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO artrans (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"artrans_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"artrans" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"artrans Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"artrans timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"artrans Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"artrans Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"artrans Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"artrans Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)arhishea_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  arhishea ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhishea (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhishea_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhishea" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"arhishea Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arhishea timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"arhishea Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arhishea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"arhishea Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arhishea Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}



-(void)arpaymnt_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  arpaymnt ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arpaymnt (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arpaymnt_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arpaymnt" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"arpaymnt Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arpaymnt timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"arpaymnt Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arpaymnt Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"arpaymnt Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arpaymnt Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}


-(void)arhisdet_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  arhisdet ::::::::");
    @try {
        
        int numberoftimes =(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arhisdet (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arhisdet_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arhisdet" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"arhisdet Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arhisdet timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"arhisdet Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arhisdet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"arhisdet Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arhisdet Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}

//arcoment
-(void)arcoment_forbulkinsertion:(NSMutableArray *)aryLines exactfileName:(NSString *)exactfileName
{
    NSLog(@"bulk trial it is ::::  arcoment ::::::::");
    @try {
        
        int numberoftimes =abs(aryLines.count/NumberofRowsInsert);
        current_index = 0;
        
        FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [db open];
        
        BOOL flag = NO;
        
        if(aryLines.count > 1)
        {
            for (int itime = 0; itime <= numberoftimes;itime++) {
                
                if(!flag)
                {
                    @autoreleasepool {
                        
                        NSString *headerString = [[NSString alloc] initWithString:[[_lines objectAtIndex:0] componentsJoinedByString: @","]];
                        
                        NSString *insertSQL = [NSString stringWithFormat:@"INSERT OR REPLACE INTO arcoment (%@) SELECT ",headerString];
                        
                        NSString *strNumOfAttributes = @"";
                        
                        //Value placeholders
                        for (id value in [_lines objectAtIndex:0])
                        {
                            strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@"?,"];
                        }
                        
                        strNumOfAttributes = [strNumOfAttributes substringToIndex:[strNumOfAttributes length] - 1];
                        strNumOfAttributes = [strNumOfAttributes stringByAppendingString:@" "];
                        insertSQL = [insertSQL stringByAppendingString:strNumOfAttributes];
                        
                        NSMutableArray *subarray = [NSMutableArray new];
                        
                        // check for remaining records
                        int remaining = (aryLines.count%NumberofRowsInsert)- 2;
                        if (itime == numberoftimes && remaining > 0) {
                            for (int j = 1; j < remaining; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j <(current_index*NumberofRowsInsert + remaining); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                            
                        }
                        else {
                            for (int j = 1; j < NumberofRowsInsert; j++) {
                                NSString *str = [NSString stringWithFormat:@"UNION SELECT ALL %@",strNumOfAttributes];
                                insertSQL = [insertSQL stringByAppendingString:str];
                            }
                            //                            NSLog(@"insertSQL  %@",insertSQL);
                            
                            
                            for (int j = current_index*NumberofRowsInsert; j < (((current_index+1)*NumberofRowsInsert)+1); j++) {
                                //                                NSLog(@"from ---> %d  to ----> %d current ---- > %d",current_index*NumberofRowsInsert,(((current_index+1)*NumberofRowsInsert)+1), j);
                                
                                [subarray addObjectsFromArray:[aryLines objectAtIndex:j+1]];
                            }
                        }
                        
                        [headerString release];
                        headerString = nil;
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                            
                            @try
                            {
                                if ([db executeUpdate:insertSQL withArgumentsInArray:subarray])
                                {
                                    current_index = current_index + 1;
                                }
                                //NSLog(@"%d Bulk Records Inserted",(int)itime);
                                NSRange rangeOfExtension = [exactfileName rangeOfString:@".csv"];
                                
                                NSString *strUpdatedDateWithUnderscore = [[exactfileName substringToIndex:rangeOfExtension.location] substringFromIndex:NSMaxRange([exactfileName rangeOfString:@"arcoment_"])];
                                
                                NSArray* arrDate = [strUpdatedDateWithUnderscore componentsSeparatedByString: @"_"];
                                
                                NSString *strNewSyncDate = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@",[arrDate objectAtIndex:0],[arrDate objectAtIndex:1],[arrDate objectAtIndex:2],[arrDate objectAtIndex:3],[arrDate objectAtIndex:4],[arrDate objectAtIndex:5]];
                                
                                arrDate = nil;
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"arcoment" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (y) {
                                    // NSLog(@"arcoment Successfully Dumped!!!");
                                }
                                else{
                                    NSLog(@"arcoment timestamp not updated!!!");
                                }
                                
                                y = nil;
                                strNewSyncDate = nil;
                            }
                            @catch(NSException* e)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^(void){
                                    NSLog(@"arcoment Error message: %@",[e description]);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arcoment Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    alert.tag = 11;
                                    [alert show];
                                });
                            }
                            
                        }];
                        
                        [databaseQueue release];
                        databaseQueue = nil;
                    }
                }
            }
            
            [db close];
            db =  nil;
            [db release];
            
        }
        else
        {
            NSLog(@"count is less than 1");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSLog(@"arcoment Bulk Records Inserted");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"arcoment Synced successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
    }
    @catch (NSException *exception) {
    }
}

@end
