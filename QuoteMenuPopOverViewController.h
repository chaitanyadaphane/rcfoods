//
//  QuoteMenuPopOverViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  DismissPopOverDelegateForMenuList
- (void)showProductList;
- (void)actionSaveQuote:(int)finalizeTag;
-(void)actionCallEntry;
-(void)reviewOrderList;
- (void)dismissPopOver;
- (void)showCommentView;
- (void)showFreightView;
-(void)showSpecials;
@optional
-(void)deleteProductList;

@end

@interface QuoteMenuPopOverViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,ReachabilityRequest_delegate>{
    IBOutlet UITableView *tblMenuList;
    
    NSMutableArray *menuList;
    
    id <DismissPopOverDelegateForMenuList> delegate;
    
    BOOL isFinalisedNeeded;
    BOOL isCommentsNotNeeded;
    BOOL isProfileOrderEntry;
    BOOL isFromOfflineOrder;

}


@property(nonatomic,retain) NSArray *menuList;
@property(nonatomic,retain) id <DismissPopOverDelegateForMenuList> delegate;
@property(nonatomic,assign) BOOL isFinalisedNeeded;
@property(nonatomic,assign) BOOL isCommentsNotNeeded;
@property(nonatomic,assign) BOOL isFreightNotNeeded;
@property(nonatomic,assign) BOOL isProfileOrderEntry;
@property(nonatomic,assign) BOOL isFromOfflineOrder;
@property(nonatomic,assign)BOOL isProfileMaintenance;
@property(nonatomic,retain) IBOutlet UITableView *tblMenuList;

@end
