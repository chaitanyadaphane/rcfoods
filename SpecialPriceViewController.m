//
//  SpecialPriceViewController.m
//  Blayney
//
//  Created by Pooja Mishra on 22/11/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import "SpecialPriceViewController.h"

@interface SpecialPriceViewController ()
{
        NSString *isModified;
        NSString *StrDiscountPer;
        MBProgressHUD *spinner;
        NSString *strWebserviceType;
        
        NSString *custBranchCode;
        NSString *custWarehouse;
        
        BOOL isSpecialInTemp;
        
}
@end

@implementation SpecialPriceViewController


#pragma mark - View LifeCycle
@synthesize selectType,priceTxt,percentTxt,priceVal,productDetailDict;

- (void)viewDidLoad {
        [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        selectType = @"M"; // Default
        isModified = @""; // default val
        self.priceTxt.text = @"";
        
        self.segmentController.selectedSegmentIndex = 0;
        NSLog(@"productDetailDict %@",productDetailDict);
        
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self getSysdisctData:db];
                [self get_branch_armasterdb:db];
        }];
        databaseQueue = nil;
        
}


- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
}

#pragma mark - IBAction / Method

- (IBAction)saveSpecialAction:(id)sender {
        
        
        if([percentTxt.text isEqualToString:@""] && [priceTxt.text isEqualToString:@""])
        {
                
                if([selectType isEqualToString:@"M"])
                {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter margin or price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                }
                else  if([selectType isEqualToString:@"A"])
                {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter absolute price or price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                }
                else if([selectType isEqualToString:@"P"])
                {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter percent or price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                }
                return;
        }
        
        [self.view endEditing:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                NSString *soapRequest = [self createxml];
                
                spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/customer_profile/discount.php"];
                NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
                ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
                [requestnew setDelegate:self];
                [requestnew setRequestMethod:@"POST"];
                [requestnew setPostValue:soapRequest forKey:@"xmlString"];
                [requestnew setPostValue:@"update" forKey:@"action"];
                NSLog(@"soapXMLStr %@",soapRequest);
                [requestnew startAsynchronous];
        });
        
}

- (IBAction)segmentedAction:(id)sender {
        UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
        NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
        
        if (selectedSegment == 0)
        {
                NSLog(@"margin selected");
                selectType = @"M";
                self.titleLabel.text = @"margin (%):";
        }
        else if (selectedSegment == 1)
        {
                NSLog(@"Percent selected");
                selectType = @"P";
                self.titleLabel.text = @"percent (%):";
        }
        else if(selectedSegment == 2)
        {
                NSLog(@"absolute selected");
                selectType = @"A";
                self.titleLabel.text = @"absolute price ($):";
        }
        self.priceTxt.text = @"";
        self.percentTxt.text = @"";
}


#pragma mark - Webservice response

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
        @try {
                [spinner removeFromSuperview];
                NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
                NSLog(@"dictResponse:::%@",dictResponse);
                NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
                NSLog(@"text:::%@",text);
                if([text containsString:@"true"])
                {
                        [productDetailDict setObject:self.priceTxt.text forKey:@"Price"];
                        [productDetailDict setObject:selectType forKey:@"STATUS"];
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        [databaseQueue   inDatabase:^(FMDatabase *db) {
                                NSLog(@"isModified  %@",isModified);
                                
                                if([isModified isEqualToString:@"1"] && isSpecialInTemp == NO)
                                {
                                        [self callUpdateSpecial:db];
                                }
                                else if ([isModified isEqualToString:@"1"] && isSpecialInTemp == YES)
                                {
                                        [self callUpdateSpecialInTempTable:db];
                                }
                                else
                                {
                                        [self callInsertSpecial:db];
                                }
                        }];
                        
                        
                        
                        [self.delegate UpdateSpecialPrice:productDetailDict];
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Saved special price successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                }
                
        }
        @catch (NSException *exception) {
                [spinner removeFromSuperview];
                NSLog(@"error::%@",exception.description);
        }
        [[KGModal sharedInstance]hideAnimated:YES];
        
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
        [[KGModal sharedInstance]hideAnimated:YES];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Saved special price Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        
        [spinner removeFromSuperview];
        NSLog(@"Failed::::: %@",requestParameter);
        
}



#pragma mark - Percent/Margin/Absolute

-(void)calPriceFromPercent:(NSString *)percent
{
        float percentage = [percent floatValue];
        float price = [self.priceVal floatValue];
        if(price == 0)
        {
                price =[[productDetailDict objectForKey:@"Price1"]floatValue];
        }
        float specialPrice =  (price - (price *(percentage/100)));
        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[percent floatValue]];
        self.priceTxt.text = [NSString stringWithFormat:@"%.2f",specialPrice];
}

-(void)calPriceFromPerMargin:(NSString *)percent
{
        float percent_float = [percent floatValue];
        float standardCost = [[productDetailDict objectForKey:@"Cost"]floatValue];
        float specialPrice =  (standardCost / (1 - (percent_float/100)));
        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[percent floatValue]];
        self.priceTxt.text = [NSString stringWithFormat:@"%.2f",specialPrice];
}


-(void)calPercentFromPrice:(NSString *)Price
{
        float price_float = [Price floatValue];
        float price = [self.priceVal floatValue];
        if(price == 0)
        {
                price =[[productDetailDict objectForKey:@"Price1"]floatValue];
        }
        float percent_float = ceilf(((price - price_float)/price)*100);
        StrDiscountPer = [NSString stringWithFormat:@"%.2f",percent_float];
        self.percentTxt.text = [NSString stringWithFormat:@"%d",(int)percent_float ];
}

-(void)calMarginPercentFromMarginPrice:(NSString *)Price
{
        float price_float = [Price floatValue];
        float standardCost = [[productDetailDict objectForKey:@"Cost"]floatValue];
        CGFloat rounded_up = (((price_float - standardCost)* 100) / price_float);
        StrDiscountPer = [NSString stringWithFormat:@"%.2f",rounded_up];
        self.percentTxt.text = [NSString stringWithFormat:@"%@",StrDiscountPer];
}


-(void)callUpdateSpecial:(FMDatabase *)db
{
        
        @try {
                NSString *prodGroupStr = @"";
                if([productDetailDict objectForKey:@"PROD_GROUP"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"PROD_GROUP"];
                }
                else if ([productDetailDict objectForKey:@"ProdGroup"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"ProdGroup"];
                }
                
                
                NSString *keyfield = [productDetailDict objectForKey:@"CUSTOMER_CODE"];
                NSString *Stockcode = [productDetailDict objectForKey:@"StockCode"];
                
                
                if(custWarehouse.length == 0 || [custWarehouse containsString:@"null"])
                {
                        custWarehouse = [[NSUserDefaults standardUserDefaults]objectForKey:@"warehouse"];
                }
                
                NSString *query = [NSString stringWithFormat:@"UPDATE sysdisct SET `PRODUCT_GROUP`= %@,`WAREHOUSE`= %@,`TYPE1`= %@,`DISCOUNT1`= %@ WHERE STOCK_CODE = %@ AND `KEY_FIELD`= %@ ",prodGroupStr,custWarehouse,selectType,StrDiscountPer,Stockcode,keyfield];
                
                NSLog(@"Query %@",query);
                BOOL y = [db executeUpdate:@"UPDATE `sysdisct` SET `PRODUCT_GROUP`= ?,`WAREHOUSE`= ?,`TYPE1`= ?,`DISCOUNT1`= ? WHERE STOCK_CODE = ? AND `KEY_FIELD`= ? ",prodGroupStr,custWarehouse,selectType,StrDiscountPer,Stockcode,keyfield];
                
                if (!y)
                {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
        }
        @catch(NSException* e)
        {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                });
        }
}

-(void)callUpdateSpecialInTempTable:(FMDatabase *)db
{
        
        @try {
                
                NSString *prodGroupStr = @"";
                if([productDetailDict objectForKey:@"PROD_GROUP"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"PROD_GROUP"];
                }
                else if ([productDetailDict objectForKey:@"ProdGroup"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"ProdGroup"];
                }
                NSString *keyfield = [productDetailDict objectForKey:@"CUSTOMER_CODE"];
                NSString *Stockcode = [productDetailDict objectForKey:@"StockCode"];
                
                
                if(custWarehouse.length == 0 || [custWarehouse containsString:@"null"])
                {
                        custWarehouse = [[NSUserDefaults standardUserDefaults]objectForKey:@"warehouse"];
                }
                
                NSString *query = [NSString stringWithFormat:@"UPDATE sysdisct_temp SET `PRODUCT_GROUP`= %@,`WAREHOUSE`= %@,`TYPE1`= %@,`DISCOUNT1`= %@ WHERE STOCK_CODE = %@ AND `KEY_FIELD`= %@ ",prodGroupStr,custWarehouse,selectType,StrDiscountPer,Stockcode,keyfield];
                
                NSLog(@"Query %@",query);
                BOOL y = [db executeUpdate:@"UPDATE `sysdisct` SET `PRODUCT_GROUP`= ?,`WAREHOUSE`= ?,`TYPE1`= ?,`DISCOUNT1`= ? WHERE STOCK_CODE = ? AND `KEY_FIELD`= ? ",prodGroupStr,custWarehouse,selectType,StrDiscountPer,Stockcode,keyfield];
                
                if (!y)
                {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
        }
        @catch(NSException* e)
        {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                });
        }
}
-(void)callInsertSpecial:(FMDatabase *)db
{
        
        @try {
                
                NSString *prodGroupStr = @"";
                if([productDetailDict objectForKey:@"PROD_GROUP"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"PROD_GROUP"];
                }
                else if ([productDetailDict objectForKey:@"ProdGroup"])
                {
                        prodGroupStr = [productDetailDict objectForKey:@"ProdGroup"];
                }
                
                NSString *keyfield = [productDetailDict objectForKey:@"CUSTOMER_CODE"];
                NSString *Stockcode = [productDetailDict objectForKey:@"StockCode"];
                
                
                if(custWarehouse.length == 0 || [custWarehouse containsString:@"null"])
                {
                        custWarehouse = [[NSUserDefaults standardUserDefaults]objectForKey:@"warehouse"];
                }
                
                
                NSString *strQuery = [NSString stringWithFormat:@"INSERT OR REPLACE INTO `sysdisct_temp` ( `SCM_RECNUM`,`RECORD_TYPE`, `GROUP_CATEGORY`,`PRODUCT_GROUP`, `CUST_CATEGORY`, `KEY_FIELD`, `WAREHOUSE`, `STOCK_CODE`,`DR_DISC`,`QTY_FROM1`,`QTY_TO1`, `DATE_FROM1`,`DATE_TO1`, `TYPE1`, `DISCOUNT1`) VALUES (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@)",@"",@"AR",@"",prodGroupStr,@"",keyfield,custWarehouse,Stockcode,@"",@"0",@"99999999",@"1901-01-01 00:00:00",@"2100-01-01 00:00:00",selectType,StrDiscountPer];
                
                NSLog(@"Query %@",strQuery);
                BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `sysdisct_temp` ( `SCM_RECNUM`,`RECORD_TYPE`, `GROUP_CATEGORY`,`PRODUCT_GROUP`, `CUST_CATEGORY`, `KEY_FIELD`, `WAREHOUSE`, `STOCK_CODE`,`DR_DISC`,`QTY_FROM1`,`QTY_TO1`, `DATE_FROM1`,`DATE_TO1`, `TYPE1`, `DISCOUNT1`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",@"",@"AR",@"",prodGroupStr,@"",keyfield,custWarehouse,Stockcode,@"",@"0",@"99999999",@"1901-01-01 00:00:00",@"2100-01-01 00:00:00",selectType,StrDiscountPer];
                if (!y)
                {
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
        }
        @catch(NSException* e)
        {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                });
        }
}

#pragma mark -  UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
        if(textField.tag == TAG_PERCENT_FIELD)
        {
                if([selectType isEqualToString:@"M"])
                {
                        NSString *strPercentMargin = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        [self calPriceFromPerMargin:[strPercentMargin trimSpaces]];
                }
                else   if([selectType isEqualToString:@"A"])
                {
                        NSString *strAbs = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[strAbs floatValue]];
                        self.priceTxt.text = [NSString stringWithFormat:@"%.2f",[strAbs floatValue]];
                }
                else if([selectType isEqualToString:@"P"])
                {
                        NSString *strPercent = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        [self calPriceFromPercent:[strPercent trimSpaces]];
                }
                
        }
        else if (textField.tag == TAG_PRICE_FIELD)
        {
                
                if([selectType isEqualToString:@"M"])
                {
                        NSString *strPriceMargin = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        [self calMarginPercentFromMarginPrice:[strPriceMargin trimSpaces]];
                }
                else   if([selectType isEqualToString:@"A"])
                {
                        NSString *strAbsPrice = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[strAbsPrice floatValue]];
                        self.percentTxt.text = StrDiscountPer;
                }
                else if([selectType isEqualToString:@"P"])
                {
                        NSString *strPercentPrice = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        [self calPercentFromPrice:[strPercentPrice trimSpaces]];
                }
        }
        
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        if(textField.tag == TAG_PERCENT_FIELD)
        {
                if([selectType isEqualToString:@"M"])
                {
                        NSString *strPercentMargin = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        [self calPriceFromPerMargin:[strPercentMargin trimSpaces]];
                }
                else   if([selectType isEqualToString:@"A"])
                {
                        NSString *strAbs = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[strAbs floatValue]];
                        self.priceTxt.text = [NSString stringWithFormat:@"%.2f",[strAbs floatValue]];
                }
                else if([selectType isEqualToString:@"P"])
                {
                        NSString *strPercent = [NSString stringWithFormat:@"%@",self.percentTxt.text];
                        [self calPriceFromPercent:[strPercent trimSpaces]];
                }
                
        }
        else if (textField.tag == TAG_PRICE_FIELD)
        {
                
                if([selectType isEqualToString:@"M"])
                {
                        NSString *strPriceMargin = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        [self calMarginPercentFromMarginPrice:[strPriceMargin trimSpaces]];
                }
                else   if([selectType isEqualToString:@"A"])
                {
                        NSString *strAbsPrice = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        StrDiscountPer = [NSString stringWithFormat:@"%.2f",[strAbsPrice floatValue]];
                        self.percentTxt.text = StrDiscountPer;
                }
                else if([selectType isEqualToString:@"P"])
                {
                        NSString *strPercentPrice = [NSString stringWithFormat:@"%@",self.priceTxt.text];
                        [self calPercentFromPrice:[strPercentPrice trimSpaces]];
                }
        }
        
        [textField resignFirstResponder];
        return YES;
}


#pragma mark -


-(void)get_branch_armasterdb:(FMDatabase *)db{
        @try {
                FMResultSet *rs1 = [db executeQuery:@"SELECT BRANCH from armaster  WHERE CODE = ?",[productDetailDict objectForKey:@"CUSTOMER_CODE"]];
                
                if (!rs1)
                {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs1 next]) {
                        NSDictionary *dictData = [rs1 resultDictionary];
                        custBranchCode = [dictData objectForKey:@"BRANCH"];
                        NSLog(@"Branch %@",dictData);
                        custWarehouse = @"00";
                }
                [rs1 close];
                //        if(custBranchCode.length > 0)
                //        {
                //            [self callWSGetWarehouse];
                //        }
                //
        }@catch (NSException* e) {
                // rethrow if not one of the two exceptions above
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
        }
}



-(void)getSysdisctData:(FMDatabase *)db{
        @try {
                FMResultSet *rs1 = [db executeQuery:@"SELECT * from sysdisct  WHERE STOCK_CODE = ? AND KEY_FIELD = ?",[productDetailDict objectForKey:@"StockCode"],[productDetailDict objectForKey:@"CUSTOMER_CODE"]];
                
                if (!rs1)
                {
                        [rs1 close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs1 next]) {
                        isSpecialInTemp = NO;
                        isModified = @"1";
                        NSDictionary *dictData = [rs1 resultDictionary];
                        NSLog(@"dictdata from sysdisct %@",dictData);
                }
                else
                {
                        
                        FMResultSet *rs11 = [db executeQuery:@"SELECT * from sysdisct_temp  WHERE STOCK_CODE = ? AND KEY_FIELD = ?",[productDetailDict objectForKey:@"StockCode"],[productDetailDict objectForKey:@"CUSTOMER_CODE"]];
                        
                        if (!rs11)
                        {
                                [rs1 close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        if ([rs11 next]) {
                                isModified = @"1";
                                isSpecialInTemp = YES;
                                NSDictionary *dictData = [rs1 resultDictionary];
                                NSLog(@"dictdata from sysdisct temp %@",dictData);
                        }
                        else
                        {
                                isSpecialInTemp = NO;
                                isModified = @"0";
                                
                        }
                }
                NSLog(@"isModified %@",isModified);
                [rs1 close];
                
        }
        @catch (NSException* e) {
                // rethrow if not one of the two exceptions above
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
        }
        
}

-(NSString *)createxml
{
        
        
        NSString *prodGroupStr = @"";
        NSString *keyfield = @"";
        
        
        if([productDetailDict objectForKey:@"PROD_GROUP"])
        {
                prodGroupStr = [productDetailDict objectForKey:@"PROD_GROUP"];
        }
        else if ([productDetailDict objectForKey:@"ProdGroup"])
        {
                prodGroupStr = [productDetailDict objectForKey:@"ProdGroup"];
        }
        
        if ([productDetailDict objectForKey:@"CUSTOMER_CODE"])
        {
                keyfield = [productDetailDict objectForKey:@"CUSTOMER_CODE"];
        }
        
        NSString *Stockcode = [productDetailDict objectForKey:@"StockCode"];
        
        
        NSString *createdDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
        
        if(custWarehouse.length == 0 || [custWarehouse containsString:@"null"])
        {
                custWarehouse = [[NSUserDefaults standardUserDefaults]objectForKey:@"warehouse"];
        }
        
        NSString *soapXml = [NSString stringWithFormat:@"<DocumentElement>"
                             "<SYSDISCT>"
                             "<RECORD_TYPE>AR</RECORD_TYPE>"
                             "<GROUP_CATEGORY />"
                             "<PRODUCT_GROUP>%@</PRODUCT_GROUP>"
                             "<CUST_CATEGORY />"
                             "<KEY_FIELD>%@</KEY_FIELD>"
                             "<WAREHOUSE>%@</WAREHOUSE>"
                             "<STOCK_CODE>%@</STOCK_CODE>"
                             "<DR_DISC>N</DR_DISC>"
                             "<QTY_FROM1>0.0000</QTY_FROM1>"
                             "<QTY_TO1>99999999.0000</QTY_TO1>"
                             
                             "<DATE_FROM1>%@</DATE_FROM1>"
                             "<DATE_TO1>%@</DATE_TO1>"
                             "<TYPE1>%@</TYPE1>"
                             "<DISCOUNT1>%@</DISCOUNT1>"
                             
                             "<QTY_FROM2>0.0000</QTY_FROM2>"
                             "<QTY_TO2>0.0000</QTY_TO2>"
                             "<DATE_FROM2>1900-01-01T00:00:00+11:00</DATE_FROM2>"
                             "<DATE_TO2>1900-01-01T00:00:00+11:00</DATE_TO2>"
                             
                             "<TYPE2 />"
                             "<DISCOUNT2>0.0000</DISCOUNT2>"
                             "<QTY_FROM3>0.0000</QTY_FROM3>"
                             "<QTY_TO3>0.0000</QTY_TO3>"
                             "<DATE_FROM3>1900-01-01T00:00:00+11:00</DATE_FROM3>"
                             "<DATE_TO3>1900-01-01T00:00:00+11:00</DATE_TO3>"
                             "<TYPE3 />"
                             "<DISCOUNT3>0.0000</DISCOUNT3>"
                             "<QTY_FROM4>0.0000</QTY_FROM4>"
                             "<QTY_TO4>0.0000</QTY_TO4>"
                             "<DATE_FROM4>1900-01-01T00:00:00+11:00</DATE_FROM4>"
                             "<DATE_TO4>1900-01-01T00:00:00+11:00</DATE_TO4>"
                             "<TYPE4 />"
                             "<DISCOUNT4>0.0000</DISCOUNT4>"
                             "<QTY_FROM5>0.0000</QTY_FROM5>"
                             "<QTY_TO5>0.0000</QTY_TO5>"
                             "<DATE_FROM5>1900-01-01T00:00:00+11:00</DATE_FROM5>"
                             "<DATE_TO5>1900-01-01T00:00:00+11:00</DATE_TO5>"
                             "<TYPE5 />"
                             "<DISCOUNT5>0.0000</DISCOUNT5>"
                             "<QTY_FROM6>0.0000</QTY_FROM6>"
                             "<QTY_TO6>0.0000</QTY_TO6>"
                             "<DATE_FROM6>1900-01-01T00:00:00+11:00</DATE_FROM6>"
                             "<DATE_TO6>1900-01-01T00:00:00+11:00</DATE_TO6>"
                             "<TYPE6 />"
                             "<DISCOUNT6>0.0000</DISCOUNT6>"
                             "<QTY_FROM7>0.0000</QTY_FROM7>"
                             "<QTY_TO7>0.0000</QTY_TO7>"
                             "<DATE_FROM7>1900-01-01T00:00:00+11:00</DATE_FROM7>"
                             "<DATE_TO7>1900-01-01T00:00:00+11:00</DATE_TO7>"
                             "<TYPE7 />"
                             "<DISCOUNT7>0.0000</DISCOUNT7>"
                             "<QTY_FROM8>0.0000</QTY_FROM8>"
                             "<QTY_TO8>0.0000</QTY_TO8>"
                             "<DATE_FROM8>1900-01-01T00:00:00+11:00</DATE_FROM8>"
                             "<DATE_TO8>1900-01-01T00:00:00+11:00</DATE_TO8>"
                             "<TYPE8 />"
                             "<DISCOUNT8>0.0000</DISCOUNT8>"
                             "<QTY_FROM9>0.0000</QTY_FROM9>"
                             "<QTY_TO9>0.0000</QTY_TO9>"
                             "<DATE_FROM9>1900-01-01T00:00:00+11:00</DATE_FROM9>"
                             "<DATE_TO9>1900-01-01T00:00:00+11:00</DATE_TO9>"
                             "<TYPE9 />"
                             "<DISCOUNT9>0.0000</DISCOUNT9>"
                             "<QTY_FROM10>0.0000</QTY_FROM10>"
                             "<QTY_TO10>0.0000</QTY_TO10>"
                             "<DATE_FROM10>1900-01-01T00:00:00+11:00</DATE_FROM10>"
                             "<DATE_TO10>1900-01-01T00:00:00+11:00</DATE_TO10>"
                             "<TYPE10 />"
                             "<DISCOUNT10>0.0000</DISCOUNT10>"
                             "<PROMO_PRICE />"
                             "<EDIT_STATUS>19</EDIT_STATUS>"
                             "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                             "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                             "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                             "<SPARE_STR />"
                             "<MODIFIED_FLAG>%@</MODIFIED_FLAG>"
                             "</SYSDISCT>"
                             "</DocumentElement>",prodGroupStr,keyfield,custWarehouse,Stockcode,createdDate,@"2100-01-01T00:00:00",selectType,StrDiscountPer,isModified];
        return soapXml;
}

#pragma mark - WS calls


-(void)callWSGetWarehouse
{
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        strWebserviceType = @"WS_CUST_BRANCH";
        
        NSString *strURL = [NSString stringWithFormat:@"%@get_sowarehouse.php?branch=%@",WSURL,custBranchCode];
        
        [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
        [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
        [spinner removeFromSuperview];
        
        NSMutableData *responseData = (NSMutableData*)resData;
        
        NSError* error;
        NSMutableDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                            options:kNilOptions
                                                                              error:&error];
        
        NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
        
        NSString * result = [responseDict objectForKey:@"result"];
        if([result boolValue] == 1 )
        {
                custWarehouse = [responseDict objectForKey:@"warehouse"];
        }
        [spinnerObj removeFromSuperview];
}

-(void)ASIHTTPRequest_Error:(id)error
{
        NSLog(@"error %@",error);
        [spinner removeFromSuperview];
        
        NSError *err = error;
        NSString *strErrorMessage;
        if (err.code == 2)
        {
                strErrorMessage = SERVER_OFFLINE_MESSAGE;
        }
        else
        {
                strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
        }
        
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:strErrorMessage
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        strErrorMessage = nil;
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
        
        switch (newtworkStatus)
        {
                case NotReachable:
                {
                        [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
                        break;
                }
                case ReachableViaWiFi:
                {
                        [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
                        break;
                }
                case ReachableViaWWAN:
                {
                        [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
                        break;
                }
                        
        }
        
        [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
        
}

@end
