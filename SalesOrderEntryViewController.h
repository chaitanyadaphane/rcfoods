//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import "StorePickupViewController.h"
#import "SalesOrderProductDetailsViewController.h"
#import "QuoteMenuPopOverViewController.h"
#import "QuoteProductComment.h"
#import "CashPickUpPopOverController.h"
#import "DeliveryRunViewController.h"
#import "DatePopOverController.h"
#import "StockPickupViewController.h"

@interface SalesOrderEntryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate,UITextFieldDelegate,StorePickupViewDelegate,StockPickupViewDelegate,SetPriceDelegate,DismissPopOverDelegateForMenuList,UIPopoverControllerDelegate,DismissPopOverDelegateForCashPickUpDelegate,SetDeliveryRunDelegate,UIAlertViewDelegate,SetCommentDelegate,SetDeliveryDateDelegate,UITextFieldDelegate,UISearchBarDelegate
>{
        
    NSString *strWebserviceType;
    
    NSMutableArray *arrProductLabels;
    
    NSMutableArray *arrProducts;
    
    MBProgressHUD *spinner;
    NSString *strPickupForm;
    NSString *strProductName;
    NSString *strProductCode;
    NSString *strWarehouse;
    
    dispatch_queue_t backgroundQueueForSalesOrderEntry;
    FMDatabaseQueue *dbQueueSalesOrderEntry;
    
    BOOL isNetworkConnected;
    
    NSString *strOrderNum;
    NSMutableDictionary *dictHeaderDetails;
    
    SEFilterControl *segControl;
    
    CALayer *layerPointingImage;
    
    
    NSString *isCashPickUpOn;
    NSString *isStockPickUpOn;
    NSString *isPickupFormOn;

    NSString *isFuelLevySelected;
    
    float totalCost;
    int totalLines;

    NSString *strCashPickUp;
    NSString *strChequeAvailable;
    
    BOOL isDeliveryRunSlected;
    
    BOOL isEditable;
    
    NSString *strSelectedOrderNum;
    
    NSIndexPath *deleteIndexPath;
    
    BOOL isCommentExist;
    
    NSInteger selectedIndex;
    NSInteger selectedSectionIndex;
    BOOL isCommentAddedOnLastLine;
    
    NSString *strPriceChanged;
    NSMutableArray *arrSearchProducts;
    
    int cntViewWillAppear;
}


@property (nonatomic,assign) BOOL isSearch;

@property (nonatomic,retain) NSMutableDictionary *dictHeaderDetails;
@property (nonatomic,retain) NSString *strProductName;
@property (nonatomic,retain) NSString *strProductCode;
@property (nonatomic,retain) NSString *strWarehouse;
@property (nonatomic,retain) NSString *strOperatorName;

@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) MBProgressHUD *spinner;
@property (nonatomic,retain) NSString *strOrderNum;
@property (nonatomic,retain) NSMutableArray *arrProductLabels;
@property (nonatomic,retain) NSString *isCashPickUpOn;
@property (assign) IBOutlet UILabel *lblTotalCost;
@property (nonatomic,assign) IBOutlet UILabel *lblTotalLines;

@property (assign) IBOutlet UIButton *btnAddMenu;
@property (nonatomic,retain) UIPopoverController *popoverController;
@property (nonatomic,retain) NSString *isFuelLevySelected;
@property (nonatomic,retain) NSString *strCashPickUp;
@property (nonatomic,retain) NSString *strChequeAvailable;
@property (nonatomic,retain) NSString *isStockPickUpOn;
@property (nonatomic,retain) NSString *isPickupFormOn;

@property (nonatomic,retain) NSString *strSelectedOrderNum;
@property (nonatomic,assign) BOOL isEditable;


@property (weak, nonatomic) IBOutlet UISearchBar *txtSearchSalesOrderEntryProducts;
@property (nonatomic,assign) IBOutlet UILabel *lblCustomerName;
@property (nonatomic,retain) NSIndexPath *deleteIndexPath;

@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnDelete;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoProducts;
@property (nonatomic,retain) IBOutlet UILabel *lblProductName;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowDownwards;

//Delivery
@property (nonatomic,retain) NSString *strDelName;
@property (nonatomic,retain) NSString *strDelAddress1;
@property (nonatomic,retain) NSString *strDelAddress2;
@property (nonatomic,retain) NSString *strDelAddress3;
@property (nonatomic,retain) NSString *strDelSubUrb;
@property (nonatomic,retain) NSString *strDelPostCode;
@property (nonatomic,retain) NSString *strDelCountry;
@property (nonatomic,retain) NSString *strDeliveryRun;
@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,retain) NSString *strDelInst1;
@property (nonatomic,retain) NSString *strDelInst2;
@property (nonatomic,retain) NSString *strCustOrder;
@property (nonatomic,retain) NSString *strContact;
@property (nonatomic,retain) NSString *strDelDate;

@property (nonatomic,retain)NSString *strAddress1;
@property (nonatomic,retain)NSString *strAddress2;
@property (nonatomic,retain)NSString *strAddress3;
@property (nonatomic,retain)NSString *strSubUrb;
@property (nonatomic,retain)NSString *strAllowPartial;
@property (nonatomic,retain)NSString *strBranch;
@property (nonatomic,retain)NSString *strCarrier;
@property (nonatomic,retain)NSString *strChargeRate;
@property (nonatomic,retain)NSString *strChargetype;
@property (nonatomic,retain)NSString *strCountry;
@property (nonatomic,retain)NSString *strDirect;
@property (nonatomic,retain)NSString *strDropSequence;
@property (nonatomic,retain)NSString *strExchangeCode;
@property (nonatomic,retain)NSString *strExportDebtor;
@property (nonatomic,retain)NSString *strHeld;
@property (nonatomic,retain)NSString *strName;
@property (nonatomic,retain)NSString *strNumboxes;
@property (nonatomic,retain)NSString *strOrderDate;
@property (nonatomic,retain)NSString *strPeriodRaised;
@property (nonatomic,retain)NSString *strPostCode;
@property (nonatomic,retain)NSString *strPriceCode;
@property (nonatomic,retain)NSString *strSalesBranch;
@property (nonatomic,retain)NSString *strSalesman;
@property (nonatomic,retain)NSString *strTaxExemption1;
@property (nonatomic,retain)NSString *strTaxExemption2;
@property (nonatomic,retain)NSString *strTaxExemption3;
@property (nonatomic,retain)NSString *strTaxExemption4;
@property (nonatomic,retain)NSString *strTaxExemption5;
@property (nonatomic,retain)NSString *strTaxExemption6;
@property (nonatomic,retain)NSString *strTradingTerms;
@property (nonatomic,retain)NSString *strYearRaised;
@property (nonatomic,retain)NSString *strStockPickup;
@property (nonatomic,retain)NSString *strPickupInfo;

@property (nonatomic,retain)NSString *strPickupForm;
@property (nonatomic,retain)NSString *strTotalWeight;
@property (nonatomic,retain)NSString *strTotalVolume;
@property (nonatomic,retain)NSString *strTotalTax;
@property (nonatomic,retain)NSString *strTotalCost;
@property (nonatomic,retain)NSString *strTotalLines;
@property (nonatomic,retain)NSString *strOperator;
@property (nonatomic,retain)NSString *strSalesType;
@property (nonatomic,retain)NSString *strUploadDate;
@property (nonatomic,retain)NSString *strSuccessMessage;
@property (nonatomic,retain)NSString *strDateRaised;
@property (nonatomic,retain)NSString *strCommenttxt;
@property (nonatomic,retain)NSString *strCommentDateCreated;
@property (nonatomic,retain)NSString *strCommentFollowDate;

@property (nonatomic,retain) NSString *strPriceChanged;

@property (nonatomic,retain) NSString *strBalanceVal;

//Static Values
@property (nonatomic,retain)NSString *strExchangeRate;
@property (nonatomic,retain)NSString *strTotalCartonQty;
@property (nonatomic,retain)NSString *strActive;
@property (nonatomic,retain)NSString *strEditStatus;
@property (nonatomic,retain)NSString *strStatus;
@property (nonatomic,retain)NSString *strRefreshPrice;
@property (nonatomic,retain)NSString *strBOQty;
@property (nonatomic,retain)NSString *strConvFactor;
@property (nonatomic,retain)NSString *strDecimalPlaces;
@property (nonatomic,retain)NSString *strOrigCost;

@property (nonatomic,assign)BOOL isCommentExist;

@property (nonatomic,assign)BOOL isCommentAddedOnLastLine;

@property (nonatomic,assign)int finalise;

//-(void)callGetProductListDetailsFromDB:(FMDatabase *)db;
-(void)callWSGetNewOrderNumber;
-(void)callWSSaveSalesOrder;
-(void)callGetSalesOrderDetailsFromDB;
@end
