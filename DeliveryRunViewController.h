//
//  DeliveryRunViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetDeliveryRunDelegate
- (void)setDeliveryRun:(NSString *)strDelRun;
@end

@interface DeliveryRunViewController : UIViewController{
    NSMutableArray *arrDeliveryRun;
    
    NSString *strWebserviceType;
    BOOL isNetworkConnected;
    
    id<SetDeliveryRunDelegate> __unsafe_unretained  delegate;
}

@property(nonatomic,retain)NSMutableArray *arrDeliveryRun;
@property(unsafe_unretained)id<SetDeliveryRunDelegate>delegate;
@property (nonatomic, retain) NSIndexPath *lastIndexPath;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDeliveryRun;

-(void)callWSGetDeliveryRunList;

@end
