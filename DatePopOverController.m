//
//  DeliveryRunViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "DatePopOverController.h"

@interface DatePopOverController ()

@end

@implementation DatePopOverController
@synthesize delegate,datePicker,lblDate;
@synthesize isStockPickup,isCallDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!isStockPickup) {
        datePicker.minimumDate = [NSDate date];
    }
}

- (void)viewDidUnload{
    [super viewDidUnload];
}


-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:animated];
    
    if (isStockPickup == YES) {
        _popTitleLbl.text = @"Call Date";
    }
    else{
        _popTitleLbl.text = @"Delivery Date";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dateChanged:(id)sender
{
    NSString *strDate = [[AppDelegate getAppDelegateObj] dateStringForAPIUsingDate:datePicker.date];
    lblDate.text = strDate;
    strDate = nil;
}

- (IBAction)dateSet:(id)sender{
    NSString *strDate = [[AppDelegate getAppDelegateObj] dateStringForAPIUsingUnixDate:datePicker.date];
    
    if (isCallDate) {
        [delegate setCallDate:strDate];
    }
    else{
        [delegate setDeliveryDate:strDate];

    }
    
    strDate = nil;
    [delegate dismissPopover];
}

- (IBAction)dismissPopover:(id)sender{
    [delegate dismissPopover];
}

@end
