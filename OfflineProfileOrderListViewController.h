//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "ASINetworkQueue.h"
#import <MessageUI/MessageUI.h>
#import "NDHTMLtoPDF.h"

@interface OfflineProfileOrderListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ReachabilityRequest_delegate,MFMailComposeViewControllerDelegate>{

    NSString *strWebserviceType;
    int currPage;
    int totalCount;
    int recordNumber;
   // int line_num;
    UIActivityIndicatorView *localSpinner;
    NSMutableArray *arrQuotes; // The master content.
    NSMutableArray *arrQuotesrForWebservices;
  //  UIButton *button ;
    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.

    NSString *strCustCode;
    
    NSString *strPriceChange;
    
    IBOutlet UIView *vwCustomLoadProfileTip;

    
    dispatch_queue_t backgroundQueueForUploadQuote;;
    FMDatabaseQueue *dbQueueQuoteList;
    
    BOOL isQuoteEntryModule;    
    BOOL isNetworkConnected;
    //********ketaki code modified**********//
    BOOL ischecked;
    
    
    NSString *strOldOrderNum;
    NSMutableArray *arrProducts;
    NSMutableDictionary *dictHeaderDetails;
    
    NSIndexPath *deleteIndexPath;
    
    BOOL isProfileOrder;
    NSOperationQueue *operationQueue;

    ASINetworkQueue *operationQueueUpload;
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrQuotes;
@property (nonatomic, retain) NSMutableArray *arrQuotesrForWebservices;
@property (nonatomic, retain) NSString *strCustCode;

@property (nonatomic, retain) NSString *strPriceChange;


@property (nonatomic,retain) NSMutableArray *arrQuoteDetails;
@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) NSMutableDictionary *dictHeaderDetails;
@property (nonatomic,retain) NSIndexPath *deleteIndexPath;
@property (nonatomic,assign) BOOL isProfileOrder;

@property (nonatomic, retain) NSTimer *timerForUpload;
@property (nonatomic, retain) NSMutableArray *arrSelectedOrders;

@property (nonatomic,unsafe_unretained)IBOutlet UILabel *lblStatusText;
@property (nonatomic,unsafe_unretained)IBOutlet UIButton *btnDelete;
@property (nonatomic,unsafe_unretained)IBOutlet UIProgressView *progessView;
@property (nonatomic,unsafe_unretained)IBOutlet UIButton *btnUpload;
@property (nonatomic,unsafe_unretained)IBOutlet UILabel *lblNoOrders;
@property (nonatomic, unsafe_unretained)IBOutlet UIButton *btnLoad;//
@property (nonatomic, unsafe_unretained) IBOutlet UITableView *tblQuoteList;
@property (nonatomic, retain)IBOutlet UIView *vwContentSuperview;
@property (nonatomic, retain)IBOutlet UIView *vwNoQuotes;
@property (nonatomic, retain)IBOutlet UIView *vwTblQuotes;
@property (nonatomic, retain) IBOutlet UIButton *selectAllBtn;

@property (atomic, retain) NSString *strDeliveryDate;
@property (atomic, retain) NSString *strContact;
@property (atomic, retain) NSString *strAddress1;
@property (atomic, retain) NSString *strAddress2;
@property (atomic, retain) NSString *strAddress3;
@property (atomic, retain) NSString *strSubUrb;
@property (atomic, retain) NSString *strAllowPartial;
@property (atomic, retain) NSString *strBranch;
@property (atomic, retain) NSString *strCarrier;
@property (atomic, retain) NSString *strChargeRate;
@property (atomic, retain) NSString *strChargetype;
@property (atomic, retain) NSString *strCountry;
@property (atomic, retain) NSString *strDirect;
@property (atomic, retain) NSString *strDropSequence;
@property (atomic, retain) NSString *strExchangeCode;
@property (atomic, retain) NSString *strExportDebtor;
@property (atomic, retain) NSString *strHeld;
@property (atomic, retain) NSString *strNumboxes;
@property (atomic, retain) NSString *strOrderDate;
@property (atomic, retain) NSString *strPeriodRaised;
@property (atomic, retain) NSString *strPostCode;
@property (atomic, retain) NSString *strPriceCode;
@property (atomic, retain) NSString *strSalesBranch;
@property (atomic, retain) NSString *strSalesman;
@property (atomic, retain) NSString *strTaxExemption1;
@property (atomic, retain) NSString *strTaxExemption2;
@property (atomic, retain) NSString *strTaxExemption3;
@property (atomic, retain) NSString *strTaxExemption4;
@property (atomic, retain) NSString *strTaxExemption5;
@property (atomic, retain) NSString *strTaxExemption6;
@property (atomic, retain) NSString *strTradingTerms;
@property (atomic, retain) NSString *strYearRaised;
@property (atomic, retain) NSString *strStockPickup;
@property (atomic, retain) NSString *strPickupInfo;

@property (atomic, retain) NSString *strWarehouse;
@property (atomic, retain) NSString *strCashRepPickUp;
@property (atomic, retain) NSString *strCashAmount;
@property (atomic, retain) NSString *strCashCollect;
@property (atomic, retain) NSString *strDateRaised;
@property (atomic, retain) NSString *strDelName;
@property (atomic, retain) NSString *strOperator;
@property (atomic, retain) NSString *strStatus;
@property (atomic, retain) NSString *strOrderNum;
@property (atomic, retain) NSString *strDelInst1;
@property (atomic, retain) NSString *strDelInst2;
@property (atomic, retain) NSString *strDeliveryRun;
@property (atomic, retain) NSString *strDebtor;
@property (atomic, retain) NSString *strCustOrder;
@property (atomic, retain) NSString *strFuelLevy;
@property (atomic, retain) NSString *strOrderValue;
@property (atomic, retain) NSString *strTotalLines;
@property (atomic, retain) NSString *stExchangeRate;
@property (atomic, retain) NSString *strTotalCartonQuantity;
@property (atomic, retain) NSString *strActive;
@property (atomic, retain) NSString *strEditStatus;
@property (atomic, retain) NSString *strTotalWeight;
@property (atomic, retain) NSString *strTotalVolume;
@property (atomic, retain) NSString *strTotalTax;
@property (atomic, retain) NSString *strUploadDate;
@property (atomic, retain) NSString *strSuccessMessage;

@property (atomic, retain) NSString *strOldOrderNum;
@property (atomic, retain) NSString *strOperatorName;
//Static Values
@property (nonatomic,retain)NSString *strExchangeRate;
@property (nonatomic,retain)NSString *strRefreshPrice;
@property (nonatomic,retain)NSString *strBOQty;
@property (nonatomic,retain)NSString *strConvFactor;
@property (nonatomic,retain)NSString *strDecimalPlaces;
@property (nonatomic,retain)NSString *strOrigCost;

@property (nonatomic, assign) BOOL isUploadInProgress;




@end
