//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "QuoteHistoryCustomerListViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryQuoteListViewController.h"
#import "CustomerTransactionViewController.h"
#import "AddQuoteViewController.h"
#import "CustomerOrderFormViewController.h"
#import "SalesOrderEntryViewController.h"
#import "ProfileOrderEntryViewController.h"
#import "CustomerSpecialsViewController.h"
#import "OfflineQuoteListViewController.h"
#import "ProfileMaintenanceViewController.h"

#import "UserCallScheduleViewController.h"

#define QUOTE_HISTORY_CUSTOMER_LIST_WS @"quote/custquote.php?"
#define QUOTE_DEBTOR_DETAILS_WS @"quote/debtordetails_quote.php?"
#define SALE_DEBTOR_DETAILS_WS @"salesorder/debtordetails_order.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

#define ADMIN @"admin"
#define TELE_SALES @"telesales"

@interface QuoteHistoryCustomerListViewController ()
{
    BOOL _loadingInProgress;
    NSString *strBranchList;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;
@end

@implementation QuoteHistoryCustomerListViewController
@synthesize listContent,filteredListContent,arrCustomer,isFromLoadProfile,arrCustomerForWebservices,spinner,lastIndexPath,mainDict,tblQuoteList,isSearchMode;
@synthesize selectedUserStr,isUserCallSchedule;
@synthesize callDay;

@synthesize searchingBar,vwResults,vwContentSuperview,vwNoResults,localSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
    {
        CGRect frm =     searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
        [self.searchingBar setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];
    }
    else{
        CGRect frm =     searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
        [self.searchingBar setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.isSearchMode = NO;
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrCustomer = [[NSMutableArray alloc] init];
    
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    
    //First page
    currPage = 1;
    totalCount = 0;
    recordNumber = 0;
    
    if (isUserCallSchedule) {
        
        operationQueue = [NSOperationQueue new];
        backgroundQueueForCustomerList = dispatch_queue_create("com.nanan.myscmipad.bgqueueForCustomerList", NULL);
        
        [localSpinner startAnimating];
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                NSString *strQuery = [NSString stringWithFormat:@"SELECT s.SALESPERSON as Salesperson, s.CODE as Code, a.NAME as Name, s.TIME as Time ,a.RECNUM as RECNUM, a.EMAIL1 as Email1,a.CONTACT1 as Contact1,a.CENTRAL_DEBTOR as CentralDebtor,a.BRANCH as branch FROM smdaytim as s  JOIN  armaster  as a ON s.CODE = a.CODE WHERE s.SALESPERSON = '%@' AND s.DAY = '%d'",selectedUserStr,callDay];// AND s.TIME  BETWEEN   '0800' AND '2000' GROUP BY s.CODE
                
                FMResultSet *res = [db executeQuery:strQuery] ;
                
                if (!res)
                {
                    NSLog(@"Error: %@", [db lastErrorMessage]);
                    return;
                }
                
                if ([res next]) {
                    totalCount = [[res resultDictionary] count];
                }
                
                [res close];
                
                [self callGetCustomerListBasedOnSelectedUser:db];
                
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [self doAfterDataFetched];
                [localSpinner stopAnimating];
            }];
        }];
        
    }
    else{
        
        
        
        strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];
        NSString *strMember = [[NSUserDefaults standardUserDefaults] objectForKey:@"members"];
        
        
        if ([strMember caseInsensitiveCompare:ADMIN]  == NSOrderedSame || [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame)
        {
            
            operationQueue = [NSOperationQueue new];
            backgroundQueueForCustomerList = dispatch_queue_create("com.nanan.myscmipad.bgqueueForCustomerList", NULL);
            
            [localSpinner startAnimating];
            [operationQueue addOperationWithBlock:^{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    FMResultSet *res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM armaster"] ;
                    
                    if (!res)
                    {
                        NSLog(@"Error: %@", [db lastErrorMessage]);
                        return;
                    }
                    
                    if ([res next]) {
                        totalCount = [[[res resultDictionary] objectForKey:@"totalCount"] intValue];
                    }
                    
                    [res close];
                    
                    [self callGetCustomerListFromDB:db];
                    
                    
                }];
                
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    [self doAfterDataFetched];
                    [localSpinner stopAnimating];
                }];
            }];
            
        }
        else
        {
            
            operationQueue = [NSOperationQueue new];
            backgroundQueueForCustomerList = dispatch_queue_create("com.nanan.myscmipad.bgqueueForCustomerList", NULL);
            
            [localSpinner startAnimating];
            [operationQueue addOperationWithBlock:^{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    FMResultSet *res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM armaster as a WHERE a.BRANCH = ?",strBranchList] ;
                    
                    if (!res)
                    {
                        NSLog(@"Error: %@", [db lastErrorMessage]);
                        return;
                    }
                    
                    if ([res next]) {
                        totalCount = [[[res resultDictionary] objectForKey:@"totalCount"] intValue];
                    }
                    
                    [res close];
                    
                    [self callGetCustomerListFromDB:db];
                    
                    
                }];
                
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    [self doAfterDataFetched];
                    [localSpinner stopAnimating];
                }];
            }];
            
        }
        
    }
    
    
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblQuoteList = nil;
    self.searchingBar = nil;
    self.vwResults = nil;
    self.vwContentSuperview = nil;
    self.vwNoResults = nil;
    self.localSpinner = nil;
    
}

- (void)doAfterDataFetched{
    if([arrCustomer count] < totalCount){
        [tblQuoteList reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        
        recordNumber += ROWS_PER_PAGE;
        
    }
    else{
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
    {
        return [self.filteredListContent count];
    }
    else
    {
        return [self.arrCustomer count];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:7];
        cell.showsReorderControl = YES;
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (isSearchMode)
    {
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Name"]];
        cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
        
    }
    else
    {
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Name"]];
        cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
    }
    
    if ([AppDelegate getAppDelegateObj].intCurrentMenuId == 0){
        
        //Profile Order Entry
        if ([AppDelegate getAppDelegateObj].intCurrentSubMenuId == 0) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[ProfileOrderEntryViewController class]]) {
                
                //cell.btnDetailView.hidden = TRUE;
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
        
        //Sales Order Entry
        if ([AppDelegate getAppDelegateObj].intCurrentSubMenuId == 1) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[SalesOrderEntryViewController class]]) {
                
                //cell.btnDetailView.hidden = TRUE;
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
        
        //Customer Order check
        if ([AppDelegate getAppDelegateObj].intCurrentSubMenuId == 2) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[SalesOrderEntryViewController class]]) {
                
                //cell.btnDetailView.hidden = TRUE;
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    
    
    //Quote Entry Module
    if ([AppDelegate getAppDelegateObj].intCurrentMenuId == 1){
        
        if ([AppDelegate getAppDelegateObj].intCurrentSubMenuId == 0) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[AddQuoteViewController class]]) {
                
                if (isFromLoadProfile) {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                
                
                if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else{
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Resign the search bar keyboard
    [self.searchingBar resignFirstResponder];
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    NSLog(@"subMenuId==%d",subMenuId);
    
    if (menuId == 0) {
        //Profile Order Entry
        if (subMenuId == 0)
        {
            CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
            int newRow = [indexPath row];
            int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
            
            if(newRow != oldRow)
            {
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
                lastIndexPath = indexPath;
            }
            
            if (isSearchMode){
                [operationQueue addOperationWithBlock:^{
                    
                    NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self callGetSalesDetailsDetailsFromDB:strCode];
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                            object:self
                                                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                    }];
                }];
            }
            else{
                
                [operationQueue addOperationWithBlock:^{
                    
                    NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self callGetSalesDetailsDetailsFromDB:strCode];
                    
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                            object:self
                                                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                    }];
                    
                    
                }];
                
            }
        }
        //Sales Order Entry
        else if (subMenuId == 1) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[SalesOrderEntryViewController class]]) {
                
                CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                int newRow = [indexPath row];
                int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                
                if(newRow != oldRow)
                {
                    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                    oldCell.accessoryType = UITableViewCellAccessoryNone;
                    lastIndexPath = indexPath;
                }
                
                
                if (isSearchMode) {
                    
                    [operationQueue addOperationWithBlock:^{
                        
                        NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        
                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                                object:self
                                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                        }];
                    }];
                }
                else{
                    [operationQueue addOperationWithBlock:^{
                        NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        
                        
                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                                object:self
                                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                        }];
                        
                        
                    }];
                    
                    
                }
                
            }
        }
        //Customer Order check
        else if (subMenuId == 2) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[SalesOrderEntryViewController class]]) {
                
                
                CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                int newRow = [indexPath row];
                int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                
                if(newRow != oldRow)
                {
                    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                    oldCell.accessoryType = UITableViewCellAccessoryNone;
                    lastIndexPath = indexPath;
                }
                
                
                if (isSearchMode){
                    
                    NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [self callGetSalesDetailsDetailsFromDB:strCode];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                        object:self
                                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                    
                }
                else{
                    
                    NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [self callGetSalesDetailsDetailsFromDB:strCode];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                        object:self
                                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                }
                
            }
        }
        // USer Call Management
        /*else if (subMenuId == 5) {
            {
                int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
                
                int indexOfPreviousController =  indexOfCurrentController -1;
                
                id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
                
                if ([classOfPreviousController isKindOfClass:[UserCallScheduleViewController class]]) {
                    
                    
                    CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                    int newRow = [indexPath row];
                    int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                    
                    if(newRow != oldRow)
                    {
                        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                        oldCell.accessoryType = UITableViewCellAccessoryNone;
                        lastIndexPath = indexPath;
                    }
                    
                    
                    if (isSearchMode){
                        
                        NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                            object:self
                                                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                        
                    }
                    else{
                        
                        NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                    }
                    
                }
            }
        }
         */
        else if (subMenuId == 5) {
            {
                int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
                
                int indexOfPreviousController =  indexOfCurrentController -1;
                
                id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
                
                if ([classOfPreviousController isKindOfClass:[ProfileMaintenanceViewController class]]) {
                    
                    
                    CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                    int newRow = [indexPath row];
                    int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                    
                    if(newRow != oldRow)
                    {
                        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                        oldCell.accessoryType = UITableViewCellAccessoryNone;
                        lastIndexPath = indexPath;
                    }
                    
                    
                    if (isSearchMode){
                        
                        NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                            object:self
                                                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                        
                    }
                    else{
                        
                        NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        [self callGetSalesDetailsDetailsFromDB:strCode];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                    }
                    
                }
            }
        }
    }
    
    //Quote Module
    if (menuId == 1){
        
        //Add Quote
        if (subMenuId == 0) {
            
            int indexOfCurrentController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack indexOfObject:self];
            
            int indexOfPreviousController =  indexOfCurrentController -1;
            
            id classOfPreviousController = [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController.viewControllersStack objectAtIndex:indexOfPreviousController];
            
            if ([classOfPreviousController isKindOfClass:[AddQuoteViewController class]]) {
                
                //Load Profile
                if (isFromLoadProfile) {
                    
                    NSString *strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
                    
                    QuoteHistoryQuoteListViewController *dataViewController = [[QuoteHistoryQuoteListViewController alloc] initWithNibName:@"QuoteHistoryQuoteListViewController" bundle:[NSBundle mainBundle]];
                    
                    dataViewController.strCustCode = strCustNum;
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    
                }
                //Debtors list
                else{
                    
                    CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
                    int newRow = [indexPath row];
                    int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
                    
                    if(newRow != oldRow)
                    {
                        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                        UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                        oldCell.accessoryType = UITableViewCellAccessoryNone;
                        lastIndexPath = indexPath;
                    }
                    
                    
                    if (isSearchMode) {
                        
                        [operationQueue addOperationWithBlock:^{
                            
                            NSString *strCode = [[[filteredListContent objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                            [self callGetViewQuoteDetailsDetailsFromDB:strCode];
                            
                            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                                    object:self
                                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                            }];
                        }];
                    }
                    else{
                        [operationQueue addOperationWithBlock:^{
                            NSString *strCode = [[[arrCustomer objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                            [self callGetViewQuoteDetailsDetailsFromDB:strCode];
                            
                            
                            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                                    object:self
                                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:mainDict, @"source", nil]];
                            }];
                            
                            
                        }];
                        
                        
                    }
                    
                }
                
            }
        }
        
        //Quote History Enquiry
        if (subMenuId == 1 )
        {
            
            NSString *strCustNum;
            if (isSearchMode)
            {
                strCustNum = [[self.filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            else
            {
                strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            
            QuoteHistoryQuoteListViewController *dataViewController = [[QuoteHistoryQuoteListViewController alloc] initWithNibName:@"QuoteHistoryQuoteListViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustCode = strCustNum;
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
        
        //Pending
        if (subMenuId == 3 ) {
            
            NSLog(@"Pending Tasks");
            
            NSString *strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            
            OfflineQuoteListViewController *dataViewController = [[OfflineQuoteListViewController alloc] initWithNibName:@"OfflineQuoteListViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustCode = strCustNum;
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
        
    }
    
    //Customer Info
    if (menuId == 3){
        
        //Customer Transaction
        if(subMenuId == 0) {
            
            NSString *strCustNum;
            
            if (isSearchMode){
                strCustNum = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            else{
                strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            
            CustomerTransactionViewController *dataViewController = [[CustomerTransactionViewController alloc] initWithNibName:@"CustomerTransactionViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustCode = strCustNum;
            dataViewController.isFromDebtorHistory = NO;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            
        }
        
        //Debtor history
        if(subMenuId == 1) {
            
            NSString *strCustNum;
            
            if (isSearchMode){
                strCustNum = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            else{
                strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            
            CustomerTransactionViewController *dataViewController = [[CustomerTransactionViewController alloc] initWithNibName:@"CustomerTransactionViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustCode = strCustNum;
            dataViewController.isFromDebtorHistory = YES;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            
            
        }
        
        if(subMenuId == 2) {
            
            NSString *strCustNum;
            
            if (isSearchMode){
                strCustNum = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            else{
                strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            
            CustomerOrderFormViewController *dataViewController = [[CustomerOrderFormViewController alloc] initWithNibName:@"CustomerOrderFormViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustomerCode = strCustNum;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            
            
        }
        //Special Price
        if(subMenuId == 3) {
            
            NSString *strCustNum;
            
            if (isSearchMode){
                strCustNum = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            else{
                strCustNum = [[arrCustomer objectAtIndex:[indexPath row]] objectForKey:@"Code"];
            }
            
            CustomerSpecialsViewController *dataViewController = [[CustomerSpecialsViewController alloc] initWithNibName:@"CustomerSpecialsViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strCustCode = strCustNum;
            dataViewController.isFromDebtorHistory = NO;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            
        }
        
    }
    
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.isDragging) {
        
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
            [_loadMoreFooterView setState:StateNormal];
            
        } else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
            [_loadMoreFooterView setState:StatePulling];
        }
    }
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrCustomer count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}


#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetCustomerListFromDB:db];
            
        }];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
}


- (void) didFinishLoadingMoreSampleData
{
    _loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f,0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void)freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    /*
     Update the filtered array based on the search text and scope.
     */
    
    [self.filteredListContent removeAllObjects]; // First clear the filtered array.
    
    [operationQueue cancelAllOperations];
    
    //    if ([scope caseInsensitiveCompare:@"Debtor Name"] && ![searchText caseInsensitiveCompare:@""]){
    
    
    if ([scope caseInsensitiveCompare:@"Debtor Name" ] == NSOrderedSame  || [scope isEqualToString:@"Debtor Name"]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerListByDebtorName:db DebtorName:searchText];
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContent count]) {
                    [vwContentSuperview addSubview:vwResults];
                    [tblQuoteList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
                
            }];
        }];
        
    }
    //    else if ([scope isEqualToString:@"Debtor Number"] && ![searchText isEqualToString:@""]){
    else if ([scope caseInsensitiveCompare:@"Debtor Number" ] == NSOrderedSame || [scope isEqualToString:@"Debtor Number"]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerListByDebtorCode:db DebtorCode:searchText];
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContent count]) {
                    [vwContentSuperview addSubview:vwResults];
                    [tblQuoteList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
                
            }];
        }];
    }
    
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        self.isSearchMode = YES;
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
        searchBar.tintColor = [UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0];
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblQuoteList reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblQuoteList reloadData];
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

#pragma mark - Database calls


-(void)getDebtorListNameForUser:(NSString *)salespersonSelected
{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            
            NSString *sqlQuery =[NSString stringWithFormat:@"SELECT * FROM smdaytim WHERE SALESPERSON =  '%@'",salespersonSelected];
            
            FMResultSet *rs = [db executeQuery:sqlQuery];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                NSDictionary *dictData = [rs resultDictionary];
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}


-(void)callGetCustomerListBasedOnSelectedUser:(FMDatabase *)db
{
    
    NSString *strUserSelectedInCall = selectedUserStr;
    NSString *strDay =[NSString stringWithFormat:@"%d",callDay];
    
    //[db open];
    
    FMResultSet *rs;
    
    @try {
        
        //            NSString *strQuery = [NSString stringWithFormat:@"SELECT s.SALESPERSON as Salesperson, s.CODE as Code, a.NAME as Name, s.TIME as Time ,a.RECNUM as RECNUM, a.EMAIL1 as Email1,a.CONTACT1 as Contact1,a.CENTRAL_DEBTOR as CentralDebtor,a.BRANCH as branch FROM smdaytim as s  JOIN  armaster  as a ON s.CODE = a.CODE WHERE s.SALESPERSON = '%@' AND s.DAY = %@ AND s.TIME  BETWEEN   '0800' AND '2000' GROUP BY s.CODE",strUserSelectedInCall,strDay];
        
        NSString *strQuery = [NSString stringWithFormat:@"SELECT s.SALESPERSON as Salesperson, s.CODE as Code, a.NAME as Name, s.TIME as Time ,a.RECNUM as RECNUM, a.EMAIL1 as Email1,a.CONTACT1 as Contact1,a.CENTRAL_DEBTOR as CentralDebtor,a.BRANCH as branch,a.DELIVERY_RUN as delivery_run FROM smdaytim as s  JOIN  armaster  as a ON s.CODE = a.CODE WHERE s.SALESPERSON = '%@' AND s.DAY = %@ ORDER BY DELIVERY_RUN",strUserSelectedInCall,strDay];
        
        rs = [db executeQuery:strQuery];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrCustomer addObject:dictData];
        }
        
        NSLog(@"arrCustomer:::::::%d",arrCustomer.count);
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        if(arrCustomer.count > 0)
        {
            [tblQuoteList reloadData];
        }
        else{
            if (arrCustomer.count == 0) {
                UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 25)];
                lbl.text = @"No Debtor Available";
                lbl.textColor = [UIColor whiteColor];
                [lbl setCenter:self.view.center];
            }
        }
    }];
    
}





-(void)callGetCustomerListFromDB:(FMDatabase *)db{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    
    NSString *username = [salesType substringFromIndex:1];
    NSString *strMember = [prefs objectForKey:@"members"];
    
    //[db open];
    
    FMResultSet *rs;
    
    @try {
        
        if ([strMember caseInsensitiveCompare:ADMIN]  == NSOrderedSame || [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            
            
            rs = [db executeQuery:@"SELECT RECNUM as RECNUM ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch, BRANCH as branch FROM armaster WHERE Name NOT LIKE  '%DO NOT USE%'"];
        }
        else{
            
            rs = [db executeQuery:@"SELECT RECNUM as RECNUM ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch,  BRANCH as branch FROM armaster WHERE branch = ? AND Name NOT LIKE '%DO NOT USE%' ORDER BY NAME LIMIT ?,?",strBranchList,[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];//username 5thmarch
            
        }
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrCustomer addObject:dictData];
        }
        
        NSLog(@"arrCustomer:::::::%d",arrCustomer.count);
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        if(arrCustomer.count > 0)
        {
            [tblQuoteList reloadData];
        }
        else{
            if (arrCustomer.count == 0) {
                UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 25)];
                lbl.text = @"No Debtor Available";
                lbl.textColor = [UIColor whiteColor];
                [lbl setCenter:self.view.center];
            }
        }
    }];
    
}

-(void)callGetCustomerListByDebtorName:(FMDatabase *)db DebtorName:(NSString *)debtorName{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    NSString *strMember = [prefs objectForKey:@"members"];
    NSString *username = [NSString stringWithFormat:@"%@",[salesType substringFromIndex:1]];
    NSString *strBranch = [prefs objectForKey:@"BRANCH"];
    FMResultSet *rs;
    
    @try {
        
        // if ([username caseInsensitiveCompare:ADMIN] || [username caseInsensitiveCompare:TELE_SALES]) {
        if ([strMember caseInsensitiveCompare:ADMIN]  == NSOrderedSame || [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE  NAME LIKE ? AND Name NOT LIKE '%DO NOT USE%'",[NSString stringWithFormat:@"%%%@%%", [debtorName trimSpaces]]];
            
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE branch = ? AND NAME LIKE ? AND Name NOT LIKE '%DO NOT USE%' ",strBranch,[NSString stringWithFormat:@"%%%@%%", [debtorName trimSpaces]]];//[username trimSpaces] 5thMarch
        }
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

-(void)callGetCustomerListByDebtorCode:(FMDatabase *)db DebtorCode:(NSString *)debtorCode{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    NSString *strBranch = [prefs objectForKey:@"BRANCH"];
    
    NSString *username = [NSString stringWithFormat:@"%@",[salesType substringFromIndex:1]];
    NSString *strMember = [prefs objectForKey:@"members"];
    FMResultSet *rs;
    
    @try {
        NSString *doNouseStr = @"%DO NOT USE%";
        if ([strMember caseInsensitiveCompare:ADMIN]  == NSOrderedSame || [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame) {
            
            NSString *strQuery = [NSString stringWithFormat:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE CODE LIKE '%@' AND Name NOT LIKE '%@'",[NSString stringWithFormat:@"%%%@%%", debtorCode],doNouseStr];
            
            
            rs = [db executeQuery:strQuery];
            
        }
        else{
            NSString *strQuery = [NSString stringWithFormat:@"SELECT SCM_RECNUM as ScmRecum ,NAME as Name ,EMAIL1 as Email1,CONTACT1 as Contact1,CODE as Code,CENTRAL_DEBTOR as CentralDebtor,CENTRAL_BRANCH as CentralBranch FROM armaster WHERE branch = '%@' AND CODE LIKE '%@' AND Name NOT LIKE '%@'",strBranch,[NSString stringWithFormat:@"%%%@%%", debtorCode],doNouseStr];//username 5thMarch
            
            rs = [db executeQuery:strQuery];
        }
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

-(void)callGetViewQuoteDetailsDetailsFromDB:(NSString *)strCode{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db) {
        @try {
            
            mainDict = [[NSMutableDictionary alloc] init];
            
            //Get Customer Details
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM armaster WHERE TRIM(CODE) = ?",strCode];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSDictionary *dictDataArmaster = nil;
            if ([rs1 next]) {
                
                dictDataArmaster = [rs1 resultDictionary];
                
                [mainDict setValue:[dictDataArmaster objectForKey:@"CODE"] forKey:@"Debtor"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"NAME"] forKey:@"Name"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS1"] forKey:@"Address1"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS2"] forKey:@"Address2"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"ADDRESS3"] forKey:@"Address3"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS1"] forKey:@"DelAddress1"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS2"] forKey:@"DelAddress2"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_ADDRESS3"] forKey:@"DelAddress3"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SUBURB"] forKey:@"Suburb"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"POST_CODE"] forKey:@"PostCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"COUNTRY"] forKey:@"Country"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_NAME"] forKey:@"DelName"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_SUBURB"]  forKey:@"DelSuburb"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_POST_CODE"] forKey:@"DelPostCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"DEL_COUNTRY"] forKey:@"DelCountry"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"CONTACT1"] forKey:@"Contact"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"BRANCH"] forKey:@"Branch"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"PRICE_CODE"] forKey:@"PriceCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SALESMAN"]  forKey:@"Salesman"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"SALES_BRANCH"] forKey:@"SalesBranch"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"TRADING_TERMS"] forKey:@"TradingTerms"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"EXCHANGE_CODE"] forKey:@"ExchangeCode"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"CHARGE_TYPE"] forKey:@"Chargetype"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"EXPORT_DEBTOR"] forKey:@"ExportDebtor"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"RATE"] forKey:@"ChargeRate"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"HOLD"] forKey:@"Held"];
                [mainDict setValue:[dictDataArmaster objectForKey:@"CARRIER"] forKey:@"CARRIER"];
                [mainDict setValue:@"0" forKey:@"Numboxes"];
                [mainDict setValue:@"N" forKey:@"Direct"];
                NSString *str_current_Date = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
                [mainDict setValue:str_current_Date forKey:@"DateRaised"];
                
            }
            
            [rs1 close];
            
            
            //Get Customer Details
            FMResultSet *rs2 = [db executeQuery:@"SELECT `DEBTOR`, `CONTACT`, `DEL_INST1` as DelInst1, `DEL_INST2` as DelInst2, `CUST_ORDER` as CustOrderno, `WAREHOUSE` as Warehouse from `soheader` WHERE DEBTOR = ?",strCode];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs2 next]) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[rs2 resultDictionary]];
                [mainDict addEntriesFromDictionary:dict];
                
            }
            
            [rs2 close];
            
            //Year & Period raised
            //Year & Period raised calculation
            int currentMonth = [[AppDelegate getCurrentMonth] intValue];
            int currentYear = [[AppDelegate getCurrentYear] intValue];
            int newmonth = 0;
            if(currentMonth < 7)
            {
                newmonth = currentMonth+6;
                currentYear = currentYear - 1;
            }
            else
            {
                newmonth = currentMonth-6;
            }
            
            NSString *strPeriodRaised = [NSString stringWithFormat:@"%d",newmonth];
            [mainDict setValue:strPeriodRaised forKey:@"PeriodRaised"];
            [mainDict setValue:[NSString stringWithFormat:@"%d",currentYear] forKey:@"YearRaised"];
            
            
            //            FMResultSet *rs3 = [db executeQuery:@"SELECT SA_PERIOD,SA_FIN_YEAR FROM syscont WHERE RECNUM = 1"];
            //
            //            if (!rs3)
            //            {
            //                [rs3 close];
            //                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            //                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            //            }
            //
            //            if ([rs3 next]) {
            //                NSDictionary *dictData = [rs3 resultDictionary];
            //                [mainDict setValue:[dictData objectForKey:@"SA_PERIOD"] forKey:@"PeriodRaised"];
            //                [mainDict setValue:[dictData objectForKey:@"SA_FIN_YEAR"] forKey:@"YearRaised"];
            //                dictData=nil;
            //            }
            
            
            FMResultSet *rs34= [db executeQuery:@"SELECT DELIVERY_RUN,DELIVERY_RUN1,DELIVERY_RUN2,DELIVERY_RUN3,DELIVERY_RUN4,DELIVERY_RUN5,DELIVERY_RUN6,DELIVERY_RUN7,DROP_SEQUENCE FROM armaster WHERE trim(`CODE`) = ?",[strCode trimSpaces]];
            
            if (!rs34)
            {
                [rs34 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs34 next]) {
                
                NSDictionary *dictData = [rs34 resultDictionary];
                NSLog(@"%@",dictData);
                NSString *strDeliveryRun;
                
                if(![[dictData objectForKey:@"DELIVERY_RUN"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN1"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN1"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN1"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN2"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN2"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN2"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN3"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN3"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN3"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN4"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN4"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN4"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN5"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN5"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN5"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN6"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN6"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN6"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN7"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN7"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN7"] trimSpaces]];
                }
                
                [mainDict setValue:[dictData objectForKey:@"DROP_SEQUENCE"] forKey:@"DropSequence"];
                
                FMResultSet *rs4= [db executeQuery:@"SELECT VALUE as yPosition FROM sysdesc WHERE trim(`CODE`) = ?",[strDeliveryRun trimSpaces]];
                
                if (!rs4)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                //Calculate delivery date
                if ([rs4 next]) {
                    
                    NSDictionary *dictData = [rs4 resultDictionary];
                    
                    NSLog(@"dictData4 %@", dictData);
                    NSRange range = [[dictData objectForKey:@"yPosition"] rangeOfString:@"Y"];
                    
                    int yposition = range.location + 1;
                    
                    int currDayOfWeek = [[AppDelegate getCurrentDayOfWeek2] intValue];
                    
                    int interval;
                    if (currDayOfWeek == 1 ) {
                        interval = 9;
                    }
                    else{
                        interval = currDayOfWeek + 1;
                    }
                    
                    interval = 9 - interval;
                    
                    int tempPosition = interval + yposition;
                    
                    int temp;
                    
                    if(tempPosition >= 7)
                    {
                        temp = tempPosition % 7;
                    }
                    else
                    {
                        temp = tempPosition;
                    }
                    
                    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*(temp)];
                    NSString *strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:newDate];
                    
                    //                    NSDictionary *dict = [[NSDictionary alloc] init];
                    //                    NSLog(@"%@",dict);
                    //                    [dict setValue:strDeliveryDate forKey:@"DeliveryDate"];
                    //                    [dict setValue:strDeliveryRun forKey:@"DeliveryRun"];
                    //                    [dict setValue:[dictData objectForKey:@"DROP_SEQUENCE"] forKey:@"DropSequence"];
                    //                    NSLog(@"%@",dict);
                    [mainDict setValue:strDeliveryDate forKey:@"DeliveryDate"];
                    [mainDict setValue:strDeliveryRun forKey:@"DeliveryRun"];
                    
                }
                // NSLog(@"%@",[dictData objectForKey:@"DROP_SEQUENCE"]);
                NSLog(@"%@",mainDict);
            }
            [rs34 close];
            
            //            NSArray *arrOldDeliveryRun = [[NSArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
            //                                                                           @"1",@"key",
            //                                                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN1"],@"value",
            //                                                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"2",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN2"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"3",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN3"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"4",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN4"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"5",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN5"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"6",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN6"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"7",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN7"],@"value",
            //                                           nil],
            //                                          nil];
            //
            //
            //            NSNumber *numCurrDayOfWeek = [NSNumber numberWithInteger: [AppDelegate getCurrentDayOfWeek]];
            //            int currDayOfWeek = [numCurrDayOfWeek intValue];
            //
            //            NSArray *arrFrom = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(currDayOfWeek, [arrOldDeliveryRun count] - currDayOfWeek)];
            //
            //            NSArray *arrTo = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(0, currDayOfWeek)];
            //
            //            NSArray *arrNewDeliveryRun = [arrFrom arrayByAddingObjectsFromArray:arrTo];
            //
            //            NSString *strDeliveryRun = nil;
            //            BOOL isDeliveryRunFound = FALSE;
            //
            //            NSString *strDeliveryDate = nil;
            //            NSString *strDropSequence = nil;
            //
            //            for (int i = 0; i < [arrNewDeliveryRun count]; i++) {
            //
            //                if (![[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"] isEqualToString:@""]) {
            //
            //                    //Execute one time
            //                    isDeliveryRunFound = TRUE;
            //                    strDeliveryRun = [[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"];
            //
            //                    int newDay = [[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"key"] intValue];
            //
            //                    //DropSequence
            //                    NSString *strDropSeqKey = [NSString stringWithFormat:@"DROP_SEQUENCE%d",newDay];
            //                    strDropSequence = [dictDataArmaster objectForKey:strDropSeqKey];
            //                    strDropSeqKey = nil;
            //
            //                    NSDate *today = [NSDate date];
            //                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            //                    [gregorian setLocale:[NSLocale currentLocale]];
            //
            //                    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
            //
            //                    if (newDay > currDayOfWeek) {
            //                        [nowComponents setWeek: [nowComponents week]];//Current Week
            //                    }
            //                    else{
            //                        [nowComponents setWeek: [nowComponents week] + 1]; //Next week
            //                    }
            //
            //                    newDay = ((newDay + 7) % 7) + 1; // Transforming so that monday = 2 and sunday = 1
            //
            //                    [nowComponents setWeekday:newDay];
            //                    [nowComponents setHour:8]; //8a.m.
            //                    [nowComponents setMinute:0];
            //                    [nowComponents setSecond:0];
            //
            //                    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
            //
            //                    strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:beginningOfWeek];
            //                    NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //
            //                    break;
            //                }
            //            }
            //
            //            if (!isDeliveryRunFound) {
            //                strDeliveryRun = [dictDataArmaster objectForKey:@"DELIVERY_RUN"];
            //
            //                NSDate *now = [NSDate date];
            //                int daysToAdd = 1;
            //                NSDate *nextDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            //                strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:nextDate];
            //                //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //                //DropSequence
            //                strDropSequence = [dictDataArmaster objectForKey:@"DROP_SEQUENCE"];
            //
            //            }
            //
            //            [mainDict setValue:strDeliveryRun forKey:@"DeliveryRun"];
            //            [mainDict setValue:strDeliveryDate forKey:@"DeliveryDate"];
            //            [mainDict setValue:strDropSequence forKey:@"DropSequence"];
            
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    
}

-(void)callGetSalesDetailsDetailsFromDB:(NSString *)debtorNum{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        @try {
            
            mainDict = [[NSMutableDictionary alloc] init];
            
            //Get Customer Details
            FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM armaster WHERE TRIM(CODE) = ?",debtorNum];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictDataArmaster = nil;
            if ([rs1 next]) {
                
                dictDataArmaster = [rs1 resultDictionary];
                
                [dict setValue:[dictDataArmaster objectForKey:@"CODE"] forKey:@"Debtor"];
                [dict setValue:[dictDataArmaster objectForKey:@"NAME"] forKey:@"Name"];
                [dict setValue:[[dictDataArmaster objectForKey:@"ADDRESS1"] trimSpaces] forKey:@"Address1"];
                [dict setValue:[[dictDataArmaster objectForKey:@"ADDRESS2"] trimSpaces] forKey:@"Address2"];
                [dict setValue:[[dictDataArmaster objectForKey:@"ADDRESS3"] trimSpaces]forKey:@"Address3"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS1"] trimSpaces] forKey:@"DelAddress1"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS2"] trimSpaces] forKey:@"DelAddress2"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_ADDRESS3"] trimSpaces] forKey:@"DelAddress3"];
                [dict setValue:[dictDataArmaster objectForKey:@"SUBURB"] forKey:@"Suburb"];
                [dict setValue:[dictDataArmaster objectForKey:@"POST_CODE"] forKey:@"PostCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"COUNTRY"] forKey:@"Country"];
                [dict setValue:[dictDataArmaster objectForKey:@"DEL_NAME"] forKey:@"DelName"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_SUBURB"] trimSpaces]  forKey:@"DelSuburb"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_POST_CODE"] trimSpaces] forKey:@"DelPostCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"PHONE"] forKey:@"Phone"];
                [dict setValue:[dictDataArmaster objectForKey:@"EMAIL1"] forKey:@"Email"];
                [dict setValue:[dictDataArmaster objectForKey:@"FAX_NO"] forKey:@"Fax"];
                [dict setValue:[dictDataArmaster objectForKey:@"CONTACT1"] forKey:@"Contact"];
                [dict setValue:[dictDataArmaster objectForKey:@"CREDIT_LIMIT"] forKey:@"CreditLimit"];
                [dict setValue:[[dictDataArmaster objectForKey:@"DEL_COUNTRY"] trimSpaces] forKey:@"DelCountry"];
                [dict setValue:[dictDataArmaster objectForKey:@"BRANCH"] forKey:@"Branch"];
                [dict setValue:[dictDataArmaster objectForKey:@"PRICE_CODE"]  forKey:@"PriceCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"TOTAL_DUE"] forKey:@"Balance"];
                [dict setValue:[dictDataArmaster objectForKey:@"SALESMAN"]  forKey:@"Salesman"];
                [dict setValue:[dictDataArmaster objectForKey:@"SALES_BRANCH"] forKey:@"SalesBranch"];
                [dict setValue:[dictDataArmaster objectForKey:@"TERMS"] forKey:@"Terms"];
                [dict setValue:[dictDataArmaster objectForKey:@"TRADING_TERMS"] forKey:@"TradingTerms"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION1"] forKey:@"TaxExemption1"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION2"] forKey:@"TaxExemption2"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION3"] forKey:@"TaxExemption3"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION4"] forKey:@"TaxExemption4"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION5"] forKey:@"TaxExemption5"];
                [dict setValue:[dictDataArmaster objectForKey:@"TAX_EXEMPTION6"] forKey:@"TaxExemption6"];
                [dict setValue:[dictDataArmaster objectForKey:@"EXCHANGE_CODE"] forKey:@"ExchangeCode"];
                [dict setValue:[dictDataArmaster objectForKey:@"ALLOW_PARTIAL"] forKey:@"AllowPartial"];
                [dict setValue:[dictDataArmaster objectForKey:@"CHARGE_TYPE"] forKey:@"Chargetype"];
                [dict setValue:[dictDataArmaster objectForKey:@"CARRIER"] forKey:@"Carrier"];
                [dict setValue:[dictDataArmaster objectForKey:@"EXPORT_DEBTOR"] forKey:@"ExportDebtor"];
                [dict setValue:[dictDataArmaster objectForKey:@"RATE"] forKey:@"ChargeRate"];
                [dict setValue:[dictDataArmaster objectForKey:@"HOLD"] forKey:@"Held"];
                [dict setValue:[dictDataArmaster objectForKey:@"CUST_CATEGORY"] forKey:@"Cust_Category"];
                
                NSString *str_current_Date = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
                
                //Current date
                [dict setValue:str_current_Date forKey:@"OrderDate"];
                [dict setValue:@"0" forKey:@"Numboxes"];
                [dict setValue:@"N" forKey:@"Direct"];
                [mainDict setObject:dict forKey:@"Data"];
                
            }
            
            [rs1 close];
            
            //Get Customer Details
            FMResultSet *rs2 = [db executeQuery:@"SELECT `DEBTOR`, `CONTACT`, `DEL_INST1` as DelInst1, `DEL_INST2` as DelInst2, `CUST_ORDER` as CustOrderno, `WAREHOUSE` as Warehouse from `soheader` WHERE trim(DEBTOR) = ?",[debtorNum trimSpaces]];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs2 next]) {
                NSDictionary *dictData = [rs2 resultDictionary];
                [dictData setValue:@"1" forKey:@"Status"];
                [mainDict setObject:dictData forKey:@"Header"];
                
            }
            
            [rs2 close];
            
            //Year & Period raised
            //Year & Period raised calculation
            int currentMonth = [[AppDelegate getCurrentMonth] intValue];
            int currentYear = [[AppDelegate getCurrentYear] intValue];
            int newmonth = 0;
            if(currentMonth < 7)
            {
                newmonth = currentMonth+6;
                currentYear = currentYear - 1;
            }
            else
            {
                newmonth = currentMonth-6;
            }
            
            NSString *strPeriodRaised = [NSString stringWithFormat:@"%d",newmonth];
            NSDictionary *dictData = [NSDictionary new];
            [dict setValue:strPeriodRaised forKey:@"PeriodRaised"];
            [dict setValue:[NSString stringWithFormat:@"%d",currentYear] forKey:@"YearRaised"];
            [mainDict setObject:dict forKey:@"Data"];
            dictData=nil;
            
            
            //            FMResultSet *rs3 = [db executeQuery:@"SELECT SA_PERIOD,SA_FIN_YEAR FROM syscont WHERE RECNUM = 1"];
            //
            //            if (!rs3)
            //            {
            //                [rs3 close];
            //                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            //                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            //            }
            //
            //            if ([rs3 next]) {
            //                NSDictionary *dictData = [rs3 resultDictionary];
            //                [dict setValue:[dictData objectForKey:@"SA_PERIOD"] forKey:@"PeriodRaised"];
            //                [dict setValue:[dictData objectForKey:@"SA_FIN_YEAR"] forKey:@"YearRaised"];
            //                [mainDict setObject:dict forKey:@"Data"];
            //                dictData=nil;
            //
            //            }
            
            
            //            NSArray *arrOldDeliveryRun = [[NSArray alloc] initWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
            //                                                                           @"1",@"key",
            //                                                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN1"],@"value",
            //                                                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"2",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN2"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"3",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN3"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"4",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN4"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"5",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN5"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                           @"6",@"key",
            //                                           [dictDataArmaster objectForKey:@"DELIVERY_RUN6"],@"value",
            //                                           nil],
            //                                          [NSDictionary dictionaryWithObjectsAndKeys:
            //                                            @"7",@"key",
            //                                            [dictDataArmaster objectForKey:@"DELIVERY_RUN7"],@"value",
            //                                            nil],
            //                                          nil];
            //
            //
            //            NSNumber *numCurrDayOfWeek = [NSNumber numberWithInteger: [AppDelegate getCurrentDayOfWeek]];
            //            int currDayOfWeek = [numCurrDayOfWeek intValue];
            //
            //            NSArray *arrFrom = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(currDayOfWeek, [arrOldDeliveryRun count] - currDayOfWeek)];
            //
            //            NSArray *arrTo = [arrOldDeliveryRun subarrayWithRange:NSMakeRange(0, currDayOfWeek)];
            //
            //            NSArray *arrNewDeliveryRun = [arrFrom arrayByAddingObjectsFromArray:arrTo];
            //
            //            NSString *strDeliveryRun = nil;
            //            BOOL isDeliveryRunFound = FALSE;
            //
            //            NSString *strDeliveryDate = nil;
            //            NSString *strDropSequence = nil;
            //
            //            for (int i = 0; i < [arrNewDeliveryRun count]; i++) {
            //
            //                if (![[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"] isEqualToString:@""]) {
            //
            //                    //Execute one time
            //                    isDeliveryRunFound = TRUE;
            //                    strDeliveryRun = [[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"value"];
            //
            //                    int newDay = [[[arrNewDeliveryRun objectAtIndex:i] objectForKey:@"key"] intValue];
            //
            //                    //DropSequence
            //                    NSString *strDropSeqKey = [NSString stringWithFormat:@"DROP_SEQUENCE%d",newDay];
            //                    strDropSequence = [dictDataArmaster objectForKey:strDropSeqKey];
            //                    strDropSeqKey = nil;
            //
            //                    NSDate *today = [NSDate date];
            //                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            //                    [gregorian setLocale:[NSLocale currentLocale]];
            //
            //                    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
            //
            //                    if (newDay > currDayOfWeek) {
            //                        [nowComponents setWeek: [nowComponents week]];//Current Week
            //                    }
            //                    else{
            //                        [nowComponents setWeek: [nowComponents week] + 1]; //Next week
            //                    }
            //
            //                    newDay = ((newDay + 7) % 7) + 1; // Transforming so that monday = 2 and sunday = 1
            //
            //                    [nowComponents setWeekday:newDay];
            //                    [nowComponents setHour:8]; //8a.m.
            //                    [nowComponents setMinute:0];
            //                    [nowComponents setSecond:0];
            //
            //                    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
            //
            //                    strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:beginningOfWeek];
            //                    //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //
            //                    break;
            //                }
            //            }
            //
            //            if (!isDeliveryRunFound) {
            //                strDeliveryRun = [dictDataArmaster objectForKey:@"DELIVERY_RUN"];
            //
            //                NSDate *now = [NSDate date];
            //                int daysToAdd = 1;
            //                NSDate *nextDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            //                strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:nextDate];
            //                //NSLog(@"strDeliveryDate %@",strDeliveryDate);
            //
            //                //DropSequence
            //                strDropSequence = [dictDataArmaster objectForKey:@"DROP_SEQUENCE"];
            //
            //            }
            //
            //            NSDictionary *dictDeliveryRun = [mainDict objectForKey:@"Data"];
            //            [dictDeliveryRun setValue:strDeliveryRun forKey:@"DeliveryRun"];
            //            [dictDeliveryRun setValue:strDeliveryDate forKey:@"DeliveryDate"];
            //            [dictDeliveryRun setValue:strDropSequence forKey:@"DropSequence"];
            
            FMResultSet *rs34= [db executeQuery:@"SELECT DELIVERY_RUN,DELIVERY_RUN1,DELIVERY_RUN2,DELIVERY_RUN3,DELIVERY_RUN4,DELIVERY_RUN5,DELIVERY_RUN6,DELIVERY_RUN7,DROP_SEQUENCE FROM armaster WHERE trim(`CODE`) = ?",[debtorNum trimSpaces]];
            
            if (!rs34)
            {
                [rs34 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs34 next]) {
                
                NSDictionary *dictData = [rs34 resultDictionary];
                //NSLog(@"%@",dictData);
                NSString *strDeliveryRun;
                
                if(![[dictData objectForKey:@"DELIVERY_RUN"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN1"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN1"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN1"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN2"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN2"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN2"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN3"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN3"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN3"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN4"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN4"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN4"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN5"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN5"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN5"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN6"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN6"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN6"] trimSpaces]];
                }
                else if(![[dictData objectForKey:@"DELIVERY_RUN7"] isEqualToString:@"NA"] && ![[dictData objectForKey:@"DELIVERY_RUN7"] isKindOfClass:[NSNull class]])
                {
                    strDeliveryRun = [NSString stringWithFormat:@"%@",[[dictData objectForKey:@"DELIVERY_RUN7"] trimSpaces]];
                }
                
                [rs34 close];

                
                FMResultSet *rs4= [db executeQuery:@"SELECT VALUE as yPosition FROM sysdesc WHERE trim(`CODE`) = ?",strDeliveryRun];
                
                if (!rs4)
                {
                    [rs4 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                //Calculate delivery date
                if ([rs4 next]) {
                    
                    NSDictionary *dictDataPosition = [rs4 resultDictionary];
                    
                    // NSLog(@"dictData4 %@", dictData);
                    NSRange range = [[dictDataPosition objectForKey:@"yPosition"] rangeOfString:@"Y"];
                    
                    int yposition = range.location + 1;
                    
                    int currDayOfWeek = [[AppDelegate getCurrentDayOfWeek2] intValue];
                    
                    int interval;
                    if (currDayOfWeek == 1 ) {
                        interval = 9;
                    }
                    else{
                        interval = currDayOfWeek + 1;
                    }
                    
                    interval = 9 - interval;
                    
                    int tempPosition = interval + yposition;
                    
                    int temp;
                    
                    if(tempPosition >= 7)
                    {
                        temp = tempPosition % 7;
                    }
                    else
                    {
                        temp = tempPosition;
                    }
                    
//                    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*(temp)];
                    
                    // Set delivery date to next date
                 /*   NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
                    dayComponent.day = 1;
                    
                    NSCalendar *theCalendar = [NSCalendar currentCalendar];
                    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
                    
                    NSLog(@"nextDate: %@ ...", nextDate);
*/
                    NSString *strDeliveryDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
                    
                    NSDictionary *dict = [mainDict objectForKey:@"Data"];
                    [dict setValue:strDeliveryDate forKey:@"DeliveryDate"];
                    [dict setValue:strDeliveryRun forKey:@"DeliveryRun"];
                    [dict setValue:[dictData objectForKey:@"DROP_SEQUENCE"] forKey:@"DropSequence"];
                    //DATE_ADD( CURDATE( ) , INTERVAL( ( 9 - IF( DAYOFWEEK( CURDATE( ) ) =1, 9, (DAYOFWEEK( CURDATE( ) ) +1 ) ) ) + %d )DAY)
                }
                
                [rs4 close];

            }
            
            //            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `NEW_USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            FMResultSet *rs6 = [db executeQuery:@"SELECT `DEBTOR`, MAX(DATE) AS DateCreated, `COMMENTTXT` as Commenttxt, `FOLLOW_DATE` as FollowDate, `USAGE` as Usage FROM (`arcoment`) WHERE `DEBTOR` = ?",debtorNum];
            
            //
            if (!rs6)
            {
                [rs6 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs6 next]) {
                
                NSDictionary *dictData = [rs6 resultDictionary];
                
                if ([[dictData objectForKey:@"Commenttxt"] isEqual:[NSNull null]]) {
                    [dictData setValue:@"False" forKey:@"Flag"];
                }
                else{
                    [dictData setValue:@"True" forKey:@"Flag"];
                }
                
                [mainDict setObject:dictData forKey:@"Comments"];
                
            }
            else{
                NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
                [dictData setValue:@"False" forKey:@"Flag"];
                [mainDict setObject:dictData forKey:@"Comments"];
                
            }
            
            //NSLog(@"mainDict : %@",mainDict);
            [rs6 close];
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
    }];
    
    NSLog(@"Check - %@",mainDict);
}

#pragma mark - WS calls
-(void)callWSGetCustomerList:(int)pageIndex
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs stringForKey:@"salesType"];
    NSString *strMembers = [prefs stringForKey:@"members"];
    
    NSString *username = [salesType substringFromIndex:1];
    
    strWebserviceType = @"WS_QUOTE_HISTORY";
    //Fetch last sync date
    NSString *last_sync_date = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"armaster"];
    NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d&last_syncDate=%@&username=%@&members=%@",pageIndex,ROWS_PER_PAGE,last_sync_date,username,strMembers];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_HISTORY_CUSTOMER_LIST_WS,parameters];
    
    NSLog(@"strURL : %@",strURL);
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewSalesDetailsDetails:(NSString *)debtorNum
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_SALES_DEBTOR_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"debtor=%@",debtorNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,SALE_DEBTOR_DETAILS_WS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewQuoteDetailsDetails:(NSString *)debtorNum
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DEBTOR_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"debtor=%@",debtorNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DEBTOR_DETAILS_WS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}



#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"%@", dictResponse);
    
    [spinner removeFromSuperview];
    
    NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_HISTORY"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Flag"]){
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                if(!self.arrCustomerForWebservices){
                    arrCustomerForWebservices = [[NSMutableArray alloc] init];
                }
                
                if (_loadMoreFooterView == nil) {
                    [self setupLoadMoreFooterView];
                }
                
                //Total Records
                totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"];
                    [arrCustomerForWebservices addObject:dict];
                }
                else{
                    
                    [arrCustomerForWebservices addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Data"]];
                }
                
            }
            //False
            else{
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"Message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            }
        }
        //No more updates
        else if([[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"UpdateInfo"]){
            
            if (_loadMoreFooterView == nil) {
                [self setupLoadMoreFooterView];
            }
            
            //Total Records
            totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
            
            
            dispatch_async(backgroundQueueForCustomerList, ^(void) {
                
                [dbQueueCustomerList inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try
                    {
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"armaster" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        //Commit here
                        [db commit];
                        
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                        
                        //[db close];
                    }
                    
                    @finally {
                        
                        [self callGetCustomerListFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        
                        @try
                        {
                            
                            [tblQuoteList reloadData];
                            
                            [spinnerObj hide:YES];
                            
                            if([arrCustomer count] < totalCount){
                                [self repositionLoadMoreFooterView];
                                
                                // Dismiss loading footer
                                [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                                
                                currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Debtors"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                                
                                //Next page
                                currPage ++;
                                
                                recordNumber += ROWS_PER_PAGE;
                                
                            }
                            else{
                                [_loadMoreFooterView removeFromSuperview];
                                _loadMoreFooterView=nil;
                            }
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                        
                    });
                    
                }];
                
            });
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_QUOTE_DEBTOR_DETAILS"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            [spinnerObj hide:YES];
            
            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Data"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductHeadersNotification"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
        
    }
    else if ([strWebserviceType isEqualToString:@"WS_SALES_DEBTOR_DETAILS"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            spinnerObj.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:HUD_COMPLETION_IMAGE]];
            spinnerObj.mode = MBProgressHUDModeCustomView;
            [spinnerObj hide:YES afterDelay:SPINNER_HIDE_TIME];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSMutableDictionary *tempDict;
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Header"]) {
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Header"] isKindOfClass:[NSDictionary class]]) {
                    tempDict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Header"];
                }
                else{
                    //--Changes for IOS8
                    //  tempDict = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Header"] objectAtIndex:0];
                    
                    NSArray *ary =  [[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Header"] ;
                    tempDict = [ary objectAtIndex:0];
                }
                
                [dict setObject:tempDict forKey:@"Header"];            }
            
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Data"]) {
                [dict setObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Data"] forKey:@"Data"];
            }
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Comments"]) {
                [dict setObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Comments"] forKey:@"Comments"];
            }
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddDebtorDetailsNotification"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"DebtorDetails"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
        
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


@end
