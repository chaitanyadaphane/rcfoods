//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import "QuoteMenuPopOverViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewQuoteProductDetailsViewController.h"
#import "QuoteProductComment.h"
#import <MessageUI/MessageUI.h>

@interface OfflineAddQuoteViewController : UIViewController<UITextFieldDelegate,ReachabilityRequest_delegate,DismissPopOverDelegateForMenuList,UIPopoverControllerDelegate,QuoteSetProfitDelegate,MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate,SetCommentDelegate>{
    
    NSArray *arrHeaderLabels;
    NSMutableArray *arrProducts;
    NSMutableArray *arrSelectedProducts;
        
    CALayer *layerPointingImage;
    
    SEFilterControl *segControl;
    
    NSMutableDictionary *dictHeaderDetails;
    
    IBOutlet UIToolbar *keyboardToolBar;
    
    NSString *strWebserviceType;
    
    dispatch_queue_t backgroundQueueForNewQuote;
    FMDatabaseQueue *dbQueueNewQuote;
    
    //Reachability *checkInt;
    //NetworkStatus internetStatus;
    
    BOOL isNetworkConnected;   
    NSString *strQuoteNum;
    
    UIPopoverController *popoverController;
    
    
    BOOL chkFinalize;
    
    NSMutableArray *tempArrForEdit;
    
    int countval;
    
    //Delete
    NSIndexPath *deleteIndexPath;
    int deleteRowIndex;
    //int tagFlsh;
    
   
    NSString *strDebtorNum;
    MBProgressHUD *spinner;
    
    NSInteger selectedIndex;
    NSInteger selectedSectionIndex;
}

@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) NSMutableArray *arrSelectedProducts;
@property (nonatomic,retain) NSDictionary *dictHeaderDetails;
@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) UIPopoverController *popoverController;
@property (nonatomic,retain) NSString *strQuoteNumRecived;
@property (nonatomic, assign) BOOL EditableMode;
@property (nonatomic,retain) NSString *strDebtorNum;
@property (nonatomic,retain) NSIndexPath *deleteIndexPath;
@property (assign) int deleteRowIndex;

@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnEmail;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnPrint;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblHeader;
@property (nonatomic, unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic, unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic, retain)IBOutlet UIView *vwContentSuperview;
@property (nonatomic, retain)IBOutlet UIView *vwHeader;
@property (nonatomic, retain)IBOutlet UIView *vwDetails;
@property (nonatomic, retain)IBOutlet UIView *vwNoProducts;
@property (nonatomic, retain)IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnDelete;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnLoadProfile;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnSaveDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddProducts;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnFinalise;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowDownwards;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowRightWards;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *btnAddMenu;
@property (nonatomic,assign)BOOL isCommentAddedOnLastLine;

#pragma mark - IBAction
- (IBAction)actionShowQuoteHistory:(id)sender;
- (IBAction)actionShowProductList:(id)sender;
- (IBAction)actionDeleteRows:(id)sender;

- (void)showOrEnableButtons;
- (void)hideOrDisableButtons;
- (void)doAfterdelete;
- (void)showPointingAnimation;
@end
