//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuoteSetProfitDelegate
- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation;
@end

@interface ViewQuoteProductDetailsViewController : UIViewController<UITextFieldDelegate>{
    
    NSString *strWebserviceType;
    
    NSMutableDictionary *dictProductDetails;    
    NSArray *arrProductLabels;

    float floatProfitPercentage;
    NSString *strSellingPrice;

    BOOL isFromViewQuoteDetails;
    
    id<QuoteSetProfitDelegate> __unsafe_unretained  delegate;
    
    NSString *strPrice;
    NSString *strDebtor;
    
    BOOL isQuoteEditable;
    NSOperationQueue *operationQueue;
    
    float quantityOrdered;
    MBProgressHUD *spinner;
}


@property (nonatomic,retain) NSMutableDictionary *dictProductDetails;
@property (nonatomic,retain) NSString *strSellingPrice;
@property (nonatomic,retain) NSString *strPrice;
@property (nonatomic,retain) NSString *strExcl;
@property (nonatomic,retain) NSString *strTax;
@property (nonatomic,retain) NSString *strExtension;
@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,assign) BOOL isFromViewQuoteDetails;
@property (nonatomic,retain) NSOperationQueue *operationQueue;
@property(assign)int productIndex;
@property(assign) BOOL isQuoteEditable;
@property(nonatomic,assign) BOOL isTaxApplicable;

@property (unsafe_unretained) id<QuoteSetProfitDelegate> __unsafe_unretained  delegate;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblProductName;

- (IBAction)saveProductDetails:(id)sender;

@end
