//
//  StockCodeViewController.h
//  MySCM_iPad
//
//  Created by Shival on 17/01/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissPopOverDelegate

- (void)actiondismissPopOverStockPickUp:(NSString*)strPickUpCode strTextField:(NSString*)strTextField;
- (void)actiondismissPickUpValue:(NSDictionary*)dictPickUp strTextField:(NSString*)strTextField;

@end

@interface StockCodeViewController : UIViewController
{
    NSArray *arrData;
    NSString *strTextField;
    NSString *strDescriptionTxtField;
    BOOL isStockCode;
    id<DismissPopOverDelegate> __unsafe_unretained delegate;
}
@property(nonatomic,assign)BOOL isStockCode;

@property(nonatomic,retain)NSArray *arrData;
@property(nonatomic,retain)NSString *strTextField;
@property(nonatomic,retain)NSString *strDescriptionTxtField;

@property (unsafe_unretained)id<DismissPopOverDelegate>delegate;

@end
