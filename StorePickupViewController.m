//
//  StorePickupViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "StorePickupViewController.h"
#import "StockCodeViewController.h"

@interface StorePickupViewController ()
{
    NSMutableArray *arrAllStockCode;
    NSMutableArray *arrFilteredStockCode;
    StockCodeViewController *viewCnt;
}
@end

@implementation StorePickupViewController
@synthesize delegate,popoverController;
@synthesize lblReason1,lblReason2,lblReason3,lblReason4,lblReason5,lblReason6,lblReason7,lblReason8;
@synthesize txtStockCode1,txtStockCode2,txtStockCode3,txtStockCode4,txtStockCode5,txtStockCode6,txtStockCode7,txtStockCode8;
@synthesize txtQuantity1,txtQuantity2,txtQuantity3,txtQuantity4,txtQuantity5,txtQuantity6,txtQuantity7,txtQuantity8;
@synthesize txtReturn1,txtReturn2,txtReturn3,txtReturn4,txtReturn5,txtReturn6;
@synthesize txtComment1,txtComment2,txtComment3,txtComment4,txtComment5,txtComment6,txtComment7,txtComment8;

@synthesize txtReturn8,txtReturn7;

CGRect  originalRect;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    originalRect = self.view.frame;
  [self SetDeligatesForTextFields];
    // Do any additional setup after loading the view from its nib.
}
-(void)SetDeligatesForTextFields
{
  txtStockCode7.delegate = txtStockCode8.delegate = txtQuantity7.delegate = txtQuantity8.delegate = txtReturn7.delegate = txtReturn8.delegate = txtComment8.delegate = txtComment7.delegate = self;
  

}
- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.lblReason1 = nil;
    self.lblReason2 = nil;
    self.lblReason3 = nil;
    self.lblReason4 = nil;
    self.lblReason5 = nil;
    self.lblReason6 = nil;
    
    self.txtStockCode1 = nil;
    self.txtStockCode2 = nil;
    self.txtStockCode3 = nil;
    self.txtStockCode4 = nil;
    self.txtStockCode5 = nil;
    self.txtStockCode6 = nil;
    
    self.txtQuantity1 = nil;
    self.txtQuantity2 = nil;
    self.txtQuantity3 = nil;
    self.txtQuantity4 = nil;
    self.txtQuantity5 = nil;
    self.txtQuantity6 = nil;
    
    self.txtReturn1 = nil;
    self.txtReturn2 = nil;
    self.txtReturn3 = nil;
    self.txtReturn4 = nil;
    self.txtReturn5 = nil;
    self.txtReturn6 = nil;
    
    self.txtComment1 = nil;
    self.txtComment2 = nil;
    self.txtComment3 = nil;
    self.txtComment4 = nil;
    self.txtComment5 = nil;
    self.txtComment6 = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    arrAllStockCode = [NSMutableArray new];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs = [db executeQuery:@"SELECT CODE,DESCRIPTION FROM samaster WHERE `STATUS` = 'OB'"];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        else
        {
            while ([rs next])
            {
                /*
                NSString *strData = [NSString stringWithFormat:@"%@ - %@",[rs stringForColumn:@"CODE"],[rs stringForColumn:@"DESCRIPTION"]];
                [arrAllStockCode addObject:strData];
                 */
                
                NSString *strData = [NSString stringWithFormat:@"%@ - %@",[[rs stringForColumn:@"CODE"] trimSpaces],[[rs stringForColumn:@"DESCRIPTION"] trimSpaces]];
                [arrAllStockCode addObject:strData];

                strData = nil;
            }
            
            [rs close];
        }
    }];
    
    //NSLog(@"%@",arrAllStockCode);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)GiveKyeboard
{
  self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
   [[UIApplication sharedApplication] resignFirstResponder];
  
}
#pragma mark - UITextfield Delegate Methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
  self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
  
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtReturn1)
    {
        [textField resignFirstResponder];

            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
            viewCnt.strTextField = @"txtReturn1";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
    }
    else if(textField == txtReturn2)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn2";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else if(textField == txtReturn3)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn3";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else if(textField == txtReturn4)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn4";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else if(textField == txtReturn5)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn5";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else if(textField == txtReturn6)
    {
        [textField resignFirstResponder];
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
        viewCnt.strTextField = @"txtReturn6";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else if(textField == txtReturn7)
    {
      [textField resignFirstResponder];
      
      if(!viewCnt)
      {
        viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
      }
      viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
      viewCnt.strTextField = @"txtReturn7";
      
      //resize the popover view shown
      //in the current view to the view's size
      viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
      
      if([self.popoverController isPopoverVisible])
      {
        [self.popoverController dismissPopoverAnimated:NO];
      }
      
      //create a popover controller
      self.popoverController = [[UIPopoverController alloc]
                                initWithContentViewController:viewCnt];
      
      viewCnt.delegate = self;
      
      [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
      
    }
  
    else if(textField == txtReturn8)
    {
      [textField resignFirstResponder];
      
      if(!viewCnt)
      {
        viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
      }
      viewCnt.arrData = [[NSArray alloc] initWithObjects:@"Yes",@"No", nil];
      viewCnt.strTextField = @"txtReturn8";
      
      //resize the popover view shown
      //in the current view to the view's size
      viewCnt.contentSizeForViewInPopover = CGSizeMake(100,130);
      
      if([self.popoverController isPopoverVisible])
      {
        [self.popoverController dismissPopoverAnimated:NO];
      }
      
      //create a popover controller
      self.popoverController = [[UIPopoverController alloc]
                                initWithContentViewController:viewCnt];
      
      viewCnt.delegate = self;
      
      [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
      
    }
  
  else if(textField == txtStockCode7 || textField == txtStockCode8 || textField == txtQuantity8 || txtQuantity7 == textField || txtReturn7 == textField || txtReturn8 == textField || txtComment7 == textField || textField == txtComment8)
  {
    self.view.frame = CGRectMake(self.view.frame.origin.x, -100, self.view.frame.size.width, self.view.frame.size.height);

  }
  else
  {
    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
  }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtStockCode1)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
           // NSLog(@"%@",arrFilteredStockCode);
            
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }

            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode1";
            viewCnt.isStockCode = NO;
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 150);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
 
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == txtStockCode2)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            // NSLog(@"%@",arrFilteredStockCode);
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode2";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == txtStockCode3)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            // NSLog(@"%@",arrFilteredStockCode);
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode3";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == txtStockCode4)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            // NSLog(@"%@",arrFilteredStockCode);
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode4";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == txtStockCode5)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            // NSLog(@"%@",arrFilteredStockCode);
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode5";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
    else if(textField == txtStockCode6)
    {
        NSString *tempString = nil;
        if ((textField.text.length - 1) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
            // NSLog(@"%@",arrFilteredStockCode);
            if(!viewCnt)
            {
                viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
            }
            
            viewCnt.arrData = arrFilteredStockCode;
            viewCnt.strTextField = @"txtStockCode6";
            
            //resize the popover view shown
            //in the current view to the view's size
            viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
            
            if([self.popoverController isPopoverVisible])
            {
                [self.popoverController dismissPopoverAnimated:NO];
            }
            
            //create a popover controller
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:viewCnt];
            
            viewCnt.delegate = self;
            
            [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            [self textFieldShouldClear:textField];
        }
    }
  
  
    else if(textField == txtStockCode7)
    {
      NSString *tempString = nil;
      if ((textField.text.length - 1) > 0)
      {
        tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
      }
      else
      {
        tempString = string;
      }
      
      if(![tempString isEqualToString:@""])
      {
        NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
        NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
        NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
        arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        // NSLog(@"%@",arrFilteredStockCode);
        if(!viewCnt)
        {
          viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode7";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
        
        if([self.popoverController isPopoverVisible])
        {
          [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
      }
      else
      {
        [self textFieldShouldClear:textField];
      }
    }
  
    else if(textField == txtStockCode8)
    {
      NSString *tempString = nil;
      if ((textField.text.length - 1) > 0)
      {
        tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
      }
      else
      {
        tempString = string;
      }
      
      if(![tempString isEqualToString:@""])
      {
        NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
        NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
        NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
        arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        // NSLog(@"%@",arrFilteredStockCode);
        if(!viewCnt)
        {
          viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode8";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(500, 300);
        
        if([self.popoverController isPopoverVisible])
        {
          [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
      }
      else
      {
        [self textFieldShouldClear:textField];
      }
    }
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
  self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
  
    if(textField == txtStockCode1)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode1";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(textField == txtStockCode2)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode2";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(textField == txtStockCode3)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode3";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(textField == txtStockCode4)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode4";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(textField == txtStockCode5)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode5";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(textField == txtStockCode6)
    {
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        
        if((textField.text.length - 1) > 0)
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
        }
        
        
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        viewCnt.arrData = arrFilteredStockCode;
        viewCnt.strTextField = @"txtStockCode6";
        
        viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
  
  else if(textField == txtStockCode7)
  {
    if(!viewCnt)
    {
      viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
    }
    
    if((textField.text.length - 1) > 0)
    {
      NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
      NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
      NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
      arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
    }
    else
    {
      arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
    }
    
    
    if([self.popoverController isPopoverVisible])
    {
      [self.popoverController dismissPopoverAnimated:NO];
    }
    
    viewCnt.arrData = arrFilteredStockCode;
    viewCnt.strTextField = @"txtStockCode7";
    
    viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:viewCnt];
    
    viewCnt.delegate = self;
    
    [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
  }
  
  else if(textField == txtStockCode8)
  {
    if(!viewCnt)
    {
      viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
    }
    
    if((textField.text.length - 1) > 0)
    {
      NSString *indexRegex = [NSString stringWithFormat:@"%@",textField.text];
      NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
      NSArray *filteredArray = [arrAllStockCode filteredArrayUsingPredicate:regexString];
      arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:filteredArray];
    }
    else
    {
      arrFilteredStockCode = [[NSMutableArray alloc] initWithArray:arrAllStockCode];
    }
    
    
    if([self.popoverController isPopoverVisible])
    {
      [self.popoverController dismissPopoverAnimated:NO];
    }
    
    viewCnt.arrData = arrFilteredStockCode;
    viewCnt.strTextField = @"txtStockCode8";
    
    viewCnt.contentSizeForViewInPopover = CGSizeMake(300, 300);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:viewCnt];
    
    viewCnt.delegate = self;
    
    [self.popoverController presentPopoverFromRect:textField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
  }
    return YES;
}

- (IBAction)actionDismiss:(id)sender{
  
  /*
    NSString *strData = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",txtStockCode1.text,txtQuantity1.text,lblReason1.text,txtReturn1.text,txtComment1.text,txtStockCode2.text,txtQuantity2.text,lblReason2.text,txtReturn2.text,txtComment2.text,txtStockCode3.text,txtQuantity3.text,lblReason3.text,txtReturn3.text,txtComment3.text,txtStockCode3.text,txtQuantity4.text,lblReason4.text,txtReturn4.text,txtComment4.text,txtStockCode5.text,txtQuantity5.text,lblReason5.text,txtReturn5.text,txtComment5.text,txtStockCode6.text,txtQuantity6.text,lblReason6.text,txtReturn6.text,txtComment6.text];
    */
  
  if([txtQuantity8.text length] <=0)
  {
    txtQuantity8.text = @"0";
  }
  if([txtQuantity7.text length] <=0)
  {
    txtQuantity7.text = @"0";
  }
  if([txtQuantity6.text length] <=0)
  {
    txtQuantity6.text = @"0";
  }
  if([txtQuantity5.text length] <=0)
  {
    txtQuantity5.text = @"0";
  }
  if([txtQuantity4.text length] <=0)
  {
    txtQuantity4.text = @"0";
  }
  if([txtQuantity3.text length] <=0)
  {
    txtQuantity3.text = @"0";
  }
  if([txtQuantity2.text length] <=0)
  {
    txtQuantity2.text = @"0";
  }
  if([txtQuantity1.text length] <=0)
  {
    txtQuantity1.text = @"0";
  }
  
  if(txtStockCode1.text.length > 0)
  {
    NSArray *ary = [txtStockCode1.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
    txtStockCode1.text = [ary objectAtIndex:0];
  }
  
  if(txtStockCode2.text.length > 0)
  {
    NSArray *ary = [txtStockCode2.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode2.text = [ary objectAtIndex:0];
  }
  if(txtStockCode3.text.length > 0)
  {
    NSArray *ary = [txtStockCode3.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode3.text = [ary objectAtIndex:0];
  }
  if(txtStockCode4.text.length > 0)
  {
    NSArray *ary = [txtStockCode4.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode4.text = [ary objectAtIndex:0];
  }
  if(txtStockCode5.text.length > 0)
  {
    NSArray *ary = [txtStockCode5.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode5.text = [ary objectAtIndex:0];
  }
  if(txtStockCode6.text.length > 0)
  {
    NSArray *ary = [txtStockCode6.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode6.text = [ary objectAtIndex:0];
  }
  if(txtStockCode7.text.length > 0)
  {
    NSArray *ary = [txtStockCode7.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode7.text = [ary objectAtIndex:0];
  }
  if(txtStockCode8.text.length > 0)
  {
    NSArray *ary = [txtStockCode8.text componentsSeparatedByString:@" "];
    if(ary.count > 0)
      txtStockCode8.text = [ary objectAtIndex:0];
  }
  
  NSString *strData = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",txtStockCode1.text,txtQuantity1.text,lblReason1.text,txtReturn1.text,txtComment1.text,txtStockCode2.text,txtQuantity2.text,lblReason2.text,txtReturn2.text,txtComment2.text,txtStockCode3.text,txtQuantity3.text,lblReason3.text,txtReturn3.text,txtComment3.text,txtStockCode3.text,txtQuantity4.text,lblReason4.text,txtReturn4.text,txtComment4.text,txtStockCode5.text,txtQuantity5.text,lblReason5.text,txtReturn5.text,txtComment5.text,txtStockCode6.text,txtQuantity6.text,lblReason6.text,txtReturn6.text,txtComment6.text,txtStockCode7.text,txtQuantity7.text,lblReason7.text,txtReturn7.text,txtComment7.text,txtStockCode8.text,txtQuantity8.text,lblReason8.text,txtReturn8.text,txtComment8.text];
    
    [self.delegate dismissStorePickUp:strData];
}

-(void)actiondismissPopOverStockPickUp:(NSString *)strPickUpCode strTextField:(NSString *)strTextField
{
    if([strTextField isEqualToString:@"txtStockCode1"])
    {
        txtStockCode1.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode2"])
    {
        txtStockCode2.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode3"])
    {
        txtStockCode3.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode4"])
    {
        txtStockCode4.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode5"])
    {
        txtStockCode5.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode6"])
    {
        txtStockCode6.text = [strPickUpCode uppercaseString];
    }
    else if([strTextField isEqualToString:@"txtStockCode7"])
    {
      txtStockCode7.text = [strPickUpCode uppercaseString];
    }
  
    else if([strTextField isEqualToString:@"txtStockCode8"])
    {
      txtStockCode8.text = [strPickUpCode uppercaseString];
    }
  
    if([strTextField isEqualToString:@"txtReturn1"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn1.text = @"Y";
        }
        else
        {
            txtReturn1.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn2"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn2.text = @"Y";
        }
        else
        {
            txtReturn2.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn3"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn3.text = @"Y";
        }
        else
        {
            txtReturn3.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn4"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn4.text = @"Y";
        }
        else
        {
            txtReturn4.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn5"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn5.text = @"Y";
        }
        else
        {
            txtReturn5.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn6"])
    {
        if([strPickUpCode isEqualToString:@"Yes"])
        {
            txtReturn6.text = @"Y";
        }
        else
        {
            txtReturn6.text = @"N";
        }
    }
    else if([strTextField isEqualToString:@"txtReturn7"])
    {
      if([strPickUpCode isEqualToString:@"Yes"])
      {
        txtReturn7.text = @"Y";
      }
      else
      {
        txtReturn7.text = @"N";
      }
    }
  
    else if([strTextField isEqualToString:@"txtReturn8"])
    {
      if([strPickUpCode isEqualToString:@"Yes"])
      {
        txtReturn8.text = @"Y";
      }
      else
      {
        txtReturn8.text = @"N";
      }
    }
    [self.popoverController dismissPopoverAnimated:YES];
}

- (void)actiondismissPopOverForReason:(NSString *)strReasonName{
    
  switch (tagTxtReason) {
    case 500:
      lblReason1.text = strReasonName;
      break;
    case 501:
      lblReason2.text = strReasonName;
      break;
    case 502:
      lblReason3.text = strReasonName;
      break;
    case 503:
      lblReason4.text = strReasonName;
      break;
    case 504:
      lblReason5.text = strReasonName;
      break;
    case 505:
      lblReason6.text = strReasonName;
      break;
    case 506:
      lblReason7.text = strReasonName;
      break;
      
    case 507:
      lblReason8.text = strReasonName;
      break;
      
      
    default:
      break;
  }
  
    [self.popoverController dismissPopoverAnimated:YES];
  
}

-(IBAction)actionDisplayReason:(id)sender{
    
    [[UIApplication sharedApplication] resignFirstResponder];
    
    UIButton *btn = (UIButton *)sender;
    tagTxtReason = btn.tag;
    ReasonListPopOverViewController *popoverContent = [[ReasonListPopOverViewController alloc] initWithNibName:@"ReasonListPopOverViewController" bundle:[NSBundle mainBundle]];
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    //popoverController.delegate = self;
    popoverContent.delegate = self;
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:btn.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

@end
