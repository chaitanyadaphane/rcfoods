//
//  QuoteProductListViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 21/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "QuoteProductListViewController.h"
#import "CustomCellGeneral.h"

#define PRODUCTLIST_WS @"stockinfo/list_stockinfo.php?"

//#define CHECKFLAG_YES @"1"
//#define CHECKFLAG_NO @"0"

#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface QuoteProductListViewController ()
{
    BOOL _loadingInProgress;
    MBProgressHUD *spinner;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;
@end

@implementation QuoteProductListViewController
@synthesize filteredListContent,arrProducts,arrSelectedProducts,isSearchingOn;
@synthesize tblProductList,btnAddToDetails,searchingBar,isSearchMode,vwContentSuperview,vwNoResults,vwResults,localSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }

    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
    {

    CGRect frm =     searchingBar.frame;
    frm.size.height = 88.0f;
    self.searchingBar.frame = frm;
    }
    else{
        
        CGRect frm =     searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
    [self.searchingBar setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isSearchMode = NO;
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrProducts = [[NSMutableArray alloc] init];
    arrSelectedProducts = [[NSMutableArray alloc] init];
    
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];

    
    //First page
    currPage = 1;
    totalCount = 0;
    
    operationQueue = [NSOperationQueue new];
    
    //Remove the plist if present
    [[NSFileManager defaultManager] removeItemAtPath:[AppDelegate getFileFromDocumentsDirectory:@"SelectedProduct.plist"] error:nil];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strMember = [prefs objectForKey:@"members"];
    
    NSString *strWarehouse = nil;
    if ([strMember isEqualToString:SALES]) {
        strWarehouse = [prefs objectForKey:@"warehouse"];
        if (strWarehouse.length < 2) {
            strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
        }
    }
   
    
    [localSpinner startAnimating];
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            
            FMResultSet *res;
            if ([strMember isEqualToString:SALES]) {
                res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM samaster WHERE TRIM(WAREHOUSE) = ?",[strWarehouse trimSpaces]];
            }
            else{
                res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM samaster"];
            }
            
            
            if (!res)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                return;
            }
            
            if ([res next]) {
                totalCount = [[[res resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [res close];
            
            [self callGetProductListFromDB:db];
            [tblProductList reloadData];
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetch];
            [localSpinner stopAnimating];
        }];
    }];

}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.tblProductList = nil;
    self.btnAddToDetails = nil;
    self.searchingBar = nil;
    self.vwResults = nil;
    self.vwContentSuperview = nil;
    self.vwNoResults = nil;
    self.localSpinner = nil;
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

-(void)doAfterDataFetch{
    if([arrProducts count] < totalCount){
        
        [tblProductList reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }

        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        
        recordNumber += ROWS_PER_PAGE;
        
    }
    else{
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [self.filteredListContent count];
    }
	else
	{
        return [self.arrProducts count];
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER7;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:6];
    }

    NSMutableDictionary *item;
    if (isSearchMode)
	{
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
//        cell.lblValue.text = @"";
        NSString *availVal =[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Available"];

        cell.lblValue.text =[NSString stringWithFormat:@"%@ AVAIL",(availVal)?availVal:@""];

        item = [filteredListContent objectAtIndex:indexPath.row];
   
    }
	else
	{
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
//        cell.lblValue.text = @"";
        
        NSString *availVal =[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Available"];
        cell.lblValue.text =[NSString stringWithFormat:@"AVAIL : %@",(availVal)?availVal:@""];
        item = [arrProducts objectAtIndex:indexPath.row];
                    
    }
    
    [item setObject:cell forKey:@"cell"];
    
    BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
       
    UIImage *image = (checked) ? [[UIImage alloc]initWithContentsOfFile:strImage1] : [[UIImage alloc]initWithContentsOfFile:strImage2];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    button.frame = frame;	// match the button's size with the image size
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
    [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self tableView: tblProductList accessoryButtonTappedForRowWithIndexPath: indexPath];
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	NSMutableDictionary *item ;
	
    if (isSearchMode)
	{
        item = [filteredListContent objectAtIndex:indexPath.row];
    }
    else{
        item = [arrProducts objectAtIndex:indexPath.row];
    }
    
	BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    
    if (checked) {
        [arrSelectedProducts removeObject:item];
    }
    else{
        [arrSelectedProducts addObject:item];
    }
    
	[item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
	    
	UITableViewCell *cell = [item objectForKey:@"cell"];
	UIButton *button = (UIButton *)cell.accessoryView;
	
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    UIImage *newImage = (checked) ? [[UIImage alloc]initWithContentsOfFile:strImage2] : [[UIImage alloc]initWithContentsOfFile:strImage1];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                                     object:self
                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:item, @"source", nil]];
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tblProductList];
	NSIndexPath *indexPath = [tblProductList indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView: tblProductList accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrProducts count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblProductList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblProductList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetProductListFromDB:db];
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetch];
        }];
    }];

}

- (void) didFinishLoadingMoreSampleData
{
	_loadingInProgress = NO;
    [tblProductList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblProductList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    
    [operationQueue cancelAllOperations];
    
    if ([scope isEqualToString:@"Code"]){
        
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetProductListByCode:db Code:searchText];
                
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                
                if ([filteredListContent count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblProductList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }
             
            }];
        }];
        
    }
    else if ([scope isEqualToString:@"Description"]){
        
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetProductListByDescription:db Description:searchText];
                
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                
                if ([filteredListContent count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblProductList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }

            }];
        }];
            
    }

}

#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblProductList reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        self.isSearchMode = YES;
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblProductList reloadData];
}

#pragma mark - IBAction Methods
- (void)actionCheckProduct:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSString *path = [AppDelegate getFileFromDocumentsDirectory:@"SelectedProduct.plist"];
    [arrSelectedProducts addObject:[arrProducts objectAtIndex:btn.tag]];
    [arrSelectedProducts writeToFile:path atomically:YES];
}

- (IBAction)actionAddProductsToDetails:(id)sender{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:arrSelectedProducts, @"source", nil]];
    
    
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

#pragma mark - Database calls
-(void)callGetProductListFromDB:(FMDatabase *)db{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSString *strMember = [prefs objectForKey:@"members"];
    
    NSString *strWarehouse  = [prefs objectForKey:@"warehouse"];
    if (strWarehouse.length < 2) {
        strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
    }
    /*if ([strMember isEqualToString:SALES]) {
        strWarehouse = [prefs objectForKey:@"warehouse"];
    }*/
    
    FMResultSet *results;
    @try {
        
//        results = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,PREF_SUPPLIER,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price1,PRICE2 as Price2,PRICE3 as Price3,PRICE4 as Price4,PRICE5 as Price5,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, PICTURE_FIELD as Picture_field,STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold,PROD_CLASS as ProdClass,PROD_GROUP as ProdGroup FROM samaster WHERE TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' ORDER BY DESCRIPTION LIMIT ?,?",[NSString stringWithFormat:@"%@",[strWarehouse trimSpaces]],[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];// AND STATUS <> 'OB'
        
          results = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,PREF_SUPPLIER,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price1,PRICE2 as Price2,PRICE3 as Price3,PRICE4 as Price4,PRICE5 as Price5,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, PICTURE_FIELD as Picture_field,STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold,PROD_CLASS as ProdClass,PROD_GROUP as ProdGroup FROM samaster WHERE TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' ORDER BY CASE WHEN PROD_CLASS = '1FRZ' THEN 1  WHEN PROD_CLASS = 'FRZN' THEN 2 WHEN PROD_CLASS = 'CHILL' THEN 3 ELSE 4 END ,PROD_CLASS,DESCRIPTION LIMIT ?,?",[NSString stringWithFormat:@"%@",[strWarehouse trimSpaces]],[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];// AND STATUS <> 'OB'
        
        
        if (!results)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }

        
        while ([results next]) {
            
            NSMutableDictionary *dictData = (NSMutableDictionary *)[results resultDictionary];
            
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            
            [dictData setValue:@"0" forKey:@"q0"];
            [dictData setValue:@"0" forKey:@"q2"];
            [dictData setValue:@"0" forKey:@"q3"];
            [dictData setValue:@"0" forKey:@"q4"];
            //--Dont add for desc product
            if([dictData objectForKey:@"Description"])
            {
                if([[dictData objectForKey:@"Description"]trimSpaces].length > 0)
                {
                    [arrProducts addObject:dictData];
                }
            }
            
        }
//        NSLog(@"%@",arrProducts);
        
        [results close];
    }
    @catch (NSException *e) {
        // rethrow if not one of the two exceptions above
        
        [results close];
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];

        }];

    }
    
}
-(void)callGetProductListByDescription:(FMDatabase *)db Description:(NSString *)description{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSString *strMember = [prefs objectForKey:@"members"];
    
    NSString *strWarehouse  = [prefs objectForKey:@"warehouse"];
    if (strWarehouse.length < 2) {
        strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
    }
    /*if ([strMember isEqualToString:SALES]) {
        strWarehouse = [prefs objectForKey:@"warehouse"];
    }
*/
    FMResultSet *rs;
    
    @try {
        
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,PREF_SUPPLIER,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price1,PRICE2 as Price2,PRICE3 as Price3,PRICE4 as Price4,PRICE5 as Price5,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume,PICTURE_FIELD as Picture_field, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE DESCRIPTION LIKE ? AND TRIM(WAREHOUSE) = ? AND STATUS <> 'OB'", [NSString stringWithFormat:@"%%%@%%", description],strWarehouse];

        
        /*if ([strMember isEqualToString:SALES]) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE DESCRIPTION LIKE ? AND WAREHOUSE = ?", [NSString stringWithFormat:@"%%%@%%", description],strWarehouse];
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE DESCRIPTION LIKE ?", [NSString stringWithFormat:@"%%%@%%", description]];
        }*/
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            
            NSMutableDictionary *dictData = (NSMutableDictionary *)[rs resultDictionary];

            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            
            [dictData setValue:@"0" forKey:@"q0"];
            [dictData setValue:@"0" forKey:@"q2"];
            [dictData setValue:@"0" forKey:@"q3"];
            [dictData setValue:@"0" forKey:@"q4"];
            
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}
-(void)callGetProductListByCode:(FMDatabase *)db Code:(NSString *)code{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSString *strMember = [prefs objectForKey:@"members"];
    
    NSString *strWarehouse = [prefs objectForKey:@"warehouse"];
    if (strWarehouse.length < 2) {
        strWarehouse = [NSString stringWithFormat:@"0%@",strWarehouse];
    }
    /*if ([strMember isEqualToString:SALES]) {
        strWarehouse = [prefs objectForKey:@"warehouse"];
    }*/

    
    FMResultSet *rs;
    
    @try {
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,PREF_SUPPLIER,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price1,PRICE2 as Price2,PRICE3 as Price3,PRICE4 as Price4,PRICE5 as Price5,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume,PICTURE_FIELD as Picture_field, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE CODE LIKE ? AND TRIM(WAREHOUSE) = ? AND STATUS <> 'OB'", [NSString stringWithFormat:@"%%%@%%", code],strWarehouse];
        
        /*if ([strMember isEqualToString:SALES]) {
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE CODE LIKE ? AND WAREHOUSE = ?", [NSString stringWithFormat:@"%%%@%%", code],strWarehouse];
        }
        else{
            rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available,ON_HAND as OnHand,PURCHASE_ORDER as PurchaseOrder,SUBSTITUTE as Subsitute,ALLOCATED as Allocated, SALES_DISS as Dissection, COST_DISS as Dissection_Cos, WEIGHT as Weight, VOLUME as Volume, STANDARD_COST as Cost, AVERAGE_COST as AverageCost, LAST_SOLD as LastSold FROM samaster WHERE CODE LIKE ?", [NSString stringWithFormat:@"%%%@%%", code]];
        }*/
        
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {            
            NSMutableDictionary *dictData = (NSMutableDictionary *)[rs resultDictionary];
            
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            
            [dictData setValue:@"0" forKey:@"q0"];
            [dictData setValue:@"0" forKey:@"q2"];
            [dictData setValue:@"0" forKey:@"q3"];
            [dictData setValue:@"0" forKey:@"q4"];
            
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

#pragma mark - WS Methods
-(void)callWSGetProductList:(int)pageIndex
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_PRODUCT_LIST";
    
    NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d",pageIndex,ROWS_PER_PAGE];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,PRODUCTLIST_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
  //   NSLog(@"%@", dictResponse);
    
    [spinner removeFromSuperview];
    
    //NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    if ([strWebserviceType isEqualToString:@"WS_GET_PRODUCT_LIST"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            if(!self.arrProducts){
                arrProducts = [[NSMutableArray alloc] init];
            }
            
            if(!self.arrSelectedProducts){
                arrSelectedProducts = [[NSMutableArray alloc] init];
            }            
            
            if (_loadMoreFooterView == nil) {
                [self setupLoadMoreFooterView];
            }
            
            //Total Records
            totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];

            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                
                NSMutableDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"];                
                [arrProducts addObject:dict];
                //[arrSelectedProducts addObject:dict];
                
            }
            else{
                
                [arrProducts addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"]];
                
                //[arrSelectedProducts addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"]];
                
            }
            
            [tblProductList reloadData];
            
            if([arrProducts count] < totalCount){
                [self repositionLoadMoreFooterView];
                
                // Dismiss loading footer
                [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                
                currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                
                //Next page
                currPage ++;
                
            }
            else{
                [_loadMoreFooterView removeFromSuperview];
                _loadMoreFooterView=nil;
            }

            
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];            
        }
    }

}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


@end
