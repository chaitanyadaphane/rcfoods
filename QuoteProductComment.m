

#import "QuoteProductComment.h"


@interface QuoteProductComment ()

@end

@implementation QuoteProductComment

@synthesize commentStr,commntTAg,commentTextView,btnComment,delegate,productIndex,isCommentAddedOnLastLine,swtchComment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    commentTextView.delegate = self;
    commentTextView.layer.cornerRadius = 5;
    commentTextView.text = commentStr;
    
    if (commntTAg == 0) {
        btnComment.enabled = YES;
    }
    else{
        btnComment.enabled = NO;
        commentTextView.editable = NO;
    }
    
    swtchComment.on = isCommentAddedOnLastLine;
}

-(void)viewDidUnload{
    [super viewDidUnload];
    self.commentTextView = nil;
    self.btnComment = nil;
}

#pragma mark UITextView delegates

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    NSRange range = NSMakeRange(0,1);
    [textView scrollRangeToVisible:range];
    return YES;
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText: (NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - Comment Method

/*
- (IBAction)switchToggled:(id)sender {
    UISwitch *swtch = (UISwitch*)sender;
    if (swtch.on) {
        [self.delegate setCommentPosition:YES];
    }
    else{
        [self.delegate setCommentPosition:NO];
    }
}*/

-(IBAction)sendComment:(id)sender
{
    if (![commentTextView.text length] == 0) {
        
//        if (swtchComment.on) {
//            [self.delegate setCommentPosition:YES];
//        }
//        else{
//            [self.delegate setCommentPosition:NO];
//        }
        [self.delegate setCommentPosition:YES];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:@"0" forKey:@"Available"];
        [dict setValue:commentTextView.text forKey:@"Description"];
        [dict setValue:@"0" forKey:@"Location"];
        [dict setValue:@"0" forKey:@"Price"];
        [dict setValue:@"0" forKey:@"ScmRecum"];
        [dict setValue:@"-" forKey:@"StockCode"];
        [dict setValue:@"0" forKey:@"Warehouse"];
        [dict setValue:@"0" forKey:@"CheckFlag"];
        [dict setValue:@"/C" forKey:@"Item"];
        [dict setValue:@"2" forKey:@"isNewProduct"];
        [dict setValue:@"/C" forKey:@"Dissection"];
        [dict setValue:@"" forKey:@"Dissection_Cos"];
        
        [self.delegate addComment:productIndex Dict:dict];
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Comment added successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter comment" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark touch delegates

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [commentTextView resignFirstResponder];
    
}

@end
