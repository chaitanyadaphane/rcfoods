//
//  TableHeaderView.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 11/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "StaticView.h"

@implementation StaticView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
