//
//  ReasonListPopOverViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "ReasonListPopOverViewController.h"
#import "CustomCellGeneral.h"

@interface ReasonListPopOverViewController ()

@end

@implementation ReasonListPopOverViewController
@synthesize delegate,arrReasonList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrReasonList = [[NSArray alloc] initWithObjects:@"CANCEL",@"CONTAMINAT",@"DAMAGED",@"FAULTY",@"MICHELS",@"NOT ORDERED",@"NOT REQD",@"NOT OF DATE",@"SHORT DEL", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrReasonList count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.textLabel.text = [arrReasonList objectAtIndex:[indexPath row]];
    
    return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate actiondismissPopOverForReason:[arrReasonList objectAtIndex:[indexPath row]]];
}

@end
