//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "CustomerOrderFormProductViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"

@interface CustomerOrderFormProductViewController ()

@end

@implementation CustomerOrderFormProductViewController
@synthesize dictCustomerHistoryDetails,strTranCode,isFromCustomerHistory,tblDetails,lblTansactionNumber,strPrice,strDebtor,strPriceType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"CustomerOrderFormProductDetail" Type:@"plist"];;
    
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    lblTansactionNumber.text = strTranCode;
    
    self.strPriceType = @"(Contract price)";
    
    if ([[dictCustomerHistoryDetails objectForKey:@"Price"]objectForKey:@"text"]) {
        
    }
    else{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            [self callGetDiscountFromDB:db];
        }];

    }
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblDetails = nil;
    self.lblTansactionNumber = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Text Field Delgates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  CGRect viewFrame = self.view.frame;
    // viewFrame.origin.y = upframeByYPosition;
    // [AppDelegate pullUpPushDownViews:self.view ByRect:viewFrame];
}


-(void)callGetDiscountFromDB:(FMDatabase *)db{
    
    @try {
        NSLog(@"%@",self.custCatogry);
        
        NSLog(@"%@",dictCustomerHistoryDetails);
        
        
        
        
        //For Price Code - NEW LOGIC SACNTRCT
        
        FMResultSet *rs001 = [db executeQuery:@"SELECT AGREED_SELL_PRC,MARGIN_PCT  FROM sacntrct WHERE CODE = ? AND STOCK_CODE = ? AND FROM_DATE <  DATETIME('now')  and TO_DATE > DATETIME('now') LIMIT 1",strDebtor,[[dictCustomerHistoryDetails objectForKey:@"StockCode"] objectForKey:@"text"]];
        
        //FMResultSet *rs001 = [db executeQuery:@"SELECT AGREED_SELL_PRC,MARGIN_PCT  FROM sacntrct WHERE CODE = ? AND STOCK_CODE = ? AND TO_DATE BETWEEN DATETIME('now') AND DATETIME('now','360 days') LIMIT 1",strDebtor,stock_code];
        
        if (!rs001)
        {
            [rs001 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        NSDictionary *dictSacntrct = [NSDictionary new];
        if ([rs001 next])
        {
            dictSacntrct = [rs001 resultDictionary];
        }
        [rs001 close];
        
        if (dictSacntrct.count > 0)
        {
            self.strPrice = [dictSacntrct objectForKey:@"AGREED_SELL_PRC"];
            self.strMargin = [dictSacntrct objectForKey:@"MARGIN_PCT"];
            self.isProductFromSacontrct = @"Y";
        }
        else
        {
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs1 next]) {
                NSDictionary *dictData = [rs1 resultDictionary];
                
                switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                    case 0:
                    {
                        if ([dictCustomerHistoryDetails objectForKey:@"Price1"])
                        {
                            priceCode = [[[dictCustomerHistoryDetails objectForKey:@"Price1"]objectForKey:@"text"] floatValue];
                        }
                        else if([dictCustomerHistoryDetails objectForKey:@"Price"])
                        {
                            priceCode = [[dictCustomerHistoryDetails objectForKey:@"Price"] floatValue];
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        priceCode = [[dictCustomerHistoryDetails objectForKey:@"Price1"] floatValue];
                    }
                        break;
                        
                    case 2:
                    {  priceCode = [[[dictCustomerHistoryDetails  objectForKey:@"Price2"] objectForKey:@"text"] floatValue];
                    }
                        break;
                        
                    case 3:
                    {
                        priceCode = [[[dictCustomerHistoryDetails objectForKey:@"Price3"] objectForKey:@"text"] floatValue];
                    }
                        break;
                        
                    case 4:
                    {
                        priceCode = [[[dictCustomerHistoryDetails  objectForKey:@"Price4"] objectForKey:@"text"] floatValue];
                    }
                        break;
                        
                    case 5:
                    {
                        priceCode = [[[dictCustomerHistoryDetails  objectForKey:@"Price5"] objectForKey:@"text"] floatValue];
                    }
                        break;
                        
                        
                    default:{
                        priceCode = [[[dictCustomerHistoryDetails  objectForKey:@"Price1"] objectForKey:@"text"] floatValue];
                    }
                        break;
                        
                }
                
                if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
                {
                    if([[dictCustomerHistoryDetails objectForKey:@"Price"]objectForKey:@"text"])
                    {
                        priceCode = [[[dictCustomerHistoryDetails objectForKey:@"Price"]objectForKey:@"text"] floatValue];
                    }
                }
            }
            else
            {
                priceCode = 0;
            }
            
            [rs1 close];

        }
        
        
        
        
//        FMResultSet *rs1 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ?",[dictCustomerHistoryDetails objectForKey:@"StockCode"],[dictCustomerHistoryDetails objectForKey:@"CUSTOMER_CODE"]];
//        
//        if (!rs1)
//        {
//            [rs1 close];
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//        }
//        
//        if ([rs1 next]) {
//            NSDictionary *dictData = [rs1 resultDictionary];
//            [rs1 close];
//            
//            if ([[dictData objectForKey:@"TYPE1"] isEqualToString:@"A"] ) {
//                self.strPriceType = @"(Special price)";
//            }
//            else if ([[dictData objectForKey:@"TYPE1"] isEqualToString:@"N"]){
//                self.strPriceType = @"(List price)";
//            }
//            
//            self.strPrice = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"DISCOUNT1"]];
//        }
//        else{
//            [self callGetPriceFromDB:db];
//        }
        
        
        
        
        //*******************ketaki code modified********************//
        
        //For Dicount
        
        FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND ((date() BETWEEN date(DATE_FROM1) AND date(DATE_to1)) OR (date() BETWEEN date(DATE_FROM2) AND date(DATE_TO2)))", [[dictCustomerHistoryDetails objectForKey:@"StockCode"] objectForKey:@"text"], strDebtor];
        
         FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = '' AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", [[dictCustomerHistoryDetails objectForKey:@"StockCode"] objectForKey:@"text"]];
        
            FMResultSet *rs2221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = '' AND ((date() BETWEEN date(DATE_FROM1) AND date(DATE_to1)) OR (date() BETWEEN date(DATE_FROM2) AND date(DATE_TO2)))", [[dictCustomerHistoryDetails objectForKey:@"StockCode"] objectForKey:@"text"]];
        
        
        // float DiscountVal1 = 0;
        // float DiscountVal2 = 0;
        
        float discountValwithKey1 = 0;
        float discountValwithKey2 = 0;
        
        float discountValwithOutKey1 = 0;
        
        float discountValwithOutKey2 = 0;
        if (!rs21)
            
        {
            
            [rs21 close];
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            
        }
        
        if (!rs221)
        {
            [rs221 close];
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            
        }
        
        if (!rs2221)
        {
            [rs2221 close];
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            
        }
        if ([rs21 next])
            
        {
            NSDictionary *dictData = [rs21 resultDictionary];
            [rs21 close];
            NSLog(@"%@",dictData);
             NSString *strTYPE1 = [dictData objectForKey:@"TYPE1"];
            
            
            
            if ([strTYPE1 isEqualToString:@"P"]) {
                
            //    priceCode = priceCode - (priceCode * 0.05);
                
              //  discountValwithKey1 = priceCode;
                
            }
            
            else if ([strTYPE1 isEqualToString:@"M"])
            {
                
                
                
                NSString *discount1Val = [dictData objectForKey:@"DISCOUNT1"];
                
                NSString *strCost = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Cost"] objectForKey:@"text"]];
                
                
                
               NSString *strMargin =[dictData objectForKey:@"DISCOUNT1"];
              
                
                
                CGFloat sellprice = ([strCost floatValue] / ( 1- ([discount1Val floatValue] / 100)));
                
                strPrice = [NSString stringWithFormat:@"%.2f",sellprice];
                
                return;
            }
            else{
                
                
                
                //compare discountValwithKey1 and discountValwithKey2
                
                
                
                discountValwithKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                
                discountValwithKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                
                
                
                if (discountValwithKey1 == 0) {
                    
                    discount =discountValwithKey2;
                    
                }
                
                else if (discountValwithKey2 == 0){
                    
                    discount = discountValwithKey1;
                    
                }
                
                else{
                    
                    if (discountValwithKey1 > discountValwithKey2 ) {
                        
                        discount = discountValwithKey2;
                        
                    }
                    
                    else{
                        
                        discount = discountValwithKey1;
                        
                    }
                    
                }
                
            }
            
            
            

        }
        else
        {
            if ([rs2221 next])
                
            {
                
                NSDictionary *dictData = [rs2221 resultDictionary];
                
                [rs2221 close];
                
                NSString *strTYPE1 = [dictData objectForKey:@"TYPE1"];
                
                
                
                if ([strTYPE1 isEqualToString:@"P"])
                {
                    
                    priceCode = priceCode - (priceCode * 0.05);
                    
                    discountValwithOutKey1 = priceCode;
                    
                }
                else
                {
                    //compare discountValwithOutKey1 and discountValwithOutKey2
                    
                    
                    
                    discountValwithOutKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    
                    discountValwithOutKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                    
                    
                    
                    if (discountValwithOutKey1 == 0) {
                        
                        discount =discountValwithOutKey2;
                        
                    }
                    
                    else if (discountValwithOutKey2 == 0){
                        
                        discount = discountValwithOutKey1;
                        
                    }
                    
                    else{
                        
                        if (discountValwithOutKey1 > discountValwithOutKey2 ) {
                            
                            discount = discountValwithOutKey2;
                            
                        }
                        
                        else{
                            
                            discount = discountValwithOutKey1;
                            
                        }
                        
                    }
                    
                }
                
            }
            else
            {
                if ([rs221 next])
                    
                {
                    
                    NSDictionary *dictData = [rs221 resultDictionary];
                    
                    [rs221 close];
                    
                    NSString *strTYPE1 = [dictData objectForKey:@"TYPE1"];
                    
                    
                    
                    if ([strTYPE1 isEqualToString:@"P"]) {
                        
                        priceCode = priceCode - (priceCode * 0.05);
                        
                        discountValwithOutKey1 = priceCode;
                        
                    }
                    else
                    {
                        //compare discountValwithOutKey1 and discountValwithOutKey2
                      
                        discountValwithOutKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                        
                        discountValwithOutKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                        
                        
                        
                        if (discountValwithOutKey1 == 0) {
                            
                            discount =discountValwithOutKey2;
                            
                        }
                        
                        else if (discountValwithOutKey2 == 0){
                            
                            discount = discountValwithOutKey1;
                            
                        }
                        
                        else{
                            
                            if (discountValwithOutKey1 > discountValwithOutKey2 ) {
                                
                                discount = discountValwithOutKey2;
                                
                            }
                            
                            else{
                                
                                discount = discountValwithOutKey1;
                                
                            }
                        }
                    }
                }
                else
                {
                    [self callGetPriceFromDB:db];
                }
            }

            }
        
        
    }
        
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

-(void)callGetPriceFromDB:(FMDatabase *)db{
    
    //NSLog(@"strDebtor %@",strDebtor);
    @try {
        
        //NSLog(@"SELECT PRICE_CODE FROM armaster WHERE CODE = %@adsafasfasfa",strDebtor);
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price1"]objectForKey:@"text"]];
                    
                }
                    break;
                case 1:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price1"] objectForKey:@"text"]];
                }
                    break;
                    
                case 2:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price2"] objectForKey:@"text"]];
                }
                    break;
                    
                case 3:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price3"] objectForKey:@"text"]];
                }
                    break;
                    
                case 4:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price4"] objectForKey:@"text"]];
                }
                    break;
                    
                case 5:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictCustomerHistoryDetails objectForKey:@"Price5"]];
                }
                    break;
                    
                    
                default:{
                    self.strPrice = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Price1"] objectForKey:@"text"]];
                }
                    break;
            }
            
        }
        
        [rs1 close];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}


#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return [arrHeaderLabels count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier = CELL_IDENTIFIER5;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:4];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    if ([indexPath section] == 0) {
        switch ([indexPath row]) {
            case 0:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"StockCode"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"StockCode"] objectForKey:@"text"]];
                }
            }
                break;
        }
    }
    
    if ([indexPath section] == 1) {
        switch ([indexPath row]) {

            case 0:
            {
//                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f%@",[strPrice floatValue],strPriceType];
                if ([dictCustomerHistoryDetails objectForKey:@"Price"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerHistoryDetails objectForKey:@"Price"]objectForKey:@"text"]floatValue]]];
                }
                
            }
                break;
            case 1:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"q0"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"q0"] objectForKey:@"text"]];
                }
            }
                break;
                
            case 2:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"q1"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"q1"] objectForKey:@"text"]];
                }
                
                
            }
                break;
            case 3:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"q2"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"q2"] objectForKey:@"text"]];
                }
                
            }
                break;
                
            case 4:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"q3"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"q3"] objectForKey:@"text"]];
                }
                
            }
                break;
                
            case 5:
            {
                if ([dictCustomerHistoryDetails objectForKey:@"q4"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"q4"] objectForKey:@"text"]];
                }
                
            }
                break;
            case 6:
            {
                if ([[dictCustomerHistoryDetails objectForKey:@"LastSold"]objectForKey:@"text"] && ![[[dictCustomerHistoryDetails objectForKey:@"LastSold"]objectForKey:@"text"] isEqualToString:STANDARD_APP_DATE] && ![[[dictCustomerHistoryDetails objectForKey:@"LastSold"] objectForKey:@"text"] isEqualToString:STANDARD_SERVER_DATE]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[[dictCustomerHistoryDetails objectForKey:@"LastSold"] objectForKey:@"text"]]];
                }
                
            }
                break;
                
            case 7:
            {
                if ([[dictCustomerHistoryDetails objectForKey:@"Gst"] objectForKey:@"text"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerHistoryDetails objectForKey:@"Gst"] objectForKey:@"text"]];
                }
                
            }
                break;
                
                
            default:
                break;
                
        }
    }

        
    
    return cell;
    
}


- (IBAction)saveProductDetails:(id)sender{
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

@end
