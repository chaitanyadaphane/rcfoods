//
//  DashboardViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "DashboardViewController.h"
#import "StaticView.h"
#import "CustomCellGeneral.h"
#import "DownloadUrlOperation.h"
#import "OpenPdfWebViewController.h"


#define HANDLE_VIEW_WIDTH 38
#define TBL_NOTICEBOARD_TAG 1
#define TBL_NEWPRODUCTS_TAG 2
#define TBL_ROW_HEIGHT 50
#define HANDLE_VIEW_OPENING_X 860
#define HANDLE_VIEW_OFFSET 50

#define NEWPRODUCTSWS @"newproducts.php?"

#define STR_NOTICEBOARD @"NoticeBoard"
#define STR_NEWPRODUCTS @"NewProducts"
#define STR_SPECIALS @"Specials"
#define STR_PUSHNOTIFICATION @"PushNotification"

@interface DashboardViewController ()
{
    UIView *viewDays;
    NSString *strDays;
}

@property (nonatomic, strong) JFDepthView* depthView;
@property (nonatomic, strong) TopViewController* topViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDays;
@property (weak, nonatomic) IBOutlet UIView *vwActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

- (IBAction)btnSelectDaysClicked:(id)sender;

@end

@implementation DashboardViewController
@synthesize arrNewProducts,arrNoticeBoards,strSpecialsPdfUrl,strSpecialsPdfThumbnailUrl,dictSpecials;
@synthesize vwNewMessagesContentView,vwNewProductsContentView,vwNewProducts,vwNewMessages,vwNoMessages,vwNoProducts,viewSpecials,tblNoticeBoard,tblNewProducts,localSpinner1,localSpinner2,localSpinner3;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}


#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
    _vwActivityIndicator.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.2];
    _vwActivityIndicator.hidden = YES;
    
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    self.depthView = [[JFDepthView alloc] initWithGestureRecognizer:tapRec];
    self.depthView.delegate = self;
    
    viewSpecials = [[[NSBundle mainBundle] loadNibNamed:@"SpecialsView" owner:self options:nil] objectAtIndex:0];
    
    viewSpecials.animate = NO;
    viewSpecials.delegate = self;
    
    [viewSpecials setUpPullableView:viewSpecials.frame];
    
    int handleViewOpeningX = HANDLE_VIEW_OPENING_X;
    int handleViewClosingX = HANDLE_VIEW_OPENING_X - viewSpecials.frame.size.width;
    handleViewClosingX += HANDLE_VIEW_OFFSET;
    
    viewSpecials.openedCenter = CGPointMake(handleViewOpeningX, self.view.frame.size.height/2);
    viewSpecials.closedCenter = CGPointMake(handleViewClosingX, self.view.frame.size.height/2);
    
    viewSpecials.center = viewSpecials.openedCenter;
    viewSpecials.handleView.frame = CGRectMake(0, viewSpecials.handleView.frame.size.height/2, viewSpecials.handleView.frame.size.width, viewSpecials.handleView.frame.size.height);
    
    //btnRefresh Button
    UIButton *btnRefresh = [[[NSBundle mainBundle] loadNibNamed:@"SpecialsView" owner:self options:nil] objectAtIndex:4];
    btnRefresh.frame = CGRectMake(129,284,btnRefresh.frame.size.width,btnRefresh.frame.size.height);
    [btnRefresh addTarget:self action: @selector(actionDownloadSpecials:)forControlEvents:UIControlEventTouchUpInside];
    
    [viewSpecials addSubview:btnRefresh];
    [self.view addSubview:viewSpecials];
    
    arrNoticeBoards = [[NSMutableArray alloc] init];
    arrNewProducts = [[NSMutableArray alloc] init];
    dictSpecials = [[NSMutableDictionary alloc] init];
    
    //---Set Default to 5 days
    strDays = @"5";
    
    backgroundQueueForSpecials = dispatch_queue_create("com.nanan.myscmipad.bgqueueForSpecials", NULL);
    
    operationQueue = [NSOperationQueue new];
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        [localSpinner1 startAnimating];
        [localSpinner2 startAnimating];
        
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inDatabase:^(FMDatabase *db) {
                [self callGetSpecialsFromDB:db];
            }];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSString *strAttachmentServerUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strSpecialsPdfThumbnailUrl];
                NSString *strAttachmentUrl = [[AppDelegate getAppDelegateObj] getFileFromCacheDir:strAttachmentServerUrl];
                
                UIImage *img= [UIImage imageWithContentsOfFile:strAttachmentUrl];
                
                if (!img)
                {
                    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"Default-Landscape" ofType:@"png"];
                    img = [UIImage imageWithContentsOfFile:strImage];
                    [viewSpecials.btnSpecialsThumbnail removeTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                    strImage = nil;
                }
                else
                {
                    [viewSpecials.btnSpecialsThumbnail addTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                }
                [viewSpecials.btnSpecialsThumbnail setImage:img forState:UIControlStateNormal];
            }];
        }];
        
        [self callDashBoardWebservices];
    }
    else
    {
        [self callDashBoardDataFromDB];
    }
    self.btnSelectDays.enabled = YES;
    
}

- (IBAction)btnSelectDaysClicked:(id)sender
{
    viewDays = [[UIView alloc] initWithFrame:CGRectMake(_btnSelectDays.frame.origin.x, _btnSelectDays.frame.origin.y + 40, 200, 261)];
    [viewDays setBackgroundColor:[UIColor whiteColor]];

    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setFrame:CGRectMake(5,35, 190, 40)];
    [btn1 setBackgroundColor:[UIColor clearColor]];
    [btn1 setTitle:@"5 days" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(daysCLicked:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = 5;
  
  
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setFrame:CGRectMake(5,77, 190, 40)];
    [btn2 setBackgroundColor:[UIColor clearColor]];
    [btn2 setTitle:@"10 days" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(daysCLicked:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = 10;
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn3 setFrame:CGRectMake(5,119, 190, 40)];
    [btn3 setBackgroundColor:[UIColor clearColor]];
    [btn3 setTitle:@"20 days" forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(daysCLicked:) forControlEvents:UIControlEventTouchUpInside];
    btn3.tag = 20;
    
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn4 setFrame:CGRectMake(5,161, 190, 40)];
    [btn4 setBackgroundColor:[UIColor clearColor]];
    [btn4 setTitle:@"30 days" forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(daysCLicked:) forControlEvents:UIControlEventTouchUpInside];
    btn4.tag = 30;
    
    UIButton *btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn5 setFrame:CGRectMake(5,203, 190, 40)];
    [btn5 setBackgroundColor:[UIColor clearColor]];
    [btn5 setTitle:@"More than 30 days" forState:UIControlStateNormal];
    [btn5 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn5 addTarget:self action:@selector(daysCLicked:) forControlEvents:UIControlEventTouchUpInside];
    btn5.tag = 60;
    
    [viewDays addSubview:btn1];
    [viewDays addSubview:btn2];
    [viewDays addSubview:btn3];
    [viewDays addSubview:btn4];
    [viewDays addSubview:btn5];
    
    [self.view addSubview:viewDays];
  
    btn5.layer.borderWidth = btn4.layer.borderWidth = btn3.layer.borderWidth = btn2.layer.borderWidth = btn1.layer.borderWidth = 1.0;
  
  btn1.layer.borderColor = [UIColor blackColor].CGColor;
  btn2.layer.borderColor = [UIColor blackColor].CGColor;
  btn3.layer.borderColor = [UIColor blackColor].CGColor;
  btn4.layer.borderColor = [UIColor blackColor].CGColor;
  btn5.layer.borderColor = [UIColor blackColor].CGColor;
    btn1 = nil;
    btn2 = nil;
    btn3 = nil;
    btn4 = nil;
    btn5 = nil;
}

-(IBAction)daysCLicked:(id)sender
{
    [viewDays removeFromSuperview];
    UIButton *tmpBtn = (UIButton*)sender;
    [_btnSelectDays setTitle:tmpBtn.titleLabel.text forState:UIControlStateNormal];
    
    strDays = [NSString stringWithFormat:@"%d",tmpBtn.tag];
    [self callDashboard];
  
//   dispatch_async(dispatch_get_main_queue(), ^(void) {
//    [localSpinner2 stopAnimating];
//    [vwNewProductsContentView  removeAllSubviews];
//    
//    if ([arrNewProducts count])
//    {
//      [vwNewProductsContentView addSubview:vwNewProducts];
//      [tblNewProducts reloadData];
//    }
//    else
//    {
//      [vwNewProductsContentView addSubview:vwNoProducts];
//    }
//    
//  });
    

        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            
            [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callGetNoticeMessagesFromDB:db];
            }];
            
            databaseQueue = nil;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [localSpinner1 stopAnimating];
                [vwNewMessagesContentView removeAllSubviews];
                
                if ([arrNoticeBoards count]) {
                    [vwNewMessagesContentView addSubview:vwNewMessages];
                    [tblNoticeBoard reloadData];
                }
                else{
                    [vwNewMessagesContentView addSubview:vwNoMessages];
                }
                
            });
        });
        
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [operationQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    //Remove observer 15th July 2015
//    for (NSOperation *operation in [operationQueue operations]) {
//        [operation removeObserver:self forKeyPath:@"isFinished"];
//    }
//    
}


#pragma mark - Database calls

-(void)callGetNewProductsFromDB:(FMDatabase *)db{

    [arrNewProducts removeAllObjects];
    
    FMResultSet *rs2;
    if([strDays isEqualToString:@"60"])
    {
      rs2 = [db executeQuery:@"SELECT * FROM new_samaster WHERE InsertDate >= date('now','-60 day') AND WAREHOUSE = ? ORDER BY InsertDate DESC LIMIT 0,100",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
    }
    else if ([strDays isEqualToString:@"10"])
    {
      rs2 = [db executeQuery:@"SELECT * FROM new_samaster WHERE InsertDate >= date('now','-10 day') AND WAREHOUSE = ? ORDER BY InsertDate DESC LIMIT 0,100",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
    }
    else if ([strDays isEqualToString:@"20"])
    {
      rs2 = [db executeQuery:@"SELECT * FROM new_samaster WHERE InsertDate >= date('now','-20 day') AND WAREHOUSE = ? ORDER BY InsertDate DESC LIMIT 0,100",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
  
    }
    else if ([strDays isEqualToString:@"30"])
    {
      rs2 = [db executeQuery:@"SELECT * FROM new_samaster WHERE InsertDate >= date('now','-30 day') AND WAREHOUSE = ? ORDER BY InsertDate DESC LIMIT 0,100",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
    }
    else
    {
      rs2 = [db executeQuery:@"SELECT * FROM new_samaster WHERE InsertDate >= date('now','-5 day') AND WAREHOUSE = ? ORDER BY InsertDate DESC LIMIT 0,100",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
    }
  
    while ([rs2 next]) {
        
        NSDictionary *dictData = [rs2 resultDictionary];
        NSMutableDictionary *dictNewProduct = [[NSMutableDictionary alloc] init];
        
        [dictNewProduct setValue:[dictData objectForKey:@"CODE"] forKey:@"Code"];
        [dictNewProduct setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
        [dictNewProduct setValue:[dictData objectForKey:@"InsertDate"] forKey:@"InsertDate"];
        [dictNewProduct setValue:[dictData objectForKey:@"LOCATION"] forKey:@"Location"];
        [dictNewProduct setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
        [dictNewProduct setValue:[dictData objectForKey:@"PROD_GROUP"] forKey:@"ProductGroup"];
        [dictNewProduct setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
        
        [arrNewProducts addObject:dictNewProduct];
        
        dictData=nil;
    }
    
    [rs2 close];
    
}

-(void)callGetNoticeMessagesFromDB:(FMDatabase *)db{
    
    FMResultSet *rs1 = [db executeQuery:@"SELECT MESSAGE_ID as MessageId, DATE as Date, MESSAGE_CONTENT as MessageContent, CREATED_BY as CreatedBy, READ_FLAG as ReadFlag, ATTACHMENT as Attachment FROM noticemsgs WHERE DATE >= date('now','-30 day') AND STATUS = 1 ORDER BY modified_date DESC"];
    
    if (!rs1) {
        DLog(@"No noticemsgs");
    }
    else{
        
        [arrNoticeBoards removeAllObjects];
        
        while ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [arrNoticeBoards addObject:dictData];
            dictData=nil;
        }
        
        
    }
    
    [rs1 close];
}

-(void)callGetSpecialsFromDB:(FMDatabase *)db{
    
    FMResultSet *rs3 = [db executeQuery:@"SELECT * FROM specials WHERE published = 1"];
    
    if (!rs3) {
        DLog(@"Error %@",[db lastError]);
    }
    else{
        
        if ([rs3 next]) {
            NSDictionary *dictData = [rs3 resultDictionary];
            
            [dictSpecials setValue:[dictData objectForKey:@"SCM_RECNUM"] forKey:@"ScmRecnum"];
            [dictSpecials setValue:[dictData objectForKey:@"created_date"] forKey:@"CreatedDate"];
            [dictSpecials setValue:[dictData objectForKey:@"modified_date"] forKey:@"ModifiedDate"];
            [dictSpecials setValue:[dictData objectForKey:@"published"] forKey:@"published"];
            [dictSpecials setValue:[dictData objectForKey:@"pdfthumbnail"] forKey:@"pdfthumbnail"];
            [dictSpecials setValue:[dictData objectForKey:@"title"] forKey:@"title"];
            [dictSpecials setValue:[dictData objectForKey:@"pdffile"] forKey:@"pdffile"];
            
            self.strSpecialsPdfThumbnailUrl = [dictData objectForKey:@"pdfthumbnail"];
            self.strSpecialsPdfUrl = [dictData objectForKey:@"pdffile"];
            
            dictData=nil;
        }
    }
    
    [rs3 close];
}


-(void)callDashBoardDataFromDB{
    
    [localSpinner1 startAnimating];
    [localSpinner2 startAnimating];
    //[viewSpecials addSubview:localSpinner3];
    //[localSpinner3 startAnimating];
    
    [operationQueue addOperationWithBlock:^{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            [self callGetNoticeMessagesFromDB:db];
        }];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [localSpinner1 stopAnimating];
            [vwNewMessagesContentView removeAllSubviews];
            
            if ([arrNoticeBoards count]) {
                [vwNewMessagesContentView addSubview:vwNewMessages];
                [tblNoticeBoard reloadData];
            }
            else{
                [vwNewMessagesContentView addSubview:vwNoMessages];
            }
            
        }];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            [self callGetNewProductsFromDB:db];
        }];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [localSpinner2 stopAnimating];
            [vwNewProductsContentView removeAllSubviews];
            
            if ([arrNewProducts count]) {
                [vwNewProductsContentView addSubview:vwNewProducts];
                [tblNewProducts reloadData];
            }
            else{
                [vwNewProductsContentView addSubview:vwNoProducts];
            }
            
        }];
        
    }];
    
    [operationQueue addOperationWithBlock:^{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            [self callGetSpecialsFromDB:db];
        }];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //[localSpinner3 stopAnimating];
            //[localSpinner3 removeFromSuperview];
            
            NSString *strAttachmentServerUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strSpecialsPdfThumbnailUrl];
            NSString *strAttachmentUrl = [[AppDelegate getAppDelegateObj] getFileFromCacheDir:strAttachmentServerUrl];
            
            UIImage *img= [UIImage imageWithContentsOfFile:strAttachmentUrl];
            
            if (!img) {
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"Default-Landscape" ofType:@"png"];
                
                img = [UIImage imageWithContentsOfFile:strImage];
                [viewSpecials.btnSpecialsThumbnail removeTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                strImage = nil;
            }
            else{
                [viewSpecials.btnSpecialsThumbnail addTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [viewSpecials.btnSpecialsThumbnail setImage:img forState:UIControlStateNormal];
           
        }];
        
    }];
    
}



#pragma mark - WS calls
//--Get the dashboard data
-(void)callDashboard
{
    self.btnSelectDays.enabled = NO;
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        
        
        _vwActivityIndicator.hidden = NO;
        [_activityLoad startAnimating];
            NSString *strNewProductsURL = [NSString stringWithFormat:@"%@%@%@",WSURL,NEWPRODUCTSWS,[NSString stringWithFormat:@"days=%@",strDays]];
        
        
        // set maximum operations possible
//        [operationQueue setMaxConcurrentOperationCount:1];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(dataAvailable:)
         name:@"DataDownloadFinished"
         object:nil ] ;
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(dataUnAvailable:)
         name:@"DataDownloadFailed"
         object:nil];
        
        NSURL *urlNewProducts = [NSURL URLWithString:strNewProductsURL];
        DownloadUrlOperation *operation2 = [[DownloadUrlOperation alloc] initWithURL:urlNewProducts];
        operation2.tag = 501;
        [operation2 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        [operationQueue addOperation:operation2]; //
    }
    else{
        [self callDashBoardDataFromDB];
        self.btnSelectDays.enabled = YES;
    }
}
-(void)callDashBoardWebservices
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserName = [prefs objectForKey:@"userName"];
    NSString *strNoticeBoardURL = [NSString stringWithFormat:@"%@%@%@",WSURL,NOTICEMSGS_WS,[NSString stringWithFormat:@"last_syncDate=%@&username=%@",STANDARD_SYNC_DATE,strUserName]];

  NSString *strNewProductsURL = [NSString stringWithFormat:@"%@%@%@",WSURL,NEWPRODUCTSWS,[NSString stringWithFormat:@"days=%@",strDays]];


    
    
    // set maximum operations possible
    [operationQueue setMaxConcurrentOperationCount:3];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataAvailable:)
     name:@"DataDownloadFinished"
     object:nil ] ;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataUnAvailable:)
     name:@"DataDownloadFailed"
     object:nil];
    
    NSURL *urlNoticeBoard = [NSURL URLWithString:strNoticeBoardURL];
    DownloadUrlOperation *operation1 = [[DownloadUrlOperation alloc] initWithURL:urlNoticeBoard];
    operation1.tag = 23;
    [operation1 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [operationQueue addOperation:operation1]; // operation starts as soon as its added
    //[operation release];
    
    NSURL *urlNewProducts = [NSURL URLWithString:strNewProductsURL];
    DownloadUrlOperation *operation2 = [[DownloadUrlOperation alloc] initWithURL:urlNewProducts];
    operation2.tag = 501;
    [operation2 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [operationQueue addOperation:operation2]; // operation starts as soon as its added
    //[operation release];
    
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    if (deviceToken.length > 0) {
        
     /*   NSString *strPushNotification = [NSString stringWithFormat:@"%@%@%@",WSURL,PUSHNOTIFICATION_WS,[NSString stringWithFormat:@"device_token=%@&user_name=%@&warehouse=%@",deviceToken,strUserName,[prefs objectForKey:@"warehouse"]]];
        
        NSLog(@"%@",strPushNotification);
        NSURL *urlPushNotification = [NSURL URLWithString:strPushNotification];
        DownloadUrlOperation *operation3 = [[DownloadUrlOperation alloc] initWithURL:urlPushNotification];
        operation3.tag = 555;
        [operation3 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        [operationQueue addOperation:operation3]; // operation starts as soon as its added
        //[operation release];
      */
    }
   

}


-(void)actionDownloadSpecials:(id)sender{
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        UIButton *btn = (UIButton *)sender;
        [[AppDelegate getAppDelegateObj] startSpinAnimation:btn];
        
        NSString *strSpecialsURL = [NSString stringWithFormat:@"%@%@%@",WSURL,SPECIALS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
        NSURL *url = [NSURL URLWithString:strSpecialsURL];
        __block __weak ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setCompletionBlock:^{
            // Use when fetching binary data
            NSData *responseData = [request responseData];
            
            NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
            
            DLog(@"Dict response: %@",dictResponse);
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    dispatch_async(backgroundQueueForSpecials, ^(void){
                        
                        NSMutableArray *arrMembers = [[NSMutableArray alloc] init];
                        
                        if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                            
                            NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                            [arrMembers addObject:dict];
                        }
                        else{
                            arrMembers = [[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Data"];
                        }
                        
                        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                        
                        [databaseQueue inDatabase:^(FMDatabase *db) {
                            
                            @try
                            {
                                [db executeUpdate:@"DELETE FROM specials"];
                                
                                for (NSDictionary *dict in arrMembers){
                                    
                                    
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `specials` (`SCM_RECNUM`, `title`, `pdffile`, `pdfthumbnail`, `published`, `created_date`, `modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?)",
                                              [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                                              [[dict objectForKey:@"title"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdffile"] objectForKey:@"text"],
                                              [[dict objectForKey:@"pdfthumbnail"] objectForKey:@"text"],
                                              [[dict objectForKey:@"published"] objectForKey:@"text"],
                                              [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                              [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                   self.strSpecialsPdfThumbnailUrl = [[dict objectForKey:@"pdfthumbnail"] objectForKey:@"text"];
                                    
                                    if (![strSpecialsPdfThumbnailUrl isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strThumbnailUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strSpecialsPdfThumbnailUrl];
                                        
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strThumbnailUrl];
                                    }
                                    
                                    
                                   self.strSpecialsPdfUrl =[[dict objectForKey:@"pdffile"] objectForKey:@"text"];
                                    
                                    if (![strSpecialsPdfUrl isEqualToString:NO_ATTACHMENT_FOUND]) {
                                        NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strSpecialsPdfUrl];
                                        
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strPdfUrl];
                                    }
                                    
                                }
                                
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"specials" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                            }
                            @catch(NSException* e)
                            {
                                DLog(@"Error : specials: %@",[e description]);
                                //[db close];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                [[AppDelegate getAppDelegateObj] stopSpinAnimation:btn];
                                
                                NSString *strAttachmentServerUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strSpecialsPdfThumbnailUrl];
                                NSString *strAttachmentUrl = [[AppDelegate getAppDelegateObj] getFileFromCacheDir:strAttachmentServerUrl];
                                
                                UIImage *img= [UIImage imageWithContentsOfFile:strAttachmentUrl];
                                
                                if (!img) {
                                    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"Default-Landscape" ofType:@"png"];
                                    
                                    img = [UIImage imageWithContentsOfFile:strImage];
                                    [viewSpecials.btnSpecialsThumbnail removeTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                                    strImage = nil;
                                }
                                else{
                                    [viewSpecials.btnSpecialsThumbnail addTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                                }
                                
                                [viewSpecials.btnSpecialsThumbnail setImage:img forState:UIControlStateNormal];
                                
                            });
                            
                        }];
                        databaseQueue=nil;
                    });
               
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"specials"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    DLog(@"Error : specials: %@",strErrorMessage);
                    
                }
                
            }
            else{
                dispatch_async(backgroundQueueForSpecials, ^(void){
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    
                    [databaseQueue inDatabase:^(FMDatabase *db) {
                        [db executeUpdate:@"DELETE FROM specials"];
                    }];
                    
                    databaseQueue = nil;
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        [[AppDelegate getAppDelegateObj] stopSpinAnimation:btn];
                        
                        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"Default-Landscape" ofType:@"png"];
                        
                        UIImage *img = [UIImage imageWithContentsOfFile:strImage];
                        [viewSpecials.btnSpecialsThumbnail setImage:img forState:UIControlStateNormal];
                        [viewSpecials.btnSpecialsThumbnail removeTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
                    });
                });
                
            }
            
        }];
        [request setFailedBlock:^{
            NSError *error = [request error];
            DLog(@"%@",error);
        }];
        [request startAsynchronous];

    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:SERVER_OFFLINE_MESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)myNotificationResponse:(NSNotification*)note {
    NSNumber *count = [note object];
    [self performSelectorOnMainThread:@selector(updateView:) withObject:count waitUntilDone:YES];
}

#pragma mark Table view data source and delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == TBL_NOTICEBOARD_TAG) {
        return [arrNoticeBoards count];
    }
    else if (tableView.tag == TBL_NEWPRODUCTS_TAG){
        return [arrNewProducts count];
    }
    
    return 0;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (tableView.tag == TBL_NOTICEBOARD_TAG) {
        static NSString *CellIdentifier1 = CELL_IDENTIFIER1;
        
        CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.imgBackgroundView clear];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dashboard-table-bg" ofType:@"png"];
        cell.imgBackgroundView.image = [UIImage imageWithContentsOfFile:strImage];
        strImage = nil;
        
        [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
        
        cell.lblDate.text = [NSString stringWithFormat:@"%@",[[AppDelegate  getAppDelegateObj] setDateOnlyInAustrailianFormat:[[arrNoticeBoards objectAtIndex:[indexPath row]] objectForKey:@"Date"]]];
        
        cell.lblMessage.text = [NSString stringWithFormat:@"%@",[[arrNoticeBoards objectAtIndex:[indexPath row]] objectForKey:@"MessageContent"]];
        
        cell.btnMessage.tag = [indexPath row];
        [cell.btnMessage addTarget:self action:@selector(openNoticeMessage:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lblCreatedBy.text = [NSString stringWithFormat:@"%@",[[arrNoticeBoards objectAtIndex:[indexPath row]] objectForKey:@"CreatedBy"]];
        
        cell.btnAttachment.tag = [indexPath row];
        [cell.btnAttachment addTarget:self action:@selector(openPdfForAttachment:) forControlEvents:UIControlEventTouchUpInside];
      
        return cell;
    }
    
    if (tableView.tag == TBL_NEWPRODUCTS_TAG){
        static NSString *CellIdentifier2 = CELL_IDENTIFIER2;
        
        CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            cell = [nib objectAtIndex:1];
            
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.imgBackgroundView clear];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"dashboard-table-bg" ofType:@"png"];
        cell.imgBackgroundView.image = [UIImage imageWithContentsOfFile:strImage];
        strImage = nil;
        
        [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
        
        cell.lblStockCode.text = [NSString stringWithFormat:@"%@",[[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
        cell.lblProdDesc.text = [NSString stringWithFormat:@"%@",[[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
        cell.lblProductGroup.text = [NSString stringWithFormat:@"%@",[[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"ProductGroup"]];
        
        cell.lblPrice.text = [[AppDelegate getAppDelegateObj] ConvertStringIntoCurrencyFormatNew:[[[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"Price1"] floatValue]];

        cell.lblLocation.text = [NSString stringWithFormat:@"%@",[[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"Location"]];
        
        return cell;
        
    }
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == TBL_NEWPRODUCTS_TAG){
        
        NSString *strCode = [[arrNewProducts objectAtIndex:[indexPath row]] objectForKey:@"Code"];
        
        TopViewController *obj = [[TopViewController alloc] initWithNibName:@"TopViewController" bundle:nil];
        obj.depthViewReference = self.depthView;
        obj.presentedInView = [AppDelegate getAppDelegateObj].rootViewController.view;
        
        obj.strCode = [NSString stringWithFormat:@"%@",strCode];
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            FMResultSet *rs2;
            @try {
                
                rs2 = [db executeQuery:@"SELECT a.WAREHOUSE,a.DESCRIPTION,a.CODE,a.UNITS,a.UNITS_PER_PALLT,a.STATUS,a.UNITS_PER_LAYER,a.ON_HAND,a.AVAILABLE,a.ALLOCATED,a.PURCHASE_ORDER,a.PRICE1,a.PRICE2,a.PRICE3,a.PRICE4,a.PRICE5,a.LOCATION,a.STANDARD_COST,a.MARGIN01,b.DELIVERY_DATE FROM samaster a LEFT OUTER JOIN podetail b ON a.CODE = b.STOCK_CODE WHERE a.CODE = ? AND a.STATUS <> 'OB' ",strCode];
                
                if (!rs2)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs2 next]) {
                    obj.dictProductDetails = [rs2 resultDictionary];
                }
                
                [rs2 close];
            }
            @catch (NSException* e) {
                // rethrow if not one of the two exceptions above
                
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
            
        }];
        
        
        [self.depthView presentViewController:obj inView:[AppDelegate getAppDelegateObj].rootViewController.view];
        
    }
}
-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    StaticView *headerView;
    
    if (tableView.tag == TBL_NOTICEBOARD_TAG) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"StaticView" owner:self options:nil] objectAtIndex:TBL_NOTICEBOARD_TAG-1];
        
        
    }
    else if (tableView.tag == TBL_NEWPRODUCTS_TAG){
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"StaticView" owner:self options:nil] objectAtIndex:TBL_NEWPRODUCTS_TAG-1];
    }
    
    return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    StaticView *headerView;
    
    if (tableView.tag == TBL_NOTICEBOARD_TAG) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"StaticView" owner:self options:nil] objectAtIndex:TBL_NOTICEBOARD_TAG-1];
    }
    else if (tableView.tag == TBL_NEWPRODUCTS_TAG){
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"StaticView" owner:self options:nil] objectAtIndex:TBL_NEWPRODUCTS_TAG-1];
    }
    
    return headerView.frame.size.height;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return TBL_ROW_HEIGHT;
    // Alternatively, return rowHeight.
}


- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened {
    if (opened) {
        DLog(@"Now I'm open!");
    } else {
        DLog(@"Now I'm closed, pull me up again!");
    }
}


#pragma mark - KVO Observing/ WS Handler
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)operation change:(NSDictionary *)change context:(void *)context {
    NSString *source = nil;
    NSData *data = nil;
    NSError *error = nil;
    if ([operation isKindOfClass:[DownloadUrlOperation class]]) {
        DownloadUrlOperation *downloadOperation = (DownloadUrlOperation *)operation;
        //Notice Board
        if (downloadOperation.tag  == 23) {
            source = STR_NOTICEBOARD;
        }
        //New products
        else if (downloadOperation.tag  == 501) {
            source = STR_NEWPRODUCTS;
        }
        //Specials
        else if (downloadOperation.tag  == 14) {
            source = STR_SPECIALS;
        }
        //PushNotification
        else if (downloadOperation.tag  == 555) {
            source = STR_PUSHNOTIFICATION;
        }

        
        if (source) {
            data = [downloadOperation data];
            error = [downloadOperation error];
        }
    }
    
    if (source) {
        if (error != nil) {
            // handle error
            // Notify that we have got an error downloading this data;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataDownloadFailed"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", error, @"error", nil]];
        } else {
            // Notify that we have got this source data;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataDownloadFinished"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", data, @"data", nil]];
        }
    }
}


#pragma mark - Notification handler

- (void)dataAvailable:(NSNotification *)notification {
  NSString *source = [notification.userInfo valueForKey:@"source"];
  
  NSData *responseData = [notification.userInfo valueForKey:@"data"];
  
  NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
  
  if ([source isEqualToString:STR_NOTICEBOARD]) {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
      FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
      
      [databaseQueue inDatabase:^(FMDatabase *db) {
        [self callGetNoticeMessagesFromDB:db];
      }];
      
      databaseQueue = nil;
      
      dispatch_async(dispatch_get_main_queue(), ^(void) {
        [localSpinner1 stopAnimating];
        [vwNewMessagesContentView removeAllSubviews];
        
        if ([arrNoticeBoards count]) {
          [vwNewMessagesContentView addSubview:vwNewMessages];
          [tblNoticeBoard reloadData];
        }
        else{
          [vwNewMessagesContentView addSubview:vwNoMessages];
        }
        
      });
    });
    
    
  }
  else if ([source isEqualToString:STR_PUSHNOTIFICATION]) {
        NSLog(@"Push Notification registered for user: %@ and device token: %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"username"],[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]);
      
  }

  else if([source isEqualToString:STR_NEWPRODUCTS])
  {

    if ([[[dictResponse objectForKey:@"DocumentElement"] objectForKey:@"NEWITEMS"] isKindOfClass:[NSArray class]])
    {
      
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSArray *arrayResponse = [[dictResponse objectForKey:@"DocumentElement"] objectForKey:@"NEWITEMS"];
        
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        
        [databaseQueue inDatabase:^(FMDatabase *db) {
          @try
          {
            //Remove all data
            [db executeUpdate:@"DELETE FROM new_samaster"];
            
            for (NSDictionary *dict in arrayResponse){
              
                
              BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `new_samaster` (`SCM_RECNUM`, `RECNUM`, `WAREHOUSE`, `CODE`, `DESCRIPTION`, `LOCATION`, `PRICE1`, `PROD_GROUP`, `InsertDate`, `created_date`, `modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",[self strimValues:[[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"RECNUM"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"CODE"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"DESCRIPTION"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"LOCATION"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"PRICE1"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"ProductGroup"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"SPARE_DATE_1"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]]];
              
              if (!y)
              {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
              }
              
            }
            
            [self callGetNewProductsFromDB:db];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
              
              [localSpinner2 stopAnimating];
              [vwNewProductsContentView  removeAllSubviews];
              
              if ([arrNewProducts count])
              {
                [vwNewProductsContentView addSubview:vwNewProducts];
                [tblNewProducts reloadData];
              }
              else{
                [vwNewProductsContentView addSubview:vwNoProducts];
              }
              _vwActivityIndicator.hidden = YES;
              [_activityLoad stopAnimating];
              
            });

          }
          @catch(NSException* e)
          {
            _vwActivityIndicator.hidden = YES;
            [_activityLoad stopAnimating];
            DLog(@"Error : noticemsgs %@",[e description]);
          }
          
        }];
        databaseQueue = nil;
      });
      
    }
    
    else if ([[[dictResponse objectForKey:@"DocumentElement"] objectForKey:@"NEWITEMS"] isKindOfClass:[NSDictionary class]])
    {
      
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSDictionary *dict = [[dictResponse objectForKey:@"DocumentElement"] objectForKey:@"NEWITEMS"];
                
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        
        [databaseQueue inDatabase:^(FMDatabase *db) {
          @try
          {
            //Remove all data
            [db executeUpdate:@"DELETE FROM new_samaster"];
         
              BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `new_samaster` (`SCM_RECNUM`, `RECNUM`, `WAREHOUSE`, `CODE`, `DESCRIPTION`, `LOCATION`, `PRICE1`, `PROD_GROUP`, `InsertDate`, `created_date`, `modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",[self strimValues:[[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"RECNUM"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"CODE"] objectForKey:@"text"]], [self strimValues:[[dict objectForKey:@"DESCRIPTION"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"LOCATION"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"PRICE1"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"ProductGroup"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"SPARE_DATE_1"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"]],[self strimValues:[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]]];
              
              if (!y)
              {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
              }

            [self callGetNewProductsFromDB:db];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
              
              [localSpinner2 stopAnimating];
              [vwNewProductsContentView  removeAllSubviews];
              
              if ([arrNewProducts count])
              {
                [vwNewProductsContentView addSubview:vwNewProducts];
                [tblNewProducts reloadData];
              }
              else{
                [vwNewProductsContentView addSubview:vwNoProducts];
              }
              _vwActivityIndicator.hidden = YES;
              [_activityLoad stopAnimating];
              
            });
            
            
          }
          @catch(NSException* e)
          {
            _vwActivityIndicator.hidden = YES;
            [_activityLoad stopAnimating];
            DLog(@"Error : noticemsgs %@",[e description]);
          }
          
        }];
        
        databaseQueue = nil;
      });
      
    }
  }
  else if ([source isEqualToString:STR_SPECIALS]) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
      FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
      
      [databaseQueue inDatabase:^(FMDatabase *db) {
        [self callGetSpecialsFromDB:db];
      }];
      
      databaseQueue = nil;
      
      dispatch_async(dispatch_get_main_queue(), ^(void) {
        //[localSpinner3 stopAnimating];
        //[localSpinner3 removeFromSuperview];
        
        NSString *strAttachmentServerUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ATTACHMENT_URL,strSpecialsPdfThumbnailUrl];
        NSString *strAttachmentUrl = [[AppDelegate getAppDelegateObj] getFileFromCacheDir:strAttachmentServerUrl];
        
        UIImage *img= [UIImage imageWithContentsOfFile:strAttachmentUrl];
        
        if (!img) {
          NSString *strImage=[[NSBundle mainBundle] pathForResource:@"Default-Landscape" ofType:@"png"];
          
          img = [UIImage imageWithContentsOfFile:strImage];
          [viewSpecials.btnSpecialsThumbnail removeTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
          strImage = nil;
        }
        else{
          [viewSpecials.btnSpecialsThumbnail addTarget:self action:@selector(openPdfForSpecials) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [viewSpecials.btnSpecialsThumbnail setImage:img forState:UIControlStateNormal];
        
        
      });
    });
    
    
  }
  
  
  self.btnSelectDays.enabled = YES;
  _vwActivityIndicator.hidden = YES;
  [_activityLoad stopAnimating];
  
}
-(NSString *)strimValues:(NSString *)strValue
{
 
   return  [strValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
- (void)dataUnAvailable:(NSNotification *)notification {
    NSString *source = [notification.userInfo valueForKey:@"source"];
    if ([source isEqualToString:STR_NOTICEBOARD]||[source isEqualToString:STR_NEWPRODUCTS]||[source isEqualToString:STR_SPECIALS]) {
      
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:@"The request timed out! Please try again."
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
    }
}

-(void)refresh:(NSNotification*)note{
    [self callDashBoardDataFromDB];
}

#pragma mark - Attachment Handler
-(void)openPdfForSpecials{
  
    if (![strSpecialsPdfUrl isEqualToString:@""]) {
        //file exist
      
        NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@%@",PDF_ATTACHMENT_URL,strSpecialsPdfUrl];

       //NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@",strSpecialsPdfUrl];
      
        OpenPdfWebViewController *dataViewController = [[OpenPdfWebViewController alloc] initWithNibName:@"OpenPdfWebViewController" bundle:[NSBundle mainBundle]];
      
        dataViewController.webviewLink = strAttachmentUrl;
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:dataViewController];
        nvc.navigationBarHidden = YES;
        dataViewController.modalPresentationStyle = UIModalPresentationFullScreen;
      
        [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
    }
    else{
        //file does not exist
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attachment not available!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      
        [alert show];
    }
}

-(void)openPdfForAttachment:(id)sender{
  
    UIButton *btn = (UIButton *)sender;
    NSString *strFileName = [[arrNoticeBoards objectAtIndex:btn.tag] objectForKey:@"Attachment"];
  
    if (![strFileName isEqualToString:@""]) {
      
      NSString *strAttachmentUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strFileName];
      
        OpenPdfWebViewController *dataViewController = [[OpenPdfWebViewController alloc] initWithNibName:@"OpenPdfWebViewController" bundle:[NSBundle mainBundle]];
      
        dataViewController.webviewLink = strAttachmentUrl;
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:dataViewController];
        nvc.navigationBarHidden = YES;
        dataViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Attachment not exist" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    
}

-(void)openNoticeMessage:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    MJDetailViewController *detailViewController = [[MJDetailViewController alloc] initWithNibName:@"MJDetailViewController" bundle:nil];
    
    detailViewController.strMessage = [[arrNoticeBoards objectAtIndex:btn.tag] objectForKey:@"MessageContent"];
    
    [self presentPopupViewController:detailViewController animationType:3];
    
}

- (void)dismiss {
    [self.depthView dismissPresentedViewInView:[AppDelegate getAppDelegateObj].rootViewController.view];
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
