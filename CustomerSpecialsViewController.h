//
//  CustomerTransactionViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import <MessageUI/MessageUI.h>
#import "NDHTMLtoPDF.h"

#define TAG_50 50
#define TAG_100 100

@interface CustomerSpecialsViewController : UIViewController<ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate>{

    SEFilterControl *segControl;
    
    NSArray *arrHeaderLabels;

    NSString *strWebserviceType;
    
    NSString *strCustCode;
    
    NSMutableDictionary *dictCustomerDetails;
    NSMutableArray *arrCustomerTransactions;
    
    BOOL isFromDebtorHistory;
   
    BOOL isNetworkConnected;
    
    dispatch_queue_t backgroundQueueForViewCustomerTransaction;
    FMDatabaseQueue *dbQueueViewCustomerTransaction;
    MBProgressHUD *spinner;

}


@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;



@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSString *strCustCode;
@property (nonatomic,retain) NSMutableDictionary *dictCustomerDetails;
@property (nonatomic,retain) NSMutableArray *arrCustomerTransactions;
@property (nonatomic,assign) BOOL isFromDebtorHistory;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblHeader;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoTransactions;

- (IBAction)sendEmailAction:(id)sender;
- (IBAction)printAction:(id)sender;

- (void)callViewCustomerSpecialsFromDB:(FMDatabase *)db;

@end
