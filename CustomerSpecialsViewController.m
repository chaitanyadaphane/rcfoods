//
//  CustomerTransactionViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CustomerSpecialsViewController.h"
#import "CustomCellGeneral.h"
#import "CustomerTransactionHistoryViewController.h"
#import "CustomerTransactionDetailViewController.h"
#import "CustomerHistoryDetailViewController.h"

#define CUSTOMER_SPECIAL_WS @"custspecial/view_special.php?"

@interface CustomerSpecialsViewController (){
      NSString *strEmailBody;
    NSMutableDictionary *emailCustDetailDict;
    NSMutableArray *emailProductListArr;
    MFMailComposeViewController *picker;
}

@end

@implementation CustomerSpecialsViewController
@synthesize PDFCreator;

@synthesize arrHeaderLabels,strCustCode,dictCustomerDetails,arrCustomerTransactions,isFromDebtorHistory;
@synthesize tblHeader,tblDetails,vwContentSuperview,vwSegmentedControl,vwHeader,vwDetails,vwNoTransactions,lblHeader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
//        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
    dictCustomerDetails = [[NSMutableDictionary alloc] init];
    arrCustomerTransactions = [[NSMutableArray alloc] init];
    

    
    /*UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [tblDetails addGestureRecognizer:longPressRecognizer];*/
    
    if (!backgroundQueueForViewCustomerTransaction) {
        backgroundQueueForViewCustomerTransaction = dispatch_queue_create("com.nanan.myscmipad.bgqueueForViewCustomerTransaction", NULL);
    }
    
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"CustomerSpecials" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
        [self callWSViewCustomerSpecial];
    }
    else{
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callViewCustomerSpecialsFromDB:db];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                [tblHeader reloadData];
                
            });
        });
        
    }
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.vwContentSuperview = nil;
    self.vwSegmentedControl = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoTransactions = nil;
    self.lblHeader = nil;
    
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
            [tblHeader reloadData];
        }
            break;
            
        case 1:{
            
            if ([arrCustomerTransactions count]) {
                [vwContentSuperview addSubview:vwDetails];
                [tblDetails reloadData];
            }
            else{
                //No Transactions
                [vwContentSuperview addSubview:vwNoTransactions];
                
            }
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (tableView.tag) {
        case TAG_50:return 60;break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];break;
            
        case TAG_100:return [arrCustomerTransactions count];break;
            
        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {

    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
  @try {
    switch (tableView.tag) {
      case TAG_50:{
        
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        
        if (cell == nil)
        {
          NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
          cell = [nib objectAtIndex:4];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        
        
        if ([indexPath section] == 0) {
          switch ([indexPath row]) {
            case 0:
//              if ([dictCustomerDetails objectForKey:@"Customer"]) {
//                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Customer"]];
//              }
                  if ([[dictCustomerDetails objectForKey:@"Customer"] isKindOfClass:[NSString class]]) {
                      cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Customer"]];
                  }
                  else if([[dictCustomerDetails objectForKey:@"Customer"] objectForKey:@"text"])
                  {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Customer"] objectForKey:@"text"]];
                  }
              default:break;
          }
        }
        
        
        if ([indexPath section] == 1) {
          switch ([indexPath row]) {
            case 0:
              if ([dictCustomerDetails objectForKey:@"Name"]) {
                if([[dictCustomerDetails objectForKey:@"Name"] isKindOfClass:[NSString class]])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Name"] trimSpaces]];
                }
                else if([[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"] trimSpaces]];
                }
                
              }
              
              break;
            case 1:
              
              if ([[dictCustomerDetails objectForKey:@"Balance"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Balance"] isKindOfClass:[NSNumber class]]) {
                  
                  cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictCustomerDetails objectForKey:@"Balance"] floatValue]];
              }
              else if ([[dictCustomerDetails objectForKey:@"Balance"] objectForKey:@"text"]) {
                  
                  cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerDetails objectForKey:@"Balance"] objectForKey:@"text"] floatValue]];

              }
                  
              break;
            case 2:
              
              if([[dictCustomerDetails objectForKey:@"Phone"]isKindOfClass:[NSString class]])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Phone"] trimSpaces]];
                }
                else if([[dictCustomerDetails objectForKey:@"Phone"]objectForKey:@"text"])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Phone"]objectForKey:@"text"] trimSpaces]];
                }
                
              
              break;
              
            case 3:
              if ([dictCustomerDetails objectForKey:@"Email"]) {
                if([[dictCustomerDetails objectForKey:@"Email"] isKindOfClass:[NSString class]])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Email"] trimSpaces]];
                }
                else if([[dictCustomerDetails objectForKey:@"Email"] objectForKey:@"text"] )
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Email"] objectForKey:@"text"] trimSpaces]];
                }
              }
              break;
                  
            case 4:
              if ([dictCustomerDetails objectForKey:@"Contact"]) {
                if([[dictCustomerDetails objectForKey:@"Contact"] isKindOfClass:[NSString class]])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Contact"] trimSpaces]];
                }
                else if([[dictCustomerDetails objectForKey:@"Contact"] objectForKey:@"text"])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Contact"] objectForKey:@"text"] trimSpaces]];
                }
                
              }
              break;
              
            case 5:
              if ([dictCustomerDetails objectForKey:@"Fax"]) {
                if([[dictCustomerDetails objectForKey:@"Fax"] isKindOfClass:[NSString class]])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Fax"] trimSpaces]];
                }
                else if([[dictCustomerDetails objectForKey:@"Fax"] objectForKey:@"text"])
                {
                  cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Fax"] objectForKey:@"text"] trimSpaces]];
                }
              }
              break;
              
              
            case 6:
              if ([[dictCustomerDetails objectForKey:@"Rate"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"Rate"] isKindOfClass:[NSNumber class]]) {
                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[dictCustomerDetails objectForKey:@"Rate"]];
              }
                  else if ([[dictCustomerDetails objectForKey:@"Rate"]objectForKey:@"text"])
                  {
                      cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[dictCustomerDetails objectForKey:@"Rate"]objectForKey:@"text"]];
                  }
              break;
              
              
            case 7:
                  if ([[dictCustomerDetails objectForKey:@"CreditLimit"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"CreditLimit"] isKindOfClass:[NSNumber class]]) {
                      cell.lblValue.text = [NSString stringWithFormat:@"$%@",[dictCustomerDetails objectForKey:@"CreditLimit"]];
                  }
                  else if ([[dictCustomerDetails objectForKey:@"CreditLimit"]objectForKey:@"text"])
                  {
                      cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[dictCustomerDetails objectForKey:@"CreditLimit"]objectForKey:@"text"]];
                  }

              break;
              
              
            case 8:
              
                  if ([[dictCustomerDetails objectForKey:@"TradingTerms"] isKindOfClass:[NSString class]] || [[dictCustomerDetails objectForKey:@"TradingTerms"] isKindOfClass:[NSNumber class]]) {
                      cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"TradingTerms"]];
                  }
                  else if ([[dictCustomerDetails objectForKey:@"TradingTerms"]objectForKey:@"text"])
                  {
                      cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"TradingTerms"]objectForKey:@"text"]];
                  }
              break;
              
            default:break;
          }
        }
        
        
        return cell;
        
        
      }break;
        
      case TAG_100:{
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
        
        if (cell == nil)
        {
          NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
          
          cell = [nib objectAtIndex:5];
          cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
        
        cell.lblValue.text = @"";
        
        return cell;
        
      }break;
        
      default:return nil;break;
    }
  }
  @catch (NSException *exception) {
    NSLog(@"Exception desc::%@",exception.description);
  }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case TAG_100:{
            CustomerTransactionDetailViewController *dataViewController = [[CustomerTransactionDetailViewController alloc] initWithNibName:@"CustomerTransactionDetailViewController" bundle:[NSBundle mainBundle]];
          
            dataViewController.strProductName = [[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"];
          
            dataViewController.isFromCustomerSpecials =YES;
            dataViewController.dictTransactionDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            dataViewController.strDebtor = strCustCode;
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
          
        }break;
    }
  
}

#pragma mark - Database calls

-(void)callViewCustomerSpecialsFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_GET_CUSTOMER_SPECIAL";
    
    FMResultSet *rs1;
    
    @try {
        rs1 = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecnum, RECNUM as Recnum, CODE as Customer, NAME as Name, TOTAL_DUE as Balance, PHONE as Phone, CONTACT1 as Contact, FAX_NO as Fax, RATE as Rate, CREDIT_LIMIT as CreditLimit, EMAIL1 as Email, TRADING_TERMS as TradingTerms, PRICE_CODE as PriceCode, created_date, modified_date from armaster WHERE CODE= ? ",strCustCode];
        
        if (!rs1)
        {
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            self.dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
            emailCustDetailDict = (NSMutableDictionary *)[rs1 resultDictionary];
        }
        else{
            NSLog(@"No Details exist for this code");
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        FMResultSet *rs2 ;
        @try {
            
            [arrCustomerTransactions removeAllObjects];
//            
//            rs2 = [db executeQuery:@"SELECT a.`KEY_FIELD` , a.`STOCK_CODE` as StockCode , b.`CUSTOMER_CODE` , b.`STOCK_CODE`, a.`TYPE1` as Type, b.`DESCRIPTION` as Description,c.PRICE1,c.PRICE2,c.PRICE3,c.PRICE4,c.PRICE5,c.STANDARD_COST as Cost FROM sysdisct a LEFT OUTER JOIN smcuspro b ON a.STOCK_CODE = b.STOCK_CODE LEFT OUTER JOIN samaster c ON c.CODE = b.STOCK_CODE WHERE a.`KEY_FIELD` =  ? AND b.`CUSTOMER_CODE` =  ? ORDER BY b.`DESCRIPTION`",strCustCode,strCustCode];
            
            
            rs2 = [db executeQuery:@"SELECT a.`KEY_FIELD` , a.`STOCK_CODE` as StockCode , b.`CUSTOMER_CODE` , b.`STOCK_CODE`, a.`TYPE1` as Type,a.DISCOUNT1 as Discount, b.`DESCRIPTION` as Description,c.PRICE1,c.PRICE2,c.PRICE3,c.PRICE4,c.PRICE5,c.STANDARD_COST as Cost FROM sysdisct a LEFT OUTER JOIN smcuspro b ON a.STOCK_CODE = b.STOCK_CODE LEFT OUTER JOIN samaster c ON c.CODE = b.STOCK_CODE WHERE trim(a.`KEY_FIELD`) =  ? AND trim(b.`CUSTOMER_CODE`) =  ? ORDER BY b.`DESCRIPTION`",strCustCode,strCustCode];
            
            
//
            
            if (!rs2)
            {
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            while ([rs2 next]) {
                NSDictionary *dictData = [rs2 resultDictionary];
                [arrCustomerTransactions addObject:dictData];
            }
            
            
            [rs2 close];
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            
            [rs2 close];
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
    //}];
}

#pragma mark - WS Methods
-(void)callWSViewCustomerSpecial{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_CUSTOMER_SPECIAL";
    
    NSString *strWarehouse = [[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"];

    //strCustCode = @"100025";
    NSString *parameters = [NSString stringWithFormat:@"code=%@&warehouse=%@",strCustCode,strWarehouse];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_SPECIAL_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Long Press
-(void)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tblDetails];
    
    NSIndexPath *indexPath = [tblDetails indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else{
        
        if (isFromDebtorHistory) {
            CustomerHistoryDetailViewController *dataViewController = [[CustomerHistoryDetailViewController alloc] initWithNibName:@"CustomerHistoryDetailViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strTranCode = [[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"] objectForKey:@"text"];
            
            dataViewController.dictCustomerHistoryDetails  = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            dataViewController.isFromCustomerHistory = YES;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
        else{
            CustomerTransactionDetailViewController *dataViewController = [[CustomerTransactionDetailViewController alloc] initWithNibName:@"CustomerTransactionDetailViewController" bundle:[NSBundle mainBundle]];
            
            dataViewController.strTranCode = [[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"] objectForKey:@"text"];
            
            dataViewController.dictTransactionDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            dataViewController.isFromTransactionHistory = FALSE;
            
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        }
    }
    
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"dictResponse %@", dictResponse);
    
    if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_SPECIAL"]){
        
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"] isKindOfClass:[NSDictionary class]])//SpecialList
            {
                
                NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"];
        
                [arrTemp addObject:dict];
            }
            else{
                arrTemp = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"];
            }
            
            for (NSDictionary *dictTemp in arrTemp) {
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setValue:[[dictTemp objectForKey:@"Cost"] objectForKey:@"text"] forKey:@"Cost"];
                [dict setValue:[[dictTemp objectForKey:@"Description"] objectForKey:@"text"] forKey:@"Description"];
                [dict setValue:[[dictTemp objectForKey:@"Discount"] objectForKey:@"text"] forKey:@"Discount"];
                [dict setValue:[[dictTemp objectForKey:@"StockCode"] objectForKey:@"text"] forKey:@"StockCode"];
                [dict setValue:[[dictTemp objectForKey:@"Type"] objectForKey:@"text"] forKey:@"Type"];
                
                [arrCustomerTransactions addObject:dict];
            }
            
            self.dictCustomerDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Data"];
            
            dispatch_async(backgroundQueueForViewCustomerTransaction, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]) {
                            // @throw [NSException exceptionWithName:@"Response Issue" reason:@"Scm Recnum can't be Null" userInfo:nil];
                        }
                        
                        FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM armaster WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        
                        if (!rs) {
                            [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        
                        
                        NSString *strEmail = @"";
                        if ([[dictCustomerDetails objectForKey:@"Email"] objectForKey:@"text"]) {
                            strEmail = [[dictCustomerDetails objectForKey:@"Email"] objectForKey:@"text"];
                        }
                        
                        NSString *strFax = @"";
                        if ([[dictCustomerDetails objectForKey:@"Fax"] objectForKey:@"text"]) {
                            strFax = [[dictCustomerDetails objectForKey:@"Fax"] objectForKey:@"text"];
                        }
                      
                      NSString *strContact1 = @"";
                      if ([[dictCustomerDetails objectForKey:@"Contact"] objectForKey:@"text"]) {
                        strContact1 = [[dictCustomerDetails objectForKey:@"Contact"] objectForKey:@"text"];
                      }
         
                      BOOL y;
                        if ([rs next]) {
                            
//                            y = [db executeUpdate:@"UPDATE `armaster` SET `SCM_RECNUM` = ?, `RECNUM`= ?, `CODE`= ?,`NAME`= ?, `CONTACT1`= ?, `FAX_NO`= ?, `RATE`= ?, `CREDIT_LIMIT`= ?,`EMAIL1`= ?,`TRADING_TERMS`= ?, `PRICE_CODE`= ?,`created_date`= ?, `modified_date`= ? WHERE SCM_RECNUM = ?", [[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictCustomerDetails objectForKey:@"Contact"] objectForKey:@"text"], strFax,[[dictCustomerDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"PriceCode"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];

                              y = [db executeUpdate:@"UPDATE `armaster` SET `RECNUM`= ?, `CODE`= ?,`NAME`= ?, `CONTACT1`= ?, `FAX_NO`= ?, `RATE`= ?, `CREDIT_LIMIT`= ?,`EMAIL1`= ?,`TRADING_TERMS`= ?, `PRICE_CODE`= ? WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"], strContact1, strFax,[[dictCustomerDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"PriceCode"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                          
                        }
                        else{
                            y = [db executeUpdate:@"INSERT OR REPLACE INTO `armaster` ( `RECNUM`, `CODE`,`NAME`, `CONTACT1`, `FAX_NO`, `RATE`, `CREDIT_LIMIT`,`EMAIL1`,`TRADING_TERMS`, `PRICE_CODE`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"], strContact1, strFax,[[dictCustomerDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"PriceCode"] objectForKey:@"text"]];
                            
                        }
                        
                        [rs close];
                        
                        if (!y)
                        {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        *rollback = NO;
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });

                    }
                    
                    
                    @finally {
//                        [self callViewCustomerSpecialsFromDB:db];
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [spinnerObj removeFromSuperview];
                            [tblHeader reloadData];
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                    });
                    
                }];
                
            });
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Custrans"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
    
}


#pragma mark - Email/Print
- (IBAction)sendEmailAction:(id)sender {
    
      strEmailBody = [[AppDelegate getAppDelegateObj] SendEmailForSpecialProduct:dictCustomerDetails aryProducts:arrCustomerTransactions strHtmlName:@"SpecialOrder"];

//    SendEmailForSpecialProduct

    [self callMailComposer];

}

- (IBAction)printAction:(id)sender {

}

-(void)printPRDF:(NSString *)result
{
    NSData *fileData = [NSData dataWithContentsOfFile:result];
    
    UIPrintInteractionController *print = [UIPrintInteractionController sharedPrintController];
    
    print.delegate = self;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    //printInfo.jobName = [appDelegate.pdfFilePath lastPathComponent];
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    print.printInfo = printInfo;
    print.showsPageRange = YES;
    print.printingItem = fileData;
    UIViewPrintFormatter *viewFormatter = [self.view viewPrintFormatter];
    viewFormatter.startPage = 0;
    print.printFormatter = viewFormatter;
    
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
    };
    
    [print presentFromRect:self.view.bounds inView:self.view animated:YES completionHandler:completionHandler];
    
}


-(void)callMailComposer{
//    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//    if (mailClass != nil)
//    {
        [self displayComposerSheet];
//    }
    
    
    //    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    //    if (mailClass != nil)
    //    {
    //
    //        if ([mailClass canSendMail])
    //            [self displayComposerSheet];
    //        else
    //            [self launchMailAppOnDevice];
    //    }
    //
    //    else
    //    {
    //        [self launchMailAppOnDevice];
    //    }
}


-(void)launchMailAppOnDevice{
    
    NSString *recipients = @"mailto:?cc=&subject=";
    NSString *body = @"&body=";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}


-(void)displayComposerSheet{
    
    
    if (picker) {
        picker=nil;
    }
    
    //    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker = [[MFMailComposeViewController alloc] init];
    
    picker.mailComposeDelegate = self;
    [picker setSubject:@""];
    
    // Set up recipients
    [picker setBccRecipients:nil];
    
    //Attachment
    // NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"CustomerOrderForm.html"];
    // NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    //[picker addAttachmentData:fileData mimeType:@"text/html" fileName:@"CustomerOrderForm.html"];
    
    [picker setMessageBody:strEmailBody isHTML:YES];
    
    
    [self presentViewController:picker animated:YES completion:nil];
    
    //    if(picker) picker=nil;
    
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString* alertMessage;
    // message.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            alertMessage = @"Email composition cancelled";
            break;
        case MFMailComposeResultSaved:
            alertMessage = @"Your e-mail has been saved successfully";
            
            break;
        case MFMailComposeResultSent:
            alertMessage = @"Your email has been sent successfully";
            
            break;
        case MFMailComposeResultFailed:
            alertMessage = @"Failed to send email";
            
            break;
        default:
            alertMessage = @"Email Not Sent";
            
            break;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];

    [self dismissViewControllerAnimated:YES completion:nil];
}



-(IBAction)sendToPrinter:(id)sender
{
 strEmailBody = [[AppDelegate getAppDelegateObj] SendEmail:dictCustomerDetails aryProducts:arrCustomerTransactions strHtmlName:@"CustomerOrder"];
    
    
    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:strEmailBody pathForPDF:[@"~/Documents/CustomerOrder.pdf" stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
        
        //                           self.PDFCreator = [NDHTMLtoPDF createPDFWithURL:[NSURL URLWithString:@"http://edition.cnn.com/2013/09/19/opinion/rushkoff-apple-ios-baby-steps/index.html"] pathForPDF:[@"~/Documents/blocksDemo.pdf" stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
        //  NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
        
        [self printPRDF:htmlToPDF.PDFpath];
        
    } errorBlock:^(NDHTMLtoPDF *htmlToPDF) {
        NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
        NSLog(@"%@",result);
        
    }];
    
   
}


@end
