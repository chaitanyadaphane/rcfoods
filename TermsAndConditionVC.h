//
//  TermsAndConditionVC.h
//  Blayney
//
//  Created by Pooja on 10/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIWebView *webviewTC;
- (IBAction)AcceptAction:(id)sender;

@end
