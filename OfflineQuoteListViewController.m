//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "OfflineQuoteListViewController.h"
#import "CustomCellGeneral.h"
#import "ViewQuoteDetailsViewController.h"
#import "OfflineAddQuoteViewController.h"
#import "DownloadUrlOperation.h"
#import "UploadStatusViewController.h"

#define QUOTE_HISTORY_QUOTE_LIST_WS @"quote/quotehistory.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface OfflineQuoteListViewController ()
{
    BOOL _loadingInProgress;
    MBProgressHUD *spinner;
    NSString *strQuote_Number;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;

@property (nonatomic) BOOL viewIsIn;

@end

@implementation OfflineQuoteListViewController
@synthesize listContent,filteredListContent,arrQuotes,strCustCode,btnLoad,arrSelectedQuotes,strQuoteNum,dictHeaderDetails,arrProducts,btnDelete,deleteIndexPath,btnUpload,progessView,timerForUpload,lblStatusText,isUploadInProgress;

@synthesize vwTblQuotes,vwContentSuperview,vwNoQuotes,tblQuoteList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrQuotes = [[NSMutableArray alloc] init];
    strQuoteNum = [[NSString alloc] init];
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    arrProducts = [[NSMutableArray alloc] init];
    arrSelectedQuotes = [[NSMutableArray alloc] init];
    
    backgroundQueueForUploadQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForUploadQuote", NULL);
    operationQueueUpload = [ASINetworkQueue new];
    [progessView setProgress:0.0];
    
    //First page
    currPage = 1;
    
    totalCount = 0;
    
    //Long press for Quote Details
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [tblQuoteList addGestureRecognizer:longPressRecognizer];
    
    
    //Notification to reload table when products added to details from product list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadOfflinQuoteListAfterEdit" object:nil];
    
    self.isUploadInProgress = NO;
    
    dispatch_async(backgroundQueueForUploadQuote, ^(void) {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue inDatabase:^(FMDatabase *db) {
            
            [self callGetQuoteListFromDB:db];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if ([arrQuotes count]) {
                    btnDelete.hidden = NO;
                }
                [self doAfterDataFetched];
            });
            
            
        }];
    });
    
    
    //Add fade in fade out animation to attract users attention
    [self addFadeInFadeOutAnimations];
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.vwNoQuotes = nil;
    self.vwContentSuperview = nil;
    self.vwTblQuotes = nil;
    self.tblQuoteList = nil;
    self.lblStatusText = nil;
    self.btnDelete = nil;
    self.progessView = nil;
    self.btnUpload = nil;
    self.btnLoad = nil;
}

#pragma mark - Cancel WS calls
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self removeFadeInFadeOutAnimations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    [operationQueueUpload cancelAllOperations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return [self.filteredListContent count];
    }
	else
	{
        return [self.arrQuotes count];
    }
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:7];
    }
    
    //cell.accessoryType = UITableViewCellAccessoryNone;
    
    
    UIButton *button = nil;
    
    BOOL isRunning = FALSE;
    
    HJManagedImageV *cachedImgView = nil;
    if (cell.accessoryView!=nil) {
        button = (UIButton *)cell.accessoryView;
        if ([[[button subviews] objectAtIndex:0] isKindOfClass:[UIActivityIndicatorView class]]) {
            UIActivityIndicatorView *indicator = [[button subviews] objectAtIndex:0];
            if (!indicator.isAnimating) {
                [indicator startAnimating];
            }
            isRunning = TRUE;
        }
        else{
            cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
            isRunning = FALSE;
        }
        
    }
    else{
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        cachedImgView = [[HJManagedImageV alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
        isRunning = FALSE;
    }
    
    if (!isRunning) {
        [cachedImgView clear];
        [button removeAllSubviews];
        
        NSMutableDictionary *item = [arrQuotes objectAtIndex:[indexPath row]];
        cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"QuoteNo"]];
        cell.lblQuoteNo.text = @"";
        
        [item setObject:cell forKey:@"cell"];
        [item setObject:indexPath forKey:@"cellIndexPath"];
        
        NSLog(@"item : %@",item);
        
        //Error
        if ([[item objectForKey:@"isErrorWhileFinalise"] intValue] == 1) {
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"error" ofType:@"png"];
            cachedImgView.image=[UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
        }
        //Success
        else if ([[item objectForKey:@"isErrorWhileFinalise"] intValue] == 2) {
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
            cachedImgView.image=[UIImage imageWithContentsOfFile:strImage];
            strImage = nil;
        }
        else{
            NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
            NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
            
            BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
            cachedImgView.image = (checked) ? [UIImage imageWithContentsOfFile:strImage1] : [UIImage imageWithContentsOfFile:strImage2];
            
            strImage1=nil;
            strImage2=nil;
        }
        
        [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
        [button addSubview:cachedImgView];
        CGRect frame = CGRectMake(0.0, 0.0, 22, 22);
        button.frame = frame;	// match the button's size with the image size
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;
        
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.deleteIndexPath = indexPath;
    
    NSMutableDictionary *item = [arrQuotes objectAtIndex:[indexPath row]];
    
    NSString *strOrderNum;
    if(strQuote_Number.length > 0)
    {
        strOrderNum = strQuote_Number;
    }
    else
    {
        strOrderNum = [item objectForKey:@"QuoteNo"];
    }
    
    
    if ([item objectForKey:@"errorMessage"]) {
        
        NSString *strErrorMessage = [item objectForKey:@"errorMessage"];
        NSString *strUploadDate = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum ;
        dataViewController.strMessage = strErrorMessage;
        dataViewController.strUploadDate = strUploadDate;
        dataViewController.isOrder = NO;
        dataViewController.isError = YES;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        
    }
    else if ([item objectForKey:@"successMessage"]) {
        
        NSString *strSuccessMessage = [item objectForKey:@"successMessage"];
        NSString *strUploadDate = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum ;
        dataViewController.strMessage = strSuccessMessage;
        dataViewController.strUploadDate = strUploadDate;
        dataViewController.isOrder = NO;
        dataViewController.isError = NO;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
    else{
        if (!isUploadInProgress) {
            [self tableView: tblQuoteList accessoryButtonTappedForRowWithIndexPath: indexPath];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        self.deleteIndexPath = indexPath;
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            [self callDeleteQuoteFromDB:db RowToDelete:[indexPath row]];
        }];
        
    }
    
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    self.deleteIndexPath = indexPath;
    
    NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];;
    
	BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    if (checked) {
        [arrSelectedQuotes removeObject:item];
    }
    else{
        [arrSelectedQuotes addObject:item];
    }
    
    [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
    
    UITableViewCell *cell = [item objectForKey:@"cell"];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    HJManagedImageV *cachedImgView = (HJManagedImageV *)[[button subviews] objectAtIndex:0];
    [cachedImgView clear];
    [button removeAllSubviews];
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    if (checked) {
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage2];
    }
    else{
        cachedImgView.image = [UIImage imageWithContentsOfFile:strImage1];
    }
    
    strImage1 = nil;
    strImage2 = nil;
    
    [[AppDelegate getAppDelegateObj].obj_Manager manage:cachedImgView];
    [button addSubview:cachedImgView];
    
    if ([arrSelectedQuotes count]) {
        btnUpload.enabled = TRUE;
    }
    else{
        btnUpload.enabled = FALSE;
    }
    
    
}


#pragma mark - Custom methods
-(void)doAfterdelete{
    
    if ([arrQuotes count] == 0) {
        [tblQuoteList reloadData];
        
        [vwContentSuperview removeAllSubviews];
        
        btnDelete.hidden = YES;
        btnUpload.hidden = YES;
        lblStatusText.text = @"";
        
        [vwContentSuperview addSubview:vwNoQuotes];
        
    }
    else{
        btnUpload.enabled = YES;
    }
    
}

-(IBAction)actionUpload:(id)sender
{
  if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
    if ([arrSelectedQuotes count] == 0) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Select atleast one quote to send!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [alert show];
      
      return;
    }
    
    self.isUploadInProgress = YES;
    
    [self doBeforeUpload];
    
    [operationQueueUpload cancelAllOperations];
    [operationQueueUpload setUploadProgressDelegate:progessView];
    [operationQueueUpload setDelegate:self];
    [operationQueueUpload setQueueDidFinishSelector:@selector(queueComplete:)];
    
    for (int i=0; i<[arrSelectedQuotes count]; i++) {
      
      self.strQuoteNum = [[arrSelectedQuotes objectAtIndex:i] objectForKey:@"QuoteNo"];
      
      FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
      [databaseQueue   inDatabase:^(FMDatabase *db) {
        [self callViewQuoteDetailsHeaderFromDB:db];
        [self callViewQuoteDetailsDetailsFromDB:db];
      }];
      
      NSString *soapXMLStr = [self callCreateSoapString:0];
      
      /*
       NSString *soapMessage = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wws=\"%@\">"
       "<soapenv:Header/>"
       "<soapenv:Body>"
       "%@"
       "</soapenv:Body>"
       "</soapenv:Envelope>",WSURL_WSDL,soapXMLStr];
       
       NSLog(@"soapMessage %@ " , soapMessage);
       NSURL *url = [NSURL URLWithString:WSURL_WSDL];
       
       
       
       ASIHTTPRequest *request = [ASIFormDataRequest requestWithURL:url];
       [request setDelegate:self];
       
       [request addRequestHeader:@"Content-Type" value:@"text/xml; charset=utf-8"];
       [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%d",[soapMessage length]]];
       [request addRequestHeader:@"SOAPAction" value:@"urn:wwservice#insertDebtorDetails"];
       [request setPostLength:[soapMessage length]];
       
       [request setRequestMethod:@"POST"];
       [request setPostBody:[[soapMessage dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
       */
      
      // create request
    NSString *strRequest=[AppDelegate getServiceURL:@"webservices_rcf/insert_modules.php"];

      ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:strRequest]];
      [requestnew setDelegate:self];
      [requestnew setRequestMethod:@"POST"];
      [requestnew setPostValue:soapXMLStr forKey:@"xmlData"];
      [requestnew setPostValue:@"0" forKey:@"transactionType"];
    //  [requestnew startAsynchronous];

      
      /*
     
      ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:strRequest]];
      [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
      [request setTimeOutSeconds:200];
      [request setDelegate:self];
      NSString *parameters = [NSString stringWithFormat:@"transactionType=%@", @"0"];
      if (soapXMLStr) {
        parameters = [NSString stringWithFormat:@"%@&xmlData=%@", parameters, soapXMLStr];
      }
      [request setpo];
      [request setPostValue:@"0" forKey:@"transactionType"];
      [request setRequestMethod:@"POST"];
       */
      
      NSIndexPath *selectedIndexPath = [[arrSelectedQuotes objectAtIndex:i] objectForKey:@"cellIndexPath"];
      NSLog(@"selectedIndexPath.row %d",selectedIndexPath.row);
      requestnew.tag = selectedIndexPath.row;
      [operationQueueUpload addOperation:requestnew];
      
      CustomCellGeneral *cell = (CustomCellGeneral *)[tblQuoteList cellForRowAtIndexPath:selectedIndexPath];
      UIButton *button = (UIButton *)cell.accessoryView;
      [button removeAllSubviews];
      
      UIActivityIndicatorView *localSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
      [localSpinner setCenter:CGPointMake(button.frame.size.width / 2,button.frame.size.height /2)];
      [button addSubview:localSpinner];
      [button bringSubviewToFront:localSpinner];
      [localSpinner startAnimating];
      soapXMLStr = nil;
      requestnew = nil;
      
    }
    
    [operationQueueUpload go];
    
    self.timerForUpload = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(loadProgress) userInfo:nil repeats:YES];
    
  }
  else{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You are offline!\nUploading requires internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
  }
}

-(void)loadProgress{
    // NSLog(@"Max: %f", [self.progessView progress]);
}
- (void)queueComplete:(ASINetworkQueue *)queue
{
    NSLog(@"Queue complete");
    //NSLog(@"Max: %f, Value: %f", [self.progessView maxValue],[self.progessView doubleValue]);
  
    [timerForUpload invalidate];
    timerForUpload=nil;
  
    lblStatusText.text = UPLOAD_AFTER_STATUS_MESSAGE;
    [self performSelector:@selector(setProgressBarHidden) withObject:nil afterDelay:1.0];
  
}

- (void)setProgressBarHidden{
    self.isUploadInProgress = NO;
    progessView.hidden = YES;
    btnDelete.enabled = TRUE;
    btnUpload.enabled = FALSE;
    [arrSelectedQuotes removeAllObjects];
    lblStatusText.text = UPLOAD_BEFORE_STATUS_QUOTE_MESSAGE;
    [self addFadeInFadeOutAnimations];
}

- (void)doBeforeUpload{
    
    [self removeFadeInFadeOutAnimations];
    
    [progessView setProgress:0.0];
    progessView.hidden = FALSE;
    btnDelete.enabled = FALSE;
    btnUpload.enabled = FALSE;
    lblStatusText.text = UPLOAD_RUNNING_STATUS_MESSAGE;
}

-(void)showErrorMessage:(id)sender event:(id)event{
    
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
	NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];
        NSString *strOrderNum = [item objectForKey:@"QuoteNo"];
  		NSString *strErrorMessage = [item objectForKey:@"errorMessage"];
        NSString *strUploadDate = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum ;
        dataViewController.strMessage = strErrorMessage;
        dataViewController.strUploadDate = strUploadDate;
        dataViewController.isError = YES;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
	}
    
}

-(void)showSuccessMessage:(id)sender event:(id)event{
    
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
	NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:indexPath.row];
  		NSString *strSuccessMessage = [item objectForKey:@"successMessage"];
        NSString *strOrderNum = [item objectForKey:@"QuoteNo"];
        NSString *strUploadDate = [item objectForKey:@"UploadDate"];
        
        UploadStatusViewController *dataViewController = [[UploadStatusViewController alloc] initWithNibName:@"UploadStatusViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strStatusNumber = strOrderNum ;
        dataViewController.strMessage = strSuccessMessage;
        dataViewController.strUploadDate = strUploadDate;
        dataViewController.isError = NO;
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
	}
    
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:tblQuoteList];
	NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView: tblQuoteList accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

- (void)removeFadeInFadeOutAnimations{
    [lblStatusText.layer removeAllAnimations];
}

- (void)addFadeInFadeOutAnimations{
    [lblStatusText.layer addAnimation:[[AppDelegate getAppDelegateObj] fadeInFadeOutAnimation]
                               forKey:@"animateOpacity"];
}

- (void)doAfterDataFetched{
    if ([arrQuotes count]) {
        lblStatusText.text = UPLOAD_BEFORE_STATUS_QUOTE_MESSAGE;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwTblQuotes];
        
        btnUpload.hidden = false;
    }
    else{
        
        lblStatusText.text = @"";
        btnUpload.hidden = true;
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoQuotes];
    }
    
}

#pragma mark - Long Press
-(void)onLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (!isUploadInProgress) {
        CGPoint p = [gestureRecognizer locationInView:tblQuoteList];
        
        NSIndexPath *indexPath = [tblQuoteList indexPathForRowAtPoint:p];
        if (indexPath == nil)
            NSLog(@"long press on table view but not on a row");
        else{
            
            NSDictionary  *item = [arrQuotes objectAtIndex:indexPath.row];
            if (![[item objectForKey:@"isErrorWhileFinalise"] isEqual:[NSNull null]] && [[item objectForKey:@"isErrorWhileFinalise"] intValue] == 1) {
                
            }
            else if (![[item objectForKey:@"isErrorWhileFinalise"] isEqual:[NSNull null]] &&[[item objectForKey:@"isErrorWhileFinalise"] intValue] == 2){
                
            }
            else{
                NSString *strQuote= [NSString stringWithFormat:@"%@",[[arrQuotes objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"]];
                
                OfflineAddQuoteViewController *dataViewController = [[OfflineAddQuoteViewController alloc] initWithNibName:@"OfflineAddQuoteViewController" bundle:[NSBundle mainBundle]];
                dataViewController.strQuoteNumRecived = strQuote;
                dataViewController.EditableMode = YES;
                
                NSLog(@"dataViewController.EditableMode==%d",dataViewController.EditableMode);
                
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            }
            
        }
    }
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrQuotes count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 600.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    //[spinner showLoadingView:self.view size:2];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        [self callGetQuoteListFromDB:db];
        
    }];
    [self doAfterDataFetched];
}

- (void) didFinishLoadingMoreSampleData
{
	_loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
	if ([scope isEqualToString:@"QuoteNo"])
    {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetQuoteListByQuoteFromDB:db Quote:searchText];
            
        }];
        
        [tblQuoteList reloadData];
    }
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller{
	/*
     Bob: Because the searchResultsTableView will be released and allocated automatically, so each time we start to begin search, we set its delegate here.
     */
	[self.searchDisplayController.searchResultsTableView setDelegate:self];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
	/*
	 Hide the search bar
	 */
	//[tblQuoteList setContentOffset:CGPointMake(0, 44.f) animated:YES];
}


#pragma mark - IBActions

//No
- (IBAction)actionDismissTipView:(UIButton *)sender{
    //[popTipView dismissAnimated:YES];
}

- (void)actionShowQuoteDetails:(UIButton *)sender{
    NSString *strQuoteNum1 = [[[arrQuotes objectAtIndex:sender.tag] objectForKey:@"QuoteNo"] objectForKey:@"text"];
    
    ViewQuoteDetailsViewController *dataViewController = [[ViewQuoteDetailsViewController alloc] initWithNibName:@"ViewQuoteDetailsViewController" bundle:[NSBundle mainBundle]];
    
    dataViewController.strQuoteNum = strQuoteNum1;
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([tblQuoteList isEditing]) {
        
        btnUpload.enabled = TRUE;
        
        // [self showOrEnableButtons];
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblQuoteList setEditing:NO animated:YES];
    }
    else{
        
        btnUpload.enabled = FALSE;
        
        // [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblQuoteList setEditing:YES animated:YES];
    }
    
    [tblQuoteList reloadData];
}




#pragma mark - Handle Notification
- (void)reloadTableViewData:(NSNotification *)notification {
    [arrQuotes removeAllObjects];
    dispatch_async(backgroundQueueForUploadQuote, ^(void) {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM soquohea_offline"] ;
            
            
            if (!results)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                //return;
            }
            
            if ([results next]) {
                totalCount = [[[results resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [results close];
            
            [self callGetQuoteListFromDB:db];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self doAfterDataFetched];
            });
            
            
        }];
    });
    
}

#pragma mark - Database calls

-(void)DeleteUploadedQuote:(NSString *)strQuoteNumtodelete indexRow:(int )indexRow
{
  @try {
     dispatch_async(backgroundQueueForUploadQuote, ^(void){
       FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
       
       [databaseQueue inDatabase:^(FMDatabase *db) {
           if([db executeUpdate:@"DELETE FROM soquohea_offline WHERE trim(QUOTE_NO) = ?",[strQuoteNumtodelete trimSpaces]])
           {
             if(arrQuotes.count > indexRow)
             {
               [arrQuotes removeObjectAtIndex:indexRow];
               
             }
             
           }
         else
         {
          
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Local DB Error" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
           [alert show];
         }
         
   }];
    
       databaseQueue = nil;
       
       dispatch_async(dispatch_get_main_queue(), ^(void) {
         
                 [tblQuoteList reloadData];
       });
     });
  
    
  }
  @catch (NSException* e) {
    // rethrow if not one of the two exceptions above
    NSString *strErrorMessage = @"";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Local DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
  }
}
-(void)callGetQuoteListFromDB:(FMDatabase *)db
{
    @try {
        
        FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO as QuoteNo, DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun FROM soquohea_offline"];
        
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [arrQuotes addObject:dictData];
            
        }
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        });
        
        
    }
}

-(void)callGetQuoteListByQuoteFromDB:(FMDatabase *)db Quote:(NSString *)quote{
    
    FMResultSet *rs;
    
    @try {
        
        rs = [db executeQuery:@"SELECT QUOTE_NO as QuoteNo,DEBTOR as Debtor,DEL_NAME as DelName,CONTACT as Contact,DEL_DATE as DelDate,DELIVERY_RUN as DelRun FROM soquohea_offline WHERE QUOTE_NO LIKE ?",[NSString stringWithFormat:@"%%%@%%", quote]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
    
}

-(void)callViewQuoteDetailsHeaderFromDB:(FMDatabase *)db{
    FMResultSet *rs1;
    @try {
        
        rs1 = [db executeQuery:@"SELECT CARRIER as Carrier,DELIVERY_RUN as Delivery_Run,BRANCH as Branch, DEBTOR as Debtor,DEL_ADDRESS1 as Address1,DEL_ADDRESS2 as Address2,DEL_ADDRESS3 as Address3,DEL_COUNTRY as Country,DEL_NAME as Name,DEL_POST_CODE as PostCode,DEL_SUBURB as Suburb,WAREHOUSE as Warehouse,DEL_INST1 as DelInst1, DEL_INST2 as DelInst2,RELEASE_TYPE as ReleaseType,QUOTE_NO as QuoteNo,DATE_RAISED as DateRaised,QUOTE_VALUE as Total,STATUS as Status,isFinalised,DEL_DATE as DeliveryDate,CONTACT as Contact,DIRECT as Direct,SALESMAN as Salesman,SALES_BRANCH as SalesBranch,TRADING_TERMS as TradingTerms,PRICE_CODE as PriceCode,EXCHANGE_CODE as ExchangeCode,CHARGE_TYPE as ChargeType,EXPORT_DEBTOR as ExportDebtor,DROP_SEQ as DropSequence,HELD as Held,CHARGE_RATE as ChargeRate,NUM_BOXES as Numboxes,EXCHANGE_RATE as ExchangeRate,ACTIVE as Active,EDIT_STATUS as EditStatus,PERIOD_RAISED as PeriodRaised,YEAR_RAISED as YearRaised FROM soquohea_offline WHERE QUOTE_NO=?",strQuoteNum];
        
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next])
        {
            self.dictHeaderDetails = (NSMutableDictionary *) [rs1 resultDictionary];
            
        }
        
        [rs1 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_QUOTE_DETAILS_DETAILS";
    
    [arrProducts removeAllObjects];
    
    FMResultSet *rs2;
    
    @try {
        //Get Details details
        rs2 = [db executeQuery:@"SELECT DISSECTION_COS as Dissection_Cos,DISSECTION as Dissection,WAREHOUSE as Warehouse,PRICE_CODE as Price_Code,DECIMAL_PLACES as Decimal_Places,BRANCH as Branch,QUOTE_NO, LINE_NO as LineNo, ITEM as Item, DESCRIPTION as Description, STATUS as Status,PRICE as Price,DISC_PERCENT as DiscPercent,EXTENSION as ExtnPrice,COST as Cost,WEIGHT as Weight,VOLUME as Volume,TAX as Tax,QUANTITY as QuantityOrdered,ALT_QTY as AltQty,GROSS as Gross,CONV_FACTOR as ConvFactor,ORIG_COST as OrigCost,EDIT_STATUS FROM soquodet_offline WHERE QUOTE_NO = ?",strQuoteNum];
        
        
        if (!rs2)
        {
            [rs2 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        
        while ([rs2 next]) {
            NSDictionary *dictData = [rs2 resultDictionary];
            [arrProducts addObject:dictData];
            
        }
        
        //NSLog(@"arrProducts %@",arrProducts);
        
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
}


-(void)callDeleteQuoteFromDB:(FMDatabase*)db RowToDelete:(int)rowToDelete
{
    NSString *strQuote = [[arrQuotes objectAtIndex:rowToDelete] objectForKey:@"QuoteNo"];
    
    
    @try {
        
        [db executeUpdate:@"DELETE FROM `soquohea_offline` WHERE QUOTE_NO = ?",strQuote];
        [db executeUpdate:@"DELETE FROM `soquodet_offline` WHERE QUOTE_NO = ?",strQuote];
        
        [arrQuotes removeObjectAtIndex:rowToDelete];
        
        [tblQuoteList beginUpdates];
        [tblQuoteList deleteRowsAtIndexPaths:[NSArray arrayWithObject:deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tblQuoteList endUpdates];
        
        [self doAfterdelete];
        
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

#pragma mark - Server Calls

-(NSString *)callCreateSoapString:(int)finalizeTag
{
  strWebserviceType = @"WS_QUOTE_SAVE";
  
  NSString *strDebtor = @"";
  NSString *strDelName = @"";
  NSString *strAddress1 = @"";
  NSString *strAddress2 = @"";
  NSString *strAddress3 = @"";
  NSString *strSubUrb = @"";
  NSString *strDelPostCode = @"";
  NSString *strDelCountry = @"";
  NSString *strReleaseType = @"";
  NSString *strDelInst1 = @"";
  NSString *strDelInst2 = @"";
  NSString *strStatus = @"1";
  NSString *strWarehouse = @"";
  NSString *strBranch = @"";
  
//  NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
  
  if ([dictHeaderDetails objectForKey:@"Debtor"]) {
    strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"DelName"]) {
    strDelName= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelName"]];
    
  }
  
  if ([dictHeaderDetails objectForKey:@"Address1"]) {
    strAddress1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address1"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Address2"]) {
    strAddress2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address2"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Address3"]) {
    strAddress3 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address3"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Suburb"]) {
    strSubUrb = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Suburb"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
    strDelPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelPostCode"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
    strDelCountry = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelCountry"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
    strReleaseType = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ReleaseType"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
    strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst1"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
    strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst2"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Status"]) {
    strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
    strWarehouse = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Warehouse"]];
  }
  
  if ([dictHeaderDetails objectForKey:@"Branch"]) {
    strBranch = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Branch"]];
  }
  
  
  NSString *strContact = @"";
  if ([dictHeaderDetails objectForKey:@"Contact"]) {
    strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
  }
  
  NSString *strPriceCode = @"";
  if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
    strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
  }
  
  NSString *strSalesman = @"";
  if ([dictHeaderDetails objectForKey:@"Salesman"]) {
    strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
  }
  
  NSString *strSalesBranch = @"";
  if ([dictHeaderDetails objectForKey:@"SalesBranch"]) {
    strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SalesBranch"]];
  }
  
  NSString *strTradingTerms = @"";
  if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
    strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
  }
  
  NSString *strExchangeCode = @"";
  if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
    strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
  }
  
  NSString *strChargetype = @"";
  if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
    strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
  }
  
  NSString *strExportDebtor = @"";
  if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
    strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
  }
  
  NSString *strChargeRate = @"";
  if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
    strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
  }
  
  NSString *strHeld = @"";
  if ([dictHeaderDetails objectForKey:@"Held"]) {
    strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
  }
  
  NSString *strNumboxes = @"";
  if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
    strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
  }
  
  NSString *strDirect = @"";
  if ([dictHeaderDetails objectForKey:@"Direct"]) {
    strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
  }
  
  NSString *strDateRaised = @"";
  if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
    strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
  }
  
  NSString *strPeriodRaised = @"";
  if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
    strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
  }
  
  NSString *strYearRaised = @"";
  if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
    strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
  }
  
  NSString *strDropSequence = @"";
  if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
    strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
  }
  
  NSString *strDeliveryDate = @"";
  if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
    strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
  }
  
  
  NSString *strDelivery_Run = @"";
  if([dictHeaderDetails objectForKey:@"Delivery_Run"])
  {
    strDelivery_Run = [dictHeaderDetails objectForKey:@"Delivery_Run"];
  }
  
  NSString *strCarrier = @"";
  if([dictHeaderDetails objectForKey:@"Carrier"])
  {
    strCarrier = [dictHeaderDetails objectForKey:@"Carrier"];
  }
  NSString *strQuote_Value = @"";
  if([dictHeaderDetails objectForKey:@"Total"])
  {
    strQuote_Value = [dictHeaderDetails objectForKey:@"Total"];
  }
  
  NSString *strExpiry_Date = @"";
  if([dictHeaderDetails objectForKey:@"DateRaised"])
  {
    //Convert String to date
    NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] trimSpaces]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date1 = [dateFormat dateFromString:dateStr];
    
    //Convert Date to string
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
    NSString *strDate = [dateFormat2 stringFromDate:date1];
    
    //Convert String to Date
    NSString *dateStr2 = strDate;
    
    NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
    [dateFormat3 setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *date2 = [dateFormat3 dateFromString:dateStr2];
    // int daysToAdd = 30;
    // NSDate *newDate1 = [date2 dateByAddingTimeInterval:60*60*24*daysToAdd];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 30;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *dateToBeIncremented = [theCalendar dateByAddingComponents:dayComponent toDate:date2 options:0];
    
    //Convert Date to string
    NSDateFormatter *newdateFormat = [[NSDateFormatter alloc] init];
    [newdateFormat setDateFormat:@"dd-MM-yyyy"];
    strExpiry_Date = [newdateFormat stringFromDate:dateToBeIncremented];
    
    //NSLog(@"New Date - %@",strExpiry_Date);
  }
  
  
//  NSString *strActive = @"N";
//  NSString *strEditStatus = @"11";
  NSString *strExchangeRate = @"1.000000";
  
  
  NSString *strItem = @"";
  
  float totalVolume = 0;
  float totalWeight = 0;
  float totalTax = 0;
  
  NSString *strLineNumber = @"0";
  
  int i =0;
  for (NSDictionary *dict in arrProducts)
  {
    strLineNumber = [NSString stringWithFormat:@"%d",++i];
    
    NSString *strStockCode = nil;
    NSString *strPrice = @"0";
//    NSString *strCost = @"0";
    NSString *strDescription = nil;
    NSString *strExtension = @"0";
    NSString *strQuantityOrdered = @"0";
    NSString *strWeight = @"0";
    NSString *strVolume = @"0";
    NSString *strTax = @"0";
    NSString *strGross = @"0";
    
    NSString *strDissection = [dict objectForKey:@"Dissection"];
    NSString *strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
    NSString *strWarehouse = [dict objectForKey:@"Warehouse"];
//    NSString *strDecimal_Places = [dict objectForKey:@"Decimal_Places"];
//    NSString *strPrice_Codes = [dict objectForKey:@"Price_Code"];
    
    //Comments
    if ([[dict objectForKey:@"Item"] isEqualToString:@"/C"])
    {
      //DEL_DATE
      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//      NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
      
        NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
      [dateFormatter setLocale:enUSPOSIXLocale];
      [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
      NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
      [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
      strDeliveryDate = [dateFormatter stringFromDate:dateDel];
      
      strStockCode = @"/C";
      strDescription = [[dict objectForKey:@"Description"] trimSpaces];
        
        //==============Check fro HTML TAGS //==============
        strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
        strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
        //==============Check fro HTML TAGS //==============
        
      strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER/><SUB_SET_NUM/><QUOTE_NO/><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>0</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>0</PRICE><TAX>0</TAX> <DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS> <DELIVERY_DATE>%@</DELIVERY_DATE><LINE_TYPE>s</LINE_TYPE><COMPONENT_PRINT>s</COMPONENT_PRINT><COMPONENT_HISTORY>s</COMPONENT_HISTORY><SPARE_NUM_1>0.0000</SPARE_NUM_1><SPARE_NUM_2>0.0000</SPARE_NUM_2><SPARE_NUM_3>0.0000</SPARE_NUM_3><SPARE_NUM_4>0.0000</SPARE_NUM_4><SPARE_NUM_5>0.0000</SPARE_NUM_5><SPARE_NUM_6>0.0000</SPARE_NUM_6> <SPARE_DATE_1>2014-09-09T07:20:26+00:00</SPARE_DATE_1><SPARE_STR>N</SPARE_STR></DETAIL>",[strLineNumber trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],[strDescription trimSpaces],[strPriceCode trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strDeliveryDate trimSpaces]];
      
    }
    else{
      
      strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
      
      strStockCode = [dict objectForKey:@"Item"];
      strPrice = [dict objectForKey:@"Price"];
      
      strDescription = [dict objectForKey:@"Description"];
      strExtension = [dict objectForKey:@"ExtnPrice"];
      
      strWeight = [dict objectForKey:@"Weight"];
      strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
      
      totalWeight += [strWeight floatValue];
      
      strVolume = [dict objectForKey:@"Volume"];
      strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
      
      totalVolume += [strVolume floatValue];
      
      strTax = [dict objectForKey:@"Tax"];
      strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
      
      totalTax += [strTax floatValue];
      
      strGross = [dict objectForKey:@"Gross"];

      //DEL_DATE
      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//      NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
      [dateFormatter setLocale:enUSPOSIXLocale];
      [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
      NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
      [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
      strDeliveryDate = [dateFormatter stringFromDate:dateDel];
      
        //==============Check fro HTML TAGS //==============
        strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
        strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
        //==============Check fro HTML TAGS //==============

      strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER/><SUB_SET_NUM/><QUOTE_NO/><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@></DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX> <DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS> <DELIVERY_DATE>%@</DELIVERY_DATE><LINE_TYPE>s</LINE_TYPE><COMPONENT_PRINT>s</COMPONENT_PRINT><COMPONENT_HISTORY>s</COMPONENT_HISTORY><SPARE_NUM_1>0.0000</SPARE_NUM_1><SPARE_NUM_2>0.0000</SPARE_NUM_2><SPARE_NUM_3>0.0000</SPARE_NUM_3><SPARE_NUM_4>0.0000</SPARE_NUM_4><SPARE_NUM_5>0.0000</SPARE_NUM_5><SPARE_NUM_6>0.0000</SPARE_NUM_6> <SPARE_DATE_1>2014-09-09T07:20:26+00:00</SPARE_DATE_1><SPARE_STR>N</SPARE_STR></DETAIL>",[strLineNumber trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],[strDescription trimSpaces],strQuantityOrdered,[strPriceCode trimSpaces],strPrice ,[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strDeliveryDate trimSpaces]];
    }
    
  }
  
  
//  NSString *strDetailLines = [NSString stringWithFormat:@"%d",[arrProducts count]];
//  NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
//  NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
//  NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
  
  //Date Raised
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
  [dateFormatter setLocale:enUSPOSIXLocale];
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateRaised = [dateFormatter dateFromString:strDateRaised];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
  strDateRaised = [dateFormatter stringFromDate:dateRaised];
  
  //--Exp Date
  [dateFormatter setDateFormat:@"dd-MM-yyyy"];
  NSDate *dateExp = [dateFormatter dateFromString:strExpiry_Date];
  [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
  strExpiry_Date = [dateFormatter stringFromDate:dateExp];
  
    //==============Check fro HTML TAGS //==============
    strAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress1];
    strAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress2];
    strAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress3];
    strDelInst2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    strDelInst1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    strSubUrb = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strSubUrb];
    strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    
    strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
    
    //==============Check fro HTML TAGS //==============
    
    NSString *strDelSuburblimit = [[strSubUrb trimSpaces]copy];
    
    if(strDelSuburblimit.length > 15)
    {
        strDelSuburblimit = [strDelSuburblimit substringToIndex:14];
    }

    
  NSString *strquote = [NSString stringWithFormat:
                        @"<Quote>"
                        "<HEADER>"
                        "<OVERWRITE_SQ>s</OVERWRITE_SQ>"
                        "<INC_PRICE>s</INC_PRICE>"
                        "<INC_WAREHOUSE>s</INC_WAREHOUSE>"
                        "<DELIVERY_ADDRESS_TYPE>s</DELIVERY_ADDRESS_TYPE>"
                        "<DELIV_NUM>1000.00</DELIV_NUM>"
                        "<SUB_SET_NUM/>"
                        "<SUPPLIED_LINE_NUMBERS>s</SUPPLIED_LINE_NUMBERS>"
                        "<QUOTE_NO/>"
                        "<CALCULATETAX>s</CALCULATETAX>"
                        "<WEBORDERID>1000.00</WEBORDERID>"
                        "<APPLY_DELIVERY_CHARGE>s</APPLY_DELIVERY_CHARGE>"
                        "<CARRIER_CODE>zz</CARRIER_CODE>"
                        "<DEBTOR>%@</DEBTOR>"
                        "<BRANCH>%@</BRANCH>"
                        "<SALES_BRANCH>%@</SALES_BRANCH>"
                        "<DEL_NAME>%@</DEL_NAME>"
                        "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                        "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                        "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                        "<DEL_SUBURB>%@</DEL_SUBURB>"
                        "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                        "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                        "<DEL_INST1>%@</DEL_INST1>"
                        "<DEL_INST2>%@</DEL_INST2>"
                        "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                        "<CHARGE_RATE>%@</CHARGE_RATE>"
                        "<DATE_RAISED>%@</DATE_RAISED>"
                        "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                        "<DEL_DATE>%@</DEL_DATE>"
                        "<CUST_ORDER/>"
                        "<CONTACT>%@</CONTACT>"
                        "<SALESMAN>%@</SALESMAN>"
                        "<WAREHOUSE>%@</WAREHOUSE>"
                        "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                        "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                        "<DISC_PERCENT>0.00</DISC_PERCENT>"
                        "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                        "<JOB_NO></JOB_NO>"
                        "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                        "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                        "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                        "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                        "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                        "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                        "<SPARE_STR></SPARE_STR>"
                        "</HEADER>"
                        "%@"
                        "</Quote>",[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strAddress1 trimSpaces],[strAddress2 trimSpaces],[strAddress3 trimSpaces],[strDelSuburblimit trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],[strChargeRate trimSpaces],[strDateRaised trimSpaces],strExpiry_Date,strDeliveryDate,[strContact trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDelivery_Run trimSpaces],strItem];

  
  NSLog(@"soapXMLStr %@",strquote);
  
  return strquote;
}
-(void)callWSSaveQuote:(int)finalizeTag
{
    strWebserviceType = @"WS_QUOTE_SAVE";
  
    //self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
  
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = @"";
    NSString *strBranch = @"";
    NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
  
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
    }
  
    if ([dictHeaderDetails objectForKey:@"DelName"]) {
        strDelName= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelName"]];
      
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address3"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Suburb"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelPostCode"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelCountry"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ReleaseType"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Status"]) {
        strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Warehouse"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Branch"]) {
        strBranch = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Branch"]];
    }
    
    
    NSString *strStockCode = @"";
    NSString *strPrice = @"";
    NSString *strDescription = @"";
    NSString *strExtension = @"";
    NSString *strItem = @"";
    
    for (NSDictionary *dict in arrProducts) {
        
        
        strStockCode = [dict objectForKey:@"Item"];
        strPrice = [dict objectForKey:@"Price"];
        
        strDescription = [dict objectForKey:@"Description"];
        strExtension = [dict objectForKey:@"Price"];
        
        
        strItem = [strItem stringByAppendingFormat:@"<item><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION></item>",strStockCode,strDescription,strPrice,strPrice];
    }
    
    strStatus = @"1";
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:InsertDebtorDetails soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "<debtorHeader xsi:type=\"wws:DebtorRequest\">"
                            "<QUOTE_NO>%@</QUOTE_NO>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"
                            "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"
                            "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"
                            "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"
                            "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                            "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"
                            "<RELEASE_TYPE>%@</RELEASE_TYPE>"
                            "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"
                            "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"
                            "<STATUS>%@</STATUS>"
                            "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
                            "<BRANCH><![CDATA[%@]]></BRANCH>"
                            "<isFinalised>%@</isFinalised>"
                            "</debtorHeader>"
                            "<debtorDetails xsi:type=\"wws:DebtorDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                            "%@"
                            "</debtorDetails>"
                            "</wws:InsertDebtorDetails>",strQuoteNum,strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strDelPostCode,strDelCountry,strReleaseType,strDelInst1,strDelInst2,strStatus,strWarehouse,strBranch,strisFinalised,strItem];
    
    NSLog(@"soapXMLStr %@",soapXMLStr);
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#insertDebtorDetails" onView:self.view];
}

#pragma mark - ASIHTTP Request delegates


- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
  @try {
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
    
    NSLog(@"dictResponse : %@",dictResponse);
    //If successfully uploaded
    
    if([[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"])
    {
      
      if([[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"])
      {
        [spinner removeFromSuperview];
        UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Quotation Created: Quotation Number:%@",[[[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [aler show];
        aler = nil;
      }
      
      else
      {
        [spinner removeFromSuperview];
        UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Quotation could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [aler show];
        aler = nil;
      }
    }
    
    else
    {
      [spinner removeFromSuperview];
      UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Quotation could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [aler show];
      aler = nil;
    }
  }
  @catch (NSException *exception) {
    NSLog(@"Erro::%@",exception.description);
  }
  
  
}


- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    NSLog(@"Error %ld : %@",(long)requestParameter.tag,[requestParameter error]);
    NSError *error = [requestParameter error];
    if (error.code == 1 || error.code == 2) {
        progessView.hidden = YES;
        btnDelete.enabled = TRUE;
        //btnUpload.enabled = YES;
        NSMutableDictionary *item = [arrQuotes objectAtIndex:requestParameter.tag];
      
        UITableViewCell *cell = [item objectForKey:@"cell"];
        UIButton *button = (UIButton *)cell.accessoryView;
      
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
        UIImage *newImage=[[UIImage alloc]initWithContentsOfFile:strImage];
        [button setBackgroundImage:newImage forState:UIControlStateNormal];
      
    }
    requestParameter = nil;
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
  
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        
    }
  
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
