//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "AddQuoteViewController.h"


@interface QuoteHistoryQuoteListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ASIHTTPRequest_delegate>{
    
    NSString *strWebserviceType;
    int currPage;
    int totalCount;
    int recordNumber;
    
    NSMutableArray *arrQuotes; // The master content.
    NSMutableArray *arrQuotesrForWebservices;
    

    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.

    NSString *strCustCode;
    
    dispatch_queue_t backgroundQueueForQuoteList;
    FMDatabaseQueue *dbQueueQuoteList;
    
    BOOL isQuoteEntryModule;    
    BOOL isNetworkConnected;
    NSOperationQueue *operationQueue;
  
  AddQuoteViewController *obj;
  
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrQuotes;
@property (nonatomic, retain) NSMutableArray *arrQuotesrForWebservices;
@property (nonatomic, retain) NSString *strCustCode;
@property (nonatomic, retain) IBOutlet UIButton *btnLoad;
@property (nonatomic,retain) NSMutableArray *arrQuoteDetails;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblQuoteList;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwNoQuotes;
@property (nonatomic,retain) IBOutlet UIView *vwTblQuotes;
@property (nonatomic,retain) IBOutlet UIView *vwCustomLoadProfileTip;

@property (nonatomic,unsafe_unretained) IBOutlet UISearchBar *searchingBar;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;

@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

#pragma mark - Custom Methods
- (void)callWSGetQuoteList:(int)pageIndex;
-(void)callGetQuoteListFromDB:(FMDatabase *)db;
- (IBAction)actionLoadProfile:(UIButton *)sender;

-(void)callGetQuoteListByQuoteFromDB:(FMDatabase *)db Quote:(NSString *)quote;


@end
