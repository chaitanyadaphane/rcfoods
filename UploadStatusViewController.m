//
//  UploadStatusViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 06/08/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "UploadStatusViewController.h"
#import "CustomCellGeneral.h"

@interface UploadStatusViewController ()

@end

@implementation UploadStatusViewController
@synthesize strMessage,strStatusNumber,arrHeaderLabels,strUploadDate,isOrder,isFinalisedOrder,strOldStatusNumber,lblStatus,isError;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *path;
    
    if (isFinalisedOrder) {
        path = [AppDelegate getFileFromLocalDirectory:@"UploadStatusForFinalised" Type:@"plist"];
    }
    else{
        if (isOrder) {
            path = [AppDelegate getFileFromLocalDirectory:@"UploadStatus" Type:@"plist"];
        }
        else{
            path = [AppDelegate getFileFromLocalDirectory:@"UploadStatusForQuote" Type:@"plist"];
        }
    }
    
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    if (isError) {
        lblStatus.textColor = [UIColor redColor];
    }
    lblStatus.text = strMessage;
}

-(void)viewDidUnload{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrHeaderLabels count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier = CELL_IDENTIFIER5;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:4];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text = [[arrHeaderLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    if (isFinalisedOrder) {
        switch ([indexPath row]) {
            case 0:
            {
                if (![strOldStatusNumber isEqual:[NSNull null]]) {
                    cell.lblValue.text = strOldStatusNumber;
                }
                
            }
                break;
                
            case 1:
            {
                cell.lblValue.text = strStatusNumber;
                
            }
                break;
                        
            case 2:
            {
                cell.lblValue.text = @"-";
                if (![strUploadDate isEqual:[NSNull null]]) {
                    cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateInAustrailianFormat:strUploadDate];
                }
            }
                break;
                
            default:
                break;
        }
        
    }
    else{
        switch ([indexPath row]) {
            case 0:
            {
                cell.lblValue.text = strStatusNumber;
                
            }
                break;
                
            case 1:
            {
                if (![strUploadDate isEqual:[NSNull null]]) {
                    cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateInAustrailianFormat:strUploadDate];
                }
                
            }
                break;
                
            default:
                break;
        }
        
    }
    
    return cell;
    
}

@end
