//
//  CallEntryViewController.m
//  Blayney
//
//  Created by POOJA MISHRA on 30/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import "CallEntryViewController.h"
#import "CustomCellGeneral.h"
#import "KGModal.h"

#define TYPE_TAG 101
#define STATUS_TAG 102
#define RESULTCODE_TAG 103
#define REASONCODE_TAG 104

@interface CallEntryViewController ()
{
    NSMutableArray * callentrydataArr;
    NSMutableDictionary * detailDataDict;
    NIDropDown *dropdownUserList;
       NIDropDown *dropdownType;
        NIDropDown *dropdownStatus;
    
    NSArray *reasonCodeNameArr;
    
    NSArray *reasonCodeArr;
    
    UIButton *typeButton;
    UIButton *statusButton;
    UIButton *resultCodeButton;
    UIButton *reasonCodeButton;
    
    NSString *strType;
    NSString *strStatus;
    NSString *strResultCode;
    NSString *strReasonCode;
    
    MBProgressHUD *spinner;
}
@end

@implementation CallEntryViewController


#pragma mark - view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    callentrydataArr = [NSMutableArray arrayWithObjects:@"Debtor",@"Requested Time",@"Actual Caller",@"Type(P/R)",@"Status(O/R/C)",@"Result Code (U/S)",@"Reason Code", nil];
    
    reasonCodeNameArr = [[NSArray alloc]initWithObjects:@"Aleady Rang Order in",@"Problem with account",@"Another supplier",@"Closed for holiday",@"Customer will call back today",@"Customer Complaint",@"Account on credit hold",@"Business is closed",@"Delivery",@"Customer Enquiry",@"Phone cont engaged",@"Customer will fax their order",@"Ordered fresh meat",@"Food service order only",@"We will call laterin week",@"Left message on machine",@"Call once month only",@"No answer",@"New owner-wants price list",@"Order not required today",@"Phone disconnected",@"Call re-promotion",@"Rep got order",@"Would like to see rep",@"Coffee machine service",@"Ordered fresh sea food", @"Streets I/cream order only",@"Called earlier short week",@"Order placed with telesales",@"Fortnightly call wk1 ",@"Fortnightly call wk2",@"Brought up wrong A/C",@"Customer will call later in week",@"Problem call referred sup/rep",nil];
    
    reasonCodeArr = [[NSArray alloc]initWithObjects:@"A",@"AC",@"AS",@"C",@"CB",@"CC",@"CH",@"CL",@"D",@"E",@"EN",@"F",@"FM",@"FS",@"LW",@"M",@"MC",@"NA",@"NO",@"NR",@"PD",@"PR",@"R",@"RP",@"S",@"SF",@"ST",@"SW",@"TS",@"W1",@"W2",@"WA",@"WR",@"XX", nil];
    
    [self callUserCallEntryDatabase];
    if(detailDataDict.count > 0)
    {
        strType = [detailDataDict objectForKey:@"Type"];
        strStatus = [detailDataDict objectForKey:@"Status"];
        strResultCode = @"U";
    }
    [_tblVw reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextField

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.tag == 3)
    {
        strType = textField.text;
 
    }
    else if(textField.tag == 4)
    {
        strStatus = textField.text;
    }
    else if (textField.tag == 5)
    {
        strResultCode = textField.text;

    }
    else if (textField.tag == 6)
    {
        strReasonCode = textField.text;
    }
    
    NSLog(@"Type: %@",strType);
    NSLog(@"Status: %@",strStatus);
    NSLog(@"Result code: %@",strResultCode);
    return YES;
}

#pragma mark - Tableview delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return 55.0;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return  callentrydataArr.count;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier9 = CELL_IDENTIFIER9;
    CustomCellGeneral *cell = nil;
    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier9];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:9];
        cell.txtGlowingValue.tag = indexPath.row;
        cell.txtGlowingValue.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lblTitle.text = [callentrydataArr objectAtIndex:indexPath.row];
    if(indexPath.row == 0)
    {
        cell.txtGlowingValue.text = [detailDataDict objectForKey:@"DebtorCode"];
        cell.userInteractionEnabled = NO;
    }
    else if (indexPath.row == 1)
    {
        cell.txtGlowingValue.text = [detailDataDict objectForKey:@"RequestTime"];
        cell.userInteractionEnabled = NO;
    }
    else if (indexPath.row == 2)
    {
        cell.txtGlowingValue.text = [detailDataDict objectForKey:@"SalesPerson"];
        cell.userInteractionEnabled = NO;
    }
    else if (indexPath.row == 3)
    {
        if(strType.length == 0)
        {
            cell.txtGlowingValue.text = [detailDataDict objectForKey:@"Type"];
        }
        else
        {
            cell.txtGlowingValue.text = strType;
        }
       
    }
    else if (indexPath.row == 4)
    {
        cell.txtGlowingValue.backgroundColor = [UIColor clearColor];
        if(strStatus.length == 0)
        {
            cell.txtGlowingValue.text = [detailDataDict objectForKey:@"Status"];
        }
        else
        {
            cell.txtGlowingValue.text = strStatus;
        }
    }
    else if (indexPath.row == 5)
    {
        if(strResultCode.length == 0)
        {
            cell.txtGlowingValue.placeholder = SELECT_VALUE;
        }
        else
        {
            cell.txtGlowingValue.text = strResultCode;
        }
    }
    else if (indexPath.row == 6)
    {
        cell.txtGlowingValue.enabled = NO;
        cell.txtGlowingValue.placeholder = SELECT_VALUE;
        if(strReasonCode.length != 0)
        {
            cell.txtGlowingValue.text = strReasonCode;
        }
    }
       return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomCellGeneral *cell = (CustomCellGeneral *)[_tblVw cellForRowAtIndexPath:indexPath];

    PopUpViewController *popupObjc = [[PopUpViewController alloc] initWithNibName:@"PopUpViewController" bundle:[NSBundle mainBundle]];
    
    popupObjc.preferredContentSize =
    CGSizeMake(300, 400);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popupObjc];
    
    CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
    
     if (indexPath.row == 6)
    {
        popupObjc.arrPopupData = [self formReasonCode];
    }
    
    popupObjc.delegate = self;
    [self.popoverController presentPopoverFromRect:rect
                                            inView:cell
                          permittedArrowDirections:UIPopoverArrowDirectionLeft
                                          animated:YES];
}


-(void)dismissPopover{
    [self.popoverController dismissPopoverAnimated:YES];
}
#pragma mark -


-(void)callUserCallEntryDatabase
{
    
    NSLog(@"debtor %@",[_debtorStr trimSpaces]);
    NSLog(@"sales %@",[_salespersonStr trimSpaces]);
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        FMResultSet *rs = [db executeQuery:@"SELECT * FROM smdaytim as s WHERE s.CODE = ? AND s.SALESPERSON = ?",[_debtorStr trimSpaces],[_salespersonStr trimSpaces]];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        if([rs next])
        {
            NSDictionary *dict = [rs resultDictionary];
            detailDataDict = [NSMutableDictionary new];
            [detailDataDict setObject:[dict objectForKey:@"RECTYPE"] forKey:@"Status"];
            [detailDataDict setObject:[dict objectForKey:@"CALLERTYPE"] forKey:@"Type"];
            [detailDataDict setObject:[dict objectForKey:@"TIME"] forKey:@"RequestTime"];
            [detailDataDict setObject:[dict objectForKey:@"SALESPERSON"] forKey:@"SalesPerson"];
            [detailDataDict setObject:[dict objectForKey:@"EDIT_STATUS"] forKey:@"EditStatus"];
            [detailDataDict setObject:[dict objectForKey:@"CODE"] forKey:@"DebtorCode"];
            [rs close];

        }
    }];
}

-(NSString *)createxml_createCall
{
    NSString *salesPerson = [detailDataDict objectForKey:@"SalesPerson"];
    NSString *debtorCode = [detailDataDict objectForKey:@"DebtorCode"];
    NSString *requestedTime = [detailDataDict objectForKey:@"RequestTime"];
    NSString *ActualTime = [self getCurrectTime];

    NSString *user = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:todayDate];
    
    
    if([strType containsString:@"null"])
    {
        strType = @"";
    }
    
    if([strStatus containsString:@"null"])
    {
        strStatus = @"";
    }
    
    if([strReasonCode containsString:@"null"])
    {
        strReasonCode = @"";
    }
    
    NSString *soapXml = [NSString stringWithFormat:@"<Calls>"
                         "<Calls>"
                         "<Call_Type>CALLADD</Call_Type>"
                         "<User>%@</User>"
                         "<Call_ID />"
                         "<Date>%@</Date>"
                         "<Run>%@</Run>"
                         "<Customer>%@</Customer>"
                         "<Requested_Time>%@</Requested_Time>"
                         "<Status>%@</Status>"
                         "<Result_Code>%@</Result_Code>"
                         "<Actual_Time>%@</Actual_Time>"
                         "<Actual_Caller>%@</Actual_Caller>"
                         "<Reason_Code>%@</Reason_Code>"
                         "<Reason />"
                         "<Type>%@</Type>"
                         "<Reference />"
                         "<No_of_Lines>0</No_of_Lines>"
                         "<Cost>0</Cost>"
                         "<Value>0</Value>"
                         "<Comment />"
                         "</Calls>"
                         "</Calls>",user,dateStr,self.deliveryrun,debtorCode,requestedTime,strStatus,strResultCode,ActualTime,salesPerson,strReasonCode,strType
                         ];
    return soapXml;
}

-(void)setCustomPopupValue:(NSString *)strValue
{
    strReasonCode = strValue;
    [self dismissPopover];
    [_tblVw reloadData];
}

-(NSString *)getCurrectTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HHmm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [dateFormatter stringFromDate:[NSDate date]];
}

#pragma mark - 

-(NSMutableArray *)formTypeArr
{
    NSMutableArray *arrdata = [NSMutableArray new];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:@"P" forKey:@"Code"];
    [dict setObject:@"" forKey:@"Description"];
    [arrdata addObject:dict];
    
    NSMutableDictionary *dict1 = [NSMutableDictionary new];
    [dict1 setObject:@"R" forKey:@"Code"];
    [dict1 setObject:@"" forKey:@"Description"];
    [arrdata addObject:dict1];
    
    return arrdata;
}


-(NSMutableArray *)formStatusArr
{
    NSMutableArray *arrdata = [NSMutableArray new];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:@"O" forKey:@"Code"];
    [dict setObject:@"Open" forKey:@"Description"];
    [arrdata addObject:dict];
    
    NSMutableDictionary *dict1 = [NSMutableDictionary new];
    [dict1 setObject:@"R" forKey:@"Code"];
    [dict1 setObject:@"Ring Back" forKey:@"Description"];
    [arrdata addObject:dict1];
    
    NSMutableDictionary *dict2 = [NSMutableDictionary new];
    [dict2 setObject:@"C" forKey:@"Code"];
    [dict2 setObject:@"COmpleted" forKey:@"Description"];
    [arrdata addObject:dict2];
    
    return arrdata;
}


-(NSMutableArray *)formResultCode
{
    NSMutableArray *arrdata = [NSMutableArray new];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:@"U" forKey:@"Code"];
    [dict setObject:@"Unsuccessful" forKey:@"Description"];
    [arrdata addObject:dict];
    
    NSMutableDictionary *dict1 = [NSMutableDictionary new];
    [dict1 setObject:@"S" forKey:@"Code"];
    [dict1 setObject:@"Successful" forKey:@"Description"];
    [arrdata addObject:dict1];
    
    return arrdata;
}


-(NSMutableArray *)formReasonCode
{
    NSMutableArray *arrdata = [NSMutableArray new];
    for (int i = 0; i < reasonCodeArr.count; i++) {
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:[reasonCodeArr objectAtIndex:i] forKey:@"Code"];
        [dict setObject:[reasonCodeNameArr objectAtIndex:i] forKey:@"Description"];
        [arrdata addObject:dict];
    }
    return arrdata;
}


#pragma mark - Save Action
- (IBAction)saveAction:(id)sender
{
    if(([strResultCode isEqualToString:@"S"] || [strResultCode isEqualToString:@"U"]) && (strReasonCode.length > 0))
    {
        NSString *soapRequest = [self createxml_createCall];
        
        //---------------------------------------------------------------------------
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/customer_profile/manage_call.php"];
        NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
        [requestnew setDelegate:self];
        [requestnew setRequestMethod:@"POST"];
        [requestnew setPostValue:soapRequest forKey:@"xmlstring"];
        [requestnew setPostValue:@"createCall" forKey:@"action"];
        NSLog(@"soapXMLStr %@",soapRequest);
        [requestnew startAsynchronous];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please Select Values (Result or reason code)" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)proceedwithorderAction:(id)sender {
    
    if([strResultCode isEqualToString:@"S"] || [strResultCode isEqualToString:@""])
    {
//        NSString *soapRequest = [self createxml_createCall];
//        
//        //---------------------------------------------------------------------------
//        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        NSString *strRequest = [AppDelegate getServiceURL:@"webservices/customer_profile/manage_call.php"];
//        NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
//        ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
//        [requestnew setDelegate:self];
//        [requestnew setRequestMethod:@"POST"];
//        [requestnew setPostValue:soapRequest forKey:@"xmlstring"];
//        [requestnew setPostValue:@"createCall" forKey:@"action"];
//        NSLog(@"soapXMLStr %@",soapRequest);
//        [requestnew startAsynchronous];
//        //---------------------------------------------------------------------------
        
        [[KGModal sharedInstance]hideAnimated:YES];

    }
    else if([strResultCode isEqualToString:@"U"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Result - Unsuccessful" message:@"You cannot proceed with order." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"You cannot proceed with order." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    

}


- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    @try {
        [spinner removeFromSuperview];
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
        NSLog(@"dictResponse:::%@",dictResponse);
        NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
        NSLog(@"text:::%@",text);
        
    }
    @catch (NSException *exception) {
        [spinner removeFromSuperview];
        NSLog(@"error::%@",exception.description);
    }
    [self.delegate callEntryPerformsAction:strResultCode];
    [[KGModal sharedInstance]hideAnimated:YES];
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    [[KGModal sharedInstance]hideAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Saved special price Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    [spinner removeFromSuperview];
    NSLog(@"Failed::::: %@",requestParameter);
}

@end
