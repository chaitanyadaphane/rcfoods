//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "SalesOrderEntryViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "QuoteProductListViewController.h"
#import "KGModal.h"

#import "MJDebtorViewController.h"

#define STOCK_PRODUCT_INFO_WS @"stockinfo/view_stockinfoform.php?"
#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10

#define TAG_50 50
#define TAG_100 100
#define TAG_200 200
#define TAG_300 300
#define TAG_400 400
#define TAG_999 999
#define TAG_9999 9999

//Tags for editable fields
#define TAG_111 111

#define TAG_500 500
#define TAG_600 600
#define TAG_700 700
#define TAG_800 800
#define TAG_900 900
#define TAG_1000 1000
#define TAG_1100 1100
#define TAG_1200 1200

#define TAG_1300 1300
#define TAG_1400 1400
#define TAG_1500 1500
#define TAG_1600 1600

@interface SalesOrderEntryViewController ()
{
    int alertFreightValue;
    float freightValue;
    
    BOOL _longPress;
    NSMutableArray *arrProductPurchaseHistory;
    NSString *strStockCode;
    UITableView *tblProductHistory;
     NSString *strCopyDelDate;
    
    NSMutableDictionary *dictCustomerDetails;
    NSMutableArray *arrCustomerTransactions;
    
    NSMutableDictionary *dictStockPickup;
    
    MJDetailViewController *detailViewController1;
    MJDebtorViewController *detailViewController;

    NSMutableArray *arrReviewOrder;

}
- (IBAction)btnShowSpecials:(id)sender;


@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentSearch;
- (IBAction)filterSearchResult:(id)sender;

@end

@implementation SalesOrderEntryViewController
@synthesize dictHeaderDetails,strProductName,strProductCode,strWarehouse,arrProducts,strOrderNum,arrProductLabels,isCashPickUpOn,lblTotalCost,btnAddMenu,popoverController,isFuelLevySelected,strCashPickUp,strChequeAvailable,isStockPickUpOn,isEditable,strSelectedOrderNum;

@synthesize strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strDelInst1,strDelInst2,strCustOrder,strDebtor,strDeliveryRun,strContact,btnDelete,strDelDate,deleteIndexPath;


@synthesize strPriceChanged;

@synthesize  isSearch;

@synthesize strBalanceVal;

@synthesize strAddress1,strAddress2,strAddress3,strSubUrb,strAllowPartial,strBranch,strCarrier,strChargeRate,strChargetype,strCountry,strDirect,strDropSequence,strExchangeCode,strExportDebtor,strHeld,strName,strNumboxes,strOrderDate,strPeriodRaised,strPostCode,strPriceCode,strSalesBranch,strSalesman,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strTradingTerms,strYearRaised,strStockPickup,strTotalCartonQty,strExchangeRate,strEditStatus,strActive,strStatus,strTotalWeight,strTotalVolume,strTotalTax,strTotalCost,strTotalLines,strRefreshPrice,strConvFactor,strBOQty,strOperator,strDecimalPlaces,strUploadDate,strSuccessMessage,strOrigCost,strDateRaised,strCommentDateCreated,strCommentFollowDate,strCommenttxt,strSalesType,strPickupInfo,strPickupForm,isPickupFormOn,spinner;

@synthesize vwNoProducts,vwContentSuperview,vwHeader,vwDetails,vwSegmentedControl,lblProductName,imgArrowDownwards,tblDetails,tblHeader,isCommentExist,isCommentAddedOnLastLine,finalise;

@synthesize lblTotalLines;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressDetails:)];
    [tblDetails addGestureRecognizer:longPressRecognizer];
    
    self.isCommentAddedOnLastLine = YES; // 14thNov
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    CATransform3D transform = CATransform3DIdentity;
    
    //Add layer to Pointing image
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"pointing_down" ofType:@"png"];
    UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    layerPointingImage = [CALayer layer];
    layerPointingImage.contents = (id)downImage.CGImage;
    layerPointingImage.bounds = CGRectMake(0, 0, imgArrowDownwards.frame.size.width, imgArrowDownwards.frame.size.height);
    layerPointingImage.position = CGPointMake(10,30);
    layerPointingImage.transform = CATransform3DTranslate(transform, 0.0, 6,0.0);
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"SalesOrderEntryHeader" Type:@"plist"];
    
    // Build the array from the plist
    arrProductLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    
    arrProducts = [[NSMutableArray alloc] init];
    
    backgroundQueueForSalesOrderEntry = dispatch_queue_create("com.nanan.myscmipad.bgqueueForStockProductDetails", NULL);
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
    self.strDateRaised = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.strOperator = [prefs stringForKey:@"userName"];
    self.strSalesType = [prefs stringForKey:@"salesType"];
    self.strWarehouse = [prefs objectForKey:@"warehouse"];
    if (self.strWarehouse.length < 2) {
        self.strWarehouse = [NSString stringWithFormat:@"0%@",self.strWarehouse];
    }
    
    self.strOperatorName = [prefs objectForKey:@"members"];
    
    //By default
    [self setDefaults];
    
    if (isEditable) {
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(backgroundQueueForSalesOrderEntry, ^(void) {
            [self callGetSalesOrderDetailsFromDB];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                [tblHeader reloadData];
            });
        });
    }
    else{
        self.isCommentExist = NO;
    }
    // Store the view is profile order.
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isSaleOrderDetailCheck"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForSale"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    cntViewWillAppear = 0;
}


- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.lblTotalLines = nil;
    self.lblTotalCost = nil;
    self.btnAddMenu = nil;
    self.vwNoProducts = nil;
    self.vwContentSuperview = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwSegmentedControl = nil;
    self.lblProductName = nil;
    self.imgArrowDownwards = nil;
    self.tblDetails = nil;
    self.tblHeader = nil;
    self.popoverController = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //Counter is taken coz viewwillappear gets called twice.
    cntViewWillAppear++;
    
    if (cntViewWillAppear == 1) {
        //Notification to reload table when debtor is selected from Debtor list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHeader:) name:@"ReloadAddDebtorDetailsNotification" object:nil];
        
        //Notification to reload table when products added to details from product list
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadAddProductDetailsNotification" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveOrderForProfile) name:@"SAVE_SALES" object:nil];

    }
    
 
    self.strPriceChanged = @"N";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    cntViewWillAppear = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Show Specials

-(void)setContractPrice:(NSMutableArray *)arrprod
{
    
}


- (IBAction)btnShowSpecials:(id)sender
{
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [db open];
    FMResultSet *results;
    @try {
        
        results = [db executeQuery:@"SELECT sam.SCM_RECNUM as ScmRecum, sam.DESCRIPTION as Description,sam.LOCATION as Location,sam.PRICE1 as Price1,sam.PRICE2 as Price2,sam.PRICE3 as Price3, sam.PRICE4 as Price4, sam.PRICE5 as Price5, sam.WAREHOUSE as Warehouse, sam.AVAILABLE as Available, sam.ON_HAND as OnHand, sam.PURCHASE_ORDER as PurchaseOrder,sam.PICTURE_FIELD as picture_field , sam.SUBSTITUTE as Subsitute, sam.ALLOCATED as Allocated, sam.SALES_DISS as Dissection, sam.COST_DISS as Dissection_Cos, sam.WEIGHT as Weight, sam.VOLUME as Volume, sam.STANDARD_COST as Cost, sam.AVERAGE_COST as AverageCost, sam.LAST_SOLD as LastSold, sam.PROD_CLASS as ProdClass, sam.PROD_GROUP as ProdGroup, sys.KEY_FIELD as Customer, sys.STOCK_CODE as StockCode, sys.TYPE1 as Type1, sys.DISCOUNT1 as Discount,sys.CUST_CATEGORY as cust_category FROM samaster as sam JOIN sysdisct as sys ON sam.CODE = sys.STOCK_CODE WHERE sys.DATE_TO1 BETWEEN DATETIME('now') AND DATETIME('now','360 days') ORDER BY  sam.DESCRIPTION"];

        if (!results)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        NSMutableArray *arrMuteData = [NSMutableArray new];
        while ([results next])
        {
            @autoreleasepool {

            NSMutableDictionary *dictData = (NSMutableDictionary *)[results resultDictionary];
            
            [dictData setValue:@"0" forKey:@"q0"];
            [dictData setValue:@"0" forKey:@"q2"];
            [dictData setValue:@"0" forKey:@"q3"];
            [dictData setValue:@"0" forKey:@"q4"];
            [dictData setValue:@"Specials" forKey:@"FromSpecials"];
            [arrMuteData addObject:dictData];
            }
        }

       [results close];
        
        if([arrMuteData count] > 0)
        {
            for (int i=0; i<[arrMuteData count]; i++)
            {
                
                //cust_category
                if([[[arrMuteData objectAtIndex:i] objectForKey:@"Customer"] isEqualToString:@""])//&& [[[arrMuteData objectAtIndex:i] objectForKey:@"cust_category"] isEqualToString:@""]
                {
                    @autoreleasepool {
                        
                        NSString *strCheckStockCode = [[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"];
                        
                        if([arrProducts count] > 0)
                        {
                            
                            BOOL isMatch = NO;
                            for (int j=0; j<[arrProducts count]; j++)
                            {
                                NSString *strCode = [[arrProducts objectAtIndex:j] objectForKey:@"  "];
                                
                                if(![strCheckStockCode isEqualToString:strCode])
                                {
                                    isMatch = NO;
                                }
                                else
                                {
                                    isMatch = YES;
                                    break;
                                }
                            }
                            
                            if(!isMatch)
                            {
                                [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                            }
                        }
                        else
                        {
                            [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                        }
                    }
                }
                else if ([[[arrMuteData objectAtIndex:i] objectForKey:@"Customer"] isEqualToString:strDebtor]) {
                    @autoreleasepool {
                        NSString *strCheckStockCode = [[arrMuteData objectAtIndex:i] objectForKey:@"StockCode"];
                        
                        if([arrProducts count] > 0)
                        {
                            for (int j=0; j<[arrProducts count]; j++)
                            {
                                NSString *strCode = [[arrProducts objectAtIndex:j] objectForKey:@"StockCode"];
                                
                                if([strCheckStockCode isEqualToString:strCode]){
                                    [[arrProducts objectAtIndex:j] setObject:@"ContractSpecial" forKey:@"ContractSpecial"];
                                }
                            }
                            
                        }
                        else
                        {
                            [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                        }
                    }
                }
                else{
                    if ([[arrProducts valueForKey:@"StockCode"] containsObject:[[arrMuteData objectAtIndex:i]objectForKey:@"StockCode"]])
                    {
                        NSLog(@"OBJECT EXISTS");
                    }
                    else{
                        [arrProducts addObject:[arrMuteData objectAtIndex:i]];
                    }
                    
                }
            }
        }
        
//       NSLog(@"arrProducts - %@",arrProducts);
        
        int lastIndex = [arrProducts count];
        if (lastIndex)
        {
            [self showOrEnableButtons];
            
            //Show delete button
            btnDelete.hidden = NO;
            tblDetails.editing = NO;
            [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
            [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
            
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwDetails];
            [tblDetails reloadData];
            
        }
        else
        {
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwNoProducts];
            btnDelete.hidden = YES;
        
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Specials Available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
        //NSLog(@"%@",arrProducts);
        
       
    }
    @catch (NSException *e) {
        // rethrow if not one of the two exceptions above
        
        [results close];
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }];
        
    }
    [db close];
}

-(void)saveOrderForProfile
{
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts)
    {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        if ([dict objectForKey:@"Item"]){
            
            if (![dict objectForKey:@"ExtnPrice"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = FALSE;
                break;
            }
            else if (![dict objectForKey:@"ExtnPrice"]&& ![[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = TRUE;
                //  break;
            }
            else if ([dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = FALSE;
                break;
            }
            else{
                isCalculationPending = FALSE;
            }
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    //    if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderOnHold"] isEqualToString:@"Y"]) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Order on hold" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //        alert.tag = TAG_400;
    //        [alert show];
    //    }
    //    else{
    
    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        [self callInsertSalesOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
    }
    else{
        //31stOct
        [self callInsertSalesOrderToDB];
        if([dictHeaderDetails objectForKey:@"PickUpForm"]){
            [self insertStockPickupToDB];
        }
        //[self callWSSaveProfileOrder];
    }
}

#pragma mark - Review Order
-(void)reviewOrderList{
    
    /* NSMutableArray *sortProfileArr = [NSMutableArray new];
     for (int i = 0; i < arrProducts.count; i++)
     {
     if (![[arrProducts objectAtIndex:i]objectForKey:@"FromSpecials" ]){
     [sortProfileArr addObject:[arrProducts objectAtIndex:i]];
     }
     }
     
     NSArray *myArray = [NSArray arrayWithArray:(NSArray*)sortProfileArr];
     NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastSold" ascending:NO];
     NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
     NSArray *sortedArray = [myArray sortedArrayUsingDescriptors:sortDescriptors];
     
     // 1.Empty SortedProfilearray and add profile Objects to the top
     if (sortProfileArr.count > 0) {
     [sortProfileArr removeAllObjects];
     sortProfileArr=[NSMutableArray arrayWithArray:sortedArray];
     }
     
     // 2. Add Special object to the bottom
     for (int i = 0; i < arrProducts.count; i++)
     {
     if ([[arrProducts objectAtIndex:i]objectForKey:@"FromSpecials" ]){
     id object = [arrProducts objectAtIndex:i];
     //        [arrProducts removeObjectAtIndex:i];
     [sortProfileArr addObject:object];
     }
     }
     
     // 3. All the orders woth quantity > 0 and commets and freight to the top
     for (int i = 0; i < sortProfileArr.count; i++)
     {
     if ([[sortProfileArr objectAtIndex:i]objectForKey:@"QuantityOrdered"])
     {
     // Check for Quantity Ordered
     if ([[[sortProfileArr objectAtIndex:i]objectForKey:@"QuantityOrdered"]integerValue] > 0) {
     id object = [sortProfileArr objectAtIndex:i];
     [sortProfileArr removeObjectAtIndex:i];
     [sortProfileArr insertObject:object atIndex:0];
     }
     }
     else if ([[sortProfileArr objectAtIndex:i]objectForKey:@"Item" ]){
     id object = [sortProfileArr objectAtIndex:i];
     [sortProfileArr removeObjectAtIndex:i];
     [sortProfileArr insertObject:object atIndex:0];
     }
     
     }
     // Merge all the formated objects to the arr Prod
     if (arrProducts.count > 0) {
     arrProducts = [sortProfileArr mutableCopy];
     }
     
     [_tblDetails setContentOffset:CGPointMake(0, 0)];
     [_tblDetails reloadData];*/
    
    // 1.Empty arrReviewOrder
    if (arrReviewOrder.count > 0) {
        [arrReviewOrder removeAllObjects];
    }
    arrReviewOrder=[NSMutableArray new];
    
    // 2. All the orders woth quantity > 0 and commets and freight to the top
    for (int i = 0; i < arrProducts.count; i++)
    {
        if ([[arrProducts objectAtIndex:i]objectForKey:@"QuantityOrdered"])
        {
            // Check for Quantity Ordered
            if ([[[arrProducts objectAtIndex:i]objectForKey:@"QuantityOrdered"]integerValue] > 0)
            {
                id object = [arrProducts objectAtIndex:i];
                [arrReviewOrder insertObject:object atIndex:0];
            }
        }
    }
    
    if(arrReviewOrder.count > 0)
    {
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
        
        UITableView * tblReviewOrder = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
        [tblReviewOrder setDataSource:self];
        [tblReviewOrder setDelegate:self];
        tblReviewOrder.tag = TAG_9999;
        [contentView addSubview:tblReviewOrder];
        
        [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
        [tblReviewOrder reloadData];
    }
    
}

#pragma mark - Long Press
-(void)onLongPressDetails:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tblDetails];
    
    NSIndexPath *indexPath = [tblDetails indexPathForRowAtPoint:p];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    
    if(isSearch)
    {
        strStockCode = [[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    else
    {
        strStockCode = [[arrProducts objectAtIndex:indexPath.row] objectForKey:@"StockCode"];
    }
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/profordentry/arhisdetlist.php"];
            NSURL *url = [NSURL URLWithString:strRequest];
            
            ASIFormDataRequest *request1 = [ASIFormDataRequest requestWithURL:url];
            __weak ASIFormDataRequest *request = request1;
            [request addPostValue:strDebtor forKey:@"customer"];
            [request addPostValue:strStockCode forKey:@"stock_code"];
            [request startAsynchronous];
            
            [request setCompletionBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                NSString *responseString = [request responseString];
                NSDictionary *dictResponse = [XMLReader dictionaryForXMLString:responseString error:nil];
                
                NSString *strFalg = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Flag"] objectForKey:@"text"];
                
                if([strFalg isEqualToString:@"True"])
                {
                    if(!arrProductPurchaseHistory)
                    {
                        arrProductPurchaseHistory = [NSMutableArray new];
                    }
                    else
                    {
                        [arrProductPurchaseHistory removeAllObjects];
                    }
                    
                    NSMutableArray *arrTempArr = [NSMutableArray new];
                    
                    if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]])
                    {
                        [arrTempArr addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"]];
                    }
                    else
                    {
                        arrTempArr = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Productinfo"] objectForKey:@"Data"];
                    }
                    
                    if([arrTempArr count] > 0)
                    {
                        for (int i=0;i<[arrTempArr count];i++)
                        {
                            NSMutableDictionary *arrTempDict = [NSMutableDictionary new];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Price"] objectForKey:@"text"] forKey:@"Price"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"DateRaised"] objectForKey:@"text"] forKey:@"DateRaised"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"InvNo"] objectForKey:@"text"] forKey:@"InvNo"];
                            [arrTempDict setValue:[[[arrTempArr objectAtIndex:i] objectForKey:@"Quantity"] objectForKey:@"text"] forKey:@"Quantity"];
                            
                            [arrProductPurchaseHistory addObject:arrTempDict];
                            
                        }
                    }
                    
                    if([arrProductPurchaseHistory count] > 0)
                    {
                        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                        
                        tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                        [tblProductHistory setDataSource:self];
                        [tblProductHistory setDelegate:self];
                        tblProductHistory.tag = TAG_999;
                        
                        [contentView addSubview:tblProductHistory];
                        
                        [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                        [tblProductHistory reloadData];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                _longPress = NO;
            }];
            [request setFailedBlock:^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [spinner hide:YES];
                    [spinner removeFromSuperview];
                });
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Error loading the details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                _longPress = NO;
            }];
        }
    }
    else
    {
        if(!_longPress)
        {
            _longPress = YES;
            
            //Get the data from DB
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                
                FMResultSet *rs1 = [db executeQuery:@"SELECT TRIM(DEBTOR), TRIM(STOCK_CODE), TRIM(QUANTITY) as QUANTITY, TRIM(PRICE) as PRICE, TRIM(DATE_RAISED) as DATE_RAISED, TRIM(TRAN_NO) as TRAN_NO from arhisdet WHERE DEBTOR = ? AND STOCK_CODE = ? ORDER BY DATE_RAISED desc",strDebtor,strStockCode];
                
                if (!rs1)
                {
                    [rs1 close];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to fetch the records." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                }
                
                if(!arrProductPurchaseHistory)
                {
                    arrProductPurchaseHistory = [NSMutableArray new];
                }
                else
                {
                    [arrProductPurchaseHistory removeAllObjects];
                }
                
                while ([rs1 next])
                {
                    NSString *strDate = [rs1 stringForColumn:@"DATE_RAISED"];
                    
                    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                    [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *theDate = [inputFormatter dateFromString:strDate];
                    
                    NSDateFormatter *inputFormatterNew = [[NSDateFormatter alloc] init];
                    [inputFormatterNew setDateFormat:@"dd/MM/yy"];
                    
                    NSString *strNewDate = [inputFormatterNew stringFromDate:theDate];
                    
                    NSMutableDictionary *tempDict = [NSMutableDictionary new];
                    [tempDict setValue:[rs1 stringForColumn:@"PRICE"] forKey:@"Price"];
                    [tempDict setValue:strNewDate forKey:@"DateRaised"];
                    [tempDict setValue:[rs1 stringForColumn:@"TRAN_NO"] forKey:@"InvNo"];
                    [tempDict setValue:[rs1 stringForColumn:@"QUANTITY"] forKey:@"Quantity"];
                    
                    [arrProductPurchaseHistory addObject:tempDict];
                }
                
                if([arrProductPurchaseHistory count] > 0)
                {
                    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 580)];
                    
                    tblProductHistory = [[UITableView alloc] initWithFrame:CGRectMake(10,10,560, 560) style:UITableViewStylePlain];
                    [tblProductHistory setDataSource:self];
                    [tblProductHistory setDelegate:self];
                    tblProductHistory.tag = TAG_999;
                    
                    [contentView addSubview:tblProductHistory];
                    
                    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
                    [tblProductHistory reloadData];
                    
                    _longPress = NO;
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No details found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    alert = nil;
                    
                    _longPress = NO;
                }
                
                [rs1 close];
            }];
        }
    }
}

- (void)changeCloseButtonType:(id)sender{
    UIButton *button = (UIButton *)sender;
    KGModal *modal = [KGModal sharedInstance];
    KGModalCloseButtonType type = modal.closeButtonType;
    
    if(type == KGModalCloseButtonTypeLeft){
        modal.closeButtonType = KGModalCloseButtonTypeRight;
        [button setTitle:@"Close Button Right" forState:UIControlStateNormal];
    }else if(type == KGModalCloseButtonTypeRight){
        modal.closeButtonType = KGModalCloseButtonTypeNone;
        [button setTitle:@"Close Button None" forState:UIControlStateNormal];
    }else{
        modal.closeButtonType = KGModalCloseButtonTypeLeft;
        [button setTitle:@"Close Button Left" forState:UIControlStateNormal];
    }
}

#pragma mark - Text Field Delgates
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == TAG_500)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 34)
        {
            return NO;
        }
    }
    else if (textField.tag == TAG_600)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
    }
    else if (textField.tag == TAG_700)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_800)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 29)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_900)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 39)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1000)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 9)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1100)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1400)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 19)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1500)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 24)
        {
            return NO;
        }
        
    }
    else if (textField.tag == TAG_1600)
    {
        int tempCount = [textField.text length];
        
        if(tempCount > 24)
        {
            return NO;
        }
        
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textfield {
    CGPoint pointInTable = [textfield.superview convertPoint:textfield.frame.origin toView:self.tblHeader];
    CGPoint contentOffset = self.tblHeader.contentOffset;
    contentOffset.y = (pointInTable.y - textfield.inputAccessoryView.frame.size.height);
    [self.tblHeader setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[CustomCellGeneral class]])
    {
        CustomCellGeneral *cell = (CustomCellGeneral*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tblHeader indexPathForCell:cell];
        
        [self.tblHeader scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];        
    }
        
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    switch (textField.tag) {
        case TAG_500:
        {
            self.strDelName = textField.text;
        }
            break;
        case TAG_600:
        {
            self.strDelAddress1 = textField.text;
            
        }
            break;
            
        case TAG_700:
        {
            self.strDelAddress2 = textField.text;
        }
            break;
            
        case TAG_800:
        {
            self.strDelAddress3 = textField.text;
        }
            break;
            
        case TAG_900:
        {
            self.strDelSubUrb = textField.text;
        }
            break;
            
        case TAG_1000:
        {
            self.strDelPostCode = textField.text;
        }
            break;
            
        case TAG_1100:
        {
            self.strDelCountry = textField.text;
        }
            break;
            
            //Contact
        case TAG_1200:{
            self.strContact = textField.text;
        }
            break;
            //Customer Order
        case TAG_1400:{
            self.strCustOrder = textField.text;
        }
            break;
            //Del inst1
        case TAG_1500:{
            self.strDelInst1 = textField.text;
        }
            break;
            //Del inst2
        case TAG_1600:{
            self.strDelInst2 = textField.text;
        }
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Table view Delegates

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == TAG_100) {
        return YES;
    }
    else{
        return NO;
    }
}

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 0) {
                return nil;
            }
            else if (indexPath.section == 2 && [indexPath row]!=11) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strName;
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = strAddress1;
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = strAddress2;
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = strAddress3;
                    }
                        break;
                    case 4:
                    {
                        stringToCheck = strSubUrb;
                        
                    }
                        break;
                    case 6:
                    {
                        stringToCheck = strCountry;
                        
                    }
                        break;
                                         
                    case 9:
                    {
                        if (strContact) {
                            stringToCheck = strContact;
                        }
                        
                    }
                        break;
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
            }
            
            else if (indexPath.section == 3 && [indexPath row]!=7) {
                return nil;
            }
            else if (indexPath.section == 4) {
                return nil;
            }

            else if (indexPath.section == 5) {
                NSString *stringToCheck = nil;
                if ([strCommenttxt isKindOfClass:[NSNull class]] || [strCommenttxt isEqualToString:@""] || strCommenttxt == nil) {
                    return indexPath;
                }
                else{
                    switch ([indexPath row]) {
                        case 0:
                        {
                            stringToCheck = strCommenttxt;
                        }
                            break;
                    }
                    //We only don't want to allow selection on any cells which cannot be expanded
                    if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                    {
                        return indexPath;
                    }
                    else {
                        return nil;
                    }
                }
                
            }
            else{
                return indexPath;
            }
        }break;
            
        case TAG_100:return indexPath;break;
            
        default:return nil;break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = strName;
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = strAddress1;
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = strAddress2;
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = strAddress3;
                    }
                        break;
                    case 4:
                    {
                        stringToCheck = strSubUrb;
                        
                    }
                        break;
                    case 6:
                    {
                        stringToCheck = strCountry;
                        
                    }
                        break;
                        
                    case 9:
                    {
                        if (strContact) {
                            stringToCheck = strContact;
                        }
                        
                    }
                        break;
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                }
            }
            else{
                return 60;break;
            }
        
        }break;
            
        case TAG_100:return 50;break;
        
        case TAG_999:return 50;break;
            
        case TAG_9999:return 50;break;

     
        default:return 0;break;
    }
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:
            if([[AppDelegate getAppDelegateObj] CheckIfDemoApp])
            {
                return [arrProductLabels count] - 2;
            }
            else
            return [arrProductLabels count];break;
            
        case TAG_100:return 1;break;
       
        case TAG_999:return 1;break;
            
        case TAG_9999:return 1;break;

        default:return 0;break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == TAG_999)
    {
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 560, 80)];
        [tempView setBackgroundColor:[UIColor lightGrayColor]];
        
        CGSize maximumSize = CGSizeMake(300, 9999);
        NSString *myString = [NSString stringWithFormat:@"Customer: %@",strDebtor];
        UIFont *myFont = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize = [myString sizeWithFont:myFont
                                   constrainedToSize:maximumSize
                                       lineBreakMode:NSLineBreakByWordWrapping];
        
        CGSize maximumSize2 = CGSizeMake(300, 9999);
        NSString *myString2 = [NSString stringWithFormat:@"Stock Code: %@",strStockCode];
        UIFont *myFont2 = [UIFont boldSystemFontOfSize:15.0f];
        CGSize myStringSize2 = [myString2 sizeWithFont:myFont2
                                     constrainedToSize:maximumSize2
                                         lineBreakMode:NSLineBreakByWordWrapping];
        
        UILabel *lblDebtor = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, myStringSize.width, myStringSize.height)];
        [lblDebtor setBackgroundColor:[UIColor clearColor]];
        [lblDebtor setText:myString];
        [lblDebtor setTextAlignment:NSTextAlignmentCenter];
        [lblDebtor setTextColor:[UIColor blackColor]];
        [lblDebtor setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        int x = 552 - myStringSize2.width;
        UILabel *lblStockCode = [[UILabel alloc] initWithFrame:CGRectMake(x, 10, myStringSize2.width, myStringSize2.height)];
        [lblStockCode setBackgroundColor:[UIColor clearColor]];
        [lblStockCode setText:myString2];
        [lblStockCode setTextAlignment:NSTextAlignmentCenter];
        [lblStockCode setTextColor:[UIColor blackColor]];
        [lblStockCode setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(8, 45, 60, 30)];
        [lblDate setBackgroundColor:[UIColor clearColor]];
        [lblDate setText:@"Date"];
        [lblDate setTextAlignment:NSTextAlignmentCenter];
        [lblDate setTextColor:[UIColor blackColor]];
        [lblDate setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblQuanty = [[UILabel alloc] initWithFrame:CGRectMake(120, 45, 90, 30)];
        [lblQuanty setBackgroundColor:[UIColor clearColor]];
        [lblQuanty setText:@"Quantity"];
        [lblQuanty setTextAlignment:NSTextAlignmentCenter];
        [lblQuanty setTextColor:[UIColor blackColor]];
        [lblQuanty setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(240,45, 90, 30)];
        [lblPrice setBackgroundColor:[UIColor clearColor]];
        [lblPrice setText:@"Price"];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];
        [lblPrice setTextColor:[UIColor blackColor]];
        [lblPrice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        
        UILabel *lblInvoice = [[UILabel alloc] initWithFrame:CGRectMake(345,45, 200, 30)];
        [lblInvoice setBackgroundColor:[UIColor clearColor]];
        [lblInvoice setText:@"Invoice/CN No"];
        [lblInvoice setTextAlignment:NSTextAlignmentCenter];
        [lblInvoice setTextColor:[UIColor blackColor]];
        [lblInvoice setFont:[UIFont boldSystemFontOfSize:20.0f]];
        
        [tempView addSubview:lblDebtor];
        [tempView addSubview:lblStockCode];
        [tempView addSubview:lblDate];
        [tempView addSubview:lblQuanty];
        [tempView addSubview:lblPrice];
        [tempView addSubview:lblInvoice];
        
        lblDate = nil;
        lblQuanty = nil;
        lblPrice = nil;
        lblInvoice = nil;
        
        return tempView;
    }
    else  if (tableView.tag == TAG_9999)
    {
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 560, 80)];
        [tempView setBackgroundColor:[UIColor lightGrayColor]];
        NSString *myString = [NSString stringWithFormat:@"Customer: %@",strDebtor];
     
        UILabel *lblDebtor = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 120, 30)];
        [lblDebtor setBackgroundColor:[UIColor clearColor]];
        [lblDebtor setText:myString];
        [lblDebtor setTextAlignment:NSTextAlignmentCenter];
        [lblDebtor setTextColor:[UIColor blackColor]];
        [lblDebtor setFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        
        
        UILabel *lblCode = [[UILabel alloc] initWithFrame:CGRectMake(8, 45, 60, 30)];
        [lblCode setBackgroundColor:[UIColor clearColor]];
        [lblCode setText:@"Code"];
        [lblCode setTextAlignment:NSTextAlignmentCenter];
        [lblCode setTextColor:[UIColor blackColor]];
        [lblCode setFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        UILabel *lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(120, 45, 220, 30)];
        [lblDescription setBackgroundColor:[UIColor clearColor]];
        [lblDescription setText:@"Decription"];
        [lblDescription setTextAlignment:NSTextAlignmentCenter];
        [lblDescription setTextColor:[UIColor blackColor]];
        [lblDescription setFont:[UIFont boldSystemFontOfSize:15.0f]];
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(380,45, 90, 30)];
        [lblPrice setBackgroundColor:[UIColor clearColor]];
        [lblPrice setText:@"Price"];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];
        [lblPrice setTextColor:[UIColor blackColor]];
        [lblPrice setFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        UILabel *lblQty = [[UILabel alloc] initWithFrame:CGRectMake(480,45, 60, 30)];
        [lblQty setBackgroundColor:[UIColor clearColor]];
        [lblQty setText:@"Qty"];
        [lblQty setTextAlignment:NSTextAlignmentCenter];
        [lblQty setTextColor:[UIColor blackColor]];
        [lblQty setFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        [tempView addSubview:lblDebtor];
        [tempView addSubview:lblCode];
        [tempView addSubview:lblDescription];
        [tempView addSubview:lblPrice];
        [tempView addSubview:lblQty];
        
        lblDebtor =nil;
        lblCode = nil;
        lblDescription = nil;
        lblQty = nil;
        lblPrice = nil;
        
        return tempView;
    }

    return nil;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
            
        case TAG_50: return [(NSArray *)[arrProductLabels objectAtIndex:section] count];break;
            
        case TAG_100:{
            if (isSearch)
            {
                return [arrSearchProducts count];
            }
            else
            {
                return [arrProducts count];
            }
        }
        break;
        
        case TAG_999: return [arrProductPurchaseHistory count];break;
            
        case TAG_9999: return [arrReviewOrder count];break;

        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    static NSString *CellIdentifier14 = CELL_IDENTIFIER14;
     static NSString *CellIdentifier19 = CELL_IDENTIFIER19;
    CustomCellGeneral *cell;
    
    
    switch (tableView.tag) {
        case TAG_50:{
            
            if ([indexPath section] == 0) {
                switch ([indexPath row]) {
                    case 0:{
                        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                        
                        if (cell == nil) {
                            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:4];
                            
                        }
                        cell.lblValue.text = nil;
                        if (isEditable) {
                            cell.lblValue.text = strOrderNum;
                        }
                        
                    }
                        break;
                }
            }
            else if ([indexPath section] == 1) {
                switch ([indexPath row]) {
                    case 0:{
                        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                        
                        //Debtor
                        if (cell == nil) {
                            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:3];
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            cell.txtValue.userInteractionEnabled = FALSE;
                            cell.txtValue.placeholder = @"Please Select";
                            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                        }
                        
                        cell.txtValue.text = nil;
                        if (strDebtor) {
                            cell.txtValue.text = strDebtor;
                            segControl.enabled = YES;
                        }
                        
                    }
                        
                        break;
                }
            }
            else if ([indexPath section] == 2) {
                
                if ([indexPath row] == 7) {
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                    if (cell == nil) {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:9];
                        cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
                        cell.txtGlowingValue.delegate = self;
                        
                    }
                }
                else if([indexPath row] == 11){
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                    if (cell == nil) {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:3];
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.txtValue.userInteractionEnabled = FALSE;
                        cell.txtValue.placeholder = @"Please Select";
                        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                       
                    }
                }
                else{
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                    if (cell == nil) {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:4];
                        
                    }
                    
                }
                
                cell.txtGlowingValue.text = nil;
                cell.txtValue.text = nil;
                cell.lblValue.text = nil;
                
                switch ([indexPath row]) {
                        
                    case 0:
                    {
                        if (strName) {
                            cell.lblValue.text = strName;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        if (strAddress1) {
                            cell.lblValue.text = strAddress1;
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        
                        if (strAddress2) {
                            cell.lblValue.text = strAddress2;
                        }
                        
                    }
                        break;
                    case 3:
                    {
                        
                        if (strAddress3) {
                            cell.lblValue.text = strAddress3;
                        }
                    }
                        break;
                    case 4:
                    {
                        
                        if (strSubUrb) {
                            cell.lblValue.text = strSubUrb;
                        }
                        
                    }
                        break;
                    case 5:
                    {
                        
                        if (strPostCode) {
                            cell.lblValue.text = strPostCode;
                        }
                        
                    }
                        break;
                    case 6:
                    {
                        if (strCountry) {
                            cell.lblValue.text = strCountry;
                        }
                        
                    }
                        break;
                    case 7:
                    {
                        if (![strCustOrder isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strCustOrder;
                        }
                        
                        cell.txtGlowingValue.tag = TAG_1400;
                    }
                        break;
                        
                        
                    case 8:
                    {
                        if (strOrderDate && ![strOrderDate isEqualToString:STANDARD_APP_DATE] && ![strOrderDate isEqualToString:STANDARD_SERVER_DATE]) {
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSDate *delDate = [formatter dateFromString:strDelDate];
                            if (delDate == nil) {
                                cell.txtValue.text = strDelDate;
                            }
                            else{
                                cell.txtValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strDelDate];
                            }
                        }
                        
                    }
                        break;
                        
                    case 9:
                    {
                        if (strContact) {
                            cell.lblValue.text = strContact;
                        }
                        
                    }
                        break;
                        
                    case 10:
                    {
                        if (strWarehouse) {
                            cell.lblValue.text = strWarehouse;
                        }
                        
                    }
                        break;
                        
                    case 11:
                    {
                        //Delivery Run
                        if (strDeliveryRun) {
                            cell.txtValue.text = strDeliveryRun;
                        }
                        
                        
                    }
                        break;
                        
                    case 12:
                    {
                        if (strTradingTerms)
                        {
                            if([[strTradingTerms trimSpaces] isEqualToString:@"14"])
                            {
                                cell.lblValue.text = @"14 days from invoice";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"C1"])
                            {
                                cell.lblValue.text = @"Cash On Delivery";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"I7"])
                            {
                                cell.lblValue.text = @"7 days from invoice";
                            }
                            else if([[strTradingTerms trimSpaces] isEqualToString:@"S7"])
                            {
                                cell.lblValue.text = @"7 days from statement";
                            }
                            else
                            {
                                cell.lblValue.text = @"30 days from invoice";
                            }
                        }
                        
                    }
                        break;
                        
                }
                
                if (selectedSectionIndex == 2 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }
                
            }
            else if ([indexPath section] == 3) {
                
                if ([indexPath row] == 7) {
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                    if (cell == nil) {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:3];
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.txtValue.userInteractionEnabled = FALSE;
                        cell.txtValue.placeholder = @"Please Select";
                        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                        
                    }
                }
                else{
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                    if (cell == nil) {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        cell = [nib objectAtIndex:9];
                        cell.txtGlowingValue.keyboardType = UIKeyboardTypeNamePhonePad;
                        cell.txtGlowingValue.delegate = self;
                    }
                    
                }
                
                cell.txtValue.text = nil;
                cell.txtGlowingValue.text = nil;
                switch ([indexPath row]) {
                        
                    case 0:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_500;
                        if (strDelName) {
                            cell.txtGlowingValue.text = strDelName;
                        }
                        
                    }
                        break;
                        
                        
                    case 1:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_600;
                        if (strDelAddress1) {
                            cell.txtGlowingValue.text = strDelAddress1;
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_700;
                        if (strDelAddress2) {
                            cell.txtGlowingValue.text = strDelAddress2;
                        }
                        
                    }
                        break;
                    case 3:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_800;
                        if (strDelAddress3) {
                            cell.txtGlowingValue.text = strDelAddress3;
                        }
                        
                    }
                        break;
                    case 4:
                    {
                        
                        cell.txtGlowingValue.tag = TAG_900;
                        if (strDelSubUrb) {
                            cell.txtGlowingValue.text = strDelSubUrb;
                        }
                        
                        
                    }
                        break;
                    case 5:
                    {
                        cell.txtGlowingValue.tag = TAG_1000;
                        if (strDelPostCode) {
                            cell.txtGlowingValue.text = strDelPostCode;
                        }
                        
                    }
                        break;
                    case 6:
                    {
                        cell.txtGlowingValue.tag = TAG_1100;
                        if (strDelCountry) {
                            cell.txtGlowingValue.text = strDelCountry;
                        }
                        
                    }
                        break;
                        
                        //Delivery date
                    case 7:
                    {
                        if (strDelDate && ![strDelDate isEqualToString:STANDARD_APP_DATE] && ![strDelDate isEqualToString:STANDARD_SERVER_DATE]) {
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSDate *delDate = [formatter dateFromString:strDelDate];
                            if (delDate == nil) {
                                cell.txtValue.text = strDelDate;
                            }
                            else{
                                cell.txtValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strDelDate];
                                
                            }

                        }
                        
                    }
                        break;
                        
                        //Del inst 1
                    case 8:
                    {
                        cell.txtGlowingValue.tag = TAG_1500;
                        if (![strDelInst1 isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strDelInst1;
                        }
                    }
                        break;
                        
                        //Del inst 2
                    case 9:
                    {
                        cell.txtGlowingValue.tag = TAG_1600;
                        if (![strDelInst2 isEqualToString:@"(null)"]) {
                            cell.txtGlowingValue.text = strDelInst2;
                        }
                    }
                        break;
                        
                    default:{
                        
                    }
                        break;
                }
            }
            
            //Comments
            else if ([indexPath section] == 4) {
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                if (cell == nil) {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                cell.lblValue.text = nil;
                
                
                switch ([indexPath row]) {
                    case 0:
                    {
                        if (strCommenttxt) {
                            cell.lblValue.text = strCommenttxt;
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        if (strCommentDateCreated && ![strCommentDateCreated isEqualToString:STANDARD_APP_DATE] && ![strCommentDateCreated isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentDateCreated];
                        }
                        
                    }
                        break;
                    case 2:
                    {
                        if (strCommentFollowDate && ![strCommentFollowDate isEqualToString:STANDARD_APP_DATE] && ![strCommentFollowDate isEqualToString:STANDARD_SERVER_DATE]) {
                            cell.lblValue.text = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:strCommentFollowDate];
                        }
                        
                    }
                        break;
                        
                        
                    default: {
                    }
                        break;
                }
                
                if (selectedSectionIndex == 6 && selectedIndex == indexPath.row) {
                    
                    CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     labelHeight);
                    
                }
                else {
                    
                    //Otherwise just return the minimum height for the label.
                    cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                     cell.lblValue.frame.origin.y,
                                                     cell.lblValue.frame.size.width,
                                                     COMMENT_LABEL_MIN_HEIGHT);
                }

                
                
            }
            
            cell.lblTitle.text = [[(NSArray *)[arrProductLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            return cell;
        }
            break;
            
        case TAG_100:{
            
            NSString *str = [NSString stringWithFormat:@"/C"];
            NSString *str1 = [NSString stringWithFormat:@"/F"];
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:5];
            }
            
            if(isSearch)
            {
                
                cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
                if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"FromSpecials"])
                {
                    if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"FromSpecials"])
                    {
                        
                        cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];
                        //   [[AppDelegate getAppDelegateObj].obj_Manager manage:cell.imgBackgroundView];
                    }
                    
                    
                }
                if([[arrSearchProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                    
                    NSString *str2 = [[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                    if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound) {
                        
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        }
                        else{
                            cell.lblValue.text = @"";
                        }
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                    else
                    {
                        //Freight
                        if ([str2 isEqualToString:str1]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                            
                            cell.accessoryType = UITableViewCellAccessoryNone;
                            
                        }
                        
                        //Comment
                        if ([str2 isEqualToString:str]) {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = @"";
                            
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        }
                        
                    }
                }
                else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrSearchProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }
            else{
                if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"])
                {
                    
                    NSString *str2 = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"];
                    if ([str rangeOfString:str2].location == NSNotFound && [str1 rangeOfString:str2].location == NSNotFound)
                    {
                        
                        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        }
                        else{
                            cell.lblValue.text = @"";
                        }
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    }
                    else
                    {
                        //Freight
                        if ([str2 isEqualToString:str1])
                        {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str1,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            
                        }
                        
                        //Comment
                        if ([str2 isEqualToString:str])
                        {
                            cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                            cell.lblValue.text = @"";
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        }
                        
                        
                    }
                }
                else{
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
//                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                        
                        //1st Dec, 2014
                        cell.lblValue.text = [NSString stringWithFormat:@"%@    $ %.2f",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"QuantityOrdered"],[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];

                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                
            }
        

            
            return cell;
            
        }
        case TAG_999:
        {
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier14];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:13];
            }
            
            int row = indexPath.row;
            cell.lblProdDate.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"DateRaised"];
            cell.lblProdQuantity.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Quantity"];
            cell.lblProdPrice.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"Price"];
            cell.lblProdInv.text = [[arrProductPurchaseHistory objectAtIndex:row] objectForKey:@"InvNo"];
            
            return cell;
        }break;
        
        case TAG_9999:
        {
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier19];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:19];
            }
            
            int row = (int)indexPath.row;
            cell.lblcode.text = [[arrReviewOrder objectAtIndex:row] objectForKey:@"StockCode"];
            cell.lblProdDesc.text = [[arrReviewOrder objectAtIndex:row] objectForKey:@"Description"];
            cell.lblOrderPrice.text = [NSString stringWithFormat:@"$ %.2f",[[[arrReviewOrder objectAtIndex:row] objectForKey:@"ExtnPrice"]floatValue]];
            cell.lblQty.text = [[arrReviewOrder objectAtIndex:row] objectForKey:@"QuantityOrdered"];
            
            return cell;
        }
            break;
        default:return nil;
            
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case TAG_50:{
            if ([indexPath section] == 1) {
                
                switch ([indexPath row]) {
                        //Debtor
                    case 0:
                    {
                        QuoteHistoryCustomerListViewController *dataViewController = [[QuoteHistoryCustomerListViewController alloc] initWithNibName:@"QuoteHistoryCustomerListViewController" bundle:[NSBundle mainBundle]];
                        dataViewController.isFromLoadProfile = NO;
                        dataViewController.isUserCallSchedule = NO;

                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                        break;
                        
                    default:{
                        
                    }
                        break;
                }
                
            }
            if ([indexPath section] == 2) {
                
                switch ([indexPath row]) {
                        //Delivery Run
                    case 11:
                    {
                                               
                        CustomCellGeneral *cell = (CustomCellGeneral *)[tblHeader cellForRowAtIndexPath:indexPath];
                                                
                        DeliveryRunViewController *dataViewController = [[DeliveryRunViewController alloc] initWithNibName:@"DeliveryRunViewController" bundle:[NSBundle mainBundle]];
                        
                        dataViewController.contentSizeForViewInPopover =
                        CGSizeMake(300, 400);
                        
                        //create a popover controller
                        self.popoverController = [[UIPopoverController alloc]
                                                  initWithContentViewController:dataViewController];
                        
                        CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                        
                        dataViewController.delegate = self;
                        [self.popoverController presentPopoverFromRect:rect
                                                                inView:cell
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
                    
                    }
                        break;
                        
                        //Customer Order
                    case 7:
                    {
                        
                    }
                        break;
                        
                    default:{
                        //The user is selecting the cell which is currently expanded
                        //we want to minimize it back
                        if(selectedIndex == indexPath.row)
                        {
                            selectedIndex = -1;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            
                            return;
                        }
                        
                        //First we check if a cell is already expanded.
                        //If it is we want to minimize make sure it is reloaded to minimize it back
                        if(selectedIndex >= 0)
                        {
                            NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                            selectedIndex = indexPath.row;
                            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                        }
                        
                        //Finally set the selected index to the new selection and reload it to expand
                        selectedIndex = indexPath.row;
                        selectedSectionIndex = indexPath.section;
                        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        
                        
                    }
                        break;
                }
                
            }
            //check for the row number
            if ([indexPath section] == 3 && [indexPath row] == 7) {
                CustomCellGeneral *cell = (CustomCellGeneral *)[tblHeader cellForRowAtIndexPath:indexPath];
                
                DatePopOverController *dataViewController = [[DatePopOverController alloc] initWithNibName:@"DatePopOverController" bundle:[NSBundle mainBundle]];
                
                dataViewController.contentSizeForViewInPopover =
                CGSizeMake(300, 300);
                dataViewController.isStockPickup = NO;
                dataViewController.isCallDate = NO;


                
                //create a popover controller
                self.popoverController = [[UIPopoverController alloc]
                                          initWithContentViewController:dataViewController];
                
                CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                
                dataViewController.delegate = self;
                [self.popoverController presentPopoverFromRect:rect
                                                        inView:cell
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
            }
            
            if ([indexPath section] == 6 && [indexPath row] == 0) {
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
               detailViewController1 = [[MJDetailViewController alloc] initWithNibName:@"MJDetailViewController" bundle:nil];
                //NSLog(@"Mesage:::::%@",dictProductDetails)
                detailViewController1.strMessage = strCommenttxt;
                
                [self presentPopupViewController:detailViewController1 animationType:3];
                
                return;
                
                
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    
                    return;
                }
                
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
            break;
            
        case TAG_100:{
            
            NSDictionary *dictProductDetails;
            if (isSearch) {
                dictProductDetails = [arrSearchProducts objectAtIndex:[indexPath row]];

            }
            else{
                dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];

            }
            
            
            //Comments
            if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/C"]){
                QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                dataViewController.commentStr = [dictProductDetails objectForKey:@"Description"];
                dataViewController.delegate = self;
                dataViewController.productIndex = [indexPath row];
                dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                
            }
            //Freight
            else if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"])
            {
                if(isEditable)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter the freight value." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                    alert.tag = 911;
                    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
                    UITextField * alertTextField = [alert textFieldAtIndex:0];
                    alertTextField.placeholder = @"Enter the freight value";
                    alertTextField.keyboardType = UIKeyboardTypeNumberPad;
                    [alert show];
                }
            }
            else{
                
                SalesOrderProductDetailsViewController *dataViewController = [[SalesOrderProductDetailsViewController alloc] initWithNibName:@"SalesOrderProductDetailsViewController" bundle:[NSBundle mainBundle]];
                //NSLog(@"Product details : %@",[arrProducts objectAtIndex:[indexPath row]]);
                dataViewController.strDebtor = strDebtor;
                dataViewController.dictProductDetails = (NSMutableDictionary *)dictProductDetails;
                if([dictHeaderDetails objectForKey:@"Price"])
                {
                    float price = [[dictHeaderDetails objectForKey:@"Price"] floatValue];
                    
                    self.strPriceChanged = [NSString stringWithFormat:@"%.2f",price];
                    
                }
                if(isEditable)
                {
                    dataViewController.isEditable = YES;
                }
                else
                {
                    dataViewController.isEditable = NO;
                }
                dataViewController.delegate = self;
                dataViewController.productIndex = [indexPath row];
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                
            }
        }
            break;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (tableView.tag) {
        case TAG_50:
        {
            return 40;
        }
            
        case TAG_100:
        {
            return 0;
        }
        case TAG_9999:
        {
            return 80.0;
        }
        default:return 0;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:
        {
            if (section == 1) {
                return nil;
            }

            if (section == 4) {
                return @"Comments";
            }
        }break;
    }
    // Return the displayed title for the specified section.
    return nil;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
            
        case TAG_100:{
            if (editingStyle == UITableViewCellEditingStyleDelete)
            {
                self.deleteIndexPath = indexPath;
                if (isEditable) {
                    if([[[arrProducts objectAtIndex:deleteIndexPath.row] objectForKey:@"isNewProduct"] isEqualToString:@"1"]){
                        [self doAfterdelete];
                    }
                    else{
                        [self callDeleteProduct];
                    }
                }
                else{
                    [self doAfterdelete];
                }
            }
            
        }break;
    }
}

#pragma mark - Custom methods
-(void)doAfterdelete{
    NSDictionary *dict = [arrProducts objectAtIndex:deleteIndexPath.row];
    [arrProducts removeObjectAtIndex:deleteIndexPath.row];
    [tblDetails deleteRowsAtIndexPaths:[NSArray arrayWithObject:deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tblDetails endUpdates];
    
    if ([dict objectForKey:@"ExtnPrice"]) {
        NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
        totalCost -= [strExtnPrice floatValue];
        totalLines = totalLines - 1;
    }
    lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    
    if ([arrProducts count] == 0) {
        tblDetails.editing = NO;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        segControl.enabled = YES;
        [self showOrEnableButtons];
        [self showPointingAnimation];
    }
    
}

- (void)showPointingAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.autoreverses = YES;
    animation.duration = 0.35;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    [layerPointingImage addAnimation:animation forKey:@"pulseAnimation"];
    [imgArrowDownwards.layer addSublayer:layerPointingImage];
}

-(void)calcFreight{
    
    freightValue = alertFreightValue + [self calcFreightTax];
    
    totalCost += freightValue;
    totalLines = totalLines + 1;
    
    NSString *strFreightValue = [NSString stringWithFormat:@"%f",freightValue];
    NSString *strFreightTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:@"0" forKey:@"Available"];
    [dict setValue:@"Freight" forKey:@"Description"];
    [dict setValue:@"0" forKey:@"Location"];
    [dict setValue:[NSString stringWithFormat:@"%d",alertFreightValue] forKey:@"Price"];
    [dict setValue:strFreightTax forKey:@"Tax"];
    [dict setValue:strFreightValue forKey:@"ExtnPrice"];
    [dict setValue:@"0" forKey:@"ScmRecum"];
    [dict setValue:@"-" forKey:@"StockCode"];
    [dict setValue:@"0" forKey:@"Warehouse"];
    [dict setValue:@"0" forKey:@"CheckFlag"];
    [dict setValue:@"/F" forKey:@"Item"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dict, @"source", nil]];
    
    
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
    
}

-(float)calcFreightTax
{
    return (alertFreightValue*FREIGHT_TAX)/100;
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
    
}


#pragma mark - Alert Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == TAG_50) {
        
        if (isEditable == NO) {
            [self prepareForNewOrder];
        }
        
    }
    
    if (alertView.tag == TAG_1300) {
        [segControl setSelectedIndex:0];
    }
    
    //Error in connection alert
    if (alertView.tag == TAG_200) {
        if (buttonIndex == 1) {
            [self callInsertSalesOrderToDB];
        }
    }
    
    //Error in connection alert
    if (alertView.tag == TAG_300) {
        if (buttonIndex == 1) {
            [self callInsertSalesOrderToDB];
        }
    }
    
    //Remove views
    if (alertView.tag == TAG_400) {
        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected){
            [self callInsertSalesOrderToDB];
        }
        else{
            if (isEditable) {
                [self callWSSaveEditSalesOrder:finalise];
            }
            else{
                [self callInsertSalesOrderToDB];
            }
        }

    }
    
    //Freight Value
    if(alertView.tag == 911)
    {
        if (buttonIndex == 1)
        {
            alertFreightValue = [[[alertView textFieldAtIndex:0] text] intValue];
            
            if(alertFreightValue > 0)
            {
                [self calcFreight];
            }
        }
    }
    
}



#pragma mark -  IBAction
- (IBAction)actionShowProductList:(id)sender{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (IBAction)actionShowComment:(id)sender{
    [self showCommentView];
}

- (IBAction)actionShowFreight:(id)sender{
    [self showFreightView];
}

- (void) switchToggled:(id)sender {
    //a switch was toggled.
    //maybe use it's tag property to figure out which one
    
    UISwitch *swtch = (UISwitch*)sender;
    UIView *view = swtch.superview; //Cell contentView
    CustomCellGeneral *cell = (CustomCellGeneral *)view.superview;
    
    
    switch (swtch.tag) {
        case TAG_200:{
            
            if (swtch.on) {
                
                self.isCashPickUpOn = @"Y";
                CashPickUpPopOverController *popoverContent = [[CashPickUpPopOverController alloc] initWithNibName:@"CashPickUpPopOverController" bundle:[NSBundle mainBundle]];
                
                popoverContent.strCashAmount = strCashPickUp;
                
                //resize the popover view shown
                //in the current view to the view's size
                popoverContent.contentSizeForViewInPopover =
                CGSizeMake(300, 150);
                
                //create a popover controller
                self.popoverController = [[UIPopoverController alloc]
                                          initWithContentViewController:popoverContent];
                
                CGRect rect = CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+25, 0, 0);
                
                popoverContent.delegate = self;
                [self.popoverController presentPopoverFromRect:rect
                                                        inView:cell
                                      permittedArrowDirections:UIPopoverArrowDirectionAny
                                                      animated:YES];
                
            }
            else{
                self.strCashPickUp = @"0";
                self.isCashPickUpOn = @"N";
                
                if (self.popoverController) {
                    [self.popoverController dismissPopoverAnimated:YES];
                    self.popoverController = nil;
                }
                
                [tblHeader reloadData];
            }
        }
            break;
            
        case TAG_300:
        {
            if (swtch.on) {
                isStockPickUpOn = @"Y";
                StorePickupViewController *obj = [[StorePickupViewController alloc] init];
                
                obj.delegate = self;
                
                UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:obj];
                nvc.navigationBarHidden = YES;
                
                
                obj.modalPresentationStyle = UIModalPresentationFullScreen;
                
                // Depricated
                [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
                
            }
            else{
                isStockPickUpOn = @"N";
            }
            
        }
            break;
            
        case TAG_400:
            NSLog(@"400");
            
            if (swtch.on) {
                isFuelLevySelected = @"Y";
            }else{
                isFuelLevySelected = @"N";
            }
            
            break;
            
        case TAG_111:{
            if (swtch.on) {
                isPickupFormOn = @"Y";
                StockPickupViewController *obj = [[StockPickupViewController alloc] init];
                obj.delegate = self;
                
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:(strDebtor)?strDebtor:@"" forKey:@"debtor_number"];
                [dict setObject:(strDelName)?strDelName:@"" forKey:@"debtor_name"];
                [dict setObject:(strDateRaised)?strDateRaised:@"" forKey:@"date_raised"];
                
                obj.debtorDetailDict = dict;
                
                UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:obj];
                nvc.navigationBarHidden = YES;
                obj.modalPresentationStyle = UIModalPresentationFullScreen;
                
                // Depricated
                [[AppDelegate getAppDelegateObj].rootViewController presentViewController:nvc animated:YES completion:Nil];
            }
            else{
                isPickupFormOn = @"N";
            }
        }
            break;
            

        default:
            break;
    }
}

- (IBAction)actionDisplayQuoteMenu:(id)sender{
    
    //tagFlsh = 1;
    //[self flashFinal:btnAddMenu];
    
    QuoteMenuPopOverViewController *popoverContent = [[QuoteMenuPopOverViewController alloc] initWithNibName:@"QuoteMenuPopOverViewController" bundle:[NSBundle mainBundle]];
    /*
     //Finalise option not needed
     if (isEditable) {
     popoverContent.isFinalisedNeeded = YES;
     }
     else{
     popoverContent.isFinalisedNeeded = NO;
     }*/
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    
    popoverController.delegate = self;
    popoverContent.delegate = self;
    popoverContent.isProfileOrderEntry = NO;
    popoverContent.isFinalisedNeeded = NO;
    
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_x_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect rectBtnMenu = CGRectMake(btnAddMenu.frame.origin.x, self.view.frame.size.height - 40,btnAddMenu.frame.size.width,btnAddMenu.frame.size.height);
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:rectBtnMenu
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}

//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([tblDetails isEditing]) {
        segControl.enabled = YES;
        [self showOrEnableButtons];
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblDetails setEditing:NO animated:YES];
    }
    else{
        segControl.enabled = NO;
        [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblDetails setEditing:YES animated:YES];
    }

}

- (void)setDeliveryDate:(NSString *)strDeliveryDate{
    self.strDelDate = strDeliveryDate;
   strCopyDelDate = [self.strDelDate copy];
    [tblHeader reloadData];
}



#pragma mark - Custom Methods

- (void)hideOrDisableButtons{
    btnAddMenu.enabled = FALSE;
}
- (void)showOrEnableButtons{
    btnAddMenu.enabled = TRUE;
}

- (void)setDefaults{
    
    self.isCashPickUpOn = @"N";
    self.strCashPickUp = @"0";
    self.strChequeAvailable = @"N";
    self.isStockPickUpOn = @"N";
    self.isPickupFormOn = @"N";
    self.strStockPickup = @"N";
    self.isFuelLevySelected = @"Y";
    self.strExchangeRate = @"1.000000";
    self.strActive = @"N";
    self.strEditStatus = @"11";
    self.strStatus = STATUS;
    self.strDecimalPlaces = @"2";
    self.strTotalCartonQty = self.strBOQty = self.strOrigCost = @"0.0000";
    self.strTotalLines = self.strTotalTax = self.strTotalVolume = self.strTotalWeight = @"0";
    
    isDeliveryRunSlected = NO;
    segControl.enabled = NO;
    
    totalCost = 0;
    totalLines = 0;
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
    
}


-(void)prepareForNewOrder{
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    [arrProducts removeAllObjects];
    [dictHeaderDetails removeAllObjects];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwHeader];
    
    [self setDefaults];
    
    self.strDelName = nil;
    self.strDelAddress1 = nil;
    self.strDelAddress2 = nil;
    self.strDelAddress3 = nil;
    self.strDelSubUrb = nil;
    self.strDelPostCode = nil;
    self.strDelCountry = nil;
    self.strDeliveryRun = nil;
    self.strDebtor = nil;
    self.strDelInst1 = nil;
    self.strDelInst2 = nil;
    self.strCustOrder = nil;
    self.strContact = nil;
    self.strDelDate = nil;
    self.strAddress1 = nil;
    self.strAddress2 = nil;
    self.strAddress3 = nil;
    self.strSubUrb = nil;
    self.strAllowPartial = nil;
    self.strBranch = nil;
    self.strCarrier = nil;
    self.strChargeRate = nil;
    self.strChargetype = nil;
    self.strCountry = nil;
    self.strDirect = nil;
    self.strDropSequence = nil;
    self.strExchangeCode = nil;
    self.strExportDebtor = nil;
    self.strHeld = nil;
    self.strName = nil;
    self.strNumboxes = nil;
    self.strOrderDate = nil;
    self.strPeriodRaised = nil;
    self.strPostCode = nil;
    self.strPriceCode = nil;
    self.strSalesBranch = nil;
    self.strSalesman = nil;
    self.strTaxExemption1 = nil;
    self.strTaxExemption2 = nil;
    self.strTaxExemption3 = nil;
    self.strTaxExemption4 = nil;
    self.strTaxExemption5 = nil;
    self.strTaxExemption6 = nil;
    self.strTradingTerms = nil;
    self.strYearRaised = nil;
    self.strUploadDate = nil;
    self.strSuccessMessage = nil;
    self.strCommenttxt = nil;
    self.strCommentDateCreated = nil;
    self.strCommentFollowDate = nil;
    
    
    //Change the selection
    [segControl setSelectedIndex:0];
    
    segControl.enabled = NO;
    [tblHeader reloadData];
}


-(void)prepareHeaderData{
    
    //Debtor's Info
    
    self.strDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"];
    self.strAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address1"];
    self.strAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address2"];
    self.strAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Address3"];
    self.strSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Suburb"];
    self.strAllowPartial = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"AllowPartial"];
    self.strBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Branch"];
    self.strCarrier = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Carrier"];
    self.strContact = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Contact"];
    self.strChargeRate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ChargeRate"];
    self.strChargetype = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Chargetype"];
    self.strCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Country"];
    self.strDirect = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Direct"];
    self.strDropSequence = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DropSequence"];
    self.strExchangeCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExchangeCode"];
    self.strExportDebtor = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"ExportDebtor"];
    self.strHeld = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Held"];
    self.strName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Name"];
    self.strNumboxes = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Numboxes"];
    self.strOrderDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderDate"];
    self.strPeriodRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PeriodRaised"];
    self.strPriceCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PriceCode"];
    self.strSalesBranch = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"SalesBranch"];
    self.strSalesman = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Salesman"];
    self.strTaxExemption1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption1"];
    self.strTaxExemption2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption2"];
    self.strTaxExemption3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption3"];
    self.strTaxExemption4 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption4"];
    self.strTaxExemption5 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption5"];
    self.strTaxExemption6 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TaxExemption6"];
    self.strTradingTerms = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"TradingTerms"];
    self.strYearRaised = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"YearRaised"];
    self.strDelDate = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryDate"];
    self.strDelAddress1 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress1"];
    self.strDelAddress2 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress2"];
    self.strDelAddress3 = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelAddress3"];
    self.strDelName = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelName"];
    self.strDelSubUrb = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelSuburb"];
    self.strDelPostCode = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelPostCode"];
    self.strDelCountry = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelCountry"];
    
    self.strBalanceVal = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Balance"];

    
    self.strDelInst1 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"]) {
        self.strDelInst1 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst1"];
    }
    
    self.strDelInst2 = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"]) {
        self.strDelInst2 = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"DelInst2"];
    }
    
    self.strCustOrder = @"";
    if ([[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"]) {
        self.strCustOrder = [[dictHeaderDetails objectForKey:@"Header"] objectForKey:@"CustOrderno"];
    }
    
    self.strCommenttxt = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Commenttxt"];
    self.strCommentDateCreated = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"DateCreated"];
    self.strCommentFollowDate = [[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"FollowDate"];
    
    
    if (isEditable) {
        if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]){
            isStockPickUpOn = @"Y";
            self.strStockPickup = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"StockPickUp"]];
        }
        else{
            self.strStockPickup = @"N";
            isStockPickUpOn = @"N";
        }
    }
    
    if (isEditable) {
        if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PickUpForm"]){
            isPickupFormOn = @"Y";
            self.strPickupForm = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"PickUpForm"]];
        }
        else{
            self.strPickupForm = @"N";
            isPickupFormOn = @"N";
        }
    }
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"]) {
        self.strDeliveryRun = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DeliveryRun"];
        isDeliveryRunSlected = YES;
    }
    else{
        isDeliveryRunSlected = NO;
    }
    
    if (isEditable) {
        if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashCollect"] isEqualToString:@"1"]) {
            isCashPickUpOn = @"Y";
            
            self.strCashPickUp = [[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CashAmount"];
        }
        else{
            isCashPickUpOn = @"N";
            self.strCashPickUp = @"0";
        }
        
    }
    
    
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    btnDelete.hidden = YES;
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
        }
            break;
            
        case 1:{
            
            if (isDeliveryRunSlected) {
                if ([arrProducts count]) {
                    
                    btnDelete.hidden = NO;
                    tblDetails.editing = NO;
                    [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
                    [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
                    [vwContentSuperview addSubview:vwDetails];
                }
                else{
                    
                    //No products
                    [vwContentSuperview addSubview:vwNoProducts];
                    [self showPointingAnimation];
                }
            }
            else{
                
                [vwContentSuperview addSubview:vwHeader];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select delivery run!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                alert.tag = TAG_1300;
                [alert show];
            }
            
        }
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark - Database Calls


-(void)insertStockPickupToDB{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            BOOL y2;
            
            NSString *orderNo = @"";
            NSString *requestedBy = @"";

            NSString *Pickup_no = @"";
            NSString *today_date = @"";
            NSString *debtor = @"";
            NSString *debtor_name = @"";
            NSString *contact_phone_no = @"";
            NSString *contact_acc_no = @"";
            NSString *related_invoice = @"";
            NSString *product_code = @"";
            NSString *batch_no= @"";
            NSString *best_before_date= @"";
            NSString *email_to= @"";
            NSString *email_cc= @"";
            
            NSString *reasonReturn= @"";
            NSString *detailReasonReturn= @"";
            
            NSString *quantity = @"";
            NSString *returnVal = @"";
            
            NSString *batch_name= @"";
            NSString *product_name= @"";
            
            NSData *image_attched1= NULL;
            NSData *image_attched2= NULL;
            NSData *image_attched3= NULL;
            NSData *image_attched4= NULL;
            NSData *image_attched5= NULL;
            NSData *image_attched6= NULL;
            
            
            // Set PickUp form details
            if([dictHeaderDetails objectForKey:@"PickUpForm"])
            {
                self.strPickupForm = [dictHeaderDetails objectForKey:@"PickUpForm"];
                NSArray *stockPickupArr= [self.strPickupForm componentsSeparatedByString:@"|"];
                NSLog(@"%@",stockPickupArr);
                
                orderNo = strOrderNum;
                Pickup_no = [dictStockPickup objectForKey:@"pickup_no"];
                requestedBy = [dictStockPickup objectForKey:@"requested_by"];
                
                NSDate *todaysDate = [NSDate date];
                NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
                today_date = [dateFormat2 stringFromDate:todaysDate];
                
                debtor = [dictStockPickup objectForKey:@"debtor"];
                debtor_name =[dictStockPickup objectForKey:@"debtor_name"];
                contact_phone_no = [dictStockPickup objectForKey:@"Contact_Phone"];
                contact_acc_no = [dictStockPickup objectForKey:@"Contact_Person"];
                related_invoice =[dictStockPickup objectForKey:@"RelatedInvoice"];
                product_code = [dictStockPickup objectForKey:@"product_code"];
                batch_no= [dictStockPickup objectForKey:@"BatchNo"];
                best_before_date= [dictStockPickup objectForKey:@"BestBeforeDate"];
                email_to= [dictStockPickup objectForKey:@"EmailRecipient"];
                email_cc= [dictStockPickup objectForKey:@"EmailCC"];
                
                reasonReturn=[dictStockPickup objectForKey:@"ReasonReturn"];
                detailReasonReturn= [dictStockPickup objectForKey:@"detailReasonReturn"];
                
                returnVal = [dictStockPickup objectForKey:@"Return"];
                quantity = [dictStockPickup objectForKey:@"Quantity"];
                
                product_name = [dictStockPickup objectForKey:@"description"];
                batch_name = [dictStockPickup objectForKey:@"BatchDetail"];
                
                image_attched1= [dictStockPickup objectForKey:@"image0"];
                image_attched2=[dictStockPickup objectForKey:@"image1"];
                image_attched3= [dictStockPickup objectForKey:@"image2"];
                image_attched4= [dictStockPickup objectForKey:@"image3"];
                image_attched5= [dictStockPickup objectForKey:@"image4"];
                image_attched6= [dictStockPickup objectForKey:@"image5"];
                
            }
            
            //    BATCH_NAME varchar,PRODUCT_NAME varchar)

            
       y2 = [db executeUpdate:@"INSERT INTO `StockPickup` (ORDER_NO,PICKUP_NO,TODAY_DATE,REQUESTED_BY,DEBTOR,DEBTOR_NAME,CONTACT_PHONE_NUMBER,CONTACT_ACCOUNT_NO,RELATED_INVOICE_NO,PRODUCT_CODE,BATCH_NUMBER,BEST_BEFORE_DATE,EMAIL_TO,EMAIL_CC,IMAGE_ATTACHED1,IMAGE_ATTACHED2,IMAGE_ATTACHED3,IMAGE_ATTACHED4,IMAGE_ATTACHED5,IMAGE_ATTACHED6,REASON_OF_RETURN,DETAIL_REASON_RETURN,QUANTITY,RETURN,BATCH_NAME,PRODUCT_NAME) VALUES (?, ?, ?, ?, ?,?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?)", orderNo,Pickup_no, today_date,requestedBy, debtor,debtor_name,contact_phone_no,contact_acc_no,related_invoice,product_code,batch_no,best_before_date,email_to,email_cc,image_attched1,image_attched2,image_attched3,image_attched4,image_attched5,image_attched6,reasonReturn,detailReasonReturn,quantity,returnVal,batch_name,product_name];
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                NSLog(@"%@",strErrorMessage);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            });
            
            
        }
        
    }];
}

-(void)callGetNewOrderNumberFromDB{
    
    strWebserviceType = @"DB_GET_ORDER_NUMBER";
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs;
        @try {
            
            
            rs = [db executeQuery:@"SELECT last_order_num FROM (SELECT ORDER_NO as last_order_num FROM soheader UNION SELECT ORDER_NO as last_order_num FROM soheader_offline)ORDER BY last_order_num DESC LIMIT 1"];
            
            //rs = [db executeQuery:@"SELECT MAX(last_order_num) as lastOrderNum FROM (SELECT CAST(ORDER_NO AS INT) as last_order_num FROM soheader UNION SELECT CAST(ORDER_NO AS INT) as last_order_num FROM soheader_offline)"];
            
            if (!rs)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                
                NSString *strLastOrderNum = [[rs resultDictionary] objectForKey:@"last_order_num"];
                int numLastOrderNum = [strLastOrderNum integerValue];
                
                int numNewOrderNum  = ++numLastOrderNum;
                self.strOrderNum = [NSString stringWithFormat:@"%d",numNewOrderNum];
                
                [dictHeaderDetails setValue:strOrderNum forKey:@"OrderNum"];
                
                [tblHeader reloadData];
                
            }
            
            [dictHeaderDetails setValue:strOrderNum forKey:@"OrderNum"];
            
            [tblHeader reloadData];
            
            
            [rs close];
        }
        @catch (NSException* e) {
            [rs close];
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            
        }
        
    }];
}


-(void)callInsertSalesOrderToDB{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        BOOL isSomethingWrongHappened = FALSE;
        //        @try
        //        {
        BOOL y2;
        
        self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
        self.strTotalLines= [NSString stringWithFormat:@"%d",[arrProducts count]];
        
        // 30thOct
        self.strUploadDate = self.strDateRaised;
        
        //Insert
        if (!isEditable) {
            int len = [strSalesType length];
            
            FMResultSet *rs = [db executeQuery:@"SELECT MAX(last_order_num) as lastOrderNum FROM (SELECT CAST(substr(ORDER_NO,?) AS INT) as last_order_num FROM soheader_offline WHERE ORDER_NO LIKE ?)",[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", strSalesType]];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            int numNewOrderNum;
            if ([rs next]) {
                
                
                if ([[[rs resultDictionary] objectForKey:@"lastOrderNum"] isEqual:[NSNull null]]) {
                    numNewOrderNum = 1;
                }
                else{
                    int numLastOrdNum = [[[rs resultDictionary] objectForKey:@"lastOrderNum"] intValue];
                    numNewOrderNum  = ++numLastOrdNum;
                }
                
                
            }
            //No record
            else{
                numNewOrderNum = 1;
            }
            
            [rs close];
            
            self.strOrderNum = [NSString stringWithFormat:@"%@%d",strSalesType,numNewOrderNum];
            
            bool Flag = NO;
            NSString *strINC = @"N";
            
            //--Check for Product with Price
            for (NSDictionary *dict in arrProducts) {
                
                if([dict objectForKey:@"PriceChnaged"])
                {
                    if(Flag == NO)
                    {
                        if([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
                        {
                            Flag = YES;
                            strINC = [dict objectForKey:@"PriceChnaged"];
                            self.strPriceChanged =[dict objectForKey:@"PriceChnaged"];
                        }
                        
                    }
                }
            }
            //            PICKUP_INFO
            //            strPickupInfo
            // Check for StockPickup form value
            NSString *pickup_num;
            if (dictStockPickup.count > 0) {
                pickup_num = [dictStockPickup objectForKey:@"pickup_no"];
                
            }
            else{
                pickup_num  = @"";
                self.strPickupInfo = @"N";
                
            }
            
            y2 = [db executeUpdate:@"INSERT INTO `soheader_offline` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS,PICKUP_INFO,PERIOD_RAISED,YEAR_RAISED,ORDER_VALUE,DETAIL_LINES, isUploadedToServer,isProfileOrder,isEdited,TOTL_CARTON_QTY,PriceChnaged,PICKUP_NO) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strOperator,strOperator,strStockPickup,strPickupInfo,strPeriodRaised,strPeriodRaised,strTotalCost,strTotalLines,[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],self.strTotalCartonQty,strPriceChanged,pickup_num];
        }
        else{
            
            //Changed soheader_offline to soheader
            FMResultSet *rs = [db executeQuery:@"SELECT ORDER_NO FROM soheader WHERE ORDER_NO = ?",strOrderNum];
            
            
            if (!rs) {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            BOOL y2;
            
            
            if([rs next]){//Changed soheader_offline to soheader
                //added 1 field named as isUploadedToServer
                y2 = [db executeUpdate:@"UPDATE `soheader` SET DEBTOR = ?,DEL_NAME = ?,DEL_ADDRESS1= ?,DEL_ADDRESS2= ?,DEL_ADDRESS3= ?,DEL_SUBURB= ?,DEL_POST_CODE= ?,DEL_COUNTRY= ?,CONTACT= ?,DEL_DATE= ?,DATE_RAISED= ?,CUST_ORDER= ?,DEL_INST1= ?,DEL_INST2= ?, STATUS= ?, DIRECT= ?, WAREHOUSE= ?, BRANCH= ?, PRICE_CODE= ?, SALESMAN= ?, SALES_BRANCH= ?, TRADING_TERMS= ?, DELIVERY_RUN= ?, TAX_EXEMPTION1= ?, TAX_EXEMPTION2= ?, TAX_EXEMPTION3= ?,TAX_EXEMPTION4= ?,TAX_EXEMPTION5= ?,TAX_EXEMPTION6= ?,EXCHANGE_CODE= ?,ALLOW_PARTIAL= ?,CHARGE_TYPE= ?,CARRIER_CODE= ?,EXPORT_DEBTOR= ?,DROP_SEQ= ?,HELD= ?,CHARGE_RATE= ?,NUM_BOXES= ?,CASH_COLLECT= ?,CASH_AMOUNT= ?,CASH_REP_PICKUP= ?,OPERATOR= ?,STOCK_RETURNS= ?, PERIOD_RAISED= ?,YEAR_RAISED= ?,ORDER_VALUE= ?,DETAIL_LINES= ?, isUploadedToServer= ?,TOTL_CARTON_QTY = ? WHERE ORDER_NO = ?",strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strOperator,strStockPickup,strPeriodRaised,strPeriodRaised,strTotalCost,strTotalLines,self.strTotalCartonQty,[NSNumber numberWithInt:0],strOrderNum];
            }
            else{//Changed soheader_offline to soheader
                //added 1 field named as isUploadedToServer
                y2 = [db executeUpdate:@"INSERT INTO `soheader` (ORDER_NO,DEBTOR,DEL_NAME,DEL_ADDRESS1,DEL_ADDRESS2,DEL_ADDRESS3,DEL_SUBURB,DEL_POST_CODE,DEL_COUNTRY,CONTACT,DEL_DATE,DATE_RAISED,CUST_ORDER,DEL_INST1,DEL_INST2, STATUS, DIRECT, WAREHOUSE, BRANCH, PRICE_CODE, SALESMAN, SALES_BRANCH, TRADING_TERMS, DELIVERY_RUN, TAX_EXEMPTION1, TAX_EXEMPTION2, TAX_EXEMPTION3,TAX_EXEMPTION4,TAX_EXEMPTION5,TAX_EXEMPTION6,EXCHANGE_CODE,ALLOW_PARTIAL,CHARGE_TYPE,CARRIER_CODE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,CASH_COLLECT,CASH_AMOUNT,CASH_REP_PICKUP,OPERATOR,CREATED_BY,STOCK_RETURNS, PERIOD_RAISED,YEAR_RAISED,ORDER_VALUE,DETAIL_LINES, isUploadedToServer,TOTL_CARTON_QTY,) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?,?,?,?)", strOrderNum,strDebtor, strDelName, strDelAddress1,strDelAddress2,strDelAddress3,strDelSubUrb,strDelPostCode,strDelCountry,strContact,strDelDate,strDateRaised,strCustOrder,strDelInst1,strDelInst2,strStatus,strDirect,strWarehouse,strBranch,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strDeliveryRun,strTaxExemption1,strTaxExemption2,strTaxExemption3,strTaxExemption4,strTaxExemption5,strTaxExemption6,strExchangeCode,strAllowPartial,strChargetype,strCarrier,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,isCashPickUpOn,strCashPickUp,strChequeAvailable,strOperator,strOperator,strStockPickup,strPeriodRaised,strPeriodRaised,strTotalCost,strTotalLines,[NSNumber numberWithInt:0],self.strTotalCartonQty];
                
            }
            
        }
        
        if (!y2)
        {
            isSomethingWrongHappened = TRUE;
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        NSArray *components = [strUploadDate componentsSeparatedByString:@" "];
        NSString *strCreatedTime = (NSString *)[components objectAtIndex:1];
        NSString *strCreatedDate = (NSString *)[components objectAtIndex:0];
        
        float totalWeight = 0;
        float totalVolume = 0 ;
        float totalTax = 0;
        int totalQuantity = 0;
        
        int i = 0;
        for (NSMutableDictionary *dictProductDetails in arrProducts) {
            
            NSString *strStockCode1 = nil;
            NSString *strPrice = @"0";
            NSString *strDissection = @"0";
            NSString *strDissection_Cos = @"0";
            NSString *strWeight = @"0";
            NSString *strVolume = @"0";
            NSString *strCost = @"0";
            NSString *strExtension = @"0";
            NSString *strGross = @"0";
            NSString *strTax = @"0";
            NSString *strQuantityOrdered = @"0";
            
            NSString *strDescription = @"";
            NSString *strAltQuantity = @"0";
            NSString *strCUST_PRICE = @"0";
            NSString *strCUST_ORD_QTY  = @"0";
            NSString *strCUST_SHIP_QTY = @"0";
            NSString *strCUST_VOLUME = @"0";
            NSString *strPRICING_UNIT = @"0";
            NSString *strCharge_Type = @"";
            NSString *strRelease_New = @"";
            
            self.strConvFactor = @"1.0000";
            NSLog(@"%@",self.strConvFactor);
            
            //Comments
            if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/C"])
            {
                
                i++;
                [dictProductDetails setValue:[NSString stringWithFormat:@"%d",i] forKey:@"LINE_NO"];
                
                strStockCode1 = @"/C";
                
                //To Be add in insert Query
                strDescription = [[dictProductDetails objectForKey:@"Description"] trimSpaces];
                self.strDecimalPlaces = @"0";
                strQuantityOrdered = @"0.00";
                self.strConvFactor = @"0.0000";
                strCharge_Type = @"";
                strRelease_New = @"Y";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                
                self.strRefreshPrice = @"";
                
                //Insert
                NSString *strLines = [NSString stringWithFormat:@"%d",i];
                BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                
                if (!y1)
                {
                    isSomethingWrongHappened = TRUE;
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    break;
                }
                
                
                
            }
            
            //Freight
            else  if ([dictProductDetails objectForKey:@"Item"] && [[dictProductDetails objectForKey:@"Item"] isEqualToString:@"/F"]){
                
                i++;
                [dictProductDetails setValue:[NSString stringWithFormat:@"%d",i] forKey:@"LINE_NO"];
                
                strStockCode1 = @"/F";
                
                if(alertFreightValue > 0)
                {
                    strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
                    strExtension = [NSString stringWithFormat:@"%f",freightValue];
                    strTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
                    totalTax += [strTax floatValue];
                }
                else
                {
                    strGross = [dictProductDetails objectForKey:@"Gross"];
                    strCost = [dictProductDetails objectForKey:@"Cost"];
                    strPrice = [dictProductDetails objectForKey:@"Price"];
                    strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                    strTax = [dictProductDetails objectForKey:@"Tax"];
                    totalTax += [strTax floatValue];
                }
                
                
                //                    strPrice = [dictProductDetails objectForKey:@"Price"];
                //                    strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                //                    strTax = [dictProductDetails objectForKey:@"Tax"];
                
                
                //To Be add in insert Query
                self.strDecimalPlaces = @"0";
                strQuantityOrdered = @"0.00";
                self.strConvFactor = @"0.0000";
                
                strCUST_PRICE = strPrice;
                strCUST_ORD_QTY  = strQuantityOrdered;
                strCUST_SHIP_QTY = strQuantityOrdered;
                strCUST_VOLUME = @"1";
                strPRICING_UNIT = @"1";
                strCharge_Type = @"N";
                strRelease_New = @"Y";
                
                strDissection = @"/F";
                strDissection_Cos = @"";
                self.strRefreshPrice = @"";
                
                
                strAltQuantity = @"0.00";
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                
                
                //Insert
                NSString *strLines = [NSString stringWithFormat:@"%d",i];
                BOOL y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?, ?, ?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,strDecimalPlaces,strLines,[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                
                if (!y1)
                {
                    isSomethingWrongHappened = TRUE;
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    break;
                }
                
                
            }
            //Product
            else if ([dictProductDetails objectForKey:@"ExtnPrice"]){
                
                if ([dictProductDetails objectForKey:@"Item"]) {
                    strStockCode1 = [dictProductDetails objectForKey:@"Item"];
                }
                else if ([dictProductDetails objectForKey:@"StockCode"]){
                    strStockCode1 = [dictProductDetails objectForKey:@"StockCode"];
                }
                
                strPrice = [dictProductDetails objectForKey:@"Price"];
                strDissection = [dictProductDetails objectForKey:@"Dissection"];
                strDissection_Cos = [dictProductDetails objectForKey:@"Dissection_Cos"];
                strWeight = [dictProductDetails objectForKey:@"Weight"];
                strVolume = [dictProductDetails objectForKey:@"Volume"];
                strCost = [dictProductDetails objectForKey:@"Cost"];
                strExtension = [dictProductDetails objectForKey:@"ExtnPrice"];
                //--check for price is greater than zero
                if(strExtension.integerValue >= 0)
                {
                    strQuantityOrdered = [dictProductDetails objectForKey:@"QuantityOrdered"];
                    
                    strAltQuantity = strQuantityOrdered;
                    
                    totalQuantity += [strQuantityOrdered intValue];
                    
                    strWeight = [dictProductDetails objectForKey:@"Weight"];
                    
                    strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                    
                    totalWeight += [strWeight floatValue];
                    
                    strVolume = [dictProductDetails objectForKey:@"Volume"];
                    
                    strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                    
                    totalVolume += [strVolume floatValue];
                    
                    strGross = [dictProductDetails objectForKey:@"Gross"];
                    strTax = [dictProductDetails objectForKey:@"Tax"];
                    
                    totalTax += [strTax floatValue];
                    
                    //To Be add in insert Query
                    self.strDecimalPlaces = @"2";
                    strCUST_PRICE = strPrice;
                    strCUST_ORD_QTY  = strQuantityOrdered;
                    strCUST_SHIP_QTY = strQuantityOrdered;
                    strCUST_VOLUME = @"1";
                    strPRICING_UNIT = @"1";
                    strCharge_Type = @"";
                    strRelease_New = @"Y";
                    self.strRefreshPrice = @"N";
                    self.strConvFactor = @"1.0000";
                    NSLog(@"%@",self.strConvFactor);
                }
                //                } jdhgsaj
                else{
                    NSLog(@"");
                }
                
                NSLog(@"  CHECK CONDITION ===== %@" ,([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)? @"true": @"false");
                NSLog(@"  CHECK CONDITION ===== %@" ,(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)? @"true": @"false");
                
                
                self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
                self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
                self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
                self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
                
                // 29th Nov -> Removed offline condition
                
                NSString *strINC = @"N";
                BOOL Flag = NO;
                
                if([dictProductDetails objectForKey:@"PriceChnaged"])
                {
                    if(Flag == NO)
                    {
                        if([dictProductDetails objectForKey:@"PriceChnaged"])
                        {
                            Flag = YES;
                            strINC = [dictProductDetails objectForKey:@"PriceChnaged"];
                        }
                        
                    }
                }
                
                
                //Insert
                BOOL y1;
                if (!isEditable) {
                    //if ([dictProductDetails objectForKey:@"ExtnPrice"])
                    // {
                    
                    i++;
                    [dictProductDetails setValue:[NSString stringWithFormat:@"%d",i] forKey:@"LINE_NO"];
                    // }
                    
                    y1 = [db executeUpdate:@"INSERT INTO `sodetail_offline` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO, isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE_New,PriceChnaged) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode1,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,self.strDecimalPlaces,[dictProductDetails objectForKey:@"LINE_NO"],[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New,strINC];
                    //  }
                    
                }
                else{
                    
                    //Changed sodetail_offline to sodetail
                    FMResultSet *rs = [db executeQuery:@"SELECT ITEM FROM sodetail WHERE ORDER_NO = ? AND ITEM = ?",strOrderNum,strStockCode];
                    
                    
                    if (!rs) {
                        [rs close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    }
                    
                    
                    if ([rs next]) {//Changed sodetail_offline to sodetail
                        if ([dictProductDetails objectForKey:@"ExtnPrice"])
                        {
                            y1 = [db executeUpdate:@"UPDATE `sodetail` SET ORDER_NO = ?, DEBTOR = ?, DELIVERY_DATE = ?, STATUS = ?, WAREHOUSE = ?, BRANCH = ?, PRICE_CODE = ?, CHARGE_TYPE = ?, ITEM = ?, DESCRIPTION = ?, PRICE = ?, EXTENSION = ?, QUANTITY = ?, ALT_QTY = ?, ORIG_ORD_QTY = ?, ORD_QTY = ?, BO_QTY = ?, CONV_FACTOR = ?, REFRESH_PRICE = ?, DISSECTION = ?, DISSECTION_COS = ?, WEIGHT = ?, VOLUME = ?, COST = ?, ORIG_COST = ?, GROSS = ?, TAX = ?, CREATE_OPERATOR = ?, CREATE_DATE = ?, CREATE_TIME = ?,EDIT_STATUS = ?,DECIMAL_PLACES = ?,LINE_NO = ?,isUploadedToServer = ?,CUST_PRICE = ?,CUST_ORD_QTY = ?,CUST_SHIP_QTY = ? WHERE ORDER_NO = ? AND ITEM = ?", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,self.strDecimalPlaces,[dictProductDetails objectForKey:@"LINE_NO"],[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strOrderNum,strStockCode];
                        }
                    }
                    else{
                        if ([dictProductDetails objectForKey:@"ExtnPrice"])
                        {
                            
                            NSString *sq = [NSString stringWithFormat:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO,isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES ('%@','%@', '%@', '%@', '%@','%@', '%@','%@','%@','%@','%@','%@', '%@','%@','%@','%@','%@', '%@', '%@','%@','%@', '%@', '%@', '%@', '%@','%@', '%@', '%@','%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,self.strDecimalPlaces,[dictProductDetails objectForKey:@"LINE_NO"],[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                            
                            NSLog(@"sq::::::::::%@",sq);
                            
                            
                            y1 = [db executeUpdate:@"INSERT INTO `sodetail` (ORDER_NO, DEBTOR, DELIVERY_DATE, STATUS, WAREHOUSE, BRANCH, PRICE_CODE, CHARGE_TYPE, ITEM, DESCRIPTION, PRICE, EXTENSION, QUANTITY, ALT_QTY, ORIG_ORD_QTY, ORD_QTY, BO_QTY, CONV_FACTOR, REFRESH_PRICE, DISSECTION, DISSECTION_COS, WEIGHT, VOLUME, COST, ORIG_COST, GROSS, TAX, CREATE_OPERATOR, CREATE_DATE, CREATE_TIME,EDIT_STATUS,DECIMAL_PLACES,LINE_NO,isUploadedToServer,CUST_PRICE,CUST_ORD_QTY,CUST_SHIP_QTY,CUST_VOLUME,PRICING_UNIT,RELEASE) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)", strOrderNum,strDebtor,strDelDate,strStatus, strWarehouse, strBranch,strPriceCode,strCharge_Type,strStockCode,[dictProductDetails objectForKey:@"Description"],strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strSalesman,strCreatedDate,strCreatedTime,strEditStatus,self.strDecimalPlaces,[dictProductDetails objectForKey:@"LINE_NO"],[NSNumber numberWithInt:0],strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strRelease_New];
                        }
                    }
                    
                    [rs close];
                }
                
                if (!y1)
                {
                    isSomethingWrongHappened = TRUE;
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    NSLog(@"strErrorMessage %@",strErrorMessage);
                    break;
                }
                
                
            }
            
        }
        
        BOOL x;
        x = [db executeUpdate:@"UPDATE `soheader` SET TOTL_CARTON_QTY = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ? WHERE ORDER_NO = ?",strTotalCartonQty,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strOrderNum];
        self.strSuccessMessage = [NSString stringWithFormat:@"Order Successfully Added!\nYou can view the order under Offline Order --> Sales Order section"];
        
        
        if (!x)
        {
            isSomethingWrongHappened = TRUE;
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            
        }
        
        *rollback = NO;
        
        // Update the view is profile order.
        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isSaleOrderDetailCheck"];
        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForSale"];
        [[NSUserDefaults standardUserDefaults]synchronize];

        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            //Prepare for new Order
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = TAG_50;
            [alert show];
            
        });
    }];
}

-(void)callGetSalesOrderDetailsFromDB
{
    strWebserviceType = @"DB_SALES_ORDER_GET_DETAILS";
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db) {
        @try
        {
            NSLog(@"strOrderNum %@",strOrderNum);
            
            FMResultSet *rs1  = [db executeQuery:@"SELECT ORDER_NO as OrderNum ,DEBTOR as Debtor,DEL_NAME as DelName,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3,DEL_SUBURB as DelSuburb,DEL_POST_CODE as DelPostCode,DEL_COUNTRY as DelCountry,CONTACT as Contact ,DEL_DATE as DeliveryDate,DATE_RAISED as PeriodRaised,CUST_ORDER as CustOrderno ,DEL_INST1 as DelInst1 ,DEL_INST2 as DelInst2, STATUS as Status, DIRECT as Direct, WAREHOUSE as Warehouse, BRANCH as Branch, PRICE_CODE as PriceCode, SALESMAN as Salesman, SALES_BRANCH as SalesBranch, TRADING_TERMS as TradingTerms, DELIVERY_RUN as DeliveryRun, TAX_EXEMPTION1 as TaxExemption1, TAX_EXEMPTION2 as TaxExemption2, TAX_EXEMPTION3 as TaxExemption3,TAX_EXEMPTION4 as  TaxExemption4,TAX_EXEMPTION5 as TaxExemption5 ,TAX_EXEMPTION6 as TaxExemption6,EXCHANGE_CODE as ExchangeCode,ALLOW_PARTIAL as AllowPartial,CHARGE_TYPE as Chargetype,CARRIER_CODE as Carrier,EXPORT_DEBTOR as ExportDebtor,DROP_SEQ as DropSequence,HELD as Held,CHARGE_RATE as ChargeRate,NUM_BOXES as Numboxes,CASH_COLLECT as CashCollect,CASH_AMOUNT as CashAmount,CASH_REP_PICKUP as CashRepPickUp,OPERATOR as Operator,STOCK_RETURNS as StockPickUp, PERIOD_RAISED as PeriodRaised,YEAR_RAISED as YearRaised,ORDER_VALUE as OrderValue,DETAIL_LINES as DetailLines FROM soheader WHERE ORDER_NO = ? AND STATUS = ?",strOrderNum,STATUS];
            
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs1 next])
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[rs1 resultDictionary]];
                
                self.strDebtor = [dict objectForKey:@"Debtor"];
                
                FMResultSet *rs11 = [db executeQuery:@"SELECT NAME as Name ,ADDRESS1 as Address1,ADDRESS2 as Address2,ADDRESS3 as Address3,SUBURB as Suburb,POST_CODE as PostCode,COUNTRY as Country FROM `armaster` WHERE CODE = ?",strDebtor];
                
                if (!rs11)
                {
                    [rs11 close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                if ([rs11 next])
                {
                    
                    NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] initWithDictionary:[rs11 resultDictionary]];
                    [dict addEntriesFromDictionary:dictTemp1];
                }
                
                [rs11 close];
                
                [dictHeaderDetails setObject:dict forKey:@"Data"];
                
                //Header
                NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
                
                [dictTemp setValue:[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelInst1"] forKey:@"DelInst1"];
                
                [dictTemp setValue:[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"DelInst2"] forKey:@"DelInst2"];
                
                [dictTemp setValue:[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"CustOrderno"] forKey:@"CustOrderno"];
                
                [dictHeaderDetails setObject:dictTemp forKey:@"Header"];
                [self prepareHeaderData];
                
            }
            
            [rs1 close];
            
            FMResultSet *rs2 = [db executeQuery:@"SELECT a.ITEM as Item, a.ORDER_NO, a.DEBTOR, a.DELIVERY_DATE, a.STATUS as Status, a.WAREHOUSE as Warehouse, a.BRANCH, a.PRICE_CODE, a.CHARGE_TYPE, a.DESCRIPTION as Description, a.PRICE as Price, a.EXTENSION as ExtnPrice,  a.QUANTITY as QuantityOrdered, a.ALT_QTY, a.ORIG_ORD_QTY, a.ORD_QTY, a.BO_QTY, a.CONV_FACTOR, a.REFRESH_PRICE, a.DISSECTION as Dissection, a.DISSECTION_COS as Dissection_Cos, a.WEIGHT as Weight, a.VOLUME as Volume, a.COST as Cost,a.PICTURE_FIELD as picture_field , a.ORIG_COST, a.GROSS as Gross, a.TAX as Tax, a.CREATE_OPERATOR, a.CREATE_DATE, a.CREATE_TIME,b.AVAILABLE as Available,b.ALLOCATED as Allocated,b.WAREHOUSE as Warehouse,b.PURCHASE_ORDER as PurchaseOrder,b.ON_HAND as OnHand,b.SUBSTITUTE as Subsitute FROM sodetail as a LEFT OUTER JOIN samaster as b ON TRIM(a.ITEM) = TRIM(b.CODE) AND TRIM(a.warehouse) = TRIM(b.warehouse) WHERE a.ORDER_NO = ? AND a.isDeleted = 0 AND a.STATUS = ? AND TRIM(a.Warehouse) = ?",strOrderNum,STATUS,strWarehouse];
            
            if (!rs2)
            {
                [rs2 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            [arrProducts removeAllObjects];
            
            totalCost = 0;
            while ([rs2 next]) {
                NSDictionary *dictProduct = [rs2 resultDictionary];
                [dictProduct setValue:@"0" forKey:@"isNewProduct"];
                [arrProducts addObject:dictProduct];
                
                float extnPrice = [[dictProduct objectForKey:@"ExtnPrice"] floatValue];
                totalCost += extnPrice;
            }
            
            [rs2 close];
            
            
        }
        @catch (NSException* e) {
            // rethrow if not one of the two exceptions above
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            });
            
        }
    }];
}

-(void)callDeleteProduct
{
    NSString *strItem = @"";
    if ([[arrProducts objectAtIndex:deleteIndexPath.row] objectForKey:@"Item"]) {
        strItem = [[arrProducts objectAtIndex:deleteIndexPath.row] objectForKey:@"Item"];
    }
    else if ([[arrProducts objectAtIndex:deleteIndexPath.row] objectForKey:@"StockCode"]){
        strItem = [[arrProducts objectAtIndex:deleteIndexPath.row] objectForKey:@"StockCode"];
    }
    
    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            @try {
                BOOL y = [db executeUpdate:@"UPDATE sodetail SET isDeleted = 1 WHERE ORDER_NO = ? AND ITEM = ?",strOrderNum,strItem];
                
                if (!y)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                [self doAfterdelete];
            }
            @catch (NSException* e) {
                // rethrow if not one of the two exceptions above
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Local DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
        }];
        
    }
    else{
        
        strWebserviceType = @"WS_DELETE_ORDER_PRODUCT";
        
        NSString *soapXMLStr = [NSString stringWithFormat:
                                @"<wws:DeleteSalesProductDetails soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                                "<salesProductDetails xsi:type=\"wws:DeleteSalesProductDetailsRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                                "<ORDER_NO>%@</ORDER_NO>"
                                "<ITEM><![CDATA[%@]]></ITEM>"
                                "</salesProductDetails>"
                                "</wws:DeleteSalesProductDetails>",strOrderNum,strItem];
        
        NSLog(@"soapXMLStr %@",soapXMLStr);
        
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
        [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#DeleteSalesProductDetails" onView:self.view];
    }
}

#pragma mark - WS calls

-(void)getBranchListNameForUser
{
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        
        @try {
            NSString *loggedInsalesman = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
            
            NSString *sqlQuery =[NSString stringWithFormat:@"SELECT BRANCH_LIST FROM syslogon WHERE LOGNAME =  '%@'",loggedInsalesman];
            
            FMResultSet *rs = [db executeQuery:sqlQuery];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs next]) {
                NSDictionary *dictData = [rs resultDictionary];
                NSString *strBranch1 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"BRANCH_LIST"]];
                [[NSUserDefaults standardUserDefaults] setObject:strBranch1 forKey:@"BRANCH"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}
-(void)callWSGetProductListDetails{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    strWebserviceType = @"WS_STOCK_PRODUCT_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"code=%@&warehouse=%@",strProductCode,strWarehouse];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,STOCK_PRODUCT_INFO_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSGetNewOrderNumber{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    strWebserviceType = @"WS_GET_ORDER_NUMBER";
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:GetOrderNo soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "</wws:GetOrderNo>"];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#GetOrderNo" onView:self.view];
    
}

-(void)callWSSaveSalesOrder
{
    strWebserviceType = @"WS_SALES_ORDER_SAVE";
    
    NSString *strItem = [[NSString alloc] init];
    float totalTax = 0;
    float totalWeight = 0;
    float totalVolume = 0;
    int totalQuantity = 0;
    NSString *strINC = @"N";
    BOOL Flag = NO;
    int i = 0;
    for (NSMutableDictionary *dict in arrProducts) {
        if([dict objectForKey:@"PriceChnaged"])
        {
            if(Flag == NO)
            {
                if([dict objectForKey:@"PriceChnaged"])
                {
                    Flag = YES;
                    strINC = [dict objectForKey:@"PriceChnaged"];
                }
                
            }
        }
        
        //--Send Y for temporary
       // strINC = @"Y";
        
        NSString *strStockCode1 = nil;
        NSString *strPrice = @"0";
        NSString *strDescription = nil;
        NSString *strExtension = @"0";
        NSString *strDissection = @"0";
        NSString *strDissection_Cos = @"0";
        NSString *strWeight = @"0";
        NSString *strVolume = @"0";
        NSString *strCost = @"0";
        NSString *strQuantityOrdered = @"0";
        NSString *strGross = @"0";
        NSString *strTax = @"0";
        NSString *strCustVolume = @"0";
        NSString *strPricingUnit = @"0";
        
        NSString *strAltQuantity = @"0";
        NSString *strCUST_PRICE = @"0";
        NSString *strCUST_ORD_QTY  = @"0";
        NSString *strCUST_SHIP_QTY = @"0";
        NSString *strCUST_VOLUME = @"0";
        NSString *strPRICING_UNIT = @"0";
        NSString *strCharge_Type = @"";
        NSString *strRelease_New = @"";
        NSString *strLineNum ;
        
        //--Comment
        if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]){
            
            strStockCode1 = @"/C";
            strDescription = [[dict objectForKey:@"Description"] trimSpaces];
            self.strDecimalPlaces = @"0";
            strQuantityOrdered = @"0.00";
            self.strConvFactor = @"0.0000";
            strCharge_Type = @"";
            strRelease_New = @"Y";
            
            strDissection = @"/C";
            strDissection_Cos = @"";
            
            self.strRefreshPrice = @"";
            
            int NumberOflines = strDescription.length/30;
            
            if(NumberOflines%30 > 0)
            {
                NumberOflines= NumberOflines +1;
                
            }
            
            if(NumberOflines == 0)
            {
                int z = 0;
                NSRange rang = NSMakeRange(z, 30);
                
                strStockCode1 = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                strRelease_New = @"Y";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                strLineNum = [NSString stringWithFormat:@"%d",++i];
                [dict setValue:strLineNum forKey:@"LINE_NO"];
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
                [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                strStockCode1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode1];
               strComemnt =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
                //==============Check fro HTML TAGS //==============
            
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[strLineNum trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode1 trimSpaces],strComemnt,[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
            
            for (int k = 0; k < NumberOflines; k++)
            {
                strLineNum = [NSString stringWithFormat:@"%d",++i];
                [dict setValue:strLineNum forKey:@"LINE_NO"];
                int z = k*30;
                NSRange rang = NSMakeRange(z, 30);
                
                strStockCode1 = @"/C";
                
                strQuantityOrdered = @"1.00";
                self.strConvFactor = @"0.0000";
                strRelease_New = @"Y";
                
                strDissection = @"/C";
                strDissection_Cos = @"";
                self.strRefreshPrice = @"";
                NSString *strComemnt;
                
                
                if(strDescription.length > z+30)
                {
                    strComemnt = [strDescription substringWithRange:rang];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                else
                {
                    
                    int r = strDescription.length-z;
                    NSRange rang = NSMakeRange(z, r);
                    strComemnt = [strDescription substringWithRange:rang ];
                    NSLog(@"s:::::::%@",strComemnt);
                }
                
                
                if ([strPriceCode isKindOfClass:[NSNumber class]])
                {
                    id priceval = strPriceCode;
                    strPriceCode = [priceval stringValue];
                }
                
                //==============Check fro HTML TAGS //==============
               strComemnt = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strComemnt];
               strStockCode1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode1];
                //==============Check fro HTML TAGS //==============
      
                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[strLineNum trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode1 trimSpaces],strComemnt,[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
                
            }
            
        }
        
        //Freight
        else if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
            
            strLineNum = [NSString stringWithFormat:@"%d",++i];
            [dict setValue:strLineNum forKey:@"LINE_NO"];
            
            strGross = [NSString stringWithFormat:@"%d",alertFreightValue];
            strCost = [NSString stringWithFormat:@"%d",alertFreightValue];
            strPrice = [NSString stringWithFormat:@"%d",alertFreightValue];
            strExtension = [NSString stringWithFormat:@"%f",freightValue];
            strTax = [NSString stringWithFormat:@"%f",[self calcFreightTax]];
            totalTax += [strTax floatValue];
            
            
            strStockCode1 = @"/F";
            strDescription = [dict objectForKey:@"Description"];
            self.strDecimalPlaces = @"0";
            
            strQuantityOrdered = @"0.00";
            self.strConvFactor = @"0.0000";
            
            strCUST_PRICE = strPrice;
            strCUST_ORD_QTY  = strQuantityOrdered;
            strCUST_SHIP_QTY = strQuantityOrdered;
            strCUST_VOLUME = @"1";
            strPRICING_UNIT = @"1";
            strCharge_Type = @"N";
            strRelease_New = @"Y";
            
            strDissection = @"/F";
            strDissection_Cos = @"";
            
            self.strRefreshPrice = @"";
            
            //==============Check fro HTML TAGS //==============
            strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
            strStockCode1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode1];
            //==============Check fro HTML TAGS //==============
            
        }
        
        
        //Product
        else{
            
            strLineNum = [NSString stringWithFormat:@"%d",++i];
            [dict setValue:strLineNum forKey:@"LINE_NO"];
            
            strStockCode1 = [dict objectForKey:@"StockCode"];
            strPrice = [dict objectForKey:@"Price"];
            strDissection = [[dict objectForKey:@"Dissection"] trimSpaces];
            strDissection_Cos = [[dict objectForKey:@"Dissection_Cos"] trimSpaces];
            strWeight = [dict objectForKey:@"Weight"];
            strVolume = [dict objectForKey:@"Volume"];
            
            strDescription = [[dict objectForKey:@"Description"] trimSpaces];
            
            //==============Check fro HTML TAGS //==============
            strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
            strStockCode1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode1];
            //==============Check fro HTML TAGS //==============
            
            strCost = [dict objectForKey:@"AverageCost"];
            
            if ([dict objectForKey:@"ExtnPrice"]) {
                strExtension = [dict objectForKey:@"ExtnPrice"];
            }
            
            strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
            totalQuantity += [strQuantityOrdered intValue];
            
            strWeight = [dict objectForKey:@"Weight"];
            
            strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
            
            totalWeight += [strWeight floatValue];
            
            strVolume = [dict objectForKey:@"Volume"];
            
            strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
            
            totalVolume += [strVolume floatValue];
            
            strGross = [dict objectForKey:@"Gross"];
            strTax = [dict objectForKey:@"Tax"];
            
            totalTax += [strTax floatValue];
            
            if ([strQuantityOrdered intValue] > 0) {
                strCustVolume = @"1";
                strPricingUnit = @"1";
            }
            else{
                strCustVolume = @"0";
                strPricingUnit = @"0";
            }
            
            self.strDecimalPlaces = @"2";
            self.strConvFactor = @"1.0000";
            self.strRefreshPrice = @"N";
        }
        
        //--check if Price is there
        if ([dict objectForKey:@"ExtnPrice"])
        {
            
            
            strItem = [strItem stringByAppendingFormat:@"<DETAIL><CUST_ORDER /><SUB_SET_NUM /><ORDER_NO /><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX><DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><TAX_AMOUNT1>%@</TAX_AMOUNT1> <TAX_AMOUNT2>0.00</TAX_AMOUNT2><TAX_AMOUNT3>0.00</TAX_AMOUNT3><TAX_AMOUNT4>0.00</TAX_AMOUNT4><TAX_AMOUNT5>0.00</TAX_AMOUNT5><TAX_AMOUNT6>0.00</TAX_AMOUNT6></DETAIL>",[strLineNum trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode1 trimSpaces],[strDescription trimSpaces],[strQuantityOrdered trimSpaces],strPriceCode ,[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strTax trimSpaces]];
        }
        
    }
    NSLog(@"%@",strItem);
    self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
    self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
    self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
    self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
    
    if ([isCashPickUpOn isEqualToString:@"N"]) {
        strCashPickUp = @"0.00";
        strChequeAvailable = @"N";
    }
    
    //QUANTITY = ALT_QTY = ORIG_ORD_QTY = ORD_QTY = BO_QTY
    self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
    self.strTotalLines = [NSString stringWithFormat:@"%d",[arrProducts count]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    
    NSString *iso8601String = [dateFormatter stringFromDate:now];
    
    //--DATE_RAISED
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateRaised = [dateFormatter dateFromString:strDateRaised];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    //if(dateRaised)
    strDateRaised = [dateFormatter stringFromDate:dateRaised];
    
    /*
     //Expirary Date
     [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
     NSDate *dateExpirary = [dateFormatter dateFromString:self.strDateRaised];
     [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
     self.strDateRaised = [dateFormatter stringFromDate:dateExpirary];
     */
    
    //--Delivery
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    if(strCopyDelDate)
    {
        NSDate *dateDel = [dateFormatter dateFromString:strCopyDelDate];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        if(dateDel)
            strCopyDelDate = [dateFormatter stringFromDate:dateDel];

    }
    else
    {
        strCopyDelDate = [strDelDate copy];
         [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateDel = [dateFormatter dateFromString:strCopyDelDate];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        if(dateDel)
          strCopyDelDate = [dateFormatter stringFromDate:dateDel];    
    }
    
    if(strCopyDelDate == nil)
    {
        strCopyDelDate = iso8601String;
    }
    
    if(self.strDateRaised == nil)
    {
        self.strDateRaised = iso8601String;
    }
    
    /*
     NSString *soapXMLStr = [NSString stringWithFormat:
     @"<SalesOrder>"
     "<HEADER>"
     "<OVERWRITE_SO>N</OVERWRITE_SO>"
     "<CANCEL_SO>N</CANCEL_SO>"
     "<INC_PRICE>Y</INC_PRICE>"
     "<INC_WAREHOUSE>Y</INC_WAREHOUSE>"
     "<DELIVERY_ADDRESS_TYPE>S</DELIVERY_ADDRESS_TYPE>"
     "<DELIV_NUM>0</DELIV_NUM>"
     "<SUB_SET_NUM/>"
     "<SUPPLIED_LINE_NUMBERS>N</SUPPLIED_LINE_NUMBERS>"
     "<ORDER_NO />"
     "<WEBORDERID>0</WEBORDERID>"
     "<EXPIRY_DATE>%@</EXPIRY_DATE>"
     "<DEBTOR>%@</DEBTOR>"
     "<BRANCH>%@</BRANCH>"
     "<SALES_BRANCH>%@</SALES_BRANCH>"
     "<DEL_NAME>%@</DEL_NAME>"
     "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
     "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
     "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
     "<DEL_SUBURB>%@</DEL_SUBURB>"
     "<DEL_POST_CODE>%@</DEL_POST_CODE>"
     "<DEL_COUNTRY>%@</DEL_COUNTRY>"
     "<DEL_INST1>%@</DEL_INST1>"
     "<DEL_INST2>%@</DEL_INST2>"
     "<CHARGE_TYPE>%@</CHARGE_TYPE>"
     "<CHARGE_RATE>%@</CHARGE_RATE>"
     "<DATE_RAISED>%@</DATE_RAISED>"
     "<DEL_DATE>%@</DEL_DATE>"
     "<CUST_ORDER>%@</CUST_ORDER>"
     "<SALESMAN>%@</SALESMAN>"
     "<WAREHOUSE>%@</WAREHOUSE>"
     "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
     "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
     "<DISC_PERCENT>0.00</DISC_PERCENT>"
     "<DELIVERY_RUN>%@</DELIVERY_RUN>"
     "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
     "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
     "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
     "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
     "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
     "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
     "</HEADER>"
     "%@"
     "</SalesOrder>",[iso8601String trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strDelAddress1 trimSpaces],[strDelAddress2 trimSpaces],[strDelAddress3 trimSpaces],[strDelSubUrb trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],strChargeRate ,[strDateRaised trimSpaces],[strCopyDelDate trimSpaces],[strCustOrder trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces],strItem];
     
     */
    
    NSString *strDelSuburblimit = [[strDelSubUrb trimSpaces]copy];
    
    if(strDelSuburblimit.length > 15)
    {
        strDelSuburblimit = [strDelSuburblimit substringToIndex:14];
    }
    
    //==============Check fro HTML TAGS //==============
   strDelAddress1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress1];
   strDelAddress2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress2];
   strDelAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress3];
   strDelInst2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
   strDelInst1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
   strDelSuburblimit = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelSuburblimit];
   strDelName = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    
    strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
    
    //==============Check fro HTML TAGS //==============
    
    self.strCustOrder = @"";
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<SalesOrder>"
                            "<HEADER>"
                            "<CUST_ORDER/>"
                            "<OVERWRITE_SO>N</OVERWRITE_SO>"
                            "<CANCEL_SO>N</CANCEL_SO>"
                            "<INC_PRICE>%@</INC_PRICE>"
                            "<INC_WAREHOUSE>Y</INC_WAREHOUSE>"
                            "<DELIVERY_ADDRESS_TYPE>S</DELIVERY_ADDRESS_TYPE>"
                            "<DELIV_NUM>0</DELIV_NUM>"
                            "<SUB_SET_NUM/>"
                            "<SUPPLIED_LINE_NUMBERS>N</SUPPLIED_LINE_NUMBERS>"
                            "<ORDER_NO />"
                            "<WEBORDERID>0</WEBORDERID>"
                            "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                            "<CASH_COLLECT>%@</CASH_COLLECT>"
                            "<CASH_AMOUNT>%@</CASH_AMOUNT>"
                            "<CASH_REP_PICKUP>%@</CASH_REP_PICKUP>"
                            "<STOCK_RETURNS>%@</STOCK_RETURNS>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<BRANCH>%@</BRANCH>"
                            "<SALES_BRANCH>%@</SALES_BRANCH>"
                            "<DEL_NAME>%@</DEL_NAME>"
                            "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                            "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                            "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                            "<DEL_SUBURB>%@</DEL_SUBURB>"
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                            "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                            "<DEL_INST1>%@</DEL_INST1>"
                            "<DEL_INST2>%@</DEL_INST2>"
                            "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                            "<CHARGE_RATE>%@</CHARGE_RATE>"
                            "<DATE_RAISED>%@</DATE_RAISED>"
                            "<DEL_DATE>%@</DEL_DATE>"
                            "<SALESMAN>%@</SALESMAN>"
                            "<WAREHOUSE>%@</WAREHOUSE>"
                            "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                            "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                            "<DISC_PERCENT>0.00</DISC_PERCENT>"
                            "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                            "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                            "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                            "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                            "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                            "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                            "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                            "</HEADER>"
                            "%@"
                            "</SalesOrder>",strINC,[iso8601String trimSpaces],self.isCashPickUpOn,self.strCashPickUp,self.strChequeAvailable,self.strStockPickup,[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strDelAddress1 trimSpaces],[strDelAddress2 trimSpaces],[strDelAddress3 trimSpaces],[strDelSuburblimit trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],strChargeRate ,[strDateRaised trimSpaces],[strCopyDelDate trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces],strItem];
    
    
    
    
    NSLog(@"soapXMLStr %@",soapXMLStr);//self.strOperatorName
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strRequest = [AppDelegate getServiceURL:@"webservices_rcf/insert_modules.php"];

    NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
    [requestnew setDelegate:self];
    [requestnew setRequestMethod:@"POST"];
    [requestnew setTimeOutSeconds:300];
    [requestnew setPostValue:soapXMLStr forKey:@"xmlData"];
    [requestnew setPostValue:@"1" forKey:@"transactionType"];
    [requestnew startAsynchronous];
    
}

- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
  @try {
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
    //NSLog(@"dictResponse:::%@",dictResponse);
      NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
      NSLog(@"text::::%@",text);
      
    if([[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"])
    {
//      [NSString stringWithFormat:@"Quote generated. Quote number %@",[[[[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]
      
      if([[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"])
      {
          [self AfterSalesOrderCreated];
         [spinner removeFromSuperview];
        UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Sales Order Created: Order Number:%@",[[[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [aler show];
        aler = nil;
        
      }
    }
    
    else
    {
       [spinner removeFromSuperview];
      UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Sales Order could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [aler show];
      aler = nil;
    }
  }
  @catch (NSException *exception) {
     [spinner removeFromSuperview];
    NSLog(@"error::%@",exception.description);
    [spinner removeFromSuperview];
    UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Sales Order could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [aler show];
    aler = nil;
  }
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    [spinner removeFromSuperview];
    UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Sales Order could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [aler show];
    aler = nil;

  [spinner removeFromSuperview];
  NSLog(@"falied:::::");
  
}

-(void)AfterSalesOrderCreated
{
    [spinner removeFromSuperview];
    [self prepareForNewOrder];
}
-(void)callWSSaveEditSalesOrder:(int)finalize
{
    strWebserviceType = @"WS_SALES_ORDER_EDIT_SAVE";
  
    //[self prepareHeaderData];
  
    int i =0;
  
    float totalTax = 0;
    float totalWeight = 0;
    float totalVolume = 0;
    int totalQuantity = 0;
  
    NSString *strItem = [[NSString alloc] init];
  

    for (NSDictionary *dict in arrProducts) {
      
        NSString *strStockCode2 = nil;
        NSString *strPrice = @"0";
        NSString *strDescription = nil;
        NSString *strExtension = @"0";
        NSString *strDissection = @"0";
        NSString *strDissection_Cos = @"0";
        NSString *strWeight = @"0";
        NSString *strVolume = @"0";
        NSString *strCost = @"0";
        NSString *strQuantityOrdered = @"0";
        NSString *strGross = @"0";
        NSString *strTax = @"0";
        NSString *strCustVolume = @"0";
        NSString *strPricingUnit = @"0";
        
        NSString *strAltQuantity = @"0";
        NSString *strCUST_PRICE = @"0";
        NSString *strCUST_ORD_QTY  = @"0";
        NSString *strCUST_SHIP_QTY = @"0";
        NSString *strCUST_VOLUME = @"0";
        NSString *strPRICING_UNIT = @"0";
        NSString *strCharge_Type = @"";
        NSString *strRelease_New = @"";
        
        NSString *strLineNum = [NSString stringWithFormat:@"%d",++i];
        
        //Comments
        if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]){
            
            strStockCode2 = @"/C";
            strDescription = [dict objectForKey:@"Description"];
            self.strDecimalPlaces = @"0";
           
            strQuantityOrdered = @"0.00";
            self.strConvFactor = @"0.0000";
            strCharge_Type = @"";
            strRelease_New = @"Y";
            
            strDissection = @"/C";
            strDissection_Cos = @"";
            
            self.strRefreshPrice = @"";

        }
        //Freight
        else if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/F"]){
            
            strStockCode2 = @"/F";
            strDescription = [dict objectForKey:@"Description"];
            strPrice = [dict objectForKey:@"Price"];
            strExtension = [dict objectForKey:@"ExtnPrice"];
            strTax = [dict objectForKey:@"Tax"];
            self.strDecimalPlaces = @"0";
            
            strQuantityOrdered = @"0.00";
            self.strConvFactor = @"0.0000";
            
            strCUST_PRICE = strPrice;
            strCUST_ORD_QTY  = strQuantityOrdered;
            strCUST_SHIP_QTY = strQuantityOrdered;
            strCUST_VOLUME = @"1";
            strPRICING_UNIT = @"1";
            strCharge_Type = @"N";
            strRelease_New = @"Y";
            
            strDissection = @"/F";
            strDissection_Cos = @"";
            self.strRefreshPrice = @"";
        }
        //Product
        else{
            
            if ([dict objectForKey:@"Item"]) {
                strStockCode2 = [[dict objectForKey:@"Item"] trimSpaces];
            }
            else if ([dict objectForKey:@"StockCode"]) {
                strStockCode2 = [[dict objectForKey:@"StockCode"] trimSpaces];
            }
            
            strPrice = [dict objectForKey:@"Price"];
            strDissection = [[dict objectForKey:@"Dissection"] trimSpaces];
            strDissection_Cos = [[dict objectForKey:@"Dissection_Cos"] trimSpaces];
            strWeight = [dict objectForKey:@"Weight"];
            strVolume = [dict objectForKey:@"Volume"];
            
            strDescription = [[dict objectForKey:@"Description"] trimSpaces];
            strCost = [dict objectForKey:@"Cost"];
            
            if ([dict objectForKey:@"ExtnPrice"]) {
                strExtension = [dict objectForKey:@"ExtnPrice"];
            }
            
            strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
            
            strAltQuantity = strQuantityOrdered;
            
            totalQuantity += [strQuantityOrdered intValue];
            strWeight = [dict objectForKey:@"Weight"];
            
            strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
            
            totalWeight += [strWeight floatValue];
            
            strVolume = [dict objectForKey:@"Volume"];
            
            strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
            
            totalVolume += [strVolume floatValue];
            
            strGross = [dict objectForKey:@"Gross"];
            strTax = [dict objectForKey:@"Tax"];
            
            totalTax += [strTax floatValue];
            
            if ([strQuantityOrdered intValue] > 0) {
                strCustVolume = @"1";
                strPricingUnit = @"1";
            }
            else{
                strCustVolume = @"0";
                strPricingUnit = @"0";
            }
            
            self.strDecimalPlaces = @"2";
            
            strCUST_PRICE = strPrice;
            strCUST_ORD_QTY  = strQuantityOrdered;
            strCUST_SHIP_QTY = strQuantityOrdered;
            strCUST_VOLUME = @"1";
            strPRICING_UNIT = @"1";
            strCharge_Type = @"";
            strRelease_New = @"";
            self.strRefreshPrice = @"N";
            self.strConvFactor = @"1.0000";
        }
        //--check if Price is there
        if ([dict objectForKey:@"ExtnPrice"])
        {
        strItem = [strItem stringByAppendingFormat:@"<product><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><ORIG_ORD_QTY>%@</ORIG_ORD_QTY><ORD_QTY>%@</ORD_QTY><BO_QTY>%@</BO_QTY><CONV_FACTOR>%@</CONV_FACTOR><REFRESH_PRICE>%@</REFRESH_PRICE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><COST>%@</COST><ORIG_COST>%@</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><CREATE_OPERATOR>%@</CREATE_OPERATOR><EDIT_STATUS>%@</EDIT_STATUS><DECIMAL_PLACES>%@</DECIMAL_PLACES><LINE_NO>%@</LINE_NO><isDeleted>%@</isDeleted><CUST_PRICE>%@</CUST_PRICE><CUST_ORD_QTY>%@</CUST_ORD_QTY><CUST_SHIP_QTY>%@</CUST_SHIP_QTY><CUST_VOLUME>%@</CUST_VOLUME><PRICING_UNIT>%@</PRICING_UNIT><CHARGE_TYPE>%@</CHARGE_TYPE></product>",strStockCode2,strDescription,strPrice,strExtension,strQuantityOrdered,strAltQuantity,strQuantityOrdered,strQuantityOrdered,strBOQty,self.strConvFactor,self.strRefreshPrice,strDissection,strDissection_Cos,strWeight,strVolume,strCost,strOrigCost,strGross,strTax,strOperator,strEditStatus,self.strDecimalPlaces,strLineNum,@"0",strCUST_PRICE,strCUST_ORD_QTY,strCUST_SHIP_QTY,strCUST_VOLUME,strPRICING_UNIT,strCharge_Type];
        }
    }
    
    
    if ([isCashPickUpOn isEqualToString:@"N"]) {
        strCashPickUp = @"0.00";
        strChequeAvailable = @"N";
    }
    
    //QUANTITY = ALT_QTY = ORIG_ORD_QTY = ORD_QTY = BO_QTY
    self.strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
    self.strTotalLines  = [NSString stringWithFormat:@"%d",[arrProducts count]];
    
    NSString *strFinalize = [NSString stringWithFormat:@"%d",finalize];
    
    self.strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
    self.strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
    self.strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
    self.strTotalCartonQty = [NSString stringWithFormat:@"%d",totalQuantity];
    
    //==============Check fro HTML TAGS //==============
    strDelAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress1];
    strDelAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress2];
    strDelAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelAddress3];
    strDelInst2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    strDelInst1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    NSString *strDelSuburblimit = [[strDelSubUrb trimSpaces]copy];
    
    if(strDelSuburblimit.length > 15)
    {
        strDelSuburblimit = [strDelSuburblimit substringToIndex:14];
    }

    strDelSuburblimit = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelSuburblimit];
    strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    //==============Check fro HTML TAGS //==============
 
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:EditSalesOrder soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "<salesOrdHeader xsi:type=\"wws:EditSalesOrdHeaderRequest\">"
                            "<ORDER_NO>%@</ORDER_NO>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<DEL_NAME>%@</DEL_NAME>"//editable
                            "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"//editable
                            "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"//editable
                            "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"//editable
                            "<DEL_SUBURB>%@</DEL_SUBURB>"//editable
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"//editable
                            "<DEL_COUNTRY>%@</DEL_COUNTRY>"//editable
                            "<CONTACT>%@</CONTACT>"//editable
                            "<DEL_DATE>%@</DEL_DATE>"
                            "<DATE_RAISED>%@</DATE_RAISED>"//todays date
                            "<CUST_ORDER/>"//editable
                            "<DEL_INST1>%@</DEL_INST1>"//editable
                            "<DEL_INST2>%@</DEL_INST2>"//editable
                            "<STATUS>%@</STATUS>"//always 1
                            "<DIRECT>%@</DIRECT>"
                            "<WAREHOUSE>%@</WAREHOUSE>"
                            "<BRANCH>%@</BRANCH>"
                            "<PRICE_CODE>%@</PRICE_CODE>"
                            "<SALESMAN>%@</SALESMAN>"
                            "<SALES_BRANCH>%@</SALES_BRANCH>"
                            "<TRADING_TERMS>%@</TRADING_TERMS>"
                            "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                            "<TAX_EXEMPTION1>%@</TAX_EXEMPTION1>"
                            "<TAX_EXEMPTION2>%@</TAX_EXEMPTION2>"
                            "<TAX_EXEMPTION3>%@</TAX_EXEMPTION3>"
                            "<TAX_EXEMPTION4>%@</TAX_EXEMPTION4>"
                            "<TAX_EXEMPTION5>%@</TAX_EXEMPTION5>"
                            "<TAX_EXEMPTION6>%@</TAX_EXEMPTION6>"
                            "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                            "<ALLOW_PARTIAL>%@</ALLOW_PARTIAL>"
                            "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                            "<CARRIER_CODE>%@</CARRIER_CODE>"
                            "<EXPORT_DEBTOR>%@</EXPORT_DEBTOR>"
                            "<DROP_SEQ>%@</DROP_SEQ>"
                            "<HELD>%@</HELD>"
                            "<CHARGE_RATE>%@</CHARGE_RATE>"
                            "<NUM_BOXES>%@</NUM_BOXES>"
                            "<CASH_COLLECT>%@</CASH_COLLECT>"
                            "<CASH_AMOUNT>%@</CASH_AMOUNT>"
                            "<CASH_REP_PICKUP>%@</CASH_REP_PICKUP>"
                            "<OPERATOR>%@</OPERATOR>"
                            "<STOCK_RETURNS>%@</STOCK_RETURNS>"
                            "<PERIOD_RAISED>%@</PERIOD_RAISED>"
                            "<YEAR_RAISED>%@</YEAR_RAISED>"
                            "<DETAIL_LINES>%@</DETAIL_LINES>"
                            "<ORDER_VALUE>%@</ORDER_VALUE>"
                            "<EXCHANGE_RATE>%@</EXCHANGE_RATE>" // Static value
                            "<TOTL_CARTON_QTY>%@</TOTL_CARTON_QTY>" // Static value
                            "<ACTIVE>%@</ACTIVE>" // Static value
                            "<EDIT_STATUS>%@</EDIT_STATUS>" // Static value
                            "<WEIGHT>%@</WEIGHT>"
                            "<VOLUME>%@</VOLUME>"
                            "<TAX>%@</TAX>"
                            "<TAX_AMOUNT1>%@</TAX_AMOUNT1>"
                            "<isFinalised>%@</isFinalised>"
                            "</salesOrdHeader>"
                            "<salesOrdDetails xsi:type=\"wws:EditSalesOrdDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                            "%@"
                            "</salesOrdDetails>"
                            "</wws:EditSalesOrder>",[strOrderNum trimSpaces],[strDebtor trimSpaces],[strDelName trimSpaces],[strDelAddress1 trimSpaces],[strDelAddress2 trimSpaces],[strDelAddress3 trimSpaces],[strDelSuburblimit trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strContact trimSpaces],[strDelDate trimSpaces],[strDateRaised trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strStatus trimSpaces],[strDirect trimSpaces],[strWarehouse trimSpaces],[strBranch trimSpaces],strPriceCode ,[strSalesman trimSpaces],[strSalesBranch trimSpaces],[strTradingTerms trimSpaces],[strDeliveryRun trimSpaces],[strTaxExemption1 trimSpaces],[strTaxExemption2 trimSpaces],[strTaxExemption3 trimSpaces],[strTaxExemption4 trimSpaces],[strTaxExemption5 trimSpaces],[strTaxExemption6 trimSpaces],[strExchangeCode trimSpaces],[strAllowPartial trimSpaces],[strChargetype trimSpaces],[strCarrier trimSpaces],[strExportDebtor trimSpaces],[strDropSequence trimSpaces],[strHeld trimSpaces],[strChargeRate trimSpaces],[strNumboxes trimSpaces],[isCashPickUpOn trimSpaces],[strCashPickUp trimSpaces],[strChequeAvailable trimSpaces],[strOperator trimSpaces],[strStockPickup trimSpaces],[strPeriodRaised trimSpaces],[strYearRaised trimSpaces],[strTotalLines trimSpaces],[strTotalCost trimSpaces],[strExchangeRate trimSpaces],[strTotalCartonQty trimSpaces],[strActive trimSpaces],[strEditStatus trimSpaces],[strTotalWeight trimSpaces],[strTotalVolume trimSpaces],[strTotalTax trimSpaces],[strTotalTax trimSpaces],[strFinalize trimSpaces],strItem];
    
    
    NSLog(@"soapXMLStr %@",soapXMLStr);
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#EditSalesOrder" onView:self.view];
}

#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    NSLog(@"dictResponse %@",dictResponse);
    [spinner removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]){
        
        spinnerObj.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:HUD_COMPLETION_IMAGE]];
        spinnerObj.mode = MBProgressHUDModeCustomView;
        [spinnerObj hide:YES afterDelay:SPINNER_HIDE_TIME];
        
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
            self.strOrderNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"orderno"] objectForKey:@"text"];
            [tblHeader reloadData];
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetOrderNoResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_SALES_ORDER_SAVE"]){
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            
            self.strUploadDate = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"UploadDate"] objectForKey:@"text"];
            
            self.strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            self.strOrderNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"orderno"] objectForKey:@"text"];
            
            
            dispatch_async(backgroundQueueForSalesOrderEntry, ^(void) {
                [self callInsertSalesOrderToDB];
            });
            
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertSalesOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_SALES_ORDER_EDIT_SAVE"]){
        
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditSalesOrderResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
            
            self.strUploadDate = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditSalesOrderResponse"] objectForKey:@"UploadDate"] objectForKey:@"text"];
            
            self.strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditSalesOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            dispatch_async(backgroundQueueForSalesOrderEntry, ^(void) {
                [self callInsertSalesOrderToDB];
                
            });
            
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditSalesOrderResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_DELETE_ORDER_PRODUCT"])
    {
        NSLog(@"WS_DELETE_ORDER_PRODUCT dictResponse==%@",dictResponse);
        
        
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteSalesProductDetailsResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
            //True
            
            NSString *strSuccessMessage1 = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteSalesProductDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage1 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            
            dispatch_async(backgroundQueueForSalesOrderEntry, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    BOOL isSomethingWrongHappened = FALSE;
                    @try
                    {
                        NSString *strItem = [NSString stringWithFormat:@"%@",[[[arrProducts objectAtIndex:deleteIndexPath.row]objectForKey:@"Item"] trimSpaces]];
                        
                        //Delete the record
                        BOOL y1 =  [db executeUpdate:@"DELETE FROM `sodetail` WHERE `ORDER_NO` = ? AND `ITEM` = ?",strOrderNum,strItem];
                        
                        if (!y1)
                        {
                            isSomethingWrongHappened = TRUE;
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        *rollback = NO;
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [self doAfterdelete];
                        });
                        
                    }
                    @catch (NSException* e) {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                    }
                    
                    
                }];
                
            });
        }
        //False
        else{
            
            NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteSalesProductDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
        
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
    if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]) {
        [self callGetNewOrderNumberFromDB];
    }
    
    if ([strWebserviceType isEqualToString:@"WS_SALES_ORDER_SAVE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:SAVE_LOCAL_MESSAGE delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = TAG_200;
        [alertView show];
        
    }
    
    if ([strWebserviceType isEqualToString:@"WS_SALES_ORDER_EDIT_SAVE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:SAVE_LOCAL_MESSAGE delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = TAG_300;
        [alertView show];
        
    }
    
}

#pragma mark - Delegates

- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict{
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
    [segControl setSelectedIndex:1];
    
    self.isCommentExist = NO;
    
    __block int prodIndex = 0;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            self.isCommentExist = YES;
            prodIndex = idx;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        //Show delete button
        btnDelete.hidden = NO;
        tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        //--Add Comment at top
//         [arrProducts insertObject:dict atIndex:0];
        [arrProducts addObject:dict];
        
        totalLines = totalLines +1;
        lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
        [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    else{
        [arrProducts replaceObjectAtIndex:prodIndex withObject:dict];
        
        if (isCommentAddedOnLastLine) {
            id object = [arrProducts objectAtIndex:prodIndex];
            [arrProducts removeObjectAtIndex:prodIndex];
            [arrProducts insertObject:object atIndex:[arrProducts count]];
        }
        [tblDetails reloadData];
    }
}


- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation{
    
    isSearch = NO;

    NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
    
    NSMutableDictionary *dictTotalPrice = [arrProducts objectAtIndex:productIndex];
    [dictTotalPrice setValue:strTotalPrice forKey:@"ExtnPrice"];
    
    //Quantity Ordered
    NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
    [dictTotalPrice setValue:strQuantityOrdered forKey:@"QuantityOrdered"];
    [dictTotalPrice setValue:[dictCalculation objectForKey:@"Gross"] forKey:@"Gross"];
    [dictTotalPrice setValue:[dictCalculation objectForKey:@"Tax"] forKey:@"Tax"];
    [dictTotalPrice setValue:[dictCalculation objectForKey:@"Price"] forKey:@"Price"];
    if([dictCalculation objectForKey:@"PriceChnaged"])
    [dictTotalPrice setValue:[dictCalculation objectForKey:@"PriceChnaged"] forKey:@"PriceChnaged"];
    [tblDetails reloadData];
    
    totalCost = 0;
    totalLines = 0;
    for (NSDictionary *dict in arrProducts) {
        if ([dict objectForKey:@"ExtnPrice"]) {
            NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
            totalCost += [strExtnPrice floatValue];
            totalLines = totalLines + 1;
        }
    }
    
    if ([strQuantityOrdered isEqualToString:@"0"]) {
        totalLines = totalLines - 1;
        NSLog(@"Product quantity is zero .... ");
    }
    
    if (totalLines > 0)
    {
        // Store number of quantity
   
        NSString *strTotalLine = [NSString stringWithFormat:@"%d",totalLines];
        [[NSUserDefaults standardUserDefaults] setObject:strTotalLine forKey:@"QuantityForSale"];
        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    else{
        // Store number of quantity
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"QuantityForSale"];
        NSLog(@"UPDATE VALUE TO SAVE RECORD");
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    lblTotalLines.text = [NSString stringWithFormat:@"%d",totalLines];
    lblTotalCost.text = [NSString stringWithFormat:@"$%.2f",totalCost];
}

- (void)actiondismissPopOverForCashPickUp:(NSString*)strPickUpAmt IsChequeAvaliable:(BOOL)isChequeAvaliable{
    if (self.popoverController) {
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
    }
    
    if ([strPickUpAmt intValue] == 0) {
        self.strCashPickUp = @"0";
    }
    else{
        self.strCashPickUp = strPickUpAmt;
    }
    
    if (isChequeAvaliable){
        self.strChequeAvailable = @"Y";
    }
    else{
        self.strChequeAvailable = @"N";
    }
    
    [tblHeader reloadData];
    
    
}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [btnAddMenu setImage:[UIImage imageNamed:@"red_plus_down.png"] forState:UIControlStateNormal];
    [[self.view findFirstResponder] resignFirstResponder];
    [tblHeader reloadData];
}



- (void)dismissStockPickUp:(NSString *)strData withDict:(NSMutableDictionary*)stockPickupDict{
    
    if (![strData isEqualToString:@"|||||||||||||||||||"]) {
        isPickupFormOn = @"Y";
        self.strPickupInfo = strData;
        [dictHeaderDetails setValue:strData forKey:@"PickUpForm"];
        dictStockPickup = [stockPickupDict mutableCopy];
    }
    else{
        [dictHeaderDetails removeObjectForKey:@"PickUpForm"];
        self.strPickupInfo = @"N";
        isStockPickUpOn = @"N";
        [tblHeader reloadData];
    }
    [[AppDelegate getAppDelegateObj].rootViewController dismissViewControllerAnimated:YES completion:Nil];
}


- (void)dismissStorePickUp:(NSString *)strData{
    
    [[AppDelegate getAppDelegateObj].rootViewController dismissViewControllerAnimated:YES completion:Nil];
    if (![strData isEqualToString:@"|||||||||||||||||||||||||||||"]) {
        isStockPickUpOn = @"Y";
        self.strStockPickup = strData;
    }
    else{
        [dictHeaderDetails removeObjectForKey:@"StockPickUp"];
        self.strStockPickup = @"N";
        isStockPickUpOn = @"N";
        [tblHeader reloadData];
    }
    
}

- (void)setDeliveryRun:(NSString *)strDelRun{
    
    isDeliveryRunSlected = YES;
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    self.strDeliveryRun = strDelRun;
    [tblHeader reloadData];
}

//Delegate Method
- (void)actionSaveQuote:(int)finalizeTag{
    self.finalise = finalizeTag;

    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts) {
        // check comments
        //        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
        
        if ([dict objectForKey:@"Item"]){
            
            if (![dict objectForKey:@"ExtnPrice"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = FALSE;
                break;
            }
            else if (![dict objectForKey:@"ExtnPrice"]&& ![[dict objectForKey:@"Item"] isEqualToString:@"/C"]) {
                isCalculationPending = TRUE;
                //  break;
            }
            else if ([dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = FALSE;
                break;
            }
            else{
                isCalculationPending = FALSE;
            }
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    else {
        
//        if ([[[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"OrderOnHold"] isEqualToString:@"Y"]) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Order on hold" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            alert.tag = TAG_400;
//            [alert show];
//        }
//        else{
        
        
            if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected){
                [self callInsertSalesOrderToDB];

                if([dictHeaderDetails objectForKey:@"PickUpForm"]){
                    [self insertStockPickupToDB];
                }
            }
            else{
                if (isEditable) {
                    [self callWSSaveEditSalesOrder:finalise];
                }
                else{
                    [self callInsertSalesOrderToDB];

                    if([dictHeaderDetails objectForKey:@"PickUpForm"]){
                        [self insertStockPickupToDB];
                    }
                   

                    // 30thOct
//                    [self callWSSaveSalesOrder];
                }
            }

    }
    
    
}

-(void)setCommentPosition:(BOOL)status
{
    self.isCommentAddedOnLastLine = status;
}

-(void)dismissPopover{
    [popoverController dismissPopoverAnimated:YES];
    self.popoverController = nil;
}

#pragma mark - Handle Notification
- (void)reloadHeader:(NSNotification *)notification {
    
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    //Show the header
    [segControl setSelectedIndex:0];
    
    self.dictHeaderDetails = [notification.userInfo valueForKey:@"source"];
    
    if ([[dictHeaderDetails objectForKey:@"Data"] objectForKey:@"Debtor"]) {
        
        [self prepareHeaderData];
        
        //Comments Exist
        if ([[[dictHeaderDetails objectForKey:@"Comments"] objectForKey:@"Flag"] isEqualToString:@"True"]) {
            // Path to the plist (in the application bundle)
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"SalesOrderEntryHeaderWithComments" Type:@"plist"];
            // Build the array from the plist
            arrProductLabels = (NSMutableArray *)[NSArray arrayWithContentsOfFile:path];
        }
        else{
            NSString *path = [AppDelegate getFileFromLocalDirectory:@"SalesOrderEntryHeader" Type:@"plist"];
            // Build the array from the plist
            arrProductLabels = (NSMutableArray *)[NSArray arrayWithContentsOfFile:path];
        }
        if ([[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"DelName"] ) {
            _lblCustomerName.text = [[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"DelName"];
            
        }
    }
    else{
        segControl.enabled = NO;
    }
    
    isSearch = NO;
    [arrSearchProducts removeAllObjects];
    if (arrProducts.count > 0) {
        lblTotalCost.text =@"0.00";
        lblTotalLines.text = @"0";
        [arrProducts removeAllObjects];
    }
    
    [tblHeader reloadData];
    
    if (detailViewController) {
        [self dismissPopupViewControllerWithanimationType:nil];
        detailViewController = nil;
    }
    detailViewController = [[MJDebtorViewController alloc] initWithNibName:@"MJDebtorViewController" bundle:nil];
    [self DisplayData:[[dictHeaderDetails objectForKey:@"Data"]objectForKey:@"Debtor"]];
    NSLog(@"dictCustomerDetails:::::%@",dictCustomerDetails);
    
    NSString *balance =self.strBalanceVal;

    NSString *Over90 =[dictCustomerDetails objectForKey:@"Over21"];
    NSString *Over60 =[dictCustomerDetails objectForKey:@"Over14"];
    NSString *Over30 =[dictCustomerDetails objectForKey:@"Over7"];
    NSString *Name =[dictCustomerDetails objectForKey:@"Name"];
    
    NSDictionary *dict = [NSMutableDictionary new];
    [dict setValue:(Name)?Name:@"" forKey:@"Name"];
    [dict setValue:(strTradingTerms)?strTradingTerms:@"" forKey:@"TradingTerms"];
    [dict setValue:(Over90)?Over90:@"" forKey:@"Over21"];
    [dict setValue:(Over60)?Over60:@"" forKey:@"Over14"];
    [dict setValue:(Over30)?Over30:@"" forKey:@"Over7"];
    [dict setValue:(balance)?balance:@"" forKey:@"Balance"];
    
    detailViewController.detailDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    [self presentPopupViewController:detailViewController animationType:3];

}

- (void)reloadTableViewData:(NSNotification *)notification {
    isSearch = NO;

    NSDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
    
    NSLog(@"dictSelectedProduct : %@",dictSelectedProduct);
    
    __block BOOL isScrollToBottom = YES;
    
    //Comments
    if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
    }
    //Freight
    else if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/F"])
    {
       //NSLog(@"%@",arrProducts);
        if (isEditable)
        {
            for(int j=0;j<[arrProducts count];j++)
            {
                if([[[arrProducts objectAtIndex:j] objectForKey:@"Item"] isEqualToString:@"/F"])
                {
                    [[arrProducts objectAtIndex:j] setObject:[dictSelectedProduct objectForKey:@"Price"] forKey:@"Cost"];
                    [[arrProducts objectAtIndex:j] setObject:[dictSelectedProduct objectForKey:@"Price"] forKey:@"Gross"];
                    [[arrProducts objectAtIndex:j] setObject:[dictSelectedProduct objectForKey:@"Price"] forKey:@"Price"];
                    [[arrProducts objectAtIndex:j] setObject:[dictSelectedProduct objectForKey:@"ExtnPrice"] forKey:@"ExtnPrice"];
                    [[arrProducts objectAtIndex:j] setObject:[dictSelectedProduct objectForKey:@"Tax"] forKey:@"Tax"];
                }
            }
        }
        else
        {
            [dictSelectedProduct setValue:@"2" forKey:@"isNewProduct"];
            
             [arrProducts insertObject:dictSelectedProduct atIndex:0];
            
          //  [arrProducts addObject:dictSelectedProduct];
        }
       NSLog(@"%@",arrProducts);

    }
    else{
        
        [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
        
        __block BOOL toRemove = NO;
        if ([arrProducts count])
        {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if (([[obj objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]])|| ([[obj objectForKey:@"Item"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]])) {
                    
                    if ([[obj objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
                        
                        NSString *strMessage = [NSString stringWithFormat:@"\"%@\" is already added!",[dictSelectedProduct objectForKey:@"Description"]];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                        strMessage = nil;
                        
                    }
                    else{
                        NSLog(@"%@",[dictSelectedProduct objectForKey:@"CheckFlag"]);
                        if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
                            [arrProducts removeObjectAtIndex:idx];
                        }
                        
                    }
                    
                    toRemove = YES;
                    isScrollToBottom = NO;
                    *stop = YES;
                }
                else{
                    toRemove = NO;
                }
                
            }];
        }
        else{
            toRemove = NO;
            
        }
        
        if (!toRemove) {
            //--Insert Product at top
//            [arrProducts addObject:dictSelectedProduct];
            [arrProducts insertObject:dictSelectedProduct atIndex:0];
        }
        
    }
    
    int lastIndex = [arrProducts count];
    if (lastIndex) {
        [self showOrEnableButtons];
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                }
            }];
        }
        
        //Show delete button
        btnDelete.hidden = NO;
        tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        if (isScrollToBottom) {
            NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
            [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        }
        
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        
    }
}

#pragma mark - PopOverDelegates
//Add Products
- (void)showProductList{
    
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

-(void)showSpecials
{
    NSLog(@"Specials Clicked");
    [self btnShowSpecials:nil];
}

- (void)dismissPopOver{
    
    if (self.popoverController) {
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
    }
    
    [btnAddMenu setImage:[UIImage imageNamed:@"red_plus_down.png"] forState:UIControlStateNormal];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    //[self viewWillAppear:YES];
}


- (void)showCommentView{
    [segControl setSelectedIndex:1];
    self.isCommentExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            self.isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
        dataViewController.commentStr = @"";
        dataViewController.commntTAg = 0;
        dataViewController.delegate = self;
        dataViewController.productIndex = -1;
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
}

- (void)showFreightView{
    //[segControl setSelectedIndex:1];
    __block BOOL isFreightExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/F"]) {
            isFreightExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isFreightExist)
    {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter the freight value." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            alert.tag = 911;
            [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            UITextField * alertTextField = [alert textFieldAtIndex:0];
            alertTextField.placeholder = @"Enter the freight value";
            alertTextField.keyboardType = UIKeyboardTypeNumberPad;
            [alert show];
            // [self calcFreight];
    }
}


#pragma  mark - Searchbar Delegates


- (IBAction)filterSearchResult:(id)sender {
    NSLog(@"Search by : ");
    UISegmentedControl *segment=(UISegmentedControl*)sender;
    switch (segment.selectedSegmentIndex) {
        case 0:
            NSLog(@"Description");
            
            break;
        case 1:
            NSLog(@"Code");
            break;
        default:
            break;
    }
    
}



-(void)searchResultByDescription:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"Description CONTAINS[cd] %@", stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void)searchResultByCode:(NSString*)stringText
{
    stringText = [stringText stringByReplacingOccurrencesOfString:@"%"
                                                       withString:@""];
    
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"StockCode CONTAINS[cd] %@", stringText];
    
    NSArray *ary = [arrProducts filteredArrayUsingPredicate:sPredicate];
    
    arrSearchProducts = [NSMutableArray arrayWithArray:ary];
    
    ary = nil;
    
    [self.view endEditing:YES];
    //
    if([arrSearchProducts count] > 0)
    {
        isSearch = YES;
        [tblDetails reloadData];
    }
    else
    {
        isSearch = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"The product you are searching for is not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}



-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length <= 0)
    {
        [arrSearchProducts removeAllObjects];
        isSearch = NO;
        [tblDetails reloadData];
    }
    
    
}



-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchTxt = [NSString stringWithFormat:@"%%%@%%", searchBar.text];
    
    isSearch = YES;
    [arrSearchProducts removeAllObjects];
    
    if (_SegmentSearch.selectedSegmentIndex == 0) {
        [self searchResultByDescription:searchTxt];
    }
    else  if (_SegmentSearch.selectedSegmentIndex == 1){
          [self searchResultByCode:searchTxt];
    }
    
    [self.view endEditing:YES];
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            if ([strWebserviceType isEqualToString:@"WS_GET_ORDER_NUMBER"]) {
                [self callGetNewOrderNumberFromDB];
            }
            
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
                [self callWSGetNewOrderNumber];
            }
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            if ([strWebserviceType isEqualToString:@"DB_GET_ORDER_NUMBER"]) {
                [self callWSGetNewOrderNumber];
            }
            
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


-(void)DisplayData:(NSString*)strcust_code{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        [self callViewCustomerTransactionFromDB:db forCustCode:strcust_code];
    }];
    
}


#pragma mark - Database calls
-(void)callViewCustomerTransactionFromDB:(FMDatabase *)db forCustCode:(NSString*)custcode{
    
    strWebserviceType = @"DB_GET_CUSTOMER_TRANSACTION";
    FMResultSet *rs1;
    @try {
        
        rs1 = [db executeQuery:@"SELECT CODE as Code, CENTRAL_DEBTOR as CentralDebtor, NAME as Name, CURRENT as Current, PHONE as Phone, CONTACT1 as Contact1,DEL_ADDRESS1 as DelAddress1,DEL_ADDRESS2 as DelAddress2,DEL_ADDRESS3 as DelAddress3,DEL_SUBURB as DelSuburb, EXCHANGE_CODE as ExchangeCode, OVER30 as Over30, CREDIT_LIMIT as CreditLimit, OVER60 as Over60, SALES_MTD as SalesMtd, SALES_YTD as SalesYtd, OVER90 as Over90, TERMS as Terms, PAYMENT_PROFILE as PaymentProfile,TRADING_TERMS as TradingTerms, LAST_PAID as LastPaid, HOLD as Hold, TOTAL_DUE as TotalDue,UNRELEASED_ORD as UnReleaseOrd FROM armaster WHERE CODE=? ",custcode];
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
            
        }
        else{
            NSLog(@"No Details exist for this code");
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
    
    FMResultSet *rs2;
    @try {
        
        [arrCustomerTransactions removeAllObjects];
        rs2 = [db executeQuery:@"SELECT DEBTOR as Debtor, TRAN_NO as TranNo, DATE_RAISED as Date, CUST_ORDER, TYPE as Type, STATUS as Status, NETT as Nett, AMOUNT_PAID FROM artrans WHERE DEBTOR = ? ",custcode];
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs2 next]) {
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            //Balance calculation
            float balance = [[dictData objectForKey:@"Nett"] floatValue] - [[dictData objectForKey:@"AMOUNT_PAID"] floatValue];
            NSString *strBalance1 = [NSString stringWithFormat:@"%f",balance];
            
            [dictData setValue:strBalance1 forKey:@"BALANCE"];
            [dictCustomerDetails setValue:strBalance1 forKey:@"Balance"];
            
            NSDate *fromDate = [[SDSyncEngine sharedEngine] dateUsingStringFromAPI:[dictData objectForKey:@"Date"]];
            NSDate *toDate = [NSDate date];
            int days = [AppDelegate calcDaysBetweenTwoDate:fromDate ToDate:toDate];
            NSString *strDays = [NSString stringWithFormat:@"%d",days];
            
            [dictData setValue:strDays forKey:@"Days"];
            
            [arrCustomerTransactions addObject:dictData];
            
        }
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
    }
    
    //}];
}
@end
