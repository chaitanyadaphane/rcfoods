//
//  QuoteStatusViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "QuoteStatusViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteStatusDetailViewController.h"

#define QUOTE_STATUS_LIST_WS @"quotestatus/listquotes.php?"

@interface QuoteStatusViewController ()
{
    BOOL _loadingInProgress;
    MBProgressHUD *spinner;
    NSArray *aryDisFilterData;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;

@end

@implementation QuoteStatusViewController
@synthesize filteredListContent,arrQuoteList,arrQuoteListForWebservices,vwNoQuotes,vwContentSuperview,vwTblQuotes,tblQuoteList,searchingBar,vwNoResults,isSearchMode,localSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
   /*     //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    */
        
    }
    return self;
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrQuoteList = [[NSMutableArray alloc] init];
    
    self.isSearchMode = NO;
    
    //First page
    currPage = 1;
    totalCount = 0;
    recordNumber = 0;
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected )
    {
         [self callWSGetAvailable];
    }
   
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please check your internet connection" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }

}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
    {
        CGRect frm =     self.searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
    else{
        CGRect frm =     self.searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;

    }
}

-(void)callWSGetAvailable{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *salesType = [prefs objectForKey:@"members"];
    NSString *strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];

    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *parameters = [NSString stringWithFormat:@"branch=%@&members=%@",strBranchList,salesType];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QuoteStatusStatus_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}
#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    @try {
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        arrQuoteList = nil;
        if([dictResponse isKindOfClass:[NSDictionary class]])
        {
            if([[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Flag"])
            {
                if([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Flag"] objectForKey:@"text"]isEqualToString:@"True"])
                {
                    if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"] isKindOfClass:[NSArray class]])
                    {
                        self.arrQuoteList = [[NSMutableArray alloc]initWithArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"]];
                        
                    }
                    else
                    {
                        if(!arrQuoteList)
                        {
                            arrQuoteList = [[NSMutableArray alloc]init];
                        }
                        [arrQuoteList addObject:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quotes"] objectForKey:@"Data"]];
                    }
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"DateRaised.text" ascending: NO];
                    NSArray *sortedArray = [(NSArray*)arrQuoteList sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                    arrQuoteList = [sortedArray mutableCopy];

                }
                else
                {
                    UIAlertView *al = [[UIAlertView alloc]initWithTitle:@"" message:@"No Quotes avaliable" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [al show];
                    al = nil;
                }
            }
            else{
                
                if ([[[[[dictResponse objectForKey:@"Response"]objectForKey:@"Quotes"]  objectForKey:@"UpdateInfo"]objectForKey:@"text"] isEqualToString:@"No Updates"]) {
                    UIAlertView *al = [[UIAlertView alloc]initWithTitle:@"" message:@"No Quotes avaliable" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [al show];
                    al = nil;
                }
 
            }
                
        }
  
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    @finally {
        [self changeData];
        [tblQuoteList reloadData];
        
        [spinner removeFromSuperview];
    }
    
}

-(void)changeData
{
    if(!self.filteredListContent)
    {
        self.filteredListContent   = [[NSMutableArray alloc]init];
    }
    for (NSDictionary *dict in self.arrQuoteList) {
        
        NSMutableDictionary *dcit1 = [[NSMutableDictionary alloc]init];
        [dcit1 setValue:[[dict objectForKey:@"DelName"] objectForKey:@"text"] forKey:@"DelName"];
        [dcit1 setValue:[[dict objectForKey:@"QuoteNo"] objectForKey:@"text"] forKey:@"QuoteNo"];
        [dcit1 setValue:[[dict objectForKey:@"CustOrder"] objectForKey:@"text"] forKey:@"CustOrder"];
        
        [self.filteredListContent addObject:dcit1];
        dcit1 = nil;
        
    }
    
}
-(void)ASIHTTPRequest_Error:(id)error
{
    [spinner removeFromSuperview];
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}
- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblQuoteList = nil;
    self.vwTblQuotes = nil;
    self.vwContentSuperview = nil;
    self.vwNoQuotes = nil;
    self.vwNoResults = nil;
    self.searchingBar = nil;
    self.localSpinner = nil;
}


- (void)doAfterDataFetched{
    if ([arrQuoteList count]) {
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwTblQuotes];
        
        if([arrQuoteList count] < totalCount){
            [self repositionLoadMoreFooterView];
            
            // Dismiss loading footer
            [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
            
            recordNumber += ROWS_PER_PAGE;
            [tblQuoteList reloadData];
            
        }
        else{
            
            [_loadMoreFooterView removeFromSuperview];
            _loadMoreFooterView=nil;
        }
        
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoQuotes];
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [aryDisFilterData count];
    }
	else
	{
        NSLog(@"COUNT:::::::%d",[self.arrQuoteList count]);
        
        return [self.arrQuoteList count];
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    @try {
        static NSString *CellIdentifier = CELL_IDENTIFIER11;
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell = [nib objectAtIndex:10];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (isSearchMode)
        {
            cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[[aryDisFilterData objectAtIndex:indexPath.row] objectForKey:@"DelName"]objectForKey:@"text"]];
            cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[[aryDisFilterData objectAtIndex:indexPath.row] objectForKey:@"QuoteNo"]objectForKey:@"text"]];
            
            if ([[[aryDisFilterData objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"]objectForKey:@"text"])
            {
                if([[[aryDisFilterData objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] isEqualToString:@"A"])
                {
                    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
                    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                    
                    [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
                }
            }
            else{
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"no" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                
                [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
            }
            
            
            if ([[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"]) {
                {
                    if([[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] isKindOfClass:[NSDictionary class]])
                    {
                        if([[[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] objectForKey:@"text"]isEqualToString:@"A"])
                        {
                            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
                            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                            
                            [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
                            
                        }
                    }
                }
                
            }
            else{
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"no" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                
                [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
            }
        }
        
        else
        {
            cell.lblCustomerName.text = [NSString stringWithFormat:@"%@",[[[self.arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"DelName"] objectForKey:@"text"]];
            cell.lblQuoteNo.text = [NSString stringWithFormat:@"%@",[[[self.arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"QuoteNo"] objectForKey:@"text"]];
            
            
            if ([[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"]) {
                {
                    if([[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"]  isKindOfClass:[NSDictionary class]])
                    {
                        if([[[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] objectForKey:@"text"] isEqualToString:@"A"])
                        {
                            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"yes" ofType:@"png"];
                            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                            
                            [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
                            
                        }
                    }
                }
                
            }
            else{
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"no" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                
                [cell.btnQuoteStatus setImage:img forState:UIControlStateNormal];
            }
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"Exeprtion::%@",exception.description);
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.searchingBar resignFirstResponder];

    if (isSearchMode) {
        
        QuoteStatusDetailViewController *dataViewController = [[QuoteStatusDetailViewController alloc] initWithNibName:@"QuoteStatusDetailViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.dictProductDetails = [aryDisFilterData objectAtIndex:[indexPath row]];
        //
        //    if ([[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] isEqualToString:@""]) {
        //        dataViewController.isQuoteApproved = NO;
        //    }
        //    else{
        //        dataViewController.isQuoteApproved = YES;
        //    }
        dataViewController.strQuoteNum = [[[aryDisFilterData objectAtIndex:[indexPath row]]objectForKey:@"QuoteNo"]objectForKey:@"text"];
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
        
    }
    else{
        
        QuoteStatusDetailViewController *dataViewController = [[QuoteStatusDetailViewController alloc] initWithNibName:@"QuoteStatusDetailViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.dictProductDetails = [arrQuoteList objectAtIndex:[indexPath row]];
        //
        //    if ([[[arrQuoteList objectAtIndex:[indexPath row]] objectForKey:@"CustOrder"] isEqualToString:@""]) {
        //        dataViewController.isQuoteApproved = NO;
        //    }
        //    else{
        //        dataViewController.isQuoteApproved = YES;
        //    }
        dataViewController.strQuoteNum = [[[arrQuoteList objectAtIndex:[indexPath row]]objectForKey:@"QuoteNo"]objectForKey:@"text"];
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }

    
    
    
}



#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrQuoteList count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblQuoteList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}

#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 600.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblQuoteList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    [operationQueue addOperationWithBlock:^{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetQuoteListFromDB:db];
        }];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
    
}

- (void) didFinishLoadingMoreSampleData
{
	_loadingInProgress = NO;
    [tblQuoteList setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    return tblQuoteList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}





#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        isSearchMode = YES;
        
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    
    isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [tblQuoteList reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    [tblQuoteList reloadData];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    @try {
        if([scope isEqualToString:@"Quote No"])
        {
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"QuoteNo.text", searchText];
            
            aryDisFilterData = [self.arrQuoteList filteredArrayUsingPredicate:predicate];
            
        }
        else
        {
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"DelName.text", searchText];
            
//            aryDisFilterData = [self.filteredListContent filteredArrayUsingPredicate:predicate];
            aryDisFilterData = [self.arrQuoteList filteredArrayUsingPredicate:predicate];

        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Erro::%@",exception.description);
    }
    @finally {
        [self.tblQuoteList reloadData];
    }
    
}

#pragma mark - Database calls
-(void)callGetQuoteListFromDB:(FMDatabase *)db{
    FMResultSet *rs;
    
    @try {
        //Get Customer list
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum, RECNUM as Recum, QUOTE_NO as QuoteNo, DEBTOR as Debtor, DEL_NAME as DelName, CUST_ORDER as CustOrder, DATE_RAISED as DateRaised, created_date, modified_date FROM soquohea ORDER BY modified_date DESC LIMIT ?,? ",[NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [arrQuoteList addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        [rs close];
        
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
}
-(void)callGetQuoteListByDebtor:(FMDatabase *)db Debtor:(NSString *)debtor{
    
    FMResultSet *rs;
    
    @try {
        
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum, RECNUM as Recum, QUOTE_NO as QuoteNo, DEBTOR as Debtor, DEL_NAME as DelName, CUST_ORDER as CustOrder, DATE_RAISED as DateRaised, created_date, modified_date FROM soquohea WHERE DEBTOR LIKE ?",[NSString stringWithFormat:@"%%%@%%", debtor]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}
-(void)callGetQuoteListByQuote:(FMDatabase *)db Quote:(NSString *)quote{
    
    FMResultSet *rs;
    @try {
        
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum, RECNUM as Recum, QUOTE_NO as QuoteNo, DEBTOR as Debtor, DEL_NAME as DelName, CUST_ORDER as CustOrder, DATE_RAISED as DateRaised, created_date, modified_date FROM soquohea WHERE QUOTE_NO LIKE ?",[NSString stringWithFormat:@"%%%@%%", quote]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

#pragma mark - WS calls
-(void)callWSGetQuoteList:(int)pageIndex
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_STATUS";
    //Fetch last sync date
    NSString *last_sync_date = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"soquohea"];
    NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d&last_syncDate=%@",pageIndex,ROWS_PER_PAGE,last_sync_date];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_STATUS_LIST_WS,parameters];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


@end
