//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "ViewQuoteDetailsViewController.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "ViewQuoteProductDetailsViewController.h"

#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

#define TAG_150 150
#define TAG_200 200

@interface ViewQuoteDetailsViewController ()
{
    MBProgressHUD *spinner;
}
@end

@implementation ViewQuoteDetailsViewController
@synthesize strQuoteNum,dictQuoteHeaderDetails,arrHeaderLabels,arrQuoteDetails,arrQuoteDetailsForWebservices,isSales;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lblQuoteNumber.text = strQuoteNum;
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    SEFilterControl *filter = [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
    [filter addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:filter];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
        // Path to the plist (in the application bundle)
        NSString *path = [AppDelegate getFileFromLocalDirectory:@"QuoteHeaderView" Type:@"plist"];
        
        // Build the array from the plist
        arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
        
        backgroundQueueForViewQuoteDetails = dispatch_queue_create("com.nanan.myscmipad.bgqueueForViewQuoteDetails", NULL);
        
        dictQuoteHeaderDetails = [[NSMutableDictionary alloc] init];
        arrQuoteDetails = [[NSMutableArray alloc] init];
        
        [[AppDelegate getAppDelegateObj].database open];
        
        [self callViewQuoteDetailsHeaderFromDB:[AppDelegate getAppDelegateObj].database];
        [self callViewQuoteDetailsDetailsFromDB:[AppDelegate getAppDelegateObj].database];
        [[AppDelegate getAppDelegateObj].database close];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -  Segmented Control Actions

-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
            
            if(!isNetworkConnected)
            {
                [tblHeader reloadData];
            }
            else{
                if ([[ASIHTTPRequest_CentralizedHelper sharedHelper] getCurrentRequest]) {
                    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
                }
                
                //[spinner showLoadingView:self.view size:2];
                [self callWSViewQuoteDetailsHeader];
            }
            
        }
            break;
            
        case 1:{
            
            if(!isNetworkConnected)
            {
                if ([arrQuoteDetails count]) {
                    [vwContentSuperview addSubview:vwDetails];
                    [tblDetails reloadData];
                }
                else{
                    //No Products
                    [vwContentSuperview addSubview:vwNoProducts];
                }
                
            }
            else{
                if ([[ASIHTTPRequest_CentralizedHelper sharedHelper] getCurrentRequest]) {
                    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
                    
                }
                
                //[spinner showLoadingView:self.view size:2];
                [self callWSViewQuoteDetailsDetails];
            }
            
            
        }
            break;
            
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (tableView.tag) {
        case TAG_150:return 60;break;
        case TAG_200:return 50;break;
        default:return 44;break;
    }
    
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_150:return [arrHeaderLabels count];break;
            
        case TAG_200:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *array;
    switch (tableView.tag) {
       array=[arrHeaderLabels objectAtIndex:section];
        case TAG_150:return [array count];break;
            
        case TAG_200:return [arrQuoteDetails count];break;
            
        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
    
    switch (tableView.tag) {
        case TAG_150:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:4];
                
            }
            NSArray  *array=[arrHeaderLabels objectAtIndex:[indexPath section]];
            cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            switch ([indexPath section]) {
                case 0:{
                    if ([indexPath row] == 0) {
                        cell.lblValue.text = [[dictQuoteHeaderDetails objectForKey:@"QuoteNo"] objectForKey:@"text"];
                    }
                }
                    
                    break;
                    
                case 1:{
                    if ([indexPath row] == 0) {
                        cell.lblValue.text = [[dictQuoteHeaderDetails objectForKey:@"Debtor"] objectForKey:@"text"];
                    }
                }
                    
                    break;
                    
                case 2:{
                    if ([indexPath row] == 0) {
                        cell.lblValue.text = [[dictQuoteHeaderDetails objectForKey:@"Name"] objectForKey:@"text"];
                    }
                    if ([indexPath row] == 1) {
                        cell.lblValue.text = [[dictQuoteHeaderDetails objectForKey:@"QuoteDate"] objectForKey:@"text"];
                    }
                    
                    if ([indexPath row] == 2) {
                        cell.lblValue.text = [[dictQuoteHeaderDetails objectForKey:@"Branch"] objectForKey:@"text"];
                    }
                    
                    if ([indexPath row] == 3) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictQuoteHeaderDetails objectForKey:@"Status"] objectForKey:@"text"]];
                    }
                    if ([indexPath row] == 4) {
                        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictQuoteHeaderDetails objectForKey:@"Total"] objectForKey:@"text"]];
                    }
                    
                }
                    
                    break;
                    
                default:
                    break;
            }
            
            return cell;
            
        }break;
            
        case TAG_200:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:5];
                
            }
           
            cell.lblTitle.text = [[[arrQuoteDetails objectAtIndex:[indexPath row] ] objectForKey:@"Description"] objectForKey:@"text"];
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[arrQuoteDetails objectAtIndex:[indexPath row] ] objectForKey:@"Price"] objectForKey:@"text"]];
            
            return cell;
            
        }break;
            
        default:return 0;break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ViewQuoteProductDetailsViewController *dataViewController = [[ViewQuoteProductDetailsViewController alloc] initWithNibName:@"ViewQuoteProductDetailsViewController" bundle:[NSBundle mainBundle]];
    dataViewController.isFromViewQuoteDetails = YES;
    dataViewController.dictProductDetails = [arrQuoteDetails objectAtIndex:[indexPath row]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    
}

#pragma mark - IBAction Methods
- (IBAction)actionShowQuoteHistory:(id)sender{
    QuoteHistoryCustomerListViewController *dataViewController = [[QuoteHistoryCustomerListViewController alloc] initWithNibName:@"QuoteHistoryCustomerListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

#pragma mark - Database calls
-(void)callViewQuoteDetailsHeaderFromDB:(FMDatabase *)db{
    
    @try {
        
        //Get Header details
        FMResultSet *rs1 = [[AppDelegate getAppDelegateObj].database executeQuery:@"SELECT DEBTOR as Debtor,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,STATUS as Status,DEL_NAME as Name,BRANCH as Branch,QUOTE_VALUE as Total from soquohea where QUOTE_NO=?",strQuoteNum];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        if ([rs1 next])
        {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Branch"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp1 forKey:@"Branch"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"Debtor"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp2 forKey:@"Debtor"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Name"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp3 forKey:@"Name"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"QuoteDate"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp4 forKey:@"QuoteDate"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"QuoteNo"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp5 forKey:@"QuoteNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp6 forKey:@"Status"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"Total"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp7 forKey:@"Total"];
            
        }
        
        [rs1 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

        
        
    }
    
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db{
    
    [arrQuoteDetails removeAllObjects];
    
    @try {
        //Get Details details
        FMResultSet *rs2 = [db executeQuery:@"SELECT QUOTE_NO, LINE_NO as LineNo, ITEM as Item, DESCRIPTION as Description, PRICING_UNIT as PricingUnit, STATUS as Status,PRICE as Price,DISC_PERCENT as DiscPercent,EXTENSION as Extension FROM soquodet WHERE QUOTE_NO = ?",strQuoteNum];
        
        while ([rs2 next]) {
            
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Description"] forKey:@"text"];
            [dict setObject:dictTemp1 forKey:@"Description"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"DiscPercent"] forKey:@"text"];
            [dict setObject:dictTemp2 forKey:@"DiscPercent"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Extension"] forKey:@"text"];
            [dict setObject:dictTemp3 forKey:@"Extension"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"Item"] forKey:@"text"];
            [dict setObject:dictTemp4 forKey:@"Item"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"LineNo"] forKey:@"text"];
            [dict setObject:dictTemp5 forKey:@"LineNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Price"] forKey:@"text"];
            [dict setObject:dictTemp6 forKey:@"Price"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"PricingUnit"] forKey:@"text"];
            [dict setObject:dictTemp7 forKey:@"PricingUnit"];
            
            NSMutableDictionary *dictTemp8 = [[NSMutableDictionary alloc] init];
            [dictTemp8 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dict setObject:dictTemp8 forKey:@"Status"];
            
            [arrQuoteDetails addObject:dict];
            
            dictData=nil;
            
        }
        
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
    NSLog(@"arrQuoteDetails==%@",arrQuoteDetails);
    [tblDetails reloadData];
    
}

-(void)callViewQuoteDetailsFromDB:(FMDatabase *)db
{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
        
        //Get Header details
        FMResultSet *rs1 = [[AppDelegate getAppDelegateObj].database executeQuery:@"SELECT DEBTOR as Debtor,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,STATUS as Status,DEL_NAME as Name,BRANCH as Branch,QUOTE_VALUE as Total from soquohea where QUOTE_NO=?",strQuoteNum];
        
        
        if ([rs1 next])
        {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Branch"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp1 forKey:@"Branch"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"Debtor"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp2 forKey:@"Debtor"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Name"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp3 forKey:@"Name"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"QuoteDate"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp4 forKey:@"QuoteDate"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"QuoteNo"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp5 forKey:@"QuoteNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp6 forKey:@"Status"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"Total"] forKey:@"text"];
            [dictQuoteHeaderDetails setObject:dictTemp7 forKey:@"Total"];
            
            
        }
        
        [rs1 close];
        
        //Get Details details
        FMResultSet *rs2 = [db executeQuery:@"SELECT QUOTE_NO, LINE_NO as LineNo, ITEM as Item, DESCRIPTION as Description, PRICING_UNIT as PricingUnit, STATUS as Status,PRICE as Price,DISC_PERCENT as DiscPercent,EXTENSION as Extension FROM soquodet WHERE QUOTE_NO = ?",strQuoteNum];
        
        
        while ([rs2 next]) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Description"] forKey:@"text"];
            [dict setObject:dictTemp1 forKey:@"Description"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"DiscPercent"] forKey:@"text"];
            [dict setObject:dictTemp2 forKey:@"DiscPercent"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Extension"] forKey:@"text"];
            [dict setObject:dictTemp3 forKey:@"Extension"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"Item"] forKey:@"text"];
            [dict setObject:dictTemp4 forKey:@"Item"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"LineNo"] forKey:@"text"];
            [dict setObject:dictTemp5 forKey:@"LineNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Price"] forKey:@"text"];
            [dict setObject:dictTemp6 forKey:@"Price"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"PricingUnit"] forKey:@"text"];
            [dict setObject:dictTemp7 forKey:@"PricingUnit"];
            
            NSMutableDictionary *dictTemp8 = [[NSMutableDictionary alloc] init];
            [dictTemp8 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dict setObject:dictTemp8 forKey:@"Status"];
            
            [arrQuoteDetails addObject:dict];
            
        }
        
        [rs2 close];
        
    }];
    
    [tblHeader reloadData];
    
}


#pragma mark - WS calls
-(void)callWSViewQuoteDetailsHeader{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DETAILS_HEADER";
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",strQuoteNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DETAILS_HEADER_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewQuoteDetailsDetails{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DETAILS_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",strQuoteNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DETAILS_DETAILS_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"%@", dictResponse);
    
    [spinner removeFromSuperview];
    
    NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_DETAILS_HEADER"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            
            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                dictQuoteHeaderDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Data"];
                
            }
            
            dispatch_async(backgroundQueueForViewQuoteDetails, ^(void) {
                
                [dbQueueViewQuoteDetails inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictQuoteHeaderDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]) {
                            @throw [NSException exceptionWithName:@"Response Issue" reason:@"Scm Recnum can't be Null" userInfo:nil];
                        }
                        
                        BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `soquohea` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`, `DEL_NAME`,`DEL_DATE`, `DEBTOR`,`STATUS`, `created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", [[dictQuoteHeaderDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictQuoteHeaderDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictQuoteHeaderDetails objectForKey:@"QuoteNo"] objectForKey:@"text"],[[dictQuoteHeaderDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictQuoteHeaderDetails objectForKey:@"QuoteDate"] objectForKey:@"text"], [[dictQuoteHeaderDetails objectForKey:@"Debtor"] objectForKey:@"text"],[[dictQuoteHeaderDetails objectForKey:@"Status"] objectForKey:@"text"],[[dictQuoteHeaderDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictQuoteHeaderDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
                        
                        
                        if (!y)
                        {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        *rollback = NO;
                        [db commit];
                    }
                    @catch(NSException* e){
                        
                        *rollback = YES;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                        
                    }
                    
                    @finally {
                        [self callViewQuoteDetailsHeaderFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [spinnerObj hide:YES];
                            [tblHeader reloadData];
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                        
                    });
                    
                }];
                
            });
            
        }
        //False
        else{
            spinnerObj.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:HUD_COMPLETION_IMAGE]];
            spinnerObj.mode = MBProgressHUDModeCustomView;
            [spinnerObj hide:YES afterDelay:SPINNER_HIDE_TIME];
            
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_QUOTE_DETAILS_DETAILS"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Flag"]) {
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                [vwContentSuperview removeAllSubviews];
                
                if(!self.arrQuoteDetailsForWebservices){
                    arrQuoteDetailsForWebservices = [[NSMutableArray alloc] init];
                }
                else{
                    [arrQuoteDetailsForWebservices removeAllObjects];
                }
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
                    [arrQuoteDetailsForWebservices addObject:dict];
                    
                }
                else{
                    self.arrQuoteDetailsForWebservices = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
                }
                
                dispatch_async(backgroundQueueForViewQuoteDetails, ^(void) {
                    
                    [dbQueueViewQuoteDetails inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        @try {
                            for (NSDictionary *dict in arrQuoteDetailsForWebservices){
                                
                                if (![[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"]) {
                                    @throw [NSException exceptionWithName:@"Response Issue" reason:@"Scm Recnum can't be Null" userInfo:nil];
                                }
                                
                                
                                BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `soquodet` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`, `DEBTOR`, `PRICING_UNIT`,`PRICE`,`DISC_PERCENT`, `EXTENSION`,`STATUS`, `DESCRIPTION`, `ITEM`, `LINE_NO`, `created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [[dict objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strQuoteNum ,[[dict objectForKey:@"Debtor"] objectForKey:@"text"],[[dict objectForKey:@"PricingUnit"] objectForKey:@"text"],[[dict objectForKey:@"Price"] objectForKey:@"text"], [[dict objectForKey:@"DiscPercent"] objectForKey:@"text"], [[dict objectForKey:@"Extension"] objectForKey:@"text"],[[dict objectForKey:@"Status"] objectForKey:@"text"],[[dict objectForKey:@"Description"] objectForKey:@"text"],[[dict objectForKey:@"Item"] objectForKey:@"text"],[[dict objectForKey:@"LineNo"] objectForKey:@"text"],[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
                                
                                
                                if (!y)
                                {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                            }
                            *rollback = NO;
                            [db commit];
                        }
                        @catch(NSException* e){
                            
                            *rollback = YES;
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                
                            });
                            
                            
                        }
                        
                        @finally {
                            [self callViewQuoteDetailsDetailsFromDB:db];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            @try
                            {
                                [spinnerObj hide:YES];
                                
                                if ([arrQuoteDetails count]) {
                                    [vwContentSuperview addSubview:vwDetails];
                                    [tblDetails reloadData];
                                    
                                    //NSString *path = [AppDelegate getFileFromDocumentsDirectory:@"QuoteDetails.plist"];
                                    //[arrQuoteDetails writeToFile:path atomically:YES];
                                }
                                else{
                                    //No Products
                                    [vwContentSuperview addSubview:vwNoProducts];
                                }
                            }
                            @catch(NSException *e)
                            {
                                NSLog(@"Exception : %@",[e reason]);
                                //[db close];
                            }
                            
                            
                        });
                        
                    }];
                    
                });
                
            }
            else{
                [vwContentSuperview addSubview:vwNoProducts];
                
            }
            
            
        }
        //False
        else{
            [spinnerObj hide:YES];
            
            [vwContentSuperview removeAllSubviews];
            [vwContentSuperview addSubview:vwNoProducts];
            
        }
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}
@end
