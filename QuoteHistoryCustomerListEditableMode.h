//
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"
#import "MBProgressHUD.h"

@interface QuoteHistoryCustomerListEditableMode : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate>{

    NSString *strWebserviceType;
    
    int currPage;
    int totalCount;
    int recordNumber;
    
    NSMutableArray *arrCustomer; // The master content.
    NSMutableArray *arrCustomerForWebservices;
    
      
    PullToLoadMoreView* _loadMoreFooterView;
    NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
    
    dispatch_queue_t backgroundQueueForCustomerListEdit;
    FMDatabaseQueue *dbQueueCustomerListEdit;
    
    BOOL isNetworkConnected;
    NSOperationQueue *operationQueue;
    NSMutableDictionary *mainDict;
}

//@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContentEdit;
@property (nonatomic, retain) NSMutableArray *arrCustomer;
@property (nonatomic, retain) NSMutableArray *arrCustomerForWebservices;

@property (nonatomic, retain) MBProgressHUD *spinner;
@property (nonatomic,unsafe_unretained)IBOutlet UITableView *tblQuoteList; 
@property (nonatomic,unsafe_unretained) IBOutlet UISearchBar *searchingBar;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwResults;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;
@property (nonatomic, retain) NSIndexPath *lastIndexPath;

@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

@property (nonatomic, retain) NSMutableDictionary *mainDict;

#pragma mark - Custom Methods
-(void)callWSGetCustomerList:(int)pageIndex;
-(void)callGetCustomerListFromDB:(FMDatabase *)db;
@end
