//
//  DashboardViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpecialsView.h"
#import "ASINetworkQueue.h"
#import "JFDepthView.h"
#import "TopViewController.h"

@interface DashboardViewController : UIViewController<PullableViewDelegate,UITableViewDataSource,UITableViewDelegate,ReachabilityRequest_delegate,JFDepthViewDelegate,UIGestureRecognizerDelegate>{
    SpecialsView *viewSpecials;

    NSOperationQueue *operationQueue;
    
    NSMutableArray *arrNewProducts;
    NSMutableArray *arrNoticeBoards;
    NSMutableDictionary *dictSpecials;
    
    NSString *strSpecialsPdfUrl;
    NSString *strSpecialsPdfThumbnailUrl;
    
    //CMPopTipView *popTipView;

    dispatch_queue_t backgroundQueueForSpecials;
    
    FMDatabaseQueue *dbQueueNewProducts;
    FMDatabaseQueue *dbQueueNoticeMessage;
    FMDatabaseQueue *dbQueueSpecials;
    
    BOOL isNetworkConnected;

}

@property (nonatomic, retain) NSMutableArray *arrNewProducts;
@property (nonatomic, retain) NSMutableArray *arrNoticeBoards;
@property (nonatomic, retain) NSMutableDictionary *dictSpecials;
@property (nonatomic, retain) NSString *strSpecialsPdfUrl;
@property (nonatomic, retain) NSString *strSpecialsPdfThumbnailUrl;
@property (nonatomic, retain) SpecialsView *viewSpecials;

@property (nonatomic,retain)IBOutlet UIView *vwNewMessagesContentView;
@property (nonatomic,retain)IBOutlet UIView *vwNewProductsContentView;
@property (nonatomic,retain)IBOutlet UIView *vwNewProducts;
@property (nonatomic,retain)IBOutlet UIView *vwNewMessages;
@property (nonatomic,retain)IBOutlet UIView *vwNoProducts;
@property (nonatomic,retain)IBOutlet UIView *vwNoMessages;

@property (nonatomic,unsafe_unretained)IBOutlet UITableView *tblNoticeBoard;
@property (nonatomic,unsafe_unretained)IBOutlet UITableView *tblNewProducts;
@property (nonatomic,unsafe_unretained)IBOutlet UIActivityIndicatorView *localSpinner1;
@property (nonatomic,unsafe_unretained)IBOutlet UIActivityIndicatorView *localSpinner2;
@property (nonatomic,retain)UIActivityIndicatorView *localSpinner3;

-(void)openPdfForSpecials;
-(void)openPdfForAttachment:(id)sender;
@end
