//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "OfflineAddQuoteViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "QuoteProductListViewController.h"
#import "QuoteHistoryCustomerListEditableMode.h"
#import "OfflineAddQuoteViewController.h"

#define TAG_50 50
#define TAG_100 100

#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10

#define QUOTE_SAVE_HEADER_WS @"quote/adddebtordetails_quote.php?"
#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

@interface OfflineAddQuoteViewController ()

@end

@implementation OfflineAddQuoteViewController

@synthesize arrHeaderLabels,arrProducts,dictHeaderDetails,strQuoteNum,popoverController,strQuoteNumRecived,EditableMode,deleteIndexPath,deleteRowIndex,arrSelectedProducts,strDebtorNum;

@synthesize btnEmail,btnPrint,lblHeader,tblHeader,tblDetails,vwContentSuperview,vwDetails,vwHeader,vwNoProducts,vwSegmentedControl,btnDelete,btnAddMenu,btnAddProducts,btnFinalise,btnLoadProfile,btnSaveDetails,imgArrowDownwards,imgArrowRightWards,isCommentAddedOnLastLine;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
//    self.isCommentAddedOnLastLine = NO;
    self.isCommentAddedOnLastLine = YES; // 14thNov

    
    //spinner = [[LoadingMethods alloc] init];
    
    lblHeader.text = @"View Quote";
    
    strQuoteNum = [[NSString alloc] init];
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"QuoteHeaderAdd" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    arrProducts = [[NSMutableArray alloc] init];
    arrSelectedProducts = [[NSMutableArray alloc] init];
    
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    tempArrForEdit = [[NSMutableArray alloc] init];
    
    
    CATransform3D transform = CATransform3DIdentity;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"pointing_down" ofType:@"png"];
    UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    layerPointingImage = [CALayer layer];
    layerPointingImage.contents = (id)downImage.CGImage;
    layerPointingImage.bounds = CGRectMake(0, 0, imgArrowDownwards.frame.size.width, imgArrowDownwards.frame.size.height);
    layerPointingImage.position = CGPointMake(10,30);
    layerPointingImage.transform = CATransform3DTranslate(transform, 0.0, 6,0.0);
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
    chkFinalize = NO;
    
    //Notification to reload table when products added to details from product list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadAddProductDetailsNotification" object:nil];
    
    //Notification to reload table when debtor is selected from Debtor list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHeader:) name:@"ReloadAddProductHeadersNotification" object:nil];
    
    backgroundQueueForNewQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForNewQuote", NULL);
    
    btnLoadProfile.hidden = YES;
    segControl.enabled = YES;
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(backgroundQueueForNewQuote, ^{
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callViewQuoteDetailsHeaderFromDB:db];
            [self callViewQuoteDetailsDetailsFromDB:db];
            
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            spinner.mode = MBProgressHUDModeIndeterminate;
            [spinner removeFromSuperview];
            [tblHeader reloadData];
        });
    });
    
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    
    self.btnEmail = nil;
    self.btnPrint = nil;
    self.lblHeader = nil;
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.vwContentSuperview = nil;
    self.vwDetails = nil;
    self.vwHeader = nil;
    self.vwNoProducts = nil;
    self.vwSegmentedControl = nil;
    self.btnDelete = nil;
    self.btnAddMenu = nil;
    self.btnAddProducts = nil;
    self.btnFinalise = nil;
    self.btnLoadProfile = nil;
    self.btnSaveDetails = nil;
    self.imgArrowDownwards = nil;
    self.imgArrowRightWards = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    btnDelete.hidden = YES;
    
    if (EditableMode==NO) {
        //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    }
    
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
        }
            break;
            
        case 1:{
            [self doAfterDataFetched];
        }
            break;
            
        default:
            break;
    }
    
}



#pragma mark Table view data source and delegate

#pragma mark Table view data source and delegate
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 0) {
                return nil;
            }
            else if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Name"];
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address1"];
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address2"];
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address3"];
                    }
                        break;
                        
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
            }
            else{
                return indexPath;
            }
            
        }break;
            
        case TAG_100:return indexPath;break;
            
        default:return nil;break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Name"];
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address1"];
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address2"];
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address3"];
                    }
                        break;
                        
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                    //return 60;
                }
            }
            else{
                return 60;break;
            }
            
            
            
        }break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];break;
            
        case TAG_100:return [arrProducts count];break;
            
        default:return 0;break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (tableView.tag) {
            
        case TAG_50:{
            if (section == 1) {
//                return @"Please Select Debtor";
                return nil;
            }
        }break;
            
        default:break;
    }
    // Return the displayed title for the specified section.
    return nil;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
    switch (tableView.tag) {
        case TAG_50:{
            
            //Debtor
            if ([indexPath section] == 1 && [indexPath row] == 0) {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:3];
                    
                }
                
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                
                cell.txtValue.userInteractionEnabled = FALSE;
                cell.txtValue.placeholder = @"Please Select";
                
            }
            else{
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:4];
                    
                }
                
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            
            cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            if (selectedSectionIndex == 2 && selectedIndex == indexPath.row) {
                
                CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                 cell.lblValue.frame.origin.y,
                                                 cell.lblValue.frame.size.width,
                                                 labelHeight);
                
            }
            else {
                
                //Otherwise just return the minimum height for the label.
                cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                 cell.lblValue.frame.origin.y,
                                                 cell.lblValue.frame.size.width,
                                                 COMMENT_LABEL_MIN_HEIGHT);
            }
            
            cell.txtValue.inputAccessoryView = keyboardToolBar;
            cell.lblValue.text = nil;
            
            if ([indexPath section] == 0) {
                switch ([indexPath row]) {
                    case 0:{
                        
                        if (EditableMode == NO) {
                            if ([dictHeaderDetails objectForKey:@"QuoteNum"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"QuoteNum"]];
                            }
                        }else{
                            cell.lblValue.text = strQuoteNumRecived;
                        }
                        break;
                    }
                    default:break;
                }
            }
            
            if ([indexPath section] == 1) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"Debtor"]) {
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
                        }
                        break;
                    default:break;
                }
            }
            
            if ([indexPath section] == 2) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"Name"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Name"]];
                        }
                        
                        break;
                    case 1:
                        
                        if ([dictHeaderDetails objectForKey:@"Address1"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address1"]];
                        }
                        break;
                    case 2:
                        
                        if ([dictHeaderDetails objectForKey:@"Address2"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address2"]];
                        }
                        break;
                    case 3:
                        
                        if ([dictHeaderDetails objectForKey:@"Address3"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address3"]];
                            
                        }
                        break;
                    case 4:
//                        if ([dictHeaderDetails objectForKey:@"Suburb"]) {
//                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Suburb"]];
//                            
//                        }
                        if ([dictHeaderDetails objectForKey:@"DeliveryDate"])
                        {
                            NSString *strDate = [dictHeaderDetails objectForKey:@"DeliveryDate"];
                            
                            NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                            [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            
                            NSDate *theDate = [inputFormatter dateFromString:strDate];
                            
                            NSDateFormatter *inputFormatterNew = [[NSDateFormatter alloc] init];
                            [inputFormatterNew setDateFormat:@"dd-MM-yyyy"];
                            
                            NSString *strNewDate = [inputFormatterNew stringFromDate:theDate];
                            
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",strNewDate];
                            
                        }

                        break;
                    case 5:
                        if ([dictHeaderDetails objectForKey:@"PostCode"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PostCode"]];
                            
                        }
                        
                        break;
                    case 6:
                        if ([dictHeaderDetails objectForKey:@"Country"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Country"]];
                            
                        }
                        break;
                        
                    default:
                        break;
                }
                
            }
            
            if ([indexPath section] == 3) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"DelName"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelName"];
                        }
                        
                        break;
                    case 1:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress1"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress1"];
                        }
                        break;
                    case 2:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress2"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress2"];
                        }
                        break;
                    case 3:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress3"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress3"];
                            
                        }
                        break;
                    case 4:
                        if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelPostCode"];
                            
                        }
                        break;
                    case 5:
                        if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelCountry"];
                            
                        }
                        
                        break;
                        
                    default:
                        break;
                }
                
            }
            
            
            return cell;
            
        }break;
            
        case TAG_100:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:5];
            }
            
        
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString *str = [NSString stringWithFormat:@"/C"];
            
            if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                
                NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]];
                
                if ([str rangeOfString:str2].location == NSNotFound) {
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                }
                else
                {
                    NSString *str111 = [NSString stringWithFormat:@"/C"];
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str111,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    cell.lblValue.text = @"";
                }
            }
            else{
                
                NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
                
                if ([str rangeOfString:str2].location == NSNotFound) {
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                }
                else
                {
                    NSString *str111 = [NSString stringWithFormat:@"/C"];
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str111,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    cell.lblValue.text = @"";
                }
                
                
            }
            return cell;
        }break;
            
        default:return 0;break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case TAG_50:{
            
            if ([indexPath section] == 1 && [indexPath row] == 0) {
                
                if (chkFinalize == NO) {
                    
                    NSLog(@"EditableMode yes chkfinalaizze no abcdViewcontroller");
                    
                    QuoteHistoryCustomerListEditableMode *dataViewController = [[QuoteHistoryCustomerListEditableMode alloc] initWithNibName:@"QuoteHistoryCustomerListEditableMode" bundle:[NSBundle mainBundle]];
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                }
            }
            if ([indexPath section] == 2) {
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    
                    return;
                }
                
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }break;
            
        case TAG_100:{
            
            
            if (chkFinalize == NO) {
                
                NSString *str = [NSString stringWithFormat:@"/C"];
                NSString *str2 = nil;
                if([[arrProducts objectAtIndex:indexPath.row] valueForKey:@"Item"]){
                    str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]];
                }
                else if([[arrProducts objectAtIndex:indexPath.row] valueForKey:@"StockCode"]){
                    str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
                }
                
                if (str2) {
                    if ([str rangeOfString:str2].location == NSNotFound)
                    {
                        ViewQuoteProductDetailsViewController *dataViewController = [[ViewQuoteProductDetailsViewController alloc] initWithNibName:@"ViewQuoteProductDetailsViewController" bundle:[NSBundle mainBundle]];
                        NSLog(@"pRODUCT Details : %@",[arrProducts objectAtIndex:[indexPath row]]);
                        dataViewController.delegate = self;
                        dataViewController.strDebtor = strDebtorNum;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                        
                    }
                    else{
                        
                        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                        
                    }
                    
                }
            }
            
        }break;
    }
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    switch (tableView.tag) {
            
        case TAG_50:{
            return NO;break;
        }break;
            
        case TAG_100:{
            return YES;break;
        }break;
            
        default:return NO;break;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
            
        case TAG_100:{
            if (editingStyle == UITableViewCellEditingStyleDelete)
            {
                
                if ([arrProducts count]) {
                    self.deleteIndexPath = indexPath;
                    self.deleteRowIndex = indexPath.row;
                    [self deleteProductInEditableMode:deleteRowIndex];
                }
                
                
            }
        }break;
    }
}

#pragma mark - Delete delegates
-(void)setCommentPosition:(BOOL)status
{
    self.isCommentAddedOnLastLine = status;
}


-(void)deleteProductInEditableMode:(int)indexCount
{
    //NSLog(@"indexCount==%d",indexCount);
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db) {
        [self callDeleteProductFromDB:db andIndexCountTempEdit:indexCount];
    }];
    
    [self doAfterdelete];
}

-(void)callDeleteProductFromDB:(FMDatabase*)db andIndexCountTempEdit:(int)indexCountTempEdit
{
    //NSLog(@"indexCountTempEdit==%d",indexCountTempEdit);
    
    @try {
        
        [db executeUpdate:@"DELETE FROM `soquodet_offline` WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNumRecived,[[arrProducts objectAtIndex:indexCountTempEdit]objectForKey:@"Item"]];
        
        NSLog(@"arrProducts %@",arrProducts);
        [arrProducts removeObjectAtIndex:indexCountTempEdit];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

#pragma mark - IBActions

- (IBAction)actionShowComment:(id)sender{
    [self showCommentView];
}

//Load Profile
- (IBAction)actionShowQuoteHistory:(id)sender{
    //Change the selection
    [segControl setSelectedIndex:1];
    
    QuoteHistoryCustomerListEditableMode *dataViewController = [[QuoteHistoryCustomerListEditableMode alloc] initWithNibName:@"QuoteHistoryCustomerListEditableMode" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (void)showCommentView{
    __block BOOL isCommentExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
        dataViewController.commentStr = @"";
        dataViewController.commntTAg = 0;
        dataViewController.delegate = self;
        dataViewController.productIndex = -1;
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
}

- (void)actionSaveQuote:(int)finalizeTag
{
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts) {
        
        //Dont check comments
        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])){
            if (![dict objectForKey:@"ExtnPrice"]) {
                isCalculationPending = TRUE;
                break;
            }
            
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }
    else{
        [self callSaveQuoteToDB:finalizeTag];
    }
    
    
}

//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([tblDetails isEditing]) {
        [self showOrEnableButtons];
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblDetails setEditing:NO animated:YES];
    }
    else{
        
        [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        [tblDetails setEditing:YES animated:YES];
    }
    
    [tblDetails reloadData];
}

- (IBAction)actionShowProductList:(id)sender{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (IBAction)actionDisplayQuoteMenu:(id)sender{
    
    //tagFlsh = 1;
    //[self flashFinal:btnAddMenu];
    
    QuoteMenuPopOverViewController *popoverContent = [[QuoteMenuPopOverViewController alloc] initWithNibName:@"QuoteMenuPopOverViewController" bundle:[NSBundle mainBundle]];
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    popoverContent.isFinalisedNeeded = NO;
    popoverContent.isFreightNotNeeded = YES;
    popoverContent.isProfileOrderEntry = NO;
    popoverContent.isFromOfflineOrder = NO;
    
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    popoverController.delegate = self;
    popoverContent.delegate = self;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_x_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect rectBtnMenu = CGRectMake(btnAddMenu.frame.origin.x, self.view.frame.size.height - 40,btnAddMenu.frame.size.width,btnAddMenu.frame.size.height);
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:rectBtnMenu
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}

#pragma mark - Database call

-(void)callSaveQuoteToDB:(int)finalizeTag
{
    strWebserviceType = @"DB_QUOTE_SAVE";
    
    
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strDelAddress1 = @"";
    NSString *strDelAddress2 = @"";
    NSString *strDelAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = @"";
    NSString *strBranch = @"";
    NSString *strDelDate = @"";
    NSString *strCarrier = @"";
    NSString *strDeliveryRun = @"";
    NSString *strDeliveryDate = @"";
    
    NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
    
    NSLog(@"strQuoteNum %@",strQuoteNum);
    
    if([dictHeaderDetails objectForKey:@"CARRIER"])
    {
        strCarrier = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CARRIER"]];
    }
    
    if([dictHeaderDetails objectForKey:@"DeliveryDate"])
    {
        strDeliveryDate = [dictHeaderDetails objectForKey:@"DeliveryDate"];
    }
    
    
    if([dictHeaderDetails objectForKey:@"DeliveryRun"])
    {
        strDeliveryRun = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryRun"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Debtor"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Name"]) {
        strDelName= [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Name"] trimSpaces]];
        
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress1"]) {
        strDelAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress2"]) {
        strDelAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress3"]) {
        strDelAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress3"] trimSpaces]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Suburb"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelPostCode"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelCountry"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"ReleaseType"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Status"]) {
        strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Warehouse"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Branch"]) {
        strBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Branch"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDelDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DeliveryDate"] trimSpaces]];
    }
    
    
    NSString *strContact = @"";
    if ([dictHeaderDetails objectForKey:@"Contact"]) {
        strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
    }
    
    NSString *strPriceCode = @"";
    if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
    }
    
    NSString *strSalesman = @"";
    if ([dictHeaderDetails objectForKey:@"Salesman"]) {
        strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
    }
    
    NSString *strSalesBranch = @"";
    if ([dictHeaderDetails objectForKey:@"SalesBranch"]) {
        strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SalesBranch"]];
    }
    
    NSString *strTradingTerms = @"";
    if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
        strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
    }
    
    NSString *strExchangeCode = @"";
    if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
        strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
    }
    
    NSString *strChargetype = @"";
    if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
        strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
    }
    
    NSString *strExportDebtor = @"";
    if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
        strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
    }
    
    NSString *strChargeRate = @"";
    if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
        strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
    }
    
    NSString *strHeld = @"";
    if ([dictHeaderDetails objectForKey:@"Held"]) {
        strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
    }
    
    NSString *strNumboxes = @"";
    if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
        strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
    }
    
    NSString *strDirect = @"";
    if ([dictHeaderDetails objectForKey:@"Direct"]) {
        strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
    }
    
    NSString *strDateRaised = @"";
    if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
    }
    
    NSString *strPeriodRaised = @"";
    if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
        strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
    }
    
    NSString *strYearRaised = @"";
    if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
        strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
    }
    
    NSString *strDropSequence = @"";
    if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
        strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
    }
    
    
    NSString *strActive = @"N";
    NSString *strEditStatus = @"11";
    NSString *strExchangeRate = @"1.000000";
    
    //Set status = 1
    strStatus = STATUS;
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        BOOL isSomethingWrongHappened = FALSE;
        @try
        {
            
            FMResultSet *rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea_offline WHERE QUOTE_NO = ?",strQuoteNum];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            BOOL y2 = FALSE;
            
            //Update
            if ([rs next]) {
                
                y2 = [db executeUpdate:@"UPDATE `soquohea_offline` SET `QUOTE_NO` = ?, `DEBTOR` =?, `DEL_NAME`=?, `DEL_ADDRESS1`=?, `DEL_ADDRESS2`=?, `DEL_ADDRESS3`=?,`isFinalised`=?,`isUploadedToServer`=? WHERE QUOTE_NO = ?", strQuoteNum, strDebtor, strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strisFinalised,[NSNumber numberWithInt:0],strQuoteNum];
            }
            
            //Insert
            else{
                
                y2 = [db executeUpdate:@"INSERT INTO `soquohea_offline` (`QUOTE_NO`, `DEBTOR`, `DEL_NAME`, `DEL_ADDRESS1`, `DEL_ADDRESS2`, `DEL_ADDRESS3`, `BRANCH`,`DEL_DATE`,`DEL_COUNTRY`,`DEL_INST1`,`DEL_INST2`,`DEL_POST_CODE`,`RELEASE_TYPE`,`STATUS`,`WAREHOUSE`,`isFinalised`,`isUploadedToServer`,ACTIVE,CHARGE_RATE,CHARGE_TYPE,DATE_RAISED,DROP_SEQ,DIRECT,EDIT_STATUS,EXCHANGE_CODE,EXCHANGE_RATE,EXPORT_DEBTOR,HELD,NUM_BOXES,PERIOD_RAISED,PRICE_CODE,RELEASE_TYPE,SALESMAN,SALES_BRANCH,TRADING_TERMS,YEAR_RAISED,CARRIER,PRICE_CODE,DELIVERY_RUN) VALUES (?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?,?,?)", strQuoteNum, strDebtor, strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strReleaseType,strStatus,strWarehouse,strisFinalised,[NSNumber numberWithInt:0],strActive,strChargeRate,strChargetype,strDateRaised,strDropSequence,strDirect,strEditStatus,strExchangeCode,strExchangeRate,strExportDebtor,strHeld,strNumboxes,strPeriodRaised,strPriceCode,strReleaseType,strSalesman,strSalesBranch,strTradingTerms,strYearRaised,strCarrier,strPriceCode,strDeliveryRun];
            }
            
            [rs close];
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            float totalWeight = 0;
            float totalVolume = 0;
            float totalTax = 0;
            
            NSString *strLineNumber = @"0";
            NSString *strConvFactor = @"1.000";
            NSString *strOrigCost = @"0";
            float totalQuoteValue;
            int i = 0;
            int detail_lines = 0;
            for (NSDictionary *dict in arrProducts)
            {
                
                NSString *strStockCode = nil;
                NSString *strPrice = @"0";
                NSString *strCost = @"0";
                NSString *strDescription = nil;
                NSString *strExtension = @"0";
                NSString *strQuantityOrdered = @"0";
                NSString *strWeight = @"0";
                NSString *strVolume = @"0";
                NSString *strTax = @"0";
                NSString *strGross = @"0";
                
                NSString *strDissection = @"";
                NSString *strDissection_Cos = @"";

                strLineNumber = [NSString stringWithFormat:@"%d",++i];
                
                //Comments
                if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]){
                    
                    detail_lines++;
                    strStockCode = @"/C";
                    strDescription = [[dict objectForKey:@"Description"] trimSpaces];
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                }
                else{
                    
                    strExtension = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ExtnPrice"]];
                    if(strExtension.length > 0)
                    {
                        detail_lines++;
                        
                        strDissection = [dict objectForKey:@"Dissection"];
                        strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
                        
                        totalQuoteValue += [strExtension floatValue];
                        
                        strStockCode = [dict objectForKey:@"StockCode"];
                        strPrice = [dict objectForKey:@"SellingPrice"];
                        strCost = [dict objectForKey:@"Cost"];
                        
                        strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                        
                        strWeight = [dict objectForKey:@"Weight"];
                        strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                        
                        totalWeight += [strWeight floatValue];
                        
                        strVolume = [dict objectForKey:@"Volume"];
                        strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                        
                        totalVolume += [strVolume floatValue];
                        
                        strTax = [dict objectForKey:@"Tax"];
                        strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
                        
                        totalTax += [strTax floatValue];
                        
                        strDescription = [dict objectForKey:@"Description"];
                        
                        
                        strGross = [dict objectForKey:@"Gross"];
                    }
                    
                }
                
                
                FMResultSet *rs3 =  [db executeQuery:@"SELECT QUOTE_NO FROM soquodet_offline WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strStockCode];
                
                BOOL y = FALSE;
                //Update
                if ([rs3 next]){
                    
                    y = [db executeUpdate:@"UPDATE `soquodet_offline` SET `QUOTE_NO` = ?, `DEBTOR`= ?, `DESCRIPTION`= ?, `EXTENSION`= ?, `ITEM`= ?, `PRICE`= ?,LINE_NO = ?,QUANTITY = ?,ALT_QTY = ?,WEIGHT = ?,VOLUME = ?,CONV_FACTOR = ?,COST = ?,ORIG_COST = ?,GROSS = ?,TAX = ?,STATUS = ?, `isUploadedToServer`= ?,EDIT_STATUS = ? WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,strStatus,[NSNumber numberWithInt:0],strEditStatus,strQuoteNumRecived,strStockCode];
                    
                }
                else{
                    
                    y = [db executeUpdate:@"INSERT INTO `soquodet_offline` (`QUOTE_NO`, `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,STATUS,`isUploadedToServer`,EDIT_STATUS,WAREHOUSE,BRANCH,PRICE_CODE,DECIMAL_PLACES,DISSECTION,DISSECTION_COS) VALUES (?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",strQuoteNumRecived,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,strStatus,[NSNumber numberWithInt:0],strEditStatus,strWarehouse,strBranch,strPriceCode,@"2",strDissection,strDissection_Cos];
                    
                }
                
                [rs3 close];
                
                if (!y)
                {
                    isSomethingWrongHappened = TRUE;
                    
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    
                }
                
            }
            
            
            
            NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
            NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
            NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
            NSString *strDetailLines = [NSString stringWithFormat:@"%d",detail_lines];
            
            BOOL x = [db executeUpdate:@"UPDATE `soquohea_offline` SET DETAIL_LINES = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ?,QUOTE_VALUE = ? WHERE QUOTE_NO = ?", strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,[NSString stringWithFormat:@"%f",totalQuoteValue],strQuoteNum];
            
            if (!x)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            *rollback = NO;
            
            NSString *strSuccessMessage = [NSString stringWithFormat:@"Quote Updated Successfully"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadOfflinQuoteListAfterEdit"
                                                                object:self
                                                              userInfo:nil];
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];

        }
        
    }];
    
    
    
    
    
    
}

-(void)callGetNewQuoteNumberFromDB{
    
    strWebserviceType = @"DB_GET_QUOTE_NUMBER";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *salesType = [prefs stringForKey:@"salesType"];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db)  {
        
        @try {
            
            FMResultSet *rs = [db executeQuery:@"SELECT last_quote_num FROM (SELECT QUOTE_NO as last_quote_num FROM soquohea WHERE QUOTE_NO LIKE ? UNION SELECT QUOTE_NO as last_quote_num FROM soquohea_offline WHERE QUOTE_NO LIKE ?)ORDER BY rowid DESC LIMIT 1",[NSString stringWithFormat:@"%@%%", salesType],[NSString stringWithFormat:@"%@%%", salesType]];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            int numNewQuoteNum;
            if ([rs next]) {
                
                NSString *strLastQuoteNum = [[rs resultDictionary] objectForKey:@"last_quote_num"];
                strLastQuoteNum = [[strLastQuoteNum componentsSeparatedByString:salesType] lastObject];
                
                int numLastQuoteNum = [strLastQuoteNum integerValue];
                
                numNewQuoteNum  = ++numLastQuoteNum;
                
                
            }
            //No record
            else{
                numNewQuoteNum = 1;
            }
            
            self.strQuoteNum = [NSString stringWithFormat:@"%@%d",salesType,numNewQuoteNum];
            
            [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
            
            NSLog(@"dictHeaderDetails==%@",dictHeaderDetails);
            
            [tblHeader reloadData];
            
            [rs close];
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            //[spinner hideLoadingView];
            
        }
        
    }];
}

-(void)callViewQuoteDetailsHeaderFromDB:(FMDatabase *)db{
    FMResultSet *rs1;
    @try {
        
        
        rs1 = [db executeQuery:@"SELECT PRICE_CODE as PriceCode,CUST_ORDER as CustOrder,DEL_DATE as DeliveryDate,DEBTOR as Debtor,BRANCH as Branch, DEBTOR as Debtor,DEL_ADDRESS1 as Address1,DEL_ADDRESS2 as Address2,DEL_ADDRESS3 as Address3,DEL_COUNTRY as Country,DEL_NAME as Name,DEL_POST_CODE as PostCode,DEL_SUBURB as Suburb,WAREHOUSE as Warehouse,DEL_INST1 as DelInst1, DEL_INST2 as DelInst2,RELEASE_TYPE as ReleaseType,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,QUOTE_VALUE as Total,STATUS as Status,isFinalised from soquohea_offline WHERE QUOTE_NO=?",strQuoteNumRecived];
        
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next])
        {
            self.dictHeaderDetails = [rs1 resultDictionary];
            self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNo"];
            
            //NSLog(@"[rs1 resultDictionary]==%@",[rs1 resultDictionary]);
            
            //Check the given quote finalised or not
            if (EditableMode == YES) {
                if ([[NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"isFinalised"]] isEqualToString:@"1"]) {
                    chkFinalize = YES;
                    btnAddMenu.hidden = YES;
                    btnAddProducts.hidden = YES;
                    btnEmail.hidden = NO;
                    btnPrint.hidden = NO;
                }
                else{
                    chkFinalize = NO;
                    btnAddMenu.hidden = NO;
                    btnAddProducts.hidden = NO;
                    btnEmail.hidden = YES;
                    btnPrint.hidden = YES;
                }
            }
            
        }
        
        [rs1 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
        
    }
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_QUOTE_DETAILS_DETAILS";
    
    [arrProducts removeAllObjects];
    
    FMResultSet *rs2;
    
    @try {
        //Get Details details
        rs2 = [db executeQuery:@"SELECT a.QUOTE_NO, a.LINE_NO as LineNo, a.ITEM as StockCode, a.DESCRIPTION as Description, a.STATUS as Status,a.PRICE as SellingPrice,a.DISC_PERCENT as DiscPercent,a.EXTENSION as ExtnPrice,a.COST as Cost,a.WEIGHT as Weight,a.VOLUME as Volume,a.TAX as Tax,a.QUANTITY as QuantityOrdered,a.GROSS as Gross,b.AVAILABLE as Available,b.ALLOCATED as Allocated,b.WAREHOUSE as Warehouse FROM soquodet_offline a LEFT OUTER JOIN samaster b ON a.ITEM = b.CODE WHERE a.QUOTE_NO = ?",strQuoteNumRecived];
        
        
        if (!rs2)
        {
            [rs2 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs2 next]) {
            NSDictionary *dictData = [rs2 resultDictionary];
            [dictData setValue:@"0" forKey:@"isNewProduct"];
            [arrProducts addObject:dictData];
        }
        
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
    
}

#pragma mark - Custom Methods

- (void)hideOrDisableButtons{
    btnLoadProfile.enabled = FALSE;
    btnSaveDetails.enabled = FALSE;
    btnAddProducts.enabled = FALSE;
    btnFinalise.enabled = FALSE;
    btnAddMenu.enabled = FALSE;
}
- (void)showOrEnableButtons{
    btnLoadProfile.enabled = TRUE;
    btnSaveDetails.enabled = TRUE;
    btnAddProducts.enabled = TRUE;
    btnFinalise.enabled = TRUE;
    btnAddMenu.enabled = TRUE;
}
- (void)showPointingAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.autoreverses = YES;
    animation.duration = 0.35;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    [layerPointingImage addAnimation:animation forKey:@"pulseAnimation"];
    [imgArrowDownwards.layer addSublayer:layerPointingImage];
}

- (void)doAfterDataFetched{
    if ([arrProducts count]) {
        
        if (EditableMode == YES ){
            if(chkFinalize == NO) {
                btnDelete.hidden = NO;
                tblDetails.editing = NO;
                [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                
                [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
            }
            else{
                btnDelete.hidden = YES;
            }
        }
        else{
            btnDelete.hidden = NO;
            tblDetails.editing = NO;
            [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
            
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
            
            [btnDelete setBackgroundImage:img  forState:UIControlStateNormal];
        }
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        //NSString *path = [AppDelegate getFileFromDocumentsDirectory:@"QuoteDetails.plist"];
        //[arrProducts writeToFile:path atomically:YES];
    }
    else{
        //No Products
        [vwContentSuperview addSubview:vwNoProducts];
        
        if (chkFinalize == NO) {
            [self showPointingAnimation];
        }
        
    }
    
}

-(void)doAfterdelete{
    //[arrProducts removeObjectAtIndex:deleteRowIndex];
    [tblDetails deleteRowsAtIndexPaths:[NSArray arrayWithObject:deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tblDetails endUpdates];
    
    if ([arrProducts count] == 0) {
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        [self showOrEnableButtons];
        [self showPointingAnimation];
    }
    
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
    
}


#pragma mark - PopOverDelegates
//Add Products
- (void)showProductList{
    
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (void)dismissPopOver{
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    [popoverController dismissPopoverAnimated:YES];
    
    [self viewWillAppear:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    //tagFlsh = 0;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    //[self flashOn:btnAddMenu];
}

#pragma mark - Delegates

- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict{
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    __block BOOL isCommentExist = NO;
    __block int prodIndex = 0;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            prodIndex = idx;
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {

        [arrProducts addObject:dict];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        
        [tblDetails reloadData];
        btnDelete.hidden = NO;
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
        [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    else{
        [arrProducts replaceObjectAtIndex:productIndex withObject:dict];
        
        if (isCommentAddedOnLastLine) {
            id object = [arrProducts objectAtIndex:productIndex];
            [arrProducts removeObjectAtIndex:productIndex];
            [arrProducts insertObject:object atIndex:[arrProducts count]];
        }
        
        [tblDetails reloadData];
    }
}

- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation{
    
    NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
    NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
    
    NSMutableDictionary *dict = [arrProducts objectAtIndex:productIndex];
    [dict setValue:strTotalPrice forKey:@"ExtnPrice"];
    [dict setValue:strQuantityOrdered forKey:@"QuantityOrdered"];
    [dict setValue:[dictCalculation objectForKey:@"Gross"] forKey:@"Gross"];
    [dict setValue:[dictCalculation objectForKey:@"Tax"] forKey:@"Tax"];
    [dict setValue:[dictCalculation objectForKey:@"SellingPrice"] forKey:@"SellingPrice"];
    
    //NSLog(@"arrProducts %@",arrProducts);
    [tblDetails reloadData];
    
    int line_no = 1;
    for (NSMutableDictionary *dict in arrProducts) {
        if ([dict objectForKey:@"ExtnPrice"]) {
            [dict setObject:[NSNumber numberWithInt:line_no] forKey:@"LINE_NO"];
            
            //NSString *strExtnPrice = [dict objectForKey:@"ExtnPrice"];
            line_no ++;
        }
    }
    
}


#pragma mark - Handle Notification

- (void)reloadTableViewData:(NSNotification *)notification {
    NSDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
    
    NSLog(@"dictSelectedProduct : %@",dictSelectedProduct);

    BOOL isScrollToBottom = YES;
    if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
        [arrProducts addObject:dictSelectedProduct];
    }
    else{
        
        [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
        
        BOOL toRemove = NO;
        if ([arrProducts count]) {
            for (int i=0; i < [arrProducts count]; i++) {
                if ([[[arrProducts objectAtIndex:i] objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]]) {
                    
                    
                    if ([[[arrProducts objectAtIndex:i] objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
                        
                        NSString *strMessage = [NSString stringWithFormat:@"\"%@\" is already added!",[dictSelectedProduct objectForKey:@"Description"]];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                        strMessage = nil;
                        
                    }
                    else{
                        if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
                            [arrProducts removeObjectAtIndex:i];
                        }
                        
                    }
                    
                    toRemove = YES;
                    isScrollToBottom = NO;
                    break;
                }
                else{
                    toRemove = NO;
                }
                
            }
        }
        else{
            toRemove = NO;
            
        }
        
        if (!toRemove) {
            [arrProducts addObject:dictSelectedProduct];
        }
        
    }
    
    int lastIndex = [arrProducts count];
    if (lastIndex) {
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"ITEM"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                }
            }];
        }
        
        //Show delete button
        btnDelete.hidden = NO;
        tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        if (isScrollToBottom) {
            NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
            [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        }
        
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        
    }
}



- (void)reloadHeader:(NSNotification *)notification {
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    self.dictHeaderDetails = [notification.userInfo valueForKey:@"source"];
    
    NSLog(@"strQuoteNum %@",strQuoteNum);
    [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
    
    strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
    self.strDebtorNum = [dictHeaderDetails objectForKey:@"Debtor"];
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        
        if (![[dictHeaderDetails objectForKey:@"Debtor"] length] == 0) {
            segControl.enabled = YES;
        }
    }
    else{
        segControl.enabled = NO;
    }
    
    NSLog(@"dictHeaderDetails==%@",dictHeaderDetails);
    
    [tblHeader reloadData];
    
    
    
}



#pragma mark - Compose Mail

-(IBAction)openMail:(id)sender{
    
    [self callMailComposer];
}

-(void)callMailComposer{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
            [self displayComposerSheet];
        else
            [self launchMailAppOnDevice];
    }
    
    else
    {
        [self launchMailAppOnDevice];
    }
}


// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet{
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    
    picker.mailComposeDelegate = self;
    [picker setSubject:@""];
    
    // Set up recipients
    //[picker setCcRecipients:[NSArray arrayWithObject:CC_RECIPIENT]];
    [picker setBccRecipients:nil];
   
   // [picker setToRecipients:[NSArray arrayWithObject:TO_RECIPIENT]];
    
    //Attachment
    NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"sample.html"];
    
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    [picker addAttachmentData:fileData mimeType:@"text/html" fileName:@"sample.html"];
    [picker setMessageBody:@"" isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:Nil];
    
    if(picker) picker=nil;
    
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString* alertMessage;
    // message.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            alertMessage = @"Email composition cancelled";
            break;
        case MFMailComposeResultSaved:
            alertMessage = @"Your e-mail has been saved successfully";
            
            break;
        case MFMailComposeResultSent:
            alertMessage = @"Your email has been sent successfully";
            
            break;
        case MFMailComposeResultFailed:
            alertMessage = @"Failed to send email";
            
            break;
        default:
            alertMessage = @"Email Not Sent";
            
            break;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark
#pragma mark Workaround
#pragma mark
// Launches the Mail application on the device.

-(void)launchMailAppOnDevice{
    
    NSString *recipients = @"mailto:?cc=&subject=";
    NSString *body = @"&body=";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}

#pragma mark - Print Method
-(IBAction)sendToPrinter:(id)sender
{
    NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"sample.html"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    UIPrintInteractionController *print = [UIPrintInteractionController sharedPrintController];
    
    print.delegate = self;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    //printInfo.jobName = [appDelegate.pdfFilePath lastPathComponent];
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    print.printInfo = printInfo;
    print.showsPageRange = YES;
    print.printingItem = fileData;
    UIViewPrintFormatter *viewFormatter = [self.view viewPrintFormatter];
    viewFormatter.startPage = 0;
    print.printFormatter = viewFormatter;
    
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {};
    
//    [print presentAnimated:YES completionHandler:completionHandler];
      [print presentFromRect:self.view.bounds inView:self.view animated:YES completionHandler:completionHandler];
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
