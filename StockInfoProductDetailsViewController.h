//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockInfoProductDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate>{
    
    NSString *strWebserviceType;
    
    NSMutableDictionary *dictProductDetails;
    NSMutableDictionary *dictSalesQuantityDetails;
    
    NSMutableArray *arrProductLabels;
    
    NSMutableArray *arrProductsDetailsForWebservices;
    
    NSString *strProductName;
    NSString *strProductCode;
    NSString *strWarehouse;
    
    dispatch_queue_t backgroundQueueForStockProductDetails;
    FMDatabaseQueue *dbQueueStockProductDetails;
    
    BOOL isNetworkConnected;
    
    NSMutableArray *arrSalesMonthAgoForYear0;
    NSMutableArray *arrSalesMonthAgoForYear1;
    
    MBProgressHUD *spinner;
   
}

@property (nonatomic,retain) NSMutableDictionary *dictProductDetails;
@property (nonatomic,retain) NSMutableDictionary *dictSalesQuantityDetails;
@property (nonatomic,retain) NSString *strProductName;
@property (nonatomic,retain) NSString *strProductCode;
@property (nonatomic,retain) NSString *strWarehouse;
@property (nonatomic, retain) NSMutableArray *arrProductsDetailsForWebservices;
@property (nonatomic, retain) MBProgressHUD *spinner;
@property (nonatomic, retain) NSMutableArray *arrSalesMonthAgoForYear0;
@property (nonatomic, retain) NSMutableArray *arrSalesMonthAgoForYear1;
@property (nonatomic, retain) NSMutableArray *arrProductLabels;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblProductName;
@property (nonatomic,assign) BOOL isCostMarginVisible;

-(void)callGetProductListDetailsFromDB:(FMDatabase *)db;

@end
