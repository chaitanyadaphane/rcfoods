//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewQuoteDetailsViewController : UIViewController<ReachabilityRequest_delegate>{
    IBOutlet UIView *vwSegmentedControl;
    IBOutlet UIView *vwContentSuperview;
    
    IBOutlet UIView *vwHeader;
    IBOutlet UIView *vwDetails;
    IBOutlet UIView *vwNoProducts;
    
    IBOutlet UITableView *tblHeader;
    IBOutlet UITableView *tblDetails;
    
    NSString *strWebserviceType;
    
    NSString *strQuoteNum;
    
    NSMutableDictionary *dictQuoteHeaderDetails;
    NSMutableArray *arrQuoteDetails;
    NSMutableArray *arrQuoteDetailsForWebservices;
    
    NSArray *arrHeaderLabels;
    
    IBOutlet UILabel *lblQuoteNumber;
    BOOL isNetworkConnected;
    
    dispatch_queue_t backgroundQueueForViewQuoteDetails;
    FMDatabaseQueue *dbQueueViewQuoteDetails;
    
    BOOL isSales;

}

@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) NSMutableDictionary *dictQuoteHeaderDetails;
@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSMutableArray *arrQuoteDetails;
@property (nonatomic,retain) NSMutableArray *arrQuoteDetailsForWebservices;

@property (assign) BOOL isSales;


#pragma mark - IBAction
- (IBAction)actionShowQuoteHistory:(id)sender;

#pragma mark - WS calls
- (void)callWSViewQuoteDetailsHeader;
- (void)callWSViewQuoteDetailsDetails;

-(void)callViewQuoteDetailsHeaderFromDB:(FMDatabase *)db;
-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db;
@end
