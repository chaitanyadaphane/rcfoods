//
//  StockWareHouseViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "StockWareHouseViewController.h"
#import "CustomCellGeneral.h"
#import "StockProductListViewController.h"
#import "OfflineProfileOrderListViewController.h"

@interface StockWareHouseViewController ()

@end

@implementation StockWareHouseViewController
@synthesize arrWareHouse;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    
    //Offline Orders
    if (menuId == 0 && subMenuId == 3) {
        arrWareHouse = [[NSMutableArray alloc] initWithObjects:@"Profile Orders",@"Sales Orders", nil];

    }
    else{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            FMResultSet *res = [db executeQuery:@"SELECT distinct trim(WAREHOUSE) as WAREHOUSE FROM samaster"];
            
            if (!res)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                //[[AppDelegate getAppDelegateObj].database close];
                return;
            }
            
            arrWareHouse = [[NSMutableArray alloc] init];
            
            while ([res next]) {
                NSDictionary *dictData = [res resultDictionary];
                if([dictData objectForKey:@"WAREHOUSE"])
                {
                    NSString *str = [dictData objectForKey:@"WAREHOUSE"];
                    str = [str trimSpaces];
                    if(str.length > 0)
                    {
                        [arrWareHouse addObject:dictData];
                    }
                }
                
                dictData = nil;
            }
            
            [res close];
        }];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrWareHouse count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:7];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    //Offline Orders
    if (menuId == 0 && subMenuId == 3) {
         cell.lblCustomerName.text = [arrWareHouse objectAtIndex:[indexPath row]];
    }
    else{
        
        cell.lblCustomerName.text = [[arrWareHouse objectAtIndex:indexPath.row] objectForKey:@"WAREHOUSE"];
        //cell.lblCustomerName.text = [[arrWareHouse objectAtIndex:indexPath.row] objectForKey:@"WAREHOUSE"];
    }
    
    cell.lblQuoteNo.text = @"";
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int menuId = [AppDelegate getAppDelegateObj].intCurrentMenuId;
    int subMenuId = [AppDelegate getAppDelegateObj].intCurrentSubMenuId;
    
    //Offline Orders
    if (menuId == 0 && subMenuId == 3) {
        OfflineProfileOrderListViewController *dataViewController = [[OfflineProfileOrderListViewController alloc] initWithNibName:@"OfflineProfileOrderListViewController" bundle:[NSBundle mainBundle]];
        
        
         if ([indexPath row] == 0) {
             dataViewController.isProfileOrder = YES;
         }
         else{
             dataViewController.isProfileOrder = NO;
         }
        
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];

        
    }
    else{
        StockProductListViewController *dataViewController = [[StockProductListViewController alloc] initWithNibName:@"StockProductListViewController" bundle:[NSBundle mainBundle]];
        
        dataViewController.strWarehouse = [[arrWareHouse objectAtIndex:indexPath.row] objectForKey:@"WAREHOUSE"];
   
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }

}
@end
