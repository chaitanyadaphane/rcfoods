

#import "OpenPdfWebViewController.h"
#import <CommonCrypto/CommonDigest.h>
@interface OpenPdfWebViewController ()

@end

@implementation OpenPdfWebViewController

@synthesize webviewLink,webViewProductDesc,localSpinner,strPdfName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
           
    [localSpinner startAnimating];
    
    NSLog(@"webviewLink %@",webviewLink);
   
    [self performSelector:@selector(loadPdf) withObject:nil afterDelay:0.1];
}

-(void)viewWillDisappear:(BOOL)animated
{
	if([webViewProductDesc isLoading])
	{
		[webViewProductDesc stopLoading];
	}
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.webViewProductDesc = nil;
    self.localSpinner = nil;
}

- (void)loadPdf{

    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *strFileName = [webviewLink getFileNameFromURL];
    NSString *pdfCachePath = [NSString stringWithFormat:@"%@/%@",
                              cachePath,
                              strFileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pdfCachePath]) {
        NSLog(@"Cached file found, using it");
    }
    else {
        // Not found in the local cache, download and store it
        NSLog(@"File  not found in the local cache, going to download it");
        ASIHTTPRequest *downloadRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:webviewLink]];
        [downloadRequest setDownloadDestinationPath:pdfCachePath];
        [downloadRequest startSynchronous];
        
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:pdfCachePath]];
    [webViewProductDesc loadRequest:request];
    //--------------AND HERE USE SCALE PAGE TO FIT------------------//
    webViewProductDesc.scalesPageToFit = NO;
    
    /*
    NSURL *url = [NSURL URLWithString:webviewLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webViewProductDesc loadRequest:request];*/
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    
	return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [localSpinner stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [localSpinner stopAnimating];
 
    if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to load file as there is no internet connection!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        return ;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to load file!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}


-(IBAction)dismissView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(NSString *) md5:(NSString *) str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}
@end