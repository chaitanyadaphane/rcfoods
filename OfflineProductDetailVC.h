//
//  OfflineProductDetailVC.h
//  Blayney
//
//  Created by Pooja on 25/04/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetPriceDelegate
- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation stock_code:(NSString*)stock_code;
@end

@interface OfflineProductDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate,ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate,UITextFieldDelegate,UIAlertViewDelegate>{
    
    NSString *strWebserviceType;
    
    NSMutableDictionary *dictProductDetails;
    
    NSArray *arrProductLabels;
    
    NSMutableArray *arrProductsDetailsForWebservices;
    
    NSString *strProductName;
    NSString *strProductCode;
    NSString *strWarehouse;
    
    dispatch_queue_t backgroundQueueForStockProductDetails;
    FMDatabaseQueue *dbQueueStockProductDetails;
    
    BOOL isNetworkConnected;
    
    int quantityOrdered;
    float price;
    
    NSString *strPrice;
    NSString *strQuantity;
    NSString *strExtnIncl;
    NSString *strTax;
    NSString *strExcl;
    NSString *strMargin;
    
    NSString *strDebtor;
    
    NSString *lastPrice;
    
    MBProgressHUD *spinner;
}

@property (nonatomic,retain) NSMutableDictionary *dictProductDetails;
@property (nonatomic,retain) NSString *strProductName;
@property (nonatomic,retain) NSString *strProductCode;
@property (nonatomic,retain) NSString *strWarehouse;
@property (nonatomic, retain) NSMutableArray *arrProductsDetailsForWebservices;
@property (nonatomic, retain) MBProgressHUD *spinner;

@property (nonatomic,retain) NSString *strAvailable;
@property (nonatomic,retain) NSString *strQuantity;
@property (nonatomic,retain) NSString *strPrice;
@property (nonatomic,retain) NSString *strExtnIncl;
@property (nonatomic,retain) NSString *strTax;
@property (nonatomic,retain) NSString *strExcl;
@property (nonatomic,retain) NSString *strMargin;

@property (nonatomic,retain) NSString *strDebtor;
@property (nonatomic,retain) NSOperationQueue *operationQueue;
@property(assign)int productIndex;
@property (nonatomic,retain) NSString *stock_code;

@property(unsafe_unretained)id<SetPriceDelegate>delegate;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblProductName;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnDone;
- (IBAction)donePressed:(id)sender;


@property(nonatomic,assign) BOOL isTaxApplicable;

-(void)callGetProductListDetailsFromDB:(FMDatabase *)db;

@end


