//
//  OfflineProductDetailVC.m
//  Blayney
//
//  Created by Pooja on 25/04/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import "OfflineProductDetailVC.h"
#import "SEFilterControl.h"
#import "CustomCellGeneral.h"

#define PRODUCT_GET_AVAILABLE_WS @"salesorder/getavailable.php?"
#define TAG_100 100
#define TAG_200 200
#define ALLOW_SPECIAL_PRICE @"Specials"

@interface OfflineProductDetailVC (){
    NSString *strTYPE1;
    NSString *strOriginalPrice;
    NSString *strOriginalMargin;
    
    BOOL isCostMarginVisible;
    BOOL firstTime;
    BOOL isSpecialValChanged;
    
    NSString *isProductFromSacontrct;
}

@end

@implementation OfflineProductDetailVC
@synthesize dictProductDetails,strProductName,strProductCode,strWarehouse,arrProductsDetailsForWebservices,strPrice,strQuantity,strExtnIncl,strTax,strExcl,strMargin,delegate,productIndex,strDebtor,operationQueue,stock_code,spinner;

@synthesize tblDetails,lblProductName,btnDone,isTaxApplicable,strAvailable;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        firstTime = YES;
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear: animated];
    isCostMarginVisible = NO;
    isSpecialValChanged = NO;
    isProductFromSacontrct = @"N";
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   // spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   // btnDone.userInteractionEnabled=false;
    // Do any additional setup after loading the view from its nib.
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"OfflineOrderProductDetail" Type:@"plist"];
    
    // Build the array from the plist
    arrProductLabels = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    if ([dictProductDetails objectForKey:@"DESCRIPTION"]) {
        lblProductName.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"DESCRIPTION"]];

    }
    else if ([dictProductDetails objectForKey:@"Description"]){
        lblProductName.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Description"]];
    }
    
    
    
    if ([dictProductDetails objectForKey:@"QUANTITY"]) {
        //Initial values
        quantityOrdered = [[dictProductDetails objectForKey:@"QUANTITY"]integerValue];
        strQuantity = [NSString stringWithFormat:@"%d",quantityOrdered];

    }
    else{
        //Initial values
        quantityOrdered = @"0";

    }
    
    
    self.strTax = [dictProductDetails objectForKey:@"TAX"];
    self.strExcl = @"0";
    self.strExtnIncl  = [NSString stringWithFormat:@"%f",[[dictProductDetails objectForKey:@"EXTENSION"] floatValue]];
    self.isTaxApplicable = YES;
    
    if ([dictProductDetails objectForKey:@"ITEM"]) {
        strProductCode = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"ITEM"]];
    }
    else if ([dictProductDetails objectForKey:@"StockCode"]){
         strProductCode = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"StockCode"]];
    }
    else{
         strProductCode = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Item"]];
    }
    
    
    if ([dictProductDetails objectForKey:@"CUST_PRICE"])
    {
        price = [[dictProductDetails objectForKey:@"CUST_PRICE"] floatValue];
        strPrice = [NSString stringWithFormat:@"%f",[[dictProductDetails objectForKey:@"CUST_PRICE"] floatValue]];
    }
    else{
        price = 0;
        strPrice = @"";
    }
       
    isProductFromSacontrct = @"N";
    operationQueue = [[NSOperationQueue alloc] init];

    //Profile products
    if ([[dictProductDetails objectForKey:@"isNewProduct"] isEqualToString:@"1"]) {
        if ([[dictProductDetails objectForKey:@"TAX_CODE1"] intValue] == -1) {
            self.isTaxApplicable = NO;
        }
        else{
            self.isTaxApplicable = YES;
        }
        
        //        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetTaxInfoFromDB:db];
                [self callGetDiscountFromDB:db];

            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                //                [spinner removeFromSuperview];
                
                //Margin Calculation
                price = [strPrice floatValue];
                strOriginalPrice = [NSString stringWithFormat:@"%f",price];
                
                //1stDec
                float cost = [[dictProductDetails objectForKey:@"Cost"] floatValue];
                float margin = ((price - cost) / price) * 100;
                
                // Set Original Margin value
                strOriginalMargin=[NSString stringWithFormat:@"%f",margin];
                self.strMargin = [NSString stringWithFormat:@"%f",margin];
                
                if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    self.strAvailable = [dictProductDetails objectForKey:@"Available"];
                   
                }
               [tblDetails reloadData];
                
            }];
        }];
    }
    else{
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
//                [self callGetTaxInfoFromDB:db];
//                [self callGetDiscountFromDB:db];
            }];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                
                //Margin Calculation
                price = [strPrice floatValue];
                
                strOriginalPrice = [NSString stringWithFormat:@"%f",price];
                
                float cost = [[dictProductDetails objectForKey:@"COST"] floatValue];
                float margin = ceilf(((price - cost) / price) * 100);
                
                // Set Original Margin value
                strOriginalMargin=[NSString stringWithFormat:@"%f",margin];
                
                self.strMargin = [NSString stringWithFormat:@"%f",margin];
                
                if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                   
                    [tblDetails reloadData];
                }
                else{
                    self.strAvailable = [dictProductDetails objectForKey:@"Available"];
                    [tblDetails reloadData];
                }
                
            }];
            
        }];
        
    }
    
    self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
    self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
    self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
    self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
    [tblDetails reloadData];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblDetails = nil;
    self.btnDone = nil;
    self.lblProductName = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Text Field Delgates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    if (textField.tag == 2) {
    //        [tblDetails setContentOffset:CGPointMake(0, 500) animated:YES];
    //    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == TAG_100) {
        return [[AppDelegate getAppDelegateObj] allowNumbersOnly:string];
    }
    
    if (textField.tag == TAG_200) {
        
        NSRange ranget = range;
        return [[AppDelegate getAppDelegateObj] allowFloatingNumbersOnly:[textField.text stringByReplacingCharactersInRange:ranget withString:string]];
    }
    
    return TRUE;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    //BOOL isAllValid = YES;
    
    if (textField.tag == TAG_100) {
        self.strQuantity = textField.text;
        quantityOrdered = [strQuantity intValue];
        
    }
    else if (textField.tag == TAG_200) {
        
        self.strPrice = textField.text;
        price = [strPrice floatValue];
    }
    
    [self calculateData];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag == 2) {
        [tblDetails setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return YES;
}


#pragma mark Table view data source and delegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width,30)];
    
    UILabel *headerLabel;
    
    if(IOS_Version < 7.0)
    {
        headerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, headerView.frame.size.width-120.0, headerView.frame.size.height)];
    }
    else
    {
        headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, headerView.frame.size.width-120.0, headerView.frame.size.height)];
    }
    
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.font = [UIFont boldSystemFontOfSize:22];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    
    [headerView addSubview:headerLabel];
    
    switch (section) {
        case 1:{
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if (isCostMarginVisible) {
                [button setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
            }
            else{
                [button setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
            }
            
            //headerView.frame.size.width/2.0
            [button setFrame:CGRectMake(435.0, 3.0, 30.0, 30.0)];
            button.tag = section;
            button.hidden = NO;
            [button setBackgroundColor:[UIColor clearColor]];
            [button addTarget:self action:@selector(showCostMargin:) forControlEvents:UIControlEventTouchDown];
            [headerView addSubview:button];
            headerLabel.text = @"Stock info";
        }
            break;
        case 2:{
            headerLabel.text = @"Purchase history";
        }break;
            
//        case 3:{
//            headerLabel.text = @"Sales (months ago)";
//        }break;
            
        default:{
            headerLabel.text = @"";
            
        }break;
    }
    
    return headerView;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    //Change by subhu
    if (indexPath.section==1)
    {
        if(!isCostMarginVisible && ([indexPath row] == 0 ||  [indexPath row] == 1))
        {
            return 0;
        }else{
            return 60;
        }
    }
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return [arrProductLabels count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    //oct 30 2014 commented
    NSArray *array=[arrProductLabels objectAtIndex:section];
    return [array count] ;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    
    CustomCellGeneral *cell;
    
    NSArray *nib;
    
    if ([indexPath section] == 0)
    {
        switch ([indexPath row]) {
            case 0:{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil) {
                    nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:4];
                }
                
                if ([dictProductDetails objectForKey:@"ITEM"]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"ITEM"]];
                }
                else if([dictProductDetails objectForKey:@"StockCode"]){
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"StockCode"]];
                    
                }
            }
                break;
                
            default: {
                
            }
                break;
        }
    }
    else if ([indexPath section] == 1)
    {
        //Change by subhu
        if ([indexPath row] == 2 ||  [indexPath row] == 3) {
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
            
            if (cell == nil) {
                nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:9];
                
                cell.txtGlowingValue.keyboardType = UIKeyboardTypeNumberPad;
                cell.txtGlowingValue.delegate = self;
            }
            
        }
        else{
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            
            if (cell == nil) {
                nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:4];
            }
        }
        
        cell.lblTitle.text = @"";
        cell.lblValue.text = @"";
        
        switch ([indexPath row]) {
                
                //--margin
            case 0:
            {
                NSLog(@"strMargin::::::::%@",strMargin);
                if(isCostMarginVisible)
                    cell.lblValue.text = [NSString stringWithFormat:@"%.2f%@",[strMargin floatValue],@"%"];
                else
                {
                    cell.lblTitle.text = @"";
                    cell.lblValue.text = @"";
                }
            }
                break;
            case 1:
            {
                //Cost -> AverageCost
                if ([dictProductDetails objectForKey:@"COST"]) {
                    if(isCostMarginVisible)
                    {
                        cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"COST"] floatValue]];
                    }
                    else
                    {
                        cell.lblTitle.text = @"";
                        cell.lblValue.text = @"";
                    }
                }
                else if ([dictProductDetails objectForKey:@"Cost"]){
                    if(isCostMarginVisible)
                    {
                        cell.lblValue.text = [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictProductDetails objectForKey:@"Cost"] floatValue]];
                    }
                    else
                    {
                        cell.lblTitle.text = @"";
                        cell.lblValue.text = @"";
                    }
                    
                }
            }
                break;
                
                //                //Available
                //            case 2:
                //            {
                //                cell.lblValue.text = [NSString stringWithFormat:@"%.0f",[strAvailable floatValue]];
                //            }
                //                break;
                
                //Quantity
            case 2:
            {
                cell.txtGlowingValue.placeholder = ENTER_QUANTITY_HERE_MESSAGE;
                cell.txtGlowingValue.tag = TAG_100;
                cell.txtGlowingValue.text = strQuantity;
            }
                break;
                
                //Price
            case 3:
            {
                NSString *tempStr =[dictProductDetails objectForKey:@"PREF_SUPPLIER"];
                NSString *prefSupplierStr = [tempStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if ([prefSupplierStr isEqualToString:@"REPAN1"] || [prefSupplierStr isEqualToString:@"REPAN2"] || [prefSupplierStr isEqualToString:@"REPAN3"] || [prefSupplierStr isEqualToString:@"REPAN4"]) {
                    //1stDec, 2014
                    float marginVal =[strMargin floatValue];
                    if (marginVal < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"]){
                        cell.txtGlowingValue.enabled = YES;
                        
                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        cell.txtGlowingValue.tag = TAG_200;
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        
                        
                    }
                    else{
                        //                        cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                        //                        cell.txtGlowingValue.tag = TAG_200;
                        cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                        cell.txtGlowingValue.enabled = NO;
                        
                    }
                    
                }
                else{
                    cell.txtGlowingValue.enabled = YES;
                    
                    cell.txtGlowingValue.placeholder = ENTER_PRICE_HERE_MESSAGE;
                    cell.txtGlowingValue.tag = TAG_200;
                    cell.txtGlowingValue.text = [NSString stringWithFormat:@"%.2f",[strPrice floatValue]];
                }
            }
                break;
                
                
                
                // LastSold
            case 4:
            {
                if ([dictProductDetails objectForKey:@"LastSold"] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_APP_DATE] && ![[dictProductDetails objectForKey:@"LastSold"] isEqualToString:STANDARD_SERVER_DATE]) {
                    cell.lblValue.text = [dictProductDetails objectForKey:@"LastSold"];
                    cell.lblValue.text = [NSString stringWithFormat:@"%@", [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"LastSold"]]];
                }
                else
                    cell.lblValue.text = @"--";
            }
                break;
                
                //Excl
            case 5:
            {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strExcl floatValue]];
            }
                break;
                
                //Tax
            case 6:
            {
                if (isTaxApplicable) {
                    cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strTax floatValue]];
                }
                else{
                    cell.lblValue.text = @"NA";
                }
            }
                break;
                
                //ExtnIncl
            case 7:
            {
                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[strExtnIncl floatValue]];
            }
                break;
            default:{
                
            }
                break;
        }
        
    }
    /*else if ([indexPath section] == 2)
     {
     cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
     
     if (cell == nil) {
     nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
     cell = [nib objectAtIndex:4];
     }
     
     switch ([indexPath row]) {
     
     case 0:
     
     {
     if ([dictProductDetails objectForKey:@"q0"]) {
     cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q0"]];
     }
     
     }
     
     break;
     
     //-1
     case 1:
     
     {
     
     if ([dictProductDetails objectForKey:@"q1"]) {
     cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q1"]];
     }
     }
     
     break;
     
     //-2
     case 2:
     
     {
     
     if ([dictProductDetails objectForKey:@"q2"]) {
     cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q2"]];
     
     }
     }
     
     break;
     
     //-3
     case 3:
     
     {
     
     if ([dictProductDetails objectForKey:@"q3"]) {
     cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q3"]];
     }
     }
     
     break;
     
     
     //-4
     case 4:
     {
     if ([dictProductDetails objectForKey:@"q4"]) {
     cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"q4"]];
     }
     }
     break;
     
     default:{
     }
     break;
     }
     
     }
     */
    NSArray *array=[arrProductLabels objectAtIndex:[indexPath section]];
    // NSLog(@"array : %@",array);
    
    if([[[array objectAtIndex:[indexPath row]] objectForKey:@"Label"] isEqualToString:@"Price"])
    {
        cell.lblTitle.text = @"";
        
        if([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"M"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Special Price($)"];
        }
        else if([strTYPE1 isEqualToString:@"N"])
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"List Price($)"];
        }
        else
        {
            cell.lblTitle.text = [NSString stringWithFormat:@"Contract Price($)"];
        }
    }
    else
    {
        if (indexPath.section==1 && ([indexPath row] == 0 ||  [indexPath row] == 1))
        {
            if(isCostMarginVisible)
            {
                cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            }else
                cell.lblTitle.text = @"";
        }else
        {
            cell.lblTitle.text = [[array objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        }
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

#pragma mark - AlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case 1:
            [tblDetails reloadData];
            break;
            
        case TAG_100:{
            
        }
            
        default:
            break;
    }
    
}

#pragma mark - Action Methods
- (IBAction)actionDonePressed:(id)sender{
    [self actionSetPrice];
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

-(void)calculateData
{
    [[self.view findFirstResponder] resignFirstResponder];
    
    if (quantityOrdered < 0) {
        // if (quantityOrdered <= 0) {
        
        //isAllValid = NO;
        
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:QUANTITY_GREATER_THAN_0
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        [tblDetails reloadData];
        return;
    }
    
//    if ([dictProductDetails objectForKey:@"ContractSpecial"]) {
//        
//        CGFloat prevMargin = [self.strMargin floatValue];
//        self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
//        self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
//        self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
//        self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
//        
//        if (strOriginalMargin.floatValue != self.strMargin.floatValue) {
//            isSpecialValChanged = YES;
//        }
//        else{
//            isSpecialValChanged = NO;
//        }
//        
//        //Change by Subhu
//        //        if(self.strMargin.floatValue <= 0)
//        //        {
//        //            [AJNotificationView showNoticeInView:self.view
//        //                                            type:AJNotificationTypeRed
//        //                                           title:MARGIN_PERCENT_ERROR
//        //                                 linedBackground:AJLinedBackgroundTypeDisabled
//        //                                       hideAfter:2.5f];
//        //            [tblDetails reloadData];
//        //            return;
//        //        }
//        
//        NSLog(@"prevMargin  %f",prevMargin);
//        NSLog(@"New margin %f",self.strMargin.floatValue);
//        
//        if (isSpecialValChanged == YES) {
//            if ( self.strMargin.floatValue < MARGIN_MINIMUM_PERCENT )//strOriginalMargin.floatValue > self.strMargin.floatValue ||
//            {
//                NSLog(@"##### ##### ##### #####");
//                NSLog(@"PLEASE CHECK THE CONDITION ----- >");
//                [AJNotificationView showNoticeInView:self.view
//                                                type:AJNotificationTypeRed
//                                               title:MARGIN_PERCENT_ERROR
//                                     linedBackground:AJLinedBackgroundTypeDisabled
//                                           hideAfter:2.5f];
//                [tblDetails reloadData];
//                return;
//                
//            }
//        }
//        
//        [tblDetails reloadData];
//        
//        if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
//        {
//            [self actionSetPrice];
//        }
//        else
//        {
//            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//            [databaseQueue   inDatabase:^(FMDatabase *db) {
//                [self callGetAvailabilityFromDB:db];
//            }];
//            databaseQueue = nil;
//        }
//    }
    
    ///--For Special
//    else if([dictProductDetails objectForKey:@"FromSpecials"]){
//        if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:ALLOW_SPECIAL_PRICE])
//        {
//            
//            CGFloat prevMargin = [self.strMargin floatValue];
//            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
//            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
//            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
//            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
//            
//            
//            if (strOriginalMargin.floatValue != self.strMargin.floatValue) {
//                isSpecialValChanged = YES;
//            }
//            else{
//                isSpecialValChanged = NO;
//            }
//            
//            //Change by Subhu
//            //            if(self.strMargin.floatValue <= 0)
//            //            {
//            //                [AJNotificationView showNoticeInView:self.view
//            //                                                type:AJNotificationTypeRed
//            //                                               title:MARGIN_PERCENT_ERROR
//            //                                     linedBackground:AJLinedBackgroundTypeDisabled
//            //                                           hideAfter:2.5f];
//            //                [tblDetails reloadData];
//            //                return;
//            //            }
//            
//            NSLog(@"prevMargin  %f",prevMargin);
//            NSLog(@"New margin %f",self.strMargin.floatValue);
//            
//            if (isSpecialValChanged == YES) {
//                if (strOriginalMargin.floatValue > self.strMargin.floatValue || self.strMargin.floatValue < MARGIN_MINIMUM_PERCENT )
//                {
//                    NSLog(@"##### ##### ##### #####");
//                    NSLog(@"PLEASE CHECK THE CONDITION ----- >");
//                    [AJNotificationView showNoticeInView:self.view
//                                                    type:AJNotificationTypeRed
//                                                   title:MARGIN_PERCENT_ERROR
//                                         linedBackground:AJLinedBackgroundTypeDisabled
//                                               hideAfter:2.5f];
//                    [tblDetails reloadData];
//                    return;
//                    
//                }
//            }
//            
//            [tblDetails reloadData];
//            
//            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
//            {
//                [self actionSetPrice];
//            }
//            else
//            {
//                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
//                [databaseQueue   inDatabase:^(FMDatabase *db) {
//                    [self callGetAvailabilityFromDB:db];
//                }];
//                databaseQueue = nil;
//            }
//        }
//    }
     if(strTYPE1.length > 0)
    {
        if([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"])
        {
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
        }
        else if ([self calcMargin]  < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            //            if(self.strMargin.floatValue <= 0)
            //            {
            //                [AJNotificationView showNoticeInView:self.view
            //                                                type:AJNotificationTypeRed
            //                                               title:MARGIN_PERCENT_ERROR
            //                                     linedBackground:AJLinedBackgroundTypeDisabled
            //                                           hideAfter:2.5f];
            //
            //                [tblDetails reloadData];
            //                return;
            //            }
            
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
            
        }
    }
    else
    {
//        if ([self calcMargin] < MARGIN_MINIMUM_PERCENT)//[[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"]
//        {
//            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
//            
//            [AJNotificationView showNoticeInView:self.view
//                                            type:AJNotificationTypeRed
//                                           title:MARGIN_PERCENT_ERROR
//                                 linedBackground:AJLinedBackgroundTypeDisabled
//                                       hideAfter:2.5f];
//            
//            [tblDetails reloadData];
//            return;
//            
//        }
//        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                ///--Dont check the avaliable.Directly set the quantity Set
                /*
                 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                 [databaseQueue   inDatabase:^(FMDatabase *db) {
                 [self callGetAvailabilityFromDB:db];
                 }];
                 databaseQueue = nil;
                 */
                [self actionSetPrice];
            }
            
//        }
    }
    
}
- (IBAction)actionAvailabilityCheck:(id)sender
{
    
    [[self.view findFirstResponder] resignFirstResponder];
    
    if (quantityOrdered < 0) {
        // if (quantityOrdered <= 0) {
        
        //isAllValid = NO;
        
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:QUANTITY_GREATER_THAN_0
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        return;
    }
    
    
    ///--For Special
    if([dictProductDetails objectForKey:@"FromSpecials"])
    {
        if([[dictProductDetails objectForKey:@"FromSpecials"] isEqualToString:ALLOW_SPECIAL_PRICE])
        {
            {
                self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
                self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
                self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
                self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
                
//                if(self.strMargin.floatValue <= 0)
//                {
//                    [AJNotificationView showNoticeInView:self.view
//                                                    type:AJNotificationTypeRed
//                                                   title:MARGIN_PERCENT_ERROR
//                                         linedBackground:AJLinedBackgroundTypeDisabled
//                                               hideAfter:2.5f];
//                    
//                    [tblDetails reloadData];
//                    return;
//                }
                
                [tblDetails reloadData];
                
                if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
                {
                    [self actionSetPrice];
                }
                else
                {
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetAvailabilityFromDB:db];
                    }];
                    databaseQueue = nil;
                }
            }
        }
    }
    else if(strTYPE1.length > 0)
    {
        if([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"])
        {
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            //Change by Subhu
            // 9th Dec, 2014 // 24thFeb2014
//            if([self calcMargin] < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
//            {
//                [AJNotificationView showNoticeInView:self.view
//                                                type:AJNotificationTypeRed
//                                               title:MARGIN_PERCENT_ERROR
//                                     linedBackground:AJLinedBackgroundTypeDisabled
//                                           hideAfter:2.5f];
//                
//                [tblDetails reloadData];
//                return;
//            }
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
        }
        else if ([self calcMargin] < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            if(self.strMargin.floatValue <= 0)
            {
                [AJNotificationView showNoticeInView:self.view
                                                type:AJNotificationTypeRed
                                               title:MARGIN_PERCENT_ERROR
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                
                [tblDetails reloadData];
                return;
            }
            
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
        }
    }
    else
    {
        if ([self calcMargin] < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                ///--Dont check the avaliable.Directly set the quantity Set
                /*
                 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                 [databaseQueue   inDatabase:^(FMDatabase *db) {
                 [self callGetAvailabilityFromDB:db];
                 }];
                 databaseQueue = nil;
                 */
                [self actionSetPrice];
            }
            
        }
    }
    
}

-(void)actionSetPrice{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:strExcl forKey:@"GROSS"];
    [dict setValue:strTax forKey:@"TAX"];
    [dict setValue:strPrice forKey:@"CUST_PRICE"];
    [dict setValue:strExcl forKey:@"EXTENSION"];
    [dict setValue:strExtnIncl forKey:@"ExtnPrice"];
    
    if([strOriginalPrice floatValue] == [strPrice floatValue] )
    {
        [dict setValue:@"N" forKey:@"PriceChnaged"];
        
        NSLog(@"NOT changed");
        
    }
    else
    {
        [dict setValue:@"Y" forKey:@"PriceChnaged"];
        NSLog(@"Changed ::::");
    }
    
    if ([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"N"]) {
        [self.delegate setPriceOfProduct:[strExtnIncl floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict stock_code:strProductCode];
    }
    else if ([[dict objectForKey:@"PriceChnaged"] isEqualToString:@"Y"] && [self calcMargin] >= MARGIN_MINIMUM_PERCENT ){
        [self.delegate setPriceOfProduct:[strExtnIncl floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict stock_code:strProductCode];
    }
    else{
        [self.delegate setPriceOfProduct:[strExtnIncl floatValue] ProductIndex:productIndex QuantityOrdered:quantityOrdered  DictCalculation:dict stock_code:strProductCode];
        
    }
    
    
}


#pragma mark - Calculation Methods
-(float)calcExtnIncl{
    return ([self calcExcl] + [self calcTax]);
}

-(float)calcTax{
    //If tax applicable
    if (isTaxApplicable) {
        //Tax is 10% of total
        return  (quantityOrdered*price*0.1);
    }
    return 0;
}

-(float)calcExcl{
    return  (quantityOrdered*price);
}

-(float)calcMargin{
    
    NSString *strCost = @"0.00";
   
    if ([dictProductDetails objectForKey:@"COST"]) {
        strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"COST"]];
        NSLog(@"%f",(((price -  [strCost floatValue]) / price) * 100));
        
        CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
        return  (rounded_up);
    }
    else    if ([dictProductDetails objectForKey:@"Cost"]) {
        strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
        NSLog(@"%f",(((price -  [strCost floatValue]) / price) * 100));
        
        CGFloat rounded_up = ceilf(((price -  [strCost floatValue]) / price) * 100);
        return  (rounded_up);
    }
    
    NSLog(@"%f",(((price -  [strCost floatValue]) / price) * 100));
    CGFloat roundval = ceilf(((price -  [strCost floatValue]) / price) * 100);
    return  (roundval);
    
}



#pragma mark - Database calls
-(void)callGetProductListDetailsFromDB:(FMDatabase *)db{
    
    FMResultSet *results1 = [db executeQuery:@"SELECT CODE as Code, DESCRIPTION as Description, ON_HAND as OnHand, AVAILABLE as Available, ALLOCATED as Allocated, PURCHASE_ORDER as PurchaseOrder, PRICE1 as Price1, PRICE2 as Price2, PRICE3 as Price3, PRICE4 as Price4, PRICE5 as Price5, LOCATION as Location, MTD_QTY as MtdQty FROM samaster WHERE CODE=? AND WAREHOUSE=?",strProductCode,strWarehouse];
    
    if (!results1)
    {
        NSLog(@"Error: %@", [db lastErrorMessage]);
        [db close];
        return;
    }
    
    if ([results1 next]) {
        if(!dictProductDetails){
            dictProductDetails = [[NSMutableDictionary alloc] init];
        }
        NSDictionary *dictData = [results1 resultDictionary];
        
        NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
        [dictTemp1 setValue:[dictData objectForKey:@"Allocated"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp1 forKey:@"Allocated"];
        
        NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
        [dictTemp2 setValue:[dictData objectForKey:@"Available"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp2 forKey:@"Available"];
        
        NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
        [dictTemp3 setValue:[dictData objectForKey:@"Code"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp3 forKey:@"Code"];
        
        NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
        [dictTemp4 setValue:[dictData objectForKey:@"Description"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp4 forKey:@"Description"];
        
        NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
        [dictTemp5 setValue:[dictData objectForKey:@"Location"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp5 forKey:@"Location"];
        
        NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
        [dictTemp6 setValue:[dictData objectForKey:@"MtdQty"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp6 forKey:@"MtdQty"];
        
        NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
        [dictTemp7 setValue:[dictData objectForKey:@"OnHand"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp7 forKey:@"OnHand"];
        
        NSMutableDictionary *dictTemp8 = [[NSMutableDictionary alloc] init];
        [dictTemp8 setValue:[dictData objectForKey:@"Price1"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp8 forKey:@"Price1"];
        
        NSMutableDictionary *dictTemp9 = [[NSMutableDictionary alloc] init];
        [dictTemp9 setValue:[dictData objectForKey:@"Price2"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp9 forKey:@"Price2"];
        
        NSMutableDictionary *dictTemp10 = [[NSMutableDictionary alloc] init];
        [dictTemp10 setValue:[dictData objectForKey:@"Price3"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp10 forKey:@"Price3"];
        
        NSMutableDictionary *dictTemp11 = [[NSMutableDictionary alloc] init];
        [dictTemp11 setValue:[dictData objectForKey:@"Price4"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp11 forKey:@"Price4"];
        
        NSMutableDictionary *dictTemp12 = [[NSMutableDictionary alloc] init];
        [dictTemp12 setValue:[dictData objectForKey:@"Price5"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp12 forKey:@"Price5"];
    }
    else{
        NSLog(@"No Details exist for this code");
    }
    
    //    FMResultSet *results2 = [db executeQuery:@"SELECT sa.CODE , s.* FROM samaster sa , sahisbud s WHERE ((sa.WAREHOUSE || sa.CODE) = s.INDEX_FIELD)  AND s.YEAR = ? AND sa.CODE = ? AND sa.WAREHOUSE = ?",@"1",strProductCode,strWarehouse];
    FMResultSet *results2 = [db executeQuery:@"SELECT sa.CODE , s.* FROM samaster sa , sahisbud s WHERE ((sa.WAREHOUSE || sa.CODE) = s.INDEX_FIELD)  AND s.YEAR = ? AND sa.CODE = ? AND sa.WAREHOUSE = ?",@"1",strProductCode,@"00"];
    
    
    
    if (!results2)
    {
        NSLog(@"Error: %@", [db lastErrorMessage]);
        [db close];
        return;
    }
    
    if ([results2 next]) {
        NSDictionary *dictData = [results2 resultDictionary];
        
        if(!dictProductDetails){
            dictProductDetails = [[NSMutableDictionary alloc] init];
        }
        
        NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
        [dictTemp1 setValue:[dictData objectForKey:@"SALES01"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp1 forKey:@"Sales1Month"];
        
        NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
        [dictTemp2 setValue:[dictData objectForKey:@"SALES02"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp2 forKey:@"Sales2Month"];
        
        NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
        [dictTemp3 setValue:[dictData objectForKey:@"SALES03"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp3 forKey:@"Sales3Month"];
        
        NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
        [dictTemp4 setValue:[dictData objectForKey:@"SALES04"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp4 forKey:@"Sales4Month"];
        
        NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
        [dictTemp5 setValue:[dictData objectForKey:@"SALES05"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp5 forKey:@"Sales5Month"];
        
        NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
        [dictTemp6 setValue:[dictData objectForKey:@"SALES06"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp6 forKey:@"Sales6Month"];
        
        NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
        [dictTemp7 setValue:[dictData objectForKey:@"SALES07"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp7 forKey:@"Sales7Month"];
        
        NSMutableDictionary *dictTemp8 = [[NSMutableDictionary alloc] init];
        [dictTemp8 setValue:[dictData objectForKey:@"SALES08"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp8 forKey:@"Sales8Month"];
        
        NSMutableDictionary *dictTemp9 = [[NSMutableDictionary alloc] init];
        [dictTemp9 setValue:[dictData objectForKey:@"SALES09"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp9 forKey:@"Sales9Month"];
        
        NSMutableDictionary *dictTemp10 = [[NSMutableDictionary alloc] init];
        [dictTemp10 setValue:[dictData objectForKey:@"SALES10"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp10 forKey:@"Sales10Month"];
        
        NSMutableDictionary *dictTemp11 = [[NSMutableDictionary alloc] init];
        [dictTemp11 setValue:[dictData objectForKey:@"SALES11"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp11 forKey:@"Sales11Month"];
        
        NSMutableDictionary *dictTemp12 = [[NSMutableDictionary alloc] init];
        [dictTemp12 setValue:[dictData objectForKey:@"SALES12"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp12 forKey:@"Sales12Month"];
        
        NSMutableDictionary *dictTemp13 = [[NSMutableDictionary alloc] init];
        [dictTemp13 setValue:[dictData objectForKey:@"SALES13"] forKey:@"text"];
        [dictProductDetails setObject:dictTemp13 forKey:@"Sales13Month"];
    }
    else{
        NSLog(@"No Details exist for this code");
    }
    
    NSLog(@"dictProductDetails==%@",dictProductDetails);
    
    [tblDetails reloadData];
}

-(void)callGetPriceFromDB:(FMDatabase *)db{
    
    NSLog(@"strDebtor %@",strDebtor);
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    if ([dictProductDetails objectForKey:@"Price1"]) {
                        self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                    }
                    else if([dictProductDetails objectForKey:@"Price"]){
                        self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                    }
                    
                }
                    break;
                case 1:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
                    
                case 2:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                }
                    break;
                    
                case 3:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                }
                    break;
                    
                case 4:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                }
                    break;
                    
                case 5:
                {
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                }
                    break;
                    
                    
                default:{
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
                    
                    
                    
            }
            
            if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"]) {
                if([dictProductDetails objectForKey:@"Price"]){
                    self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                }
                
            }
            
        }
        
        [rs1 close];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
        
    }
    
}


/*
 -(void)callGetDiscountFromDB:(FMDatabase *)db{
 
 NSLog(@"strProductCode %@",strProductCode);
 
 @try {
 
 float priceCode = 0;
 float discount = 0;
 
 //For Last Price
 FMResultSet *rs = [db executeQuery:@"SELECT PRICE FROM arhisdet WHERE STOCK_CODE = ? AND DEBTOR = ? ORDER BY DATE_RAISED desc LIMIT 1",stock_code,strDebtor];
 
 if (!rs)
 {
 [rs close];
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
 }
 
 if ([rs next])
 {
 lastPrice = [rs stringForColumn:@"PRICE"];
 }
 [rs close];
 
 //For Price Code
 FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
 
 if (!rs1)
 {
 [rs1 close];
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
 }
 
 if ([rs1 next]) {
 NSDictionary *dictData = [rs1 resultDictionary];
 
 //NSLog(@"dictData %@",dictData);
 
 switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
 case 0:
 {
 if ([dictProductDetails objectForKey:@"Price1"])
 {
 priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
 
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
 }
 else if([dictProductDetails objectForKey:@"Price"])
 {
 priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
 }
 
 }
 break;
 case 1:
 {
 priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
 }
 break;
 
 case 2:
 {
 priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
 }
 break;
 
 case 3:
 {
 priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
 }
 break;
 
 case 4:
 {
 priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
 }
 break;
 
 case 5:
 {
 priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
 }
 break;
 
 
 default:{
 priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
 }
 break;
 
 
 
 }
 
 if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
 {
 if([dictProductDetails objectForKey:@"Price"])
 {
 priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
 //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
 }
 
 }
 
 }
 else
 {
 priceCode = 0;
 }
 
 [rs1 close];
 
 
 //For Dicount
 FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND date() between DATE_FROM1 and DATE_TO1", strProductCode, strDebtor];
 
 FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = '' AND TRIM(CUST_CATEGORY) = '' AND date() between DATE_FROM1 and DATE_TO1", strProductCode];
 
 
 float discount1 = 0;
 float discount2 = 0;
 
 if (!rs21)
 {
 [rs21 close];
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
 @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
 }
 
 if ([rs21 next])
 {
 NSDictionary *dictData = [rs21 resultDictionary];
 [rs21 close];
 
 strTYPE1 = [dictData objectForKey:@"TYPE1"];
 
 if ([strTYPE1 isEqualToString:@"P"]) {
 priceCode = priceCode - (priceCode * 0.05);
 discount1 = priceCode;
 }
 else{
 discount1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
 }
 }
 if ([rs221 next]) {
 NSDictionary *dictData = [rs221 resultDictionary];
 [rs221 close];
 strTYPE1 = [dictData objectForKey:@"TYPE1"];
 
 if ([strTYPE1 isEqualToString:@"P"]) {
 priceCode = priceCode - (priceCode * 0.05);
 discount2 = priceCode;
 }
 else{
 discount2 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
 }
 }
 
 if (discount1 == 0) {
 discount =discount2;
 }
 else if (discount2 == 0){
 discount = discount1;
 }
 else{
 if (discount1 > discount2 ) {
 discount = discount2;
 }
 else{
 discount = discount1;
 }
 }
 
 
 if (discount == 0) {
 strTYPE1 = ALLOW_SPECIAL_PRICE;
 }
 
 
 if(priceCode > 0 && discount > 0)
 {
 if(priceCode > discount)
 {
 self.strPrice = [NSString stringWithFormat:@"%f",discount];
 }
 else
 {
 self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
 }
 }
 else if (priceCode == 0)
 {
 self.strPrice = [NSString stringWithFormat:@"%f",discount];
 }
 else if (discount == 0)
 {
 self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
 }
 else
 {
 self.strPrice = @"0";
 }
 
 }@catch (NSException* e) {
 // rethrow if not one of the two exceptions above
 
 [[NSOperationQueue mainQueue]addOperationWithBlock:^{
 NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
 [alert show];
 }];
 
 
 }
 
 }*/

//-(void)callGetNewDiscountFromDB:(FMDatabase *)db{
//    NSLog(@"strProductCode %@",strProductCode);
//
//    @try {
//
//        float priceCode = 0;
//        float discount = 0;
//
//        //For Last Price
//        FMResultSet *rs = [db executeQuery:@"SELECT CODE,STOCK_CODE,MARGIN_PCT,AGREED_SELL_PRC,REBATE_TYPE FROM sacntrct WHERE STOCK_CODE = ? AND DEBTOR = ? ORDER BY FROM_DATE desc LIMIT 1",stock_code,strDebtor];
//
//        if (!rs)
//        {
//            [rs close];
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//        }
//
//        if ([rs next])
//        {
//            lastPrice = [rs stringForColumn:@"PRICE"];
//        }
//        [rs close];
//
//        //For Price Code
//        FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
//
//        if (!rs1)
//        {
//            [rs1 close];
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//        }
//
//        if ([rs1 next]) {
//            NSDictionary *dictData = [rs1 resultDictionary];
//
//            //NSLog(@"dictData %@",dictData);
//
//            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
//                case 0:
//                {
//                    if ([dictProductDetails objectForKey:@"Price1"])
//                    {
//                        priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
//
//                        //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
//                    }
//                    else if([dictProductDetails objectForKey:@"Price"])
//                    {
//                        priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
//                        //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
//                    }
//
//                }
//                    break;
//                case 1:
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
//                }
//                    break;
//
//                case 2:
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
//                }
//                    break;
//
//                case 3:
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
//                }
//                    break;
//
//                case 4:
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
//                }
//                    break;
//
//                case 5:
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
//                }
//                    break;
//
//
//                default:{
//                    priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
//                }
//                    break;
//
//
//
//            }
//
//            if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
//            {
//                if([dictProductDetails objectForKey:@"Price"])
//                {
//                    priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
//                    //self.strPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
//                }
//            }
//        }
//        else
//        {
//            priceCode = 0;
//        }
//
//        [rs1 close];
//
//
//        //For Dicount
//        FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", strProductCode, strDebtor];
//
//        FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = '' AND TRIM(CUST_CATEGORY) = '' AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", strProductCode];
//
//
//        float DiscountVal1 = 0;
//        float DiscountVal2 = 0;
//
//        float discountValwithKey1 = 0;
//        float discountValwithKey2 = 0;
//
//        float discountValwithOutKey1 = 0;
//        float discountValwithOutKey2 = 0;
//
//        if (!rs21)
//        {
//            [rs21 close];
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//        }
//
//        if ([rs21 next])
//        {
//            NSDictionary *dictData = [rs21 resultDictionary];
//            [rs21 close];
//
//            strTYPE1 = [dictData objectForKey:@"TYPE1"];
//
//            if ([strTYPE1 isEqualToString:@"P"]) {
//                priceCode = priceCode - (priceCode * 0.05);
//                discountValwithKey1 = priceCode;
//            }
//            else{
//
//                //compare discountValwithKey1 and discountValwithKey2
//
//                discountValwithKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
//                discountValwithKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
//
//                if (discountValwithKey1 == 0) {
//                    DiscountVal1 =discountValwithKey2;
//                }
//                else if (discountValwithKey2 == 0){
//                    DiscountVal1 = discountValwithKey1;
//                }
//                else{
//                    if (discountValwithKey1 > discountValwithKey2 ) {
//                        DiscountVal1 = discountValwithKey2;
//                    }
//                    else{
//                        DiscountVal1 = discountValwithKey1;
//                    }
//                }
//            }
//        }
//        if ([rs221 next]) {
//            NSDictionary *dictData = [rs221 resultDictionary];
//            [rs221 close];
//            strTYPE1 = [dictData objectForKey:@"TYPE1"];
//
//            if ([strTYPE1 isEqualToString:@"P"]) {
//                priceCode = priceCode - (priceCode * 0.05);
//                discountValwithOutKey1 = priceCode;
//            }
//            else{
//
//                //compare discountValwithOutKey1 and discountValwithOutKey2
//
//                discountValwithOutKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
//                discountValwithOutKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
//
//                if (discountValwithOutKey1 == 0) {
//                    DiscountVal2 =discountValwithOutKey2;
//                }
//                else if (discountValwithOutKey2 == 0){
//                    DiscountVal2 = discountValwithOutKey1;
//                }
//                else{
//                    if (discountValwithOutKey1 > discountValwithOutKey2 ) {
//                        DiscountVal2 = discountValwithOutKey2;
//                    }
//                    else{
//                        DiscountVal2 = discountValwithOutKey1;
//                    }
//                }
//            }
//        }
//
//
//        // Final comparison DiscountVal1 and DiscountVal2
//        if (DiscountVal1 == 0) {
//            discount =DiscountVal2;
//        }
//        else if (DiscountVal2 == 0){
//            discount = DiscountVal1;
//        }
//        else{
//            if (DiscountVal1 > DiscountVal2 ) {
//                discount = DiscountVal2;
//            }
//            else{
//                discount = DiscountVal1;
//            }
//        }
//
//
//        if (discount == 0) {
//            strTYPE1 = ALLOW_SPECIAL_PRICE;
//        }
//
//
//        if(priceCode > 0 && discount > 0)
//        {
//            if(priceCode > discount)
//            {
//                self.strPrice = [NSString stringWithFormat:@"%f",discount];
//            }
//            else
//            {
//                self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
//            }
//        }
//        else if (priceCode == 0)
//        {
//            self.strPrice = [NSString stringWithFormat:@"%f",discount];
//        }
//        else if (discount == 0)
//        {
//            self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
//        }
//        else
//        {
//            self.strPrice = @"0";
//        }
//
//    }@catch (NSException* e) {
//        // rethrow if not one of the two exceptions above
//
//        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
//            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
//        }];
//
//
//    }
//
//}

-(void)callGetDiscountFromDB:(FMDatabase *)db{
    
    NSLog(@"strProductCode %@",strProductCode);
    
    @try {
        
        float priceCode = 0;
        float discount = 0;
        
        //For Last Price
        FMResultSet *rs = [db executeQuery:@"SELECT PRICE FROM arhisdet WHERE STOCK_CODE = ? AND DEBTOR = ? ORDER BY DATE_RAISED desc LIMIT 1",stock_code,strDebtor];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs next])
        {
            lastPrice = [rs stringForColumn:@"PRICE"];
        }
        [rs close];
        
        //For Price Code - NEW LOGIC SACNTRCT
        
        FMResultSet *rs001 = [db executeQuery:@"SELECT AGREED_SELL_PRC,MARGIN_PCT  FROM sacntrct WHERE CODE = ? AND STOCK_CODE = ? AND FROM_DATE <  DATETIME('now')  and TO_DATE > DATETIME('now') LIMIT 1",strDebtor,stock_code];
        
        //FMResultSet *rs001 = [db executeQuery:@"SELECT AGREED_SELL_PRC,MARGIN_PCT  FROM sacntrct WHERE CODE = ? AND STOCK_CODE = ? AND TO_DATE BETWEEN DATETIME('now') AND DATETIME('now','360 days') LIMIT 1",strDebtor,stock_code];
        
        if (!rs001){
            [rs001 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        NSDictionary *dictSacntrct = [NSDictionary new];
        if ([rs001 next])
        {
            dictSacntrct = [rs001 resultDictionary];
        }
        [rs001 close];
        
        if (dictSacntrct.count > 0) {
            self.strPrice = [dictSacntrct objectForKey:@"AGREED_SELL_PRC"];
            self.strMargin = [dictSacntrct objectForKey:@"MARGIN_PCT"];
            isProductFromSacontrct = @"Y";
        }
        else{
            
            FMResultSet *rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor];
            
            if (!rs1)
            {
                [rs1 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs1 next]) {
                NSDictionary *dictData = [rs1 resultDictionary];
                
                switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                    case 0:
                    {
                        if ([dictProductDetails objectForKey:@"Price1"])
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                        }
                        else if([dictProductDetails objectForKey:@"Price"])
                        {
                            priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                    }
                        break;
                        
                    case 2:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price2"] floatValue];
                    }
                        break;
                        
                    case 3:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price3"] floatValue];
                    }
                        break;
                        
                    case 4:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price4"] floatValue];
                    }
                        break;
                        
                    case 5:
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price5"] floatValue];
                    }
                        break;
                        
                        
                    default:{
                        priceCode = [[dictProductDetails objectForKey:@"Price1"] floatValue];
                    }
                        break;
                        
                }
                
                if ([strPrice isEqual:[NSNull null]] || [strPrice isEqualToString:@"(null)"])
                {
                    if([dictProductDetails objectForKey:@"Price"])
                    {
                        priceCode = [[dictProductDetails objectForKey:@"Price"] floatValue];
                    }
                }
            }
            else
            {
                priceCode = 0;
            }
            
            [rs1 close];
            
            
            //For Dicount
            FMResultSet *rs21 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ? AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", strProductCode, strDebtor];
            
            FMResultSet *rs221 = [db executeQuery:@"SELECT DISCOUNT1,TYPE1,DISCOUNT2,TYPE2 FROM sysdisct WHERE STOCK_CODE = ? AND KEY_FIELD = ''  AND ((date() between DATE_FROM1 and DATE_TO1) OR (date() between DATE_FROM2 and DATE_TO2))", strProductCode];
            
            
            float DiscountVal1 = 0;
            float DiscountVal2 = 0;
            
            float discountValwithKey1 = 0;
            float discountValwithKey2 = 0;
            
            
            float discountValwithOutKey1 = 0;
            float discountValwithOutKey2 = 0;
            
            
            if (!rs21)
            {
                [rs21 close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            if ([rs21 next])
            {
                NSDictionary *dictData = [rs21 resultDictionary];
                [rs21 close];
                
                strTYPE1 = [dictData objectForKey:@"TYPE1"];
                
                if ([strTYPE1 isEqualToString:@"P"]) {
                    priceCode = priceCode - (priceCode * 0.05);
                    discountValwithKey1 = priceCode;
                }
                else if ([strTYPE1 isEqualToString:@"M"]){
                    
                    NSString *discount1Val = [dictData objectForKey:@"DISCOUNT1"];
                    NSString *strCost = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Cost"]];
                    
                    strMargin =[dictData objectForKey:@"DISCOUNT1"];
                    
                    CGFloat sellprice = ([strCost floatValue] / ( 1- ([discount1Val floatValue] / 100)));
                    strPrice = [NSString stringWithFormat:@"%.2f",sellprice];
                    return;
                }
                else{
                    
                    //compare discountValwithKey1 and discountValwithKey2
                    
                    discountValwithKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    discountValwithKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                    
                    if (discountValwithKey1 == 0) {
                        DiscountVal1 =discountValwithKey2;
                    }
                    else if (discountValwithKey2 == 0){
                        DiscountVal1 = discountValwithKey1;
                    }
                    else{
                        if (discountValwithKey1 > discountValwithKey2 ) {
                            DiscountVal1 = discountValwithKey2;
                        }
                        else{
                            DiscountVal1 = discountValwithKey1;
                        }
                    }
                }
            }
            
            
            if ([rs221 next]) {
                NSDictionary *dictData = [rs221 resultDictionary];
                [rs221 close];
                strTYPE1 = [dictData objectForKey:@"TYPE1"];
                
                if ([strTYPE1 isEqualToString:@"P"]) {
                    priceCode = priceCode - (priceCode * 0.05);
                    discountValwithOutKey1 = priceCode;
                }
                
                else{
                    
                    //compare discountValwithOutKey1 and discountValwithOutKey2
                    
                    discountValwithOutKey1 = [[dictData objectForKey:@"DISCOUNT1"] floatValue];
                    discountValwithOutKey2 = [[dictData objectForKey:@"DISCOUNT2"] floatValue];
                    
                    if (discountValwithOutKey1 == 0) {
                        DiscountVal2 =discountValwithOutKey2;
                    }
                    else if (discountValwithOutKey2 == 0){
                        DiscountVal2 = discountValwithOutKey1;
                    }
                    else{
                        if (discountValwithOutKey1 > discountValwithOutKey2 ) {
                            DiscountVal2 = discountValwithOutKey2;
                        }
                        else{
                            DiscountVal2 = discountValwithOutKey1;
                        }
                    }
                }
            }
            
            
            // Final comparison DiscountVal1 and DiscountVal2
            if (DiscountVal1 == 0) {
                discount =DiscountVal2;
            }
            else if (DiscountVal2 == 0){
                discount = DiscountVal1;
            }
            else{
                if (DiscountVal1 > DiscountVal2 ) {
                    discount = DiscountVal2;
                }
                else{
                    discount = DiscountVal1;
                }
            }
            
            
            if (discount == 0) {
                strTYPE1 = ALLOW_SPECIAL_PRICE;
            }
            
            
            if(priceCode > 0 && discount > 0)
            {
                if(priceCode > discount)
                {
                    self.strPrice = [NSString stringWithFormat:@"%f",discount];
                }
                else
                {
                    self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
                }
            }
            else if (priceCode == 0)
            {
                self.strPrice = [NSString stringWithFormat:@"%f",discount];
            }
            else if (discount == 0)
            {
                self.strPrice = [NSString stringWithFormat:@"%f",priceCode];
            }
            else
            {
                self.strPrice = @"0";
            }
            
        }
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
        
    }
    
}


-(void)callGetTaxInfoFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT TAX_CODE1 FROM sysistax WHERE CODE = ?",[dictProductDetails objectForKey:@"ITEM"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            if ([[dictData objectForKey:@"TAX_CODE1"] intValue] == -1) {
                self.isTaxApplicable = NO;
            }
            else{
                self.isTaxApplicable = YES;
            }
            
            //dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
        
    }
    
}

-(void)callGetAvailabilityFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT AVAILABLE FROM samaster WHERE CODE = ? AND WAREHOUSE = ?",[dictProductDetails objectForKey:@"ITEM"],[dictProductDetails objectForKey:@"WAREHOUSE"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            //            //NSLog(@"[strQuantityOrdered intValue] %@",strQuantityOrdered);
            //            if (quantityOrdered > [[dictData objectForKey:@"AVAILABLE"] intValue]) {
            //
            //                NSString *strMessage = [NSString stringWithFormat:@"Order quantity is more than Available(%@)",[dictData objectForKey:@"AVAILABLE"]];
            //
            //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            //                [alert show];
            //
            //                strMessage = nil;
            //            }
            //            else{
            //                [self actionSetPrice];
            //            }
            
            // 31stMarch2015 : Change request
            [self actionSetPrice];
            dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
}

-(void)callUpdateAvailableToDB:(FMDatabase *)db{
    
    @try {
        BOOL y = [db executeUpdate:@"UPDATE samaster SET AVAILABLE = ? WHERE WAREHOUSE = ? AND CODE = ?",strAvailable,strWarehouse,strProductCode];
        
        if (!y)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
        
    }
    
}

#pragma mark - WS calls
-(void)callWSGetAvailable{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_PRODUCT_GET_AVIALABLE_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"stock_code=%@&warehouse=%@&customer=%@",strProductCode,strWarehouse,strDebtor];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,PRODUCT_GET_AVAILABLE_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    NSLog(@"dictResponse %@",dictResponse);
    [spinner removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_PRODUCT_GET_AVIALABLE_DETAILS"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"]) {
            
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"])
            {
                
                if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]])
                {
                    self.strAvailable = [NSString stringWithFormat:@"%@",[[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] objectForKey:@"Available"] objectForKey:@"text"]];
                    
                    if([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] objectForKey:@"Price"] objectForKey:@"text"])
                    {
                        lastPrice = [NSString stringWithFormat:@"%@",[[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"]objectForKey:@"Price"] objectForKey:@"text"]];
                    }
                    else
                    {
                        lastPrice = @"";
                    }
                    
                }
                else
                {
                    
                    self.strAvailable=@"";
                    lastPrice=@"";
                    
                    if([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] isKindOfClass:[NSArray class]])
                    {
                        NSArray *arrData=[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"];
                        
                        if ([arrData count]>0)
                        {
                            self.strAvailable = [NSString stringWithFormat:@"%@",[[[arrData objectAtIndex:0] objectForKey:@"Available"] objectForKey:@"text"]];
                        }
                        
                        if (arrData.count>1)
                        {
                            if([[[[arrData objectAtIndex:1] objectForKey:@"Price"] objectForKey:@"text"]isKindOfClass:[NSString class]])
                            {
                                lastPrice = [NSString stringWithFormat:@"%@",[[[arrData objectAtIndex:1] objectForKey:@"Price"] objectForKey:@"text"]];
                            }
                        }
                        
                    }
                    
                }
                
                [operationQueue addOperationWithBlock:^{
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callUpdateAvailableToDB:db];
                    }];
                    
                    databaseQueue = nil;
                    
                }];
                
            }
            else{
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
        else{
            
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [tblDetails reloadData];
        }];
        
    }
    else if ([strWebserviceType isEqualToString:@"WS_CHECK_AVIALABLITY"]){
        if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            [tblDetails reloadData];
            [self actionSetPrice];
        }
        else if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"False"]) {
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@(%@)",[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"message"] objectForKey:@"text"],[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:CheckAvailableResponse"] objectForKey:@"available"] objectForKey:@"text"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
        
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


#pragma mark - Custom methods

/*
 -(void)showCostMargin:(id)sender{
 @try {
 isCostMarginVisible = (isCostMarginVisible)? NO: YES;
 
 UIButton *btn = (UIButton *)sender;
 NSMutableArray *tempArray = [[NSMutableArray alloc] init];
 
 NSMutableArray *arr = [arrProductLabels objectAtIndex:btn.tag];
 
 [tempArray addObject:[NSIndexPath indexPathForRow:0 inSection:btn.tag]];
 [tempArray addObject:[NSIndexPath indexPathForRow:1 inSection:btn.tag]];
 
 
 if(isCostMarginVisible)
 {
 [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
 [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
 
 NSMutableDictionary *dictMargin = [[NSMutableDictionary alloc] init];
 [dictMargin setValue:@"Margin" forKey:@"Label"];
 [arr insertObject:dictMargin atIndex:0];
 
 NSMutableDictionary *dictCost = [[NSMutableDictionary alloc] init];
 [dictCost setValue:@"Cost" forKey:@"Label"];
 [arr insertObject:dictCost atIndex:1];
 [tblDetails beginUpdates];
 [tblDetails insertRowsAtIndexPaths:(NSArray *)tempArray withRowAnimation:UITableViewRowAnimationBottom];
 //            [tblDetails insertRowsAtIndexPaths:(NSArray *)arr withRowAnimation:UITableViewRowAnimationBottom];
 
 [tblDetails endUpdates];
 
 }
 else{
 
 [btn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
 [arr removeObjectAtIndex:0];
 [arr removeObjectAtIndex:1];
 
 [tblDetails beginUpdates];
 [tblDetails deleteRowsAtIndexPaths:(NSArray *)tempArray  withRowAnimation:UITableViewRowAnimationFade];
 [tblDetails endUpdates];
 
 }
 
 }
 @catch (NSException *exception) {
 NSLog(@"Eror::%@",exception.description);
 }
 }*/

//Change by Subhu
-(void)showCostMargin:(id)sender{
    @try {
        isCostMarginVisible = (isCostMarginVisible)? NO: YES;
        
        UIButton *btn = (UIButton *)sender;
        
        if(isCostMarginVisible)
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"minus.png"]  forState:UIControlStateNormal];
        }
        else
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"plus.png"]  forState:UIControlStateNormal];
        }
        
        [tblDetails reloadData];
    }
    @catch (NSException *exception) {
        NSLog(@"Eror::%@",exception.description);
    }
}

- (IBAction)donePressed:(id)sender
{
    if(strTYPE1.length > 0)
    {
        if([strTYPE1 isEqualToString:@"A"] || [strTYPE1 isEqualToString:@"T"])
        {
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
        }
        else if ([self calcMargin]  < MARGIN_MINIMUM_PERCENT && [[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"])
        {
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            if(self.strMargin.floatValue <= 0)
            {
                [AJNotificationView showNoticeInView:self.view
                                                type:AJNotificationTypeRed
                                               title:MARGIN_PERCENT_ERROR
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                
                [tblDetails reloadData];
                return;
            }
            
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetAvailabilityFromDB:db];
                }];
                databaseQueue = nil;
            }
            
        }
    }
    else
    {
        if ([self calcMargin] < MARGIN_MINIMUM_PERCENT)//[[dictProductDetails objectForKey:@"PriceChnaged"] isEqualToString:@"Y"]
        {
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            
            [AJNotificationView showNoticeInView:self.view
                                            type:AJNotificationTypeRed
                                           title:MARGIN_PERCENT_ERROR
                                 linedBackground:AJLinedBackgroundTypeDisabled
                                       hideAfter:2.5f];
            
            [tblDetails reloadData];
            return;
            
        }
        else{
            self.strExcl = [NSString stringWithFormat:@"%f",[self calcExcl]];
            self.strTax = [NSString stringWithFormat:@"%f",[self calcTax]];
            self.strMargin = [NSString stringWithFormat:@"%f",[self calcMargin]];
            self.strExtnIncl = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
            
            [tblDetails reloadData];
            
            if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
            {
                [self actionSetPrice];
            }
            else
            {
                ///--Dont check the avaliable.Directly set the quantity Set
                /*
                 FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                 [databaseQueue   inDatabase:^(FMDatabase *db) {
                 [self callGetAvailabilityFromDB:db];
                 }];
                 databaseQueue = nil;
                 */
                [self actionSetPrice];
            }
            
        }
    }
    
    NSLog(@"done pressed");
}
@end


