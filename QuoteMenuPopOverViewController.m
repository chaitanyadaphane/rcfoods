//
//  QuoteMenuPopOverViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "QuoteMenuPopOverViewController.h"
#import "QuoteProductListViewController.h"

@interface QuoteMenuPopOverViewController ()

@end

@implementation QuoteMenuPopOverViewController
@synthesize tblMenuList,menuList,delegate,isFinalisedNeeded,isCommentsNotNeeded,isFreightNotNeeded,isProfileOrderEntry,isFromOfflineOrder,isProfileMaintenance;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    menuList = [[NSMutableArray alloc] init];
    [self setMenus];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    self.tblMenuList = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setMenus{
    //--Removed Comment from PopUp
    if (isFinalisedNeeded)
    {
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
              menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Products",@"Save for later",@"Finalize",nil];
        }
        else{
            menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Products",@"Save",nil];
        }
    }
    else if (isProfileOrderEntry){
        menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Comments",@"Add Products",@"Review",@"Save",nil];
    }
    else if (isFromOfflineOrder){
        menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Comments",@"Add Products",@"Save",nil];
    }
    else if (isProfileMaintenance)
    {
        menuList = [NSMutableArray arrayWithObjects:@"Add Products",@"Remove Products",nil];

    }
    else{
       menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Comments",@"Add Products",@"Review",@"Save",nil];
       //  menuList = [NSMutableArray arrayWithObjects:@"Add Freight",@"Add Products",@"Add Specials",@"Save",nil];
    }
    
    if (isFreightNotNeeded) {
        [menuList removeObjectAtIndex:0];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [menuList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:CellIdentifier];
    }
    
    // Set up the cell...
    NSString *cellValue = [menuList objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [delegate dismissPopOver];
    
    //--Changed As no comment
    
    if (isFinalisedNeeded) {
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
            switch ([indexPath row]) {
                    //Add Product
                case 0:
                {
                    [delegate showProductList];
                }
                    break;
                    //Save for later
                case 1:
                {
                    [delegate actionSaveQuote:0];
                }
                    break;
                    
                    //save
                case 2:
                {
                    [delegate actionSaveQuote:1];
                }
                    break;
                    
                default:
                    break;
            }
        }
        else{
            switch ([indexPath row]) {
                case 0:
                {
                    [delegate showProductList];
                }
                    break;
                case 1:
                {
                    [delegate actionSaveQuote:0];
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    else{
        if (isFreightNotNeeded) {
            switch ([indexPath row]) {
                    
                case 0:
                {
                    [delegate showProductList];
                }
                    break;
                    
                    //save
                case 1:
                {
                    [delegate actionSaveQuote:0];
                }
                    break;
                    //FInilize
                case 2:
                {
                    [delegate actionSaveQuote:1];
                    NSLog(@"Add Specials");
                    //[delegate actionSaveQuote:1];
                }
                    break;
                    //finalise
                    
                case 3:
                {
                    [delegate actionSaveQuote:1];
                    //                [delegate reviewOrderList];
                }
                    break;
                case 4:
                {
                    [delegate reviewOrderList];
                    //                [delegate actionSaveQuote:1];
                }
                    break;
                    
                default:
                    break;
            }
        }
        else if (isProfileOrderEntry){
            switch ([indexPath row]) {
                case 0:
                {
                    [delegate showFreightView];
                }
                    break;
                case 1:
                {
                    [delegate showCommentView];
                }
                    break;
                case 2:
                {
                    [delegate showProductList];
                }
                    break;
                case 3:
                {
                    [delegate reviewOrderList];
                }
                    break;
                    //save
                case 4:
                {
                    [delegate actionSaveQuote:0];

                    //                [delegate actionSaveQuote:1];
                }
                    break;
     
                default:
                    break;
            }
            
        }
        else if (isFromOfflineOrder){
            switch ([indexPath row]) {
                case 0:
                {
                    [delegate showFreightView];
                }
                    break;
                case 1:
                {
                    [delegate showCommentView];
                }
                    break;
                case 2:
                {
                    [delegate showProductList];
                }
                    break;
                    //save
                case 3:
                {
                    [delegate actionSaveQuote:0];
                }
                    break;
                    
                    //finalise
                default:
                    break;
            }
            
        }
        else if (isProfileMaintenance)
        {
            switch ([indexPath row]) {
                case 0:
                {
                    [delegate showProductList];
                }
                    break;
                case 1:
                {
                    [delegate deleteProductList];
                }
                    break;
                                   
                    //finalise
                default:
                    break;
            }
            
        }
        else{
            switch ([indexPath row]) {
                case 0:
                {
                    [delegate showFreightView];
                }
                    break;
                case 1:
                {
                    [delegate showCommentView];
                }
                    break;
                case 2:
                {
                    [delegate showProductList];
                }
                    break;
                    
                case 3:
                {
                    [delegate reviewOrderList];
                }
                    break;
                    
                    //save
                case 4:
                {
                    [delegate actionSaveQuote:0];
                }
                    break;
                    
                    //finalise
            
                default:
                    break;
            }
        }
        
    }
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
          [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
          break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        default:{
            
        }
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
    
    [self setMenus];
    [tblMenuList reloadData];
}

@end
