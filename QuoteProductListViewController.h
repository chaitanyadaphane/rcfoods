//
//  QuoteProductListViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 21/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToLoadMoreView.h"

@interface QuoteProductListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchDisplayDelegate,UISearchBarDelegate>{

    NSString *strWebserviceType;
    
    NSMutableArray *filteredListContent;	// The content filtered as a result of a search.
    NSMutableArray *arrProducts; // The master content.
    NSMutableArray *arrSelectedProducts;
    
    int currPage;
    int totalCount;    
    int recordNumber;
    
    PullToLoadMoreView* _loadMoreFooterView;
    
    BOOL isNetworkConnected;
    
    BOOL isSearchingOn;
    
    NSOperationQueue *operationQueue;
}

@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrProducts;
@property (nonatomic, retain) NSMutableArray *arrSelectedProducts;
@property (nonatomic, assign)  BOOL isSearchingOn;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblProductList;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddToDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UISearchBar *searchingBar;

@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwResults;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;

@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;


#pragma mark - WS calls
- (void)callWSGetProductList:(int)pageIndex;
- (IBAction)actionAddProductsToDetails:(id)sender;
-(void)callGetProductListFromDB:(FMDatabase *)db;
@end
