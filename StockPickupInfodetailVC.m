//
//  StockPickupInfodetailVC.m
//  Blayney
//
//  Created by Pooja on 02/01/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import "StockPickupInfodetailVC.h"
#import "CustomCellGeneral.h"
#import "MJShowPhotoViewController.h"
#import "KGModal.h"

@interface StockPickupInfodetailVC (){
    MJShowPhotoViewController *detailViewController;
    NSMutableArray *imageAttachedArr;
    UIButton *showphotoBtn;
//    NSString *strPickup_No;
    NSString * strWebserviceType ;
}

@end

@implementation StockPickupInfodetailVC
@synthesize arrStockPickupInfoTitle,arrStockPickupInfoValues,dictStockValue;
@synthesize strDebtorName,strPickupNo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"StockPickupInfoDetail" Type:@"plist"];
    
    arrStockPickupInfoTitle = [[NSMutableArray alloc]init];
    imageAttachedArr = [[NSMutableArray alloc]init];
    
    // Build the array from the plist
    arrStockPickupInfoTitle = [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (strPickupNo.length > 0) {
        [self getStockPickupDetailInfoFromDB:strPickupNo];
    }
    
    _deborNameLbl.text = strDebtorName;
    

}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.deborNameLbl = nil;
    self.tblView = nil;
    imageAttachedArr = nil;
    arrStockPickupInfoTitle = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getStockPickupDetailInfoFromDB:(NSString *)pickupno{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        NSString *sqlquery = [NSString stringWithFormat:@"SELECT * FROM StockPickup where PICKUP_NO = %@",strPickupNo];
        FMResultSet *res = [db executeQuery:sqlquery];
        
        if (!res)
        {
            NSLog(@"Error: %@", [db lastErrorMessage]);
            //[[AppDelegate getAppDelegateObj].database close];
            return;
        }
        
        dictStockValue = [[NSMutableDictionary alloc] init];
        NSLog(@"str Pickup %@",strPickupNo);
        
        while ([res next]) {
            NSDictionary *dictData = [res resultDictionary];
            
            
            strPickupNo = [dictData objectForKey:@"PICKUP_NO"];

            NSString *strDateRaised = [dictData objectForKey:@"TODAY_DATE"];
            NSString *strRequestedBy = [dictData objectForKey:@"REQUESTED_BY"];
            NSString *strDebtor = [dictData objectForKey:@"DEBTOR"];
            
            NSString *strDebtorName1 = [dictData objectForKey:@"DEBTOR_NAME"];
            NSString *strContactAccNo = [dictData objectForKey:@"CONTACT_ACCOUNT_NO"];
            NSString *strContactPhoneNo = [dictData objectForKey:@"CONTACT_PHONE_NUMBER"];
            
            NSString *strProductno = [dictData objectForKey:@"PRODUCT_CODE"];
            NSString *strProductName =[dictData objectForKey:@"PRODUCT_NAME"];
            NSString *strBatchNo = [dictData objectForKey:@"BATCH_NUMBER"];
            
            NSString *strBatchName = [dictData objectForKey:@"BATCH_NAME"];
            NSString *strInvoiceAttached = [dictData objectForKey:@"INVOICE_ATTACHED"];
            NSString *strEmailTo = [dictData objectForKey:@"EMAIL_TO"];
            
            NSString *strEmailCC = [dictData objectForKey:@"EMAIL_CC"];
            NSString *strReasonRtn = [dictData objectForKey:@"REASON_OF_RETURN"];
            NSString *strReasonRtnDetail = [dictData objectForKey:@"DETAIL_REASON_RETURN"];
            
            NSString *best_before_date = [dictData objectForKey:@"BEST_BEFORE_DATE"];
            NSString *relatedInvoiceAttached = [dictData objectForKey:@"RELATED_INVOICE_NO"];
            NSString *isEmailSent = [dictData objectForKey:@"isEmailSent"];
            
                if (![[dictData objectForKey:@"IMAGE_ATTACHED1"] isKindOfClass:[NSNull class]] || [dictData objectForKey:@"IMAGE_ATTACHED1"] != nil || [[dictData objectForKey:@"IMAGE_ATTACHED1"]length ] > 0) {
                    NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED1"];
                    [imageAttachedArr addObject:imagedata];
                }
                 if (![[dictData objectForKey:@"IMAGE_ATTACHED2"] isKindOfClass:[NSNull class]] || [dictData objectForKey:@"IMAGE_ATTACHED2"] != nil || [[dictData objectForKey:@"IMAGE_ATTACHED2"]length ] > 0) {
                    NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED2"];
                    [imageAttachedArr addObject:imagedata];
                }
                 if (![[dictData objectForKey:@"IMAGE_ATTACHED3"] isKindOfClass:[NSNull class]] || [dictData objectForKey:@"IMAGE_ATTACHED3"] != nil || [[dictData objectForKey:@"IMAGE_ATTACHED3"]length ] > 0) {
                    NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED3"];
                    [imageAttachedArr addObject:imagedata];
                }
                 if (![[dictData objectForKey:@"IMAGE_ATTACHED4"] isKindOfClass:[NSNull class]] || [dictData objectForKey:@"IMAGE_ATTACHED4"] != nil || [[dictData objectForKey:@"IMAGE_ATTACHED4"]length ] > 0) {
                    NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED4"];
                    [imageAttachedArr addObject:imagedata];
                }
                 if (![[dictData objectForKey:@"IMAGE_ATTACHED5"] isKindOfClass:[NSNull class]] || [dictData objectForKey:@"IMAGE_ATTACHED5"] != nil || [[dictData objectForKey:@"IMAGE_ATTACHED5"]length ] > 0) {
                    NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED5"];
                    [imageAttachedArr addObject:imagedata];
                }

            [dictStockValue setObject:strDateRaised forKey:@"DATE_RAISED"];
            [dictStockValue setObject:strRequestedBy forKey:@"REQUESTED_BY"];
            [dictStockValue setObject:strDebtor forKey:@"DEBTOR_NO"];

            [dictStockValue setObject:strDebtorName1 forKey:@"DEBTOR_NAME"];
            [dictStockValue setObject:strContactAccNo forKey:@"CONTACT_ACCOUNT_NO"];
            [dictStockValue setObject:strContactPhoneNo forKey:@"CONTACT_PHONE_NUMBER"];

            [dictStockValue setObject:strProductno forKey:@"PRODUCT_NO"];
            [dictStockValue setObject:strProductName forKey:@"PRODUCT_NAME"];
            [dictStockValue setObject:strBatchNo forKey:@"BATCH_NUMBER"];

            [dictStockValue setObject:strBatchName forKey:@"BATCH_NAME"];
            [dictStockValue setObject:strInvoiceAttached forKey:@"INVOICE_ATTACHED"];
            [dictStockValue setObject:strEmailTo forKey:@"EMAIL_TO"];

            [dictStockValue setObject:strEmailCC forKey:@"EMAIL_CC"];
            [dictStockValue setObject:strReasonRtn forKey:@"REASON_OF_RETURN"];
            [dictStockValue setObject:strReasonRtnDetail forKey:@"DETAIL_REASON_RETURN"];
            
            [dictStockValue setObject:strProductName forKey:@"PRODUCT_NAME"];
            [dictStockValue setObject:strBatchName forKey:@"BATCH_NAME"];
            
            [dictStockValue setObject:best_before_date forKey:@"BEST_BEFORE_DATE"];
            [dictStockValue setObject:relatedInvoiceAttached forKey:@"RELATED_INVOICE_NO"];
            [dictStockValue setObject:isEmailSent forKey:@"isEmailSent"];

            dictData = nil;
        }
        
        [res close];
    }];

}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return [arrStockPickupInfoTitle count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)[arrStockPickupInfoTitle objectAtIndex:section] count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {

    
    static NSString *CellIdentifier16 = CELL_IDENTIFIER16;
    CustomCellGeneral *cell = nil;
    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier16];
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:15];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;


    @try {
        
        cell.lblTitle.text = [[(NSArray *)[arrStockPickupInfoTitle objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
        cell.showPhotoBtn.hidden = YES;


        switch ([indexPath section]) {
            case 0:{
                                cell.lblValue.hidden = NO;

                if ([indexPath row] == 0) {
                    if ([dictStockValue objectForKey:@"DATE_RAISED"]) {
                        if([[dictStockValue objectForKey:@"DATE_RAISED"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"DATE_RAISED"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"DATE_RAISED"]];
                        }
                        else{
                            cell.lblValue.text = @"---";

                        }
                    }
                }
                else if ([indexPath row] == 1){
                    
                    NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",strSalesman];
                }
                else if ([indexPath row] == 2){
                    if ([dictStockValue objectForKey:@"DEBTOR_NAME"]) {
                        if([[dictStockValue objectForKey:@"DEBTOR_NAME"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"DEBTOR_NAME"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"DEBTOR_NAME"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 3){
                    if ([dictStockValue objectForKey:@"DEBTOR_NO"]) {
                        if([[dictStockValue objectForKey:@"DEBTOR_NO"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"DEBTOR_NO"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"DEBTOR_NO"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 4){
                    if ([dictStockValue objectForKey:@"CONTACT_ACCOUNT_NO"]) {
                        if([[dictStockValue objectForKey:@"CONTACT_ACCOUNT_NO"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"CONTACT_ACCOUNT_NO"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"CONTACT_ACCOUNT_NO"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 5){
                    if ([dictStockValue objectForKey:@"CONTACT_PHONE_NUMBER"]) {
                        if([[dictStockValue objectForKey:@"CONTACT_PHONE_NUMBER"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"CONTACT_PHONE_NUMBER"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"CONTACT_PHONE_NUMBER"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 6){
                    if ([dictStockValue objectForKey:@"PRODUCT_NO"]) {
                        if([[dictStockValue objectForKey:@"PRODUCT_NO"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"PRODUCT_NO"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"PRODUCT_NO"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 7){
                    if ([dictStockValue objectForKey:@"PRODUCT_NAME"]) {
                        if([[dictStockValue objectForKey:@"PRODUCT_NAME"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"PRODUCT_NAME"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"PRODUCT_NAME"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 8){
                    if ([dictStockValue objectForKey:@"BATCH_NUMBER"]) {
                        if([[dictStockValue objectForKey:@"BATCH_NUMBER"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"BATCH_NUMBER"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"BATCH_NUMBER"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 9){
                    if ([dictStockValue objectForKey:@"BATCH_NAME"]) {
                        if([[dictStockValue objectForKey:@"BATCH_NAME"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"BATCH_NAME"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"BATCH_NAME"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 10){
                    if ([dictStockValue objectForKey:@"BEST_BEFORE_DATE"]) {
                        if([[dictStockValue objectForKey:@"BEST_BEFORE_DATE"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"BEST_BEFORE_DATE"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"BEST_BEFORE_DATE"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
//
                else if ([indexPath row] == 11){
                    if ([dictStockValue objectForKey:@"REASON_OF_RETURN"]) {
                        if([[dictStockValue objectForKey:@"REASON_OF_RETURN"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"REASON_OF_RETURN"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"REASON_OF_RETURN"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 12){
                    if ([dictStockValue objectForKey:@"RELATED_INVOICE_NO"]) {
                        if([[dictStockValue objectForKey:@"RELATED_INVOICE_NO"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"RELATED_INVOICE_NO"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"RELATED_INVOICE_NO"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                cell.showPhotoBtn.hidden = YES;
            }
                break;
                
            case 1:{
                cell.showPhotoBtn.hidden = NO;
                cell.lblValue.hidden = YES;
                if ([indexPath row] == 0){
                    cell.lblValue.text = nil;
                        cell.showPhotoBtn.enabled = TRUE;
//                        cell.showPhotoBtn.tag = [strPickupNo intValue];
                        [cell.showPhotoBtn addTarget:Nil action:@selector(showphotoAction:) forControlEvents:UIControlEventTouchUpInside];
                    return cell;
                }
                
            }
                break;
                
            case 2:{
                cell.lblValue.hidden = NO;


                 if ([indexPath row] == 0){
                    if ([dictStockValue objectForKey:@"EMAIL_TO"]) {
                        if([[dictStockValue objectForKey:@"EMAIL_TO"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"EMAIL_TO"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"EMAIL_TO"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 1){
                    if ([dictStockValue objectForKey:@"EMAIL_CC"]) {
                        if([[dictStockValue objectForKey:@"EMAIL_CC"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"EMAIL_CC"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"EMAIL_CC"]];
                        }
                        else{
                            cell.lblValue.text = @"---";
                            
                        }
                    }
                }
                else if ([indexPath row] == 2){
                    if ([dictStockValue objectForKey:@"isEmailSent"]) {
                        if([[dictStockValue objectForKey:@"isEmailSent"] isKindOfClass:[NSString class]] || [[dictStockValue objectForKey:@"isEmailSent"] isKindOfClass:[NSNumber class]])
                        {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictStockValue objectForKey:@"isEmailSent"]];
                        }
                        else{
                            cell.lblValue.text = @"No";
                        }
                    }
                }

                cell.showPhotoBtn.hidden = YES;
            }
                break;
            default:{
                cell.showPhotoBtn.hidden = YES;
            }
                break;
        }
        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"Error:::%@",exception.description);
    }
}


-(void)showphotoAction:(id)sender{
//    @try {
    
        detailViewController = [[MJShowPhotoViewController alloc] initWithNibName:@"MJShowPhotoViewController" bundle:nil];
        detailViewController.imageDataArr = imageAttachedArr;
        [[KGModal sharedInstance] showWithContentView:detailViewController.view andAnimated:YES];
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Eror::%@",exception.description);
//    }
}


- (IBAction)actionResendButton:(id)sender {
    
    // Get data from StockPickup table
    // Call Email Stock Pickup webservice
    [self getStockPickupFORWS:strPickupNo];
    [self CallServiceForStockPickupEmail];
}


#pragma mark - Email_for_StockPickup

-(void)updateEmailStockPickupSend:(NSString *)pickupNum{
    @try {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            NSString *strQuery = [NSString stringWithFormat:@"Update  StockPickup set isEmailSent ='YES' where PICKUP_NO = '%@'",pickupNum];
            BOOL result;
            result = [db executeUpdate:strQuery];
            
            if(result == NO)
            {
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
        }];
    }
    @catch (NSException *exception) {
        @throw [NSException exceptionWithName:@"Local DB Error" reason:@"Local DB Error" userInfo:nil];
    }
}



-(void)getStockPickupFORWS:(NSString *)orderNumStr{
    
    
    NSLog(@"strPickup_No :%@",strPickupNo);
    dictStockValue = [[NSMutableDictionary alloc] init];
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        NSString *sqlquery = [NSString stringWithFormat:@"SELECT * FROM StockPickup WHERE PICKUP_NO = '%@'",strPickupNo];
        
        NSLog(@"%@",sqlquery);
        FMResultSet *res = [db executeQuery:sqlquery];
        
        if (!res)
        {
            NSLog(@"Error: %@", [db lastErrorMessage]);
            //[[AppDelegate getAppDelegateObj].database close];
            return;
        }
        
        NSLog(@"str Order Num %@",strPickupNo);
        
        while ([res next]) {
            NSDictionary *dictData = [res resultDictionary];
            
            
            NSString *pickupNo =[dictData objectForKey:@"PICKUP_NO"];
            NSString *dateRaised = [dictData objectForKey:@"TODAY_DATE"];
            NSString *requestedBy = [dictData objectForKey:@"REQUESTED_BY"];
            NSString *customerId = [dictData objectForKey:@"DEBTOR"];
            
            NSString *customerName = [dictData objectForKey:@"DEBTOR_NAME"];
            NSString *contactAccountNo = [dictData objectForKey:@"CONTACT_ACCOUNT_NO"];
            NSString *contactPhone = [dictData objectForKey:@"CONTACT_PHONE_NUMBER"];
            
            NSString *relatedInvoiceNo = [dictData objectForKey:@"RELATED_INVOICE_NO"];
            
            NSString *productCode = [dictData objectForKey:@"PRODUCT_CODE"];
            NSString *productDesc =[dictData objectForKey:@"PRODUCT_NAME"];
            NSString *batchNo = [dictData objectForKey:@"BATCH_NUMBER"];
            
            NSString *strBatchName = [dictData objectForKey:@"BATCH_NAME"];
            NSString *bestBeforedate = [dictData objectForKey:@"BEST_BEFORE_DATE"];
            NSString *emailto = [dictData objectForKey:@"EMAIL_TO"];
            
            NSString *emailcc = [dictData objectForKey:@"EMAIL_CC"];
            NSString *reason = [dictData objectForKey:@"REASON_OF_RETURN"];
            NSString *comment = [dictData objectForKey:@"DETAIL_REASON_RETURN"];
            
            NSString *quantity = [dictData objectForKey:@"QUANTITY"];
            NSString *returnVal =[dictData objectForKey:@"RETURN"];
            
            
            
            //    $pickupNo = $_REQUEST['pickupNo'];
            //    $pickupNo = 5;
            //    $dateRaised = $_REQUEST['dateRaised'];
            //    $requestedBy = $_REQUEST['requestedBy'];
            //    $customerId = $_REQUEST['customerId'];
            //    $customerName = $_REQUEST['customerName'];
            //    $contactPhone = $_REQUEST['contactPhone'];
            //    $contactAccountNo = $_REQUEST['contactAccountNo'];
            //    $relatedInvoiceNo = $_REQUEST['relatedInvoiceNo'];
            //    $productCode = $_REQUEST['productCode'];
            //    $productDesc = $_REQUEST['productDesc'];
            //    $batchNo = $_REQUEST['batchNo'];
            //    $bestBeforedate = $_REQUEST['bestBeforedate'];
            //
            //    $stockcode = explode(" --- ", $productCode);
            //    $stock_returns = $stockcode[0] . "|" . $_REQUEST['quantity'] . "|" . $REQUEST['reason'] . "|" . $REQUEST['return'] . "|" . $REQUEST['comment'] . "|";
            //
            //    $emailto = $_REQUEST['emailto'];
            //    $emailcc = $_REQUEST['emailcc'];
            //    $imageAttached1 = $_REQUEST['imageAttached1'];
            //    $imageAttached2 = $_REQUEST['imageAttached2'];
            //    $imageAttached3 = $_REQUEST['imageAttached3'];
            //    $imageAttached4 = $_REQUEST['imageAttached4'];
            //    $imageAttached5 = $_REQUEST['imageAttached5'];
            
            
            [dictStockValue setObject:(pickupNo)?pickupNo:@"" forKey:@"pickupNo"];
            [dictStockValue setObject:(dateRaised)?dateRaised:@"" forKey:@"dateRaised"];
            [dictStockValue setObject:(requestedBy)?requestedBy:@"" forKey:@"requestedBy"];
            
            [dictStockValue setObject:(customerId)?customerId:@"" forKey:@"customerId"];
            [dictStockValue setObject:(customerName)?customerName:@"" forKey:@"customerName"];
            [dictStockValue setObject:(contactPhone)?contactPhone:@"" forKey:@"contactPhone"];
            
            [dictStockValue setObject:(contactAccountNo)?contactAccountNo:@"" forKey:@"contactAccountNo"];
            [dictStockValue setObject:(relatedInvoiceNo)?relatedInvoiceNo:@"" forKey:@"relatedInvoiceNo"];
            [dictStockValue setObject:(productCode)?productCode:@"" forKey:@"productCode"];
            
            [dictStockValue setObject:(productDesc)?productDesc:@"" forKey:@"productDesc"];
            [dictStockValue setObject:(batchNo)?batchNo:@"" forKey:@"batchNo"];
            [dictStockValue setObject:(strBatchName)?strBatchName:@"" forKey:@"BATCH_NAME"];
            
            [dictStockValue setObject:(reason)?reason:@"" forKey:@"reason"];
            [dictStockValue setObject:(quantity)?quantity:@"" forKey:@"quantity"];
            [dictStockValue setObject:(returnVal)?returnVal:@"" forKey:@"return"];
            [dictStockValue setObject:(comment)?comment:@"" forKey:@"comment"];
            
            [dictStockValue setObject:(bestBeforedate)?bestBeforedate:@"" forKey:@"bestBeforedate"];
            
            [dictStockValue setObject:(emailto)?emailto:@"" forKey:@"emailto"];
            [dictStockValue setObject:(emailcc)?emailcc:@"" forKey:@"emailcc"];
            
            
            // Add image1 data
            NSString *image1 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED1"]];
            if ([image1 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached1"];
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED1"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached1"];
            }
            
            // Add image2 data
            NSString *image2 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED2"]];
            if ([image2 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached2"];
                
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED2"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached2"];
            }
            
            // Add image3 data
            NSString *image3 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED3"]];
            if ([image3 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached3"];
                
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED3"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached3"];
                
            }
            
            // Add image4 data
            NSString *image4 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED4"]];
            if ([image4 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached4"];
            }
            else{
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED4"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached4"];
            }
            
            // Add image5 data
            NSString *image5 = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"IMAGE_ATTACHED5"]];
            
            if ([image5 isEqualToString:@""]) {
                [dictStockValue setObject:@"" forKey:@"imageAttached5"];
                
            }
            else{
                
                NSData *imagedata = [dictData objectForKey:@"IMAGE_ATTACHED5"];
                [imageAttachedArr addObject:imagedata];
                [dictStockValue setObject:[imagedata base64Encoding] forKey:@"imageAttached5"];
                
            }
            
            dictData = nil;
        }
        
        [res close];
    }];
    
}



-(void)CallServiceForStockPickupEmail{

    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString *strRequest=[AppDelegate getServiceURL:@"webservices_rcf/stockpickupform.php?status=0"];
    
    NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
    if (dictStockValue.count > 0) {
        
        NSLog(@"dictStockValue %@",dictStockValue);
        
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        
        [request setPostValue:[dictStockValue objectForKey:@"batchNo"] forKey:@"batchNo"];
        [request setPostValue:[dictStockValue objectForKey:@"BATCH_NAME"] forKey:@"batchName"];
        
        [request setPostValue:[dictStockValue objectForKey:@"bestBeforedate"] forKey:@"bestBeforedate"];
        [request setPostValue:[dictStockValue objectForKey:@"comment"]forKey:@"comment"];
        [request setPostValue:[dictStockValue objectForKey:@"contactAccountNo"] forKey:@"contactAccountNo"];
        [request setPostValue:[dictStockValue objectForKey:@"contactPhone"] forKey:@"contactPhone"];
        [request setPostValue:[dictStockValue objectForKey:@"customerId"] forKey:@"customerId"];
        
        [request setPostValue:[dictStockValue objectForKey:@"customerName"] forKey:@"customerName"];
        [request setPostValue:[dictStockValue objectForKey:@"dateRaised"] forKey:@"dateRaised"];
        [request setPostValue:[dictStockValue objectForKey:@"emailcc"] forKey:@"emailcc"];
        [request setPostValue:[dictStockValue objectForKey:@"emailto"] forKey:@"emailto"];
        [request setPostValue:[dictStockValue objectForKey:@"pickupNo"] forKey:@"pickupNo"];
        
        [request setPostValue:[dictStockValue objectForKey:@"productCode"] forKey:@"productCode"];
        
        [request setPostValue:[dictStockValue objectForKey:@"productDesc"] forKey:@"productDesc"];
        [request setPostValue:[dictStockValue objectForKey:@"reason"] forKey:@"reason"];
        [request setPostValue:[dictStockValue objectForKey:@"relatedInvoiceNo"] forKey:@"relatedInvoiceNo"];
        [request setPostValue:[dictStockValue objectForKey:@"requestedBy"] forKey:@"requestedBy"];
        [request setPostValue:[dictStockValue objectForKey:@"return"] forKey:@"return"];
        [request setPostValue:[dictStockValue objectForKey:@"quantity"] forKey:@"quantity"];
        
        
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached1"] forKey:@"imageAttached1"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached2"] forKey:@"imageAttached2"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached3"] forKey:@"imageAttached3"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached4"] forKey:@"imageAttached4"];
        [request setPostValue:[dictStockValue objectForKey:@"imageAttached5"] forKey:@"imageAttached5"];
        
        
        //            request.userInfo = dictStockValue;
        //            request.tag = selectedIndexPath.row;
        ////            dictInfo = nil;
        //            NSLog(@"selectedIndexPath.row %d",selectedIndexPath.row);
        strWebserviceType = @"StockPickUpForm";
       [request startAsynchronous];

        
    }
    

    //        soapXMLStr = nil;
    request = nil;
    [dictStockValue removeAllObjects];
    
}


#pragma mark : ASIHTTPRESPONSE

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [spinner hide:YES];
        [spinner removeFromSuperview];
    });
    
    
    
    NSString *responseString = [request responseString];
    
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLString:responseString error:nil];
    
    [self updateEmailStockPickupSend:strPickupNo];

    
    NSLog(@"dictResponse %@",dictResponse);
    
}
@end
