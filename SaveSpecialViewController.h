//
//  SaveSpecialViewController.h
//  Blayney
//
//  Created by POOJA MISHRA on 27/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaveSpecialViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *saveSpecialBtn;
@property (weak, nonatomic) IBOutlet UITextField *priceTxt;
@property (weak, nonatomic) IBOutlet UILabel *resultvalue;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentmargin_absolute;


@property(strong,nonatomic)NSString  *selectType;

@property(strong, nonatomic)NSMutableDictionary *productDetailDict;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;


- (IBAction)selectOptionAction:(id)sender;
- (IBAction)saveSpecialAction:(id)sender;



@end
