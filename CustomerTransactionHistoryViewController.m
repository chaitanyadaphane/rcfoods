//
//  CustomerTransactionViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CustomerTransactionHistoryViewController.h"
#import "CustomCellGeneral.h"
#import "CustomerTransactionDetailViewController.h"
#import "CustomerHistoryDetailViewController.h"

#define CUSTOMER_TRANSACTION_HISTORY_WS @"custrans/view_saleshistoryenquiry.php?"
#define CUSTOMER_PAYMENT_HISTORY_WS @"custhistory/view_payment.php?"

@interface CustomerTransactionHistoryViewController ()

@end

@implementation CustomerTransactionHistoryViewController

@synthesize arrHeaderLabels,strTranCode,dictCustomerDetails,arrCustomerTransactions,isFromDebtorHistory;
@synthesize tblHeader,tblDetails,vwContentSuperview,vwSegmentedControl,vwHeader,vwDetails,vwNoTransactions,lblHeader,spinner,vwNoPayment,vwNoSalesHistory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dictCustomerDetails = [[NSMutableDictionary alloc] init];
    arrCustomerTransactions = [[NSMutableArray alloc] init];
    
    NSString *path;
    
    if (!backgroundQueueForViewCustomerTransactionHistory) {
        backgroundQueueForViewCustomerTransactionHistory = dispatch_queue_create("com.nanan.myscmipad.bgqueueForViewCustomerTransactionHistory", NULL);
        
    }
    
    
    [vwContentSuperview removeAllSubviews];
    
    // Path to the plist (in the application bundle)
    if(isFromDebtorHistory){
        
        lblHeader.text = @"Customer Payment Info";
        
        //Header view by default
        [vwContentSuperview addSubview:vwDetails];

        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionHistoryPaymentHeader" Type:@"plist"];
        
        // Build the array from the plist
        arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
            [self callWSViewCustomerPaymentHistory];
        }
        else{
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            dispatch_async(backgroundQueueForViewCustomerTransactionHistory, ^(void) {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callViewCustomerPaymentHistoryFromDB:db];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [spinner removeFromSuperview];
                    [tblDetails reloadData];
                });
                
            });
            
        }
        
    }
    else{
        lblHeader.text = @"Sales History";
        
        //Segmented Control
        CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
        segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
         segControl.tintColor = [UIColor purpleColor];
        [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
        [vwSegmentedControl addSubview:segControl];
        
        path = [AppDelegate getFileFromLocalDirectory:@"CustomerTransactionHistoryHeader" Type:@"plist"];
        
        // Build the array from the plist
        arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
        
        //[spinner showLoadingView:self.view size:2];
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected == YES) {
            [self callWSViewCustomerTransactionHistory];
        }
        else{
            NSLog(@"localDB");
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            dispatch_async(backgroundQueueForViewCustomerTransactionHistory, ^(void) {
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callViewCustomerTransactionHistoryFromDB:db];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [spinner removeFromSuperview];
                });
                
            });
            
        }
        
    }
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.vwContentSuperview = nil;
    self.vwSegmentedControl = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoTransactions = nil;
    self.vwNoPayment = nil;
    self.vwNoSalesHistory = nil;
    self.lblHeader = nil;
    
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
            [tblHeader reloadData];
        }
            break;
            
        case 1:{
            
            if ([arrCustomerTransactions count]) {
                [vwContentSuperview addSubview:vwDetails];
                [tblDetails reloadData];
            }
            else{
                //No Transactions
                [vwContentSuperview addSubview:vwNoTransactions];
                
            }
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (tableView.tag) {
        case TAG_50:return 60;break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:{
            
            if (isFromDebtorHistory) {
                return 1;
            }
            else{
                return [arrHeaderLabels count];
            }
            
        }break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:{
            
            if (isFromDebtorHistory) {
                return [arrHeaderLabels count];
            }
            else{
                return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];
            }
            
        }break;
            
        case TAG_100:return [arrCustomerTransactions count];break;
            
        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier9 = CELL_IDENTIFIER9;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
    switch (tableView.tag) {
        case TAG_50:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                cell = [nib objectAtIndex:4];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            if (isFromDebtorHistory) {
                
                cell.lblTitle.text = [[arrHeaderLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];
                
                if ([indexPath section] == 0) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([[dictCustomerDetails objectForKey:@"Debtor"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Debtor"]objectForKey:@"text"]];
                            }
                            else     if ([dictCustomerDetails objectForKey:@"Debtor"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Debtor"]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                        default:break;
                    }
                }
                
                if ([indexPath section] == 1) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([[dictCustomerDetails objectForKey:@"TranNo"] objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"TranNo"] objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"TranNo"] ) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"TranNo"] ];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            
                        default:break;
                    }
                }
                
                
                if ([indexPath section] == 2) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([[dictCustomerDetails objectForKey:@"Name"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Name"]objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"Name"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Name"]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            
                            break;
                        case 1:
                            if ([dictCustomerDetails objectForKey:@"Type"])
                            {
                                NSString *strType = @"";
                                
                                // Invoice
                                if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DI"])
                                {
                                    strType = @"Invoice";
                                }
                                else   if([[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DI"])
                                {
                                    strType = @"Invoice";
                                }
                                
                                // credit Note
                                else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DC"])
                                {
                                    strType = @"Credit note";
                                }
                                else if([[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DC"])
                                {
                                    strType = @"Credit note";
                                }
                                
                                // Receipt
                                else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DR"])
                                    
                                {
                                    strType = @"Receipt";
                                }
                                else if([[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DR"])
                                {
                                    strType = @"Receipt";
                                }
                                
                                // Debit Journal
                                else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DD"])
                                {
                                    strType = @"Debit Journal";
                                }
                                else if([[[dictCustomerDetails objectForKey:@"Type"]objectForKey:@"text"] isEqualToString:@"DD"])
                                {
                                    strType = @"Debit Journal";
                                }
                                
                                // credit Journal
                                else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DJ"])
                                {
                                    strType = @"Credit Journal";
                                }
                                else if([[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DJ"])
                                    
                                {
                                    strType = @"Credit Journal";
                                }
                                else
                                {
                                    strType = @"";
                                }
                                
                                cell.lblValue.text = strType;
                            }
                            break;
                        case 2:
                            
                            if ([[dictCustomerDetails objectForKey:@"OrderNo"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"OrderNo"] objectForKey:@"text"]];
                            }
                            else     if ([dictCustomerDetails objectForKey:@"OrderNo"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"OrderNo"]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                        case 3:
                            
                            if ([[dictCustomerDetails objectForKey:@"DelRun"]objectForKey:@"text"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"DelRun"]objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"DelRun"])
                            {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"DelRun"]];
                            }
                            else
                            {
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                            
                        case 4:
                            
                            if ([dictCustomerDetails objectForKey:@"Raised"] && ![[[dictCustomerDetails objectForKey:@"Raised"]objectForKey:@"text"] isEqualToString:STANDARD_SERVER_DATE]) {
                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[[dictCustomerDetails objectForKey:@"Raised"]objectForKey:@"text"]]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"Raised"]  && ![[dictCustomerDetails objectForKey:@"Raised"] isEqualToString:STANDARD_SERVER_DATE]) {
                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictCustomerDetails objectForKey:@"Raised"]]];
                            }
                            
                            break;
                            
                            
                            
                        case 5:
                            
                            if ([[dictCustomerDetails objectForKey:@"CustOrder"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"CustOrder"]objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"CustOrder"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"CustOrder"]];
                            }
                            break;
                            
                            
                        case 6:
                            
                            if ([[dictCustomerDetails objectForKey:@"TotalDiscount"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[dictCustomerDetails objectForKey:@"TotalDiscount"]objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"TotalDiscount"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[dictCustomerDetails objectForKey:@"TotalDiscount"]];
                            }
                            break;
                            
                            
                        case 7:
                            if ([[dictCustomerDetails objectForKey:@"Tax"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[dictCustomerDetails objectForKey:@"Tax"]objectForKey:@"text"]];
                            }
                            else if ([dictCustomerDetails objectForKey:@"Tax"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[dictCustomerDetails objectForKey:@"Tax"]];
                            }
                            break;
                            
                        case 8:
                            
                            if ([[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"]floatValue]]];
//                                [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"]floatValue]];
                                return cell;
                            }
                            else if ([dictCustomerDetails objectForKey:@"Total"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[dictCustomerDetails objectForKey:@"Total"]floatValue]]];

//                                cell.lblValue.text = [NSString stringWithFormat:@"$%@",[dictCustomerDetails objectForKey:@"Total"]];
//                                [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
                                return cell;
                            }
                            
                            break;
                            
                            
                        default:break;
                    }
                }
                
                
                
                return cell;
                
            }
            else{
                
                cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
                
                
                if ([indexPath section] == 0) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([dictCustomerDetails objectForKey:@"Debtor"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Debtor"]objectForKey:@"text"]];
                            }
                        default:break;
                    }
                }
                
                if ([indexPath section] == 1) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([dictCustomerDetails objectForKey:@"TranNo"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"TranNo"] objectForKey:@"text"]];
                            }
                        default:break;
                    }
                }
                
                
                if ([indexPath section] == 2) {
                    switch ([indexPath row]) {
                        case 0:
                            if ([dictCustomerDetails objectForKey:@"Name"]) {
                               if([[dictCustomerDetails objectForKey:@"Name"] isKindOfClass:[NSString class]])
                               {
                                 cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"Name"] trimSpaces]];
                               }
                              else if([[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"])
                              {
                                 cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerDetails objectForKey:@"Name"]objectForKey:@"text"] trimSpaces]];
                              }
                              
                            }
                            
                            break;
                        case 1:
                        @try {
                          if ([dictCustomerDetails objectForKey:@"Type"])
                          {
                            if ([dictCustomerDetails objectForKey:@"Type"])
                            {
                              NSString *strType = @"";
                                
                                // Invoice
                              if([[dictCustomerDetails objectForKey:@"Type"] isKindOfClass:[NSString class]])
                              {
                                if([[dictCustomerDetails objectForKey:@"Type"]  isEqualToString:@"DI"])
                                  strType = @"Invoice";
                              }
                              else if([[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"])
                              {
                                if([[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"] isEqualToString:@"DI"])
                                  strType = @"Invoice";
                              }
                              else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DI"])
                              {
                                strType = @"Invoice";
                              }
                                
                                //Credit Notes
                              else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DC"])
                              {
                                strType = @"Credit note";
                              }
                              else if([[[dictCustomerDetails objectForKey:@"Type"]objectForKey:@"text" ] isEqualToString:@"DC"])
                              {
                                  strType = @"Credit note";
                              }
                                // Receipt
                              else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DR"])
                              {
                                strType = @"Receipt";
                              }
                              else if([[[dictCustomerDetails objectForKey:@"Type"]objectForKey:@"text"] isEqualToString:@"DR"])
                              {
                                  strType = @"Receipt";
                              }
                                
                                //Debit Journal
                              else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DD"])
                              {
                                strType = @"Debit Journal";
                              }
                              else if([[[dictCustomerDetails objectForKey:@"Type"]objectForKey:@"text"] isEqualToString:@"DD"])
                              {
                                  strType = @"Debit Journal";
                              }
                                
                                // Credit Journal
                              else if([[dictCustomerDetails objectForKey:@"Type"] isEqualToString:@"DJ"])
                              {
                                strType = @"Credit Journal";
                              }
                              else if([[[dictCustomerDetails objectForKey:@"Type"]objectForKey:@"text"] isEqualToString:@"DJ"])
                              {
                                  strType = @"Credit Journal";
                              }
                              else
                              {
                                strType = @"";
                              }
                              
                              cell.lblValue.text = strType;
                              //cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerDetails objectForKey:@"Type"]];
                            }
                          }
                        }
                        @catch (NSException *exception) {
                          NSLog(@"eror::%@",exception.description);
                        }
                        
                            break;
                        case 2:
                            
                            if ([[dictCustomerDetails objectForKey:@"OrderNo"]objectForKey:@"text"]) {
                                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"OrderNo"]objectForKey:@"text"]];
                                }
                                else{
                                    cell.lblValue.text = @"--";
                                }
                            break;
                            
                        case 3:
                            
                            if ([[dictCustomerDetails objectForKey:@"DelRun"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"DelRun"]objectForKey:@"text" ]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                            
                        case 4:
//                            [[dictCustomerDetails objectForKey:@"Raised"] isEqualToString:STANDARD_APP_DATE]
                            if ([dictCustomerDetails objectForKey:@"Raised"] && ![[[dictCustomerDetails objectForKey:@"Raised"]objectForKey:@"text"] isEqualToString:STANDARD_SERVER_DATE]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[[dictCustomerDetails objectForKey:@"Raised"] objectForKey:@"text"]]];
                            }
                            break;
                            
                            
                            
                        case 5:
                            
                            if ([[dictCustomerDetails objectForKey:@"CustOrder"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerDetails objectForKey:@"CustOrder"]objectForKey:@"text"]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                            
                            
                        case 6:
                            
                            if ([[dictCustomerDetails objectForKey:@"TotalDiscount"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[dictCustomerDetails objectForKey:@"TotalDiscount"]objectForKey:@"text"] floatValue]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                        case 7:
                            
                            if ([[dictCustomerDetails objectForKey:@"Tax"]objectForKey:@"text"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[dictCustomerDetails objectForKey:@"Tax"]objectForKey:@"text"] floatValue]];
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                        case 8:
                            
                            if ([[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"]) {
//                                cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"] floatValue]];
//                                [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:[[[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"] floatValue]];

                                
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormatNew:[[[dictCustomerDetails objectForKey:@"Total"]objectForKey:@"text"] floatValue]]];

                                
                                return cell;
                            }
                            else{
                                cell.lblValue.text = @"--";
                            }
                            break;
                            
                            
                        default:break;
                    }
                }
                
                //}
                
                return cell;
            }
            
        }break;
            
        case TAG_100:{
            
            if (isFromDebtorHistory) {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    cell = [nib objectAtIndex:5];
                }
                
                cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Invoice"]];
                
                cell.lblValue.text = @"";
            }
            else{
                if (tableView.editing) {
                    
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier9];
                    
                    if (cell == nil)
                    {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        
                        cell = [nib objectAtIndex:8];
                    }
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"]objectForKey:@"text"]];
                    
                    cell.lblValue.text = @"";
                    
                    
                    
                }
                else{
                    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
                    
                    if (cell == nil)
                    {
                        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                        
                        cell = [nib objectAtIndex:5];
                    }
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"]objectForKey:@"text"]];
                    
                    cell.lblValue.text = @"";
                    
                }
                
            }
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            return cell;
            
        }break;
            
        default:return nil;break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case TAG_100:{
            
            if (isFromDebtorHistory) {
                CustomerHistoryDetailViewController *dataViewController = [[CustomerHistoryDetailViewController alloc] initWithNibName:@"CustomerHistoryDetailViewController" bundle:[NSBundle mainBundle]];
                dataViewController.dictCustomerHistoryDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
                
                if ([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]objectForKey:@"text"])
                {
                    dataViewController.strTranCode = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]objectForKey:@"text"]];
                }
                else{
                    dataViewController.strTranCode = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]];
                }
                
                dataViewController.isFromCustomerHistory = NO;
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            }
            
            else{
                CustomerTransactionDetailViewController *dataViewController = [[CustomerTransactionDetailViewController alloc] initWithNibName:@"CustomerTransactionDetailViewController" bundle:[NSBundle mainBundle]];
                dataViewController.dictTransactionDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
                
                
                if ([[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]objectForKey:@"text"])
                {
                    dataViewController.strTranCode = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]objectForKey:@"text"]];
                }
                else{
                    dataViewController.strTranCode = [NSString stringWithFormat:@"%@",[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"TranNo"]];
                }
                
                dataViewController.isFromTransactionHistory = TRUE;
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            }
            
        }break;
            
        default:break;
    }
    
}

#pragma mark - Database calls
-(void)callViewCustomerTransactionHistoryFromDB:(FMDatabase *)db{
    
    strWebserviceType = @"DB_GET_CUSTOMER_TRANSACTION_HISTORY";
    FMResultSet *rs1;
    @try {
        
        //NSLog(@"Database query : SELECT DEBTOR as Debtor, DEL_NAME as Name, TRAN_NO as TranNo, TYPE as Type, ORDER_NO as OrderNo, DELIVERY_RUN as DelRun, DATE_RAISED as Raised, CUST_ORDER as CustOrder, TAX as Tax, DISCOUNT as TotalDiscount FROM arhishea WHERE TRAN_NO = %@",strTranCode);
        
        rs1 = [db executeQuery:@"SELECT DEBTOR as Debtor, DEL_NAME as Name, TRAN_NO as TranNo, TYPE as Type, ORDER_NO as OrderNo, DELIVERY_RUN as DelRun, DATE_RAISED as Raised, CUST_ORDER as CustOrder, TAX as Tax, DISCOUNT as TotalDiscount FROM arhishea WHERE trim(TRAN_NO) = ? ",[strTranCode trimSpaces]];
        
        if (!rs1)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            self.dictCustomerDetails = (NSMutableDictionary *)[rs1 resultDictionary];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwHeader];
                [tblHeader reloadData];
            });
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                vwSegmentedControl.hidden = YES;
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwNoSalesHistory];
            });
            
        }
        
        [rs1 close];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
        
    }
    
    FMResultSet *rs2;
    @try {
        
        [arrCustomerTransactions removeAllObjects];
        
        rs2 = [db executeQuery:@"SELECT TRAN_NO as TranNo, DESCRIPTION as Description, QUANTITY as Quantity, PRICE as Price, DISCOUNT as Discount, EXTENSION as Extension FROM arhisdet WHERE trim(TRAN_NO) = ? ",[strTranCode trimSpaces]];
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
         float total = 0;
        while([rs2 next]) {
            
            NSMutableDictionary *dictData = (NSMutableDictionary *)[rs2 resultDictionary];
            total += [[dictData objectForKey:@"Extension"] floatValue];
            [arrCustomerTransactions addObject:dictData];
            
        }
        
        NSString *strTotal = [NSString stringWithFormat:@"%f",total];
        [dictCustomerDetails setValue:strTotal forKey:@"Total"];

        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
}

-(void)callViewCustomerPaymentHistoryFromDB:(FMDatabase *)db{
    //open the database
    strWebserviceType = @"DB_GET_CUSTOMER_PAYMENT_HISTORY";
    
    FMResultSet *rs;
    
    @try {
        [arrCustomerTransactions removeAllObjects];
        
        //Get Customer list
        rs = [db executeQuery:@"SELECT DEBTOR as Debtor, DESCRIPTION as Description, TRAN_NO as TranNo, DATE_PAID as DatePaid, INVOICE_NO as Invoice, TYPE as Type, REFERENCE as Reference, INVOICE_TOTAL as Total, PAYMENT_TOTAL as Payment from arpaymnt where TRAN_NO= ? ORDER BY DATE_PAID DESC",strTranCode];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            [arrCustomerTransactions addObject:dictData];
        }
        
        [rs close];
        
        //NSLog(@"arrCustomerTransactions %@",arrCustomerTransactions);
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
    }
}

#pragma mark - WS Methods
-(void)callWSViewCustomerTransactionHistory{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    strWebserviceType = @"WS_GET_CUSTOMER_TRANSACTION_HISTORY";
    NSString *parameters = [NSString stringWithFormat:@"tranNo=%@",strTranCode];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_TRANSACTION_HISTORY_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewCustomerPaymentHistory{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_CUSTOMER_PAYMENT_HISTORY";
    NSString *parameters = [NSString stringWithFormat:@"tranNo=%@",strTranCode];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_PAYMENT_HISTORY_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}


#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"dictResponse %@", dictResponse);
    
    if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_TRANSACTION_HISTORY"]){
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"SalesHistoryEnquiryList"] objectForKey:@"SalesHistory"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"SalesHistoryEnquiryList"] objectForKey:@"SalesHistory"];
                
                [arrCustomerTransactions addObject:dict];
            }
            else{
                self.arrCustomerTransactions = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"SalesHistoryEnquiryList"] objectForKey:@"SalesHistory"];
            }
            
            self.dictCustomerDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"Data"];
            
            
            dispatch_async(backgroundQueueForViewCustomerTransactionHistory, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]) {
                            @throw [NSException exceptionWithName:@"Response Issue" reason:@"Recnum can't be Null" userInfo:nil];
                        }
                        
                        
                        FMResultSet *rs = [db executeQuery:@"SELECT Recnum FROM arhishea WHERE Recnum = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        
                        if (!rs) {
                            [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        NSString *strOrderNum = @"";
                        if ([[dictCustomerDetails objectForKey:@"OrderNo"] objectForKey:@"text"]) {
                            strOrderNum = [[dictCustomerDetails objectForKey:@"OrderNo"] objectForKey:@"text"];
                        }
                        
                        NSString *strDeliveryRun = @"";
                        if ([[dictCustomerDetails objectForKey:@"DelRun"] objectForKey:@"text"]) {
                            strDeliveryRun = [[dictCustomerDetails objectForKey:@"DelRun"] objectForKey:@"text"];
                        }
                        
                        BOOL y;
                        if ([rs next]) {
                            
                            NSString *strCustOrder = @"";
                            
                            if([[dictCustomerDetails objectForKey:@"CustOrder"] objectForKey:@"text"])
                            {
                                strCustOrder = [[dictCustomerDetails objectForKey:@"CustOrder"] objectForKey:@"text"];
                            }
                            else
                            {
                                strCustOrder = @"";
                            }
                            
                            y = [db executeUpdate:@"UPDATE `arhishea` SET  `RECNUM`= ?, `CUST_ORDER`= ?, `DEBTOR`= ?,`DELIVERY_RUN`= ?, `DEL_NAME`= ?,`ORDER_NO`= ?,`DATE_RAISED`= ?,`TAX`= ?,`DISCOUNT`= ?,`TRAN_NO`= ?,`TYPE`= ? WHERE RECNUM = ?",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"], strCustOrder,[[dictCustomerDetails objectForKey:@"Debtor"] objectForKey:@"text"], strDeliveryRun, [[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"],strOrderNum,[[dictCustomerDetails objectForKey:@"Raised"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Tax"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"TotalDiscount"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"TranNo"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                            
                        }
                        else{
                            
                            NSString *strCustOrder = @"";
                            
                            if([[dictCustomerDetails objectForKey:@"CustOrder"] objectForKey:@"text"])
                            {
                                strCustOrder = [[dictCustomerDetails objectForKey:@"CustOrder"] objectForKey:@"text"];
                            }
                            else
                            {
                                strCustOrder = @"";                                
                            }
                        
//                            y = [db executeUpdate:@"INSERT INTO `arhishea` ( `RECNUM`, `CUST_ORDER`, `DEBTOR`,`DELIVERY_RUN`, `DEL_NAME`,`ORDER_NO`,`DATE_RAISED`,`TAX`,`DISCOUNT`,`TRAN_NO`,`TYPE`) VALUES (  ?, ?, ?, ?, ?, ?,?, ?, ?,?, ?)",[[dictCustomerDetails objectForKey:@"Recnum"] objectForKey:@"text"],strCustOrder,[[dictCustomerDetails objectForKey:@"Debtor"] objectForKey:@"text"], strDeliveryRun, [[dictCustomerDetails objectForKey:@"Name"] objectForKey:@"text"],strOrderNum,[[dictCustomerDetails objectForKey:@"Raised"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Tax"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"TotalDiscount"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"TranNo"] objectForKey:@"text"],[[dictCustomerDetails objectForKey:@"Type"] objectForKey:@"text"]];
                            
                        }
                        
                        [rs close];
                        
                        if (!y)
                        {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        for (NSDictionary *dict in arrCustomerTransactions){
                            
                            NSString *strScmRecnum = [[dict objectForKey:@"Recnum"] objectForKey:@"text"];
                            if (![[dict objectForKey:@"Recnum"] objectForKey:@"text"]) {
                                //@throw [NSException exceptionWithName:@"Response Issue" reason:@"ScmRecnum can't be Null" userInfo:nil];
                            }
                            
                            FMResultSet *rs = [db executeQuery:@"SELECT RECNUM FROM arhisdet WHERE RECNUM = ?",strScmRecnum];
                            
                            if (!rs) {
                                [rs close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            BOOL y;
                          NSString *strDesc = @"";
                          if([[dict objectForKey:@"Description"] objectForKey:@"text"])
                          {
                              strDesc = [[dict objectForKey:@"Description"] objectForKey:@"text"];
                          }
                          
                            if ([rs next]) {
//                                y = [db executeUpdate:@"UPDATE `arhisdet` SET `SCM_RECNUM` = ?, `RECNUM` = ?, `DESCRIPTION` = ?, `DISCOUNT` = ?, `EXTENSION` = ?, `PRICE` = ?,`QUANTITY` = ?, `TRAN_NO` = ?,`created_date` = ?, `modified_date` = ? WHERE SCM_RECNUM = ?", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], [[dict objectForKey:@"Description"] objectForKey:@"text"],[[dict objectForKey:@"Discount"] objectForKey:@"text"], [[dict objectForKey:@"Extension"] objectForKey:@"text"], [[dict objectForKey:@"Price"] objectForKey:@"text"],[[dict objectForKey:@"Quantity"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"],[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"],strScmRecnum];
//                            }
                              y = [db executeUpdate:@"UPDATE `arhisdet` SET  `RECNUM` = ?, `DESCRIPTION` = ?, `DISCOUNT` = ?, `EXTENSION` = ?, `PRICE` = ?,`QUANTITY` = ?, `TRAN_NO` = ? WHERE RECNUM = ?", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], strDesc,[[dict objectForKey:@"Discount"] objectForKey:@"text"], [[dict objectForKey:@"Extension"] objectForKey:@"text"], [[dict objectForKey:@"Price"] objectForKey:@"text"],[[dict objectForKey:@"Quantity"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"],strScmRecnum];
                              
                            }
                              else{
//                                y = [db executeUpdate:@"INSERT INTO `arhisdet` (`SCM_RECNUM`, `RECNUM`, `DESCRIPTION`, `DISCOUNT`, `EXTENSION`, `PRICE`,`QUANTITY`, `TRAN_NO`,`created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], [[dict objectForKey:@"Description"] objectForKey:@"text"],[[dict objectForKey:@"Discount"] objectForKey:@"text"], [[dict objectForKey:@"Extension"] objectForKey:@"text"], [[dict objectForKey:@"Price"] objectForKey:@"text"],[[dict objectForKey:@"Quantity"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"],[[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
//                                y = [db executeUpdate:@"INSERT INTO `arhisdet` ( `RECNUM`, `DESCRIPTION`, `DISCOUNT`, `EXTENSION`, `PRICE`,`QUANTITY`, `TRAN_NO`) VALUES (?, ?, ?, ?, ?, ?, ?)", strScmRecnum, strDesc,[[dict objectForKey:@"Discount"] objectForKey:@"text"], [[dict objectForKey:@"Extension"] objectForKey:@"text"], [[dict objectForKey:@"Price"] objectForKey:@"text"],[[dict objectForKey:@"Quantity"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"]];

                              }
                            
                            
                            [rs close];
//                            if (!y)
//                            {
//                                
//                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//                            }
                            
                            
                        }
                        
                        *rollback = NO;
                        //5thjune
//                        [self callViewCustomerTransactionHistoryFromDB:db];
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [spinnerObj removeFromSuperview];
                            [vwContentSuperview removeAllSubviews];
                            [vwContentSuperview addSubview:vwHeader];
                            [tblHeader reloadData];
                        });
                        
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        self.dictCustomerDetails = nil;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [spinnerObj removeFromSuperview];
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                    }
                    
                }];
                
            });
            
        }
        //False
        else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [spinnerObj removeFromSuperview];
                vwSegmentedControl.hidden = YES;
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwNoSalesHistory];
                
                /*
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"SalesHistoryEnquiry"] objectForKey:@"Message"] objectForKey:@"text"];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                 */
            });
            
        }
    }
    else if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_PAYMENT_HISTORY"]){
        
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"PaymentDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            [arrCustomerTransactions removeAllObjects];
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"PaymentDetails"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"PaymentDetails"] objectForKey:@"Data"];
                
                [arrCustomerTransactions addObject:dict];
            }
            else{
                self.arrCustomerTransactions = [[[dictResponse objectForKey:@"Response"] objectForKey:@"PaymentDetails"] objectForKey:@"Data"];
            }
            
            
            dispatch_async(backgroundQueueForViewCustomerTransactionHistory, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        for (NSDictionary *dict in arrCustomerTransactions){
                            
                            NSString *strScmRecnum = [[dict objectForKey:@"Recnum"] objectForKey:@"text"];
                            
                            if (![[dict objectForKey:@"Recnum"] objectForKey:@"text"]) {
                                @throw [NSException exceptionWithName:@"Response Issue" reason:@"Recnum can't be Null" userInfo:nil];
                            }
                            
                            FMResultSet *rs = [db executeQuery:@"SELECT Recnum FROM arpaymnt WHERE Recnum = ?",strScmRecnum];
                            
                            if (!rs) {
                                [rs close];
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            BOOL y;
                            if ([rs next]) {
                                y = [db executeUpdate:@"UPDATE `arpaymnt` SET  `RECNUM` = ?, `INVOICE_TOTAL` = ?, `DATE_PAID` = ?, `DEBTOR` = ?,`DESCRIPTION` = ?, `INVOICE_NO` = ?, `PAYMENT_TOTAL` = ?, `REFERENCE` = ?, `TRAN_NO` = ?,`TYPE` = ? WHERE SCM_RECNUM = ?", strScmRecnum, [[dict objectForKey:@"Total"] objectForKey:@"text"],[[dict objectForKey:@"DatePaid"] objectForKey:@"text"], [[dict objectForKey:@"Debtor"] objectForKey:@"text"], [[dict objectForKey:@"Description"] objectForKey:@"text"],[[dict objectForKey:@"Invoice"] objectForKey:@"text"],[[dict objectForKey:@"Payment"] objectForKey:@"text"],[[dict objectForKey:@"Reference"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"],[[dict objectForKey:@"Type"] objectForKey:@"text"],strScmRecnum];
                            }
                            else{
                                y = [db executeUpdate:@"INSERT INTO `arpaymnt` (`RECNUM`, `INVOICE_TOTAL`, `DATE_PAID`, `DEBTOR`,`DESCRIPTION`, `INVOICE_NO`, `PAYMENT_TOTAL`, `REFERENCE`, `TRAN_NO`,`TYPE`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", strScmRecnum,[[dict objectForKey:@"Recnum"] objectForKey:@"text"], [[dict objectForKey:@"Total"] objectForKey:@"text"],[[dict objectForKey:@"DatePaid"] objectForKey:@"text"], [[dict objectForKey:@"Debtor"] objectForKey:@"text"], [[dict objectForKey:@"Description"] objectForKey:@"text"],[[dict objectForKey:@"Invoice"] objectForKey:@"text"],[[dict objectForKey:@"Payment"] objectForKey:@"text"],[[dict objectForKey:@"Reference"] objectForKey:@"text"],[[dict objectForKey:@"TranNo"] objectForKey:@"text"],[[dict objectForKey:@"Type"] objectForKey:@"text"]];
                            }
                            
                            [rs close];
                            if (!y)
                            {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            
                        }
                        
                        *rollback = NO;
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                    }
                    
                    @finally {
//                        [self callViewCustomerPaymentHistoryFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [spinnerObj removeFromSuperview];
                            
                            [vwContentSuperview removeAllSubviews];
                            if([arrCustomerTransactions count]){
                                vwSegmentedControl.hidden = NO;
                                [vwContentSuperview addSubview:vwDetails];
                                [tblDetails reloadData];
                            }
                            else{
                                vwSegmentedControl.hidden = YES;
                                [vwContentSuperview addSubview:vwNoPayment];
                            }
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                    });
                    
                }];
                
            });
            
        }
        //False
        else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [spinnerObj removeFromSuperview];
                vwSegmentedControl.hidden = YES;
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwNoPayment];
                /*NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"PaymentDetails"] objectForKey:@"Message"] objectForKey:@"text"];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];*/
                
            });
        }
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}


@end
