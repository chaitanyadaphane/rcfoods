//
//  AddQuoteViewController.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "AddQuoteViewController.h"
#import "CustomCellGeneral.h"
#import "QuoteHistoryCustomerListViewController.h"
#import "QuoteHistoryCustomerListEditableMode.h"
#import "KGModal.h"
#import "StockCodeViewController.h"
#import "CopyProfileViewController.h"

#define TAG_50 50
#define TAG_100 100
#define TAG_200 200
#define TAG_300 300

#define COMMENT_LABEL_WIDTH 189
#define COMMENT_LABEL_MIN_HEIGHT 34
#define COMMENT_LABEL_PADDING 10

#define QUOTE_SAVE_HEADER_WS @"quote/adddebtordetails_quote.php?"
#define QUOTE_DETAILS_HEADER_WS @"quote/view_quotehistoryheader.php?"
#define QUOTE_DETAILS_DETAILS_WS @"quote/view_quotehistorydetails.php?"

@interface AddQuoteViewController ()
{
    NSString *strExpiry_Date;
    
    StockCodeViewController *viewCnt;
    UIView *contentView;
    NSMutableArray *arrAutoFill;
    NSMutableArray *arrFilteredData;
    UITextField *txtDebtName;
    UITextField *txtDebtID;
    UITextField *txtCustOrder;
    
    NSMutableArray *quoteDebtorCodeStr;
    
    NSString *strCopyProfileDebtorId;
    
    NSString *strExclSelectAll;
    NSString *strExtensionSelectAll;
    NSString *strTaxSelectAll;
    NSString *strSellingPrice;
    NSString *strPriceSelectAll;
    CGFloat floatProfitPercentage;
    BOOL isTaxApplicable;
    BOOL isSelectAll;
    int line_no_selectAll;
    NSMutableDictionary *dictProductDetails;
}

@end

@implementation AddQuoteViewController

NSString *strDeliveryDate;
@synthesize arrHeaderLabels,arrProducts,dictHeaderDetails,strQuoteNum,popoverController,strQuoteNumRecived,EditableMode,deleteIndexPath,deleteRowIndex,btnEmail,btnPrint,lblHeader,finalise,strDebtorNum;
@synthesize arrcopyprofileArr;

@synthesize btnDelete,btnLoadProfile,btnSaveDetails,btnAddProducts,btnFinalise,imgArrowDownwards,imgArrowRightWards,vwSegmentedControl,vwContentSuperview,vwHeader,vwDetails,vwNoProducts,tblHeader,tblDetails,keyboardToolBar,btnAddMenu,progessView,lblPercentageIndicator,isCommentAddedOnLastLine;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Set the Reachability observer
        
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isSelectAll = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardWillChangeFrameNotification:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    self.isCommentAddedOnLastLine = NO;
    
    strQuoteNum = [[NSString alloc] init];
    
    CATransform3D transform = CATransform3DIdentity;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"pointing_down" ofType:@"png"];
    UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    layerPointingImage = [CALayer layer];
    layerPointingImage.contents = (id)downImage.CGImage;
    layerPointingImage.bounds = CGRectMake(0, 0, imgArrowDownwards.frame.size.width, imgArrowDownwards.frame.size.height);
    layerPointingImage.position = CGPointMake(10,30);
    layerPointingImage.transform = CATransform3DTranslate(transform, 0.0, 6,0.0);
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
    segControl.tintColor = [UIColor purpleColor];

    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    chkFinalize = NO;
    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"QuoteHeaderAdd" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    //NSLog(@"%d",[arrHeaderLabels count]);
    arrProducts = [[NSMutableArray alloc] init];
    arrcopyprofileArr = [[NSMutableArray alloc]init];
    
    dictHeaderDetails = [[NSMutableDictionary alloc] init];
    //tempArrForEdit = [[NSMutableArray alloc] init];
    
    //Notification to reload table when debtor is selected from Debtor list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHeader:) name:@"ReloadAddProductHeadersNotification" object:nil];
    
    //Notification to reload table when products added to details from product list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:@"ReloadAddProductDetailsNotification" object:nil];
    
    //Notification to popup when user exits with out saving created quote
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callSaveEditQuoteToDB:) name:@"SAVE_QUOTE_CREATED" object:nil];

    
    backgroundQueueForNewQuote = dispatch_queue_create("com.nanan.myscmipad.bgqueueForNewQuote", NULL);
    
    operationQueue = [NSOperationQueue new];
    
    
    ///--Code commented to generate Quote Number Locally Alway
    if (EditableMode == NO)
    {
        [self setUpInitialView];
        [self callGetNewQuoteNumberFromDB];
    }
    else
    {
        
        lblHeader.text = @"View Quote";
        
        btnLoadProfile.hidden = YES;
        segControl.enabled = YES;
        
        [self setUpInitialView];
        
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [operationQueue addOperationWithBlock:^{
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            
            [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callViewQuoteDetailsHeaderFromDB:db];
                [self callViewQuoteDetailsDetailsFromDB:db];
            }];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                [tblHeader reloadData];
            }];
        }];
        
    }
    
    
    /*
     if (EditableMode == NO) {
     lblHeader.text = @"Add Quote";
     btnLoadProfile.hidden = NO;
     segControl.enabled = NO;
     
     
     if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
     {
     
     //[self.view addSubview:etActivity];
     
     NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
     NSURL *url = [NSURL URLWithString:strUrl];
     
     [operationQueue setMaxConcurrentOperationCount:1];
     
     [operationQueue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
     
     //Quote Header
     DownloadUrlOperation *operation1 = [[DownloadUrlOperation alloc] initWithURL:url];
     operation1.tag = 102;
     operation1.isUploading = YES;
     operation1.isQuotes= YES;
     [operation1 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
     [operationQueue addOperation:operation1];
     
     //Quote Detail
     DownloadUrlOperation *operation2 = [[DownloadUrlOperation alloc] initWithURL:url];
     operation2.tag = 103;
     operation2.isUploading = YES;
     operation2.isQuotes= YES;
     [operation2 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
     [operationQueue addOperation:operation2];
     
     //Offline Quotes Header
     DownloadUrlOperation *operation3 = [[DownloadUrlOperation alloc] initWithURL:url];
     operation3.tag = 104;
     operation3.isUploading = YES;
     operation3.isOfflineQuotes= YES;
     [operation3 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
     [operationQueue addOperation:operation3];
     
     //Offline Quotes Detail
     DownloadUrlOperation *operation4 = [[DownloadUrlOperation alloc] initWithURL:url];
     operation4.tag = 105;
     operation4.isUploading = YES;
     operation4.isOfflineQuotes= YES;
     [operation4 addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
     [operationQueue addOperation:operation4];
     
     
     }
     
     else{
     [self setUpInitialView];
     [self callGetNewQuoteNumberFromDB];
     }
     
     
     }
     else{
     
     lblHeader.text = @"View Quote";
     
     btnLoadProfile.hidden = YES;
     segControl.enabled = YES;
     
     [self setUpInitialView];
     
     spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     [operationQueue addOperationWithBlock:^{
     FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
     
     [databaseQueue inDatabase:^(FMDatabase *db) {
     [self callViewQuoteDetailsHeaderFromDB:db];
     [self callViewQuoteDetailsDetailsFromDB:db];
     }];
     
     [[NSOperationQueue mainQueue] addOperationWithBlock:^{
     spinner.mode = MBProgressHUDModeIndeterminate;
     [spinner removeFromSuperview];
     [tblHeader reloadData];
     }];
     }];
     
     }
     */
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Store the view is profile order.
    [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isQuoteCreated"];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForQuote"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
}




- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.btnDelete = nil;
    self.btnLoadProfile = nil;
    self.btnSaveDetails = nil;
    self.btnAddProducts = nil;
    self.btnFinalise = nil;
    self.imgArrowDownwards = nil;
    self.imgArrowRightWards = nil;
    self.vwSegmentedControl = nil;
    self.vwContentSuperview = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoProducts = nil;
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.keyboardToolBar = nil;
    self.btnAddMenu = nil;
    self.btnEmail = nil;
    self.btnPrint = nil;
    self.progessView = nil;
    self.lblPercentageIndicator = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if (EditableMode == YES) {
        _btnSelectAll.hidden = YES;
    }
    else{
        _btnSelectAll.hidden = NO;
    }
}

- (IBAction)btnCopyProfile:(id)sender
{
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 580, 480)];
    
    UILabel *lblDetails = [[UILabel alloc] initWithFrame:CGRectMake(170, 20, 250, 40)];
    [lblDetails setBackgroundColor:[UIColor clearColor]];
    [lblDetails setText:@"Enter Profile Details"];
    [lblDetails setTextAlignment:NSTextAlignmentCenter];
    [lblDetails setTextColor:[UIColor whiteColor]];
    [lblDetails setFont:[UIFont boldSystemFontOfSize:25.0f]];
    
    txtDebtName = [[UITextField alloc] initWithFrame:CGRectMake(65, 100, 450, 50)];
    [txtDebtName setBackgroundColor:[UIColor whiteColor]];
    txtDebtName.delegate = self;
    txtDebtName.text = @"";
    txtDebtName.tag  = 101;
    [txtDebtName setBorderStyle:UITextBorderStyleRoundedRect];
    [txtDebtName setPlaceholder:@" Debtor Name"];
    
    UILabel *lblOR = [[UILabel alloc] initWithFrame:CGRectMake(270, 160, 50, 40)];
    [lblOR setBackgroundColor:[UIColor clearColor]];
    [lblOR setText:@"OR"];
    [lblOR setTextAlignment:NSTextAlignmentCenter];
    [lblOR setTextColor:[UIColor darkGrayColor]];
    [lblOR setFont:[UIFont boldSystemFontOfSize:25.0f]];
    
    txtDebtID = [[UITextField alloc] initWithFrame:CGRectMake(65, 210, 450, 50)];
    [txtDebtID setBackgroundColor:[UIColor whiteColor]];
    txtDebtID.delegate = self;
    txtDebtID.text = @"";
    txtDebtID.tag  = 102;
    [txtDebtID setBorderStyle:UITextBorderStyleRoundedRect];
    [txtDebtID setPlaceholder:@" Debtor ID"];
    
    UIButton *btnOK = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOK setFrame:CGRectMake(230, 400, 150, 40)];
    [btnOK setBackgroundColor:[UIColor clearColor]];
    [btnOK setBackgroundImage:[UIImage imageNamed:@"blue_button.png"] forState:UIControlStateNormal];
    [btnOK setTitle:@"OK" forState:UIControlStateNormal];
    [btnOK addTarget:self action:@selector(btnOKPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:lblDetails];
    [contentView addSubview:txtDebtName];
    [contentView addSubview:lblOR];
    [contentView addSubview:txtDebtID];
    [contentView addSubview:btnOK];
    
    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
}


-(void)btnOKPressed
{
    if(txtDebtName.text.length <= 0 && txtDebtID.text.length <= 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter Debtor Name or Debtor Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        quoteDebtorCodeStr = [dictHeaderDetails objectForKey:@"PriceCode"];
        
        __block NSString *strID;
        if (txtDebtName.text.length > 0)
        {
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                
                FMResultSet *rs = [db executeQuery:@"SELECT CODE from armaster where NAME = ?",txtDebtName.text];
                
                if (!rs)
                {
                    [rs close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    
                }
                else
                {
                    while ([rs next])
                    {
                        strID = [rs stringForColumn:@"CODE"];
                    }
                    
                    [rs close];
                }
            }];
        }
        else
        {
            strID = txtDebtID.text;
        }
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
            
            @try
            {
                
                //Change by subhu
                strID = [strID trimSpaces];
                strCopyProfileDebtorId = strID;
                
                FMResultSet *rs = [db executeQuery:@"SELECT AVAILABLE,STOCK_CODE, PREF_SUPPLIER, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, max(AVERAGE_COST) AS AVERAGE_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT a.AVAILABLE as AVAILABLE, a.PREF_SUPPLIER AS PREF_SUPPLIER,a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.AVERAGE_COST AS AVERAGE_COST,a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1, a.PROD_GROUP as PROD_GROUP,                                                                                                                                                                                                                                                                                               (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ? )  temp GROUP BY STOCK_CODE ORDER BY PROD_GROUP",strID,strID,strID,strID,strID,strID,[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces]];//AND ((a.`STATUS` = 'AC' OR a.`STATUS` = 'NO') AND  a.`STATUS` <> 'OB'
                
                if (!rs)
                {
                    [rs close];
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                    
                }
                else
                {
                    [arrProducts removeAllObjects];
                    if(!arrProducts)
                    {
                        arrProducts = [NSMutableArray new];
                    }
                    
                    while([rs next])
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        
                        NSDictionary *dictData = [rs resultDictionary];
                        [dict setValue:[dictData objectForKey:@"AVAILABLE"] forKey:@"Available"];
                        [dict setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
                        [dict setValue:[dictData objectForKey:@"COST_DISS"] forKey:@"Dissection_Cos"];
                        [dict setValue:[dictData objectForKey:@"SALES_DISS"] forKey:@"Dissection"];
                        [dict setValue:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"LastSold"];
                        [dict setValue:[dictData objectForKey:@"STOCK_CODE"] forKey:@"StockCode"];
                        [dict setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
                        [dict setValue:[dictData objectForKey:@"PRICE2"] forKey:@"Price2"];
                        [dict setValue:[dictData objectForKey:@"PRICE3"] forKey:@"Price3"];
                        [dict setValue:[dictData objectForKey:@"PRICE4"] forKey:@"Price4"];
                        [dict setValue:[dictData objectForKey:@"PRICE5"] forKey:@"Price5"];
                        [dict setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
                        [dict setValue:[dictData objectForKey:@"q0"] forKey:@"q0"];
                        [dict setValue:[dictData objectForKey:@"q1"] forKey:@"q1"];
                        [dict setValue:[dictData objectForKey:@"q2"]forKey:@"q2"];
                        [dict setValue:[dictData objectForKey:@"q3"] forKey:@"q3"];
                        [dict setValue:[dictData objectForKey:@"q4"] forKey:@"q4"];
                        [dict setValue:[dictData objectForKey:@"STANDARD_COST"] forKey:@"Cost"];
                        [dict setValue:[dictData objectForKey:@"AVERAGE_COST"] forKey:@"AverageCost"];
                        [dict setValue:[dictData objectForKey:@"TAX_CODE1"] forKey:@"TAX_CODE1"];
                        [dict setValue:[dictData objectForKey:@"ALLOCATED"] forKey:@"Allocated"];
                        [dict setValue:[dictData objectForKey:@"CUSTOMER_CODE"] forKey:@"CUSTOMER_CODE"];
                        [dict setValue:@"0" forKey:@"isNewProduct"];

                        [dict setValue:quoteDebtorCodeStr forKey:@"PriceCode"];
                        [arrProducts addObject:dict];
//                        [arrcopyprofileArr addObject:dict];
                    }
                }
                [rs close];
            }
            @catch (NSException *e)
            {
                
            }
            
        }];
        
        
        if ([arrProducts count] > 0)
        {
            //Debtor List
            
//            CopyProfileViewController *dataViewController = [[CopyProfileViewController alloc] initWithNibName:@"CopyProfileViewController" bundle:[NSBundle mainBundle]];
//            dataViewController.arrProductProfile = arrcopyprofileArr;
//            dataViewController.customerNamestr = txtDebtName.text;
//            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
//            
             [vwContentSuperview removeAllSubviews];
             [vwContentSuperview addSubview:vwDetails];
             [tblDetails reloadData];

            [[KGModal sharedInstance] hideAnimated:YES];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Products found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
    
    if ([[operationQueue operations] count]) {
        
        [operationQueue cancelAllOperations];
        
        if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected && !EditableMode) {
            
            //Remove observer
            for (NSOperation *req in [operationQueue operations]) {
                [req removeObserver:self forKeyPath:@"isFinished"];
            }
            
            [operationQueue removeObserver:self forKeyPath:@"operations"];
        }
        
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    btnDelete.hidden = YES;
    
    if (EditableMode==NO) {
        //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    }
    
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
        }
            break;
            
        case 1:{
            
            if (EditableMode == NO) {
                
                if ([arrProducts count]) {
                    btnDelete.hidden = NO;
                    [vwContentSuperview addSubview:vwDetails];
                }
                else{
                    //No products
                    [vwContentSuperview addSubview:vwNoProducts];
                    [self showPointingAnimation];
                }
            }
            else{
                [self doAfterDataFetched];
            }
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark - KVO Observing/ WS Handler

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)operation change:(NSDictionary *)change context:(void *)context {
    NSString *source = nil;
    //NSData *data = nil;
    NSError *error = nil;
    
    if ([operation isKindOfClass:[DownloadUrlOperation class]]) {
        DownloadUrlOperation *downloadOperation = (DownloadUrlOperation *)operation;
        source = [NSString stringWithFormat:@"%d",downloadOperation.tag];
        
        if (source) {
            
            //get total queue size by the first success and add 1 back
            if (queueSize ==0) {
                queueSize = [[operationQueue operations] count] +1.0;
            }
            
            float progress = (float)(queueSize-[[operationQueue operations] count])/queueSize;
            NSLog(@"progress %f",progress);
            progessView.progress = progress;
            lblPercentageIndicator.text = [NSString stringWithFormat:@"%.0f%@",progress*100,@"%"];
            
            error = [downloadOperation error];
            //data = [downloadOperation data];
            
            //Error
            if (error != nil) {
                NSLog(@"error.code %d",error.code);
            }
            //Success
            else{
                
                NSLog(@"success.code %d",error.code);
            }
            
        }
        
    }
    //Check Queue is empty or not
    else if (operation == operationQueue && [keyPath isEqualToString:@"operations"]) {
        if ([operationQueue.operations count] == 0) {
            // Do something here when your queue has completed
            NSLog(@"queue has completed");
            //[self executeSyncCompletedOperations];
            
            [self setUpInitialView];
            
            //Remove observer
            for (NSOperation *req in [operation operations]) {
                [req removeObserver:self forKeyPath:@"isFinished"];
            }
            
            [operation removeObserver:self forKeyPath:@"operations"];
            
            if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
                //Fetch latest quote number from server
                [self callWSGetNewQuoteNumber];
            }
            else{
                [self callGetNewQuoteNumberFromDB];
            }
            
        }
    }
    
    else {
        [super observeValueForKeyPath:keyPath ofObject:operation
                               change:change context:context];
    }
}

#pragma mark -
#pragma mark UIKeyboardWillHideNotification

- (void)KeyboardWillChangeFrameNotification:(NSNotification *)notification
{
    CGRect keyboardEndFrame;
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame fromView:nil];
    
    if (CGRectIntersectsRect(keyboardFrame, self.view.frame)){
        // Keyboard is visible
    }
    else
    {
        CGPoint contentOffset = CGPointMake(0,0);
        
        [self.tblHeader setContentOffset:contentOffset animated:YES];
        
    }
}

#pragma mark - Text Field Delgates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSString *strBranchList = [[NSUserDefaults standardUserDefaults] objectForKey:@"BRANCH"];
    NSString *strMember = [[NSUserDefaults standardUserDefaults] objectForKey:@"members"];
    
    
    if ([strMember caseInsensitiveCompare:ADMIN]  == NSOrderedSame || [strMember caseInsensitiveCompare:TELE_SALES] == NSOrderedSame)
    {
        
        if(textField.tag == 101)
        {
            if (txtDebtID.text.length > 0)
            {
                [txtDebtID setText:@""];
            }
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                FMResultSet *rs;
                
                @try
                {
                    NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
                    rs = [db executeQuery:@"SELECT NAME FROM armaster ORDER BY NAME"];
                    
                    if (!rs)
                    {
                        [rs close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        
                    }
                    else
                    {
                        [arrAutoFill removeAllObjects];
                        if(!arrAutoFill)
                        {
                            arrAutoFill = [NSMutableArray new];
                        }
                        
                        while([rs next])
                        {
                            [arrAutoFill addObject:[rs stringForColumn:@"NAME"]];
                        }
                    }
                    //NSLog(@"%@",arrAutoFill);
                    [rs close];
                }
                @catch (NSException *e)
                {
                    
                }
            }];
            
        }
        else if (textField.tag == 102)
        {
            if (txtDebtName.text.length > 0)
            {
                [txtDebtName setText:@""];
            }
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                FMResultSet *rs;
                
                @try
                {
                    //                NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
                    rs = [db executeQuery:@"SELECT CODE FROM armaster  ORDER BY CODE"];
                    
                    if (!rs)
                    {
                        [rs close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        
                    }
                    else
                    {
                        [arrAutoFill removeAllObjects];
                        
                        if(!arrAutoFill)
                        {
                            arrAutoFill = [NSMutableArray new];
                        }
                        
                        while([rs next])
                        {
                            [arrAutoFill addObject:[rs stringForColumn:@"CODE"]];
                        }
                    }
                    //NSLog(@"%@",arrAutoFill);
                    [rs close];
                }
                @catch (NSException *e)
                {
                    
                }
            }];
        }

    }
    else
    {
        if(textField.tag == 101)
        {
            if (txtDebtID.text.length > 0)
            {
                [txtDebtID setText:@""];
            }
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                FMResultSet *rs;
                
                @try
                {
                    NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
                    rs = [db executeQuery:@"SELECT NAME FROM armaster WHERE BRANCH = ? ORDER BY NAME",[strBranchList trimSpaces]];
                    
                    if (!rs)
                    {
                        [rs close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        
                    }
                    else
                    {
                        [arrAutoFill removeAllObjects];
                        if(!arrAutoFill)
                        {
                            arrAutoFill = [NSMutableArray new];
                        }
                        
                        while([rs next])
                        {
                            [arrAutoFill addObject:[rs stringForColumn:@"NAME"]];
                        }
                    }
                    //NSLog(@"%@",arrAutoFill);
                    [rs close];
                }
                @catch (NSException *e)
                {
                    
                }
            }];
            
        }
        else if (textField.tag == 102)
        {
            if (txtDebtName.text.length > 0)
            {
                [txtDebtName setText:@""];
            }
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                FMResultSet *rs;
                
                @try
                {
                    //                NSString *strSalesman = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
                    rs = [db executeQuery:@"SELECT CODE FROM armaster WHERE BRANCH = ? ORDER BY CODE",[strBranchList trimSpaces]];
                    
                    if (!rs)
                    {
                        [rs close];
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        
                    }
                    else
                    {
                        [arrAutoFill removeAllObjects];
                        
                        if(!arrAutoFill)
                        {
                            arrAutoFill = [NSMutableArray new];
                        }
                        
                        while([rs next])
                        {
                            [arrAutoFill addObject:[rs stringForColumn:@"CODE"]];
                        }
                    }
                    //NSLog(@"%@",arrAutoFill);
                    [rs close];
                }
                @catch (NSException *e)
                {
                    
                }
            }];
        }

    }
  }

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textfield
{
    if(textfield.tag != 101 || textfield.tag != 102)
    {
        CGPoint pointInTable = [textfield.superview convertPoint:textfield.frame.origin toView:self.tblHeader];
        CGPoint contentOffset = self.tblHeader.contentOffset;
        
        contentOffset.y = (pointInTable.y - textfield.inputAccessoryView.frame.size.height);
        
        //NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
        
        [self.tblHeader setContentOffset:contentOffset animated:YES];
        txtCustOrder = textfield;
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    CGPoint contentOffset = CGPointMake(0,0);
    
    [self.tblHeader setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == 101)
    {
        NSString *tempString = nil;
        if ((textField.text.length) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@",textField.text];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAutoFill filteredArrayUsingPredicate:regexString];
            [arrFilteredData removeAllObjects];
            arrFilteredData = nil;
            arrFilteredData = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            [arrFilteredData removeAllObjects];
            arrFilteredData = nil;
            
            arrFilteredData = [[NSMutableArray alloc] initWithArray:arrAutoFill];
        }
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = arrFilteredData;
        viewCnt.strTextField = @"Name";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(400,230);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:contentView permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else if(textField.tag == 102)
    {
        NSString *tempString = nil;
        if ((textField.text.length) > 0)
        {
            tempString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        }
        else
        {
            tempString = string;
        }
        
        if(![tempString isEqualToString:@""])
        {
            NSString *indexRegex = [NSString stringWithFormat:@"%@",tempString];
            NSPredicate *regexString = [NSPredicate predicateWithFormat:@"SELF beginswith[cd] %@",indexRegex];
            NSArray *filteredArray = [arrAutoFill filteredArrayUsingPredicate:regexString];
            
            [arrFilteredData removeAllObjects];
            arrFilteredData = nil;
            arrFilteredData = [[NSMutableArray alloc] initWithArray:filteredArray];
        }
        else
        {
            [arrFilteredData removeAllObjects];
            arrFilteredData = nil;
            arrFilteredData = [[NSMutableArray alloc] initWithArray:arrAutoFill];
        }
        
        if(!viewCnt)
        {
            viewCnt = [[StockCodeViewController alloc] initWithNibName:@"StockCodeViewController" bundle:[NSBundle mainBundle]];
        }
        viewCnt.arrData = arrFilteredData;
        viewCnt.strTextField = @"ID";
        
        //resize the popover view shown
        //in the current view to the view's size
        viewCnt.contentSizeForViewInPopover = CGSizeMake(400,230);
        
        if([self.popoverController isPopoverVisible])
        {
            [self.popoverController dismissPopoverAnimated:NO];
        }
        
        //create a popover controller
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewCnt];
        
        viewCnt.delegate = self;
        
        [self.popoverController presentPopoverFromRect:textField.frame inView:contentView permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else if(textField.tag == 231)
    {
        int tempCount = [textField.text length];
        
        txtCustOrder = textField;
        
        if(tempCount > 19)
        {
            return NO;
        }
    }
    else{
        textField.text = @"";
    }
    
    return YES;
}
#pragma mark Table view data source and delegate
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 0) {
                return nil;
            }
            else if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Name"];
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address1"];
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address2"];
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address3"];
                    }
                        break;
                        
                }
                
                //We only don't want to allow selection on any cells which cannot be expanded
                if([self getLabelHeightForString:stringToCheck] > COMMENT_LABEL_MIN_HEIGHT)
                {
                    return indexPath;
                }
                else {
                    return nil;
                }
            }
            else{
                return indexPath;
            }
            
        }break;
            
        case TAG_100:return indexPath;break;
            
        default:return nil;break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case TAG_50:{
            //If this is the selected index we need to return the height of the cell
            //in relation to the label height otherwise we just return the minimum label height with padding
            
            if (indexPath.section == 2) {
                
                NSString *stringToCheck = nil;
                switch ([indexPath row]) {
                    case 0:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Name"];
                    }
                        break;
                    case 1:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address1"];
                    }
                        break;
                    case 2:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address2"];
                    }
                        break;
                    case 3:
                    {
                        stringToCheck = [dictHeaderDetails objectForKey:@"Address3"];
                    }
                        break;
                        
                }
                
                if(selectedIndex == indexPath.row)
                {
                    return [self getLabelHeightForString:stringToCheck] + COMMENT_LABEL_PADDING * 2;
                }
                else {
                    return COMMENT_LABEL_MIN_HEIGHT + COMMENT_LABEL_PADDING * 2;
                    //return 60;
                }
            }
            else{
                return 60;break;
            }
            
            
            
        }break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSLog(@"%d\n",[[arrHeaderLabels objectAtIndex:section] count]);
    switch (tableView.tag)
    {
            
        case TAG_50:return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];break;
            
        case TAG_100:return [arrProducts count];break;
            
        default:return 0;break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (tableView.tag) {
            
        case TAG_50:{
            if (section == 1) {
//                return @"Please Select Debtor";
                return nil;
            }
        }break;
            
        default:break;
    }
    // Return the displayed title for the specified section.
    return nil;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier4 = CELL_IDENTIFIER4;
    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    static NSString *CellIdentifier10 = CELL_IDENTIFIER10;
    
    CustomCellGeneral *cell = nil;
    switch (tableView.tag) {
        case TAG_50:{
            
            //Debtor
            if ([indexPath section] == 1 && [indexPath row] == 0)
            {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:3];
                    
                }
                
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                
                cell.txtValue.userInteractionEnabled = FALSE;
                cell.txtValue.placeholder = @"Please Select";
                
            }
            else{
                
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:4];
                    
                }
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            
            
            if (selectedSectionIndex == 2 && selectedIndex == indexPath.row) {
                
                CGFloat labelHeight = [self getLabelHeightForString:cell.lblValue.text];
                cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                 cell.lblValue.frame.origin.y,
                                                 cell.lblValue.frame.size.width,
                                                 labelHeight);
                
            }
            else {
                
                //Otherwise just return the minimum height for the label.
                cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x,
                                                 cell.lblValue.frame.origin.y,
                                                 cell.lblValue.frame.size.width,
                                                 COMMENT_LABEL_MIN_HEIGHT);
            }
            
            
            cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            cell.txtValue.text = nil;
            cell.lblValue.text = nil;
            
            cell.txtValue.inputAccessoryView = keyboardToolBar;
            //cell.txtValue.delegate = self;
            
            if ([indexPath section] == 0) {
                switch ([indexPath row]) {
                    case 0:{
                        
                        if (EditableMode == NO) {
                            if ([dictHeaderDetails objectForKey:@"QuoteNum"]) {
                                cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"QuoteNum"]];
                            }
                        }else{
                            cell.lblValue.text = strQuoteNumRecived;
                        }
                        break;
                    }
                    default:break;
                }
            }
            
            if ([indexPath section] == 1) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"Debtor"]) {
                            cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
                        }
                        break;
                    default:break;
                }
            }
            
            if ([indexPath section] == 2) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"Name"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Name"] trimSpaces]];
                        }
                        
                        break;
                    case 1:
                        
                        if ([dictHeaderDetails objectForKey:@"Address1"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address1"] trimSpaces]];
                        }
                        break;
                    case 2:
                        
                        if ([dictHeaderDetails objectForKey:@"Address2"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address2"] trimSpaces]];
                        }
                        break;
                    case 3:
                        
                        if ([dictHeaderDetails objectForKey:@"Address3"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address3"] trimSpaces]];
                            
                        }
                        break;
                    case 4:
                        
                        
                        if ([dictHeaderDetails objectForKey:@"DeliveryDate"])
                        {
                            NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DeliveryDate"] trimSpaces]];
                            
                            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            
                            NSDate *date1 = [dateFormat dateFromString:dateStr];
                            
                            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                            [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
                            NSString *strDate = [dateFormat2 stringFromDate:date1];
                            
                            
                            cell.lblValue.text = strDate;
                            
                        }
                        break;
                    case 5:
                        
                        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
                        
                        if (cell == nil)
                        {
                            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                            cell = [nib objectAtIndex:9];
                        }
                        
                        cell.txtGlowingValue.keyboardType = UIKeyboardTypeNamePhonePad;
                        cell.txtGlowingValue.delegate = self;
                        cell.txtGlowingValue.tag = 231;
                        
                        cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
                        if ([dictHeaderDetails objectForKey:@"CustOrderno"])
                        {
                            //cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"CustOrderno"] trimSpaces]];
                            cell.txtGlowingValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"CustOrderno"] trimSpaces]];
                        }
                        
                        
                        break;
                    default:
                        break;
                }
                
            }
            
            if ([indexPath section] == 3) {
                switch ([indexPath row]) {
                    case 0:
                        if ([dictHeaderDetails objectForKey:@"DelName"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelName"];
                        }
                        
                        break;
                    case 1:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress1"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress1"];
                        }
                        break;
                    case 2:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress2"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress2"];
                        }
                        break;
                    case 3:
                        
                        if ([dictHeaderDetails objectForKey:@"DelAddress3"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelAddress3"];
                            
                        }
                        break;
                    case 4:
                        if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelPostCode"];
                            
                        }
                        break;
                    case 5:
                        if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
                            cell.lblValue.text = [dictHeaderDetails objectForKey:@"DelCountry"];
                            
                        }
                        break;
                    case 6:
                        if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DeliveryDate"] trimSpaces]];
                            
                        }
                        break;
                    case 7:
                        if ([dictHeaderDetails objectForKey:@"CustOrderno"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"CustOrderno"] trimSpaces]];
                            
                        }
                        break;
                        
                    default:
                        break;
                }
                
            }
            
            
            
            
            return cell;
            
        }break;
            
        case TAG_100:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:5];
            }
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString *str = [NSString stringWithFormat:@"/C"];
            
            if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                
                NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]];
                
                if ([str rangeOfString:str2].location == NSNotFound) {
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                }
                else
                {
                    NSString *str111 = [NSString stringWithFormat:@"/C"];
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str111,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    cell.lblValue.text = @"";
                }
            }
            else{
                
                NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
                
                if ([str rangeOfString:str2].location == NSNotFound) {
                    
                    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    
                    if ([[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"]) {
                        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"ExtnPrice"] floatValue]];
                    }
                    else{
                        cell.lblValue.text = @"";
                    }
                    
                }
                else
                {
                    NSString *str111 = [NSString stringWithFormat:@"/C"];
                    cell.lblTitle.text =[NSString stringWithFormat:@"%@ %@",str111,[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                    cell.lblValue.text = @"";
                }
                
                
            }
            return cell;
        }break;
            
        default:return nil;break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (tableView.tag) {
        case TAG_50:{
            
            if ([indexPath section] == 1 && [indexPath row] == 0) {
                
                if(EditableMode)
                {
                    return;
                }
                NSLog(@"EditableMode==%d",EditableMode);
                
                if (EditableMode == NO) {
                    
                    NSLog(@"EditableMode no QuoteHistoryCustomerListViewController");
                    
                    //Debtor List
                    QuoteHistoryCustomerListViewController *dataViewController = [[QuoteHistoryCustomerListViewController alloc] initWithNibName:@"QuoteHistoryCustomerListViewController" bundle:[NSBundle mainBundle]];
                    dataViewController.isFromLoadProfile = NO;
                    dataViewController.isUserCallSchedule = NO;
                    
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                }
                else{
                    
                    if (chkFinalize == NO) {
                        
                        NSLog(@"EditableMode yes chkfinalaizze no abcdViewcontroller");
                        
                        QuoteHistoryCustomerListEditableMode *dataViewController = [[QuoteHistoryCustomerListEditableMode alloc] initWithNibName:@"QuoteHistoryCustomerListEditableMode" bundle:[NSBundle mainBundle]];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                }
            }
            
            if ([indexPath section] == 2) {
                //The user is selecting the cell which is currently expanded
                //we want to minimize it back
                if(selectedIndex == indexPath.row)
                {
                    selectedIndex = -1;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    
                    return;
                }
                
                //First we check if a cell is already expanded.
                //If it is we want to minimize make sure it is reloaded to minimize it back
                if(selectedIndex >= 0)
                {
                    NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:selectedSectionIndex];
                    selectedIndex = indexPath.row;
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                //Finally set the selected index to the new selection and reload it to expand
                selectedIndex = indexPath.row;
                selectedSectionIndex = indexPath.section;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }break;
            
        case TAG_100:{
            
            
            if (chkFinalize == NO) {
                NSString *str = [NSString stringWithFormat:@"/C"];
                
                if([[arrProducts objectAtIndex:indexPath.row] objectForKey:@"Item"]) {
                    
                    NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Item"]];
                    
                    if ([str rangeOfString:str2].location == NSNotFound)
                    {
                        ViewQuoteProductDetailsViewController *dataViewController = [[ViewQuoteProductDetailsViewController alloc] initWithNibName:@"ViewQuoteProductDetailsViewController" bundle:[NSBundle mainBundle]];
                        NSLog(@"pRODUCT Details : %@",[arrProducts objectAtIndex:[indexPath row]]);
                        dataViewController.delegate = self;
                        
                        NSString *isnew = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"];
                        
                        if ([isnew isEqualToString:@"1"]) {
                            dataViewController.strDebtor = strDebtorNum;
                        }
                        else{
                            dataViewController.strDebtor = strCopyProfileDebtorId;

                        }
                        
                        dataViewController.isFromViewQuoteDetails = NO;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                    else{
                        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                }
                else{
                    
                    NSString *str2 = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
                    
                    if ([str rangeOfString:str2].location == NSNotFound)
                    {
                        ViewQuoteProductDetailsViewController *dataViewController = [[ViewQuoteProductDetailsViewController alloc] initWithNibName:@"ViewQuoteProductDetailsViewController" bundle:[NSBundle mainBundle]];
                        //NSLog(@"pRODUCT Details : %@",[arrProducts objectAtIndex:[indexPath row]]);
                        dataViewController.delegate = self;
                        
//                        dataViewController.strDebtor = strCopyProfileDebtorId;
                        NSString *isnew = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"isNewProduct"];
                        
                        if ([isnew isEqualToString:@"1"]) {
                            dataViewController.strDebtor = strDebtorNum;
                        }
                        else{
                            dataViewController.strDebtor = strCopyProfileDebtorId;
                            
                        }


                        dataViewController.isFromViewQuoteDetails = NO;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.dictProductDetails = [arrProducts objectAtIndex:[indexPath row]];
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                    }
                    else{
                        
                        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
                        dataViewController.commentStr = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
                        dataViewController.delegate = self;
                        dataViewController.productIndex = [indexPath row];
                        dataViewController.isCommentAddedOnLastLine = isCommentAddedOnLastLine;
                        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
                        
                    }
                    
                    
                }
            }
        }break;
    }
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    switch (tableView.tag) {
            
        case TAG_50:{
            return NO;break;
        }break;
            
        case TAG_100:{
            return YES;break;
        }break;
            
        default:return NO;break;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
            
        case TAG_100:{
            if (editingStyle == UITableViewCellEditingStyleDelete)
            {
                self.deleteIndexPath = indexPath;
                self.deleteRowIndex = indexPath.row;
                
                if (EditableMode == YES) {
                    if ([arrProducts count]) {
                        if([[[arrProducts objectAtIndex:deleteRowIndex] objectForKey:@"isNewProduct"] isEqualToString:@"1"]){
                            [self doAfterdelete];
                        }
                        else{
                            [self deleteProductInEditableMode:deleteRowIndex];
                        }
                        
                    }
                }
                else{
                    [self doAfterdelete];
                }
            }
        }break;
    }
}

#pragma mark - Dissmiss Popover
-(void)actiondismissPopOverStockPickUp:(NSString *)strPickUpCode strTextField:(NSString *)strTextField
{
    if([strTextField isEqualToString:@"Name"])
    {
        [txtDebtName setText:strPickUpCode];
    }
    else
    {
        [txtDebtID setText:strPickUpCode];
    }
    
    [self.popoverController dismissPopoverAnimated:YES];
}

#pragma mark - IBActions

- (IBAction)actionShowComment:(id)sender
{
    [self showCommentView];
}

//Load Profile
- (IBAction)actionShowQuoteHistory:(id)sender{
    //Change the selection
    [segControl setSelectedIndex:1];
    
    QuoteHistoryCustomerListEditableMode *dataViewController = [[QuoteHistoryCustomerListEditableMode alloc] initWithNibName:@"QuoteHistoryCustomerListEditableMode" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (void)showCommentView{
    __block BOOL isCommentExist = NO;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]||[[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        QuoteProductComment *dataViewController = [[QuoteProductComment alloc] initWithNibName:@"QuoteProductComment" bundle:[NSBundle mainBundle]];
        dataViewController.commentStr = @"";
        dataViewController.commntTAg = 0;
        dataViewController.delegate = self;
        dataViewController.productIndex = -1;
        [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
    }
}


//-----Original Code changed to save data into local Database always
/*
 - (void)actionSaveQuote:(int)finalizeTag
 {
 self.finalise = finalizeTag;
 
 BOOL isCalculationPending = FALSE;
 for (NSDictionary *dict in arrProducts) {
 
 //Dont check comments
 if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]))
 {
 if (![dict objectForKey:@"ExtnPrice"])
 {
 isCalculationPending = TRUE;
 //break;
 }
 else
 {
 isCalculationPending = FALSE;
 break;
 }
 }
 }
 
 if(isCalculationPending){
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
 
 [alert show];
 return;
 }
 //    else{
 
 if (EditableMode == NO) {
 
 if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
 {
 [self callSaveQuoteToDB:finalizeTag];
 }
 else{
 [self callWSSaveQuote:finalizeTag];
 }
 }
 else {
 if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
 {
 [self callSaveEditQuoteToDB:finalizeTag];
 }
 else{
 [self callWSSaveQuoteForEdit:finalizeTag];
 }
 
 }
 //    }
 
 }
 
 */

- (void)actionSaveQuote:(int)finalizeTag
{
    self.finalise = finalizeTag;
    
    BOOL isCalculationPending = FALSE;
    for (NSDictionary *dict in arrProducts) {
        
        //Dont check comments
        if (!([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"]))
        {
            if (![dict objectForKey:@"ExtnPrice"])
            {
                isCalculationPending = TRUE;
                //break;
            }
            else
            {
                isCalculationPending = FALSE;
                break;
            }
        }
    }
    
    if(isCalculationPending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please complete rest of the calculations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        return;
    }
    //    else{
    
    if (EditableMode == NO) {
        
        //--Always save to dabtabase
        if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
        {
            [self callSaveEditQuoteToDB:finalizeTag];
        }
        else{
            //--Send data to server
            if(finalizeTag == 1)
            {
                [self callWSSaveQuote:finalizeTag];
            }
            else if (finalizeTag == 0)
                [self callSaveEditQuoteToDB:finalizeTag];
        }
    }
    else {
        //--If Editabale Mode send data to Webservice
        if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
        {
            if (finalizeTag == 0) {
                [self callSaveEditQuoteToDB:finalizeTag];
            }
            else if(finalizeTag == 1)
                [self callWSSaveQuote:finalizeTag];
        }
        else
        {
            [self callSaveEditQuoteToDB:finalizeTag];
        }
        /*
         if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
         {
         [self callSaveEditQuoteToDB:finalizeTag];
         }
         else{
         [self callWSSaveQuoteForEdit:finalizeTag];
         }
         */
        
    }
    //    }
    
}

//Delete actions
- (IBAction)actionDeleteRows:(id)sender{
    
    if ([tblDetails isEditing]) {
        [self showOrEnableButtons];
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
        UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:downImage  forState:UIControlStateNormal];
        [tblDetails setEditing:NO animated:YES];
    }
    else{
        
        [self hideOrDisableButtons];
        [btnDelete setTitle:@"Done" forState:UIControlStateNormal];
        
        NSString *strImage=[[NSBundle mainBundle] pathForResource:@"done_button" ofType:@"png"];
        UIImage *downImage=[[UIImage alloc]initWithContentsOfFile:strImage];
        
        [btnDelete setBackgroundImage:downImage  forState:UIControlStateNormal];
        [tblDetails setEditing:YES animated:YES];
    }
    
    //[tblDetails reloadData];
}

- (IBAction)actionShowProductList:(id)sender{
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (IBAction)actionDisplayQuoteMenu:(id)sender{
    
    //tagFlsh = 1;
    //[self flashFinal:btnAddMenu];
    
    QuoteMenuPopOverViewController *popoverContent = [[QuoteMenuPopOverViewController alloc] initWithNibName:@"QuoteMenuPopOverViewController" bundle:[NSBundle mainBundle]];

    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.contentSizeForViewInPopover =
    CGSizeMake(200, 300);
    popoverContent.isFinalisedNeeded = YES;
    popoverContent.isFreightNotNeeded = YES;
    popoverContent.isProfileOrderEntry = NO;
    popoverContent.isFromOfflineOrder = NO;
//
    //create a popover controller
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:popoverContent];
    
    popoverController.delegate = self;
    popoverContent.delegate = self;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_x_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect rectBtnMenu = CGRectMake(btnAddMenu.frame.origin.x, self.view.frame.size.height - 40,btnAddMenu.frame.size.width,btnAddMenu.frame.size.height);
    
    //present the popover view non-modal with a
    //refrence to the button pressed within the current view
    [self.popoverController presentPopoverFromRect:rectBtnMenu
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}

-(void)prepareForNewQuote{
    
    [arrProducts removeAllObjects];
    [dictHeaderDetails removeAllObjects];
    
    self.strQuoteNum = @"";
    
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwHeader];
    
    //Change the selection
    [segControl setSelectedIndex:0];
    
    segControl.enabled = NO;
    //[tblHeader reloadData];
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
}

#pragma mark - Alert Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 001)
    {
        if (buttonIndex == 1)
        {
            NSLog(@"Clicked OK");
        }
    }
    
    if (alertView.tag == TAG_50) {
        if (EditableMode == NO) {
            
            [self prepareForNewQuote];
            
            
            //---Generate new quote locally
            [self callGetNewQuoteNumberFromDB];
            [tblHeader reloadData];
            
            /*
             if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
             //Fetch latest quote number from server
             [self callWSGetNewQuoteNumber];
             }
             else{
             [self callGetNewQuoteNumberFromDB];
             }
             */
            
            //[self callGetNewQuoteNumberFromDB];
        }
        else{
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeViewsFromRightEndFromStack];
        }
        
        // Store the view is profile order.
        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isQuoteCreated"];
        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForQuote"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    //Error in connection alert
    if (alertView.tag == TAG_200) {
        if (buttonIndex == 1) {
            [self callSaveQuoteToDB:finalise];
        }
    }
    
    //Error in connection alert
    if (alertView.tag == TAG_300) {
        if (buttonIndex == 1) {
            [self callSaveEditQuoteToDB:finalise];
        }
    }
    
}


#pragma mark - Database call
//--Delete Quote Uploaded to server
-(void)DeleteQuoteFromDatabase:(NSString *)QuoteNum
{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    
    [databaseQueue inDatabase:^(FMDatabase *db) {
        [self deleteQuoteFromHeader:db QuoteNum:QuoteNum];
        
    }];
    
}
-(void)deleteQuoteFromHeader:(FMDatabase *)db QuoteNum:(NSString *)QuoteNum{
    
    @try {
        BOOL *Result;
        NSString *strDelete = [NSString stringWithFormat:@"Delete from soquohea where QUOTE_NO = '%@'",QuoteNum];
        Result = [db executeUpdate:strDelete];
        
        if (Result == NO)
        {
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        else if(Result)
        {
            [self deleteQuoteFromDetails:db QuoteNum:QuoteNum];
        }
        
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Local Db Error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert = nil;
    }
    
}

-(void)deleteQuoteFromDetails:(FMDatabase *)db QuoteNum:(NSString *)QuoteNum{
    
    @try {
        BOOL Result;
        NSString *strDelete = [NSString stringWithFormat:@"Delete from soquodet where QUOTE_NO = '%@'",QuoteNum];
        Result = [db executeQuery:strDelete];
        if (Result == NO)
        {
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        else if(Result)
        {
            
            spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [operationQueue addOperationWithBlock:^{
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                
                [databaseQueue inDatabase:^(FMDatabase *db) {
                    [self callViewQuoteDetailsHeaderFromDB:db];
                    [self callViewQuoteDetailsDetailsFromDB:db];
                }];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
                    //EX 2: without Userinfo and without preparing NSNotification object
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadData" object:nil];
                    
                    //  spinner.mode = MBProgressHUDModeIndeterminate;
                    [spinner removeFromSuperview];
                    //[self prepareForNewQuote];
                    [tblHeader reloadData];
                }];
            }];
            
        }
        
        else
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
                //EX 2: without Userinfo and without preparing NSNotification object
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadData" object:nil];
                
                //  spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                //[self prepareForNewQuote];
                [tblHeader reloadData];
            }];
        }
        
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Local Db Error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert = nil;
    }
    
}

-(void)callSaveQuoteToDB:(int)finalizeTag
{
    strWebserviceType = @"DB_QUOTE_SAVE";
    
    self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
    
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strDelAddress1 = @"";
    NSString *strDelAddress2 = @"";
    NSString *strDelAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = @"";
    NSString *strBranch = @"";
    NSString *strDelDate = @"";
    
    
    NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Debtor"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelName"]) {
        strDelName= [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelName"] trimSpaces]];
        
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress1"]) {
        strDelAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress2"]) {
        strDelAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress3"]) {
        strDelAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress3"] trimSpaces]];
    }
    
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Suburb"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelPostCode"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelCountry"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"ReleaseType"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Status"]) {
        strStatus = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Status"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Warehouse"] trimSpaces]];
        
        if (strWarehouse.length > 2) {
            NSLog(@"");
        }
    }
    
    if ([dictHeaderDetails objectForKey:@"Branch"]) {
        strBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Branch"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDelDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DeliveryDate"] trimSpaces]];
    }
    
    
    NSString *strContact = @"";
    if ([dictHeaderDetails objectForKey:@"Contact"]) {
        strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
    }
    
    NSString *strPriceCode = @"";
    if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
    }
    
    NSString *strSalesman = @"";
    if ([dictHeaderDetails objectForKey:@"Salesman"]) {
        strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
    }
    
    NSString *strSalesBranch = @"";
    if ([dictHeaderDetails objectForKey:@"SalesBranch"]) {
        strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SalesBranch"]];
    }
    
    NSString *strTradingTerms = @"";
    if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
        strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
    }
    
    NSString *strExchangeCode = @"";
    if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
        strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
    }
    
    NSString *strChargetype = @"";
    if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
        strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
    }
    
    NSString *strExportDebtor = @"";
    if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
        strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
    }
    
    NSString *strChargeRate = @"";
    if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
        strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
    }
    
    NSString *strHeld = @"";
    if ([dictHeaderDetails objectForKey:@"Held"]) {
        strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
    }
    
    NSString *strNumboxes = @"";
    if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
        strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
    }
    
    NSString *strDirect = @"";
    if ([dictHeaderDetails objectForKey:@"Direct"]) {
        strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
    }
    
    NSString *strDateRaised = @"";
    if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
    }
    
    NSString *strPeriodRaised = @"";
    if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
        strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
    }
    
    NSString *strYearRaised = @"";
    if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
        strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
    }
    
    NSString *strDropSequence = @"";
    if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
        strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
    }
    
    NSString *strDeliveryDate = @"";
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
    }
    
    NSString *strActive = @"N";
    NSString *strEditStatus = @"11";
    NSString *strExchangeRate = @"1.000000";
    
    if([dictHeaderDetails objectForKey:@"DateRaised"])
    {
        //Convert String to date
        NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] trimSpaces]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date1 = [dateFormat dateFromString:dateStr];
        
        //Convert Date to string
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
        NSString *strDate = [dateFormat2 stringFromDate:date1];
        
        //Convert String to Date
        NSString *dateStr2 = strDate;
        
        NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
        [dateFormat3 setDateFormat:@"dd-MM-yyyy"];
        
        NSDate *date2 = [dateFormat3 dateFromString:dateStr2];
        // int daysToAdd = 30;
        // NSDate *newDate1 = [date2 dateByAddingTimeInterval:60*60*24*daysToAdd];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 30;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *dateToBeIncremented = [theCalendar dateByAddingComponents:dayComponent toDate:date2 options:0];
        
        //Convert Date to string
        NSDateFormatter *newdateFormat = [[NSDateFormatter alloc] init];
//        [newdateFormat setDateFormat:@"yyyy-MM-dd"];
        [newdateFormat setDateFormat:@"dd-MM-yyyy"];

        strExpiry_Date = [newdateFormat stringFromDate:dateToBeIncremented];
        
        //NSLog(@"New Date - %@",strExpiry_Date);
    }
    
    NSString *strDeliveryRun = [dictHeaderDetails objectForKey:@"DeliveryRun"];
    NSString *strCarrier = [dictHeaderDetails objectForKey:@"CARRIER"];
    
    if(strWarehouse.length <= 0)
    {
        strWarehouse = [[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"];
    }
    
    //Set status = 1
    strStatus = STATUS;
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        BOOL isSomethingWrongHappened = FALSE;
        FMResultSet *rs;
        @try
        {
            
            rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea_offline WHERE QUOTE_NO = ?",strQuoteNum];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            if ([rs next]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Quote Already Exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                [rs close];
                
                return;
            }
            
            [rs close];
            
            //           BOOL y2 = [db executeUpdate:@"INSERT INTO `soquohea_offline` (`QUOTE_NO`, `DEBTOR`, `DEL_NAME`, `DEL_ADDRESS1`, `DEL_ADDRESS2`, `DEL_ADDRESS3`, `BRANCH`,`DEL_DATE`,`DEL_COUNTRY`,`DEL_INST1`,`DEL_INST2`,`DEL_POST_CODE`,`RELEASE_TYPE`,`STATUS`,`WAREHOUSE`,`isFinalised`,`isUploadedToServer`,ACTIVE,CHARGE_RATE,CHARGE_TYPE,DATE_RAISED,DROP_SEQ,DIRECT,EDIT_STATUS,EXCHANGE_CODE,EXCHANGE_RATE,EXPORT_DEBTOR,HELD,NUM_BOXES,PERIOD_RAISED,PRICE_CODE,RELEASE_TYPE,SALESMAN,SALES_BRANCH,TRADING_TERMS,YEAR_RAISED,EXPIRY_DATE,DELIVERY_RUN,CARRIER) VALUES (?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?,?,?)", strQuoteNum, strDebtor, strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,@"S",strStatus,strWarehouse,strisFinalised,0,strActive,strChargeRate,strChargetype,strDateRaised,strDropSequence,strDirect,strEditStatus,strExchangeCode,strExchangeRate,strExportDebtor,strHeld,strNumboxes,strPeriodRaised,strPriceCode,strReleaseType,strSalesman,strSalesBranch,strTradingTerms,strYearRaised,strExpiry_Date,strDeliveryRun,strCarrier];
            //RELEASE_TYPE
            BOOL y2 = [db executeUpdate:@"INSERT INTO `soquohea_offline` (`QUOTE_NO`, `DEBTOR`, `DEL_NAME`, `DEL_ADDRESS1`, `DEL_ADDRESS2`, `DEL_ADDRESS3`, `BRANCH`,`DEL_DATE`,`DEL_COUNTRY`,`DEL_INST1`,`DEL_INST2`,`DEL_POST_CODE`,`STATUS`,`WAREHOUSE`,`isFinalised`,`isUploadedToServer`,ACTIVE,CHARGE_RATE,CHARGE_TYPE,DATE_RAISED,DROP_SEQ,DIRECT,EDIT_STATUS,EXCHANGE_CODE,EXCHANGE_RATE,EXPORT_DEBTOR,HELD,NUM_BOXES,PERIOD_RAISED,PRICE_CODE,RELEASE_TYPE,SALESMAN,SALES_BRANCH,TRADING_TERMS,YEAR_RAISED,EXPIRY_DATE,DELIVERY_RUN,CARRIER) VALUES (?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,?,?, ?, ?, ?,?,?,?,?,?)", strQuoteNum, strDebtor, strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strStatus,strWarehouse,strisFinalised,0,strActive,strChargeRate,strChargetype,strDateRaised,strDropSequence,strDirect,strEditStatus,strExchangeCode,strExchangeRate,strExportDebtor,strHeld,strNumboxes,strPeriodRaised,strPriceCode,strReleaseType,strSalesman,strSalesBranch,strTradingTerms,strYearRaised,strExpiry_Date,strDeliveryRun,strCarrier];
            
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            NSString *strStockCode = @"";
            NSString *strPrice = @"";
            NSString *strCost = @"0";
            NSString *strDescription = @"";
            NSString *strExtension = @"";
            NSString *strQuantityOrdered = @"0";
            NSString *strWeight = @"0";
            NSString *strVolume = @"0";
            NSString *strTax = @"0";
            NSString *strGross = @"0";
            
            float totalWeight = 0;
            float totalVolume = 0;
            float totalTax = 0;
            
            NSString *strLineNumber = @"0";
            
            NSString *strConvFactor = @"1.000";
            NSString *strOrigCost = @"0";
            
            int i = 0;
            
            NSString *strDissection = @"0";
            NSString *strDissection_Cos = @"0";
            
            int detail_lines = 0;
            float totalCost = 0.0;
            
            for (NSDictionary *dict in arrProducts)
            {
                
                strLineNumber = [NSString stringWithFormat:@"%d",++i];
                
                BOOL y1;
                //Comments
                if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])
                {
                    detail_lines++;
                    
                    strStockCode = @"/C";
                    strDescription = [[dict objectForKey:@"Description"] trimSpaces];
                    
                    strDissection = @"/C";
                    strDissection_Cos = @"";
                    
                    y1 = [db executeUpdate:@"INSERT INTO `soquodet_offline` (`QUOTE_NO`, `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,STATUS,EDIT_STATUS,`isUploadedToServer`,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",strQuoteNum,strDebtor,strDescription, @"0", strStockCode,@"0",strLineNumber,@"0",@"0",@"0",@"0",@"1.000",@"0",@"0",@"0",@"0",strStatus,strEditStatus,[NSNumber numberWithInt:0],strBranch,@"0",strPriceCode,strDissection,strDissection_Cos,strWarehouse];
                    
                    
                }
                else{
                    
                    strExtension = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ExtnPrice"]];
                    if(strExtension.length > 0)
                    {
                        detail_lines++;
                        totalCost += [strExtension floatValue];
                        
                        strStockCode = [dict objectForKey:@"StockCode"];
                        strPrice = [dict objectForKey:@"SellingPrice"];
                        strCost = [dict objectForKey:@"AverageCost"];
                        
                        strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                        
                        strWeight = [dict objectForKey:@"Weight"];
                        strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                        
                        totalWeight += [strWeight floatValue];
                        
                        strVolume = [dict objectForKey:@"Volume"];
                        strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                        
                        totalVolume += [strVolume floatValue];
                        
                        strTax = [dict objectForKey:@"Tax"];
                        strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
                        
                        totalTax += [strTax floatValue];
                        
                        strDescription = [dict objectForKey:@"Description"];
                        
                        
                        strGross = [dict objectForKey:@"Gross"];
                        
                        strDissection = [dict objectForKey:@"Dissection"];
                        strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
                        
                        y1 = [db executeUpdate:@"INSERT INTO `soquodet_offline` (`QUOTE_NO`, `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,STATUS,EDIT_STATUS,`isUploadedToServer`,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",strQuoteNum,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,strStatus,strEditStatus,[NSNumber numberWithInt:0],strBranch,@"2",strPriceCode,strDissection,strDissection_Cos,strWarehouse];
                    }
                    
                }
            }
            
            if (!y1)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
            NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
            NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
            NSString *strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
            NSString *strDetailLines = [NSString stringWithFormat:@"%d",detail_lines];
            
            BOOL x = [db executeUpdate:@"UPDATE `soquohea_offline` SET DETAIL_LINES = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ?,QUOTE_VALUE = ? WHERE QUOTE_NO = ?", strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strTotalCost,strQuoteNum];
            
            if (!x)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            // Store the view is profile order.
            [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isQuoteCreated"];
            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForQuote"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            *rollback = NO;
            
            NSString *strSuccessMessage = [NSString stringWithFormat:@"Quote Successfully Added!\nYou can view the quote under Offline Quote section"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = TAG_50;
            [alert show];
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
            NSLog(@"%@",strErrorMessage);
            
            // Store the view is profile order.
            [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"isQuoteCreated"];
            [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"QuantityForQuote"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        
    }];
    
}

-(void)callSaveEditQuoteToDB:(int)finalizeTag
{
    strWebserviceType = @"DB_QUOTE_SAVE_EDIT";
    
    //self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
    
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strDelAddress1 = @"";
    NSString *strDelAddress2 = @"";
    NSString *strDelAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = @"";
    NSString *strBranch = @"";
    NSString *strDelDate = @"";
    
    
    NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Debtor"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelName"]) {
        strDelName= [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelName"] trimSpaces]];
        
    }
  
    if ([dictHeaderDetails objectForKey:@"DEL_NAME"]) {
            strDelName= [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DEL_NAME"] trimSpaces]];
            
        
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelAddress1"]) {
        strDelAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress1"] trimSpaces]];
    }
    
    else
    {
        strDelAddress1 = strAddress1;
    }

    if ([dictHeaderDetails objectForKey:@"DelAddress2"]) {
        strDelAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress2"] trimSpaces]];
    }
    
    else
    {
        strDelAddress2 = strAddress2;
    }

    
    if ([dictHeaderDetails objectForKey:@"DelAddress3"]) {
        strDelAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelAddress3"] trimSpaces]];
    }
    else
    {
        strDelAddress3 = strAddress3;
    }
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Suburb"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelPostCode"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelCountry"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"ReleaseType"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Status"]) {
        strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Warehouse"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Branch"]) {
        strBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Branch"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDelDate = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DeliveryDate"] trimSpaces]];
    }
    
    NSString *strContact = @"";
    if ([dictHeaderDetails objectForKey:@"Contact"]) {
        strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
    }
    
    NSString *strPriceCode = @"";
    if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
    }
    
    NSString *strSalesman = @"";
    if ([dictHeaderDetails objectForKey:@"Salesman"]) {
        strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
    }
    
    NSString *strSalesBranch = @"";
    if ([dictHeaderDetails objectForKey:@"SalesBranch"]) {
        strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SalesBranch"]];
    }
    
    NSString *strTradingTerms = @"";
    if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
        strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
    }
    
    NSString *strExchangeCode = @"";
    if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
        strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
    }
    
    NSString *strChargetype = @"";
    if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
        strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
    }
    
    NSString *strExportDebtor = @"";
    if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
        strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
    }
    
    NSString *strChargeRate = @"";
    if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
        strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
    }
    
    NSString *strHeld = @"";
    if ([dictHeaderDetails objectForKey:@"Held"]) {
        strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
    }
    
    NSString *strNumboxes = @"";
    if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
        strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
    }
    
    NSString *strDirect = @"";
    if ([dictHeaderDetails objectForKey:@"Direct"]) {
        strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
    }
    
    NSString *strDateRaised = @"";
    if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
    }
    
    NSString *strPeriodRaised = @"";
    if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
        strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
    }
    
    NSString *strYearRaised = @"";
    if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
        strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
    }
    
    NSString *strDropSequence = @"";
    if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
        strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
    }
    
    NSString *strDeliveryDate = @"";
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
    }
    
    NSString *strDeliveryRun = @"";
    
    if([dictHeaderDetails objectForKey:@"DeliveryRun"])
    {
        strDeliveryRun  = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryRun"]];
    }
    
    //Set status = 1
    strStatus = STATUS;
    
    NSString *strActive = @"N";
    NSString *strEditStatus = @"11";
    NSString *strExchangeRate = @"1.000000";
    
    NSString *createdDate = [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:[NSDate date]];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
        BOOL isSomethingWrongHappened = FALSE;
        FMResultSet *rs;
        @try
        {
            
            //-Chnaged to Quote Received
            rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strQuoteNum];
            //rs = [db executeQuery:@"SELECT QUOTE_NO FROM soquohea WHERE QUOTE_NO = ?",strQuoteNumRecived];
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            BOOL y2 = FALSE;
            if ([rs next]) {
                //-Chnaged to Quote Received
                
                y2 = [db executeUpdate:@"UPDATE `soquohea` SET `QUOTE_NO` = ?, `DEBTOR` = ? , `DEL_NAME` = ? , `DEL_ADDRESS1`= ? , `DEL_ADDRESS2`= ? , `DEL_ADDRESS3` = ?, `BRANCH` = ?,`DEL_DATE` = ? ,`DEL_COUNTRY`= ?,`DEL_INST1`= ?,`DEL_INST2`= ?,`DEL_POST_CODE`= ?,`RELEASE_TYPE`= ?,`STATUS`= ?,`WAREHOUSE`= ?,`isFinalised` = ?,`isUploadedToServer` = ?,modified_date = ?,CONTACT = ?,PRICE_CODE = ?,SALESMAN = ?,SALES_BRANCH = ?,TRADING_TERMS = ?,EXCHANGE_CODE = ?,CHARGE_TYPE = ?,EXPORT_DEBTOR = ?,CHARGE_RATE = ?,HELD = ?,NUM_BOXES = ?,DIRECT = ?,DATE_RAISED = ?,PERIOD_RAISED = ?,YEAR_RAISED = ?,DROP_SEQ = ?,DEL_DATE = ?,EXCHANGE_RATE = ?,ACTIVE = ?,EDIT_STATUS = ?,DELIVERY_RUN = ? WHERE QUOTE_NO = ?", strQuoteNum, strDebtor, strDelName,strDelAddress1,strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strReleaseType,strStatus,strWarehouse,strisFinalised,[NSNumber numberWithInt:0],createdDate,strContact,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strExchangeCode,strChargetype,strExportDebtor,strChargeRate,strHeld,strNumboxes,strDirect,strDateRaised,strPeriodRaised,strYearRaised,strDropSequence,strDeliveryDate,strExchangeRate,strActive,strEditStatus,strDeliveryRun,strQuoteNum];
            }
            else{
                
                if([dictHeaderDetails objectForKey:@"DateRaised"])
                {
                    //Convert String to date
                    NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] trimSpaces]];
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *date1 = [dateFormat dateFromString:dateStr];
                    
                    //Convert Date to string
                    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                    [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
                    NSString *strDate = [dateFormat2 stringFromDate:date1];
                    
                    //Convert String to Date
                    NSString *dateStr2 = strDate;
                    
                    NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
                    [dateFormat3 setDateFormat:@"dd-MM-yyyy"];
                    
                    NSDate *date2 = [dateFormat3 dateFromString:dateStr2];
                    // int daysToAdd = 30;
                    // NSDate *newDate1 = [date2 dateByAddingTimeInterval:60*60*24*daysToAdd];
                    
                    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
                    dayComponent.day = 30;
                    
                    NSCalendar *theCalendar = [NSCalendar currentCalendar];
                    NSDate *dateToBeIncremented = [theCalendar dateByAddingComponents:dayComponent toDate:date2 options:0];
                    
                    //Convert Date to string
                    NSDateFormatter *newdateFormat = [[NSDateFormatter alloc] init];
                    [newdateFormat setDateFormat:@"dd-MM-yyyy"];
                    if(strExpiry_Date == nil || strExpiry_Date.length <=0)
                    {
                        strExpiry_Date = [newdateFormat stringFromDate:dateToBeIncremented];
                    }
                    
                    //NSLog(@"New Date - %@",strExpiry_Date);
                }
           
//                NSString *strQuery = [NSString stringWithFormat:@"INSERT OR REPLACE INTO soquohea (QUOTE_NO, DEBTOR, DEL_NAME, DEL_ADDRESS1, DEL_ADDRESS2, DEL_ADDRESS3,BRANCH,DEL_DATE,DEL_COUNTRY,DEL_INST1,DEL_INST2,DEL_POST_CODE,RELEASE_TYPE,STATUS,WAREHOUSE,isFinalised,isUploadedToServer,created_date,modified_date,ACTIVE,CHARGE_RATE,CHARGE_TYPE,DATE_RAISED,DROP_SEQ,DIRECT,EDIT_STATUS,EXCHANGE_CODE,EXCHANGE_RATE,EXPORT_DEBTOR,HELD,NUM_BOXES,PERIOD_RAISED,PRICE_CODE,RELEASE_TYPE,SALESMAN,SALES_BRANCH,TRADING_TERMS,YEAR_RAISED,EXPIRY_DATE,DELIVERY_RUN) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")", strQuoteNum, strDebtor, strDelName,[strDelAddress1 stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strReleaseType,strStatus,strWarehouse,strisFinalised,@"0",createdDate,createdDate,strActive,strChargeRate,strChargetype,strDateRaised,strDropSequence,strDirect,strEditStatus,strExchangeCode,strExchangeRate,strExportDebtor,strHeld,strNumboxes,strPeriodRaised,strPriceCode,strReleaseType,strSalesman,strSalesBranch,strTradingTerms,strYearRaised,strExpiry_Date,strDeliveryRun];
                
                y2 = [db executeUpdate:@"INSERT OR REPLACE INTO soquohea (QUOTE_NO, DEBTOR, DEL_NAME, DEL_ADDRESS1, DEL_ADDRESS2, DEL_ADDRESS3,BRANCH,DEL_DATE,DEL_COUNTRY,DEL_INST1,DEL_INST2,DEL_POST_CODE,RELEASE_TYPE,STATUS,WAREHOUSE,isFinalised,isUploadedToServer,created_date,modified_date,ACTIVE,CHARGE_RATE,CHARGE_TYPE,DATE_RAISED,DROP_SEQ,DIRECT,EDIT_STATUS,EXCHANGE_CODE,EXCHANGE_RATE,EXPORT_DEBTOR,HELD,NUM_BOXES,PERIOD_RAISED,PRICE_CODE,RELEASE_TYPE,SALESMAN,SALES_BRANCH,TRADING_TERMS,YEAR_RAISED,EXPIRY_DATE,DELIVERY_RUN) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", strQuoteNum, strDebtor, strDelName,[strDelAddress1 stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],strDelAddress2,strDelAddress3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strReleaseType,strStatus,strWarehouse,strisFinalised,@"0",createdDate,createdDate,strActive,strChargeRate,strChargetype,strDateRaised,strDropSequence,strDirect,strEditStatus,strExchangeCode,strExchangeRate,strExportDebtor,strHeld,strNumboxes,strPeriodRaised,strPriceCode,strReleaseType,strSalesman,strSalesBranch,strTradingTerms,strYearRaised,strExpiry_Date,strDeliveryRun];
                
            }
            
            [rs close];
            
            
            if (!y2)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            
            float totalWeight = 0;
            float totalVolume = 0;
            float totalTax = 0;
            
            NSString *strLineNumber = @"0";
            
            NSString *strConvFactor = @"1.000";
            NSString *strOrigCost = @"0";
            
            int detail_lines = 0;
            float totalCost = 0.0;
            NSString *strDissection = @"";
            NSString *strDissection_Cos = @"";
            
            int i = 0;
            for (NSDictionary *dict in arrProducts) {
                
                NSString *strStockCode = nil;
                NSString *strPrice = @"0";
                NSString *strCost = @"0";
                NSString *strDescription = nil;
                NSString *strExtension = @"0";
                NSString *strQuantityOrdered = @"0";
                NSString *strWeight = @"0";
                NSString *strVolume = @"0";
                NSString *strTax = @"0";
                NSString *strGross = @"0";
                
                
                
                //Comments
                if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])
                {
                    strLineNumber = [NSString stringWithFormat:@"%d",++i];
                    detail_lines++;
                    
                    strStockCode = @"C";
                    strDescription = [[dict objectForKey:@"Description"] trimSpaces];
                    
                    strDissection = @"C";
                    strDissection_Cos = @"";
                    
                    
                    
                    
                    FMResultSet *rs3 =  [db executeQuery:@"SELECT QUOTE_NO FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strStockCode];
                    // NSLog(@"strQuoteNumRecived %@",strQuoteNumRecived);
                    BOOL y = FALSE;
                    //Update
                    if ([rs3 next]){
                        
                        
                        y = [db executeUpdate:@"UPDATE `soquodet` SET `QUOTE_NO` = ?, `DEBTOR`= ?, `DESCRIPTION`= ?, `EXTENSION`= ?, `ITEM`= ?, `PRICE`= ?,LINE_NO = ?,QUANTITY = ?,ALT_QTY = ?,WEIGHT = ?,VOLUME = ?,CONV_FACTOR = ?,COST = ?,ORIG_COST = ?,GROSS = ?,TAX = ?, STATUS = ?,EDIT_STATUS = ?,`isUploadedToServer`= ?,modified_date = ? WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,strStatus,strEditStatus,[NSNumber numberWithInt:0],createdDate,strQuoteNumRecived,strStockCode];
                        
                    }
                    else{
                        
                        
                        
                        
                        NSString *strQuery = [NSString stringWithFormat:@"INSERT INTO soquodet (QUOTE_NO, DEBTOR, DESCRIPTION, EXTENSION, ITEM, PRICE,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,isUploadedToServer,created_date,modified_date,STATUS,EDIT_STATUS,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",strQuoteNum,strDebtor,[[strDescription stringByReplacingOccurrencesOfString:@"\"" withString:@"'"] trimSpaces], strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,@"0",createdDate,createdDate,strStatus,strEditStatus,strBranch,@"0",strPriceCode,strDissection,strDissection_Cos,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
                        
                        
                        
                        /*
                         y = [db executeUpdate:@"INSERT INTO `soquodet` (`QUOTE_NO`, `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,`isUploadedToServer`,created_date,modified_date,STATUS,EDIT_STATUS,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",strQuoteNum,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,[NSNumber numberWithInt:0],createdDate,createdDate,strStatus,strEditStatus,strBranch,@"0",strPriceCode,strDissection,strDissection_Cos,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
                         */
                        
                        y = [db executeUpdate:strQuery];
                        
                    }
                    
                    [rs3 close];
                    
                    if (!y)
                    {
                        isSomethingWrongHappened = TRUE;
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        
                    }
                    
                }
                
                //--Products Add only if hey have extension
                else
                {
                    if([dict objectForKey:@"ExtnPrice"])
                    {
                        strLineNumber = [NSString stringWithFormat:@"%d",++i];
                        strExtension = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ExtnPrice"]];
                        
                        
                        
                        strDissection_Cos = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Dissection_Cos"]];
                        
                        strDissection = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Dissection"]];
                        
                        totalCost += [strExtension floatValue];
                        
                        strStockCode = [dict objectForKey:@"StockCode"];
                        strPrice = [dict objectForKey:@"SellingPrice"];
                        strCost = [dict objectForKey:@"AverageCost"];
                        
                        strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                        
                        strWeight = [dict objectForKey:@"Weight"];
                        strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                        
                        totalWeight += [strWeight floatValue];
                        
                        strVolume = [dict objectForKey:@"Volume"];
                        strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                        
                        totalVolume += [strVolume floatValue];
                        
                        strTax = [dict objectForKey:@"Tax"];
                        strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
                        
                        totalTax += [strTax floatValue];
                        
                        strDescription = [dict objectForKey:@"Description"];
                        
                        
                        strGross = [dict objectForKey:@"Gross"];
                        //
                        //                        FMResultSet *rs3 =  [db executeQuery:@"SELECT QUOTE_NO FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNumRecived,strStockCode];
                        
                        
                        FMResultSet *rs3 =  [db executeQuery:@"SELECT QUOTE_NO FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strStockCode];
                        
                        // NSLog(@"strQuoteNumRecived %@",strQuoteNumRecived);
                        BOOL y = FALSE;
                        //Update
                        if ([rs3 next]){
                            
                            //--Changed the Quote Number Received from Server
                            
                            
                            y = [db executeUpdate:@"UPDATE `soquodet` SET `QUOTE_NO` = ?, `DEBTOR`= ?, `DESCRIPTION`= ?, `EXTENSION`= ?, `ITEM`= ?, `PRICE`= ?,LINE_NO = ?,QUANTITY = ?,ALT_QTY = ?,WEIGHT = ?,VOLUME = ?,CONV_FACTOR = ?,COST = ?,ORIG_COST = ?,GROSS = ?,TAX = ?, STATUS = ?,EDIT_STATUS = ?,`isUploadedToServer`= ?,modified_date = ? WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNum,strDebtor,[[strDescription stringByReplacingOccurrencesOfString:@"\"" withString:@"'"] trimSpaces], strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,strStatus,strEditStatus,[NSNumber numberWithInt:0],createdDate,strQuoteNumRecived,strStockCode];
                            
                        }
                        else{
                            
                            
                            //                            NSString *strQ = [NSString stringWithFormat:@"INSERT INTO soquodet (QUOTE_NO, DEBTOR, DESCRIPTION, EXTENSION, ITEM, PRICE,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,isUploadedToServer,created_date,modified_date,STATUS,EDIT_STATUS,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",strQuoteNum,strDebtor,strDescription, strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,@"0",createdDate,createdDate,strStatus,strEditStatus,strBranch,@"2",strPriceCode,strDissection,strDissection_Cos,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
                            
                            
                            y = [db executeUpdate:@"INSERT INTO soquodet (QUOTE_NO, DEBTOR, DESCRIPTION, EXTENSION, ITEM, PRICE,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,isUploadedToServer,created_date,modified_date,STATUS,EDIT_STATUS,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS,WAREHOUSE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",strQuoteNum,strDebtor,[[strDescription stringByReplacingOccurrencesOfString:@"\"" withString:@"'"] trimSpaces], strExtension, strStockCode,strPrice,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,strConvFactor,strCost,strOrigCost,strGross,strTax,@"0",createdDate,createdDate,strStatus,strEditStatus,strBranch,@"2",strPriceCode,strDissection,strDissection_Cos,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]]];
                            
                        }
                        
                        [rs3 close];
                        
                        if (!y)
                        {
                            isSomethingWrongHappened = TRUE;
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            
                        }
                    }
                }
            }
            
            NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
            NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
            NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
            NSString *strDetailLines = [NSString stringWithFormat:@"%d",detail_lines];
            
            BOOL x = [db executeUpdate:@"UPDATE `soquohea` SET DETAIL_LINES = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ?,QUOTE_VALUE = ? WHERE QUOTE_NO = ?", strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strQuoteNum,[NSString stringWithFormat:@"%f",totalCost]];
            
            if (!x)
            {
                isSomethingWrongHappened = TRUE;
                
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                
            }
            
            *rollback = NO;
            
            
            
            
            NSString *strSuccessMessage = [NSString stringWithFormat:@"Quote Successfully Added!\n You can view the Quote under Quote --> Quote History Enquiry."];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = TAG_50;
            [alert show];
            
            
        }
        @catch (NSException* e) {
            *rollback = YES;
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exception" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            
        }
        
    }];
    
}


-(void)callGetNewQuoteNumberFromDB{
    
    strWebserviceType = @"DB_GET_QUOTE_NUMBER";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *salesType = [prefs stringForKey:@"salesType"];
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db)  {
        
        FMResultSet *rs;
        @try {
            
            //SELECT MAX(CAST(substr(QUOTE_NO,4) AS INT)) FROM soquohea  WHERE QUOTE_NO LIKE 'RNJ%'
            
            
            int len = [salesType length];
            
            
            //rs = [db executeQuery:@"SELECT MAX(last_quote_num) as lastQuoteNum FROM (SELECT CAST(substr(QUOTE_NO,?) AS INT) as last_quote_num FROM soquohea_offline WHERE QUOTE_NO LIKE ?)",[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", salesType]];
            
            rs = [db executeQuery:@"SELECT MAX(QUOTE_NO) as lastQuoteNum FROM (SELECT CAST(substr(QUOTE_NO,?) AS INT) as QUOTE_NO FROM soquohea WHERE QUOTE_NO LIKE ?)",[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", salesType]];
            
            
            //rs = [db executeQuery:@"SELECT MAX(last_quote_num) as lastQuoteNum FROM (SELECT CAST(substr(QUOTE_NO,?) AS INT) as last_quote_num FROM soquohea WHERE QUOTE_NO LIKE ? UNION SELECT CAST(substr(QUOTE_NO,?) AS INT) as last_quote_num FROM soquohea_offline WHERE QUOTE_NO LIKE ?)",[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", salesType],[NSNumber numberWithInt:len+1],[NSString stringWithFormat:@"%@%%", salesType]];
            
            
            if (!rs)
            {
                [rs close];
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
            }
            
            int numNewQuoteNum;
            if ([rs next]) {
                
                
                if ([[[rs resultDictionary] objectForKey:@"lastQuoteNum"] isEqual:[NSNull null]]) {
                    numNewQuoteNum = 1;
                }
                else{
                    int numLastQuoteNum = [[[rs resultDictionary] objectForKey:@"lastQuoteNum"] intValue];
                    numNewQuoteNum  = ++numLastQuoteNum;
                }
                
                
            }
            //No record
            else{
                numNewQuoteNum = 1;
            }
            
            [rs close];
            
            self.strQuoteNum = [NSString stringWithFormat:@"%@%d",salesType,numNewQuoteNum];
            self.strQuoteNumRecived = [NSString stringWithFormat:@"%@%d",salesType,numNewQuoteNum];
            [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
            
            NSLog(@"dictHeaderDetails==%@",dictHeaderDetails);
            
            [tblHeader reloadData];
            
        }
        @catch (NSException* e) {
            
            // rethrow if not one of the two exceptions above
            
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
        
    }];
}

-(void)callViewQuoteDetailsHeaderFromDB:(FMDatabase *)db{
    FMResultSet *rs1;
    @try {
        
        
        //        rs1 = [db executeQuery:@"SELECT DELIVERY_RUN as DeliveryRun,CARRIER as CARRIER,PRICE_CODE as PriceCode,BRANCH as Branch, DEBTOR as Debtor,DEL_ADDRESS1 as Address1,DEL_ADDRESS2 as Address2,DEL_ADDRESS3 as Address3,DEL_COUNTRY as Country,DEL_NAME as Name,DEL_POST_CODE as PostCode,DEL_SUBURB as Suburb,WAREHOUSE as Warehouse,DEL_INST1 as DelInst1, DEL_INST2 as DelInst2,RELEASE_TYPE as ReleaseType,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,QUOTE_VALUE as Total,STATUS as Status,isFinalised from soquohea where QUOTE_NO=?",strQuoteNumRecived];
        
        rs1 = [db executeQuery:@"SELECT SALESMAN as Salesman,EXCHANGE_CODE,CHARGE_RATE,CHARGE_TYPE,DEL_NAME,SALES_BRANCH,DATE_RAISED as DateRaised,EXPIRY_DATE,Del_Date as DeliveryDate,DELIVERY_RUN as DeliveryRun,CARRIER as CARRIER,PRICE_CODE as PriceCode,BRANCH as Branch, DEBTOR as Debtor,DEL_ADDRESS1 as Address1,DEL_ADDRESS2 as Address2,DEL_ADDRESS3 as Address3,DEL_COUNTRY as Country,DEL_NAME as Name,DEL_POST_CODE as PostCode,DEL_SUBURB as Suburb,WAREHOUSE as Warehouse,DEL_INST1 as DelInst1, DEL_INST2 as DelInst2,RELEASE_TYPE as ReleaseType,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,QUOTE_VALUE as Total,STATUS as Status,isFinalised from soquohea where QUOTE_NO=?",strQuoteNumRecived];
        
        strQuoteNum = strQuoteNumRecived;
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next])
        {
            
            self.dictHeaderDetails = [rs1 resultDictionary];
            
            //Check the given quote finalised or not
            if (EditableMode == YES) {
                if ([dictHeaderDetails objectForKey:@"Debtor"]) {
                    self.strDebtorNum = [dictHeaderDetails objectForKey:@"Debtor"];
                }
                if ([dictHeaderDetails objectForKey:@"EXPIRY_DATE"]) {
                    strExpiry_Date = [dictHeaderDetails objectForKey:@"EXPIRY_DATE"];
                }
                
                if([dictHeaderDetails objectForKey:@"DeliveryDate"])
                {
                    strDeliveryDate = [dictHeaderDetails objectForKey:@"DeliveryDate"];
                }
                
                if ([[NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"isFinalised"]] isEqualToString:@"1"]) {
                    chkFinalize = YES;
                    btnAddMenu.hidden = YES;
                    btnAddProducts.hidden = YES;
                    btnEmail.hidden = NO;
                    btnPrint.hidden = NO;
                }
                else{
                    chkFinalize = NO;
                    btnAddMenu.hidden = NO;
                    btnAddProducts.hidden = NO;
                    btnEmail.hidden = YES;
                    btnPrint.hidden = YES;
                }
            }
        }
        
        [rs1 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs1 close];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
    }
    
    
}

-(void)callViewQuoteDetailsDetailsFromDB:(FMDatabase *)db{
    NSLog(@"strQuoteNumRecived %@",strQuoteNumRecived);
    strWebserviceType = @"DB_QUOTE_DETAILS_DETAILS";
    
    [arrProducts removeAllObjects];
    
    FMResultSet *rs2;
    
    @try {
        
            rs2 = [db executeQuery:@"SELECT a.DISSECTION as Dissection,a.DISSECTION_COS as Dissection_Cos,a.QUOTE_NO, a.LINE_NO as LineNo, a.ITEM as StockCode, a.DESCRIPTION as Description, a.STATUS as Status,a.PRICE as SellingPrice,a.DISC_PERCENT as DiscPercent,a.EXTENSION as ExtnPrice,a.COST as AverageCost,a.WEIGHT as Weight,a.VOLUME as Volume,a.TAX as Tax,a.QUANTITY as QuantityOrdered,a.GROSS as Gross,b.AVAILABLE as Available,b.ALLOCATED as Allocated,b.WAREHOUSE as Warehouse FROM soquodet a LEFT OUTER JOIN samaster b ON a.ITEM = b.CODE WHERE a.QUOTE_NO = ? AND a.isDeleted = 0 AND a.STATUS = ? AND trim(b.WAREHOUSE) = ?",strQuoteNumRecived,STATUS,[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces]];
        
        
        if (!rs2)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs2 next]) {
            NSDictionary *dictData = [rs2 resultDictionary];
            [dictData setValue:@"0" forKey:@"isNewProduct"];
            [arrProducts addObject:dictData];
        }
        
        [rs2 close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs2 close];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
    }
    
    
    
}

-(void)callViewQuoteDetailsFromDB:(FMDatabase *)db
{
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue  inDatabase:^(FMDatabase *db) {
        
        
        //Get Header details
        FMResultSet *rs1 = [[AppDelegate getAppDelegateObj].database executeQuery:@"SELECT DEBTOR as Debtor,QUOTE_NO as QuoteNo,DATE_RAISED as QuoteDate,STATUS as Status,DEL_NAME as Name,BRANCH as Branch,QUOTE_VALUE as Total from soquohea where QUOTE_NO=? AND STATUS = ?",strQuoteNumRecived,STATUS];
        
        if ([rs1 next])
        {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Branch"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp1 forKey:@"Branch"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"Debtor"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp2 forKey:@"Debtor"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Name"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp3 forKey:@"Name"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"QuoteDate"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp4 forKey:@"QuoteDate"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"QuoteNo"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp5 forKey:@"QuoteNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp6 forKey:@"Status"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"Total"] forKey:@"text"];
            [dictHeaderDetails setObject:dictTemp7 forKey:@"Total"];
            
            
        }
        
        [rs1 close];
        //Get Details details
        FMResultSet *rs2 = [db executeQuery:@"SELECT QUOTE_NO, LINE_NO as LineNo, ITEM as Item, DESCRIPTION as Description, PRICING_UNIT as PricingUnit, STATUS as Status,PRICE as Price,DISC_PERCENT as DiscPercent,EXTENSION as Extension FROM soquodet WHERE QUOTE_NO = ? AND STATUS = ?",strQuoteNumRecived,STATUS];
        
        
        while ([rs2 next]) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs2 resultDictionary];
            
            NSMutableDictionary *dictTemp1 = [[NSMutableDictionary alloc] init];
            [dictTemp1 setObject:[dictData objectForKey:@"Description"] forKey:@"text"];
            [dict setObject:dictTemp1 forKey:@"Description"];
            
            NSMutableDictionary *dictTemp2 = [[NSMutableDictionary alloc] init];
            [dictTemp2 setObject:[dictData objectForKey:@"DiscPercent"] forKey:@"text"];
            [dict setObject:dictTemp2 forKey:@"DiscPercent"];
            
            NSMutableDictionary *dictTemp3 = [[NSMutableDictionary alloc] init];
            [dictTemp3 setObject:[dictData objectForKey:@"Extension"] forKey:@"text"];
            [dict setObject:dictTemp3 forKey:@"Extension"];
            
            NSMutableDictionary *dictTemp4 = [[NSMutableDictionary alloc] init];
            [dictTemp4 setObject:[dictData objectForKey:@"Item"] forKey:@"text"];
            [dict setObject:dictTemp4 forKey:@"Item"];
            
            NSMutableDictionary *dictTemp5 = [[NSMutableDictionary alloc] init];
            [dictTemp5 setObject:[dictData objectForKey:@"LineNo"] forKey:@"text"];
            [dict setObject:dictTemp5 forKey:@"LineNo"];
            
            NSMutableDictionary *dictTemp6 = [[NSMutableDictionary alloc] init];
            [dictTemp6 setObject:[dictData objectForKey:@"Price"] forKey:@"text"];
            [dict setObject:dictTemp6 forKey:@"Price"];
            
            NSMutableDictionary *dictTemp7 = [[NSMutableDictionary alloc] init];
            [dictTemp7 setObject:[dictData objectForKey:@"PricingUnit"] forKey:@"text"];
            [dict setObject:dictTemp7 forKey:@"PricingUnit"];
            
            NSMutableDictionary *dictTemp8 = [[NSMutableDictionary alloc] init];
            [dictTemp8 setObject:[dictData objectForKey:@"Status"] forKey:@"text"];
            [dict setObject:dictTemp8 forKey:@"Status"];
            
            [arrProducts addObject:dict];
            
        }
        [rs2 close];
        
    }];
    
    [tblHeader reloadData];
    
}

-(void)callDeleteProductFromDB:(FMDatabase*)db andIndexCountTempEdit:(int)indexCountTempEdit
{
    //NSLog(@"indexCountTempEdit==%d",indexCountTempEdit);
    
    @try {
        
        [db executeUpdate:@"DELETE FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNumRecived,[[arrProducts objectAtIndex:indexCountTempEdit]objectForKey:@"StockCode"]];
        
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Local DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

#pragma mark - WS call
-(void)callWSGetNewQuoteNumber{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *salesType = [prefs stringForKey:@"salesType"];
    
    strWebserviceType = @"WS_GET_QUOTE_NUMBER";
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:GetQuoteNo soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "<quoteno>"
                            "<TYPE>%@</TYPE>"
                            "</quoteno>"
                            "</wws:GetQuoteNo>",salesType];
    
    NSLog(@"New Quote Mumber : soapXMLStr %@",soapXMLStr);
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#GetQuoteNo" onView:self.view];
    
}

/*===============================Oringal Soap Request============================
 -(void)callWSSaveQuote:(int)finalizeTag
 {
 strWebserviceType = @"WS_QUOTE_SAVE";
 
 self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
 
 NSLog(@"dictHeaderDetails %@",dictHeaderDetails);
 NSString *strDebtor = @"";
 NSString *strDelName = @"";
 NSString *strAddress1 = @"";
 NSString *strAddress2 = @"";
 NSString *strAddress3 = @"";
 NSString *strSubUrb = @"";
 NSString *strDelPostCode = @"";
 NSString *strDelCountry = @"";
 NSString *strReleaseType = @"";
 NSString *strDelInst1 = @"";
 NSString *strDelInst2 = @"";
 NSString *strStatus = @"";
 NSString *strWarehouse = [[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"];
 NSString *strBranch = @"";
 NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
 NSString *strCarrier = [dictHeaderDetails objectForKey:@"CARRIER"];
 
 if([dictHeaderDetails objectForKey:@"DateRaised"])
 {
 //Convert String to date
 NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] trimSpaces]];
 
 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
 [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
 
 NSDate *date1 = [dateFormat dateFromString:dateStr];
 
 //Convert Date to string
 NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
 [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
 NSString *strDate = [dateFormat2 stringFromDate:date1];
 
 //Convert String to Date
 NSString *dateStr2 = strDate;
 
 NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
 [dateFormat3 setDateFormat:@"dd-MM-yyyy"];
 
 NSDate *date2 = [dateFormat3 dateFromString:dateStr2];
 // int daysToAdd = 30;
 // NSDate *newDate1 = [date2 dateByAddingTimeInterval:60*60*24*daysToAdd];
 
 NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
 dayComponent.day = 30;
 
 NSCalendar *theCalendar = [NSCalendar currentCalendar];
 NSDate *dateToBeIncremented = [theCalendar dateByAddingComponents:dayComponent toDate:date2 options:0];
 
 //Convert Date to string
 NSDateFormatter *newdateFormat = [[NSDateFormatter alloc] init];
 [newdateFormat setDateFormat:@"yyyy-MM-dd"];
 strExpiry_Date = [newdateFormat stringFromDate:dateToBeIncremented];
 
 //NSLog(@"New Date - %@",strExpiry_Date);
 }
 
 if ([dictHeaderDetails objectForKey:@"Debtor"]) {
 strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"DelName"]) {
 strDelName= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelName"]];
 
 }
 
 if ([dictHeaderDetails objectForKey:@"Address1"]) {
 strAddress1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address1"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"Address2"]) {
 strAddress2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address2"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"Address3"]) {
 strAddress3 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address3"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"Suburb"]) {
 strSubUrb = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Suburb"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
 strDelPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelPostCode"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
 strDelCountry = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelCountry"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
 strReleaseType = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ReleaseType"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
 strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst1"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
 strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst2"]];
 }
 
 if ([dictHeaderDetails objectForKey:@"Status"]) {
 strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
 }
 
 NSString *strContact = @"";
 if ([dictHeaderDetails objectForKey:@"Contact"]) {
 strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
 }
 
 NSString *strPriceCode = @"";
 if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
 strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
 }
 
 NSString *strSalesman = @"";
 if ([dictHeaderDetails objectForKey:@"Salesman"]) {
 strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
 }
 
 NSString *strSalesBranch = @"";
 if ([dictHeaderDetails objectForKey:@"SalesBranch"]) {
 strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SalesBranch"]];
 }
 
 NSString *strTradingTerms = @"";
 if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
 strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
 }
 
 NSString *strExchangeCode = @"";
 if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
 strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
 }
 
 NSString *strChargetype = @"";
 if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
 strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
 }
 
 NSString *strExportDebtor = @"";
 if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
 strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
 }
 
 NSString *strChargeRate = @"";
 if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
 strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
 }
 
 NSString *strHeld = @"";
 if ([dictHeaderDetails objectForKey:@"Held"]) {
 strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
 }
 
 NSString *strNumboxes = @"";
 if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
 strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
 }
 
 NSString *strDirect = @"";
 if ([dictHeaderDetails objectForKey:@"Direct"]) {
 strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
 }
 
 NSString *strDateRaised = @"";
 if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
 strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
 }
 
 NSString *strPeriodRaised = @"";
 if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
 strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
 }
 
 NSString *strYearRaised = @"";
 if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
 strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
 }
 
 NSString *strDropSequence = @"";
 if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
 strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
 }
 
 NSString *strDeliveryDate = @"";
 if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
 strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
 }
 
 if([dictHeaderDetails objectForKey:@"Branch"])
 {
 strBranch = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Branch"]];
 }
 
 NSString *strActive = @"N";
 NSString *strEditStatus = @"11";
 NSString *strExchangeRate = @"1.000000";
 
 
 NSString *strStockCode = @"";
 NSString *strPrice = @"0";
 NSString *strCost = @"0";
 NSString *strDescription = @"";
 NSString *strExtension = @"0";
 NSString *strItem = @"";
 NSString *strQuantityOrdered = @"0";
 NSString *strWeight = @"0";
 NSString *strVolume = @"0";
 NSString *strTax = @"0";
 NSString *strGross = @"0";
 NSString *strDeliveryRun = [dictHeaderDetails objectForKey:@"DeliveryRun"];
 
 float totalVolume = 0;
 float totalWeight = 0;
 float totalTax = 0;
 
 NSString *strLineNumber = @"0";
 NSString *strDissection = @"";
 NSString *strDissection_Cos = @"";
 NSString *strDecimal_Places = @"";
 
 int i = 0;
 int detail_lines = 0;
 float totalCost = 0.0;
 for (NSDictionary *dict in arrProducts)
 {
 
 strLineNumber = [NSString stringWithFormat:@"%d",++i];
 
 //Comments
 if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])
 {
 detail_lines++;
 strStockCode = @"/C";
 strDescription = [[dict objectForKey:@"Description"] trimSpaces];
 strDissection_Cos = @"";
 strDissection = @"/C";
 strDecimal_Places = @"0";
 strItem = [strItem stringByAppendingFormat:@"<item><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>0</PRICE><EXTENSION>0</EXTENSION><LINE_NO>%@</LINE_NO><QUANTITY>0</QUANTITY><ALT_QTY>0</ALT_QTY><WEIGHT>0</WEIGHT><VOLUME>0</VOLUME><CONV_FACTOR>0</CONV_FACTOR><COST>0</COST><ORIG_COST>0</ORIG_COST><GROSS>0</GROSS><TAX>0</TAX><EDIT_STATUS>11</EDIT_STATUS><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><PRICE_CODE>%@</PRICE_CODE><DECIMAL_PLACES>%@</DECIMAL_PLACES><WAREHOUSE>%@</WAREHOUSE><BRANCH>%@</BRANCH></item>",strStockCode,strDescription,strLineNumber,strDissection,strDissection_Cos,strPriceCode,strDecimal_Places,strWarehouse,strBranch];
 
 }
 else
 {
 strExtension = [dict objectForKey:@"ExtnPrice"];
 if (strExtension.length > 0)
 {
 detail_lines++;
 totalCost += [strExtension floatValue];
 
 strStockCode = [dict objectForKey:@"StockCode"];
 strCost = [dict objectForKey:@"AverageCost"];
 strPrice  = [dict objectForKey:@"SellingPrice"];
 
 strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
 
 strWeight = [dict objectForKey:@"Weight"];
 strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
 
 totalWeight += [strWeight floatValue];
 
 strVolume = [dict objectForKey:@"Volume"];
 strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
 
 totalVolume += [strVolume floatValue];
 
 strTax = [dict objectForKey:@"Tax"];
 strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
 
 totalTax += [strTax floatValue];
 
 strDescription = [dict objectForKey:@"Description"];
 
 
 strGross = [dict objectForKey:@"Gross"];
 
 strDissection = [dict objectForKey:@"Dissection"];
 strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
 strDecimal_Places = @"2";
 
 strItem = [strItem stringByAppendingFormat:@"<item><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><LINE_NO>%@</LINE_NO><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><CONV_FACTOR>%@</CONV_FACTOR><COST>%@</COST><ORIG_COST>%@</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><EDIT_STATUS>11</EDIT_STATUS><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><PRICE_CODE>%@</PRICE_CODE><DECIMAL_PLACES>%@</DECIMAL_PLACES><WAREHOUSE>%@</WAREHOUSE><BRANCH>%@</BRANCH></item>",strStockCode,strDescription,strPrice,strExtension,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,@"1.000",strCost,@"0.00",strGross,strTax,strDissection,strDissection_Cos,strPriceCode,strDecimal_Places,strWarehouse,strBranch];
 }
 
 }
 
 }
 
 strStatus = @"1";
 
 NSString *strDetailLines = [NSString stringWithFormat:@"%d",detail_lines];
 NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
 NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
 NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
 NSString *strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
 
 
 NSString *soapXMLStr = [NSString stringWithFormat:
 @"<wws:InsertDebtorDetails soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
 "<debtorHeader xsi:type=\"wws:DebtorRequest\">"
 "<QUOTE_NO>%@</QUOTE_NO>"
 "<DEBTOR>%@</DEBTOR>"
 "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"
 "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"
 "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"
 "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"
 "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"
 "<DEL_POST_CODE>%@</DEL_POST_CODE>"
 "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"
 "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"
 "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"
 "<STATUS>%@</STATUS>"
 "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
 "<BRANCH><![CDATA[%@]]></BRANCH>"
 "<isFinalised>%@</isFinalised>"
 "<DETAIL_LINES><![CDATA[%@]]></DETAIL_LINES>"
 "<WEIGHT><![CDATA[%@]]></WEIGHT>"
 "<VOLUME><![CDATA[%@]]></VOLUME>"
 "<TAX><![CDATA[%@]]></TAX>"
 "<TAX_AMOUNT1><![CDATA[%@]]></TAX_AMOUNT1>"
 "<DATE_RAISED><![CDATA[%@]]></DATE_RAISED>"
 "<DEL_DATE><![CDATA[%@]]></DEL_DATE>"
 "<CONTACT><![CDATA[%@]]></CONTACT>"
 "<DIRECT><![CDATA[%@]]></DIRECT>"
 "<SALESMAN><![CDATA[%@]]></SALESMAN>"
 "<SALES_BRANCH><![CDATA[%@]]></SALES_BRANCH>"
 "<TRADING_TERMS><![CDATA[%@]]></TRADING_TERMS>"
 "<PRICE_CODE><![CDATA[%@]]></PRICE_CODE>"
 "<EXCHANGE_CODE><![CDATA[%@]]></EXCHANGE_CODE>"
 "<CHARGE_TYPE><![CDATA[%@]]></CHARGE_TYPE>"
 "<EXPORT_DEBTOR><![CDATA[%@]]></EXPORT_DEBTOR>"
 "<DROP_SEQ><![CDATA[%@]]></DROP_SEQ>"
 "<HELD><![CDATA[%@]]></HELD>"
 "<CHARGE_RATE><![CDATA[%@]]></CHARGE_RATE>"
 "<NUM_BOXES><![CDATA[%@]]></NUM_BOXES>"
 "<EXCHANGE_RATE><![CDATA[%@]]></EXCHANGE_RATE>"
 "<ACTIVE><![CDATA[%@]]></ACTIVE>"
 "<EDIT_STATUS><![CDATA[%@]]></EDIT_STATUS>"
 "<PERIOD_RAISED><![CDATA[%@]]></PERIOD_RAISED>"
 "<YEAR_RAISED><![CDATA[%@]]></YEAR_RAISED>"
 "<EXPIRY_DATE><![CDATA[%@]]></EXPIRY_DATE>"
 "<RELEASE_TYPE><![CDATA[S]]></RELEASE_TYPE>"
 "<CARRIER><![CDATA[%@]]></CARRIER>"
 "<QUOTE_VALUE><![CDATA[%@]]></QUOTE_VALUE>"
 "<DELIVERY_RUN><![CDATA[%@]]></DELIVERY_RUN>"
 "<STATUS_MODE><![CDATA[0]]></STATUS_MODE>"
 "</debtorHeader>"
 "<debtorDetails xsi:type=\"wws:DebtorDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
 "%@"
 "</debtorDetails>"
 "</wws:InsertDebtorDetails>",strQuoteNum,strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strDelPostCode,strDelCountry,strDelInst1,strDelInst2,strStatus,strWarehouse,strBranch,strisFinalised,strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strDateRaised,strDeliveryDate,strContact,strDirect,strSalesman,strSalesBranch,strTradingTerms,strPriceCode,strExchangeCode,strChargetype,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,strExchangeRate,strActive,strEditStatus,strPeriodRaised,strYearRaised,strExpiry_Date,strCarrier,strTotalCost,strDeliveryRun,strItem];
 
 NSLog(@"soapXMLStr %@",soapXMLStr);
 
 spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
 [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
 [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#insertDebtorDetails" onView:self.view];
 }
 ==================================================================================
 */


-(void)callWSSaveQuote:(int)finalizeTag
{
    strWebserviceType = @"WS_QUOTE_SAVE";
    
    self.strQuoteNum = [dictHeaderDetails objectForKey:@"QuoteNum"];
    
    NSLog(@"dictHeaderDetails %@",dictHeaderDetails);
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = [[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"];
    NSString *strBranch = @"";
//    NSString *strisFinalised = [NSString stringWithFormat:@"%d",finalizeTag];
//    NSString *strCarrier = [dictHeaderDetails objectForKey:@"CARRIER"];
    
    if([dictHeaderDetails objectForKey:@"DateRaised"])
    {
        //Convert String to date
        NSString *dateStr = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DateRaised"] trimSpaces]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date1 = [dateFormat dateFromString:dateStr];
        
        //Convert Date to string
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setDateFormat:@"dd-MM-yyyy"];
        NSString *strDate = [dateFormat2 stringFromDate:date1];
        
        //Convert String to Date
        NSString *dateStr2 = strDate;
        
        NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
        [dateFormat3 setDateFormat:@"dd-MM-yyyy"];
        
        NSDate *date2 = [dateFormat3 dateFromString:dateStr2];
        // int daysToAdd = 30;
        // NSDate *newDate1 = [date2 dateByAddingTimeInterval:60*60*24*daysToAdd];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 30;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *dateToBeIncremented = [theCalendar dateByAddingComponents:dayComponent toDate:date2 options:0];
        
        //Convert Date to string
        NSDateFormatter *newdateFormat = [[NSDateFormatter alloc] init];
        [newdateFormat setDateFormat:@"dd-MM-yyyy"];
        strExpiry_Date = [newdateFormat stringFromDate:dateToBeIncremented];
        
        //NSLog(@"New Date - %@",strExpiry_Date);
    }
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelName"]) {
        strDelName= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelName"]];
        
    }
    
    
    if ([dictHeaderDetails objectForKey:@"DEL_NAME"]) {
        strDelName= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_NAME"]];
        
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Address3"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Suburb"]];
        if(strSubUrb.length > 15)
        {
            strSubUrb = [strSubUrb substringToIndex:14];
        }
    }
    
    if ([dictHeaderDetails objectForKey:@"DelPostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelPostCode"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelCountry"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelCountry"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ReleaseType"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst1"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DelInst2"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Status"]) {
        strStatus = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Status"]];
    }
    
    NSString *strContact = @"";
    if ([dictHeaderDetails objectForKey:@"Contact"]) {
        strContact = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Contact"]];
    }
    
    NSString *strPriceCode = @"";
    if ([dictHeaderDetails objectForKey:@"PriceCode"]) {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
    }
    
    NSString *strSalesman = @"";
    if ([dictHeaderDetails objectForKey:@"Salesman"]) {
        strSalesman = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Salesman"]];
    }
    
    NSString *strSalesBranch = @"";
    if ([dictHeaderDetails objectForKey:@"SALES_BRANCH"]) {
        strSalesBranch= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"SALES_BRANCH"]];
    }
    
    NSString *strTradingTerms = @"";
    if ([dictHeaderDetails objectForKey:@"TradingTerms"]) {
        strTradingTerms = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"TradingTerms"]];
    }
    
    NSString *strExchangeCode = @"";
    if ([dictHeaderDetails objectForKey:@"ExchangeCode"]) {
        strExchangeCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExchangeCode"]];
    }
    
    NSString *strChargetype = @"";
    if ([dictHeaderDetails objectForKey:@"Chargetype"]) {
        strChargetype = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Chargetype"]];
    }
    
    NSString *strExportDebtor = @"";
    if ([dictHeaderDetails objectForKey:@"ExportDebtor"]) {
        strExportDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ExportDebtor"]];
    }
    
    NSString *strChargeRate = @"";
    if ([dictHeaderDetails objectForKey:@"ChargeRate"]) {
        strChargeRate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ChargeRate"]];
    }
    
    NSString *strHeld = @"";
    if ([dictHeaderDetails objectForKey:@"Held"]) {
        strHeld = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Held"]];
    }
    
    NSString *strNumboxes = @"";
    if ([dictHeaderDetails objectForKey:@"Numboxes"]) {
        strNumboxes = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Numboxes"]];
    }
    
    NSString *strDirect = @"";
    if ([dictHeaderDetails objectForKey:@"Direct"]) {
        strDirect= [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Direct"]];
    }
    
    NSString *strDateRaised = @"";
    if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
    }
    
    NSString *strPeriodRaised = @"";
    if ([dictHeaderDetails objectForKey:@"PeriodRaised"]) {
        strPeriodRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PeriodRaised"]];
    }
    
    NSString *strYearRaised = @"";
    if ([dictHeaderDetails objectForKey:@"YearRaised"]) {
        strYearRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"YearRaised"]];
    }
    
    NSString *strDropSequence = @"";
    if ([dictHeaderDetails objectForKey:@"DropSequence"]) {
        strDropSequence = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DropSequence"]];
    }
    
    NSString *strDeliveryDate = @"";
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
    }
    
    if([dictHeaderDetails objectForKey:@"Branch"])
    {
        strBranch = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Branch"]];
    }
    
    if([dictHeaderDetails objectForKey:@"DEL_NAME"])
    {
        strDelName = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DEL_NAME"]];
    }
    if([dictHeaderDetails objectForKey:@"CHARGE_TYPE"])
    {
        strChargetype  = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_TYPE"]];
    }
    
    if([dictHeaderDetails objectForKey:@"CHARGE_RATE"])
    {
        strChargeRate  = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_TYPE"]];
    }
    
    if([dictHeaderDetails objectForKey:@"EXCHANGE_CODE"])
    {
        strExchangeCode  = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CHARGE_TYPE"]];
    }
    
    
//    NSString *strActive = @"N";
//    NSString *strEditStatus = @"11";
    NSString *strExchangeRate = @"1.000000";
    
    
    NSString *strStockCode = @"";
    NSString *strPrice = @"0";
    NSString *strCost = @"0";
    NSString *strDescription = @"";
    NSString *strExtension = @"0";
    NSString *strItem = @"";
    NSString *strQuantityOrdered = @"0";
    NSString *strWeight = @"0";
    NSString *strVolume = @"0";
    NSString *strTax = @"0";
    NSString *strGross = @"0";
    NSString *strDeliveryRun ;
    
    if([dictHeaderDetails objectForKey:@"DeliveryRun"])
    {
        strDeliveryRun  = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryRun"]];
    }
    
    
    NSString *strCustorderNo =@"";
    
    if ([dictHeaderDetails objectForKey:@"CustOrderno"]) {
        strCustorderNo = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"CustOrderno"]];
    }
    
    float totalVolume = 0;
    float totalWeight = 0;
    float totalTax = 0;
    
    NSString *strLineNumber = @"0";
    NSString *strDissection = @"";
    NSString *strDissection_Cos = @"";
    NSString *strDecimal_Places = @"";
    
    int i = 0;
    int detail_lines = 0;
    float totalCost = 0.0;
    
    
    for (NSDictionary *dict in arrProducts)
    {
        
        strLineNumber = [NSString stringWithFormat:@"%d",++i];
        
        //Comments
        if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])
        {
            
            //DEL_DATE
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
            NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
            [dateFormatter setLocale:enUSPOSIXLocale];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
            strDeliveryDate = [dateFormatter stringFromDate:dateDel];
            
            detail_lines++;
            strStockCode = @"/C";
            strDescription = [[dict objectForKey:@"Description"] trimSpaces];
            strDissection_Cos = @"";
            strDissection = @"/C";
            strDecimal_Places = @"0";
            //==============Check fro HTML TAGS //==============
            strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
            strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
            strCustorderNo =[[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strCustorderNo];
            //==============Check fro HTML TAGS //==============

            strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM/><QUOTE_NO/><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>0</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>0</PRICE><TAX>0</TAX> <DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS> <DELIVERY_DATE>%@</DELIVERY_DATE><LINE_TYPE>s</LINE_TYPE><COMPONENT_PRINT>s</COMPONENT_PRINT><COMPONENT_HISTORY>s</COMPONENT_HISTORY><SPARE_NUM_1>0.0000</SPARE_NUM_1><SPARE_NUM_2>0.0000</SPARE_NUM_2><SPARE_NUM_3>0.0000</SPARE_NUM_3><SPARE_NUM_4>0.0000</SPARE_NUM_4><SPARE_NUM_5>0.0000</SPARE_NUM_5><SPARE_NUM_6>0.0000</SPARE_NUM_6> <SPARE_DATE_1>2014-09-09T07:20:26+00:00</SPARE_DATE_1><SPARE_STR>N</SPARE_STR></DETAIL>",[strCustorderNo trimSpaces],[strLineNumber trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],strStockCode,[strDescription trimSpaces],[strPriceCode trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strDeliveryDate trimSpaces]];
            
        }
        else
        {
            
            if ([[dict objectForKey:@"ExtnPrice"] isKindOfClass:[NSNumber class]]) {
                id ext = [dict objectForKey:@"ExtnPrice"];
                strExtension = [NSString stringWithFormat:@"%@",[ext stringValue]];
            }
            else
                strExtension = [dict objectForKey:@"ExtnPrice"];
            if (strExtension.length > 0)
            {
                detail_lines++;
                totalCost += [strExtension floatValue];
                
                
                strCost = [dict objectForKey:@"AverageCost"];
                strPrice  = [dict objectForKey:@"SellingPrice"];
                
                strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                
                strWeight = [dict objectForKey:@"Weight"];
                strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                
                totalWeight += [strWeight floatValue];
                
                strVolume = [dict objectForKey:@"Volume"];
                strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                
                totalVolume += [strVolume floatValue];
                
                strTax = [dict objectForKey:@"Tax"];
                strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
                
                totalTax += [strTax floatValue];
                
                strDescription = [dict objectForKey:@"Description"];
                
                strGross = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Gross"]];
                
                strDissection = [dict objectForKey:@"Dissection"];
                strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
                strDecimal_Places = @"2";
                
                //DEL_DATE
                
                if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
                    strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
                }
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
                [dateFormatter setLocale:enUSPOSIXLocale];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
                strDeliveryDate = [dateFormatter stringFromDate:dateDel];
                
                if([strQuantityOrdered isKindOfClass:[NSNumber class]])
                {
                    //NSNumber *qty = [strQuantityOrdered integerValue];
                    strQuantityOrdered = [NSString stringWithFormat:@"%d",[strQuantityOrdered integerValue]];
                }
                
                if([strPrice isKindOfClass:[NSNumber class]])
                {
                    //NSNumber *qty = [strQuantityOrdered integerValue];
                    strPrice = [NSString stringWithFormat:@"%f",[strPrice doubleValue]];
                }
                
                //--Comment
                strStockCode = [dict objectForKey:@"StockCode"];
                if([strStockCode isEqualToString:@"C"])
                {
                    strStockCode = @"/C";
                    strDissection = @"/C";
                    strDecimal_Places = @"0";
                }
                
                //==============Check fro HTML TAGS //==============
                strDescription = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
                strStockCode = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];
                //==============Check fro HTML TAGS //==============

                
                strItem = [strItem stringByAppendingFormat:@"<DETAIL><SUB_SET_NUM/><QUOTE_NO/><CUST_ORDER>%@</CUST_ORDER><LINE_NUMBER>%@</LINE_NUMBER><DEBTOR>%@</DEBTOR><BRANCH>%@</BRANCH><WAREHOUSE>%@</WAREHOUSE><ITEM>%@</ITEM><DESCRIPTION>%@</DESCRIPTION><QUANTITY>%@</QUANTITY><PRICE_CODE>%@</PRICE_CODE><PRICE>%@</PRICE><TAX>%@</TAX> <DISC_PERCENT>0.00</DISC_PERCENT><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS> <DELIVERY_DATE>%@</DELIVERY_DATE><LINE_TYPE>s</LINE_TYPE><COMPONENT_PRINT>s</COMPONENT_PRINT><COMPONENT_HISTORY>s</COMPONENT_HISTORY><SPARE_NUM_1>0.0000</SPARE_NUM_1><SPARE_NUM_2>0.0000</SPARE_NUM_2><SPARE_NUM_3>0.0000</SPARE_NUM_3><SPARE_NUM_4>0.0000</SPARE_NUM_4><SPARE_NUM_5>0.0000</SPARE_NUM_5><SPARE_NUM_6>0.0000</SPARE_NUM_6> <SPARE_DATE_1>2014-09-09T07:20:26+00:00</SPARE_DATE_1><SPARE_STR>N</SPARE_STR></DETAIL>",strCustorderNo,[strLineNumber trimSpaces],[strDebtor trimSpaces],[strBranch trimSpaces],[strWarehouse trimSpaces],[strStockCode trimSpaces],[strDescription trimSpaces],[strQuantityOrdered trimSpaces],[strPriceCode trimSpaces],[strPrice trimSpaces],[strTax trimSpaces],[strDissection trimSpaces],[strDissection_Cos trimSpaces],[strDeliveryDate trimSpaces]];
            }
            
        }
        
    }
    
    
    NSString *strCustOrder = @"";
    if (txtCustOrder) {
        strCustOrder = txtCustOrder.text;
    }
    NSString *iso8601String = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSLocale *enUSPOSIXLocale = [NSLocale currentLocale]; // 12thNov2014
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    
    iso8601String = [dateFormatter stringFromDate:now];
    
    //--Date Raised
    if ([dictHeaderDetails objectForKey:@"DateRaised"]) {
        strDateRaised = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DateRaised"]];
        
    }
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateRaised = [dateFormatter dateFromString:strDateRaised];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    strDateRaised = [dateFormatter stringFromDate:dateRaised];
    
    //Expirary Date
    if(strExpiry_Date == nil)
    {
        strExpiry_Date = [dictHeaderDetails objectForKey:@"EXPIRY_DATE"];
    }
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateExpirary = [dateFormatter dateFromString:strExpiry_Date];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    strExpiry_Date = [dateFormatter stringFromDate:dateExpirary];
    
    if ([dictHeaderDetails objectForKey:@"DeliveryDate"]) {
        strDeliveryDate = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"DeliveryDate"]];
    }
    
    
    //DEL_DATE
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateDel = [dateFormatter dateFromString:strDeliveryDate];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    strDeliveryDate = [dateFormatter stringFromDate:dateDel];
    
    strCustOrder = @"";
    
    //==============Check fro HTML TAGS //==============
    strAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress1];
    strAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress2];
    strAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress3];
    strDelInst2 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    strDelInst1 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    strSubUrb = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strSubUrb];
    strDelName =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    strContact = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strContact];
    //==============Check fro HTML TAGS //==============
    strCustorderNo =[[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strCustorderNo];
    //==============Check fro HTML TAGS //==============
    
 
    
    
    NSString *strquote = [NSString stringWithFormat:
                          @"<Quote>"
                          "<HEADER>"
                          "<OVERWRITE_SQ>s</OVERWRITE_SQ>"
                          "<INC_PRICE>Y</INC_PRICE>"
                          "<INC_WAREHOUSE>s</INC_WAREHOUSE>"
                          "<DELIVERY_ADDRESS_TYPE>s</DELIVERY_ADDRESS_TYPE>"
                          "<DELIV_NUM>1000.00</DELIV_NUM>"
                          "<SUB_SET_NUM/>"
                          "<SUPPLIED_LINE_NUMBERS>s</SUPPLIED_LINE_NUMBERS>"
                          "<QUOTE_NO/>"
                          "<CALCULATETAX>s</CALCULATETAX>"
                          "<WEBORDERID>1000.00</WEBORDERID>"
                          "<APPLY_DELIVERY_CHARGE>s</APPLY_DELIVERY_CHARGE>"
                          "<CARRIER_CODE>zz</CARRIER_CODE>"
                          "<CASH_COLLECT></CASH_COLLECT>"
                          "<CASH_AMOUNT>0.00</CASH_AMOUNT>"
                          "<CASH_REP_PICKUP></CASH_REP_PICKUP>"
                          "<STOCK_RETURNS></STOCK_RETURNS>"
                          "<DEBTOR>%@</DEBTOR>"
                          "<BRANCH>%@</BRANCH>"
                          "<SALES_BRANCH>%@</SALES_BRANCH>"
                          "<DEL_NAME>%@</DEL_NAME>"
                          "<DEL_ADDRESS1>%@</DEL_ADDRESS1>"
                          "<DEL_ADDRESS2>%@</DEL_ADDRESS2>"
                          "<DEL_ADDRESS3>%@</DEL_ADDRESS3>"
                          "<DEL_SUBURB>%@</DEL_SUBURB>"
                          "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                          "<DEL_COUNTRY>%@</DEL_COUNTRY>"
                          "<DEL_INST1>%@</DEL_INST1>"
                          "<DEL_INST2>%@</DEL_INST2>"
                          "<CHARGE_TYPE>%@</CHARGE_TYPE>"
                          "<CHARGE_RATE>%@</CHARGE_RATE>"
                          "<DATE_RAISED>%@</DATE_RAISED>"
                          "<EXPIRY_DATE>%@</EXPIRY_DATE>"
                          "<DEL_DATE>%@</DEL_DATE>"
                          "<CUST_ORDER>%@</CUST_ORDER>"
                          "<CONTACT>%@</CONTACT>"
                          "<SALESMAN>%@</SALESMAN>"
                          "<WAREHOUSE>%@</WAREHOUSE>"
                          "<EXCHANGE_CODE>%@</EXCHANGE_CODE>"
                          "<EXCHANGE_RATE>%@</EXCHANGE_RATE>"
                          "<DISC_PERCENT>0.00</DISC_PERCENT>"
                          "<DELIVERY_RUN>%@</DELIVERY_RUN>"
                          "<JOB_NO></JOB_NO>"
                          "<SPARE_NUM_1>0.0000</SPARE_NUM_1>"
                          "<SPARE_NUM_2>0.0000</SPARE_NUM_2>"
                          "<SPARE_NUM_3>0.0000</SPARE_NUM_3>"
                          "<SPARE_NUM_4>0.0000</SPARE_NUM_4>"
                          "<SPARE_NUM_5>0.0000</SPARE_NUM_5>"
                          "<SPARE_NUM_6>0.0000</SPARE_NUM_6>"
                          "<SPARE_STR></SPARE_STR>"
                          "</HEADER>"
                          "%@"
                          "</Quote>",[strDebtor trimSpaces],[strBranch trimSpaces],[strSalesBranch trimSpaces],[strDelName trimSpaces],[strAddress1 trimSpaces],[strAddress2 trimSpaces],[strAddress3 trimSpaces],[strSubUrb trimSpaces],[strDelPostCode trimSpaces],[strDelCountry trimSpaces],[strDelInst1 trimSpaces],[strDelInst2 trimSpaces],[strChargetype trimSpaces],@"0",[strDateRaised trimSpaces],strExpiry_Date,strDeliveryDate,[strCustorderNo trimSpaces],[strContact trimSpaces],[strSalesman trimSpaces],[strWarehouse trimSpaces],[strExchangeCode trimSpaces],[strExchangeRate trimSpaces],[strDeliveryRun trimSpaces],strItem];
    
    
    
    
    /*
     // create request
     NSString *strRequest = @"http://mail.jlstewart.com.au:82/webservices/insert_modules.php";
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
     [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
     [request setHTTPShouldHandleCookies:NO];
     [request setTimeoutInterval:200];
     [request setHTTPMethod:@"POST"];
     
     // set Content-Type in HTTP header
     [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
     
     NSString *parameters = [NSString stringWithFormat:@"transactionType=%@", @"0"];
     if (strquote) {
     parameters = [NSString stringWithFormat:@"%@&xmlData=%@", parameters, strquote];
     }
     
     spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     NSData *body = [parameters dataUsingEncoding:NSUTF8StringEncoding];
     [request setHTTPBody:body];
     NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
     [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
     [request setURL:[NSURL URLWithString:strRequest]];
     
     [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
     completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
     @try {
     if(response)
     {
     [spinner removeFromSuperview];
     NSLog(@"REsponse:::%@",response);
     
     NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:data error:nil];
     NSLog(@"dictResponse:::%@",dictResponse);
     NSLog(@":::%@",[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"]);
     NSLog(@":::%@",[[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"]);
     if([[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"])
     {
     
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Quote generated. Quote number %@",[[[[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     alert = nil;
     }
     else
     {
     
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again. Quote could not generated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     alert = nil;
     
     }
     }
     if(error)
     {
     [spinner removeFromSuperview];
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again. Quote could not generated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     alert = nil;
     NSLog(@"errkr::%@",error.description);
     }
     
     }
     @catch (NSException *exception) {
     NSLog(@"catch:::%@",exception.description);
     [spinner removeFromSuperview];
     }
     }];
     */
    self.strQuoteNum = self.strQuoteNumRecived = [dictHeaderDetails objectForKey:@"QuoteNo"];
    
    // [self DeleteQuoteFromDatabase:self.strQuoteNum];
    //return;
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //NSString *strRequest = @"http://mail.jlstewart.com.au:82/webservices/insert_modules.php";
    
    NSString *strRequest=[AppDelegate getServiceURL:@"webservices_rcf/insert_modules.php"];
     NSURL *url = [NSURL URLWithString:[strRequest stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    ASIFormDataRequest *requestnew = [[ASIFormDataRequest alloc] initWithURL:url];
    [requestnew setDelegate:self];
    [requestnew setRequestMethod:@"POST"];
    [requestnew setTimeOutSeconds:200];
    [requestnew setPostValue:strquote forKey:@"xmlData"];
    [requestnew setPostValue:@"0" forKey:@"transactionType"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.strQuoteNum forKey:@"QuoteNumber"];
    requestnew.userInfo = dict;
    [requestnew startAsynchronous];
    dict = nil;
    
    //  NSLog(@"soapXMLStr %@",soapXMLStr);
    //
    //
    //  spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //
    //
    //  [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    //  [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#insertDebtorDetails" onView:self.view];
}
#pragma mark -- Quote Webservice Response
- (void)requestFinished:(ASIHTTPRequest *)requestParameter
{
    @try {
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:[requestParameter responseData] error:nil];
        NSLog(@"dictResponse:::%@",dictResponse);
        NSString *text = [[NSString alloc] initWithData:[requestParameter responseData] encoding:NSUTF8StringEncoding];
        NSLog(@"text:::%@",text);
        NSDictionary *userInfo=requestParameter.userInfo;
        
        if([[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"])
        {
            //      [NSString stringWithFormat:@"Quote generated. Quote number %@",[[[[[dictResponse objectForKey:@"ReturnValues"] objectForKey:@"ReturnDataTable"] objectForKey:@"Value"] objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]
            
            if([[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"])
            {
                [spinner removeFromSuperview];
                UIAlertView *aler = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Quotation Created: Quotation Number:%@",[[[[[dictResponse objectForKey:@"ReturnValues"]objectForKey:@"ReturnDataTable"]objectForKey:@"Value"]objectForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aler show];
                aler = nil;
                if([userInfo isKindOfClass:[NSDictionary class]])
                {
                    if([userInfo objectForKey:@"QuoteNumber"])
                    {
                        [self DeleteQuoteFromDatabase:[userInfo objectForKey:@"QuoteNumber"]];
                    }
                    else
                    {
                        
                        [self prepareForNewQuote];
                        //---Generate new quote locally
                        [self callGetNewQuoteNumberFromDB];
                        [tblHeader reloadData];
                    }
                }
               
                
            }
            
            else
            {
                [spinner removeFromSuperview];
                UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Quotation could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aler show];
                aler = nil;
            }
        }
        
        else
        {
            [spinner removeFromSuperview];
            UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Quotation could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [aler show];
            aler = nil;
        }
    }
    @catch (NSException *exception) {
        [spinner removeFromSuperview];
        UIAlertView  *aler = [[UIAlertView alloc]initWithTitle:@"" message:@"Quotation could not be generated. Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [aler show];
        aler = nil;
        
        NSLog(@"error::%@",exception.description);
    }
}

- (void)requestFailed:(ASIHTTPRequest *)requestParameter
{
    [spinner removeFromSuperview];
    NSLog(@"falied:::::");
    
}
-(void)callWSSaveQuoteForEdit:(int)finalizeTag
{
    
    NSLog(@"callWSSaveQuoteForEdit");
    
    strWebserviceType = @"WS_QUOTE_EDIT_SAVE";
    
    NSString *strDebtor = @"";
    NSString *strDelName = @"";
    NSString *strAddress1 = @"";
    NSString *strAddress2 = @"";
    NSString *strAddress3 = @"";
    NSString *strSubUrb = @"";
    NSString *strDelPostCode = @"";
    NSString *strDelCountry = @"";
    NSString *strReleaseType = @"";
    NSString *strDelInst1 = @"";
    NSString *strDelInst2 = @"";
    NSString *strStatus = @"";
    NSString *strWarehouse = @"";
    NSString *strBranch = @"";
    NSString *strisFinalised  = [NSString stringWithFormat:@"%d",finalizeTag];
    NSString *strCarrier = [dictHeaderDetails objectForKey:@"CARRIER"];
    NSString *strDeliveryRun = [dictHeaderDetails objectForKey:@"DeliveryRun"];
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        strDebtor = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Debtor"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Name"]) {
        strDelName= [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Name"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address1"]) {
        strAddress1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address2"]) {
        strAddress2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Address3"]) {
        strAddress3 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Address3"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Suburb"]) {
        strSubUrb = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Suburb"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"PostCode"]) {
        strDelPostCode = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"PostCode"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Country"]) {
        strDelCountry = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Country"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"ReleaseType"]) {
        strReleaseType = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"ReleaseType"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst1"]) {
        strDelInst1 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst1"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"DelInst2"]) {
        strDelInst2 = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"DelInst2"] trimSpaces]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Warehouse"]) {
        strWarehouse = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"Warehouse"]];
    }
    
    if ([dictHeaderDetails objectForKey:@"Branch"]) {
        strBranch = [NSString stringWithFormat:@"%@",[[dictHeaderDetails objectForKey:@"Branch"] trimSpaces]];
    }
    
    NSString *strPriceCode = @"";
    if ([dictHeaderDetails objectForKey:@"PriceCode"])
    {
        strPriceCode = [NSString stringWithFormat:@"%@",[dictHeaderDetails objectForKey:@"PriceCode"]];
    }
    
    
    //==============Check fro HTML TAGS //==============
    strDebtor = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDebtor];
    strDelName = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelName];
    strDelPostCode =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelPostCode];
    strWarehouse =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strWarehouse];
    strSubUrb =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strSubUrb];
    
    strAddress1 =    [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress1];
    strAddress2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress2];
    strAddress3 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strAddress3];
    
    strDelInst1 = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst1];
    strDelInst2 =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelInst2];
    strDeliveryRun =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDeliveryRun];

    strBranch = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strBranch];
    strReleaseType = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strReleaseType];
    strDelCountry = [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDelCountry];
    
    
    strStatus = STATUS;
    
    NSString *strStockCode = @"";
    NSString *strPrice = @"0";
    NSString *strCost = @"0";
    NSString *strDescription = @"";
    NSString *strExtension = @"";
    NSString *strItem = @"";
    NSString *strQuantityOrdered = @"0";
    NSString *strWeight = @"0";
    NSString *strVolume = @"0";
    NSString *strTax = @"0";
    NSString *strGross = @"0";
    
    float totalVolume = 0;
    float totalWeight = 0;
    float totalTax = 0;
    
    NSString *strLineNumber = @"0";
    NSString *strDecimal_Places = @"";
    
    int i = 0;
    int detail_lines = 0;
    float totalCost = 0.0;
    for (NSDictionary *dict in arrProducts) {
        
        strLineNumber = [NSString stringWithFormat:@"%d",++i];
        
        NSString *strDissection = @"0";
        NSString *strDissection_Cos = @"0";
        
        //Comments
        if ([dict objectForKey:@"Item"] && [[dict objectForKey:@"Item"] isEqualToString:@"/C"])
        {
            detail_lines++;
            strStockCode = @"/C";
            strDescription = [[dict objectForKey:@"Description"] trimSpaces];
            
            
            // Handle HTML tags
            strDescription =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];

            strDissection = @"/C";
            strDissection_Cos = @"";
            strDecimal_Places = @"0";
            
            strItem = [strItem stringByAppendingFormat:@"<item><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>0</PRICE><EXTENSION>0</EXTENSION><LINE_NO>%@</LINE_NO><QUANTITY>0</QUANTITY><ALT_QTY>0</ALT_QTY><WEIGHT>0</WEIGHT><VOLUME>0</VOLUME><CONV_FACTOR>0</CONV_FACTOR><COST>0</COST><ORIG_COST>0</ORIG_COST><GROSS>0</GROSS><TAX>0</TAX><EDIT_STATUS>11</EDIT_STATUS><BRANCH>%@</BRANCH><DECIMAL_PLACES>0</DECIMAL_PLACES><PRICE_CODE>%@</PRICE_CODE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><PRICE_CODE>%@</PRICE_CODE><DECIMAL_PLACES>%@</DECIMAL_PLACES><WAREHOUSE>%@</WAREHOUSE><BRANCH>%@</BRANCH></item>",strStockCode,strDescription,strLineNumber,strBranch,strPriceCode,strDissection,strDissection_Cos,strPriceCode,strDecimal_Places,strWarehouse,strBranch];
        }
        else
        {
            strExtension = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ExtnPrice"]];
            
            if(strExtension.length > 0)
            {
                detail_lines++;
                totalCost += [strExtension floatValue];
                
                if (![[dict objectForKey:@"StockCode"] isEqualToString:@"-"]) {
                    strStockCode = [dict objectForKey:@"StockCode"];
                }
                else if ([dict objectForKey:@"Item"]) {
                    strStockCode = [dict objectForKey:@"Item"];

                }
                
                if ([dict objectForKey:@"AverageCost"]) {
                    strCost = [dict objectForKey:@"AverageCost"];
                }
                
                strPrice  = [dict objectForKey:@"SellingPrice"];
                
                strQuantityOrdered = [dict objectForKey:@"QuantityOrdered"];
                
                strWeight = [dict objectForKey:@"Weight"];
                strWeight = [NSString stringWithFormat:@"%f",[strWeight floatValue]*[strQuantityOrdered intValue]];
                
                totalWeight += [strWeight floatValue];
                
                strVolume = [dict objectForKey:@"Volume"];
                strVolume = [NSString stringWithFormat:@"%f",[strVolume floatValue]*[strQuantityOrdered intValue]];
                
                totalVolume += [strVolume floatValue];
                
                strTax = [dict objectForKey:@"Tax"];
                strTax = [NSString stringWithFormat:@"%f",[strTax floatValue]*[strQuantityOrdered intValue]];
                
                totalTax += [strTax floatValue];
                
                strDescription = [dict objectForKey:@"Description"];
                strExtension = [dict objectForKey:@"ExtnPrice"];
                
                strDissection = [dict objectForKey:@"Dissection"];
                strDissection_Cos = [dict objectForKey:@"Dissection_Cos"];
                
                strGross = [dict objectForKey:@"Gross"];
                strDecimal_Places = @"2";
                
                
                // Handle HTML tags
                strDescription =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strDescription];
                strStockCode =  [[AppDelegate getAppDelegateObj]HandleHTMLCharacters:strStockCode];

                strItem = [strItem stringByAppendingFormat:@"<item><ITEM>%@</ITEM><DESCRIPTION><![CDATA[%@]]></DESCRIPTION><PRICE>%@</PRICE><EXTENSION>%@</EXTENSION><LINE_NO>%@</LINE_NO><QUANTITY>%@</QUANTITY><ALT_QTY>%@</ALT_QTY><WEIGHT>%@</WEIGHT><VOLUME>%@</VOLUME><CONV_FACTOR>%@</CONV_FACTOR><COST>%@</COST><ORIG_COST>%@</ORIG_COST><GROSS>%@</GROSS><TAX>%@</TAX><EDIT_STATUS>11</EDIT_STATUS><BRANCH>%@</BRANCH><DECIMAL_PLACES>2</DECIMAL_PLACES><PRICE_CODE>%@</PRICE_CODE><DISSECTION>%@</DISSECTION><DISSECTION_COS>%@</DISSECTION_COS><WAREHOUSE>%@</WAREHOUSE>+*</item>",strStockCode,strDescription,strPrice,strExtension,strLineNumber,strQuantityOrdered,strQuantityOrdered,strWeight,strVolume,@"1.000",strCost,@"0.00",strGross,strTax,strBranch,strPriceCode,strDissection,strDissection_Cos,strWarehouse];
            }
        }
    }
    
    strStatus = @"1";
    
    
    NSString *strDetailLines = [NSString stringWithFormat:@"%d",[arrProducts count]];
    NSString *strTotalTax = [NSString stringWithFormat:@"%f",totalTax];
    NSString *strTotalWeight = [NSString stringWithFormat:@"%f",totalWeight];
    NSString *strTotalVolume = [NSString stringWithFormat:@"%f",totalVolume];
    NSString *strTotalCost = [NSString stringWithFormat:@"%f",totalCost];
    
    NSString *soapXMLStr = [NSString stringWithFormat:
                            @"<wws:EditDebtorDetails soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                            "<debtorHeader xsi:type=\"wws:DebtorRequest\">"
                            "<QUOTE_NO>%@</QUOTE_NO>"
                            "<DEBTOR>%@</DEBTOR>"
                            "<DEL_NAME><![CDATA[%@]]></DEL_NAME>"
                            "<DEL_ADDRESS1><![CDATA[%@]]></DEL_ADDRESS1>"
                            "<DEL_ADDRESS2><![CDATA[%@]]></DEL_ADDRESS2>"
                            "<DEL_ADDRESS3><![CDATA[%@]]></DEL_ADDRESS3>"
                            "<DEL_SUBURB><![CDATA[%@]]></DEL_SUBURB>"
                            "<DEL_POST_CODE>%@</DEL_POST_CODE>"
                            "<DEL_COUNTRY><![CDATA[%@]]></DEL_COUNTRY>"
                            "<RELEASE_TYPE>S</RELEASE_TYPE>"
                            "<DEL_INST1><![CDATA[%@]]></DEL_INST1>"
                            "<DEL_INST2><![CDATA[%@]]></DEL_INST2>"
                            "<STATUS>%@</STATUS>"
                            "<WAREHOUSE><![CDATA[%@]]></WAREHOUSE>"
                            "<BRANCH><![CDATA[%@]]></BRANCH>"
                            "<isFinalised>%@</isFinalised>"
                            "<DETAIL_LINES><![CDATA[%@]]></DETAIL_LINES>"
                            "<WEIGHT><![CDATA[%@]]></WEIGHT>"
                            "<VOLUME><![CDATA[%@]]></VOLUME>"
                            "<TAX><![CDATA[%@]]></TAX>"
                            "<TAX_AMOUNT1><![CDATA[%@]]></TAX_AMOUNT1>"
                            "<CARRIER><![CDATA[%@]]></CARRIER>"
                            "<QUOTE_VALUE><![CDATA[%@]]></QUOTE_VALUE>"
                            "<DELIVERY_RUN><![CDATA[%@]]></DELIVERY_RUN>"
                            "</debtorHeader>"
                            "<debtorDetails xsi:type=\"wws:DebtorDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                            "%@"
                            "</debtorDetails>"
                            "</wws:EditDebtorDetails>",strQuoteNumRecived,strDebtor,strDelName,strAddress1,strAddress2,strAddress3,strSubUrb,strDelPostCode,strDelCountry,strDelInst1,strDelInst2,strStatus,strWarehouse,strBranch,strisFinalised,strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTax,strCarrier,strTotalCost,strDeliveryRun,strItem];
    
    
    NSLog(@"soapXMLStr %@",soapXMLStr);
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#insertDebtorDetails" onView:self.view];
}



-(void)callWSViewQuoteDetailsHeader{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DETAILS_HEADER";
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",strQuoteNumRecived];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DETAILS_HEADER_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

-(void)callWSViewQuoteDetailsDetails:(NSString*)quoteNum
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_QUOTE_DETAILS_DETAILS";
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",quoteNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QUOTE_DETAILS_DETAILS_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Custom Methods

- (void)hideOrDisableButtons{
    btnLoadProfile.enabled = FALSE;
    btnSaveDetails.enabled = FALSE;
    btnAddProducts.enabled = FALSE;
    btnFinalise.enabled = FALSE;
    btnAddMenu.enabled = FALSE;
}
- (void)showOrEnableButtons{
    btnLoadProfile.enabled = TRUE;
    btnSaveDetails.enabled = TRUE;
    btnAddProducts.enabled = TRUE;
    btnFinalise.enabled = TRUE;
    btnAddMenu.enabled = TRUE;
}
- (void)showPointingAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.autoreverses = YES;
    animation.duration = 0.35;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = HUGE_VALF;
    [layerPointingImage addAnimation:animation forKey:@"pulseAnimation"];
    [imgArrowDownwards.layer addSublayer:layerPointingImage];
}

- (void)doAfterDataFetched{
    if ([arrProducts count]) {
        
        if (EditableMode == YES ){
            if(chkFinalize == NO) {
                btnDelete.hidden = NO;
                tblDetails.editing = NO;
                [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
                
                NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
                UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
                [btnDelete setBackgroundImage:img forState:UIControlStateNormal];
            }
            else{
                btnDelete.hidden = YES;
            }
        }
        else{
            btnDelete.hidden = NO;
            tblDetails.editing = NO;
            [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
            
            NSString *strImage=[[NSBundle mainBundle] pathForResource:@"delete_button" ofType:@"png"];
            UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
            [btnDelete setBackgroundImage:img forState:UIControlStateNormal];
        }
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        //NSString *path = [AppDelegate getFileFromDocumentsDirectory:@"QuoteDetails.plist"];
        //[arrProducts writeToFile:path atomically:YES];
    }
    else{
        //No Products
        [vwContentSuperview addSubview:vwNoProducts];
        
        if (chkFinalize == NO) {
            [self showPointingAnimation];
        }
        
    }
    
}

-(void)doAfterdelete{
    [arrProducts removeObjectAtIndex:deleteRowIndex];
    [tblDetails deleteRowsAtIndexPaths:[NSArray arrayWithObject:deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tblDetails endUpdates];
    
    if ([arrProducts count] == 0) {
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        [self showOrEnableButtons];
        [self showPointingAnimation];
    }
    
}

- (void)setUpInitialView{
    [vwContentSuperview removeAllSubviews];
    [vwSegmentedControl addSubview:segControl];
    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    
}

-(void)deleteProductInEditableMode:(int)indexCount
{
    //NSLog(@"indexCount==%d",indexCount);
    
    if(![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected)
    {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
            
            @try {
                
                BOOL y = [db executeUpdate:@"UPDATE soquodet SET isDeleted = 1 WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNumRecived,[[arrProducts objectAtIndex:indexCount]objectForKey:@"StockCode"]];
                
                if (!y)
                {
                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                }
                
                [self doAfterdelete];
                
            }
            @catch (NSException* e) {
                // rethrow if not one of the two exceptions above
                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Local DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
        }];
        
    }
    else{
        
        strWebserviceType = @"WS_DELETE_QUOTE_PROUDUCT";
        countval = indexCount;
        
        NSString *soapXMLStr = [NSString stringWithFormat:
                                @"<wws:DeleteQuoteProducts soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
                                "<debtorDetails xsi:type=\"wws:DebtorDetRequest\" soapenc:arrayType=\"wws:DetRequest[]\">"
                                "<QUOTE_NO>%@</QUOTE_NO>"
                                "<ITEM><![CDATA[%@]]></ITEM>"
                                "</debtorDetails>"
                                "</wws:DeleteQuoteProducts>",strQuoteNumRecived,[[arrProducts objectAtIndex:indexCount]objectForKey:@"StockCode"]];
        
        NSLog(@"soapXMLStr %@",soapXMLStr);
        
        spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = self;
        [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservicePost:soapXMLStr soapAction:@"urn:wwservice#deleteQuoteProducts" onView:self.view];
    }
}


//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForString:(NSString *)string
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [string sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    //NSLog(@"%f",labelHeighSize.height);
    return labelHeighSize.height;
    
}


#pragma mark - PopOverDelegates
//Add Products
- (void)showProductList{
    
    QuoteProductListViewController *dataViewController = [[QuoteProductListViewController alloc] initWithNibName:@"QuoteProductListViewController" bundle:[NSBundle mainBundle]];
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}

- (void)dismissPopOver{
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    [popoverController dismissPopoverAnimated:YES];
    
    [self viewWillAppear:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    //tagFlsh = 0;
    
    NSString *strImage=[[NSBundle mainBundle] pathForResource:@"red_plus_down" ofType:@"png"];
    UIImage *img=[[UIImage alloc]initWithContentsOfFile:strImage];
    
    [btnAddMenu setImage:img forState:UIControlStateNormal];
    
    //[self flashOn:btnAddMenu];
}

#pragma mark - Delegates
- (void)addComment:(int)productIndex Dict:(NSMutableDictionary *)dict{
    //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
    
    __block BOOL isCommentExist = NO;
    __block int prodIndex = 0;
    [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"] || [[obj objectForKey:@"StockCode"] isEqualToString:@"/C"]) {
            prodIndex = idx;
            isCommentExist = YES;
            *stop = YES;
        }
    }];
    
    if (!isCommentExist) {
        [arrProducts addObject:dict];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        
        [tblDetails reloadData];
        btnDelete.hidden = NO;
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
        [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    else{
        [arrProducts replaceObjectAtIndex:prodIndex withObject:dict];
        
        if (isCommentAddedOnLastLine) {
            id object = [arrProducts objectAtIndex:prodIndex];
            [arrProducts removeObjectAtIndex:prodIndex];
            [arrProducts insertObject:object atIndex:[arrProducts count]];
        }
        
        [tblDetails reloadData];
    }
}

- (void)setPriceOfProduct:(float)totalPrice ProductIndex:(int)productIndex QuantityOrdered:(int)quantityOrdered DictCalculation:(NSMutableDictionary *)dictCalculation
{
    
    //NSLog(@"arrProducts %@",arrProducts);
    if (!isSelectAll) {
        
        NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
        NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
        
        NSMutableDictionary *dict = [arrProducts objectAtIndex:productIndex];
        [dict setValue:strTotalPrice forKey:@"ExtnPrice"];
        [dict setValue:strQuantityOrdered forKey:@"QuantityOrdered"];
        [dict setValue:[dictCalculation objectForKey:@"Gross"] forKey:@"Gross"];
        [dict setValue:[dictCalculation objectForKey:@"Tax"] forKey:@"Tax"];
        [dict setValue:[dictCalculation objectForKey:@"SellingPrice"] forKey:@"SellingPrice"];
        
        [tblDetails reloadData];

        
    }
    else{
        NSString *strTotalPrice = [NSString stringWithFormat:@"%f",totalPrice];
        NSString *strQuantityOrdered = [NSString stringWithFormat:@"%d",quantityOrdered];
        
        NSMutableDictionary *dict = [arrProducts objectAtIndex:productIndex];
        [dict setValue:strTotalPrice forKey:@"ExtnPrice"];
        [dict setValue:strQuantityOrdered forKey:@"QuantityOrdered"];
        [dict setValue:[dictCalculation objectForKey:@"Gross"] forKey:@"Gross"];
        [dict setValue:[dictCalculation objectForKey:@"Tax"] forKey:@"Tax"];
        [dict setValue:[dictCalculation objectForKey:@"SellingPrice"] forKey:@"SellingPrice"];
        [tblDetails reloadData];
       
    }
   

    line_no_selectAll = 1;
    for (NSMutableDictionary *dict in arrProducts) {
        if ([dict objectForKey:@"ExtnPrice"]) {
            [dict setObject:[NSNumber numberWithInt:line_no_selectAll] forKey:@"LINE_NO"];
            line_no_selectAll ++;
        }
    }
    
    // Updated
    NSString *totallines = [NSString stringWithFormat:@"%d",line_no_selectAll];
    [[NSUserDefaults standardUserDefaults] setObject:totallines forKey:@"QuantityForQuote"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(void)setCommentPosition:(BOOL)status
{
    self.isCommentAddedOnLastLine = status;
}


#pragma mark - Handle Notification

- (void)reloadTableViewData:(NSNotification *)notification {
    NSDictionary *dictSelectedProduct = [notification.userInfo valueForKey:@"source"];
    
    BOOL isScrollToBottom = YES;
    
    if ([[dictSelectedProduct objectForKey:@"Item"] isEqualToString:@"/C"]) {
        [arrProducts addObject:dictSelectedProduct];
    }
    else{
        isSelectAll = NO;
        [dictSelectedProduct setValue:@"1" forKey:@"isNewProduct"];
        
        BOOL toRemove = NO;
        if ([arrProducts count]) {
            for (int i=0; i < [arrProducts count]; i++) {
                if ([[[arrProducts objectAtIndex:i] objectForKey:@"StockCode"] isEqualToString:[dictSelectedProduct objectForKey:@"StockCode"]]) {
                    
                    
                    if ([[[arrProducts objectAtIndex:i] objectForKey:@"isNewProduct"] boolValue] != [[dictSelectedProduct objectForKey:@"isNewProduct"] boolValue]) {
                        
                        NSString *strMessage = [NSString stringWithFormat:@"\"%@\" is already added!",[dictSelectedProduct objectForKey:@"Description"]];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                        strMessage = nil;
                        
                    }
                    else{
                        if (![[dictSelectedProduct objectForKey:@"CheckFlag"] boolValue]) {
                            [arrProducts removeObjectAtIndex:i];
                        }
                        
                    }
                    
                    toRemove = YES;
                    isScrollToBottom = NO;
                    DLog(@"%hhd",isScrollToBottom);
                    break;
                }
                else{
                    toRemove = NO;
                }
                
            }
        }
        else{
            toRemove = NO;
            
        }
        
        if (!toRemove)
        {
            [arrProducts insertObject:dictSelectedProduct atIndex:0];
            //[arrProducts addObject:dictSelectedProduct];
        }
        
    }
    
    int lastIndex = [arrProducts count];
    if (lastIndex) {
        
        if (isCommentAddedOnLastLine) {
            [arrProducts enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                if ([[obj objectForKey:@"Item"] isEqualToString:@"/C"]) {
                    id object = [arrProducts objectAtIndex:idx];
                    [arrProducts removeObjectAtIndex:idx];
                    [arrProducts insertObject:object atIndex:lastIndex-1];
                    *stop = YES;
                }
            }];
        }
        
        
        //Show delete button
        btnDelete.hidden = NO;
        tblDetails.editing = NO;
        [btnDelete setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDelete setBackgroundImage:[UIImage imageNamed:@"delete_button.png"]  forState:UIControlStateNormal];
        
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwDetails];
        [tblDetails reloadData];
        
        /*
         if (isScrollToBottom) {
         NSIndexPath* ipath = [NSIndexPath indexPathForRow: [arrProducts count]-1 inSection: 0];
         [tblDetails scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
         }
         */
        
    }
    else{
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoProducts];
        btnDelete.hidden = YES;
        
    }
}



- (void)reloadHeader:(NSNotification *)notification {
    
    selectedIndex = -1;
    selectedSectionIndex = -1;
    
    //Show the header
    [segControl setSelectedIndex:0];
    
    if (EditableMode == NO) {
        //[[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
        
    }
    
    self.dictHeaderDetails = [notification.userInfo valueForKey:@"source"];
    
    //NSDictionary *dict  = [[NSDictionary alloc] initWithObjectsAndKeys:strQuoteNum,@"text", nil];
    
    [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
    
    self.strDebtorNum = [dictHeaderDetails objectForKey:@"Debtor"];
    
    if ([dictHeaderDetails objectForKey:@"Debtor"]) {
        
        if (![[dictHeaderDetails objectForKey:@"Debtor"] length] == 0) {
            segControl.enabled = YES;
        }
    }
    else{
        segControl.enabled = NO;
    }
    
    NSLog(@"dictHeaderDetails==%@",dictHeaderDetails);
    
    [tblHeader reloadData];
}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    [spinner removeFromSuperview];
    
    if (EditableMode == NO) {
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        
        if ([strWebserviceType isEqualToString:@"WS_QUOTE_SAVE"]){
            
            //NSLog(@"dictHeaderDetails %@",dictHeaderDetails);
            
            if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
                //True
                
                NSDictionary *dict = [[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"debtorHeader"];
                
                NSString *strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                
                NSMutableArray *arrQuoteDetails;
                
                if ([[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"]) {
                    arrQuoteDetails = [[NSMutableArray alloc]init];
                    
                    if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"] isKindOfClass:[NSDictionary class]]) {
                        [arrQuoteDetails addObject:[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"]];
                    }
                    else{
                        arrQuoteDetails = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"];
                    }
                }
                
                dispatch_async(backgroundQueueForNewQuote, ^(void) {
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        BOOL isSomethingWrongHappened = FALSE;
                        @try
                        {
                            
                            NSString *strDelName = @"";
                            if ([[dict objectForKey:@"DEL_NAME"] objectForKey:@"text"]) {
                                strDelName = [[[dict objectForKey:@"DEL_NAME"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelAdd1 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS1"] objectForKey:@"text"]) {
                                strDelAdd1 = [[[dict objectForKey:@"DEL_ADDRESS1"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelAdd2 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS2"] objectForKey:@"text"]) {
                                strDelAdd2 = [[[dict objectForKey:@"DEL_ADDRESS2"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelAdd3 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS3"] objectForKey:@"text"]) {
                                strDelAdd3 = [[[dict objectForKey:@"DEL_ADDRESS3"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strBranch = @"";
                            if ([[dict objectForKey:@"BRANCH"] objectForKey:@"text"]) {
                                strBranch = [[[dict objectForKey:@"BRANCH"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelDate = @"";
                            if ([[dict objectForKey:@"DELIVERY_DATE"] objectForKey:@"text"]) {
                                strDelDate = [[[dict objectForKey:@"DELIVERY_DATE"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelCountry = @"";
                            if ([[dict objectForKey:@"DEL_COUNTRY"] objectForKey:@"text"]) {
                                strDelCountry = [[[dict objectForKey:@"DEL_COUNTRY"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelInst1 = @"";
                            if ([[dict objectForKey:@"DEL_INST1"] objectForKey:@"text"]) {
                                strDelInst1 = [[dict objectForKey:@"DEL_INST1"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelInst2 = @"";
                            if ([[dict objectForKey:@"DEL_INST2"] objectForKey:@"text"]) {
                                strDelInst2 = [[[dict objectForKey:@"DEL_INST2"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelPostCode = @"";
                            if ([[dict objectForKey:@"DEL_POST_CODE"] objectForKey:@"text"]) {
                                strDelPostCode = [[[dict objectForKey:@"DEL_POST_CODE"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDelSubUrb = @"";
                            if ([[dict objectForKey:@"DEL_SUBURB"] objectForKey:@"text"]) {
                                strDelSubUrb = [[[dict objectForKey:@"DEL_SUBURB"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strReleaseType = @"";
                            if ([[dict objectForKey:@"RELEASE_TYPE"] objectForKey:@"text"]) {
                                strReleaseType = [[[dict objectForKey:@"RELEASE_TYPE"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strStatus = STATUS;
                            
                            NSString *strWarehouse = @"";
                            if ([[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"]) {
                                strWarehouse = [[[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strisFinalised = @"";
                            if ([[dict objectForKey:@"isFinalised"] objectForKey:@"text"]) {
                                strisFinalised = [[[dict objectForKey:@"isFinalised"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strDetailLines = @"0";
                            if ([[dict objectForKey:@"DETAIL_LINES"] objectForKey:@"text"]) {
                                strDetailLines = [[[dict objectForKey:@"DETAIL_LINES"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strWeight = @"0";
                            if ([[dict objectForKey:@"WEIGHT"] objectForKey:@"text"]) {
                                strWeight = [[[dict objectForKey:@"WEIGHT"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strVolume = @"";
                            if ([[dict objectForKey:@"VOLUME"] objectForKey:@"text"]) {
                                strVolume = [[[dict objectForKey:@"VOLUME"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strTax = @"";
                            if ([[dict objectForKey:@"TAX"] objectForKey:@"text"]) {
                                strTax = [[[dict objectForKey:@"TAX"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            NSString *strTaxAmount1 = @"";
                            if ([[dict objectForKey:@"TAX_AMOUNT1"] objectForKey:@"text"]) {
                                strTaxAmount1 = [[[dict objectForKey:@"TAX_AMOUNT1"] objectForKey:@"text"] trimSpaces];
                            }
                            
                            
                            NSString *strContact = @"";
                            if ([[dict objectForKey:@"CONTACT"] objectForKey:@"text"]) {
                                strContact = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CONTACT"] objectForKey:@"text"]];
                            }
                            
                            NSString *strPriceCode = @"";
                            if ([[dict objectForKey:@"PRICE_CODE"] objectForKey:@"text"]) {
                                strPriceCode = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"PRICE_CODE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strSalesman = @"";
                            if ([[dict objectForKey:@"SALESMAN"] objectForKey:@"text"]) {
                                strSalesman = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"SALESMAN"] objectForKey:@"text"]];
                            }
                            
                            NSString *strSalesBranch = @"";
                            if ([[dict objectForKey:@"SALES_BRANCH"] objectForKey:@"text"]) {
                                strSalesBranch= [NSString stringWithFormat:@"%@",[[dict objectForKey:@"SALES_BRANCH"] objectForKey:@"text"]];
                            }
                            
                            NSString *strTradingTerms = @"";
                            if ([[dict objectForKey:@"TRADING_TERMS"] objectForKey:@"text"]) {
                                strTradingTerms = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"TRADING_TERMS"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExchangeCode = @"";
                            if ([[dict objectForKey:@"EXCHANGE_CODE"] objectForKey:@"text"]) {
                                strExchangeCode = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXCHANGE_CODE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strChargetype = @"";
                            if ([[dict objectForKey:@"CHARGE_TYPE"] objectForKey:@"text"]) {
                                strChargetype = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CHARGE_TYPE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExportDebtor = @"";
                            if ([[dict objectForKey:@"EXPORT_DEBTOR"] objectForKey:@"text"]) {
                                strExportDebtor = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXPORT_DEBTOR"] objectForKey:@"text"]];
                            }
                            
                            NSString *strChargeRate = @"";
                            if ([[dict objectForKey:@"CHARGE_RATE"] objectForKey:@"text"]) {
                                strChargeRate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CHARGE_RATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strHeld = @"";
                            if ([[dict objectForKey:@"HELD"] objectForKey:@"text"]) {
                                strHeld = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"HELD"] objectForKey:@"text"]];
                            }
                            
                            NSString *strNumboxes = @"";
                            if ([[dict objectForKey:@"NUM_BOXES"] objectForKey:@"text"]) {
                                strNumboxes = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"NUM_BOXES"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDirect = @"";
                            if ([[dict objectForKey:@"DIRECT"] objectForKey:@"text"]) {
                                strDirect= [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DIRECT"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDateRaised = @"";
                            if ([[dict objectForKey:@"DATE_RAISED"] objectForKey:@"text"]) {
                                strDateRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DATE_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strPeriodRaised = @"";
                            if ([[dict objectForKey:@"PERIOD_RAISED"] objectForKey:@"text"]) {
                                strPeriodRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"PERIOD_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strYearRaised = @"";
                            if ([[dict objectForKey:@"YEAR_RAISED"] objectForKey:@"text"]) {
                                strYearRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"YEAR_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDropSequence = @"";
                            if ([[dict objectForKey:@"DROP_SEQ"] objectForKey:@"text"]) {
                                strDropSequence = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DROP_SEQ"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDeliveryDate = @"";
                            if ([[dict objectForKey:@"DEL_DATE"] objectForKey:@"text"]) {
                                strDeliveryDate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DEL_DATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExchangeRate = @"";
                            if ([[dict objectForKey:@"EXCHANGE_RATE"] objectForKey:@"text"]) {
                                strExchangeRate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXCHANGE_RATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strActive = @"";
                            if ([[dict objectForKey:@"ACTIVE"] objectForKey:@"text"]) {
                                strActive = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"ACTIVE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strEditStaus = @"";
                            if ([[dict objectForKey:@"EDIT_STATUS"] objectForKey:@"text"]) {
                                strEditStaus = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EDIT_STATUS"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDELIVERY_RUN = @"";
                            
                            if([[dict objectForKey:@"DELIVERY_RUN"] objectForKey:@"text"])
                            {
                                strDELIVERY_RUN = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DELIVERY_RUN"] objectForKey:@"text"]];
                            }
                            NSString *strQUOTE_VALUE = @"";
                            
                            if ([[dict objectForKey:@"QUOTE_VALUE"] objectForKey:@"text"]) {
                                
                                strQUOTE_VALUE = [[dict objectForKey:@"QUOTE_VALUE"] objectForKey:@"text"];
                            }
                            
                            BOOL y2 = [db executeUpdate:@"INSERT INTO `soquohea` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`, `DEBTOR`, `DEL_NAME`, `DEL_ADDRESS1`, `DEL_ADDRESS2`, `DEL_ADDRESS3`, `BRANCH`,`DEL_DATE`,`DEL_COUNTRY`,`DEL_INST1`,`DEL_INST2`,`DEL_POST_CODE`,`DEL_SUBURB`,`RELEASE_TYPE`,`STATUS`,`WAREHOUSE`,DETAIL_LINES,WEIGHT,VOLUME,TAX,TAX_AMOUNT1,DATE_RAISED,CONTACT,DIRECT, SALESMAN, SALES_BRANCH,TRADING_TERMS,PRICE_CODE,EXCHANGE_CODE,CHARGE_TYPE,EXPORT_DEBTOR,DROP_SEQ,HELD,CHARGE_RATE,NUM_BOXES,EXCHANGE_RATE,ACTIVE,EDIT_STATUS,PERIOD_RAISED,YEAR_RAISED,`isFinalised`, `created_date`, `modified_date`,EXPIRY_DATE,CARRIER,DELIVERY_RUN,QUOTE_VALUE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"], [[dict objectForKey:@"RECNUM"] objectForKey:@"text"],[[dict objectForKey:@"QUOTE_NO"] objectForKey:@"text"], [[dict objectForKey:@"DEBTOR"] objectForKey:@"text"], strDelName,strDelAdd1,strDelAdd2,strDelAdd3,strBranch,strDelDate,strDelCountry,strDelInst1,strDelInst2,strDelPostCode,strDelSubUrb,strReleaseType,strStatus,strWarehouse,strDetailLines,strWeight,strVolume,strTax,strTaxAmount1,strDateRaised,strContact,strDirect,strSalesman,strSalesBranch,strTradingTerms,strPriceCode,strExchangeCode,strChargetype,strExportDebtor,strDropSequence,strHeld,strChargeRate,strNumboxes,strExchangeRate,strActive,strEditStaus,strPeriodRaised,strYearRaised,strisFinalised,[[dict objectForKey:@"created_date"] objectForKey:@"text"],[[dict objectForKey:@"modified_date"] objectForKey:@"text"],strExpiry_Date,[[dict objectForKey:@"CARRIER"]objectForKey:@"text"],strDELIVERY_RUN,strQUOTE_VALUE];
                            
                            
                            if (!y2)
                            {
                                isSomethingWrongHappened = TRUE;
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            
                            for (NSDictionary *dictTemp in arrQuoteDetails) {
                                BOOL y1 = [db executeUpdate:@"INSERT INTO `soquodet` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`,  `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT, VOLUME,CONV_FACTOR,COST,ORIG_COST,GROSS,TAX,STATUS,EDIT_STATUS,`created_date`, `modified_date`,BRANCH,WAREHOUSE,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?,?,?,?)", [[dictTemp objectForKey:@"SCM_RECNUM"] objectForKey:@"text"], [[dictTemp objectForKey:@"RECNUM"] objectForKey:@"text"],strQuoteNum,[dictHeaderDetails objectForKey:@"Debtor"],[[dictTemp objectForKey:@"DESCRIPTION"] objectForKey:@"text"], [[dictTemp objectForKey:@"EXTENSION"] objectForKey:@"text"], [[dictTemp objectForKey:@"ITEM"] objectForKey:@"text"],[[dictTemp objectForKey:@"PRICE"] objectForKey:@"text"],[[dictTemp objectForKey:@"LINE_NO"] objectForKey:@"text"],[[dictTemp objectForKey:@"QUANTITY"] objectForKey:@"text"],[[dictTemp objectForKey:@"ALT_QTY"] objectForKey:@"text"],[[dictTemp objectForKey:@"WEIGHT"] objectForKey:@"text"],[[dictTemp objectForKey:@"VOLUME"] objectForKey:@"text"],[[dictTemp objectForKey:@"CONV_FACTOR"] objectForKey:@"text"],[[dictTemp objectForKey:@"COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"ORIG_COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"GROSS"] objectForKey:@"text"],[[dictTemp objectForKey:@"TAX"] objectForKey:@"text"],strStatus,[[dictTemp objectForKey:@"EDIT_STATUS"] objectForKey:@"text"],[[dict objectForKey:@"created_date"] objectForKey:@"text"],[[dictTemp objectForKey:@"modified_date"] objectForKey:@"text"],[[dictTemp objectForKey:@"BRANCH"] objectForKey:@"text"],[[dictTemp objectForKey:@"WAREHOUSE"] objectForKey:@"text"],[[dictTemp objectForKey:@"DECIMAL_PLACES"] objectForKey:@"text"],[[dictTemp objectForKey:@"PRICE_CODE"] objectForKey:@"text"],[[dictTemp objectForKey:@"DISSECTION"] objectForKey:@"text"],[[dictTemp objectForKey:@"DISSECTION_COS"] objectForKey:@"text"]];
                                
                                if (!y1)
                                {
                                    isSomethingWrongHappened = TRUE;
                                    
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    break;
                                }
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                alert.tag = TAG_50;
                                [alert show];
                                
                            });
                            
                            
                        }
                        @catch (NSException* e) {
                            *rollback = YES;
                            
                            // rethrow if not one of the two exceptions above
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                                
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                
                                [alert show];
                                
                            });
                            
                        }
                        
                        
                    }];
                    
                });
            }
            //False
            else{
                
                NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:InsertDebtorDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
            }
        }
        else if ([strWebserviceType isEqualToString:@"WS_GET_QUOTE_NUMBER"]){
            
            
            if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
                self.strQuoteNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"quoteno"] objectForKey:@"text"];
                
                [dictHeaderDetails setValue:strQuoteNum forKey:@"QuoteNum"];
                [tblHeader reloadData];
            }
            //False
            else{
                
                NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
    }
    else{
        
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        
        if ([strWebserviceType isEqualToString:@"WS_QUOTE_DETAILS_DETAILS"]){
            
            
            NSLog(@"ASIHTTPRequest_Success WS_QUOTE_DETAILS_DETAILS dictResponse : %@",dictResponse);
            
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    [vwContentSuperview removeAllSubviews];
                    
                    //NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                    
                    if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                        
                        NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
                        //[arrProducts addObject:dict];
                        [arrProducts addObject:dict];
                        
                    }
                    else{
                        
                        self.arrProducts = [[[dictResponse objectForKey:@"Response"] objectForKey:@"QuoteDetails"] objectForKey:@"Data"];
                    }
                    
                    [self doAfterDataFetched];
                    
                }
                else{
                    
                    [vwContentSuperview addSubview:vwNoProducts];
                    if (chkFinalize == NO) {
                        [self showPointingAnimation];
                    }
                    
                    
                }
                
                
            }
            //False
            else{
                
                [vwContentSuperview removeAllSubviews];
                [vwContentSuperview addSubview:vwNoProducts];
                
            }
        }
        else if ([strWebserviceType isEqualToString:@"WS_QUOTE_EDIT_SAVE"])
        {
            NSLog(@"ASIHTTPRequest_Success WS_QUOTE_EDIT_SAVE dictResponse : %@",dictResponse);
            
            if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
                //True
                
                NSDictionary *dict = [[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"debtorHeader"];
                
                
                NSString *strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                
                NSMutableArray *arrQuoteDetails;
                
                if ([[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"]) {
                    arrQuoteDetails = [[NSMutableArray alloc]init];
                    
                    if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"] isKindOfClass:[NSDictionary class]]) {
                        [arrQuoteDetails addObject:[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"]];
                    }
                    else{
                        arrQuoteDetails = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"debtorDetails"] objectForKey:@"item"];
                    }
                }
                
                
                dispatch_async(backgroundQueueForNewQuote, ^(void) {
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        BOOL isSomethingWrongHappened = FALSE;
                        @try
                        {
                            
                            NSString *strDelName = @"";
                            if ([[dict objectForKey:@"DEL_NAME"] objectForKey:@"text"]) {
                                strDelName = [[dict objectForKey:@"DEL_NAME"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelAdd1 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS1"] objectForKey:@"text"]) {
                                strDelAdd1 = [[dict objectForKey:@"DEL_ADDRESS1"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelAdd2 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS2"] objectForKey:@"text"]) {
                                strDelAdd2 = [[dict objectForKey:@"DEL_ADDRESS2"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelAdd3 = @"";
                            if ([[dict objectForKey:@"DEL_ADDRESS3"] objectForKey:@"text"]) {
                                strDelAdd3 = [[dict objectForKey:@"DEL_ADDRESS3"] objectForKey:@"text"];
                            }
                            
                            NSString *strBranch = @"";
                            if ([[dict objectForKey:@"BRANCH"] objectForKey:@"text"]) {
                                strBranch = [[dict objectForKey:@"BRANCH"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelDate = @"";
                            if ([[dict objectForKey:@"DELIVERY_DATE"] objectForKey:@"text"]) {
                                strDelDate = [[dict objectForKey:@"DELIVERY_DATE"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelCountry = @"";
                            if ([[dict objectForKey:@"DEL_COUNTRY"] objectForKey:@"text"]) {
                                strDelCountry = [[dict objectForKey:@"DEL_COUNTRY"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelInst1 = @"";
                            if ([[dict objectForKey:@"DEL_INST1"] objectForKey:@"text"]) {
                                strDelInst1 = [[dict objectForKey:@"DEL_INST1"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelInst2 = @"";
                            if ([[dict objectForKey:@"DEL_INST2"] objectForKey:@"text"]) {
                                strDelInst2 = [[dict objectForKey:@"DEL_INST2"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelPostCode = @"";
                            if ([[dict objectForKey:@"DEL_POST_CODE"] objectForKey:@"text"]) {
                                strDelPostCode = [[dict objectForKey:@"DEL_POST_CODE"] objectForKey:@"text"];
                            }
                            
                            NSString *strDelSubUrb = @"";
                            if ([[dict objectForKey:@"DEL_SUBURB"] objectForKey:@"text"]) {
                                strDelSubUrb = [[dict objectForKey:@"DEL_SUBURB"] objectForKey:@"text"];
                            }
                            
                            NSString *strReleaseType = @"";
                            if ([[dict objectForKey:@"RELEASE_TYPE"] objectForKey:@"text"]) {
                                strReleaseType = [[dict objectForKey:@"RELEASE_TYPE"] objectForKey:@"text"];
                            }
                            
                            NSString *strStatus = STATUS;
                            
                            NSString *strWarehouse = @"";
                            if ([[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"]) {
                                strWarehouse = [[dict objectForKey:@"WAREHOUSE"] objectForKey:@"text"];
                            }
                            
                            NSString *strDetailLines = @"";
                            if ([[dict objectForKey:@"DETAIL_LINES"] objectForKey:@"text"]) {
                                strDetailLines = [[dict objectForKey:@"DETAIL_LINES"] objectForKey:@"text"];
                            }
                            
                            NSString *strTotalWeight = @"";
                            if ([[dict objectForKey:@"WEIGHT"] objectForKey:@"text"]) {
                                strTotalWeight = [[dict objectForKey:@"WEIGHT"] objectForKey:@"text"];
                            }
                            
                            NSString *strTotalVolume = @"";
                            if ([[dict objectForKey:@"VOLUME"] objectForKey:@"text"]) {
                                strTotalVolume = [[dict objectForKey:@"VOLUME"] objectForKey:@"text"];
                            }
                            
                            NSString *strTotalTax = @"";
                            if ([[dict objectForKey:@"TAX"] objectForKey:@"text"]) {
                                strTotalTax = [[dict objectForKey:@"TAX"] objectForKey:@"text"];
                            }
                            
                            NSString *strTotalTaxAmount1 = @"";
                            if ([[dict objectForKey:@"TAX_AMOUNT1"] objectForKey:@"text"]) {
                                strTotalTaxAmount1 = [[dict objectForKey:@"TAX_AMOUNT1"] objectForKey:@"text"];
                            }
                            
                            //------
                            NSString *strContact = @"";
                            if ([[dict objectForKey:@"CONTACT"] objectForKey:@"text"]) {
                                strContact = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CONTACT"] objectForKey:@"text"]];
                            }
                            
                            NSString *strPriceCode = @"";
                            if ([[dict objectForKey:@"PRICE_CODE"] objectForKey:@"text"]) {
                                strPriceCode = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"PRICE_CODE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strSalesman = @"";
                            if ([[dict objectForKey:@"SALESMAN"] objectForKey:@"text"]) {
                                strSalesman = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"SALESMAN"] objectForKey:@"text"]];
                            }
                            
                            NSString *strSalesBranch = @"";
                            if ([[dict objectForKey:@"SALES_BRANCH"] objectForKey:@"text"]) {
                                strSalesBranch= [NSString stringWithFormat:@"%@",[[dict objectForKey:@"SALES_BRANCH"] objectForKey:@"text"]];
                            }
                            
                            NSString *strTradingTerms = @"";
                            if ([[dict objectForKey:@"TRADING_TERMS"] objectForKey:@"text"]) {
                                strTradingTerms = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"TRADING_TERMS"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExchangeCode = @"";
                            if ([[dict objectForKey:@"EXCHANGE_CODE"] objectForKey:@"text"]) {
                                strExchangeCode = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXCHANGE_CODE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strChargetype = @"";
                            if ([[dict objectForKey:@"CHARGE_TYPE"] objectForKey:@"text"]) {
                                strChargetype = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CHARGE_TYPE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExportDebtor = @"";
                            if ([[dict objectForKey:@"EXPORT_DEBTOR"] objectForKey:@"text"]) {
                                strExportDebtor = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXPORT_DEBTOR"] objectForKey:@"text"]];
                            }
                            
                            NSString *strChargeRate = @"";
                            if ([[dict objectForKey:@"CHARGE_RATE"] objectForKey:@"text"]) {
                                strChargeRate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"CHARGE_RATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strHeld = @"";
                            if ([[dict objectForKey:@"HELD"] objectForKey:@"text"]) {
                                strHeld = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"HELD"] objectForKey:@"text"]];
                            }
                            
                            NSString *strNumboxes = @"";
                            if ([[dict objectForKey:@"NUM_BOXES"] objectForKey:@"text"]) {
                                strNumboxes = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"NUM_BOXES"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDirect = @"";
                            if ([[dict objectForKey:@"DIRECT"] objectForKey:@"text"]) {
                                strDirect= [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DIRECT"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDateRaised = @"";
                            if ([[dict objectForKey:@"DATE_RAISED"] objectForKey:@"text"]) {
                                strDateRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DATE_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strPeriodRaised = @"";
                            if ([[dict objectForKey:@"PERIOD_RAISED"] objectForKey:@"text"]) {
                                strPeriodRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"PERIOD_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strYearRaised = @"";
                            if ([[dict objectForKey:@"YEAR_RAISED"] objectForKey:@"text"]) {
                                strYearRaised = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"YEAR_RAISED"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDropSequence = @"";
                            if ([[dict objectForKey:@"DROP_SEQ"] objectForKey:@"text"]) {
                                strDropSequence = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DROP_SEQ"] objectForKey:@"text"]];
                            }
                            
                            NSString *strDeliveryDate = @"";
                            if ([[dict objectForKey:@"DEL_DATE"] objectForKey:@"text"]) {
                                strDeliveryDate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"DEL_DATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strExchangeRate = @"";
                            if ([[dict objectForKey:@"EXCHANGE_RATE"] objectForKey:@"text"]) {
                                strExchangeRate = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EXCHANGE_RATE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strActive = @"";
                            if ([[dict objectForKey:@"ACTIVE"] objectForKey:@"text"]) {
                                strActive = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"ACTIVE"] objectForKey:@"text"]];
                            }
                            
                            NSString *strEditStaus = @"";
                            if ([[dict objectForKey:@"EDIT_STATUS"] objectForKey:@"text"]) {
                                strEditStaus = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"EDIT_STATUS"] objectForKey:@"text"]];
                            }
                            
                            
                            //                            BOOL y2 = [db executeUpdate:@"UPDATE soquohea SET SCM_RECNUM = ?,RECNUM = ?,DEBTOR = ?,DEL_NAME = ?,DEL_ADDRESS1 = ?,DEL_ADDRESS2 = ?,DEL_ADDRESS3 = ?,BRANCH = ?,DEL_DATE = ?,DEL_COUNTRY = ?,DEL_INST1 = ?,DEL_INST2 = ?,DEL_POST_CODE = ?, DEL_SUBURB= ?,RELEASE_TYPE = ?,STATUS= ?,WAREHOUSE= ?,DETAIL_LINES = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ?,CONTACT = ?,PRICE_CODE = ?,SALESMAN = ?,SALES_BRANCH = ?,TRADING_TERMS = ?,EXCHANGE_CODE = ?,CHARGE_TYPE = ?,EXPORT_DEBTOR = ?,CHARGE_RATE = ?,HELD = ?,NUM_BOXES = ?,DIRECT = ?,DATE_RAISED = ?,PERIOD_RAISED = ?,YEAR_RAISED = ?,DROP_SEQ = ?,DEL_DATE = ?,EXCHANGE_RATE = ?,ACTIVE = ?,EDIT_STATUS = ?,created_date = ?,modified_date = ?  WHERE QUOTE_NO  = ?",
                            //                                       [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                            //                                       [[dict objectForKey:@"RECNUM"] objectForKey:@"text"],
                            //                                       [[dict objectForKey:@"DEBTOR"] objectForKey:@"text"],
                            //                                       strDelName,
                            //                                       strDelAdd1,
                            //                                       strDelAdd2,
                            //                                       strDelAdd3,
                            //                                       strBranch,
                            //                                       strDelDate,
                            //                                       strDelCountry,
                            //                                       strDelInst1,
                            //                                       strDelInst2,
                            //                                       strDelPostCode,
                            //                                       strDelSubUrb,
                            //                                       strReleaseType,
                            //                                       strStatus,
                            //                                       strWarehouse,strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTaxAmount1,strContact,strPriceCode,strSalesman,strSalesBranch,strTradingTerms,strExchangeCode,strChargetype,strExportDebtor,strChargeRate,strHeld,strNumboxes,strDirect,strDateRaised,strPeriodRaised,strYearRaised,strDropSequence,strDeliveryDate,strExchangeRate,strActive,strEditStaus,
                            //                                       [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                            //                                       [[dict objectForKey:@"modified_date"] objectForKey:@"text"],
                            //                                       strQuoteNumRecived];
                            
                            BOOL y2 = [db executeUpdate:@"UPDATE soquohea SET SCM_RECNUM = ?,RECNUM = ?,DEBTOR = ?,DEL_NAME = ?,DEL_ADDRESS1 = ?,DEL_ADDRESS2 = ?,DEL_ADDRESS3 = ?,DETAIL_LINES = ?,WEIGHT = ?,VOLUME = ?,TAX = ?,TAX_AMOUNT1 = ?,created_date = ?,modified_date = ?  WHERE QUOTE_NO  = ?",
                                       [[dict objectForKey:@"SCM_RECNUM"] objectForKey:@"text"],
                                       [[dict objectForKey:@"RECNUM"] objectForKey:@"text"],
                                       [[dict objectForKey:@"DEBTOR"] objectForKey:@"text"],
                                       strDelName,strDelAdd1,strDelAdd2,strDelAdd3,strDetailLines,strTotalWeight,strTotalVolume,strTotalTax,strTotalTaxAmount1,[[dict objectForKey:@"created_date"] objectForKey:@"text"],[[dict objectForKey:@"modified_date"] objectForKey:@"text"],strQuoteNumRecived];
                            
                            if (!y2)
                            {
                                isSomethingWrongHappened = TRUE;
                                
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            
                            for (NSDictionary *dictTemp in arrQuoteDetails)
                            {
                                
                                FMResultSet *rs =  [db executeQuery:@"SELECT QUOTE_NO FROM soquodet WHERE QUOTE_NO = ? AND ITEM = ?",strQuoteNumRecived,[[dictTemp objectForKey:@"ITEM"] objectForKey:@"text"]];
                                
                                BOOL y1;
                                //Update
                                if ([rs next]){
                                    
                                    
                                    y1 = [db executeUpdate:@"UPDATE `soquodet` SET `SCM_RECNUM` = ? ,`RECNUM` = ? ,`QUOTE_NO` = ?, `DEBTOR` = ? , `DESCRIPTION` = ?, `EXTENSION` = ?, `ITEM` = ?, `PRICE` = ?, LINE_NO = ?,QUANTITY = ?,ALT_QTY = ?,WEIGHT = ?,VOLUME = ?,CONV_FACTOR = ?,COST = ?,ORIG_COST = ?,GROSS = ?,TAX = ?,`created_date` = ?, `modified_date` = ? ,STATUS = ?,EDIT_STATUS = ? WHERE QUOTE_NO = ? AND ITEM = ?", [[dictTemp objectForKey:@"SCM_RECNUM"] objectForKey:@"text"], [[dictTemp objectForKey:@"RECNUM"] objectForKey:@"text"],strQuoteNumRecived,[dictHeaderDetails objectForKey:@"Debtor"],[[dictTemp objectForKey:@"DESCRIPTION"] objectForKey:@"text"], [[dictTemp objectForKey:@"EXTENSION"] objectForKey:@"text"], [[dictTemp objectForKey:@"ITEM"] objectForKey:@"text"],[[dictTemp objectForKey:@"PRICE"] objectForKey:@"text"],[[dictTemp objectForKey:@"LINE_NO"] objectForKey:@"text"],[[dictTemp objectForKey:@"QUANTITY"] objectForKey:@"text"],[[dictTemp objectForKey:@"ALT_QTY"] objectForKey:@"text"],[[dictTemp objectForKey:@"WEIGHT"] objectForKey:@"text"],[[dictTemp objectForKey:@"VOLUME"] objectForKey:@"text"],[[dictTemp objectForKey:@"CONV_FACTOR"] objectForKey:@"text"],[[dictTemp objectForKey:@"COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"ORIG_COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"GROSS"] objectForKey:@"text"],[[dictTemp objectForKey:@"TAX"] objectForKey:@"text"],[[dictTemp objectForKey:@"created_date"] objectForKey:@"text"],[[dictTemp objectForKey:@"modified_date"] objectForKey:@"text"],strStatus,[[dictTemp objectForKey:@"EDIT_STATUS"] objectForKey:@"text"],strQuoteNumRecived,[[dictTemp objectForKey:@"ITEM"] objectForKey:@"text"]];
                                }
                                else{
                                    y1 = [db executeUpdate:@"INSERT INTO `soquodet` (`SCM_RECNUM`, `RECNUM`, `QUOTE_NO`,  `DEBTOR`, `DESCRIPTION`, `EXTENSION`, `ITEM`, `PRICE`,LINE_NO,QUANTITY,ALT_QTY,WEIGHT,VOLUME,CONV_FACTOR, COST,ORIG_COST,GROSS,TAX,`created_date`, `modified_date`,STATUS,EDIT_STATUS,WAREHOUSE,BRANCH,DECIMAL_PLACES,PRICE_CODE,DISSECTION,DISSECTION_COS) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?)", [[dictTemp objectForKey:@"SCM_RECNUM"] objectForKey:@"text"], [[dictTemp objectForKey:@"RECNUM"] objectForKey:@"text"],strQuoteNumRecived,[dictHeaderDetails objectForKey:@"Debtor"],[[dictTemp objectForKey:@"DESCRIPTION"] objectForKey:@"text"], [[dictTemp objectForKey:@"EXTENSION"] objectForKey:@"text"], [[dictTemp objectForKey:@"ITEM"] objectForKey:@"text"],[[dictTemp objectForKey:@"PRICE"] objectForKey:@"text"],[[dictTemp objectForKey:@"LINE_NO"] objectForKey:@"text"],[[dictTemp objectForKey:@"QUANTITY"] objectForKey:@"text"],[[dictTemp objectForKey:@"ALT_QTY"] objectForKey:@"text"],[[dictTemp objectForKey:@"WEIGHT"] objectForKey:@"text"],[[dictTemp objectForKey:@"VOLUME"] objectForKey:@"text"],[[dictTemp objectForKey:@"CONV_FACTOR"] objectForKey:@"text"],[[dictTemp objectForKey:@"COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"ORIG_COST"] objectForKey:@"text"],[[dictTemp objectForKey:@"GROSS"] objectForKey:@"text"],[[dictTemp objectForKey:@"TAX"] objectForKey:@"text"],[[dictTemp objectForKey:@"created_date"] objectForKey:@"text"],[[dictTemp objectForKey:@"modified_date"] objectForKey:@"text"],strStatus,[[dictTemp objectForKey:@"EDIT_STATUS"] objectForKey:@"text"],[[dictTemp objectForKey:@"WAREHOUSE"] objectForKey:@"text"],[[dictTemp objectForKey:@"BRANCH"] objectForKey:@"text"],[[dictTemp objectForKey:@"DECIMAL_PLACES"] objectForKey:@"text"],[[dictTemp objectForKey:@"PRICE_CODE"] objectForKey:@"text"],[[dictTemp objectForKey:@"DISSECTION"] objectForKey:@"text"],[[dictTemp objectForKey:@"DISSECTION_COS"] objectForKey:@"text"]];
                                }
                                
                                
                                if (!y1)
                                {
                                    [rs close];
                                    isSomethingWrongHappened = TRUE;
                                    
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    break;
                                }
                                
                                [rs close];
                            }
                            
                            
                            //Commit here
                            *rollback = NO;
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                alert.tag = TAG_50;
                                [alert show];
                                
                            });
                            
                            
                        }
                        @catch (NSException* e) {
                            *rollback = YES;
                            // rethrow if not one of the two exceptions above
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                
                            });
                            
                        }
                        
                    }];
                    
                    databaseQueue = nil;
                    
                });
                
                
            }
            //False
            else{
                
                NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:EditDebtorDetailsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
        else if ([strWebserviceType isEqualToString:@"WS_GET_QUOTE_NUMBER"]){
            
            NSLog(@"WS_GET_QUOTE_NUMBER hide spinnnnnnnner");
            
            if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
                self.strQuoteNum = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"quoteno"] objectForKey:@"text"];
                
                self.dictHeaderDetails = nil;
                
                NSDictionary *dict  = [[NSDictionary alloc] initWithObjectsAndKeys:strQuoteNum,@"text", nil];
                
                [dictHeaderDetails setObject:dict forKey:@"QuoteNum"];
                [tblHeader reloadData];
            }
            //False
            else{
                
                NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:GetQuoteNoResponse"] objectForKey:@"message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
        else if ([strWebserviceType isEqualToString:@"WS_DELETE_QUOTE_PROUDUCT"])
        {
            NSLog(@"WS_DELETE_QUOTE_PROUDUCT dictResponse==%@",dictResponse);
            
            if ([[[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteQuoteProductsResponse"] objectForKey:@"flag"] objectForKey:@"text"] isEqualToString:@"true"]) {
                //True
                
                NSString *strSuccessMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteQuoteProductsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                
                
                // NSLog(@"countval success message==%d",countval);
                
                dispatch_async(backgroundQueueForNewQuote, ^(void) {
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        BOOL isSomethingWrongHappened = FALSE;
                        @try
                        {
                            NSString *strItem = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:countval]objectForKey:@"StockCode"]];
                            
                            //Delete the record from offline db
                            BOOL y1 =  [db executeUpdate:@"DELETE FROM `soquodet` WHERE `QUOTE_NO` = ? AND `ITEM` = ?",strQuoteNumRecived,strItem];
                            
                            
                            //[arrProducts removeObjectAtIndex:countval];
                            
                            if (!y1)
                            {
                                isSomethingWrongHappened = TRUE;
                                NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strSuccessMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                [self doAfterdelete];
                            });
                            
                            
                        }
                        @catch (NSException* e) {
                            *rollback = YES;
                            // rethrow if not one of the two exceptions above
                            
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                                
                            });
                            
                            
                        }
                        
                        
                    }];
                    
                });
            }
            //False
            else{
                
                NSString *strErrorMessage = [[[[[dictResponse objectForKey:@"SOAP-ENV:Envelope"] objectForKey:@"SOAP-ENV:Body"] objectForKey:@"ns1:DeleteQuoteProductsResponse"] objectForKey:@"message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
        
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
    
    if ([strWebserviceType isEqualToString:@"WS_GET_QUOTE_NUMBER"]) {
        [self callGetNewQuoteNumberFromDB];
        
    }
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_SAVE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:SAVE_LOCAL_MESSAGE delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = TAG_200;
        [alertView show];
        
    }
    if ([strWebserviceType isEqualToString:@"WS_QUOTE_EDIT_SAVE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:SERVER_OFFLINE_MESSAGE message:SAVE_LOCAL_MESSAGE delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = TAG_300;
        [alertView show];
    }
    
}


#pragma mark - Compose Mail

-(IBAction)openMail:(id)sender{
    
    [self callMailComposer];
}

-(void)callMailComposer{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
            [self displayComposerSheet];
        else
            [self launchMailAppOnDevice];
    }
    
    else
    {
        [self launchMailAppOnDevice];
    }
}


// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet{
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    
    picker.mailComposeDelegate = self;
    [picker setSubject:@""];
    
    // Set up recipients
    //[picker setCcRecipients:[NSArray arrayWithObject:CC_RECIPIENT]];
    [picker setBccRecipients:nil];
    
    // [picker setToRecipients:[NSArray arrayWithObject:TO_RECIPIENT]];
    
    //Attachment
    NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"sample.html"];
    
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    [picker addAttachmentData:fileData mimeType:@"text/html" fileName:@"sample.html"];
    
    [picker setMessageBody:@"" isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:Nil];
    
    if(picker) picker=nil;
    
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString* alertMessage;
    // message.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            alertMessage = @"Email composition cancelled";
            break;
        case MFMailComposeResultSaved:
            alertMessage = @"Your e-mail has been saved successfully";
            
            break;
        case MFMailComposeResultSent:
            alertMessage = @"Your email has been sent successfully";
            
            break;
        case MFMailComposeResultFailed:
            alertMessage = @"Failed to send email";
            
            break;
        default:
            alertMessage = @"Email Not Sent";
            
            break;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark
#pragma mark Workaround
#pragma mark
// Launches the Mail application on the device.

-(void)launchMailAppOnDevice{
    
    NSString *recipients = @"mailto:?cc=&subject=";
    NSString *body = @"&body=";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}

#pragma mark - Print Method
-(IBAction)sendToPrinter:(id)sender
{
    NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"sample.html"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    UIPrintInteractionController *print = [UIPrintInteractionController sharedPrintController];
    
    print.delegate = self;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    //printInfo.jobName = [appDelegate.pdfFilePath lastPathComponent];
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    print.printInfo = printInfo;
    print.showsPageRange = YES;
    print.printingItem = fileData;
    UIViewPrintFormatter *viewFormatter = [self.view viewPrintFormatter];
    viewFormatter.startPage = 0;
    print.printFormatter = viewFormatter;
    
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {};
    
//    [print presentAnimated:YES completionHandler:completionHandler];
      [print presentFromRect:self.view.bounds inView:self.view animated:YES completionHandler:completionHandler];
}


#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            
            /*if (EditableMode == NO) {
             if ([strWebserviceType isEqualToString:@"WS_GET_QUOTE_NUMBER"]) {
             [self callGetNewQuoteNumberFromDB];
             
             }
             }
             else{
             
             }*/
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
    
}

#pragma mark - Select All

- (IBAction)actionSelectAll:(id)sender {
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    operationQueue = [[NSOperationQueue alloc] init];
    isSelectAll  = YES;
    line_no_selectAll = 1;
    
    [operationQueue addOperationWithBlock:^{

        for (int i = 0; i < arrProducts.count; i++) {
            
            strExclSelectAll = nil;
            strExtensionSelectAll = nil;
            strTaxSelectAll = nil;
            strSellingPrice=nil;
            strPriceSelectAll=nil;
            
            
            dictProductDetails = [NSMutableDictionary new];
            dictProductDetails = [arrProducts objectAtIndex:i];
            isTaxApplicable = YES;
            
            //Cost Price
            strPriceSelectAll = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"AverageCost"]];
            
            
            if ([dictProductDetails objectForKey:@"SellingPrice"]) {
                strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"SellingPrice"]];
            }
            
            
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue inDatabase:^(FMDatabase *db) {
                [self callGetTaxInfoFromDB:db];
                [self callGetDiscountFromDB:db];
                [self calcMargin];
                
                if(floatProfitPercentage <= 0)
                {
                }
                strExclSelectAll= [NSString stringWithFormat:@"%f",[self calcExcl]];
                strTaxSelectAll= [NSString stringWithFormat:@"%f",[self calcTax]];
                strExtensionSelectAll= [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:strExclSelectAll forKey:@"Gross"];
                [dict setValue:strExtensionSelectAll forKey:@"Extension"];
                [dict setValue:strTaxSelectAll forKey:@"Tax"];
                [dict setValue:strSellingPrice forKey:@"SellingPrice"];
                
                [self setPriceOfProduct:[strExtensionSelectAll floatValue] ProductIndex:i QuantityOrdered:1  DictCalculation:dict ];
                
                
            }];
            databaseQueue = nil;
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                spinner.mode = MBProgressHUDModeIndeterminate;
                [spinner removeFromSuperview];
                [tblDetails reloadData];

            }];
        }
//        [tblDetails reloadData];
    }];
}


#pragma mark - Database Calls
-(void)callGetPriceFromDB:(FMDatabase *)db{
    NSString *strDebtor_id;
    NSString *isnew = [dictProductDetails objectForKey:@"isNewProduct"];
    if ([isnew isEqualToString:@"1"]) {
        strDebtor_id = strDebtorNum;
    }
    else{
        strDebtor_id = strCopyProfileDebtorId;
    }

    FMResultSet *rs1;
    @try {
        rs1 = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strDebtor_id];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            NSDictionary *dictData = [rs1 resultDictionary];
            
            //NSLog(@"dictData %@",dictData);
            
            switch ([[dictData objectForKey:@"PRICE_CODE"] intValue]) {
                case 0:
                {
                    if ([dictProductDetails objectForKey:@"Price1"]) {
                        strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                    }
                    else if([dictProductDetails objectForKey:@"Price"]){
                       strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                    }
                    
                }
                    break;
                case 1:
                {
                    strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
                    
                case 2:
                {
                    strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price2"]];
                }
                    break;
                    
                case 3:
                {
                    strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price3"]];
                }
                    break;
                    
                case 4:
                {
                    strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price4"]];
                }
                    break;
                    
                case 5:
                {
                    strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price5"]];
                }
                    break;
                    
                    
                default:{
                   strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price1"]];
                }
                    break;
            }
            
            if ([strSellingPrice isEqual:[NSNull null]] || [strSellingPrice isEqualToString:@"(null)"]) {
                if([dictProductDetails objectForKey:@"Price"]){
                   strSellingPrice = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"Price"]];
                }
                
            }
            
        }
        
        [rs1 close];
        [self CalculatePrice];
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}
-(void)CalculatePrice
{
    [self calcMargin];
    if (floatProfitPercentage > MARGIN_MINIMUM_PERCENT_QUOTE) {
        strExclSelectAll= [NSString stringWithFormat:@"%f",[self calcExcl]];
        strTaxSelectAll= [NSString stringWithFormat:@"%f",[self calcTax]];
        strExtensionSelectAll = [NSString stringWithFormat:@"%f",[self calcExtnIncl]];
        
//        [tblDetails reloadData];
    }
}
-(void)callGetTaxInfoFromDB:(FMDatabase *)db{
    
    @try {
        FMResultSet *rs1 = [db executeQuery:@"SELECT TAX_CODE1 FROM sysistax WHERE CODE = ?",[dictProductDetails objectForKey:@"StockCode"]];
        
        if (!rs1)
        {
            [rs1 close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs1 next]) {
            
            NSDictionary *dictData = [rs1 resultDictionary];
            [rs1 close];
            
            if ([[dictData objectForKey:@"TAX_CODE1"] intValue] == -1) {
                isTaxApplicable = NO;
            }
            else{
               isTaxApplicable = YES;
            }
            
            dictData = nil;
        }
        
    }@catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

-(void)callGetDiscountFromDB:(FMDatabase *)db{
    
    @try {
        [self callGetPriceFromDB:db];
        
    }
    @catch (NSException* e) {
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
        
    }
    
}

- (IBAction)saveProductDetails:(id)sender{
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController removeRightMostViewFromStack];
}

#pragma mark - Custom Methods
-(float)calcTax{
    //If tax applicable
    if (isTaxApplicable) {
        //Tax is 10% of total
        return  (1*[strSellingPrice floatValue]*0.1);//Quantity Order = 1
        
    }
    
    return 0;
}

-(float)calcExcl{
    return  (1*[strSellingPrice floatValue]);//Quantity Order = 1

}


-(float)calcExtnIncl{
    return ([self calcExcl] + [self calcTax]);
}

-(void)calcMargin{
    float floatCostPrice = [strPriceSelectAll floatValue];//Quantity Order = 1

    //Selling price
    float floatSellingPrice = [strSellingPrice floatValue];
    //Profit percentage
    floatProfitPercentage = ((floatSellingPrice - floatCostPrice)/floatSellingPrice)*100;
}

@end
