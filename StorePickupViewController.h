//
//  StorePickupViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReasonListPopOverViewController.h"
#import "StockCodeViewController.h"

@protocol StorePickupViewDelegate
- (void)dismissStorePickUp:(NSString *)strData;
@end

@interface StorePickupViewController : UIViewController<DismissPopOverDelegateForReasonDelegate,UITextFieldDelegate,UITableViewDelegate,UIPopoverControllerDelegate>{
    id<StorePickupViewDelegate> __unsafe_unretained  delegate;
    
    UIPopoverController *popoverController;
    int tagTxtReason;
    
    UITableView *tblView;
  
  
}
@property(unsafe_unretained)id<StorePickupViewDelegate>delegate;
@property(nonatomic,retain) UIPopoverController *popoverController;

@property(assign)IBOutlet UILabel *lblReason1;
@property(assign)IBOutlet UILabel *lblReason2;
@property(assign)IBOutlet UILabel *lblReason3;
@property(assign)IBOutlet UILabel *lblReason4;
@property(assign)IBOutlet UILabel *lblReason5;
@property (assign) IBOutlet UILabel *lblReason7;
@property(assign)IBOutlet UILabel *lblReason6;
@property (assign) IBOutlet UILabel *lblReason8;

@property (assign) IBOutlet UITextField *txtStockCode1;
@property (assign) IBOutlet UITextField *txtStockCode2;
@property (assign) IBOutlet UITextField *txtStockCode3;
@property (assign) IBOutlet UITextField *txtStockCode4;
@property (assign) IBOutlet UITextField *txtStockCode5;
@property (assign) IBOutlet UITextField *txtStockCode6;
@property (assign) IBOutlet UITextField *txtStockCode7;
@property (assign) IBOutlet UITextField *txtStockCode8;

@property (assign) IBOutlet UITextField *txtQuantity1;
@property (assign) IBOutlet UITextField *txtQuantity2;
@property (assign) IBOutlet UITextField *txtQuantity3;
@property (assign) IBOutlet UITextField *txtQuantity4;
@property (assign) IBOutlet UITextField *txtQuantity5;
@property (assign) IBOutlet UITextField *txtQuantity7;
@property (assign) IBOutlet UITextField *txtQuantity6;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity8;

@property (assign) IBOutlet UITextField *txtReturn1;
@property (assign) IBOutlet UITextField *txtReturn2;
@property (assign) IBOutlet UITextField *txtReturn3;
@property (assign) IBOutlet UITextField *txtReturn4;
@property (assign) IBOutlet UITextField *txtReturn5;
@property (assign) IBOutlet UITextField *txtReturn6;
@property (assign) IBOutlet UITextField *txtReturn7;
@property (assign) IBOutlet UITextField *txtReturn8;



@property (assign) IBOutlet UITextField *txtComment1;
@property (assign) IBOutlet UITextField *txtComment2;
@property (assign) IBOutlet UITextField *txtComment3;
@property (assign) IBOutlet UITextField *txtComment4;
@property (assign) IBOutlet UITextField *txtComment5;
@property (assign) IBOutlet UITextField *txtComment6;
@property (assign) IBOutlet UITextField *txtComment7;
@property (assign) IBOutlet UITextField *txtComment8;

-(IBAction)actionDismiss:(id)sender;
@end
