//
//  DeliveryRunViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetDeliveryDateDelegate
- (void)setDeliveryDate:(NSString *)strDelDate;
- (void)dismissPopover;

@optional
- (void)setCallDate:(NSString *)strDelDate;


@end

@interface DatePopOverController : UIViewController{
    
    id<SetDeliveryDateDelegate> __unsafe_unretained  delegate;
}


@property(nonatomic,unsafe_unretained)BOOL isStockPickup;
@property(nonatomic,unsafe_unretained)BOOL isCallDate;


@property(unsafe_unretained)id<SetDeliveryDateDelegate>delegate;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *popTitleLbl;

@property(nonatomic,unsafe_unretained)IBOutlet UIDatePicker *datePicker;
@property(nonatomic,unsafe_unretained)IBOutlet UILabel *lblDate;

@end
