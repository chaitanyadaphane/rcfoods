//
//  CustomerOrderFormViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import <MessageUI/MessageUI.h>
#define TAG_50 50
#define TAG_100 100
#import "NDHTMLtoPDF.h"
@interface CustomerOrderFormViewController : UIViewController<UIPrintInteractionControllerDelegate,ReachabilityRequest_delegate,MFMailComposeViewControllerDelegate>{
    NSArray *arrHeaderLabels;
    
    NSString *strWebserviceType;
    NSString *strCustomerCode;
    
    NSMutableDictionary *dictCustomerOrderFormDetails;
    
    dispatch_queue_t backgroundQueueForViewCustomerOrderForm;
    FMDatabaseQueue *dbQueueViewCustomerOrderForm;
    
    BOOL isNetworkConnected;
    SEFilterControl *segControl;
   
}

@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSString *strCustomerCode;
@property (nonatomic,retain) NSMutableDictionary *dictCustomerOrderFormDetails;
@property (nonatomic,retain) NSMutableArray *arrCustomerTransactions;
@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoTransactions;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;
@property  NSString *strCustCatagory;

-(void)callWSGetCustomerOrderForm;
-(void)callGetCustomerOrderFormFromDB:(FMDatabase *)db;

@end
