//
//  Reachability_Centralized.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 23/02/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "Reachability_Centralized.h"

@implementation Reachability_Centralized
@synthesize delegate;
@synthesize internetReachable;

-(void)setReachabilityObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifyNetworkReachability:) name:kReachabilityChangedNotification object:nil];
    //self.internetReachable = [Reachability reachabilityForInternetConnection];
    self.internetReachable = [Reachability reachabilityWithHostName:HOST_NAME];
    [internetReachable startNotifier];
}

-(void) notifyNetworkReachability:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus newtworkStatus = [internetReachable currentReachabilityStatus];
    
    //--If Demo Application always run in offline Mode
#ifdef DemoAppTest
    {
        NSLog(@"Welcome to DemoAppTest");
        newtworkStatus =  NotReachable;
    }
    
#endif
    
    [delegate checkNetworkStatus:newtworkStatus];
}
@end
