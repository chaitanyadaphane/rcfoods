//
//  QuoteStatusDetailViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 07/03/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "QuoteStatusDetailViewController.h"
#import "CustomCellGeneral.h"


@interface QuoteStatusDetailViewController ()

@end

 MBProgressHUD *spinner;
NSMutableArray *aryProducts;
NSString *strHTLM ;
@implementation QuoteStatusDetailViewController
@synthesize dictProductDetails,isQuoteApproved;
@synthesize lblQuoteName,btnPrint,btnEmail,tblDetails,strQuoteNum;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
//        
    }
    return self;
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    // Path to the plist (in the application bundle)
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"QuoteStatusDetails" Type:@"plist"];
    
    // Build the array from the plist
    arrQuoteLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    lblQuoteName.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"QuoteNo"] objectForKey:@"text"]];
    self.vwProductList.hidden = YES;
    [self.segControl setTintColor:[UIColor colorWithRed:14.0/255.0 green:121.0/255.0 blue:255.0/255.0 alpha:1.0]];
    
    if([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected )
    [self callWSGetAvailable];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please check your internet connection" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    btnEmail.hidden = YES;
    btnPrint.hidden = YES;
    
    if([dictProductDetails objectForKey:@"CustOrder"])
    {
        if([[dictProductDetails objectForKey:@"CustOrder"]isKindOfClass:[NSDictionary class]])
        {
            if([[[dictProductDetails objectForKey:@"CustOrder"]objectForKey:@"text"]isEqualToString:@"A"])
            {
                btnEmail.hidden = NO;
                btnPrint.hidden = NO;

            }
        }
    }
    
    btnEmail.hidden = NO;
    btnPrint.hidden = NO;
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.lblQuoteName = nil;
    self.btnPrint = nil;
    self.btnEmail = nil;
    self.tblDetails = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 60;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 50)
    return [arrQuoteLabels count];
    return [aryProducts count];
    
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    CustomCellGeneral *cell = nil;
    
    if(tableView.tag == 100)
    {
        cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:@"cellIdentifier15"];
        
        
        if (cell == nil)
        {
            NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
            
            cell = [nib objectAtIndex:14];
            cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];
            
        }

        cell.backgroundColor = [UIColor clearColor];
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"] objectForKey:@"text"]];
        if ([[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] isKindOfClass:[NSString class]] || [[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] isKindOfClass:[NSNumber class]]) {
            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] floatValue]];
            [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
            [cell.lblValue setTextAlignment:NSTextAlignmentRight];
            return cell;
        }
      else if ([[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] objectForKey:@"text"]) {
            cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[[aryProducts objectAtIndex:[indexPath row]] objectForKey:@"Extension"] objectForKey:@"text"] floatValue]];
          [[AppDelegate getAppDelegateObj]ConvertStringIntoCurrencyFormat:cell.lblValue];
          [cell.lblValue setTextAlignment:NSTextAlignmentRight];
          return cell;

        }
        else{
            cell.lblValue.text = @"";
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        return cell;
        
    }
    static NSString *CellIdentifier = CELL_IDENTIFIER5;
    
    cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:4];
        
    }
    
    cell.lblTitle.text = [[arrQuoteLabels objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    if ([indexPath row] == 0) {
     //   Header
       // [[dictProductDetails objectForKey:@"Debtor"] objectForKey:@"text"]
        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Debtor"] objectForKey:@"text"]];
    }
    
    if ([indexPath row] == 1) {
        
        cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Delname"] objectForKey:@"text"]];
    }
    
    
    if ([indexPath row] == 2) {
        if ([[[dictProductDetails objectForKey:@"Header"] objectForKey:@"DateRaised"] objectForKey:@"text"] && ![[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"DateRaised"] objectForKey:@"text"] isEqualToString:STANDARD_APP_DATE] && ![[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"DateRaised"] objectForKey:@"text"] isEqualToString:STANDARD_SERVER_DATE]) {
            NSString *strDate = [[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"DateRaised"] objectForKey:@"text"]];
            cell.lblValue.text = [NSString stringWithFormat:@"%@",strDate];
        }

    }
    
    return cell;
    
}


-(IBAction)openMail:(id)sender{
    
    NSString *strProducts = @"";
    NSString *strProductshtml = @"";
    
    //Read from HTML file
    NSError* error = nil;
    
    NSString *path = [[NSBundle mainBundle] pathForResource: @"QuoteStatus" ofType: @"html"];
    strHTLM = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
    
    
    if(aryProducts.count > 0)
    {
        for (int j = 0; j < aryProducts.count; j++) {
            strProducts = [NSString stringWithFormat:@"<tr><td valign=\"middle\" class=\"td7\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td7\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td8\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td9\">"
                           "<p class=\"p2\">%@</p>"
                           "</tr>",[[[aryProducts objectAtIndex:j] objectForKey:@"Item"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Description"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Price"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Tax"] objectForKey:@"text"]];
            
            // s = [NSString stringWithFormat:@"<br>%@</br>",strProducts];
            
            strProductshtml = [strProductshtml stringByAppendingString:strProducts];
            
        }
    }
    
    strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"CustoMerOrderToReplace" withString:strProductshtml];
    strHTLM =   [strHTLM stringByReplacingOccurrencesOfString:@"trim($code)" withString:[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Debtor"] objectForKey:@"text"]];
    
    strHTLM =  [strHTLM stringByReplacingOccurrencesOfString:@"trim($name)" withString:[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Delname"] objectForKey:@"text"]];
    
     /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileInDirectory1 = [documentsDirectory stringByAppendingPathComponent:@"TestNEW.html"];
    [strHTLM writeToFile:fileInDirectory1 atomically:YES];
    */
    
    [self callMailComposer];

}
-(void)callMailComposer{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
            [self displayComposerSheet];
        else
            [self launchMailAppOnDevice];
    }
    
    else
    {
        [self launchMailAppOnDevice];
    }
}
- (IBAction)SegmentControlPressed:(id)sender {
    
    if(self.segControl.selectedSegmentIndex == 0)
    {
        self.vwProductList.hidden = YES;
        self.vwHeader.hidden = NO;
    }
    
   else if(self.segControl.selectedSegmentIndex == 1)
    {
        self.vwProductList.hidden = NO;
        self.vwHeader.hidden = YES;

        
    }
    
    
}

#pragma mark -
#pragma mark Compose Mail
#pragma mark

// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet{
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    
    picker.mailComposeDelegate = self;
    NSString *tosubject =@"";
    [picker setSubject:tosubject];
    NSArray *toRecipents = [NSArray arrayWithObject:@"orders@rumcityfoods"];
    [picker setToRecipients:toRecipents];

    // Set up recipients
    //[picker setBccRecipients:nil];
    [picker setMessageBody:strHTLM isHTML:YES];
    [self presentViewController:picker animated:YES completion:Nil];
    
    if(picker) picker=nil;
    
}



// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString* alertMessage;
    // message.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            alertMessage = @"Email composition cancelled";
            break;
        case MFMailComposeResultSaved:
            alertMessage = @"Your e-mail has been saved successfully";
            
            break;
        case MFMailComposeResultSent:
            alertMessage = @"Your email has been sent successfully";
            
            break;
        case MFMailComposeResultFailed:
            alertMessage = @"Failed to send email";
            
            break;
        default:
            alertMessage = @"Email Not Sent";
            
            break;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark
#pragma mark Workaround
#pragma mark
// Launches the Mail application on the device.

-(void)launchMailAppOnDevice{
    
    NSString *recipients = @"mailto:?cc=&subject=";
    NSString *body = @"&body=";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}

#pragma mark - Print Method
-(IBAction)sendToPrinter:(id)sender
{
    NSString *strProducts = @"";
    NSString *strProductshtml = @"";
    
    //Read from HTML file
    NSError* error = nil;
    
    NSString *path = [[NSBundle mainBundle] pathForResource: @"QuoteStatus" ofType: @"html"];
    strHTLM = [NSString stringWithContentsOfFile: path encoding:NSUTF8StringEncoding error: &error];
    
    
    if(aryProducts.count > 0)
    {
        for (int j = 0; j < aryProducts.count; j++) {
            strProducts = [NSString stringWithFormat:@"<tr><td valign=\"middle\" class=\"td7\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td7\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td8\">"
                           "<p class=\"p2\">%@</p>"
                           "</td>"
                           "<td valign=\"middle\" class=\"td9\">"
                           "<p class=\"p2\">%@</p>"
                           "</tr>",[[[aryProducts objectAtIndex:j] objectForKey:@"Item"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Description"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Price"] objectForKey:@"text"],[[[aryProducts objectAtIndex:j] objectForKey:@"Tax"] objectForKey:@"text"]];
            
            // s = [NSString stringWithFormat:@"<br>%@</br>",strProducts];
            
            strProductshtml = [strProductshtml stringByAppendingString:strProducts];
            
        }
    }
    
    strHTLM = [strHTLM stringByReplacingOccurrencesOfString:@"CustoMerOrderToReplace" withString:strProductshtml];
    
    strHTLM =   [strHTLM stringByReplacingOccurrencesOfString:@"trim($code)" withString:[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Debtor"] objectForKey:@"text"]];
    
    strHTLM =  [strHTLM stringByReplacingOccurrencesOfString:@"trim($name)" withString:[[[dictProductDetails objectForKey:@"Header"] objectForKey:@"Delname"] objectForKey:@"text"]];

    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:strHTLM pathForPDF:[@"~/Documents/QuoteStatus.pdf" stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
        
        //                           self.PDFCreator = [NDHTMLtoPDF createPDFWithURL:[NSURL URLWithString:@"http://edition.cnn.com/2013/09/19/opinion/rushkoff-apple-ios-baby-steps/index.html"] pathForPDF:[@"~/Documents/blocksDemo.pdf" stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
        //  NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
        [self printPRDF:htmlToPDF.PDFpath];
        
    } errorBlock:^(NDHTMLtoPDF *htmlToPDF) {
        NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
        NSLog(@"%@",result);
        
    }];

    
}

-(void)printPRDF:(NSString *)result
{
    NSData *fileData = [NSData dataWithContentsOfFile:result];
    
    UIPrintInteractionController *print = [UIPrintInteractionController sharedPrintController];
    
    print.delegate = self;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    //printInfo.jobName = [appDelegate.pdfFilePath lastPathComponent];
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    print.printInfo = printInfo;
    print.showsPageRange = YES;
    print.printingItem = fileData;
    UIViewPrintFormatter *viewFormatter = [self.view viewPrintFormatter];
    viewFormatter.startPage = 0;
    print.printFormatter = viewFormatter;
    
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
    };
//    [print presentAnimated:YES completionHandler:completionHandler];
      [print presentFromRect:self.view.bounds inView:self.view animated:YES completionHandler:completionHandler];
    
}
#pragma mark - Create Html file
-(void)createHtmlFile{
    // get user input
    
    NSString *userText = @"Hello, world!";
    
    // build the HTML
    
    NSString *html = [NSString stringWithFormat:@"<html><body>%@</body><html>", userText];
    
    // build the path where you're going to save the HTML
    
    NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filename = [docsFolder stringByAppendingPathComponent:@"sample.html"];
    
    // save the NSString that contains the HTML to a file
    
    NSError *error;
    [html writeToFile:filename atomically:NO encoding:NSUTF8StringEncoding error:&error];
}


-(void)callWSGetAvailable{

    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *parameters = [NSString stringWithFormat:@"quoteno=%@",strQuoteNum];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,QuoteDetails_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    NSLog(@"strQuoteNum:::%@",strQuoteNum);
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}
#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    @try {
        NSMutableData *responseData = (NSMutableData*)resData;
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        
        
         NSLog(@"dictResponse %@",dictResponse);
         
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *documentsDirectory = [paths objectAtIndex:0];
         NSString *fileInDirectory1 = [documentsDirectory stringByAppendingPathComponent:@"TESTNEW.plist"];
         [dictResponse writeToFile:fileInDirectory1 atomically:YES];
        dictProductDetails = [[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"];
        
        if([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Detail"] objectForKey:@"Lines"]isKindOfClass:[NSArray class]])
        {
            aryProducts = [[NSMutableArray alloc]initWithArray:[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Detail"] objectForKey:@"Lines"]];
        }
        
        else if([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Detail"] objectForKey:@"Lines"]isKindOfClass:[NSDictionary class]])
        {
            aryProducts = [[NSMutableArray alloc]initWithObjects:[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Quote"] objectForKey:@"Detail"] objectForKey:@"Lines"], nil];
        }

        
        
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:exception.description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    @finally {
       // [self.tblDetails reloadata ];
        _tblData.dataSource = self;
        _tblData.delegate = self;
        [_tblData reloadData];
        _tblProductList.dataSource = self;
        _tblProductList.delegate = self;
        [_tblProductList reloadData];
        self.vwHeader.hidden = NO;
        self.vwProductList.hidden = YES;
        [spinner removeFromSuperview];
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    [spinner removeFromSuperview];
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

@end
