//
//  ViewController.m
//  JFDepthVewExample
//
//  Created by Jeremy Fox on 10/17/12.
//  Copyright (c) 2012 Jeremy Fox. All rights reserved.

//  Chuck Norris Picture Credit: http://www.reactionface.info/sites/default/files/images/1313574161997.jpg

#import "TopViewController.h"
#import "CustomCellGeneral.h"
@interface TopViewController ()

@end

@implementation TopViewController
@synthesize arrHeaderLabels,strCode,dictProductDetails,tblProductDetails,navBar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"DashboardProductDetail" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    
    self.navBar.topItem.title = [dictProductDetails objectForKey:@"DESCRIPTION"];
    
    NSLog(@"dictProductDetails %@",dictProductDetails);
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    self.topView= nil;
    self.navBar = nil;
    self.tblProductDetails = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
   // [operationQueue cancelAllOperations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - iOS 5 Rotation Support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    NSLog(@"Top View Controller Received didRotateFromInterfaceOrientation: event from JFDepthView");
}

#pragma mark - iOS 6 Rotation Support

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationMaskAll;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (IBAction)closeView:(id)sender {
    [self.depthViewReference dismissPresentedViewInView:self.presentedInView];
}


#pragma mark - Table view delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return [arrHeaderLabels count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    if (section == 2)
//    {
//        return 20;
//    }
//    else
//    {
//        return 0.0;
//    }
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    
//    if (section == 2)
//    {
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width,20)];
//    
//    
//        UILabel *headerLabel;
//        
//        if(IOS_Version < 7.0)
//        {
//            headerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, headerView.frame.size.width-120.0, headerView.frame.size.height)];
//        }
//        else
//        {
//            headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 5, headerView.frame.size.width-120.0, headerView.frame.size.height)];
//        }
//        
//        headerLabel.textAlignment = UITextAlignmentLeft;
//        headerLabel.font = [UIFont boldSystemFontOfSize:18];
//        headerLabel.text = @"Price";
//        headerLabel.backgroundColor = [UIColor clearColor];
//        
//        [headerView addSubview:headerLabel];
//    
//        return headerView;
//    }
//    else
//    {
//        return nil;
//    }
//}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 2) {
        return @"Price";
    }
    // Return the displayed title for the specified section.
    return nil;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier;
    
    CellIdentifier = CELL_IDENTIFIER4;
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:3];
        
    }
    
    cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
    
    cell.txtValue.text = nil;
    
    if ([indexPath section] == 0) {
        switch ([indexPath row]) {
            case 0:
                
                if ([dictProductDetails objectForKey:@"CODE"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"CODE"] trimSpaces]];
                }
                
                break;
                
            default:
                break;
        }
    }
    
    if ([indexPath section] == 1) {
        switch ([indexPath row]) {
            case 0:
                if ([dictProductDetails objectForKey:@"WAREHOUSE"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"WAREHOUSE"] trimSpaces]];
                    
                }
                break;
//            case 1:
//                if ([dictProductDetails objectForKey:@"AVERAGE_COST"]) {
//                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"AVERAGE_COST"] floatValue]];
//                }
//                break;
//            case 2:
//                if ([dictProductDetails objectForKey:@"MARGIN01"]) {
//                    cell.txtValue.text = [NSString stringWithFormat:@"%@%%",[dictProductDetails objectForKey:@"MARGIN01"]];
//                }
//                break;
            case 1:
                if ([dictProductDetails objectForKey:@"UNITS"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"UNITS"] trimSpaces]];
                }
                break;
            case 2:
                if ([dictProductDetails objectForKey:@"STATUS"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"STATUS"] trimSpaces]];
                }
                break;
            case 3:
                if ([dictProductDetails objectForKey:@"UNITS_PER_PALLT"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"UNITS_PER_PALLT"]];
                }
                break;
            case 4:
                if ([dictProductDetails objectForKey:@"UNITS_PER_LAYER"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"UNITS_PER_LAYER"]];
                }
                break;
            case 5:
                if ([dictProductDetails objectForKey:@"LOCATION"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[dictProductDetails objectForKey:@"LOCATION"] trimSpaces]];
                }
                break;
            case 6:
                if ([dictProductDetails objectForKey:@"ON_HAND"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"ON_HAND"]];
                }
                break;
            case 7:
                if ([dictProductDetails objectForKey:@"PURCHASE_ORDER"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"PURCHASE_ORDER"]];
                }
                break;
                //Date of arrival
            case 8:
                if ([dictProductDetails objectForKey:@"DELIVERY_DATE"]&&![[dictProductDetails objectForKey:@"DELIVERY_DATE"] isEqual:[NSNull null]] && ![[dictProductDetails objectForKey:@"DELIVERY_DATE"] isEqualToString:STANDARD_APP_DATE] && ![[dictProductDetails objectForKey:@"DELIVERY_DATE"] isEqualToString:STANDARD_SERVER_DATE]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] setDateOnlyInAustrailianFormat:[dictProductDetails objectForKey:@"DELIVERY_DATE"]]];
                }
                break;
            case 9:
                if ([dictProductDetails objectForKey:@"ALLOCATED"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"ALLOCATED"]];
                }
                break;
            case 10:
                if ([dictProductDetails objectForKey:@"AVAILABLE"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"%@",[dictProductDetails objectForKey:@"AVAILABLE"]];
                }
                break;
                
            default:
                break;
        }
    }
    
    if ([indexPath section] == 2) {
        switch ([indexPath row]) {
            case 0:
                if ([dictProductDetails objectForKey:@"PRICE1"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"PRICE1"] floatValue]];
                }
                break;
            case 1:
                if ([dictProductDetails objectForKey:@"PRICE2"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"PRICE2"] floatValue]];
                }
                break;
            case 2:
                if ([dictProductDetails objectForKey:@"PRICE3"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"PRICE3"] floatValue]];
                }
                break;
            case 3:
                if ([dictProductDetails objectForKey:@"PRICE4"]) {
                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"PRICE4"] floatValue]];
                }
                break;
//            case 4:
//                if ([dictProductDetails objectForKey:@"PRICE5"]) {
//                    cell.txtValue.text = [NSString stringWithFormat:@"$%.2f",[[dictProductDetails objectForKey:@"PRICE5"] floatValue]];
//                }
//                break;
            default:
                break;
        }
    }
    
    
    return cell;
    
}
@end
