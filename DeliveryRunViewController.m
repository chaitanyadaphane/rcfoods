//
//  DeliveryRunViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 10/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "DeliveryRunViewController.h"
#import "CustomCellGeneral.h"

#define DELIVERY_RUN_WS @"salesorder/deliveryRunList.php?"
#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface DeliveryRunViewController ()
{
    MBProgressHUD *spinner;
}
@end

@implementation DeliveryRunViewController
@synthesize tblDeliveryRun,arrDeliveryRun,delegate,lastIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
           }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrDeliveryRun = [[NSMutableArray alloc] init];
    
//    [self callWSGetDeliveryRunList];
    
    if (![AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected) {
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetDeliveryRunListFromDB:db];
            
        }];
    }
    else{
        [self callWSGetDeliveryRunList];

    }
   
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblDeliveryRun = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Database calls
-(void)callGetDeliveryRunListFromDB:(FMDatabase *)db{
    FMResultSet *rs;
    @try {
        //Get Customer list
        rs = [db executeQuery:@"SELECT `CODE` as Code, `RECORD_TYPE`, `DESCRIPTION` as Description FROM `sysdesc` WHERE `RECORD_TYPE` = 'RUNCD' ORDER BY `CODE` ASC"];
    
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {            
            NSDictionary *dictData = [rs resultDictionary];
            [arrDeliveryRun addObject:dictData];
        }
        
        [rs close];
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}


#pragma mark - WS calls
-(void)callWSGetDeliveryRunList
{
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    strWebserviceType = @"WS_DELIVERY_RUN";
    
    //Fetch last sync date
    //NSString *last_sync_date = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"armaster"];
    // NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d&last_syncDate=%@",pageIndex,ROWS_PER_PAGE,last_sync_date];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@",WSURL,DELIVERY_RUN_WS];
    
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.arrDeliveryRun count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER13;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:12];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;

    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arrDeliveryRun objectAtIndex:[indexPath row]] objectForKey:@"Code"]];
    cell.lblTitle.text =  [NSString stringWithFormat:@"%@",[[arrDeliveryRun objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
    
    if (lastIndexPath != nil && lastIndexPath.row == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomCellGeneral* newCell = (CustomCellGeneral *)[tableView cellForRowAtIndexPath:indexPath];
    int newRow = [indexPath row];
    int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
    
    if(newRow != oldRow)
    {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        lastIndexPath = indexPath;
    }
    
    NSString *strCode = [[[arrDeliveryRun objectAtIndex: indexPath.row] objectForKey:@"Code"] stringByReplacingOccurrencesOfString:@" " withString:@""];

    
    [self.delegate setDeliveryRun:strCode];
}


#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    NSLog(@"%@", dictResponse);
    
    //[spinner hideLoadingView];
    
    NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    [spinnerObj removeFromSuperview];
    
    if ([strWebserviceType isEqualToString:@"WS_DELIVERY_RUN"]){
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"DelRun"] objectForKey:@"Flag"]){
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"DelRun"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                
                NSMutableArray *arrDelRunTemp =[[[dictResponse objectForKey:@"Response"] objectForKey:@"DelRun"] objectForKey:@"Data"];
                if ([arrDelRunTemp count] > 0) {
                    for (int i = 0; i< arrDelRunTemp.count; i++) {
                        NSMutableDictionary *dict = [NSMutableDictionary new];
                        NSString *code = [[[arrDelRunTemp objectAtIndex:i]objectForKey:@"Code"]objectForKey:@"text"];
                        NSString *description =[[[arrDelRunTemp objectAtIndex:i]objectForKey:@"Description"]objectForKey:@"text"];
                        [dict setObject:code?code:@"codetest" forKey:@"Code"];
                        [dict setObject:description forKey:@"Description"];
                        [arrDeliveryRun addObject:dict];
                        
                    }
                }
                else // if service returns nil then get data from local db
                {
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue   inDatabase:^(FMDatabase *db) {
                        [self callGetDeliveryRunListFromDB:db];
                        
                    }];
                }
              
                
                
//              arrDeliveryRun = [[[dictResponse objectForKey:@"Response"] objectForKey:@"DelRun"] objectForKey:@"Data"];
                
                [tblDeliveryRun reloadData];
                
            }
            else{
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue   inDatabase:^(FMDatabase *db) {
                    [self callGetDeliveryRunListFromDB:db];
                    
                }];
                [tblDeliveryRun reloadData];

                
//                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"DelRun"] objectForKey:@"Message"] objectForKey:@"text"];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
//                [alert show];
                
            }
            
        }
        
    }
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
    
}

@end
