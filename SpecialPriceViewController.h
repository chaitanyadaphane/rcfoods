//
//  SpecialPriceViewController.h
//  Blayney
//
//  Created by Pooja Mishra on 22/11/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KGModal.h"

#define TAG_PERCENT_FIELD 300
#define TAG_PRICE_FIELD 400

@protocol SpeacialPriceUpdateDelegate
- (void)UpdateSpecialPrice:(NSMutableDictionary *)productDict;
@end
@interface SpecialPriceViewController : UIViewController
@property(strong,nonatomic) id<SpeacialPriceUpdateDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *percentTxt;
@property (weak, nonatomic) IBOutlet UITextField *priceTxt;


- (IBAction)saveSpecialAction:(id)sender;
- (IBAction)segmentedAction:(id)sender;


@property(strong,nonatomic)NSString  *selectType;
@property(strong,nonatomic)NSString *priceVal;
@property(strong, nonatomic)NSMutableDictionary *productDetailDict;
@end
