//
//  CustomerOrderFormViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CustomerOrderFormViewController.h"
#import "CustomCellGeneral.h"
#import "CustomerOrderFormProductViewController.h"

#define CUSTOMER_ORDER_FORM_WS @"custorder/view_custorder.php?"

@interface CustomerOrderFormViewController ()
{
    MBProgressHUD *spinner;
    
    NSString *strEmailBody;
    
    NSMutableArray *arrCustomerProdEmailData;
}
@end

@implementation CustomerOrderFormViewController
@synthesize arrHeaderLabels,strCustomerCode,dictCustomerOrderFormDetails;
@synthesize tblHeader,tblDetails,vwContentSuperview,vwSegmentedControl,vwHeader,vwDetails,vwNoTransactions,arrCustomerTransactions,localSpinner;
@synthesize PDFCreator;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //Set the Reachability observer
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
//        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
//        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
//        
//        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
//        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
//        
//        if(internetStatus == NotReachable)
//        {
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
//        }
//        else{
//            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
//        }
//        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *path = [AppDelegate getFileFromLocalDirectory:@"CustomerOrderFormHeader" Type:@"plist"];
    
    // Build the array from the plist
    arrHeaderLabels = [[NSArray alloc] initWithContentsOfFile:path];
    dictCustomerOrderFormDetails  = [[NSMutableDictionary alloc] init];
    arrCustomerTransactions = [[NSMutableArray alloc] init];
    arrCustomerProdEmailData = [[NSMutableArray alloc]init];
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"Headers", @"Details", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (!backgroundQueueForViewCustomerOrderForm) {
        backgroundQueueForViewCustomerOrderForm = dispatch_queue_create("com.nanan.myscmipad.bgqueueForViewCustomerOrderForm", NULL);
    }
    
    if ([AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected ) {
        [self callWSGetCustomerOrderForm];
    }
    else{
        [localSpinner startAnimating];
        dispatch_async(backgroundQueueForViewCustomerOrderForm, ^(void) {
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerOrderFormFromDB:db];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self doAfterDataFetch];
                [localSpinner stopAnimating];
            });
            
        });
    }
    
    
}


- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.tblHeader = nil;
    self.tblDetails = nil;
    self.vwContentSuperview = nil;
    self.vwSegmentedControl = nil;
    self.vwHeader = nil;
    self.vwDetails = nil;
    self.vwNoTransactions = nil;
    
}

#pragma mark - Cancel WS calls
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] cancelRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Segmented Control Actions
-(void)filterValueChanged:(SEFilterControl *) sender
{
    
    //Remove all Subviews
    [vwContentSuperview removeAllSubviews];
    
    switch (sender.SelectedIndex) {
        case 0:{
            [vwContentSuperview addSubview:vwHeader];
            [tblHeader reloadData];
        }
            break;
            
        case 1:{
            
            if ([arrCustomerTransactions count]) {
                [vwContentSuperview addSubview:vwDetails];
                [tblDetails reloadData];
            }
            else{
                //No Transactions
                [vwContentSuperview addSubview:vwNoTransactions];
            }
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark Table view data source and delegate
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    switch (tableView.tag) {
        case TAG_50:return 60;break;
            
        case TAG_100:return 50;break;
            
        default:return 0;break;
    }
    
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    switch (tableView.tag) {
        case TAG_50:return [arrHeaderLabels count];break;
            
        case TAG_100:return 1;break;
            
        default:return 0;break;
    }
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (tableView.tag) {
        case TAG_50:return [(NSArray *)[arrHeaderLabels objectAtIndex:section] count];break;
            
        case TAG_100:return [arrCustomerTransactions count];break;
            
        default:return 0;break;
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {

    static NSString *CellIdentifier5 = CELL_IDENTIFIER5;
    static NSString *CellIdentifier9 = CELL_IDENTIFIER9;
    static NSString *CellIdentifier6 = CELL_IDENTIFIER6;
    
    CustomCellGeneral *cell = nil;
    
    switch (tableView.tag) {
        case TAG_50:{
            
            cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
            
            if (cell == nil)
            {
                NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                
                cell = [nib objectAtIndex:4];
                
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.lblTitle.text = [[(NSArray *)[arrHeaderLabels objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"Label"];
            
            switch ([indexPath section]) {
                case 0:{
                    if ([indexPath row] == 0) {
                        
                        if ([[dictCustomerOrderFormDetails objectForKey:@"Customer"] isKindOfClass:[NSString class]]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[dictCustomerOrderFormDetails objectForKey:@"Customer"]];
                        }
                        
                        else  if ([[[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"] isKindOfClass:[NSString class]]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"]];
                        }
                    }
                }
                    
                    break;
                    
                case 1:{
                    if ([indexPath row] == 0) {
                        
                        if ([dictCustomerOrderFormDetails objectForKey:@"Name"]) {
                          if([[dictCustomerOrderFormDetails objectForKey:@"Name"] isKindOfClass:[NSString class]])
                          {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Name"] trimSpaces]];
                          }
                          else if([[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"])
                          {
                             cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"] trimSpaces]];
                          }
                        }
                        
                    }
                    if ([indexPath row] == 1) {
                        if ([dictCustomerOrderFormDetails objectForKey:@"Phone"]) {
                          if([[dictCustomerOrderFormDetails objectForKey:@"Phone"] isKindOfClass:[NSString class]])
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Phone"] trimSpaces]];
                          else if([[dictCustomerOrderFormDetails objectForKey:@"Phone"]objectForKey:@"text"])
                          {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerOrderFormDetails objectForKey:@"Phone"]objectForKey:@"text"] trimSpaces]];
                          }
                        }
                    }
                    
                    if ([indexPath row] == 2) {
                        if ([dictCustomerOrderFormDetails objectForKey:@"Contact"]) {
                           if([[dictCustomerOrderFormDetails objectForKey:@"Contact"] isKindOfClass:[NSString class]])
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Contact"] trimSpaces]];
                           else if([[dictCustomerOrderFormDetails objectForKey:@"Contact"]objectForKey:@"text"])
                           {
                             cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerOrderFormDetails objectForKey:@"Contact"] objectForKey:@"text"]trimSpaces]];
                           }
                        }
                    }
                    
                    if ([indexPath row] == 3) {
                        if ([dictCustomerOrderFormDetails objectForKey:@"Fax"]) {
                            if([[dictCustomerOrderFormDetails objectForKey:@"Fax"] isKindOfClass:[NSString class]])
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Fax"] trimSpaces]];
                            else if([[dictCustomerOrderFormDetails objectForKey:@"Fax"] objectForKey:@"text"])
                            {
                              cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerOrderFormDetails objectForKey:@"Fax"] objectForKey:@"text"]trimSpaces]];
                            }
                        }
                    }
                    
                    if ([indexPath row] == 4) {
                        if ([dictCustomerOrderFormDetails objectForKey:@"Email"]) {
                            if([[dictCustomerOrderFormDetails objectForKey:@"Email"]isKindOfClass:[NSString class]]){
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"Email"] trimSpaces]];
                            }
                           else if([[[dictCustomerOrderFormDetails objectForKey:@"Email"] objectForKey:@"text"]isKindOfClass:[NSString class]])
                           {
                              cell.lblValue.text = [NSString stringWithFormat:@"%@",[[[dictCustomerOrderFormDetails objectForKey:@"Email"] objectForKey:@"text"] trimSpaces]];
                           }
                        }
                    }
                    
                }
                    
                    break;
                    
                case 2:{
                    if ([indexPath row] == 0) {
                        
                        if ([[dictCustomerOrderFormDetails objectForKey:@"Balance"] isKindOfClass:[NSString class]] || [[dictCustomerOrderFormDetails objectForKey:@"Balance"] isKindOfClass:[NSNumber class]] ) {
                            if([[dictCustomerOrderFormDetails objectForKey:@"Balance"] isKindOfClass:[NSNumber class]])
                            {
                                NSNumber *balance = [dictCustomerOrderFormDetails objectForKey:@"Balance"] ;
                                float b = [balance floatValue];
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",b];
                            }
                            else
                            cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[dictCustomerOrderFormDetails objectForKey:@"Balance"]];
                        }
                        
                        else if([[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"])
                        {
                            if([[[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"] isKindOfClass:[NSNumber class]])
                            {
                                NSNumber *balance = [[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"] ;
                                float b = [balance floatValue];
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",b];
                            }
                            else
                                cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"]];
                            float b = [cell.lblValue.text floatValue];
                            cell.lblValue.text = [NSString stringWithFormat:@"$ %.2f",b];
                        }
                        
                    }
                    if ([indexPath row] == 1) {
                        if ([[dictCustomerOrderFormDetails objectForKey:@"Rate"] isKindOfClass:[NSString class]] || [[dictCustomerOrderFormDetails objectForKey:@"Rate"] isKindOfClass:[NSNumber class]]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[dictCustomerOrderFormDetails objectForKey:@"Rate"]];
                        }
                        else if ([[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"])
                                {
                                    cell.lblValue.text = [NSString stringWithFormat:@"$ %@",[[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"]];
                                }
                    }
                    
                    if ([indexPath row] == 2) {
                        if ([[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"]objectForKey:@"text"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"]objectForKey:@"text"]];
                        }
                    }
                    
                    if ([indexPath row] == 3) {
                        if ([[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[AppDelegate getAppDelegateObj] getTradingTerms:[[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"]]];
                        }
                    }
                    
                    if ([indexPath row] == 4) {
                        if ([[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"]) {
                            cell.lblValue.text = [NSString stringWithFormat:@"%@",[[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"]];
                        }
                    }
                }
                    
                    break;
                    
                default:
                    break;
            }
            
            return cell;
        }break;
            
        case TAG_100:{
            
            if (tableView.editing) {
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier9];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:8];
                }
                
                cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"] objectForKey:@"text"]];
                cell.lblValue.text = @"";
                
            }
            else{
                cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
                
                if (cell == nil)
                {
                    NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
                    
                    cell = [nib objectAtIndex:5];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                
                cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"] objectForKey:@"text"]];
                
                
                cell.lblValue.text = @"";
                
            }

            return cell;
        }break;
            
        default:return nil;break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (tableView.tag) {
        case TAG_100:{
            CustomerOrderFormProductViewController *dataViewController = [[CustomerOrderFormProductViewController alloc] initWithNibName:@"CustomerOrderFormProductViewController" bundle:[NSBundle mainBundle]];
            dataViewController.strTranCode = [[[arrCustomerTransactions objectAtIndex:[indexPath row]] objectForKey:@"Description"] objectForKey:@"text"];
            dataViewController.custCatogry=self.strCustCatagory;
            dataViewController.strDebtor = strCustomerCode;
            dataViewController.dictCustomerHistoryDetails = [arrCustomerTransactions objectAtIndex:[indexPath row]];
            [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
            
        }break;
    }
    
}


/*
 -(IBAction)printFromIpad:(id)sender {
 
 UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
 pic.delegate = self;
 
 UIPrintInfo *printInfo = [UIPrintInfo printInfo];
 printInfo.outputType = UIPrintInfoOutputGeneral;
 printInfo.jobName = self.documentName;
 pic.printInfo = printInfo;
 
 UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
 initWithText:yourNSStringWithContextOfTextFileHere];
 textFormatter.startPage = 0;
 textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
 textFormatter.maximumContentWidth = 6 * 72.0;
 pic.printFormatter = textFormatter;
 
 pic.showsPageRange = YES;
 
 void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
 ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
 if (!completed && error) {
 NSLog(@"Printing could not complete because of error: %@", error);
 }
 };
 if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
 [pic presentFromBarButtonItem:sender animated:YES completionHandler:completionHandler];
 } else {
 [pic presentAnimated:YES completionHandler:completionHandler];
 }
 }
 */

#pragma mark - Database calls
-(void)callGetCustomerOrderFormFromDB:(FMDatabase *)db
{
    strWebserviceType = @"DB_GET_CUSTOMER_ORDER";
    FMResultSet *rs;
    
    
    //**********ketaki code modified***********//
    FMResultSet *rs0 = [db executeQuery:@"SELECT CUST_CATEGORY FROM armaster WHERE trim(CODE) = ?",[strCustomerCode trimSpaces]];
    
    if (!rs0)
    {
        [rs0 close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
    }
    
    if ([rs0 next]) {
        NSDictionary *dictData = [rs0 resultDictionary];
        
        
        
        self.strCustCatagory = [dictData objectForKey:@"CUST_CATEGORY"];
        NSLog(@"%@",self.strCustCatagory);
    }
    
    [rs0 close];
    //**************************************************************************************//
    
    
    
    
    
    
    
    
    @try {
        
        rs = [db executeQuery:@"SELECT CODE as Customer, NAME as Name, TOTAL_DUE as Balance, PHONE as Phone, CONTACT1 as Contact, FAX_NO as Fax, RATE as Rate, CREDIT_LIMIT as CreditLimit, EMAIL1 as Email, TRADING_TERMS as TradingTerms, PRICE_CODE as PriceCode FROM armaster WHERE CODE= ? ",strCustomerCode];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs next]) {
            
            self.dictCustomerOrderFormDetails = (NSMutableDictionary *)[rs resultDictionary];
            
        }
        else{
            NSLog(@"No Detail");
        }
        
        [rs close];
        
        NSString *strPriceCode;
        FMResultSet *rs = [db executeQuery:@"SELECT PRICE_CODE FROM armaster WHERE CODE = ?",strCustomerCode];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        if ([rs next]) {
            NSDictionary *dictData = [rs resultDictionary];
            strPriceCode = [dictData objectForKey:@"PRICE_CODE"];
        }
        
        [rs close];

      /*
             rs = [db executeQuery:@"SELECT STOCK_CODE, MAX(RECNUM) AS RECNUM, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT                                                                                                                                                 a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1,                                                                                                                                                 a.`RECNUM` AS RECNUM,(SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t FROM smcuspro WHERE STOCK_CODE = a.`CODE` AND CUSTOMER_CODE = ? AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')))) AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t FROM smcuspro  WHERE STOCK_CODE = a.`CODE` AND CUSTOMER_CODE = ? AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1))) AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ?) temp GROUP BY STOCK_CODE ORDER BY DESCRIPTION",strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]];
     
      
      strCustomerCode = @"791305";
      NSString *str = @"SELECT STOCK_CODE, MAX(RECNUM) AS RECNUM, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT   a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1,   a.`RECNUM` AS RECNUM,(SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t FROM smcuspro WHERE STOCK_CODE = a.`CODE` AND CUSTOMER_CODE = 791305 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')))) AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))) AS q0,  (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t FROM smcuspro  WHERE STOCK_CODE = a.`CODE` AND CUSTOMER_CODE = 791305 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1))) AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now')))) ) AS q1,  (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t  FROM smcuspro WHERE STOCK_CODE = a.`CODE`   AND CUSTOMER_CODE = 791305   AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y', ('now'))))  ) AS q2,  (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t  FROM smcuspro    WHERE STOCK_CODE = a.`CODE`   AND CUSTOMER_CODE = 791305  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))   AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))    ) AS q3, (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t FROM smcuspro    WHERE STOCK_CODE = a.`CODE`   AND CUSTOMER_CODE = 791305 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))  ) AS q4    FROM samaster a   JOIN smcuspro b ON a.CODE = STOCK_CODE     JOIN sysistax c ON a.CODE = c.CODE                                                WHERE `CUSTOMER_CODE` = 791305  AND `WAREHOUSE` = 01) temp GROUP BY STOCK_CODE ORDER BY DESCRIPTION";
      
      
      
      
      NSString *strSubqueries1 = @"";
      
      
      
      @try {
        rs = [db executeQuery:str];
        

      }
      @catch (NSException *exception) {
        NSLog(@"Error:::%@",exception.description);
      }
  
       */
        
        //Change by subhu
        strCustomerCode = [strCustomerCode trimSpaces];
        
        rs = [db executeQuery:@"SELECT STOCK_CODE, MAX(SCM_RECNUM) AS SCM_RECNUM, MAX(DESCRIPTION) AS DESCRIPTION, MAX(WAREHOUSE) AS WAREHOUSE, MAX(SALES_DISS) AS SALES_DISS,MAX(COST_DISS) AS COST_DISS, MAX(PRICE1) AS PRICE1, MAX(PRICE2) AS PRICE2, MAX(PRICE3) AS PRICE3, max(PRICE4) AS PRICE4, MAX(PRICE5) AS PRICE5, max(STANDARD_COST) AS STANDARD_COST, MAX(DATE_LAST_PURCH) AS DATE_LAST_PURCH, MAX(TAX_CODE1) AS TAX_CODE1,MAX(CUSTOMER_CODE) AS CUSTOMER_CODE, MAX(q0) as q0, MAX(q1) as q1, MAX(q2) as q2, MAX(q3) as q3, max(q4) as q4 from (SELECT a.DESCRIPTION AS DESCRIPTION, a.WAREHOUSE AS WAREHOUSE, a.SALES_DISS AS SALES_DISS, a.COST_DISS AS COST_DISS, a.PRICE1 AS PRICE1, a.PRICE2 AS PRICE2, a.PRICE3 AS PRICE3, a.PRICE4 AS PRICE4, a.PRICE5 AS PRICE5, a.STANDARD_COST AS STANDARD_COST, b.DATE_LAST_PURCH AS DATE_LAST_PURCH, b.STOCK_CODE AS STOCK_CODE, b.CUSTOMER_CODE AS CUSTOMER_CODE, c.TAX_CODE1 AS TAX_CODE1,                                                                                                                                                 a.`SCM_RECNUM` AS SCM_RECNUM, a.PROD_GROUP AS PROD_GROUP,                                                                                                                                               (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now'))))                                                                                                                                                 AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q0,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`) = trim(strftime('%W', date('now')) - 1)))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q1,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 2))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q2,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                  AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 3))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q3,                                                                                                                                                 (SELECT IFNULL( SUM(  QTY_LAST_SOLD ) , 0 ) AS t                                                                                                                                                  FROM smcuspro                                                                                                                                                  WHERE STOCK_CODE = a.`CODE`                                                                                                                                                  AND CUSTOMER_CODE = ?                                                                                                                                                 AND (trim(strftime('%W', `DATE_LAST_PURCH`)) = trim(strftime('%W', date('now')) - 4))                                                                                                                                                  AND (trim(strftime('%Y', `DATE_LAST_PURCH`)) = trim(strftime ('%Y',  date('now'))))                                                                                                                                                  ) AS q4                                                                                                                                                 FROM samaster a                                                                                                                                                 JOIN smcuspro b ON a.CODE = STOCK_CODE                                                                                                                                                 JOIN sysistax c ON a.CODE = c.CODE                                                                                                                                                 WHERE `CUSTOMER_CODE` = ?  AND `WAREHOUSE` = ?) temp GROUP BY STOCK_CODE ORDER BY PROD_GROUP",strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,strCustomerCode,[[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"] trimSpaces]];
      
        
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [rs resultDictionary];

            [dict setValue:[dictData objectForKey:@"DESCRIPTION"] forKey:@"Description"];
            [dict setValue:[dictData objectForKey:@"COST_DISS"] forKey:@"Dissection_Cos"];
            [dict setValue:[dictData objectForKey:@"SALES_DISS"] forKey:@"Dissection"];
            [dict setValue:[dictData objectForKey:@"DATE_LAST_PURCH"] forKey:@"LastSold"];
            [dict setValue:[dictData objectForKey:@"STOCK_CODE"] forKey:@"StockCode"];
            [dict setValue:[dictData objectForKey:@"WAREHOUSE"] forKey:@"Warehouse"];
            [dict setValue:[dictData objectForKey:@"q0"] forKey:@"q0"];
            [dict setValue:[dictData objectForKey:@"q1"] forKey:@"q1"];
            [dict setValue:[dictData objectForKey:@"q2"]forKey:@"q2"];
            [dict setValue:[dictData objectForKey:@"q3"] forKey:@"q3"];
            [dict setValue:[dictData objectForKey:@"q4"] forKey:@"q4"];
            [dict setValue:[dictData objectForKey:@"STANDARD_COST"] forKey:@"Cost"];
            [dict setValue:[dictData objectForKey:@"PRICE1"] forKey:@"Price1"];
            [dict setValue:[dictData objectForKey:@"PRICE2"] forKey:@"Price2"];
            [dict setValue:[dictData objectForKey:@"PRICE3"] forKey:@"Price3"];
            [dict setValue:[dictData objectForKey:@"PRICE4"] forKey:@"Price4"];
            [dict setValue:[dictData objectForKey:@"PRICE5"] forKey:@"Price5"];
            [dict setValue:strCustomerCode forKey:@"CUSTOMER_CODE"];
            NSString *strGST = nil;
         
            if ([[dictData objectForKey:@"TAX_CODE1"] intValue] == -1) {
                strGST = @"N";
            }
            else{
                strGST = @"Y";
            }
           
            [dict setValue:strGST forKey:@"GST"];
            
            [arrCustomerTransactions addObject:dict];
            
        }
        
        [rs close];

        //NSLog(@"arrCustomerTransactions :%@",arrCustomerTransactions);
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DB Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        });
                
    }
}



#pragma mark - WS Methods
-(void)callWSGetCustomerOrderForm{
    
    spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_CUSTOMER_ORDER";
    NSString *parameters = [NSString stringWithFormat:@"code=%@&warehouse=%@",strCustomerCode,[[NSUserDefaults standardUserDefaults] objectForKey:@"warehouse"]];
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,CUSTOMER_ORDER_FORM_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Custom methods
-(void)doAfterDataFetch{
   
    [vwSegmentedControl addSubview:segControl];

    //Header view by default
    [vwContentSuperview addSubview:vwHeader];
    [tblHeader reloadData];
}

#pragma mark - Print Method & Mail Composer

#pragma mark - Compose Mail

-(IBAction)openMail:(id)sender{
 strEmailBody = [[AppDelegate getAppDelegateObj] SendEmail:dictCustomerOrderFormDetails aryProducts:arrCustomerTransactions strHtmlName:@"CustomerOrder"];
    
//    strEmailBody = [[AppDelegate getAppDelegateObj] SendEmail:dictCustomerOrderFormDetails aryProducts:arrCustomerProdEmailData strHtmlName:@"CustomerOrder"];
    [self callMailComposer];
}

-(void)callMailComposer{
    [self displayComposerSheet];

    
//    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//    if (mailClass != nil)
//    {
//        // We must always check whether the current device is configured for sending emails
//        if ([mailClass canSendMail])
//            [self displayComposerSheet];
//        else
//            [self launchMailAppOnDevice];
//    }
//    
//    else
//    {
//        [self launchMailAppOnDevice];
//    }
}

-(void)launchMailAppOnDevice{
    
    NSString *recipients = @"mailto:?cc=&subject=";
    NSString *body = @"&body=";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}
// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet{
    
        
       if( MFMailComposeViewController.canSendMail )
       {
               MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
               picker.mailComposeDelegate = self;
               [picker setSubject:@""];
    
               // Set up recipients
              // [picker setCcRecipients:[NSArray arrayWithObject:CC_RECIPIENT]];
               [picker setBccRecipients:nil];
    
                [picker setToRecipients:[NSArray arrayWithObject:TO_RECIPIENT]];
    
               //Attachment
               // NSString *filePath = [AppDelegate getFileFromDocumentsDirectory:@"CustomerOrderForm.html"];
    
               // NSData *fileData = [NSData dataWithContentsOfFile:filePath];
               //[picker addAttachmentData:fileData mimeType:@"text/html" fileName:@"CustomerOrderForm.html"];
    
    
               // NSString *strBody = [NSString stringWithFormat:@"CustomerOrderForm Email\n.%@",strEmailBody];
    
               [picker setMessageBody:strEmailBody isHTML:YES];
               [self presentViewController:picker animated:YES completion:nil];
       }
       else
       {
               NSLog(@"Mail services are not available");
               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Mail services are not available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
               [alertView show];
               //return;
       }
    
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    NSString* alertMessage;
    // message.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            alertMessage = @"Email composition cancelled";
            break;
        case MFMailComposeResultSaved:
            alertMessage = @"Your e-mail has been saved successfully";
            
            break;
        case MFMailComposeResultSent:
            alertMessage = @"Your email has been sent successfully";
            
            break;
        case MFMailComposeResultFailed:
            alertMessage = @"Failed to send email";
            
            break;
        default:
            alertMessage = @"Email Not Sent";
            
            break;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(IBAction)sendToPrinter:(id)sender
{
     strEmailBody = [[AppDelegate getAppDelegateObj] SendEmail:dictCustomerOrderFormDetails aryProducts:arrCustomerTransactions strHtmlName:@"CustomerOrder"];
        
    
    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:strEmailBody pathForPDF:[@"~/Documents/CustomerOrder.pdf" stringByExpandingTildeInPath] pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5) successBlock:^(NDHTMLtoPDF *htmlToPDF) {
        
        [self printPRDF:htmlToPDF.PDFpath];
        
    } errorBlock:^(NDHTMLtoPDF *htmlToPDF) {
        NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
        NSLog(@"%@",result);
     
    }];

    
}
-(void)printPRDF:(NSString *)result
{
    NSData *fileData = [NSData dataWithContentsOfFile:result];
        
        if ([UIPrintInteractionController isPrintingAvailable]) {
                //Printing is OK
                UIPrintInteractionController *print = [UIPrintInteractionController sharedPrintController];
                
                print.delegate = self;
                UIPrintInfo *printInfo = [UIPrintInfo printInfo];
                printInfo.outputType = UIPrintInfoOutputGeneral;
                //printInfo.jobName = [appDelegate.pdfFilePath lastPathComponent];
                printInfo.duplex = UIPrintInfoDuplexLongEdge;
                print.printInfo = printInfo;
                print.showsPageRange = YES;
                print.printingItem = fileData;
                UIViewPrintFormatter *viewFormatter = [self.view viewPrintFormatter];
                viewFormatter.startPage = 0;
                print.printFormatter = viewFormatter;
                
                UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
                };
                
                //    [print presentAnimated:YES completionHandler:completionHandler];
                [print presentFromRect:self.view.bounds inView:self.view animated:YES completionHandler:completionHandler];
                
                
        }
        else {
                //Printing is not ok
        }
   
    

}

#pragma mark - Centalized Class delegate Methods
-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength;
{
    [spinner removeFromSuperview];
    
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    //NSLog(@"%@", dictResponse);
   // NSLog(@"ASIHTTPRequest_Success:-  %@", responseStr);
    
    if ([strWebserviceType isEqualToString:@"WS_GET_CUSTOMER_ORDER"]){
        //True
        if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
            
            
            if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                
                self.dictCustomerOrderFormDetails = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Data"];
                
                self.arrCustomerTransactions = [[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"];
                
//                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"] isKindOfClass:[NSArray class]]) {
//                    NSMutableArray *arrcust =[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Details"];
//                    
//                    
//                    for (int i = 0; i< arrcust.count; i++) {
//                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//                        
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"Description"]objectForKey:@"text"] forKey:@"Description"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"StockCode"]objectForKey:@"text"] forKey:@"StockCode"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"q0"]objectForKey:@"text"] forKey:@"q0"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"q1"]objectForKey:@"text"] forKey:@"q1"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"q2"]objectForKey:@"text"]forKey:@"q2"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"q3"]objectForKey:@"text"] forKey:@"q3"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"q4"]objectForKey:@"text"] forKey:@"q4"];
//                        [dict setValue:[[[arrcust objectAtIndex:i ]objectForKey:@"Gst"]objectForKey:@"text"] forKey:@"Gst"];
//                        
//                        [arrCustomerProdEmailData addObject:dict];
//                    }

//                }
                
            }
            
           
            //NSLog(@"Details - %@",self.dictCustomerOrderFormDetails);
            
           // self.arrCustomerTransactions =
            
            dispatch_async(backgroundQueueForViewCustomerOrderForm, ^(void) {
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    
                    @try {
                        
                        if (![[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"]) {
                            @throw [NSException exceptionWithName:@"Response Issue" reason:@" Recnum can't be Null" userInfo:nil];
                        }
                        
                        
                        FMResultSet *rs = [db executeQuery:@"SELECT Recnum FROM armaster WHERE Recnum = ?",[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        
                        if (!rs) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        NSString *strEmail = @"";
                        if ([[dictCustomerOrderFormDetails objectForKey:@"Email"] objectForKey:@"text"]) {
                            strEmail = [[dictCustomerOrderFormDetails objectForKey:@"Email"] objectForKey:@"text"];
                        }
                        
                        NSString *strFax = @"";
                        if ([[dictCustomerOrderFormDetails objectForKey:@"Fax"] objectForKey:@"text"]) {
                            strFax = [[dictCustomerOrderFormDetails objectForKey:@"Fax"] objectForKey:@"text"];
                        }
                      
                      NSString *strphone = @"";
                      if ([[dictCustomerOrderFormDetails objectForKey:@"Phone"] objectForKey:@"text"]) {
                        strphone = [[dictCustomerOrderFormDetails objectForKey:@"Phone"] objectForKey:@"text"];
                      }

                      NSString *strcontact = @"";
                      if ([[dictCustomerOrderFormDetails objectForKey:@"Contact"] objectForKey:@"text"]) {
                        strcontact = [[dictCustomerOrderFormDetails objectForKey:@"Contact"] objectForKey:@"text"];
                      }
                      
                      
                        BOOL y;
                        if ([rs next]) {
//                            y = [db executeUpdate:@"UPDATE `armaster` SET `SCM_RECNUM` = ?, `RECNUM`= ?, `CODE`= ?, `NAME`= ?,`TOTAL_DUE`= ?, `PHONE`= ?,`CONTACT1`= ?,`FAX_NO`= ?,`RATE`= ?,`CREDIT_LIMIT`= ?,`EMAIL1`= ?,`TRADING_TERMS`= ?,`PRICE_CODE`= ?,`created_date`= ?, `modified_date`= ? WHERE SCM_RECNUM = ?", [[dictCustomerOrderFormDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Phone"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Contact"] objectForKey:@"text"],strFax,[[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"]];

                          y = [db executeUpdate:@"UPDATE `armaster` SET  `RECNUM`= ?, `CODE`= ?, `NAME`= ?,`TOTAL_DUE`= ?, `PHONE`= ?,`CONTACT1`= ?,`FAX_NO`= ?,`RATE`= ?,`CREDIT_LIMIT`= ?,`EMAIL1`= ?,`TRADING_TERMS`= ?,`PRICE_CODE`= ? WHERE RECNUM = ?",[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"], strphone,strcontact,strFax,[[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"]];
                        }
                        else{
//                            y = [db executeUpdate:@"INSERT INTO `armaster` (`SCM_RECNUM`, `RECNUM`, `CODE`, `NAME`,`TOTAL_DUE`, `PHONE`,`CONTACT1`,`FAX_NO`,`RATE`,`CREDIT_LIMIT`,`EMAIL1`,`TRADING_TERMS`,`PRICE_CODE`,`created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [[dictCustomerOrderFormDetails objectForKey:@"ScmRecnum"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Phone"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Contact"] objectForKey:@"text"],strFax,[[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreatedDate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"ModifiedDate"] objectForKey:@"text"]];

                          y = [db executeUpdate:@"INSERT INTO `armaster` ( `RECNUM`, `CODE`, `NAME`,`TOTAL_DUE`, `PHONE`,`CONTACT1`,`FAX_NO`,`RATE`,`CREDIT_LIMIT`,`EMAIL1`,`TRADING_TERMS`,`PRICE_CODE`) VALUES (  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",[[dictCustomerOrderFormDetails objectForKey:@"Recnum"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Customer"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"Name"] objectForKey:@"text"], [[dictCustomerOrderFormDetails objectForKey:@"Balance"] objectForKey:@"text"], strphone,strcontact,strFax,[[dictCustomerOrderFormDetails objectForKey:@"Rate"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"CreditLimit"] objectForKey:@"text"],strEmail,[[dictCustomerOrderFormDetails objectForKey:@"TradingTerms"] objectForKey:@"text"],[[dictCustomerOrderFormDetails objectForKey:@"PriceCode"] objectForKey:@"text"]];
                        }
                        
                        
                        if (!y)
                        {
                            [rs close];
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        [rs close];
                        *rollback = NO;
                    }
                    @catch(NSException* e){
                        *rollback = YES;
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });
                        
                    }
                                        
                    @finally {
//                        [self callGetCustomerOrderFormFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [spinnerObj removeFromSuperview];
                            [self doAfterDataFetch];
                            
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db close];
                        }
                        
                    });
                    
                }];
                
            });
            
            
        }
        //False
        else{
            NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"CustOrder"] objectForKey:@"Message"] objectForKey:@"text"];
            if ([strErrorMessage isEqualToString:@""] || strErrorMessage.length == 0) {
                strErrorMessage = @"Something went wrong with service";
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            
        }
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    NSError *err = error;
    
    NSLog(@"error : %d",err.code);

    //Request timeout
    if (err.code == 2) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:SERVER_TAKING_TIME_MESSAGE message:FETCH_LOCALLY_MESSAGE delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = TAG_50;
        [alert show];

    }
    else{
        [AJNotificationView showNoticeInView:self.view
                                        type:AJNotificationTypeRed
                                       title:ERROR_IN_CONNECTION_MESSAGE
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];

    }
}

#pragma mark - Alert Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == TAG_50) {
        [localSpinner startAnimating];
        dispatch_async(backgroundQueueForViewCustomerOrderForm, ^(void) {
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
            [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetCustomerOrderFormFromDB:db];
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self doAfterDataFetch];
                [localSpinner stopAnimating];
            });
            
        });

    }
    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
