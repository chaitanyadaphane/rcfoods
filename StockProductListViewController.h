//
//  StockProductListViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import "PullToLoadMoreView.h"

@interface StockProductListViewController : UIViewController<ASIHTTPRequest_delegate,ReachabilityRequest_delegate,MBProgressHUDDelegate>{
    
    NSMutableArray *filteredListContent;	// The content filtered as a result of a search.
    
    NSMutableArray *arrProducts; // The master content.
    NSMutableArray *arrProductsForWebservices;
    
    SEFilterControl *segControl;
    
    NSString *strWebserviceType;
    PullToLoadMoreView* _loadMoreFooterView;
    
    int currPage;
    int totalCount;
    int recordNumber;
    
    NSString *strWarehouse;
     
    NSOperationQueue *operationQueue;
    FMDatabaseQueue *dbQueueSamaster;
    
    BOOL isNetworkConnected;  
}

@property (nonatomic, retain) NSMutableArray *filteredListContent;
@property (nonatomic, retain) NSMutableArray *arrProducts;
@property (nonatomic, retain) NSMutableArray *arrProductsForWebservices;
@property (nonatomic, retain) NSString *strWarehouse;
@property (nonatomic, retain) MBProgressHUD *spinner;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblProductList;
@property (nonatomic,unsafe_unretained) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblWarehouse;
@property (nonatomic,unsafe_unretained) IBOutlet UISearchBar *searchingBar;

@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwResults;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;

@property (nonatomic, assign) BOOL isSearchMode;
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

-(void)callWSGetStockList:(int)pageIndex Warehouse:(NSString *)strWareHouse;
@end
