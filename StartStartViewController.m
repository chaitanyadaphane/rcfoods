//
//  StartStartViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 24/07/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "StartStartViewController.h"
#import "DownloadUrlOperation.h"

NSString * const kStartSyncEngineInitialCompleteKey = @"StartSyncEngineInitialSyncCompleted";
NSString * const kStartSyncEngineSyncCompletedNotificationName = @"StartSyncEngineSyncCompleted";


@interface StartStartViewController ()

@property (nonatomic, strong) NSMutableArray *registeredClassesToSync;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation StartStartViewController

@synthesize syncInProgress = _syncInProgress;
@synthesize registeredClassesToSync = _registeredClassesToSync;
@synthesize dateFormatter = _dateFormatter;
@synthesize downloadOperationQueue,isQueueCancelled,numDatabaseDumped;

@synthesize imgView,lblStatus,lblPercentageIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;

}

#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGSize viewSize = [AppDelegate getRotatedViewSize:self.view];
    self.view.frame = CGRectMake(0, 20, viewSize.width, viewSize.height);
    
    if (![AppDelegate getAppDelegateObj].isNetworkConnected) {
        lblStatus.text = @"";
        [AppDelegate getAppDelegateObj].window.rootViewController = [AppDelegate getAppDelegateObj].loginViewController;
    }
    else{
        
        NSArray *array = [NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",
                          @"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",nil];
        NSMutableArray *imgArray = [[NSMutableArray alloc] init];
        
        for (int i=0; i<[array count]; i++ )
        {
            NSString *strImageName = [NSString stringWithFormat:@"refresharraow_%@",[array objectAtIndex:i]];
            NSString *strImagePath=[[NSBundle mainBundle] pathForResource:strImageName ofType:@"png"];
            [imgArray addObject: [UIImage imageWithContentsOfFile:strImagePath]];
            strImageName = nil;
            strImagePath = nil;
        }
        
        imgView.animationImages = imgArray;
        
        [imgArray release];
        imgArray = nil;
        
        imgView.animationRepeatCount = 0;
        imgView.animationDuration = 1.0;
        [imgView startAnimating];
        [self startSync];
    }
    
}

- (void) viewDidUnload{
    [super viewDidUnload];
    self.imgView = nil;
    self.lblStatus = nil;
    self.lblPercentageIndicator = nil;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// this is for iOS < 6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)dealloc{
    [imgView release];
    imgView = nil;
    
    [lblStatus release];
    lblStatus = nil;
    
    [downloadOperationQueue release];
    downloadOperationQueue = nil;
    
    [super dealloc];
}


+ (StartStartViewController *)sharedEngine {
    static StartStartViewController *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[StartStartViewController alloc] init];
    });
    
    return sharedEngine;
}

- (void)registerNSManagedObjectClassToSync:(NSString *)tableName {
    if (!self.registeredClassesToSync) {
        self.registeredClassesToSync = [NSMutableArray array];
    }
    
    if (![self.registeredClassesToSync containsObject:tableName]) {
        [self.registeredClassesToSync addObject:tableName];
    } else {
        DLog(@"Unable to register %@ as it is already registered", tableName);
    }
}

- (void)startSync {
    if (!self.syncInProgress) {
        self.isQueueCancelled = NO;

        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = YES;
        [self didChangeValueForKey:@"syncInProgress"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self downloadDataForRegisteredObjects:YES];
        });
    }
    
}


- (void)executeSyncCompletedOperations {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setInitialSyncCompleted];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kStartSyncEngineSyncCompletedNotificationName
         object:nil];
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = NO;
        [self didChangeValueForKey:@"syncInProgress"];
        
        
        //Remove observer
        for (NSOperation *req in [downloadOperationQueue operations]) {
            [req removeObserver:self forKeyPath:@"isFinished"];
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"StartSyncDataDownloadFinished" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"StartSyncDataDownloadFailed" object:nil];
        
        if (!isQueueCancelled) {
            [imgView stopAnimating];
            self.lblStatus.text = @"Done";
            [AppDelegate getAppDelegateObj].window.rootViewController = [AppDelegate getAppDelegateObj].loginViewController;
        }
        
    });
}


-(void)showSyncErrorMsg:(NSString*)err
{
    DLog(@"Error : %@",err);
    
}

- (BOOL)initialSyncComplete {
    return [[[NSUserDefaults standardUserDefaults] valueForKey:kStartSyncEngineInitialCompleteKey] boolValue];
}

- (void)setInitialSyncCompleted {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kStartSyncEngineInitialCompleteKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)mostRecentUpdatedAtDateForEntityWithName:(NSString *)entityName {
    __block NSDate *date = nil;
    
    __block NSString *dateString = nil;
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue  inDatabase:^(FMDatabase *db) {
        FMResultSet *results = [db executeQuery:@"SELECT last_sync_date FROM tbl_lastSyncDates WHERE table_name = ? ",entityName];
        
        if ([results next]){
            NSDictionary *dictData = [results resultDictionary];
            NSString *strDateFromDb = [NSString stringWithFormat:@"%@",[dictData objectForKey:@"last_sync_date"]];
            date = [self dateUsingStringFromAPI:strDateFromDb];
            dateString = [self dateStringForAPIUsingDate:date];
            
            dateString = [dateString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        else{
            DLog(@"table doesnt exist in db");
        }
        
    }];
    
    return dateString;
}

- (BOOL)setUpdatedLastDateForEntityWithName:(NSString *)entityName NewSyncDate:(NSString *)newSyncDate Database:(FMDatabase *)db{

    
    BOOL y = [db executeUpdate:@"UPDATE tbl_lastSyncDates SET last_sync_date = ? WHERE table_name = ?",newSyncDate,entityName];
    
    
    return y;
}


- (void)downloadDataForRegisteredObjects:(BOOL)useUpdatedAtDate {
    
    cntWebserviceNumber = 0;
    numDatabaseDumped = 0;
    
    downloadOperationQueue = [NSOperationQueue new];
    // set maximum operations possible
    [downloadOperationQueue setMaxConcurrentOperationCount:1];
    //downloadOperationQueue.suspended = YES;
    [self.downloadOperationQueue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataAvailable:)
     name:@"StartSyncDataDownloadFinished"
     object:nil ] ;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dataUnAvailable:)
     name:@"StartSyncDataDownloadFailed"
     object:nil ];
    
    [self callMembersWebservice];
    
    
    //[self callArmasterWebservice];
    //[self callArtransWebservice];
    //[self callArcommentWebservice];
    //[self callNewsamasterWebservice];
    //[self callPodetailWebservice];
    //[self callSahisbudWebservice];
    //[self callSamasterWebservice];
    //[self callSmcusproWebservice];
    //[self callSodetailWebservice];
    //[self callSoheaderWebservice];
    //[self callSoquoheaWebservice];
    //[self callSoquodetWebservice];
    [self callSpecialsWebservice];
    //[self callSysdescWebservice];
    //[self callSysdisctWebservice];
    //[self callSysistaxWebservice];
    //[self callArpaymentWebservice];
    //[self callArhisheaWebservice];   // not used in application
    //[self callArhisdetWebservice];   // not used in application
     //[self callNoticemsgsWebservice];
}


#pragma mark - Upload WS calls

//Upload Order headers
-(void)callUploadOrdersHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadOrders strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 100;
    operation.isUploading = YES;
    operation.isOrders = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}

//Upload Order Details
-(void)callUploadOrdersDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 101;
    operation.isUploading = YES;
    operation.isOrders = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}

//Upload Quote Headers
-(void)callUploadQuotesHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 102;
    operation.isUploading = YES;
    operation.isQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}

//Upload Quote Details
-(void)callUploadQuotesDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 103;
    operation.isUploading = YES;
    operation.isQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}

//Upload Offline Quote Headers
-(void)callUploadOfflineQuotesHeaders
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 104;
    operation.isUploading = YES;
    operation.isOfflineQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}

//Upload Offline Quote Details
-(void)callUploadOfflineQuotesDetails
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",WSURL,UPLOAD_FILE_WS];
    DLog(@"uploadQuotes strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 105;
    operation.isUploading = YES;
    operation.isOfflineQuotes = YES;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation];
    [operation release];
    operation = nil;
}



#pragma mark - Download WS calls

-(void)callMembersWebservice
{
    //NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"members"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,MEMBER_WS_NEW,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    DLog(@"members strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 1;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callArmasterWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"armaster"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"armaster strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 2;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callArtransWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"artrans"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARTRANS_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"artrans strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 3;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callArcommentWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arcoment"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARCOMMENT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"ARCOMMENT_WS strUrl : %@",strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 4;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callNewsamasterWebservice
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,NEWSAMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    DLog(@"new_samaster strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 5;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callPodetailWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"podetail"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,PODETAIL_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"PODETAIL_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 6;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSahisbudWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sahisbud"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SAHISBUD_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SAHISBUD_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 7;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSamasterWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"samaster"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SAMASTER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SAMASTER_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 8;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSmcusproWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"smcuspro"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SMCUSPRO_NEW,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SMCUSPRO_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 9;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    
}

-(void)callSodetailWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sodetail"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SODETAIL_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SODETAIL_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 10;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSoheaderWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soheader"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOHEADER_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SOHEADER_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 11;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSoquoheaWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soquohea"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOQUOHEA_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SOQUOHEA_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 12;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSoquodetWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"soquodet"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SOQUODET_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SOQUODET_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 13;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}


-(void)callSpecialsWebservice
{
    //NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"specials"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SPECIALS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    DLog(@"SPECIALS_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 14;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
    
}


-(void)callSysdescWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdesc"];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSDESC_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSDESC_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 15;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSysdisctWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdisct"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSDISCT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSDISCT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 16;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}


-(void)callSyslogonWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysdisct"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSLOGON_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSLOGON_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 17;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSysistaxWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysistax"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSISTAX_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSISTAX_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 18;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSysusrnoWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"sysistax"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSUSRNO_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSUSRNO_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 19;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callArpaymentWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arpaymnt"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARPAYMENT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"ARPAYMENT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 20;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callArhisheaWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arhishea"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARHISHEA_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"ARHISHEA_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 21;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}


-(void)callArhisdetWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"arhisdet"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,ARHISDET_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"ARHISDET_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 22;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callNoticemsgsWebservice
{    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,NOTICEMSGS_WS,[NSString stringWithFormat:@"last_syncDate=%@",STANDARD_SYNC_DATE]];
    
    DLog(@"NOTICEMSGS_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 23;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}

-(void)callSyscontWebservice
{
    NSString *last_sync_date = [self mostRecentUpdatedAtDateForEntityWithName:@"syscont"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",WSURL,SYSCONT_WS,[NSString stringWithFormat:@"last_syncDate=%@",last_sync_date]];
    
    DLog(@"SYSCONT_WS strUrl : %@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    DownloadUrlOperation *operation = [[DownloadUrlOperation alloc] initWithURL:url];
    operation.tag = 24;
    [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
    [downloadOperationQueue addOperation:operation]; // operation starts as soon as its added
    [operation release];
    operation = nil;
}



#pragma mark - KVO Observing/ WS Handler
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)operation change:(NSDictionary *)change context:(void *)context
{
    NSString *source = nil;
    NSError *error = nil;
    
    if ([operation isKindOfClass:[DownloadUrlOperation class]]) {
        DownloadUrlOperation *downloadOperation = (DownloadUrlOperation *)operation;
        source = [NSString stringWithFormat:@"%d",downloadOperation.tag];
        
        if (source) {
            
            //get total queue size by the first success and add 1 back
            if (queueSize ==0) {
                queueSize = [[downloadOperationQueue operations] count] + 1.0;
            }
            
            float progress = (float)(queueSize-[[downloadOperationQueue operations] count])/queueSize;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strProgress = [NSString stringWithFormat:@"%.0f%@",progress*100,@"%"];
                lblPercentageIndicator.text = strProgress;
                strProgress = nil;

            });
            
            error = [downloadOperation error];
        }
        
        if (source) {
            if (error != nil) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StartSyncDataDownloadFailed"
                                                                    object:self
                                                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", error, @"error", nil]];
            }
        }
        
    }
    
    //Check Queue is empty or not
    else if (operation == self.downloadOperationQueue && [keyPath isEqualToString:@"operations"]) {
        if ([self.downloadOperationQueue.operations count] == 0) {
            // Do something here when your queue has completed
            DLog(@"queue has completed");
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strProgress = [NSString stringWithFormat:@"100%@",@"%"];
                lblPercentageIndicator.text = strProgress;
                strProgress = nil;
            });
            
            [self executeSyncCompletedOperations];
                    
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSString *strProgress = [NSString stringWithFormat:@"0%@",@"%"];
                lblPercentageIndicator.text = strProgress;
                strProgress = nil;
                
            });

        }
    }
    
    else {
        [super observeValueForKeyPath:keyPath ofObject:operation
                               change:change context:context];
    }
}


#pragma mark - Notification handler

- (void)dataAvailable:(NSNotification *)notification
{
    NSString *source = [notification.userInfo valueForKey:@"source"];
    
    //Members table
    if ([source isEqualToString:@"1"]) {
        
        
    }
    //Armaster
    else if ([source isEqualToString:@"2"]) {
        
        
    }
    //Artrans
    else if ([source isEqualToString:@"3"]) {
        
        
    }
    //Arcoment
    else if ([source isEqualToString:@"4"]) {
        
        
    }
    
    //New samaster
    else if ([source isEqualToString:@"5"]) {
        
    }
    //Podetail
    else if ([source isEqualToString:@"6"]) {
        
        
    }
    //sahisbud
    else if ([source isEqualToString:@"7"]) {
        
    }
    
    //Samaster
    else if ([source isEqualToString:@"8"]) {
        
        
    }
    //Smcuspro
    else if ([source isEqualToString:@"9"]) {
        
        
    }
    //Sodetail
    else if ([source isEqualToString:@"10"]) {
        
        
    }
    //Soheader
    else if ([source isEqualToString:@"11"]) {
        
        
        
    }
    //soquohea
    else if ([source isEqualToString:@"12"]) {
        
    }
    //soquodet
    else if ([source isEqualToString:@"13"]) {
        
        
    }
    //Specials
    else if ([source isEqualToString:@"14"]) {
        
        
    }
    //sysdesc
    else if ([source isEqualToString:@"15"]) {
        
    }
    //sysdisct
    else if ([source isEqualToString:@"16"]) {
        
        
        
    }
    //sysistax
    else if ([source isEqualToString:@"18"]) {
        
    }
    //arpayment
    else if ([source isEqualToString:@"20"]) {
        
        
        
    }
    //Arhishea
    else if ([source isEqualToString:@"21"]) {
        
    }
    //Arhisdet
    else if ([source isEqualToString:@"22"]) {
    }
    //NoticeMessages
    else if ([source isEqualToString:@"23"]) {
        NSData *responseData = [notification.userInfo valueForKey:@"data"];
        
        NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
        if ([dictResponse objectForKey:@"Response"]) {
            if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Flag"]) {
                //True
                if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                    
                    NSMutableArray *arrMembers = [[NSMutableArray alloc] init];
                    
                    if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                        
                        NSDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"];
                        [arrMembers addObject:dict];
                    }
                    else{
                        arrMembers = [[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Data"];
                    }
                    
                    
                    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                    [databaseQueue  inDatabase:^(FMDatabase *db) {
                        @autoreleasepool {
                            @try
                            {
                                for (NSDictionary *dict in arrMembers){
                                    BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `noticemsgs` (`MESSAGE_ID`, `DATE`, `TO`, `MESSAGE_CONTENT`, `CREATED_BY`, `READ_FLAG`, `ATTACHMENT`,`created_date`,`modified_date`) VALUES(?, ?, ?, ?, ?, ?, ?,?,?)",
                                              [[dict objectForKey:@"MESSAGE_ID"] objectForKey:@"text"],
                                              [[dict objectForKey:@"DATE"] objectForKey:@"text"],
                                              [[dict objectForKey:@"TO"] objectForKey:@"text"],
                                              [[dict objectForKey:@"MESSAGE_CONTENT"] objectForKey:@"text"],
                                              [[dict objectForKey:@"CREATED_BY"] objectForKey:@"text"],
                                              [[dict objectForKey:@"READ_FLAG"] objectForKey:@"text"],
                                              [[dict objectForKey:@"ATTACHMENT"] objectForKey:@"text"],
                                              [[dict objectForKey:@"created_date"] objectForKey:@"text"],
                                              [[dict objectForKey:@"modified_date"] objectForKey:@"text"]];
                                    
                                    if (!y)
                                    {
                                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                        @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                    }
                                    
                                    
                                    NSString *strPdfName =[[dict objectForKey:@"ATTACHMENT"] objectForKey:@"text"];
                                    
                                    if (![strPdfName isEqualToString:@""]) {
                                        //Download Pdf
                                        if (![[NSFileManager defaultManager] fileExistsAtPath:[AppDelegate getFileFromDocumentsDirectory:strPdfName]]) {
                                            
                                            NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strPdfName];
                                            
                                            [downloadOperationQueue addOperationWithBlock:^{
                                                [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strPdfUrl];
                                                
                                                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                    DLog(@"Done with downloading of pdf - for Notice messages %@",[strPdfUrl getFileNameFromURL]);
                                                }];
                                            }];
                                            
                                            
                                        }
                                    }
                                    
                                }
                                
                                //Update the New "Last sync" date
                                NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                                
                                BOOL y = [self setUpdatedLastDateForEntityWithName:@"noticemsgs" NewSyncDate:strNewSyncDate Database:db];
                                
                                if (!y) {
                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                                }
                                
                                //Commit here
                                //[db commit];
                                
                                
                            }
                            @catch(NSException* e)
                            {
                                //*rollback = YES;
                                dispatch_async(dispatch_get_main_queue(), ^(void) {
                                    [self showSyncErrorMsg:[e description]];
                                });
                                
                                //[db close];
                            }
                        }
                    }];
                    
                    
                }
                //False
                else{
                    
                    NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"Message"] objectForKey:@"text"];
                    
                    DLog(@"Error : noticemsgs: %@",strErrorMessage);
                    
                }
                
            }
            else{
                
                FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
                [databaseQueue  inDatabase:^(FMDatabase *db) {
                    @try{
                        
                        FMResultSet *rs = [db executeQuery:@"SELECT ATTACHMENT FROM noticemsgs"];
                        
                        while ([rs next]) {
                            NSDictionary *dictTemp = [rs resultDictionary];
                            
                            NSString *strPdfName =[dictTemp objectForKey:@"ATTACHMENT"];
                            
                            if (![strPdfName isEqualToString:@""]) {
                                //Download Pdf
                                if (![[NSFileManager defaultManager] fileExistsAtPath:[AppDelegate getFileFromDocumentsDirectory:strPdfName]]) {
                                    
                                    NSString *strPdfUrl = [NSString stringWithFormat:@"%@%@",NOTICE_ATTACHMENT_URL,strPdfName];
                                    
                                    [downloadOperationQueue addOperationWithBlock:^{
                                        [[AppDelegate getAppDelegateObj] downloadFileToCacheFromURL:strPdfUrl];
                                        
                                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                            DLog(@"Done with downloading of pdf - for Notice messages %@",[strPdfUrl getFileNameFromURL]);
                                        }];
                                    }];
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                        [rs close];
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"noticemsgs"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [self setUpdatedLastDateForEntityWithName:@"noticemsgs" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                    }
                    @catch(NSException* e)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [self showSyncErrorMsg:[e description]];
                        });
                    }
                    
                }];
                
            }
        }
    }
    
}

- (void)dataUnAvailable:(NSNotification *)notification
{
    DLog(@"Source : %@",[notification.userInfo valueForKey:@"source"]);
    DLog(@"Error : %@",[notification.userInfo valueForKey:@"error"]);
    
    NSError *error= [notification.userInfo valueForKey:@"error"];
    
    //No internet
    if (error.code == -1009 || error.code == -1005 || error.code == -1004||error.code == -1001) {
        
        NSString *strError = error.localizedDescription;
        [self showSyncErrorMsg:strError];
        [downloadOperationQueue cancelAllOperations];
    }
    
    //Cancelled error code
    if (error.code == 123) {
        self.isQueueCancelled = YES;
    }
}

- (void)initializeDateFormatter
{
    if (!self.dateFormatter)
    {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [self.dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    }
}

- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString
{
    [self initializeDateFormatter];
    return [self.dateFormatter dateFromString:dateString];
}

- (NSString *)dateStringForAPIUsingDate:(NSDate *)date
{
    [self initializeDateFormatter];
    NSString *dateString = [self.dateFormatter stringFromDate:date];
    return dateString;
}


#pragma mark - File Management

- (NSURL *)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -Alert View Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        exit(0);
    }
    
}


@end
