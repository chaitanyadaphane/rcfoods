//
//  ReasonListPopOverViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 16/04/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissPopOverDelegateForReasonDelegate
- (void)actiondismissPopOverForReason:(NSString *)strReasonName;
@end

@interface ReasonListPopOverViewController : UIViewController{
    id<DismissPopOverDelegateForReasonDelegate> __unsafe_unretained delegate;    
    NSArray *arrReasonList;
}

@property(unsafe_unretained)id<DismissPopOverDelegateForReasonDelegate>delegate;
@property(nonatomic,retain)NSArray *arrReasonList;
@end
