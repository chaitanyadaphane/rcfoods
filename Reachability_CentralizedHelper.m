//
//  Reachability_CentralizedHelper.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 23/02/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "Reachability_CentralizedHelper.h"

@implementation Reachability_CentralizedHelper

static Reachability_CentralizedHelper * _sharedHelper;

+ (Reachability_CentralizedHelper *) sharedHelper
{
    if (_sharedHelper != nil)
	{
        return _sharedHelper;
    }
    _sharedHelper = [[Reachability_CentralizedHelper alloc] init];
    return _sharedHelper;
}

- (id)init
{
    if ((self = [super init]))
	{
    }
    return self;
}

@end
