//
//  CallEntryViewController.h
//  Blayney
//
//  Created by POOJA MISHRA on 30/06/16.
//  Copyright © 2016 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "PopUpViewController.h"

@protocol CallEntryDelegate <NSObject>

-(void)callEntryPerformsAction:(NSString *)strResult;

@end

@interface CallEntryViewController : UIViewController<UITextFieldDelegate,NIDropDownDelegate,SetCustomPopupDelegate,UIPopoverControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblVw;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property(strong,nonatomic)NSString *debtorStr;
@property(strong,nonatomic)NSString *salespersonStr;

@property(strong,nonatomic)  id <CallEntryDelegate> delegate;

@property (strong,nonatomic)NSString *deliveryrun;
@property (assign,nonatomic)int no_of_lines;
@property (strong,nonatomic)NSString *totalCost;

@property (strong,nonatomic) UIPopoverController * popoverController;

- (IBAction)saveAction:(id)sender;
- (IBAction)proceedwithorderAction:(id)sender;

@end
