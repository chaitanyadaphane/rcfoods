//
//  NSObject+UITableViewMyCustomTable.m
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 23/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import "NSObject+UITableViewMyCustomTable.h"

@implementation UITableView (MyCustomTable)

-(void)applyCornersAtBottom{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                   byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight
                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the image view's layer
    self.layer.mask = maskLayer;
}

@end
