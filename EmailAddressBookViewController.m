//
//  EmailAddressBookViewController.m
//  Blayney
//
//  Created by Pooja on 31/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "EmailAddressBookViewController.h"

@interface EmailAddressBookViewController ()

@end

@implementation EmailAddressBookViewController
@synthesize arrData;
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_emailSelectedArr == nil){
        _emailSelectedArr = [NSMutableArray new];
    }
    else{
        _emailSelectedArr =[[[NSUserDefaults standardUserDefaults] objectForKey:@"EmailAddresSelected"] mutableCopy];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Cell Initialisation here
    static NSString *CellIdentifier = @"Cell";
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString * strdata = [NSString stringWithFormat:@"%@",[arrData objectAtIndex:[indexPath row]]];
    cell.textLabel.text = strdata;
    
    if ([_emailSelectedArr containsObject:[arrData objectAtIndex:[indexPath row]]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:[arrData objectAtIndex:[indexPath row]] animated:YES];

    if ([_emailSelectedArr containsObject:[arrData objectAtIndex:[indexPath row]]])
    {
        [_emailSelectedArr removeObject:[arrData objectAtIndex:[indexPath row]]];
    }
    else
    {
        [_emailSelectedArr addObject:[arrData objectAtIndex:[indexPath row]]];
    }
    
    NSLog(@"emailSelectedArr %@",_emailSelectedArr);
    
    [_tableView reloadData];
}


- (IBAction)actionDone:(id)sender {
    
    NSString * strTextField;
    if (_emailSelectedArr.count > 0) {
        strTextField = [(NSArray *)self.emailSelectedArr componentsJoinedByString:@", "];
    }
    else{
        strTextField = @"";
    }
    
    arrData = nil;
    
    [[NSUserDefaults standardUserDefaults]setObject:_emailSelectedArr forKey:@"EmailAddresSelected"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [delegate actiondismissEmailPopOverStockPickUp:strTextField];
}

- (IBAction)actionCancel:(id)sender {
    arrData = nil;
    [delegate actiondismissEmailPopOverStockPickUp:@""];

}
@end
