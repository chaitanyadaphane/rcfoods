//
//  StockProductListViewController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 14/01/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "StockProductListViewController.h"
#import "CustomCellGeneral.h"
#import "StockInfoProductDetailsViewController.h"

#define STOCK_LIST_WS @"stockinfo/list_stockinfowarehouse.php?"
#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); abort(); } }

#define CHECKFLAG_YES @"1"
#define CHECKFLAG_NO @"0"

@interface StockProductListViewController ()
{
    BOOL _loadingInProgress;
}

- (void) setupLoadMoreFooterView;
- (void) didFinishLoadingMoreSampleData;
- (void) repositionLoadMoreFooterView;
- (void) freeUp;
- (CGFloat) tableViewContentHeight;
- (CGFloat) endOfTableView:(UIScrollView *)scrollView;

@end


@implementation StockProductListViewController
@synthesize filteredListContent,arrProducts,strWarehouse,arrProductsForWebservices,tblProductList,lblWarehouse,vwSegmentedControl,isSearchMode,searchingBar;

@synthesize vwResults,vwContentSuperview,vwNoResults,localSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //Set the Reachability observer
        [Reachability_CentralizedHelper sharedHelper].delegate = nil;
        [Reachability_CentralizedHelper sharedHelper].delegate = (id)self;
        [[Reachability_CentralizedHelper sharedHelper] setReachabilityObserver];
        
        Reachability *checkInt = [Reachability reachabilityWithHostName:HOST_NAME];
        
        NetworkStatus internetStatus = [checkInt currentReachabilityStatus];
        
        if(internetStatus == NotReachable)
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
        }
        else{
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
        }
        
    }
    return self;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
    {
        CGRect frm =     searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
    else{
        CGRect frm =     searchingBar.frame;
        frm.size.height = 88.0f;
        self.searchingBar.frame = frm;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.isSearchMode = NO;
    
    filteredListContent = [[NSMutableArray alloc] init];
    arrProducts = [[NSMutableArray alloc] init];
    
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    
    //Segmented Control
    CGRect frameSeg = CGRectMake(0, 0, vwSegmentedControl.frame.size.width, vwSegmentedControl.frame.size.height);
    segControl= [[SEFilterControl alloc]initWithFrame:frameSeg Titles:[NSArray arrayWithObjects:@"WH 01", @"WH 02", nil]];
     segControl.tintColor = [UIColor purpleColor];
    [segControl addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    [vwSegmentedControl addSubview:segControl];
    
    //First page
    currPage = 1;
    totalCount = 0;
    recordNumber = 0;
    
    
    operationQueue = [NSOperationQueue new];
        
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    NSString *strMember = [prefs objectForKey:@"members"];
    
//    if ([strMember isEqualToString:SALES]) {
        self.strWarehouse = [prefs objectForKey:@"warehouse"];
        if (self.strWarehouse.length < 2) {
            self.strWarehouse = [NSString stringWithFormat:@"0%@",self.strWarehouse];
        }
//    }
    
    
    lblWarehouse.text = strWarehouse;
  
    strWarehouse = [strWarehouse trimSpaces];
  
    [localSpinner startAnimating];
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
          NSString *strQuery = [NSString stringWithFormat:@"SELECT COUNT(*) as totalCount FROM samaster WHERE TRIM(WAREHOUSE) = '%@'",strWarehouse];
          
            FMResultSet *res = [db executeQuery:strQuery];

           //FMResultSet *res = [db executeQuery:@"SELECT COUNT(*) as totalCount FROM samaster WHERE TRIM(WAREHOUSE) = '01'"];
            if (!res)
            {
                NSLog(@"Error: %@", [db lastErrorMessage]);
                //[[AppDelegate getAppDelegateObj].database close];
                return;
            }
            
            if ([res next])
            {
             totalCount = [[[res resultDictionary] objectForKey:@"totalCount"] intValue];
            }
            
            [res close];
            
            [self callGetProductListFromDB:db];
            
        }];

        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
            [localSpinner stopAnimating];
        }];
    }];
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    
    self.vwSegmentedControl = nil;
    self.tblProductList  = nil;
    self.lblWarehouse = nil;
    self.searchingBar = nil;
    self.vwResults = nil;
    self.vwContentSuperview = nil;
    self.vwNoResults = nil;
    self.spinner = nil;
    self.localSpinner = nil;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [operationQueue cancelAllOperations];
}

-(void) doAfterDataFetched{
    
    if([arrProducts count] < totalCount){
        
        [tblProductList reloadData];
        
        if (_loadMoreFooterView == nil) {
            [self setupLoadMoreFooterView];
        }
        
        [self repositionLoadMoreFooterView];
        
        // Dismiss loading footer
        [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
        recordNumber += ROWS_PER_PAGE;
       
        
    }
    else{
        [_loadMoreFooterView removeFromSuperview];
        _loadMoreFooterView=nil;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[filteredListContent removeAllObjects];
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    
    
    if ([scope isEqualToString:@"Code"] && ![searchText isEqualToString:@""]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetProductListByCode:db Code:searchText];
            }];
            
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContent count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblProductList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }

            }];
        }];

    }
    else if ([scope isEqualToString:@"Description"] && ![searchText isEqualToString:@""]){
        [operationQueue addOperationWithBlock:^{
            
            FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
                [self callGetProductListByDescription:db Description:searchText];
            }];

            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [vwContentSuperview removeAllSubviews];
                if ([filteredListContent count]) {
                    
                    [vwContentSuperview addSubview:vwResults];
                    [tblProductList reloadData];
                }
                else{
                    [vwContentSuperview addSubview:vwNoResults];
                }

            }];
        }];
    }
}


#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblProductList reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        self.isSearchMode = YES;
        [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles] objectAtIndex:searchBar.selectedScopeButtonIndex]];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    self.isSearchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [vwContentSuperview removeAllSubviews];
    [vwContentSuperview addSubview:vwResults];
    [tblProductList reloadData];
}


#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isSearchMode)
	{
        return [self.filteredListContent count];
    }
	else
	{
        return [self.arrProducts count];
    }
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER14;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:14];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.imgProduct.image  =[UIImage imageNamed:@"menu_bar.png"];

    
    if (isSearchMode)
	{
        NSString *user_Category = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"USER_CATEGORY_6"];
        if([user_Category isEqualToString:@"GOLD"])
        {
            cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];

        }
        cell.lblTitle.text = [[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Description"];
        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Price"]floatValue]];
    }
	else
	{
        NSString *user_Category = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"USER_CATEGORY_6"];
        if([user_Category isEqualToString:@"GOLD"])
        {
            cell.imgProduct.image  =[UIImage imageNamed:@"topbar.png"];
            
        }
        
        
        cell.lblTitle.text = [[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"];
        cell.lblValue.text = [NSString stringWithFormat:@"$%.2f",[[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Price"]floatValue]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //Resign the search bar keyboard
    [self.searchingBar resignFirstResponder];

    StockInfoProductDetailsViewController *dataViewController = [[StockInfoProductDetailsViewController alloc] initWithNibName:@"StockInfoProductDetailsViewController" bundle:[NSBundle mainBundle]];
    
    if (isSearchMode){
        dataViewController.strProductCode = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
        
        dataViewController.strProductName = [NSString stringWithFormat:@"%@",[[filteredListContent objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
    }
    else{
        dataViewController.strProductCode = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"StockCode"]];
        
        dataViewController.strProductName = [NSString stringWithFormat:@"%@",[[arrProducts objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
    }
    
    
    dataViewController.strWarehouse = strWarehouse;
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}


#pragma mark - UIScrollViewDelegate methods

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.isDragging) {
        
        CGFloat endOfTable = [self endOfTableView:scrollView];
        
        if (_loadMoreFooterView.state == StatePulling && endOfTable < 0.0f && endOfTable > -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StateNormal];
            
		} else if (_loadMoreFooterView.state == StateNormal && endOfTable < -60.0f && !_loadingInProgress) {
			[_loadMoreFooterView setState:StatePulling];
		}
	}
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if ([self endOfTableView:scrollView] <= -60.0f && !_loadingInProgress) {
        
        if([arrProducts count] < totalCount){
            // Show loading footer
            _loadingInProgress = YES;
            [_loadMoreFooterView setState:StateLoading];
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            tblProductList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 55.0f, 0.0f);
            [UIView commitAnimations];
            
            // Load some more data into table, pretend it took 3 seconds to get it
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:3.0];
        }
    }
}


#pragma mark - PullToLoadMoreView helper methods

- (void) setupLoadMoreFooterView
{    
    CGRect frame = CGRectMake(0.0f, [self tableViewContentHeight], self.view.frame.size.width, 60.0f);
    
    _loadMoreFooterView = [[PullToLoadMoreView alloc] initWithFrame:frame];
    _loadMoreFooterView.backgroundColor = [UIColor clearColor];
    
    [tblProductList addSubview:_loadMoreFooterView];
}

- (void) loadMoreData
{
    [operationQueue addOperationWithBlock:^{
        
        FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
        [databaseQueue   inDatabase:^(FMDatabase *db) {
            [self callGetProductListFromDB:db];
            
        }];
        
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self doAfterDataFetched];
        }];
    }];
}

- (void) didFinishLoadingMoreSampleData
{
    _loadingInProgress = NO;
    [tblProductList setContentInset:UIEdgeInsetsMake(0.0f,0.0f, 0.0f, 0.0f)];
    
    if ([_loadMoreFooterView state] != StateNormal) {
        [_loadMoreFooterView setState:StateNormal];
    }
}

- (CGFloat) tableViewContentHeight
{
    //NSLog(@"tblProductList.contentSize.height %f",tblProductList.contentSize.height);
    return tblProductList.contentSize.height;
}

- (CGFloat) endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewContentHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

- (void) repositionLoadMoreFooterView
{
    _loadMoreFooterView.center = CGPointMake(self.view.frame.size.width / 2,
                                             [self tableViewContentHeight] + _loadMoreFooterView.frame.size.height / 2);
}

#pragma mark - Memory management and cleanup

- (void) freeUp
{
    _loadMoreFooterView = nil;
    //[_sampleDataSource release];
}

#pragma mark - Database calls

-(void)callGetProductListFromDB:(FMDatabase *)db{

  
    FMResultSet *results;
    @try {
        
//        results = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available FROM samaster WHERE TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' ORDER BY DESCRIPTION LIMIT ?,?",strWarehouse, [NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        
        
        results = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,PICTURE_FIELD as Picture_field,USER_CATEGORY_6 as USER_CATEGORY_6,AVAILABLE as Available FROM samaster WHERE TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' AND Description !=''  ORDER BY DESCRIPTION LIMIT ?,?",strWarehouse, [NSNumber numberWithInt:recordNumber],[NSNumber numberWithInt:ROWS_PER_PAGE]];
        
        
        if (!results)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        while ([results next]) {
            NSMutableDictionary *dictData = (NSMutableDictionary *)[results resultDictionary];
            [dictData setValue:CHECKFLAG_NO forKey:@"CheckFlag"];
            if([dictData objectForKey:@"Description"])
            {
                if([[dictData objectForKey:@"Description"]trimSpaces].length > 0)
                {
                    [arrProducts addObject:dictData];
                }
            }
            
        }
        
        [results close];
    }
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [results close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);

    }
    
}
-(void)callGetProductListByCode:(FMDatabase *)db Code:(NSString *)code{

    FMResultSet *rs;
    @try {
        
        rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available from samaster where TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' AND CODE LIKE ?",strWarehouse, [NSString stringWithFormat:@"%%%@%%", code]];
        
        if (!rs)
        {
            [rs close];
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            NSDictionary *dictData = [[NSDictionary alloc] initWithDictionary:[rs resultDictionary]];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
    
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);

    }
    
}

-(void)callGetProductListByDescription:(FMDatabase *)db Description:(NSString *)description{
    
    FMResultSet *rs;
    
    @try {
        
         rs = [db executeQuery:@"SELECT SCM_RECNUM as ScmRecum,CODE as StockCode,DESCRIPTION as Description,LOCATION as Location,PRICE1 as Price,WAREHOUSE as Warehouse,AVAILABLE as Available from samaster where TRIM(WAREHOUSE) = ? AND STATUS <> 'OB' AND DESCRIPTION LIKE ?",strWarehouse, [NSString stringWithFormat:@"%%%@%%", description]];
        
        if (!rs)
        {
            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
        }
        
        
        while ([rs next]) {
            //NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            NSDictionary *dictData = [[NSDictionary alloc] initWithDictionary:[rs resultDictionary]];
            [filteredListContent addObject:dictData];
        }
        
        [rs close];
        
    }
    
    @catch (NSException* e) {
        // rethrow if not one of the two exceptions above
        
        [rs close];
        NSString *strErrorMessage = [NSString stringWithFormat:@"Exception : %@",[e description]];
        
        NSLog(@"%@",strErrorMessage);
    }
    
}

#pragma mark - WS Methods
-(void)callWSGetStockList:(int)pageIndex Warehouse:(NSString *)strWareHouse{
    
    _spinner = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    strWebserviceType = @"WS_GET_STOCK_LIST";
    
    NSString *last_sync_date_samaster = [[SDSyncEngine sharedEngine] mostRecentUpdatedAtDateForEntityWithName:@"samaster"];
    
    NSString *parameters = [NSString stringWithFormat:@"page=%d&limit=%d&warehouse=%@&last_syncDate=%@",pageIndex,ROWS_PER_PAGE,strWareHouse,last_sync_date_samaster];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@",WSURL,STOCK_LIST_WS,parameters];
    [ASIHTTPRequest_CentralizedHelper sharedHelper].delegate = (id)self;
    [[ASIHTTPRequest_CentralizedHelper sharedHelper] CallingWebservice:strURL onView:self.view];
}

#pragma mark - Centalized Class delegate Methods

-(void)ASIHTTPRequest_Success:(NSString *)responseStr nsMutableData:(id)resData Spinner:(MBProgressHUD *)spinnerObj ExpectedLength:(long long)expectedLength CurrentLength:(long long)currentLength
{
    NSMutableData *responseData = (NSMutableData*)resData;
    NSDictionary *dictResponse = [XMLReader dictionaryForXMLData:responseData error:nil];
    
    [_spinner removeFromSuperview];
    
    
    if ([strWebserviceType isEqualToString:@"WS_GET_STOCK_LIST"]){
        
        if ([[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"]) {
            //True
            if ([[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Flag"] objectForKey:@"text"] isEqualToString:@"True"]) {
                
                if(!self.arrProductsForWebservices){
                    arrProductsForWebservices = [[NSMutableArray alloc] init];
                }
                else
                {
                    [arrProductsForWebservices removeAllObjects];
                }
                
                if (_loadMoreFooterView == nil) {
                    [self setupLoadMoreFooterView];
                }
                
                //Total Records
                totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
                
                if ([[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                    
                    NSMutableDictionary *dict = [[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"];
                    [arrProductsForWebservices addObject:dict];
                }
                else{
                    [arrProductsForWebservices addObjectsFromArray:[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Data"]];
                }
                
                //dispatch_async(backgroundQueueForSamaster, ^(void) {
                    
//                    [dbQueueSamaster inTransaction:^(FMDatabase *db, BOOL *rollback) {
//                        @try
//                        {
//                            for (NSDictionary *dict in arrProductsForWebservices){
//                                
//                                BOOL y = [db executeUpdate:@"INSERT OR REPLACE INTO `samaster` (`SCM_RECNUM`, `WAREHOUSE`, `CODE`, `DESCRIPTION`, `PRICE1`, `AVAILABLE`, `LOCATION`,`RECNUM`, `created_date`, `modified_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)",
//                                          [[dict objectForKey:@"ScmRecum"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Warehouse"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"StockCode"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Description"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Price"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Available"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Location"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"Recum"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"CreatedDate"] objectForKey:@"text"],
//                                          [[dict objectForKey:@"ModifiedDate"] objectForKey:@"text"]];
//                                
//                                if (!y)
//                                {
//                                    NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
//                                    @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
//                                }
//                                
//                            }
//                            //Commit here
//                            [db commit];
//                            
//                        }
//                        @catch(NSException* e)
//                        {
//                            *rollback = YES;
//                            // rethrow if not one of the two exceptions above
//                            
//                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
//                            dispatch_async(dispatch_get_main_queue(), ^(void) {
//                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                [alert show];
//                                
//                            });
//
//                            
//                            //[db close];
//                        }
//                        
//                        @finally {
//                            [self callGetProductListFromDB:db];
//                        }
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            @try
//                            {
//                                [tblProductList reloadData];
//                                
//                                if([arrProductsForWebservices count] < totalCount){
//                                    
//                                    [self repositionLoadMoreFooterView];
//                                    
//                                    // Dismiss loading footer
//                                    [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
//                                    
//                                    currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
//                                    
//                                    //Next page
//                                    currPage ++;
//                                    
//                                    recordNumber += ROWS_PER_PAGE;
//                                    
//                                }
//                                else{
//                                    [_loadMoreFooterView removeFromSuperview];
//                                    _loadMoreFooterView=nil;
//                                }
//                            }
//                            @catch(NSException *e)
//                            {
//                                NSLog(@"Exception : %@",[e reason]);
//                                //*rollback = YES;
//                                //[db close];
//                            }
//                            
//                        });
//                        
//                    }];
                    
               // });
            }
            //False
            else{
                NSString *strErrorMessage = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"Message"] objectForKey:@"text"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }
        else if([[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"UpdateInfo"]){
            
            if (_loadMoreFooterView == nil) {
                [self setupLoadMoreFooterView];
            }
            
            totalCount = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"RowsCount"] objectForKey:@"text"] intValue];
            
            //dispatch_async(backgroundQueueForSamaster, ^(void) {
                
                [dbQueueSamaster inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    @try{
                        
                        //Update the New "Last sync" date
                        NSString *strNewSyncDate = [[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"LastSyncDate"] objectForKey:@"text"];
                        
                        BOOL y = [[SDSyncEngine sharedEngine] setUpdatedLastDateForEntityWithName:@"samaster" NewSyncDate:strNewSyncDate Database:db];
                        
                        if (!y) {
                            NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[db lastErrorMessage]];
                            @throw [NSException exceptionWithName:@"Local DB Error" reason:strErrorMessage userInfo:nil];
                        }
                        
                        *rollback = NO;
                        //Commit here
                        [db commit];
                    }
                    @catch(NSException* e)
                    {
                        *rollback = YES;
                        // rethrow if not one of the two exceptions above
                        
                        NSString *strErrorMessage = [NSString stringWithFormat:@"%@",[e description]];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:strErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                        });

                        
                        //[db close];
                    }
                    
                    @finally {
                        [self callGetProductListFromDB:db];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        @try
                        {
                            [tblProductList reloadData];
                            
                            if([arrProducts count] < totalCount){
                                [self repositionLoadMoreFooterView];
                                
                                // Dismiss loading footer
                                [self performSelector:@selector(didFinishLoadingMoreSampleData) withObject:nil];
                                
                                currPage = [[[[[dictResponse objectForKey:@"Response"] objectForKey:@"Stockinfo"] objectForKey:@"IndexPage"] objectForKey:@"text"] intValue];
                                
                                //currPage = 1;
                                
                                //Next page
                                currPage ++;
                                
                                recordNumber += ROWS_PER_PAGE;
                            }
                            else{
                                
                                [_loadMoreFooterView removeFromSuperview];
                                _loadMoreFooterView=nil;
                            }
                        }
                        @catch(NSException *e)
                        {
                            NSLog(@"Exception : %@",[e reason]);
                            //[db rollback];
                            //[db close];
                        }
                    });
                }];
            //});
        }
    }
    
}

-(void)ASIHTTPRequest_Error:(id)error
{
    
    NSLog(@"error %@",error);
    
    NSError *err = error;
    
    
    
    NSString *strErrorMessage;
    if (err.code == 2) {
        
        strErrorMessage = SERVER_OFFLINE_MESSAGE;
    }
    else{
        strErrorMessage = ERROR_IN_CONNECTION_MESSAGE;
    }
    
    [AJNotificationView showNoticeInView:self.view
                                    type:AJNotificationTypeRed
                                   title:strErrorMessage
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
    strErrorMessage = nil;

    
}

#pragma mark - Reachability class delegate Methods
-(void)checkNetworkStatus:(NetworkStatus)newtworkStatus{
    
    switch (newtworkStatus)
    {
        case NotReachable:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            [AppDelegate getAppDelegateObj].rootViewController.isNetworkConnected = YES;
            break;
        }
            
    }
    
    [[AppDelegate getAppDelegateObj].rootViewController setInternetStatus];
}

@end
