//
//  ViewController.h
//  JFDepthVewExample
//
//  Created by Jeremy Fox on 10/17/12.
//  Copyright (c) 2012 Jeremy Fox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JFDepthView.h"

@interface TopViewController : UIViewController{
    NSArray *arrHeaderLabels;
}

@property (strong, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) JFDepthView* depthViewReference;
@property (weak, nonatomic) UIView* presentedInView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UITableView *tblProductDetails;
@property (nonatomic,retain)  NSArray *arrHeaderLabels;
@property (nonatomic,retain)  NSString *strCode;
@property (nonatomic,retain)  NSDictionary *dictProductDetails;

- (IBAction)closeView:(id)sender;
@end
