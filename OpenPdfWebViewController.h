#import <UIKit/UIKit.h>

@interface OpenPdfWebViewController : UIViewController <UIWebViewDelegate,UIScrollViewDelegate>
{
    
}
@property (nonatomic,retain) NSString *strPdfName;
@property (nonatomic,retain) NSString *webviewLink;
@property (nonatomic,unsafe_unretained) IBOutlet UIWebView *webViewProductDesc; 
@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

-(IBAction)dismissView:(id)sender;
-(NSString *) md5:(NSString *) str;
@end
