//
//  StockPickupViewController.h
//  Blayney
//
//  Created by Pooja on 17/12/14.
//  Copyright (c) 2014 ashutosh duingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReasonListPopOverViewController.h"
#import "StockCodeViewController.h"
#import "DatePopOverController.h"
#import "EmailAddressBookViewController.h"
@protocol StockPickupViewDelegate
- (void)dismissStockPickUp:(NSString *)strData withDict:(NSMutableDictionary*)stockPickupDict;@end

@interface StockPickupViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIPopoverControllerDelegate,DismissPopOverDelegate,DismissPopOverDelegateForReasonDelegate,SetDeliveryDateDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,DismissEmailPopOverDelegate>{
    
    UIPopoverController *popoverController;
    UIPopoverController *popoverControllerForImage;

    int tagTxtReason;
    int selectedRow;
}

@property (nonatomic,retain) NSMutableDictionary *debtorDetailDict;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@property(nonatomic,retain) UIPopoverController *popoverController;
@property(nonatomic,retain) UIPopoverController *popoverControllerForImage;

@property (weak, nonatomic) IBOutlet UITextField *dateRaised;
@property (weak, nonatomic) IBOutlet UITextField *requestedByTxt;

@property (weak, nonatomic) IBOutlet UITextField *emailRecipientTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailCcTxt;

@property(weak,nonatomic)id <StockPickupViewDelegate> delegate;
- (IBAction)addEmailcc:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *batchNoTxt;
@property (weak, nonatomic) IBOutlet UITextField *contactPersonTxt;
@property (weak, nonatomic) IBOutlet UITextField *returnTxt;

@property (weak, nonatomic) IBOutlet UITextField *invoiceNoTxt;
@property (weak, nonatomic) IBOutlet UIButton *closeBn;
@property (weak, nonatomic) IBOutlet UITextField *contactPhoneTxt;

@property (weak, nonatomic) IBOutlet UITextField *custAccNoTxt;
@property (weak, nonatomic) IBOutlet UITextField *photoOfProdTxt;

@property (weak, nonatomic) IBOutlet UITextField *custNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *reasonReturnTxt;
@property (weak, nonatomic) IBOutlet UITextField *bestBeforeDateTxt;
@property (weak, nonatomic) IBOutlet UITextField *batchDetailsTxt;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTxt;
@property (weak, nonatomic) IBOutlet UITextField *productCodeTxt;
@property (weak, nonatomic) IBOutlet UITextField *quantityTxt;
@property (weak, nonatomic) IBOutlet UITextView *reasonTextView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)actionAddEmailCc:(id)sender;

- (IBAction)actionDismiss:(id)sender;
- (IBAction)actionBestBeforeDate:(id)sender;

- (IBAction)actionPopViewForTextField:(id)sender;
- (IBAction)actionReturnValues:(id)sender;


@end
