//
//  StartStartViewController.h
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 24/07/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "LoginViewController.h"

@interface StartStartViewController : UIViewController<UIAlertViewDelegate>{
    NSOperationQueue *downloadOperationQueue;
    int cntWebserviceNumber;
    BOOL isQueueCancelled;
    int numDatabaseDumped;
    int queueSize;
}

@property(nonatomic,unsafe_unretained)IBOutlet UIImageView *imgView;
@property(nonatomic,unsafe_unretained)IBOutlet UILabel *lblStatus;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *lblPercentageIndicator;

@property (atomic, readonly) BOOL syncInProgress;
@property (atomic, readonly) NSOperationQueue *downloadOperationQueue;
@property (assign)BOOL isQueueCancelled;
@property (assign) int numDatabaseDumped;

+ (StartStartViewController *)sharedEngine;

- (void)registerNSManagedObjectClassToSync:(NSString *)tableName;
- (void)startSync;
- (NSString *)mostRecentUpdatedAtDateForEntityWithName:(NSString *)entityName;
- (NSDate *)dateUsingStringFromAPI:(NSString *)dateString;
- (NSString *)dateStringForAPIUsingDate:(NSDate *)date;
- (BOOL)setUpdatedLastDateForEntityWithName:(NSString *)entityName NewSyncDate:(NSString *)newSyncDate Database:(FMDatabase *)db;

@end
