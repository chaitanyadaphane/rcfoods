//
//  StockCodeViewController.m
//  MySCM_iPad
//
//  Created by Shival on 17/01/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "StockCodeViewController.h"

@interface StockCodeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation StockCodeViewController
@synthesize isStockCode;
@synthesize arrData;
@synthesize delegate;
@synthesize strTextField;
@synthesize strDescriptionTxtField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tblView reloadData];
}

#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrData count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *strdata  = @"";
    if (isStockCode == YES) {
        strdata = [NSString stringWithFormat:@"%@ - %@",[[arrData objectAtIndex:[indexPath row]]objectForKey:@"CODE"],[[arrData objectAtIndex:[indexPath row]]objectForKey:@"DESCRIPTION"]];

    }
    else{
        strdata = [NSString stringWithFormat:@"%@",[arrData objectAtIndex:[indexPath row]]];
    }
    cell.textLabel.text = strdata;
    
    return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isStockCode == YES) {
        [delegate actiondismissPickUpValue:[arrData objectAtIndex:[indexPath row]] strTextField:strTextField];
    }
    else{
        [delegate actiondismissPopOverStockPickUp:[arrData objectAtIndex:[indexPath row]] strTextField:strTextField];
    }
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
