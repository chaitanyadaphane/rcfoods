//
//  CopyProfileViewController.h
//  Blayney
//
//  Created by Pooja on 24/02/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CopyProfileViewController : UIViewController
{
    NSMutableArray *profileProdArr;
}
@property (weak, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic)NSString *customerNamestr;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll;
- (IBAction)selectAllAction:(id)sender;

@property (nonatomic, retain) NSMutableArray *arrProductProfile;
@property (nonatomic, retain) NSMutableArray *arrSelectedProducts;

@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblProductList;

@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwResults;
@property (nonatomic,retain) IBOutlet UIView *vwNoResults;

@property (nonatomic,unsafe_unretained) IBOutlet UIActivityIndicatorView *localSpinner;

@end
