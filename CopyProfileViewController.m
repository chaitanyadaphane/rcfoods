//
//  CopyProfileViewController.m
//  Blayney
//
//  Created by Pooja on 24/02/15.
//  Copyright (c) 2015 ashutosh dingankar. All rights reserved.
//

#import "CopyProfileViewController.h"
#import "CustomCellGeneral.h"

@interface CopyProfileViewController ()

@end

@implementation CopyProfileViewController
@synthesize arrProductProfile;
@synthesize tblProductList;
@synthesize arrSelectedProducts;
@synthesize vwContentSuperview;
@synthesize vwNoResults;
@synthesize vwResults;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (arrProductProfile.count > 0) {
        _customerNameLbl.text = _customerNamestr;
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwResults];
    }
    else{
         _customerNameLbl.text = @"";
        [vwContentSuperview removeAllSubviews];
        [vwContentSuperview addSubview:vwNoResults];

    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
  
        return [self.arrProductProfile count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER7;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        cell = [nib objectAtIndex:6];
    }
    
    NSMutableDictionary *item;
   
        cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrProductProfile objectAtIndex:[indexPath row]] objectForKey:@"Description"]];
        //        cell.lblValue.text = @"";
        
        NSString *availVal =[[arrProductProfile objectAtIndex:[indexPath row]] objectForKey:@"Available"];
        cell.lblValue.text =[NSString stringWithFormat:@"AVAIL : %@",(availVal)?availVal:@""];
        item = [arrProductProfile objectAtIndex:indexPath.row];
        
    
    [item setObject:cell forKey:@"cell"];
    
    BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    UIImage *image = (checked) ? [[UIImage alloc]initWithContentsOfFile:strImage1] : [[UIImage alloc]initWithContentsOfFile:strImage2];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    button.frame = frame;	// match the button's size with the image size
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
    [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self tableView: tblProductList accessoryButtonTappedForRowWithIndexPath: indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *item ;
    
  
        item = [arrProductProfile objectAtIndex:indexPath.row];
    
    BOOL checked = [[item objectForKey:@"CheckFlag"] boolValue];
    
    
    if (checked) {
        [arrSelectedProducts removeObject:item];
    }
    else{
        [arrSelectedProducts addObject:item];
    }
    
    [item setObject:[NSNumber numberWithBool:!checked] forKey:@"CheckFlag"];
    
    UITableViewCell *cell = [item objectForKey:@"cell"];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    NSString *strImage1=[[NSBundle mainBundle] pathForResource:@"checked" ofType:@"png"];
    NSString *strImage2=[[NSBundle mainBundle] pathForResource:@"unchecked" ofType:@"png"];
    
    UIImage *newImage = (checked) ? [[UIImage alloc]initWithContentsOfFile:strImage2] : [[UIImage alloc]initWithContentsOfFile:strImage1];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:item, @"source", nil]];
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:tblProductList];
    NSIndexPath *indexPath = [tblProductList indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView: tblProductList accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}


- (IBAction)selectAllAction:(id)sender {
    arrSelectedProducts = [arrProductProfile mutableCopy];
    
    for (int i = 0 ; i < arrProductProfile.count; i++) {
        [[arrProductProfile objectAtIndex:i] setObject:[NSNumber numberWithBool:1] forKey:@"CheckFlag"];
    }
    [tblProductList reloadData];
    
    

    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadAddProductDetailsNotification"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:arrProductProfile, @"source_select_all", nil]];
    
    [self.view removeAllSubviews];
}
@end
