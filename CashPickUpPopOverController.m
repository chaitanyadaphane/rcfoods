//
//  CashPickUpPopOverController.m
//  MySCM_iPad
//
//  Created by ashutosh dingankar on 08/05/13.
//  Copyright (c) 2013 ashutosh dingankar. All rights reserved.
//

#import "CashPickUpPopOverController.h"

@interface CashPickUpPopOverController ()

@end

@implementation CashPickUpPopOverController
@synthesize txtCashPickAmount,swtchChequeNotAvailable,delegate,strCashAmount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [txtCashPickAmount becomeFirstResponder];
    txtCashPickAmount.text = strCashAmount;
    
}

- (void)viewDidUnload{
    txtCashPickAmount = nil;
    swtchChequeNotAvailable = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
-(IBAction)actionSetChequeAvailability:(id)sender{
    if (swtchChequeNotAvailable.on) {
        isChequeAvailable = YES;
    }
    else{
        isChequeAvailable = NO;
    }
}

-(IBAction)actionDissmissPopOver:(id)sender{
    [delegate actiondismissPopOverForCashPickUp:txtCashPickAmount.text IsChequeAvaliable:isChequeAvailable];
}

#pragma mark - Text Field Delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
   return [[AppDelegate getAppDelegateObj] allowFloatingNumbersOnly:[textField.text stringByReplacingCharactersInRange:range withString:string]];
}

@end
