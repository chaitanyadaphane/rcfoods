//
//  StockPickupViewController.m
//  Blayney
//
//  Created by Pooja on 31/12/14.
//  Copyright (c) 2014 ashutosh dingankar. All rights reserved.
//

#import "StockPickupViewListController.h"
#import "StockPickupInfodetailVC.h"
#import "CustomCellGeneral.h"

@interface StockPickupViewListController (){
    UILabel *emptyDataLbl;
    StockPickupInfodetailVC *dataViewController ;
}

@end

@implementation StockPickupViewListController
@synthesize arrStockPickup;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    FMDatabaseQueue *databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[AppDelegate getFileFromDocumentsDirectory:DATABASE_NAME]];
    [databaseQueue   inDatabase:^(FMDatabase *db) {
        FMResultSet *res = [db executeQuery:@"SELECT PICKUP_NO,DEBTOR_NAME FROM StockPickup"];
        
        if (!res)
        {
            NSLog(@"Error: %@", [db lastErrorMessage]);
            //[[AppDelegate getAppDelegateObj].database close];
            return;
        }
        
        arrStockPickup = [[NSMutableArray alloc] init];
        
        while ([res next]) {
            NSDictionary *dictData = [res resultDictionary];
            if([dictData objectForKey:@"DEBTOR_NAME"])
            {
                NSString *str = [dictData objectForKey:@"DEBTOR_NAME"];
//                NSString *strPickup = [dictData objectForKey:@"PICKUP_NO"];
                str = [str trimSpaces];
                if(str.length > 0)
                {
                    [arrStockPickup addObject:dictData];
                }
            }
            
            dictData = nil;
        }
        
        [res close];
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (arrStockPickup.count == 0) {
        emptyDataLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
        emptyDataLbl.text = @"No Stock Pickup data";
        emptyDataLbl.alpha = 0.5;
        emptyDataLbl.center = self.view.center;
        emptyDataLbl.textColor = [UIColor blackColor];
        emptyDataLbl.backgroundColor = [UIColor clearColor];
        emptyDataLbl.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:emptyDataLbl];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark Table view data source and delegate

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 50;
    // Alternatively, return rowHeight.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrStockPickup count];
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString *CellIdentifier = CELL_IDENTIFIER8;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CustomCellGeneral *cell = (CustomCellGeneral *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib= [[NSBundle mainBundle] loadNibNamed:@"CustomCellGeneral" owner:self options:nil];
        
        cell = [nib objectAtIndex:7];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.lblCustomerName.text = [[arrStockPickup objectAtIndex:indexPath.row] objectForKey:@"DEBTOR_NAME"];
    
    cell.lblQuoteNo.text = @"";
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    StockPickupInfodetailVC *dataViewController = [[StockPickupInfodetailVC alloc] initWithNibName:@"StockPickupInfodetailVC" bundle:[NSBundle mainBundle]];
    dataViewController = [[StockPickupInfodetailVC alloc] initWithNibName:@"StockPickupInfodetailVC" bundle:[NSBundle mainBundle]];
    
    dataViewController.strPickupNo = [[arrStockPickup objectAtIndex:indexPath.row] objectForKey:@"PICKUP_NO"];
    dataViewController.strDebtorName = [[arrStockPickup objectAtIndex:indexPath.row] objectForKey:@"DEBTOR_NAME"];
    
    [[AppDelegate getAppDelegateObj].rootViewController.stackScrollViewController addViewInSlider:dataViewController invokeByController:self isStackStartView:FALSE];
}


@end
