//
//  AddQuoteViewController.h
//  MySCM_iPad
//
//  Created by Ashutosh Dingankar on 19/12/12.
//  Copyright (c) 2012 ashutosh dingankar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEFilterControl.h"
#import "QuoteMenuPopOverViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewQuoteProductDetailsViewController.h"
#import <MessageUI/MessageUI.h>
#import "DownloadUrlOperation.h"
#import "QuoteProductListViewController.h"
#import "QuoteProductComment.h"
#import "StockCodeViewController.h"

@interface AddQuoteViewController : UIViewController<ASIHTTPRequest_delegate,UITextFieldDelegate,ReachabilityRequest_delegate,UIPopoverControllerDelegate,QuoteSetProfitDelegate,MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate,UIAlertViewDelegate,SetCommentDelegate,DismissPopOverDelegate>{
    
    
    NSArray *arrHeaderLabels;
    NSMutableArray *arrProducts;
        
    CALayer *layerPointingImage;
    
    SEFilterControl *segControl;
    
    NSMutableDictionary *dictHeaderDetails;
    
    NSString *strWebserviceType;
    
    dispatch_queue_t backgroundQueueForNewQuote;
    FMDatabaseQueue *dbQueueNewQuote;
    
    //Reachability *checkInt;
    //NetworkStatus internetStatus;
    
    BOOL isNetworkConnected;   
    NSString *strQuoteNum;
    
    UIPopoverController *popoverController;
    
    
    BOOL chkFinalize;
    
    NSMutableArray *tempArrForEdit;
    
    int countval;
    
    //Delete
    NSIndexPath *deleteIndexPath;
    int deleteRowIndex;
    //int tagFlsh;
    
    int finalise;
    
    NSString *strDebtorNum;
    
    NSOperationQueue *operationQueue;
    
    int queueSize;
    
    MBProgressHUD *spinner;
    
    NSInteger selectedSectionIndex;
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll;
- (IBAction)actionSelectAll:(id)sender;

@property (nonatomic,retain) NSArray *arrHeaderLabels;
@property (nonatomic,retain) NSMutableArray *arrcopyprofileArr;
@property (nonatomic,retain) NSMutableArray *arrProducts;
@property (nonatomic,retain) NSDictionary *dictHeaderDetails;
@property (nonatomic,retain) NSString *strQuoteNum;
@property (nonatomic,retain) UIPopoverController *popoverController;
@property (nonatomic,retain) NSString *strQuoteNumRecived;
@property (nonatomic,retain) NSString *strDebtorNum;
@property (nonatomic, assign) BOOL EditableMode;
@property (nonatomic, assign) int finalise;
@property (nonatomic,retain) NSIndexPath *deleteIndexPath;
@property (assign) int deleteRowIndex;

@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnEmail;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnPrint;
@property (nonatomic,unsafe_unretained) IBOutlet UILabel *lblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnDelete;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnLoadProfile;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnSaveDetails;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddProducts;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnFinalise;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowDownwards;
@property (nonatomic,unsafe_unretained) IBOutlet UIImageView *imgArrowRightWards;
@property (nonatomic,retain) IBOutlet UIView *vwSegmentedControl;
@property (nonatomic,retain) IBOutlet UIView *vwContentSuperview;
@property (nonatomic,retain) IBOutlet UIView *vwHeader;
@property (nonatomic,retain) IBOutlet UIView *vwDetails;
@property (nonatomic,retain) IBOutlet UIView *vwNoProducts;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblHeader;
@property (nonatomic,unsafe_unretained) IBOutlet UITableView *tblDetails;
@property (nonatomic,retain) IBOutlet UIToolbar *keyboardToolBar;
@property (nonatomic,unsafe_unretained) IBOutlet UIButton *btnAddMenu;

@property (nonatomic, unsafe_unretained) IBOutlet UIProgressView *progessView;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *lblPercentageIndicator;
@property (nonatomic,assign)BOOL isCommentAddedOnLastLine;



#pragma mark - IBAction
- (IBAction)actionShowQuoteHistory:(id)sender;
- (IBAction)actionShowProductList:(id)sender;
- (IBAction)actionDeleteRows:(id)sender;

- (void)showOrEnableButtons;
- (void)hideOrDisableButtons;
//- (void)callSaveQuoteHeaderDetailsToDB:(FMDatabase *)db;
- (void)doAfterdelete;
- (void)showPointingAnimation;
- (IBAction)btnCopyProfile:(id)sender;

@end


